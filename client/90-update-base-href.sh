#!/bin/bash

if [[ -v ANIS_BASE_HREF ]]; then
  sed -i 's@<base href="/">@<base href="'$ANIS_BASE_HREF'">@' /usr/share/nginx/html/index.html
fi

exit 0
