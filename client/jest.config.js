// jest.config.js
module.exports = {
    preset: 'jest-preset-angular',
    setupFilesAfterEnv: ['<rootDir>/setup-jest.ts'],
    //globalSetup: 'jest-preset-angular/global-setup',
    moduleDirectories: ['node_modules', '<rootDir>'],
    collectCoverage: false,
    collectCoverageFrom: ['src/**/*.ts'],
    coverageReporters: ['html'],
    coverageDirectory: 'coverage/anis-client',
    moduleNameMapper: { 'd3(.*)': '<rootDir>/node_modules/d3$1/dist/d3$1.min.js' }
};
