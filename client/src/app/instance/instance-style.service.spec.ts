/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { StyleService } from "../shared/services/style.service";
import { InstanceStyleService } from "./instance-style.service";
import { DESIGN_CONFIG } from "src/test-data";

describe('[Instance] InstanceStyleService', () => {
    class mockStyleService extends StyleService {
        override setStyle = jest.fn().mockImplementation(() => {})
        override setStyles = jest.fn()
    }
    
    let service: InstanceStyleService;
    let designConfig = { ...DESIGN_CONFIG };
    
    beforeEach(() =>{
        service = new InstanceStyleService(new mockStyleService());
    });
  
    it('call all services  privates methods', () => {
        let spyOnGeneralStyle = jest.spyOn((service as any), 'generalStyle');
        let spyOnNavbarStyle =jest.spyOn((service as any), 'navbarStyle');
        let spyOnFooterStyle =jest.spyOn((service as any), 'footerStyle');
        let spyOnSearchProgressBarStyle = jest.spyOn((service as any), 'searchProgressBarStyle')
        let spyOnsearchNextBackButtonsStyle = jest.spyOn((service as any), 'searchNextBackButtonsStyle');
        let spyOnSearchFamilyStyle = jest.spyOn((service as any), 'searchFamilyStyle');
        let spyOnSearchInfoStyle = jest.spyOn((service as any), 'searchInfoStyle');
        let spyOnSearchDatasetSelectionStyle = jest.spyOn((service as any), 'searchDatasetSelectionStyle');
        let spyOnSearchCriteriaStyle =jest.spyOn((service as any), 'searchCriteriaStyle');
        let spyOnSearchOutputColumnsStyle = jest.spyOn((service as any), 'searchOutputColumnsStyle');
        let spyOnSearchResultTable= jest.spyOn((service as any), 'searchResultTable');
        service.applyInstanceStyle(designConfig)
        expect(spyOnGeneralStyle).toHaveBeenCalledTimes(1);
        expect(spyOnNavbarStyle).toHaveBeenCalledTimes(1);
        expect(spyOnFooterStyle).toHaveBeenCalledTimes(1);
    });

    it('call set style for bordered data_table', () => {
        let spyOnGeneralStyle = jest.spyOn((service as any), 'generalStyle');
        let spyOnNavbarStyle =jest.spyOn((service as any), 'navbarStyle');
        let spyOnFooterStyle =jest.spyOn((service as any), 'footerStyle');
        designConfig.result_datatable_bordered = true;
        designConfig.result_datatable_bordered_radius = true;
        service.applyInstanceStyle(designConfig)
        expect(spyOnGeneralStyle).toHaveBeenCalledTimes(1);
        expect(spyOnNavbarStyle).toHaveBeenCalledTimes(1);
        expect(spyOnFooterStyle).toHaveBeenCalledTimes(1);
    });
});
