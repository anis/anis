/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { UserProfile } from 'src/app/user/store/models/user-profile.model';
import { Instance, DesignConfig, MenuItem } from 'src/app/metamodel/models';
import * as authActions from 'src/app/user/store/actions/auth.actions';
import * as authSelector from 'src/app/user/store/selectors/auth.selector';
import * as designConfigActions from 'src/app/metamodel/actions/design-config.actions';
import * as designConfigSelector from 'src/app/metamodel/selectors/design-config.selector';
import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as datasetFamilyActions from 'src/app/metamodel/actions/dataset-family.actions';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as menuItemActions from 'src/app/metamodel/actions/menu-item.actions';
import * as menuItemSelector from 'src/app/metamodel/selectors/menu-item.selector';

@Component({
    selector: 'app-instance',
    templateUrl: 'instance.component.html'
})
export class InstanceComponent implements OnInit {
    public favIcon: HTMLLinkElement = document.querySelector('#favicon');
    public body: HTMLBodyElement = document.querySelector('body');
    public isAuthenticated: Observable<boolean>;
    public userProfile: Observable<UserProfile>;
    public userRoles: Observable<string[]>;
    public instance: Observable<Instance>;
    public designConfig: Observable<DesignConfig>;
    public designConfigIsLoading: Observable<boolean>;
    public designConfigIsLoaded: Observable<boolean>;
    public menuItemList: Observable<MenuItem[]>;
    public menuItemListIsLoading: Observable<boolean>;
    public menuItemListIsLoaded: Observable<boolean>;
    public datasetListIsLoading: Observable<boolean>;
    public datasetListIsLoaded: Observable<boolean>;

    constructor(
        private store: Store<{ }>
    ) {
        this.isAuthenticated = store.select(authSelector.selectIsAuthenticated);
        this.userProfile = store.select(authSelector.selectUserProfile);
        this.userRoles = store.select(authSelector.selectUserRoles);
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.designConfig = store.select(designConfigSelector.selectDesignConfig);
        this.designConfigIsLoading = store.select(designConfigSelector.selectDesignConfigIsLoading);
        this.designConfigIsLoaded = store.select(designConfigSelector.selectDesignConfigIsLoaded);
        this.menuItemList = store.select(menuItemSelector.selectAllMenuItems);
        this.menuItemListIsLoading = store.select(menuItemSelector.selectMenuItemListIsLoading);
        this.menuItemListIsLoaded = store.select(menuItemSelector.selectMenuItemListIsLoaded);
        this.datasetListIsLoading = store.select(datasetSelector.selectDatasetListIsLoading);
        this.datasetListIsLoaded = store.select(datasetSelector.selectDatasetListIsLoaded);
    }

    ngOnInit() {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(designConfigActions.loadDesignConfig()));
        Promise.resolve(null).then(() => this.store.dispatch(menuItemActions.loadMenuItemList()));
        Promise.resolve(null).then(() => this.store.dispatch(datasetActions.loadDatasetList()));
        Promise.resolve(null).then(() => this.store.dispatch(datasetFamilyActions.loadDatasetFamilyList()));
    }

    /**
     * Dispatches action to log in.
     */
    login(): void {
        this.store.dispatch(authActions.login({ redirectUri: window.location.toString() }));
    }

    /**
     * Dispatches action to log out.
     */
    logout(): void {
        this.store.dispatch(authActions.logout());
    }

    /**
     * Dispatches action to open profile editor.
     */
    openEditProfile(): void {
        this.store.dispatch(authActions.openEditProfile());
    }
}
