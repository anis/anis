/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SearchEffects } from './search.effects';
import { SearchMultipleEffects } from './search-multiple.effects';
import { ConeSearchEffects } from './cone-search.effects';
import { DetailEffects } from './detail.effects';
import { ArchiveEffects } from './archive.effects';

export const instanceEffects = [
    SearchEffects,
    SearchMultipleEffects,
    ConeSearchEffects,
    DetailEffects,
    ArchiveEffects
];
