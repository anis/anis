/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';

import { provideMockActions } from '@ngrx/effects/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';

import { SearchMultipleEffects } from './search-multiple.effects';
import { SearchService } from '../services/search.service';
import * as fromSearch from '../reducers/search.reducer';
import * as fromSearchMultiple from '../reducers/search-multiple.reducer';
import * as fromInstance from '../../../metamodel/reducers/instance.reducer';
import * as datasetSelector from '../../../metamodel/selectors/dataset.selector';
import * as coneSearchSelector from '../selectors/cone-search.selector';
import * as coneSearchActions from '../actions/cone-search.actions';
import { ConeSearch, SearchMultipleDatasetLength } from '../models';
import * as searchMultipleSelector from '../selectors/search-multiple.selector';
import * as instanceSelector from '../../../metamodel/selectors/instance.selector';
import * as designConfigSelector from 'src/app/metamodel/selectors/design-config.selector';
import * as searchMultipleActions from '../actions/search-multiple.actions';
import * as authSelector from 'src/app/user/store/selectors/auth.selector';
import { AppConfigService } from 'src/app/app-config.service';
import { INSTANCE, DATASET, DESIGN_CONFIG } from 'src/test-data';

describe('[Instance][Store] SearchMultipleEffects', () => {
    let actions = new Observable();
    let effects: SearchMultipleEffects;
    let metadata: EffectsMetadata<SearchMultipleEffects>;
    let searchService: SearchService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = {
        metamodel: { instance: { ...fromInstance.initialState }},
        instance: {
            search: {...fromSearch.initialState},
            searchMultiple: {...fromSearchMultiple.initialState}
        }
    };
    let mockSearchMultipleSelectorSelectPristine;
    let mockSearchMultipleSelectorSelectSelectedDatasetsByRoute;
    let mockSearchMultipleSelectorSelectSelectedDatasets;
    let mockConeSearchSelectorSelectConeSearchByRoute;
    let mockConeSearchSelectorSelectConeSearch;
    let mockInstanceSelectorSelectorSelectInstanceByRouteName;
    let mockInstanceSelectorSelectorSelectInstanceNameByRoute;
    let mockDesignConfigSelectorSelectDesignConfig;
    let mockDatasetSelectorSelectAllConeSearchDatasets;
    let mockAuthSelectIsAuthenticated;
    let mockAuthSelectorSelectUserRoles;

    let appConfigServiceStub = new AppConfigService();
    appConfigServiceStub.authenticationEnabled = false;
    appConfigServiceStub.adminRoles = ['test'];

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                SearchMultipleEffects,
                { provide: AppConfigService, useValue: appConfigServiceStub },
                { provide: SearchService, useValue: { retrieveDataLength: jest.fn() }},
                { provide: ToastrService, useValue: { error: jest.fn() }},
                provideMockActions(() => actions),
                provideMockStore({ initialState }),
            ]
        }).compileComponents();
        effects = TestBed.inject(SearchMultipleEffects);
        metadata = getEffectsMetadata(effects);
        searchService = TestBed.inject(SearchService);
        toastr = TestBed.inject(ToastrService);
        store = TestBed.inject(MockStore);
        mockSearchMultipleSelectorSelectPristine = store.overrideSelector(
            searchMultipleSelector.selectPristine,
            true
        );
        mockSearchMultipleSelectorSelectSelectedDatasetsByRoute = store.overrideSelector(
            searchMultipleSelector.selectSelectedDatasetsByRoute,
            ''
        );
        mockSearchMultipleSelectorSelectSelectedDatasets = store.overrideSelector(
            searchMultipleSelector.selectSelectedDatasets,
            []
        );
        mockConeSearchSelectorSelectConeSearchByRoute = store.overrideSelector(
            coneSearchSelector.selectConeSearchByRoute,
            ''
        );
        mockConeSearchSelectorSelectConeSearch = store.overrideSelector(
            coneSearchSelector.selectConeSearch,
            undefined
        );
        mockInstanceSelectorSelectorSelectInstanceByRouteName = store.overrideSelector(
            instanceSelector.selectInstanceByRouteName,
            undefined
        );
        mockInstanceSelectorSelectorSelectInstanceNameByRoute = store.overrideSelector(
            instanceSelector.selectInstanceNameByRoute,
            undefined
        );
        mockDesignConfigSelectorSelectDesignConfig = store.overrideSelector(
            designConfigSelector.selectDesignConfig,
            undefined
        );
        mockDatasetSelectorSelectAllConeSearchDatasets = store.overrideSelector(
            datasetSelector.selectAllConeSearchDatasets,
            []
        );
        mockAuthSelectIsAuthenticated = store.overrideSelector(
            authSelector.selectIsAuthenticated,
            false
        );
        mockAuthSelectorSelectUserRoles = store.overrideSelector(
            authSelector.selectUserRoles,
            []
        );
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('initSearch$ effect', () => {
        it('should dispatch the restartSearch action when dataset or cone search changed', () => {
            mockSearchMultipleSelectorSelectPristine = store.overrideSelector(
                searchMultipleSelector.selectPristine,
                false
            );
            mockConeSearchSelectorSelectConeSearchByRoute = store.overrideSelector(
                coneSearchSelector.selectConeSearchByRoute,
                ''
            );

            const action = searchMultipleActions.initSearch();
            actions = hot('-a', { a: action });
            const expected = cold('-(bc)', {
                b: coneSearchActions.deleteConeSearch(),
                c: searchMultipleActions.restartSearch()
            });

            expect(effects.initSearch$).toBeObservable(expected);
        });

        it('should not dispatch action when default form parameters already loaded or no dataset selected', () => {
            mockSearchMultipleSelectorSelectPristine = store.overrideSelector(
                searchMultipleSelector.selectPristine,false
            );
            mockConeSearchSelectorSelectConeSearchByRoute = store.overrideSelector(
                coneSearchSelector.selectConeSearchByRoute,'1:2:3'
            );

            const action = searchMultipleActions.initSearch();
            actions = hot('-a', { a: action });
            const expected = cold('-b', { b: { type: '[No Action] Load Default Form Parameters' } });

            expect(effects.initSearch$).toBeObservable(expected);
        });

        it('should dispatch a bunch of actions when page is reloaded with cone search in it', () => {
            mockSearchMultipleSelectorSelectPristine = store.overrideSelector(
                searchMultipleSelector.selectPristine,true
            );
            mockConeSearchSelectorSelectConeSearchByRoute = store.overrideSelector(
                coneSearchSelector.selectConeSearchByRoute,'1:2:3'
            );
            mockSearchMultipleSelectorSelectSelectedDatasetsByRoute = store.overrideSelector(
                searchMultipleSelector.selectSelectedDatasetsByRoute,''
            );
            mockDesignConfigSelectorSelectDesignConfig = store.overrideSelector(
                designConfigSelector.selectDesignConfig,
                { ...DESIGN_CONFIG }
            );

            const coneSearch: ConeSearch = { ra: 1, dec: 2, radius: 3 };

            const action = searchMultipleActions.initSearch();
            actions = hot('-a', { a: action });
            const expected = cold('-(bc)', {
                b: searchMultipleActions.markAsDirty(),
                c: coneSearchActions.addConeSearch({ coneSearch })
            });

            expect(effects.initSearch$).toBeObservable(expected);
        });

        it('should dispatch a bunch of actions when page is reloaded with selected datasets in it', () => {
            mockSearchMultipleSelectorSelectPristine = store.overrideSelector(
                searchMultipleSelector.selectPristine,true
            );
            mockConeSearchSelectorSelectConeSearchByRoute = store.overrideSelector(
                coneSearchSelector.selectConeSearchByRoute,''
            );
            mockSearchMultipleSelectorSelectSelectedDatasetsByRoute = store.overrideSelector(
                searchMultipleSelector.selectSelectedDatasetsByRoute,'d1;d2'
            );

            const selectedDatasets: string[] = ['d1', 'd2'];

            const action = searchMultipleActions.initSearch();
            actions = hot('-a', { a: action });
            const expected = cold('-(bcd)', {
                b: searchMultipleActions.markAsDirty(),
                c: searchMultipleActions.updateSelectedDatasets({ selectedDatasets }),
                d: searchMultipleActions.checkDatasets()
            });

            expect(effects.initSearch$).toBeObservable(expected);
        });

        it('should dispatch a bunch of actions when page is reloaded with default selected datasets', () => {
            mockSearchMultipleSelectorSelectPristine = store.overrideSelector(
                searchMultipleSelector.selectPristine,true
            );
            mockConeSearchSelectorSelectConeSearchByRoute = store.overrideSelector(
                coneSearchSelector.selectConeSearchByRoute,''
            );
            mockSearchMultipleSelectorSelectSelectedDatasetsByRoute = store.overrideSelector(
                searchMultipleSelector.selectSelectedDatasetsByRoute,''
            );
            mockInstanceSelectorSelectorSelectInstanceByRouteName = store.overrideSelector(
                instanceSelector.selectInstanceByRouteName, { ...INSTANCE }
            );
            mockDatasetSelectorSelectAllConeSearchDatasets = store.overrideSelector(
                datasetSelector.selectAllConeSearchDatasets, [ { ...DATASET } ]
            );
            mockDesignConfigSelectorSelectDesignConfig = store.overrideSelector(
                designConfigSelector.selectDesignConfig,
                { ...DESIGN_CONFIG, search_multiple_all_datasets_selected: true }
            );

            const selectedDatasets: string[] = ['my-dataset'];

            const action = searchMultipleActions.initSearch();
            actions = hot('-a', { a: action });
            const expected = cold('-(bc)', {
                b: searchMultipleActions.markAsDirty(),
                c: searchMultipleActions.updateSelectedDatasets({ selectedDatasets })
            });

            expect(effects.initSearch$).toBeObservable(expected);
        });
    });

    describe('restartSearch$ effect', () => {
        it('should dispatch the initSearch action', () => {
            const action = searchMultipleActions.restartSearch();
            const outcome = searchMultipleActions.initSearch();

            actions = hot('-a', { a: action });
            const expected = cold('-b', { b: outcome });

            expect(effects.restartSearch$).toBeObservable(expected);
        });
    });

    describe('retrieveDataLength$ effect', () => {
        it('should dispatch the retrieveDataLengthSuccess action on success', () => {
            mockInstanceSelectorSelectorSelectInstanceNameByRoute = store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute,
                'default'
            );
            mockSearchMultipleSelectorSelectSelectedDatasets = store.overrideSelector(
                searchMultipleSelector.selectSelectedDatasets, ['my-dataset']
            );
            mockConeSearchSelectorSelectConeSearch = store.overrideSelector(
                coneSearchSelector.selectConeSearch, { ra: 1, dec: 2, radius: 3 }
            );

            const dataLength: SearchMultipleDatasetLength[] = [{ datasetName: 'my-dataset', length: 1 }];
            const action = searchMultipleActions.retrieveDataLength();
            const outcome = searchMultipleActions.retrieveDataLengthSuccess({ dataLength });

            actions = hot('-a', { a: action });
            const response = cold('-b|', { b: [{ nb: 1 }] });
            const expected = cold('---c', { c: outcome });
            searchService.retrieveDataLength = jest.fn(() => response);

            expect(effects.retrieveDataLength$).toBeObservable(expected);
        });

        it('should dispatch the retrieveDataLengthFail action on failure', () => {
            mockInstanceSelectorSelectorSelectInstanceNameByRoute = store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute,
                'default'
            );
            mockSearchMultipleSelectorSelectSelectedDatasets = store.overrideSelector(
                searchMultipleSelector.selectSelectedDatasets, ['myDataset']
            );
            mockConeSearchSelectorSelectConeSearch = store.overrideSelector(
                coneSearchSelector.selectConeSearch, { ra: 1, dec: 2, radius: 3 }
            );

            const action = searchMultipleActions.retrieveDataLength();
            const error = new Error();
            const outcome = searchMultipleActions.retrieveDataLengthFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            searchService.retrieveDataLength = jest.fn(() => response);

            expect(effects.retrieveDataLength$).toBeObservable(expected);
        });

        it('should pass correct query to the service', () => {
            mockInstanceSelectorSelectorSelectInstanceNameByRoute = store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute,
                'default'
            );
            mockSearchMultipleSelectorSelectSelectedDatasets = store.overrideSelector(
                searchMultipleSelector.selectSelectedDatasets, ['my-dataset', 'my-other-dataset']
            );
            mockConeSearchSelectorSelectConeSearch = store.overrideSelector(
                coneSearchSelector.selectConeSearch, { ra: 1, dec: 2, radius: 3 }
            );

            const dataLength: SearchMultipleDatasetLength[] = [
                { datasetName: 'my-dataset', length: 1 },
                { datasetName: 'my-other-dataset', length: 1 }
            ];

            jest.spyOn(searchService, 'retrieveDataLength');

            const action = searchMultipleActions.retrieveDataLength();
            const outcome = searchMultipleActions.retrieveDataLengthSuccess({ dataLength });

            actions = hot('-a', { a: action });
            const response = cold('-b|', { b: [{ nb: 1 }] });
            const expected = cold('---c', { c: outcome });
            searchService.retrieveDataLength = jest.fn(() => response);

            expect(effects.retrieveDataLength$).toBeObservable(expected);
            expect(searchService.retrieveDataLength).toHaveBeenCalledTimes(2);
            expect(searchService.retrieveDataLength).toHaveBeenCalledWith('default', 'my-dataset', 'a=count&cs=1:2:3');
            expect(searchService.retrieveDataLength).toHaveBeenCalledWith('default', 'my-other-dataset', 'a=count&cs=1:2:3');
        });
    });

    describe('retrieveDataLengthFail$ effect', () => {
        it('should display a error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = searchMultipleActions.retrieveDataLengthFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.retrieveDataLengthFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith('Loading Failed', 'The search multiple data length loading failed');
        });
    });
});
