/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable, of, Subject, throwError } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';

import { ArchiveEffects } from './archive.effects';
import { ArchiveService } from '../services/archive.service';
import { AppConfigService } from 'src/app/app-config.service';
import * as archiveActions from '../actions/archive.actions';
import * as searchActions from '../actions/search.actions';
import { DATASET, INSTANCE, ARCHIVE } from 'src/test-data';

jest.mock('src/app/shared/utils', () => ({
    getHost: jest.fn(() => 'host'),
}));

describe('[Instance][Store] ArchiveEffects', () => {
    let actions = new Observable();
    let effects: ArchiveEffects;
    let metadata: EffectsMetadata<ArchiveEffects>;
    let archiveService: ArchiveService;
    let toastr: ToastrService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ArchiveEffects,
                {
                    provide: ArchiveService,
                    useValue: {},
                },
                { provide: ToastrService, useValue: { error: jest.fn() } },
                { provide: AppConfigService, useValue: { apiUrl: 'test' } },
                provideMockActions(() => actions),
            ],
        });

        effects = TestBed.inject(ArchiveEffects);
        metadata = getEffectsMetadata(effects);
        archiveService = TestBed.inject(ArchiveService);
        toastr = TestBed.inject(ToastrService);
    });

    it('effect should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('startTaskCreateArchive$ effect', () => {
        it('should start create archive', () => {
            let archive = { ...ARCHIVE };
            const action = archiveActions.startTaskCreateArchive({
                query: 'test',
            });
            archiveService.startTaskCreateArchive = jest.fn(() =>
                of(archive)
            );
            const completion = archiveActions.startTaskCreateArchiveSuccess({ archive });
            actions = cold('a', { a: action });
            const expected = cold('a', { a: completion });
            expect(effects.startTaskCreateArchive$).toBeObservable(expected);
        });

        it('should trigger startTaskCreateArchiveFail', () => {
            const action = archiveActions.startTaskCreateArchive({
                query: 'test',
            });
            const completion = archiveActions.startTaskCreateArchiveFail();
            archiveService.startTaskCreateArchive = jest
                .fn()
                .mockImplementation((error) => throwError(() => error));
            actions = cold('a', { a: action });
            const expected = cold('a', { a: completion });
            expect(effects.startTaskCreateArchive$).toBeObservable(expected);
        });
    });

    describe('startTaskCreateArchiveFail$ effect', () => {
        it('should trigger fail toast error', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = archiveActions.startTaskCreateArchiveFail();
            actions = hot('a', { a: action });
            expect(effects.startTaskCreateArchiveFail$).toBeObservable(
                cold('a', { a: action })
            );
            expect(spy).toHaveBeenCalledTimes(1);
        });
    });

    describe('isArchiveAvailable$ effect', () => {
        it('should trigger isArchiveAvailableSuccess action', () => {
            let archive = { ...ARCHIVE };
            // @ts-ignore
            effects.kill$ = new Subject();
            archiveService.isArchiveAvailable = jest.fn(() =>
                of({ archive_is_available: true })
            );
            const action = archiveActions.isArchiveAvailable({ archive });
            const completion = archiveActions.isArchiveAvailableSuccess({
                filename: 'archive_default_observations_2023-09-22T13:59:28.zip',
                url: 'host/archive/download-archive/12',
            });
            actions = cold('a', { a: action });
            const expected = cold('a', { a: completion });
            expect(effects.isArchiveAvailable$).toBeObservable(expected);
        });

        it('should trigger [No Action] Is Archive Available action', () => {
            let archive = { ...ARCHIVE };
            const action = archiveActions.isArchiveAvailable({ archive });
            const completion = { type: '[No Action] Is Archive Available' };
            archiveService.isArchiveAvailable = jest
                .fn()
                .mockImplementation(() => of({ archive_is_available: false }));
            actions = cold('a', { a: action });
            const expected = cold('a', { a: completion });
            expect(effects.isArchiveAvailable$).toBeObservable(expected);
        });

        it('should trigger isArchiveAvailableFail action', () => {
            let archive = { ...ARCHIVE };
            const action = archiveActions.isArchiveAvailable({ archive });
            const completion = archiveActions.isArchiveAvailableFail();
            archiveService.isArchiveAvailable = jest
                .fn()
                .mockImplementation(() => of({ archive_is_available: true }));
            actions = cold('a', { a: action });
            const expected = cold('a', { a: completion });
            expect(effects.isArchiveAvailable$).toBeObservable(expected);
        });
    });

    describe('isArchiveAvailableSuccess$ effect', () => {
        it('should trigger downloadFile action ', () => {
            const action = archiveActions.isArchiveAvailableSuccess({
                filename: 'test',
                url: 'test',
            });
            actions = hot('a', { a: action });
            const completion = searchActions.downloadFile({
                filename: 'test',
                url: 'test',
            });
            expect(effects.isArchiveAvailableSuccess$).toBeObservable(
                cold('a', { a: completion })
            );
        });
    });

    describe('isArchiveAvailableFail$ effect', () => {
        it('should trigger fail toast error', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = archiveActions.isArchiveAvailableFail();
            actions = hot('a', { a: action });
            expect(effects.isArchiveAvailableFail$).toBeObservable(
                cold('a', { a: action })
            );
            expect(spy).toHaveBeenCalledTimes(1);
        });
    });

    describe('resetArchive$ effect', () => {
        it('should call unsuscribe on kill', () => {
            //@ts-ignore
            effects.kill$ = new Subject();
            //@ts-ignore
            let spy = jest.spyOn(effects.kill$, 'unsubscribe');
            const action = archiveActions.resetArchive();
            actions = hot('a', { a: action });
            expect(effects.resetArchive$).toBeObservable(
                cold('a', { a: action })
            );
            expect(spy).toHaveBeenCalledTimes(1);
        });
    });
});
