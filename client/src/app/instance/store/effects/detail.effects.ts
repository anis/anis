/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import { DetailService } from '../services/detail.service';
import * as detailActions from '../actions/detail.actions';
import * as detailSelector from '../selectors/detail.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';

@Injectable()
export class DetailEffects {
    /**
     * Calls actions to retrieve object.
     */
    retrieveObject$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(detailActions.retrieveObject),
            concatLatestFrom(() => [
                this.store.select(instanceSelector.selectInstanceNameByRoute),
                this.store.select(datasetSelector.selectDatasetByRouteName),
                this.store.select(detailSelector.selectIdByRoute)
            ]),
            mergeMap(([, instanceName, dataset, id]) => this.detailService.retrieveObject(
                instanceName,
                dataset.name,
                dataset.primary_key,
                id
            ).pipe(
                map(object => detailActions.retrieveObjectSuccess({ object: object[0] })),
                catchError(() => of(detailActions.retrieveObjectFail()))
            ))
        )
    );

    /**
     * Displays retrieve object error notification.
     */
    retrieveObjectFail$ = createEffect(() => 
        this.actions$.pipe(
            ofType(detailActions.retrieveObjectFail),
            tap(() => this.toastr.error('Loading Failed!', 'Unable to load the object'))
        ), { dispatch: false}
    );

    /**
     * Calls actions to retrieve spectra.
     */
    retrieveSpectra$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(detailActions.retrieveSpectra),
            concatLatestFrom(() => [
                this.store.select(instanceSelector.selectInstanceNameByRoute),
                this.store.select(datasetSelector.selectDatasetNameByRoute)
            ]),
            mergeMap(([action, instanceName, datasetName]) => this.detailService.retrieveSpectra(
                instanceName,
                datasetName,
                action.filename
            ).pipe(
                map(spectraCSV => detailActions.retrieveSpectraSuccess({ spectraCSV })),
                catchError(() => of(detailActions.retrieveSpectraFail()))
            ))
        )
    );

    /**
     * Displays retrieve spectra error notification.
     */
    retrieveSpectraFail$ = createEffect(() => 
        this.actions$.pipe(
            ofType(detailActions.retrieveSpectraFail),
            tap(() => this.toastr.error('Loading Failed!', 'Unable to load spectra'))
        ), { dispatch: false}
    );
    
    constructor(
        private actions$: Actions,
        private detailService: DetailService,
        private store: Store<{ }>,
        private toastr: ToastrService
    ) {}
}
