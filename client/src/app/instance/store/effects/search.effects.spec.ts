/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';
import { KeycloakService } from 'keycloak-angular';
import { SearchEffects } from './search.effects';
import { SearchService } from '../services/search.service';

import * as searchActions from '../actions/search.actions';
import * as fromSearch from '../reducers/search.reducer';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as searchSelector from '../selectors/search.selector';
import * as attributeActions from 'src/app/metamodel/actions/attribute.actions';
import * as criteriaFamilyActions from 'src/app/metamodel/actions/criteria-family.actions';
import * as outputFamilyActions from 'src/app/metamodel/actions/output-family.actions';
import * as attributeSelector from 'src/app/metamodel/selectors/attribute.selector';
import * as coneSearchSelector from '../selectors/cone-search.selector';
import * as coneSearchActions from '../actions/cone-search.actions';
import * as coneSearchConfigSelector from 'src/app/metamodel/selectors/cone-search-config.selector';
import * as coneSearchConfigActions from 'src/app/metamodel/actions/cone-search-config.actions';
import { Criterion, PaginationOrder } from '../models';
import * as authSelector from 'src/app/user/store/selectors/auth.selector';
import { ATTRIBUTE_LIST } from 'src/test-data';

jest.mock('file-saver');

describe('[Instance][Store] SearchEffects', () => {
    let actions = new Observable();
    let effects: SearchEffects;
    let metadata: EffectsMetadata<SearchEffects>;
    let searchService: SearchService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { search: { ...fromSearch.initialState } };
    let keycloak = {
        getToken: jest.fn().mockImplementation(() => Promise.resolve('test')),
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                SearchEffects,
                {
                    provide: SearchService,
                    useValue: {
                        retrieveData: jest.fn(),
                        retrieveDataLength: jest.fn(),
                    },
                },
                { provide: ToastrService, useValue: { error: jest.fn() } },
                { provide: KeycloakService, useValue: keycloak },
                provideMockActions(() => actions),
                provideMockStore({ initialState }),
            ],
        }).compileComponents();
        effects = TestBed.inject(SearchEffects);
        metadata = getEffectsMetadata(effects);
        searchService = TestBed.inject(SearchService);
        toastr = TestBed.inject(ToastrService);
        store = TestBed.inject(MockStore);
        store.overrideSelector(instanceSelector.selectInstanceNameByRoute, '');
        store.overrideSelector(datasetSelector.selectDatasetNameByRoute, '');
        store.overrideSelector(searchSelector.selectCurrentDataset, '');
        store.overrideSelector(searchSelector.selectPristine, true);
        store.overrideSelector(attributeSelector.selectAllAttributes, []);
        store.overrideSelector(searchSelector.selectStepsByRoute, '');
        store.overrideSelector(searchSelector.selectCriteriaListByRoute, '');
        store.overrideSelector(searchSelector.selectCriteriaList, []);
        store.overrideSelector(coneSearchSelector.selectConeSearchByRoute, '');
        store.overrideSelector(coneSearchSelector.selectConeSearch, {
            ra: 1,
            dec: 2,
            radius: 3,
        });
        store.overrideSelector(searchSelector.selectOutputListByRoute, '');
        store.overrideSelector(searchSelector.selectOutputList, []);
        store.overrideSelector(
            coneSearchConfigSelector.selectConeSearchConfig,
            null,
        );
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('initSearch$ effect', () => {
        it('should dispatch the restartSearch action when dataset changed', () => {
            store.overrideSelector(
                datasetSelector.selectDatasetNameByRoute,
                'myNewDataset',
            );
            store.overrideSelector(
                searchSelector.selectCurrentDataset,
                'myOldDataset',
            );

            const action = searchActions.initSearch();
            const outcome = searchActions.restartSearch();

            actions = hot('-a', { a: action });
            const expected = cold('-b', { b: outcome });

            expect(effects.initSearch$).toBeObservable(expected);
        });

        it('should dispatch a bunch of actions when a dataset is selected or a page is reloaded', () => {
            store.overrideSelector(
                datasetSelector.selectDatasetNameByRoute,
                'myDatasetName',
            );
            store.overrideSelector(searchSelector.selectPristine, true);
            store.overrideSelector(
                searchSelector.selectCurrentDataset,
                'myDatasetName',
            );

            const action = searchActions.initSearch();
            actions = hot('-a', { a: action });
            const expected = cold('-(bcdefg)', {
                b: searchActions.changeCurrentDataset({
                    currentDataset: 'myDatasetName',
                }),
                c: attributeActions.loadAttributeList(),
                d: criteriaFamilyActions.loadCriteriaFamilyList(),
                e: outputFamilyActions.loadOutputFamilyList(),
                f: coneSearchConfigActions.loadConeSearchConfig(),
                g: searchActions.tryLoadDefaultFormParameters(),
            });

            expect(effects.initSearch$).toBeObservable(expected);
        });

        it('should dispatch a bunch of actions when a dataset is selected or a page is reloaded with steps checked', () => {
            store.overrideSelector(
                datasetSelector.selectDatasetNameByRoute,
                'myDatasetName',
            );
            store.overrideSelector(searchSelector.selectPristine, true);
            store.overrideSelector(
                searchSelector.selectCurrentDataset,
                'myDatasetName',
            );
            store.overrideSelector(searchSelector.selectStepsByRoute, '111');

            const action = searchActions.initSearch();
            actions = hot('-a', { a: action });
            const expected = cold('-(bcdefghij)', {
                b: searchActions.changeCurrentDataset({
                    currentDataset: 'myDatasetName',
                }),
                c: attributeActions.loadAttributeList(),
                d: criteriaFamilyActions.loadCriteriaFamilyList(),
                e: outputFamilyActions.loadOutputFamilyList(),
                f: coneSearchConfigActions.loadConeSearchConfig(),
                g: searchActions.tryLoadDefaultFormParameters(),
                h: searchActions.checkCriteria(),
                i: searchActions.checkOutput(),
                j: searchActions.checkResult(),
            });

            expect(effects.initSearch$).toBeObservable(expected);
        });

        it('should dispatch a resetSearch action when user get back to search module', () => {
            store.overrideSelector(
                datasetSelector.selectDatasetNameByRoute,
                '',
            );
            store.overrideSelector(searchSelector.selectPristine, false);

            const action = searchActions.initSearch();
            const outcome = searchActions.resetSearch();

            actions = hot('-a', { a: action });
            const expected = cold('-b', { b: outcome });

            expect(effects.initSearch$).toBeObservable(expected);
        });

        it('should not dispatch action when step changed on same search', () => {
            store.overrideSelector(
                datasetSelector.selectDatasetNameByRoute,
                '',
            );

            const action = searchActions.initSearch();
            const outcome = { type: '[No Action] Init Search' };

            actions = hot('-a', { a: action });
            const expected = cold('-b', { b: outcome });

            expect(effects.initSearch$).toBeObservable(expected);
        });
    });

    describe('restartSearch$ effect', () => {
        it('should dispatch the initSearch action', () => {
            const action = searchActions.restartSearch();

            actions = hot('-a', { a: action });
            const expected = cold('-(bcd)', {
                b: attributeActions.resetAttributeList(),
                c: coneSearchConfigActions.resetConeSearchConfig(),
                d: searchActions.initSearch(),
            });

            expect(effects.restartSearch$).toBeObservable(expected);
        });
    });
    describe('resetSearch$ effect', () => {
        it('should dispatch resetAttributeList and resetConeSearchConfig actions', () => {
            let action = searchActions.resetSearch();
            actions = hot('a', { a: action });
            const expected = cold('(bc)', {
                b: attributeActions.resetAttributeList(),
                c: coneSearchConfigActions.resetConeSearchConfig(),
            });

            expect(effects.resetSearch$).toBeObservable(expected);
        });
    });
    describe('tryLoadDefaultFormParameters$ effect', () => {
        it('should dispatch loadDefaultFormParameters action', () => {
            store.overrideSelector(
                attributeSelector.selectAttributeListIsLoaded,
                true,
            );
            store.overrideSelector(
                coneSearchConfigSelector.selectConeSearchConfigIsLoaded,
                true,
            );
            let action = searchActions.tryLoadDefaultFormParameters();
            actions = hot('a', { a: action });
            const expected = cold('a', {
                a: searchActions.loadDefaultFormParameters(),
            });
            expect(effects.tryLoadDefaultFormParameters$).toBeObservable(
                expected,
            );
        });
    });
    describe('loadDefaultFormParameters$ effect', () => {
        it('should not dispatch action if params already loaded', () => {
            store.overrideSelector(searchSelector.selectPristine, false);
            store.overrideSelector(
                searchSelector.selectCurrentDataset,
                'myDataset',
            );

            const action = searchActions.loadDefaultFormParameters();
            const outcome = {
                type: '[No Action] Load Default Form Parameters',
            };

            actions = hot('-a', { a: action });
            const expected = cold('-b', { b: outcome });

            expect(effects.loadDefaultFormParameters$).toBeObservable(expected);
        });

        it('should not dispatch action if no dataset selected', () => {
            const action = searchActions.loadDefaultFormParameters();
            const outcome = {
                type: '[No Action] Load Default Form Parameters',
            };

            actions = hot('-a', { a: action });
            const expected = cold('-b', { b: outcome });

            expect(effects.loadDefaultFormParameters$).toBeObservable(expected);
        });

        it('should dispatch a bunch of actions to update search', () => {
            store.overrideSelector(searchSelector.selectPristine, true);
            store.overrideSelector(
                searchSelector.selectCurrentDataset,
                'myDataset',
            );

            const action = searchActions.loadDefaultFormParameters();
            actions = hot('-a', { a: action });

            const defaultCriteriaList = [];
            const defaultConeSearch = null;
            const defaultOutputList = [];
            const expected = cold('-(bcde)', {
                b: searchActions.updateCriteriaList({
                    criteriaList: defaultCriteriaList,
                }),
                c: coneSearchActions.addConeSearch({
                    coneSearch: defaultConeSearch,
                }),
                d: searchActions.updateOutputList({
                    outputList: defaultOutputList,
                }),
                e: searchActions.markAsDirty(),
            });

            expect(effects.loadDefaultFormParameters$).toBeObservable(expected);
        });

        it('should set a default criteria list', () => {
            store.overrideSelector(searchSelector.selectPristine, true);
            store.overrideSelector(
                searchSelector.selectCurrentDataset,
                'my-dataset',
            );
            store.overrideSelector(attributeSelector.selectAllAttributes, [
                {
                    ...ATTRIBUTE_LIST[0],
                    search_type: 'field',
                    operator: 'eq',
                    min: 'one',
                    id_criteria_family: 1,
                    selected: false,
                },
                {
                    ...ATTRIBUTE_LIST[1],
                    search_type: 'field',
                    operator: 'eq',
                    min: 'two',
                    id_criteria_family: 1,
                    selected: false,
                },
            ]);

            const action = searchActions.loadDefaultFormParameters();
            actions = hot('-a', { a: action });

            const defaultCriteriaList = [
                { id: 1, type: 'field', operator: 'eq', value: 'one' },
                { id: 2, type: 'field', operator: 'eq', value: 'two' },
            ];
            const defaultConeSearch = null;
            const defaultOutputList = [];
            const expected = cold('-(bcde)', {
                b: searchActions.updateCriteriaList({
                    criteriaList: defaultCriteriaList,
                }),
                c: coneSearchActions.addConeSearch({
                    coneSearch: defaultConeSearch,
                }),
                d: searchActions.updateOutputList({
                    outputList: defaultOutputList,
                }),
                e: searchActions.markAsDirty(),
            });

            expect(effects.loadDefaultFormParameters$).toBeObservable(expected);
        });

        it('should set criteria list from URL', () => {
            store.overrideSelector(searchSelector.selectPristine, true);
            store.overrideSelector(
                searchSelector.selectCurrentDataset,
                'myDataset',
            );
            store.overrideSelector(attributeSelector.selectAllAttributes, [
                ...ATTRIBUTE_LIST,
            ]);
            store.overrideSelector(
                searchSelector.selectCriteriaListByRoute,
                '1::eq::un;2::eq::deux',
            );
            store.overrideSelector(
                searchSelector.selectOutputListByRoute,
                '1;2;3',
            );

            const action = searchActions.loadDefaultFormParameters();
            actions = hot('-a', { a: action });

            const criteriaList = [
                { id: 1, type: 'field', operator: 'eq', value: 'un' },
                { id: 2, type: 'field', operator: 'eq', value: 'deux' },
            ];
            const defaultConeSearch = null;
            const defaultOutputList = [1, 2, 3];
            const expected = cold('-(bcde)', {
                b: searchActions.updateCriteriaList({
                    criteriaList: criteriaList,
                }),
                c: coneSearchActions.addConeSearch({
                    coneSearch: defaultConeSearch,
                }),
                d: searchActions.updateOutputList({
                    outputList: defaultOutputList,
                }),
                e: searchActions.markAsDirty(),
            });

            expect(effects.loadDefaultFormParameters$).toBeObservable(expected);
        });

        it('should set cone search from URL', () => {
            store.overrideSelector(searchSelector.selectPristine, true);
            store.overrideSelector(
                searchSelector.selectCurrentDataset,
                'myDataset',
            );
            store.overrideSelector(
                coneSearchSelector.selectConeSearchByRoute,
                '1:2:3',
            );

            const action = searchActions.loadDefaultFormParameters();
            actions = hot('-a', { a: action });

            const defaultCriteriaList = [];
            const coneSearch = { ra: 1, dec: 2, radius: 3 };
            const defaultOutputList = [];
            const expected = cold('-(bcde)', {
                b: searchActions.updateCriteriaList({
                    criteriaList: defaultCriteriaList,
                }),
                c: coneSearchActions.addConeSearch({ coneSearch: coneSearch }),
                d: searchActions.updateOutputList({
                    outputList: defaultOutputList,
                }),
                e: searchActions.markAsDirty(),
            });

            expect(effects.loadDefaultFormParameters$).toBeObservable(expected);
        });

        it('should set a default output list', () => {
            store.overrideSelector(searchSelector.selectPristine, true);
            store.overrideSelector(
                searchSelector.selectCurrentDataset,
                'myDataset',
            );
            store.overrideSelector(attributeSelector.selectAllAttributes, [
                {
                    ...ATTRIBUTE_LIST[0],
                    id_output_category: '1_1',
                    selected: true,
                },
                {
                    ...ATTRIBUTE_LIST[1],
                    id_output_category: '1_1',
                    selected: true,
                },
            ]);

            const action = searchActions.loadDefaultFormParameters();
            actions = hot('-a', { a: action });

            const defaultCriteriaList = [];
            const defaultConeSearch = null;
            const defaultOutputList = [1, 2];
            const expected = cold('-(bcde)', {
                b: searchActions.updateCriteriaList({
                    criteriaList: defaultCriteriaList,
                }),
                c: coneSearchActions.addConeSearch({
                    coneSearch: defaultConeSearch,
                }),
                d: searchActions.updateOutputList({
                    outputList: defaultOutputList,
                }),
                e: searchActions.markAsDirty(),
            });

            expect(effects.loadDefaultFormParameters$).toBeObservable(expected);
        });

        it('should set output list from URL', () => {
            store.overrideSelector(searchSelector.selectPristine, true);
            store.overrideSelector(
                searchSelector.selectCurrentDataset,
                'myDataset',
            );
            store.overrideSelector(
                searchSelector.selectOutputListByRoute,
                '1;2;3',
            );

            const action = searchActions.loadDefaultFormParameters();
            actions = hot('-a', { a: action });

            const defaultCriteriaList = [];
            const defaultConeSearch = null;
            const outputList = [1, 2, 3];
            const expected = cold('-(bcde)', {
                b: searchActions.updateCriteriaList({
                    criteriaList: defaultCriteriaList,
                }),
                c: coneSearchActions.addConeSearch({
                    coneSearch: defaultConeSearch,
                }),
                d: searchActions.updateOutputList({ outputList: outputList }),
                e: searchActions.markAsDirty(),
            });

            expect(effects.loadDefaultFormParameters$).toBeObservable(expected);
        });
    });

    describe('retrieveDataLength$ effect', () => {
        it('should dispatch the retrieveDataLengthSuccess action on success', () => {
            const action = searchActions.retrieveDataLength();
            const outcome = searchActions.retrieveDataLengthSuccess({
                length: 5,
            });

            actions = hot('-a', { a: action });
            const response = cold('-b|', { b: [{ nb: 5 }] });
            const expected = cold('--c', { c: outcome });
            searchService.retrieveDataLength = jest.fn(() => response);

            expect(effects.retrieveDataLength$).toBeObservable(expected);
        });

        it('should dispatch the retrieveDataLengthFail action on failure', () => {
            const action = searchActions.retrieveDataLength();
            const error = new Error();
            const outcome = searchActions.retrieveDataLengthFail();
            const spyConsole = jest
                .spyOn(console, 'error')
                .mockImplementation(jest.fn());

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            searchService.retrieveDataLength = jest.fn(() => response);

            expect(effects.retrieveDataLength$).toBeObservable(expected);
            expect(spyConsole).toHaveBeenCalled();
        });

        it('should pass correct query to the service', () => {
            store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute,
                'default',
            );
            store.overrideSelector(
                datasetSelector.selectDatasetNameByRoute,
                'my-dataset',
            );
            store.overrideSelector(searchSelector.selectCriteriaList, [
                {
                    id: 1,
                    type: 'field',
                    operator: 'eq',
                    value: 'one',
                } as Criterion,
            ]);
            store.overrideSelector(coneSearchSelector.selectConeSearch, {
                ra: 1,
                dec: 2,
                radius: 3,
            });

            jest.spyOn(searchService, 'retrieveDataLength');

            const action = searchActions.retrieveDataLength();
            const outcome = searchActions.retrieveDataLengthSuccess({
                length: 5,
            });

            actions = hot('-a', { a: action });
            const response = cold('-b|', { b: [{ nb: 5 }] });
            const expected = cold('--c', { c: outcome });
            searchService.retrieveDataLength = jest.fn(() => response);

            expect(effects.retrieveDataLength$).toBeObservable(expected);
            expect(searchService.retrieveDataLength).toHaveBeenCalledTimes(1);
            expect(searchService.retrieveDataLength).toHaveBeenCalledWith(
                'default',
                'my-dataset',
                'a=count&c=1::eq::one&cs=1:2:3',
            );
        });
    });

    describe('retrieveDataLengthFail$ effect', () => {
        it('should display a error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = searchActions.retrieveDataLengthFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.retrieveDataLengthFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Loading Failed',
                'The search data length loading failed',
            );
        });
    });

    describe('retrieveData$ effect', () => {
        it('should dispatch the retrieveDataSuccess action on success', () => {
            const action = searchActions.retrieveData({
                pagination: {
                    dname: 'myDatasetName',
                    page: 1,
                    nbItems: 10,
                    sortedCol: 1,
                    order: PaginationOrder.a,
                },
            });
            const outcome = searchActions.retrieveDataSuccess({
                data: ['data'],
            });

            actions = hot('-a', { a: action });
            const response = cold('-b|', { b: ['data'] });
            const expected = cold('--c', { c: outcome });
            searchService.retrieveData = jest.fn(() => response);

            expect(effects.retrieveData$).toBeObservable(expected);
        });

        it('should dispatch the retrieveDataFail action on failure', () => {
            const action = searchActions.retrieveData({
                pagination: {
                    dname: 'myDatasetName',
                    page: 1,
                    nbItems: 10,
                    sortedCol: 1,
                    order: PaginationOrder.a,
                },
            });
            const error = new Error();
            const outcome = searchActions.retrieveDataFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', {}, error);
            const expected = cold('--b', { b: outcome });
            searchService.retrieveData = jest.fn(() => response);

            expect(effects.retrieveData$).toBeObservable(expected);
        });

        it('should pass correct query to the service', () => {
            store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute,
                'default',
            );
            store.overrideSelector(
                datasetSelector.selectDatasetNameByRoute,
                'my-dataset',
            );
            store.overrideSelector(searchSelector.selectCriteriaList, [
                {
                    id: 1,
                    type: 'field',
                    operator: 'eq',
                    value: 'one',
                } as Criterion,
            ]);
            store.overrideSelector(coneSearchSelector.selectConeSearch, {
                ra: 1,
                dec: 2,
                radius: 3,
            });
            store.overrideSelector(searchSelector.selectOutputList, [1, 2]);

            jest.spyOn(searchService, 'retrieveData');

            const action = searchActions.retrieveData({
                pagination: {
                    dname: 'myDatasetName',
                    page: 1,
                    nbItems: 10,
                    sortedCol: 1,
                    order: PaginationOrder.a,
                },
            });
            const outcome = searchActions.retrieveDataSuccess({
                data: ['data'],
            });

            actions = hot('-a', { a: action });
            const response = cold('-b|', { b: ['data'] });
            const expected = cold('--c', { c: outcome });
            searchService.retrieveData = jest.fn(() => response);

            expect(effects.retrieveData$).toBeObservable(expected);
            expect(searchService.retrieveData).toHaveBeenCalledTimes(1);
            expect(searchService.retrieveData).toHaveBeenCalledWith(
                'default',
                'my-dataset',
                'a=1;2&c=1::eq::one&cs=1:2:3&p=10:1&o=1:a',
            );
        });
    });

    describe('retrieveDataFail$ effect', () => {
        it('should display a error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = searchActions.retrieveDataFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.retrieveDataFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Loading Failed',
                'The search data loading failed',
            );
        });
    });

    describe('downloadFile$ effect', () => {
        it('should call getToken() on keycload if user is authenticated', () => {
            store.overrideSelector(authSelector.selectIsAuthenticated, true);
            let spy = jest.spyOn(keycloak, 'getToken');
            const action = searchActions.downloadFile({
                filename: 'test',
                url: 'test?test',
            });
            actions = hot('(a)', { a: action });
            const expected = cold('a', { a: [action, true] });
            expect(effects.downloadFile$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
        });
    });
});
