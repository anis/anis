/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of, timer, mapTo, takeUntil, Subject } from 'rxjs';
import { map, tap, mergeMap, switchMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import { ArchiveService } from '../services/archive.service';
import * as archiveActions from '../actions/archive.actions';
import * as searchActions from '../actions/search.actions';
import { AppConfigService } from 'src/app/app-config.service';
import { getHost } from 'src/app/shared/utils';

@Injectable()
export class ArchiveEffects {
    startTaskCreateArchive$ = createEffect(() =>
        this.actions$.pipe(
            ofType(archiveActions.startTaskCreateArchive),
            mergeMap(action => this.archiveService
                .startTaskCreateArchive(action.query)
                .pipe(
                    map((archive) =>
                        archiveActions.startTaskCreateArchiveSuccess({ archive })
                    ),
                    catchError(() =>
                        of(archiveActions.startTaskCreateArchiveFail())
                    )
                )
            )
        )
    );

    startTaskCreateArchiveSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(archiveActions.startTaskCreateArchiveSuccess),
            tap((action) => (this.kill$ = new Subject())),
            switchMap((action) =>
                timer(0, 1000).pipe(
                    mapTo(archiveActions.isArchiveAvailable(action)),
                    takeUntil(this.kill$)
                )
            )
        )
    );

    startTaskCreateArchiveFail$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(archiveActions.startTaskCreateArchiveFail),
                tap(() =>
                    this.toastr.error(
                        'The creation of the archive file failed',
                        'Start async task failed'
                    )
                )
            ),
        { dispatch: false }
    );

    isArchiveAvailable$ = createEffect(() =>
        this.actions$.pipe(
            ofType(archiveActions.isArchiveAvailable),
            switchMap((action) =>
                this.archiveService
                    .isArchiveAvailable(
                        action.archive.id
                    )
                    .pipe(
                        map((result) => {
                            if (result.archive_is_available) {
                                this.kill$.next({});
                                this.kill$.unsubscribe();

                                return archiveActions.isArchiveAvailableSuccess(
                                    {
                                        url: `${getHost(
                                            this.config.apiUrl
                                        )}/archive/download-archive/${
                                            action.archive.id
                                        }`,
                                        filename: action.archive.name,
                                    }
                                );
                            } else {
                                return {
                                    type: '[No Action] Is Archive Available',
                                };
                            }
                        }),
                        catchError(() =>
                            of(archiveActions.isArchiveAvailableFail())
                        )
                    )
            )
        )
    );

    isArchiveAvailableSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(archiveActions.isArchiveAvailableSuccess),
            map((action) => searchActions.downloadFile(action))
        )
    );

    isArchiveAvailableFail$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(archiveActions.isArchiveAvailableFail),
                tap(() =>
                    this.toastr.error(
                        'The creation of the archive has encountered a problem',
                        'Archive result download failed'
                    )
                )
            ),
        { dispatch: false }
    );

    resetArchive$ = createEffect(
        () =>
            this.actions$.pipe(
                ofType(archiveActions.resetArchive),
                tap(() => {
                    if (this.kill$) {
                        this.kill$.unsubscribe();
                    }
                })
            ),
        { dispatch: false }
    );

    private kill$: Subject<{}>;

    constructor(
        private actions$: Actions,
        private archiveService: ArchiveService,
        private toastr: ToastrService,
        private config: AppConfigService
    ) {}
}
