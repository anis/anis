/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';

export const retrieveObject = createAction('[Detail] Retrieve Object');
export const retrieveObjectSuccess = createAction('[Detail] Retrieve Object Success', props<{ object: any }>());
export const retrieveObjectFail = createAction('[Detail] Retrieve Object Fail');
export const retrieveSpectra = createAction('[Detail] Retrieve Spectra', props<{ filename: string }>());
export const retrieveSpectraSuccess = createAction('[Detail] Retrieve Spectra Success', props<{ spectraCSV: string }>());
export const retrieveSpectraFail = createAction('[Detail] Retrieve Spectra Fail');
export const destroyDetail = createAction('[Detail] Destroy Detail');
