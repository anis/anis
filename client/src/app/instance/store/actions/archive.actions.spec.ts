/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as archiveActions from './archive.actions';

describe('[Samp] Samp actions', () => {
    it('should create register action', () => {
        const action = archiveActions.startTaskCreateArchive({ query: 'a=allc=1::eq::100'});
        expect(action).toEqual({ type: '[Archive] Start Task Create Archive', query: 'a=allc=1::eq::100' });
    });
});
