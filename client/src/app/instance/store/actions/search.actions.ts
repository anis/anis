/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';

import { Criterion, Pagination } from '../models';

export const initSearch = createAction('[Search] Init Search');
export const restartSearch = createAction('[Search] Restart Search');
export const resetSearch = createAction('[Search] Reset Search');
export const destroyResults = createAction('[Search] Destroy Results');
export const tryLoadDefaultFormParameters = createAction('[Search] Try Load Default Form Parameters');
export const loadDefaultFormParameters = createAction('[Search] Load Default Form Parameters');
export const markAsDirty = createAction('[Search] Mark As Dirty');
export const changeCurrentDataset = createAction('[Search] Change Current Dataset', props<{ currentDataset: string }>());
export const changeStep = createAction('[Search] Change Step', props<{ step: string }>());
export const checkCriteria = createAction('[Search] Check Criteria');
export const checkOutput = createAction('[Search] Check Output');
export const checkResult = createAction('[Search] Check Result');
export const updateCriteriaList = createAction('[Search] Update Criteria List', props<{ criteriaList: Criterion[] }>());
export const addCriterion = createAction('[Search] Add Criterion', props<{ criterion: Criterion }>());
export const updateCriterion = createAction('[Search] Update Criterion', props<{ updatedCriterion: Criterion }>());
export const deleteCriterion = createAction('[Search] Delete Criterion', props<{ idCriterion: number }>());
export const updateOutputList = createAction('[Search] Update Output List', props<{ outputList: number[] }>());
export const retrieveDataLength = createAction('[Search] Retrieve Data Length');
export const retrieveDataLengthSuccess = createAction('[Search] Retrieve Data Length Success', props<{ length: number }>());
export const retrieveDataLengthFail = createAction('[Search] Retrieve Data Length Fail');
export const retrieveData = createAction('[Search] Retrieve Data', props<{ pagination: Pagination }>());
export const retrieveDataSuccess = createAction('[Search] Retrieve Data Success', props<{ data: any[] }>());
export const retrieveDataFail = createAction('[Search] Retrieve Data Fail');
export const addSelectedData = createAction('[Search] Add Selected Data', props<{ id: number | string }>());
export const deleteSelectedData = createAction('[Search] Delete Selected Data', props<{ id: number | string }>());
export const deleteAllSelectedData = createAction('[Search] Delete All Selected Data');
export const downloadFile = createAction('[Search] Download File', props<{ url: string, filename: string }>());
