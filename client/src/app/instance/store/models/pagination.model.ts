/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export interface Pagination {
    dname: string;
    page: number;
    nbItems: number;
    sortedCol: number;
    order: string
}

/**
 * Enum for PaginationOrder values.
 * @readonly
 * @enum {string}
 */
export enum PaginationOrder {
    a = 'a',
    d = 'd'
}
