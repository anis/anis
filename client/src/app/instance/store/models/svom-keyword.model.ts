/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export interface SvomKeyword {
    name: string;
    default: string;
    data_type: string;
    extension: string;
    sdb_search: boolean;
    description: string;
    expected_values: {
        mode: string;
        values: string[] | number[];
    };
}
