/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as fromArchive from './archive.reducer';
import * as archiveActions from '../actions/archive.actions';
import { Action } from '@ngrx/store';
import { ARCHIVE } from 'src/test-data';

describe('[Instance][Store] Archive reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromArchive;
        const action = { type: 'Unknown' };
        const state = fromArchive.archiveReducer(initialState, action);

        expect(state).toBe(initialState);
    });

    it('startTaskCreateArchiveSuccess action should set archiveIsCreating  to true', () => {
        const initialState = {
            ...fromArchive.initialState,
        };
        const action = archiveActions.startTaskCreateArchiveSuccess({archive: { ...ARCHIVE }});
        const state = fromArchive.archiveReducer(initialState, action);
        expect(state).not.toEqual(initialState);
    });
    it('isArchiveAvailableSuccess action should set archiveIsCreating to false', () => {
        const initialState = {
            ...fromArchive.initialState,
            archiveIsCreating: true
        };
        expect(initialState.archiveIsCreating).toBe(true);
        const action = archiveActions.isArchiveAvailableSuccess({ filename: 'test', url: 'test' });
        const state = fromArchive.archiveReducer(initialState, action);
        expect(state.archiveIsCreating).toBe(false);
        expect(state).not.toEqual(initialState);
    });
    it('resetArchive action should reset archive', () => {
        const action = archiveActions.resetArchive();
        const state = fromArchive.archiveReducer(undefined, action);
        expect(state).toEqual(fromArchive.initialState);
    });
    it('should get archiveIsCreating', () => {
        const action = {} as Action;
        const state = fromArchive.archiveReducer(undefined, action);

        expect(fromArchive.selectArchiveIsCreating(state)).toBeFalsy();
    });
});
