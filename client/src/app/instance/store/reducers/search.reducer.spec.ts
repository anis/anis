/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromSearch from './search.reducer';
import * as searchActions from '../actions/search.actions';
import { Criterion, Pagination, PaginationOrder } from '../models';

describe('[Instance][Store] Search reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromSearch;
        const action = { type: 'Unknown' };
        const state = fromSearch.searchReducer(initialState, action);

        expect(state).toBe(initialState);
    });

    it('restartSearch action should set currentStep to \'dataset\'', () => {
        const { initialState } = fromSearch;
        const action = searchActions.restartSearch();
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBe('criteria');
        expect(state.criteriaStepChecked).toBeTruthy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('changeStep action should change the currentStep', () => {
        const { initialState } = fromSearch;
        const action = searchActions.changeStep({ step: 'myStep' });
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBe('myStep');
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('markAsDirty action should set pristine to false', () => {
        const { initialState } = fromSearch;
        const action = searchActions.markAsDirty();
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeFalsy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('changeCurrentDataset action should set pristine to false', () => {
        const { initialState } = fromSearch;
        const action = searchActions.changeCurrentDataset({ currentDataset: 'myDataset' });
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBe('myDataset');
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('checkCriteria action should set criteriaStepChecked to true', () => {
        const { initialState } = fromSearch;
        const action = searchActions.checkCriteria();
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeTruthy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('checkOutput action should set outputStepChecked to true', () => {
        const { initialState } = fromSearch;
        const action = searchActions.checkOutput();
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeTruthy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('checkResult action should set resultStepChecked to true', () => {
        const { initialState } = fromSearch;
        const action = searchActions.checkResult();
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeTruthy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('checkResult action should set resultStepChecked to true', () => {
        const { initialState } = fromSearch;
        const action = searchActions.checkResult();
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeTruthy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('updateCriteriaList action should set criteriaList', () => {
        const { initialState } = fromSearch;
        const criteriaList: Criterion[] = [{ id: 1, type: 'field' }];
        const action = searchActions.updateCriteriaList({ criteriaList });
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(1);
        expect(state.criteriaList).toEqual(criteriaList);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('addCriterion action should add criterion to the criteriaList', () => {
        const { initialState } = fromSearch;
        const criterion: Criterion = { id: 1, type: 'field' };
        const action = searchActions.addCriterion({ criterion });
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(1);
        expect(state.criteriaList).toEqual([criterion]);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('deleteCriterion action should remove criterion to the criteriaList', () => {
        const initialState = {
            ...fromSearch.initialState,
            criteriaList: [{ id: 1, type: 'field' }]
        };
        const action = searchActions.deleteCriterion({ idCriterion: 1 });
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('updateOutputList action should set outputList', () => {
        const { initialState } = fromSearch;
        const outputList: number[] = [1, 2, 3];
        const action = searchActions.updateOutputList({ outputList });
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(3);
        expect(state.outputList).toEqual(outputList);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('addSelectedData action should add data id to the selectedData list', () => {
        const { initialState } = fromSearch;
        const action = searchActions.addSelectedData({ id: 1 });
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(1);
        expect(state.selectedData).toEqual([1]);
        expect(state).not.toBe(initialState);
    });

    it('deleteSelectedData action should remove data id to the selectedData list', () => {
        const initialState = {
            ...fromSearch.initialState,
            selectedData: [1]
        };
        const action = searchActions.deleteSelectedData({ id: 1 });
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('deleteAllSelectedData action should remove all data to the selectedData list', () => {
        const initialState = {
            ...fromSearch.initialState,
            selectedData: [1, 2, 3]
        };
        const action = searchActions.deleteAllSelectedData();
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('retrieveDataLength action should set dataLengthIsLoading to true and dataLengthIsLoaded to false', () => {
        const initialState = {
            ...fromSearch.initialState,
            dataLengthIsLoaded: true
        };
        const action = searchActions.retrieveDataLength();
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeTruthy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('retrieveDataLengthSuccess action should set dataLength, set dataLengthIsLoading to true and dataLengthIsLoaded to false', () => {
        const initialState = {
            ...fromSearch.initialState,
            dataLengthIsLoading: true
        };
        const action = searchActions.retrieveDataLengthSuccess({ length: 1 });
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeTruthy();
        expect(state.dataLength).toEqual(1);
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('retrieveDataLengthFail action should set dataLengthIsLoading to false', () => {
        const initialState = {
            ...fromSearch.initialState,
            dataLengthIsLoading: true
        };
        const action = searchActions.retrieveDataLengthFail();
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('retrieveData action should set dataIsLoading to true and dataIsLoaded to false', () => {
        const initialState = {
            ...fromSearch.initialState,
            dataIsLoaded: true
        };
        const pagination: Pagination = { dname: 'myDataset', page: 1, nbItems: 10, sortedCol: 1, order: PaginationOrder.a };
        const action = searchActions.retrieveData({ pagination });
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeTruthy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('retrieveDataSuccess action should set data, set dataIsLoading to false and dataIsLoaded to true', () => {
        const initialState = {
            ...fromSearch.initialState,
            dataIsLoading: true
        };
        const action = searchActions.retrieveDataSuccess({ data: ['myData'] });
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(1);
        expect(state.data).toEqual(['myData']);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeTruthy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('retrieveDataFail action should set dataIsLoading to false', () => {
        const initialState = {
            ...fromSearch.initialState,
            dataIsLoading: true
        };
        const action = searchActions.retrieveDataFail();
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('destroyResults action should reset data and dataLength', () => {
        const initialState = {
            ...fromSearch.initialState,
            data: ['myData'],
            dataLength: 1
        };
        const action = searchActions.destroyResults();
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toBeNull();
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('resetSearch action should set currentStep to \'dataset\'', () => {
        const { initialState } = fromSearch;
        const action = searchActions.resetSearch();
        const state = fromSearch.searchReducer(initialState, action);

        expect(state.pristine).toBeTruthy();
        expect(state.currentDataset).toBeNull();
        expect(state.currentStep).toEqual('dataset');
        expect(state.criteriaStepChecked).toBeFalsy();
        expect(state.outputStepChecked).toBeFalsy();
        expect(state.resultStepChecked).toBeFalsy();
        expect(state.coneSearchAdded).toBeFalsy();
        expect(state.criteriaList.length).toEqual(0);
        expect(state.outputList.length).toEqual(0);
        expect(state.dataLengthIsLoading).toBeFalsy();
        expect(state.dataLengthIsLoaded).toBeFalsy();
        expect(state.dataLength).toBeNull();
        expect(state.data.length).toEqual(0);
        expect(state.dataIsLoading).toBeFalsy();
        expect(state.dataIsLoaded).toBeFalsy();
        expect(state.selectedData.length).toEqual(0);
        expect(state).not.toBe(initialState);
    });

    it('should get pristine', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectPristine(state)).toBeTruthy();
    });

    it('should get currentDataset', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectCurrentDataset(state)).toBeNull();
    });

    it('should get currentStep', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectCurrentStep(state)).toBeNull();
    });

    it('should get criteriaStepChecked', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectCriteriaStepChecked(state)).toBeFalsy();
    });

    it('should get outputStepChecked', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectOutputStepChecked(state)).toBeFalsy();
    });

    it('should get resultStepChecked', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectResultStepChecked(state)).toBeFalsy();
    });

    it('should get coneSearchAdded', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectIsConeSearchAdded(state)).toBeFalsy();
    });

    it('should get criteriaList', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectCriteriaList(state).length).toEqual(0);
    });

    it('should get outputList', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectOutputList(state).length).toEqual(0);
    });

    it('should get dataLengthIsLoading', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectDataLengthIsLoading(state)).toBeFalsy();
    });

    it('should get dataLengthIsLoaded', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectDataLengthIsLoaded(state)).toBeFalsy();
    });

    it('should get dataLength', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectDataLength(state)).toBeNull();
    });

    it('should get data', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectData(state).length).toEqual(0);
    });

    it('should get dataIsLoading', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectDataIsLoading(state)).toBeFalsy();
    });

    it('should get dataIsLoaded', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectDataIsLoaded(state)).toBeFalsy();
    });

    it('should get selectedData', () => {
        const action = {} as Action;
        const state =  fromSearch.searchReducer(undefined, action);

        expect(fromSearch.selectSelectedData(state).length).toEqual(0);
    });
});
