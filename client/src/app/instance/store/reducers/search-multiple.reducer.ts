/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';

import { SearchMultipleDatasetLength } from '../models';
import * as searchMultipleActions from '../actions/search-multiple.actions';

export interface State {
    pristine: boolean;
    currentStep: string;
    datasetsStepChecked: boolean;
    resultStepChecked: boolean;
    selectedDatasets: string[];
    dataLengthIsLoading: boolean;
    dataLengthIsLoaded: boolean;
    dataLength: SearchMultipleDatasetLength[];
}

export const initialState: State = {
    pristine: true,
    currentStep: null,
    datasetsStepChecked: false,
    resultStepChecked: false,
    selectedDatasets: [],
    dataLengthIsLoading: false,
    dataLengthIsLoaded: false,
    dataLength: []
};

export const searchMultipleReducer = createReducer(
    initialState,
    on(searchMultipleActions.restartSearch, () => ({
        ...initialState,
        currentStep: 'position'
    })),
    on(searchMultipleActions.changeStep, (state, { step }) => ({
        ...state,
        currentStep: step
    })),
    on(searchMultipleActions.markAsDirty, state => ({
        ...state,
        pristine: false
    })),
    on(searchMultipleActions.checkDatasets, state => ({
        ...state,
        datasetsStepChecked: true
    })),
    on(searchMultipleActions.checkResult, state => ({
        ...state,
        resultStepChecked: true
    })),
    on(searchMultipleActions.updateSelectedDatasets, (state, { selectedDatasets }) => ({
        ...state,
        selectedDatasets
    })),
    on(searchMultipleActions.retrieveDataLength, state => ({
        ...state,
        dataLengthIsLoading: true,
        dataLengthIsLoaded: false
    })),
    on(searchMultipleActions.retrieveDataLengthSuccess, (state, { dataLength }) => ({
        ...state,
        dataLength,
        dataLengthIsLoading: false,
        dataLengthIsLoaded: true
    })),
    on(searchMultipleActions.retrieveDataLengthFail, state => ({
        ...state,
        dataLengthIsLoading: false
    }))
);

export const selectPristine = (state: State) => state.pristine;
export const selectCurrentStep = (state: State) => state.currentStep;
export const selectDatasetsStepChecked = (state: State) => state.datasetsStepChecked;
export const selectResultStepChecked = (state: State) => state.resultStepChecked;
export const selectSelectedDatasets = (state: State) => state.selectedDatasets;
export const selectDataLengthIsLoading = (state: State) => state.dataLengthIsLoading;
export const selectDataLengthIsLoaded = (state: State) => state.dataLengthIsLoaded;
export const selectDataLength = (state: State) => state.dataLength;
