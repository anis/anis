/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromDetail from './detail.reducer';
import * as detailActions from '../actions/detail.actions';

describe('[Instance][Store] Detail reducer', () => {
    it('unknown action should return the default state', () => {
        const { initialState } = fromDetail;
        const action = { type: 'Unknown' };
        const state = fromDetail.detailReducer(initialState, action);

        expect(state).toBe(initialState);
    });

    it('retrieveObject action should set objectIsLoading to true and objectIsLoaded to false', () => {
            const initialState = {
                ...fromDetail.initialState,
                objectIsLoaded: false
            };
        const action = detailActions.retrieveObject();
        const state = fromDetail.detailReducer(initialState, action);

        expect(state.object).toBeNull();
        expect(state.objectIsLoading).toBeTruthy();
        expect(state.objectIsLoaded).toBeFalsy();
        expect(state.spectraCSV).toBeNull();
        expect(state.spectraIsLoading).toBeFalsy();
        expect(state.spectraIsLoaded).toBeFalsy();
        expect(state).not.toBe(initialState);
    });

    it('retrieveObjectSuccess action should add object, set objectIsLoading to false and objectIsLoaded to true', () => {
            const initialState = {
                ...fromDetail.initialState,
                objectIsLoading: true
            };
        const action = detailActions.retrieveObjectSuccess({ object: 'myObject' });
        const state = fromDetail.detailReducer(initialState, action);

        expect(state.object).toBe('myObject');
        expect(state.objectIsLoading).toBeFalsy();
        expect(state.objectIsLoaded).toBeTruthy();
        expect(state.spectraCSV).toBeNull();
        expect(state.spectraIsLoading).toBeFalsy();
        expect(state.spectraIsLoaded).toBeFalsy();
        expect(state).not.toBe(initialState);
    });

    it('retrieveObjectFail action should set objectIsLoading to false', () => {
            const initialState = {
                ...fromDetail.initialState,
                objectIsLoading: true
            };
        const action = detailActions.retrieveObjectFail();
        const state = fromDetail.detailReducer(initialState, action);

        expect(state.object).toBeNull();
        expect(state.objectIsLoading).toBeFalsy();
        expect(state.objectIsLoaded).toBeFalsy();
        expect(state.spectraCSV).toBeNull();
        expect(state.spectraIsLoading).toBeFalsy();
        expect(state.spectraIsLoaded).toBeFalsy();
        expect(state).not.toBe(initialState);
    });

    it('retrieveSpectra action should set spectraIsLoading to true and spectraIsLoaded to false', () => {
            const initialState = {
                ...fromDetail.initialState,
                spectraIsLoaded: true
            };
        const action = detailActions.retrieveSpectra({ filename: 'mySpectra' });
        const state = fromDetail.detailReducer(initialState, action);

        expect(state.object).toBeNull();
        expect(state.objectIsLoading).toBeFalsy();
        expect(state.objectIsLoaded).toBeFalsy();
        expect(state.spectraCSV).toBeNull();
        expect(state.spectraIsLoading).toBeTruthy();
        expect(state.spectraIsLoaded).toBeFalsy();
        expect(state).not.toBe(initialState);
    });

    it('retrieveSpectraSuccess action should add spectraCSV, set spectraIsLoading to false and spectraIsLoaded to true', () => {
            const initialState = {
                ...fromDetail.initialState,
                spectraIsLoading: true
            };
        const action = detailActions.retrieveSpectraSuccess({ spectraCSV: 'mySpectra' });
        const state = fromDetail.detailReducer(initialState, action);

        expect(state.object).toBeNull();
        expect(state.objectIsLoading).toBeFalsy();
        expect(state.objectIsLoaded).toBeFalsy();
        expect(state.spectraCSV).toBe('mySpectra');
        expect(state.spectraIsLoading).toBeFalsy();
        expect(state.spectraIsLoaded).toBeTruthy();
        expect(state).not.toBe(initialState);
    });

    it('retrieveSpectraFail action should set spectraIsLoading to false', () => {
            const initialState = {
                ...fromDetail.initialState,
                spectraIsLoading: true
            };
        const action = detailActions.retrieveSpectraFail();
        const state = fromDetail.detailReducer(initialState, action);

        expect(state.object).toBeNull();
        expect(state.objectIsLoading).toBeFalsy();
        expect(state.objectIsLoaded).toBeFalsy();
        expect(state.spectraCSV).toBeNull();
        expect(state.spectraIsLoading).toBeFalsy();
        expect(state.spectraIsLoaded).toBeFalsy();
        expect(state).not.toBe(initialState);
    });

    it('should get object', () => {
        const action = {} as Action;
        const state =  fromDetail.detailReducer(undefined, action);

        expect(fromDetail.selectObject(state)).toBeNull();
    });

    it('should get objectIsLoading', () => {
        const action = {} as Action;
        const state =  fromDetail.detailReducer(undefined, action);

        expect(fromDetail.selectObjectIsLoading(state)).toBeFalsy();
    });

    it('should get objectIsLoaded', () => {
        const action = {} as Action;
        const state =  fromDetail.detailReducer(undefined, action);

        expect(fromDetail.selectObjectIsLoaded(state)).toBeFalsy();
    });

    it('should get spectraCSV', () => {
        const action = {} as Action;
        const state =  fromDetail.detailReducer(undefined, action);

        expect(fromDetail.selectSpectraCSV(state)).toBeNull();
    });

    it('should get spectraIsLoading', () => {
        const action = {} as Action;
        const state =  fromDetail.detailReducer(undefined, action);

        expect(fromDetail.selectSpectraIsLoading(state)).toBeFalsy();
    });

    it('should get spectraIsLoaded', () => {
        const action = {} as Action;
        const state =  fromDetail.detailReducer(undefined, action);

        expect(fromDetail.selectSpectraIsLoaded(state)).toBeFalsy();
    });
});
