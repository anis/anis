/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    HttpClientTestingModule,
    HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { AppConfigService } from 'src/app/app-config.service';
import { ArchiveService } from './archive.service';

describe('[instance][store][services] ArchiveService', () => {
    let archiveService: ArchiveService;
    let httpController: HttpTestingController;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                ArchiveService,
                {
                    provide: AppConfigService,
                    useValue: {
                        apiUrl: 'test',
                    },
                },
            ],
        });
        archiveService = TestBed.inject(ArchiveService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
    }));

    it('#startTaskCreateArchive() should request return an Observable<any>', () => {
        archiveService.startTaskCreateArchive('test').subscribe();
        const url = 'test/archive/start-task-create-archive/test';
        httpController.expectOne({ method: 'GET', url: `${url}` });
    });

    it('#isArchiveAvailable() should request return an Observable<any>', () => {
        archiveService.isArchiveAvailable(3).subscribe();
        const url = 'test/archive/is-archive-available/3';
        httpController.expectOne({ method: 'GET', url: `${url}` });
    });
});
