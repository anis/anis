/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AppConfigService } from 'src/app/app-config.service';

@Injectable()
export class DetailService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves object details for the given parameters.
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param number criterionId - The criterion ID.
     * @param string objectSelected - The selected object ID.
     *
     * @return Observable<any[]>
     */
    retrieveObject(instanceName: string, datasetName: string, criterionId: number, objectSelected: string): Observable<any[]> {
        const query = `c=${criterionId}::eq::${objectSelected}&a=all`;
        return this.http.get<any[]>(`${this.config.apiUrl}/search/${instanceName}/${datasetName}?${query}`);
    }

    /**
     * Retrieves spectra data for the given spectra file.
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param string spectraFile - The spectra file name
     *
     * @return Observable<string>
     */
    retrieveSpectra(instanceName: string, datasetName: string, spectraFile: string): Observable<string> {
        return this.http.get(`${this.config.servicesUrl}/spectra/spectra-to-csv/${instanceName}/${datasetName}?filename=${spectraFile}`, { responseType: 'text' });
    }
}
