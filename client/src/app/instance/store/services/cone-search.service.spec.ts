/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ConeSearchService } from './cone-search.service';

describe('[Instance][Store] ConeSearchService', () => {
    let service: ConeSearchService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [ConeSearchService]
        });
        service = TestBed.inject(ConeSearchService);
    });

    it('#retrieveCoordinates() should return an Observable<any[]>',
        inject([HttpTestingController, ConeSearchService], (httpMock: HttpTestingController, coneSearchService: ConeSearchService) => {
            const mockResponse = 'myResponse';

            coneSearchService.retrieveCoordinates('myObject').subscribe((event: any) => {
                expect(event).toEqual(mockResponse);
            });

            const mockRequest = httpMock.expectOne('https://cdsweb.u-strasbg.fr/cgi-bin/nph-sesame/-ox/NSV?myObject');

            expect(mockRequest.cancelled).toBeFalsy();
            expect(mockRequest.request.responseType).toEqual('text');
            mockRequest.flush(mockResponse);

            httpMock.verify();
        })
    );
});
