/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AppConfigService } from 'src/app/app-config.service';
import { SearchService } from './search.service';
import { RESULT } from 'src/test-data';

describe('[Instance][Store] SearchService', () => {
    let searchService: SearchService;
    let httpController: HttpTestingController;
    let result: any[];

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                SearchService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                }
            ]
        });
        searchService = TestBed.inject(SearchService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        result = [ ...RESULT ];
    }));

    it('#retrieveData() should return an Observable<any[]>', () => {
        searchService.retrieveData('default', 'observations', 'a=1;2;3&c=1::in::418|419|420').subscribe((event: any[]) => {
            expect(event).toEqual(result);
        });

        const url = 'http://localhost:8080/search/default/observations?a=1;2;3&c=1::in::418|419|420';
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });

        expect(mockRequest.cancelled).toBeFalsy();
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush(result);
    });

    it('#retrieveDataLength() should return an Observable<{ nb: number }[]>', () => {
        let countResult = [
            {
                nb: result.length
            }
        ]

        searchService.retrieveDataLength('default', 'observations', 'a=count&c=1::in::418|419|420').subscribe((event: any[]) => {
            expect(event).toEqual(countResult);
        });

        const url = 'http://localhost:8080/search/default/observations?a=count&c=1::in::418|419|420';
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });

        expect(mockRequest.cancelled).toBeFalsy();
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush(countResult);
    });
});
