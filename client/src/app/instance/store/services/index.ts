/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SearchService } from './search.service';
import { ConeSearchService } from './cone-search.service';
import { DetailService } from './detail.service';
import { ArchiveService } from './archive.service';

export const instanceServices = [
    SearchService,
    ConeSearchService,
    DetailService,
    ArchiveService
];
