/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { AppConfigService } from 'src/app/app-config.service';
import { Archive } from '../models/archive.model';

@Injectable({ providedIn: 'root' })
export class ArchiveService {
    constructor(private http: HttpClient, private config: AppConfigService) {}

    startTaskCreateArchive(query: string) {
        return this.http
            .get<{
                status: number;
                data: Archive;
            }>(`${this.config.apiUrl}/archive/start-task-create-archive/${query}`)
            .pipe(
                map((response) => response.data),
            );
    }

    isArchiveAvailable(id: number) {
        return this.http
            .get<{ status: number; data: { archive_is_available: boolean } }>(
                `${this.config.apiUrl}/archive/is-archive-available/${id}`
            )
            .pipe(map((response) => response.data));
    }
}
