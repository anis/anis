/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AppConfigService } from 'src/app/app-config.service';

@Injectable()
export class SearchService {
    /**
     * @param HttpClient http - Service used to call http url
     */
    constructor(private http: HttpClient, private config: AppConfigService) { }

    /**
     * Retrieves results for the given parameters.
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param string query - The ANIS search query
     *
     * @return Observable<any[]>
     */
    retrieveData(instanceName: string, datasetName: string, query: string): Observable<any[]> {
        return this.http.get<any[]>(`${this.config.apiUrl}/search/${instanceName}/${datasetName}?${query}`);
    }

    /**
     * Retrieves results number for the given parameters.
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param string query - The ANIS search query
     *
     * @return Observable<{ nb: number }[]>
     */
    retrieveDataLength(instanceName: string, datasetName: string, query: string): Observable<{ nb: number }[]> {
        return this.http.get<{ nb: number }[]>(`${this.config.apiUrl}/search/${instanceName}/${datasetName}?${query}`);
    }
}
