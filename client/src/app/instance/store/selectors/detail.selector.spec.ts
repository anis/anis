/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as detailSelector from './detail.selector';
import * as fromDetail from '../reducers/detail.reducer';

describe('[Instance][Store] Detail selector', () => {
    it('should get object', () => {
        const state = { instance: { detail: { ...fromDetail.initialState }}};
        expect(detailSelector.selectObject(state)).toBeNull();
    });

    it('should get objectIsLoading', () => {
        const state = { instance: { detail: { ...fromDetail.initialState }}};
        expect(detailSelector.selectObjectIsLoading(state)).toBeFalsy();
    });

    it('should get objectIsLoaded', () => {
        const state = { instance: { detail: { ...fromDetail.initialState }}};
        expect(detailSelector.selectObjectIsLoaded(state)).toBeFalsy();
    });

    it('should get spectraCSV', () => {
        const state = { instance: { detail: { ...fromDetail.initialState }}};
        expect(detailSelector.selectSpectraCSV(state)).toBeNull();
    });

    it('should get spectraIsLoading', () => {
        const state = { instance: { detail: { ...fromDetail.initialState }}};
        expect(detailSelector.selectSpectraIsLoading(state)).toBeFalsy();
    });

    it('should get spectraIsLoaded', () => {
        const state = { instance: { detail: { ...fromDetail.initialState }}};
        expect(detailSelector.selectSpectraIsLoaded(state)).toBeFalsy();
    });

    it('should get id object by route', () => {
        const state = { router: { state: { params: { id: 'myObjectId' }}}};
        expect(detailSelector.selectIdByRoute(state)).toEqual('myObjectId');
    });
});
