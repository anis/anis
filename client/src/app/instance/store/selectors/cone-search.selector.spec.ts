/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as coneSearchSelector from './cone-search.selector';
import * as fromConeSearch from '../reducers/cone-search.reducer';

describe('[Instance][Store] Cone search selector', () => {
    it('should get coneSearch', () => {
        const state = { instance: { coneSearch: { ...fromConeSearch.initialState }}};
        expect(coneSearchSelector.selectConeSearch(state)).toBeNull();
    });

    it('should get resolverIsLoading', () => {
        const state = { instance: { coneSearch: { ...fromConeSearch.initialState }}};
        expect(coneSearchSelector.selectResolverIsLoading(state)).toBeFalsy();
    });

    it('should get resolverIsLoaded', () => {
        const state = { instance: { coneSearch: { ...fromConeSearch.initialState }}};
        expect(coneSearchSelector.selectResolverIsLoaded(state)).toBeFalsy();
    });

    it('should get cone search by route', () => {
        const state = { router: { state: { queryParams: { cs: 'myConeSearch' }}}};
        expect(coneSearchSelector.selectConeSearchByRoute(state)).toEqual('myConeSearch');
    });
});
