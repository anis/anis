/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../../instance.reducer';
import * as fromArchive from '../reducers/archive.reducer';

export const selectArchiveState = createSelector(
    reducer.getInstanceState,
    (state: reducer.State) => state.archive
);

export const selectArchiveIsCreating = createSelector(
    selectArchiveState,
    fromArchive.selectArchiveIsCreating
);
