/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { TestBed, waitForAsync, ComponentFixture } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { InstanceComponent } from './instance.component';
import * as authActions from 'src/app/user/store/actions/auth.actions';
import { DesignConfig, Instance, MenuItem } from '../metamodel/models';
import { UserProfile } from '../user/store/models/user-profile.model';

describe('[Instance] InstanceComponent', () => {
    @Component({ selector: 'app-instance-navbar', template: '' })
    class NavbarStubComponent {
        @Input() isAuthenticated: boolean;
        @Input() userProfile: UserProfile = null;
        @Input() userRoles: string[];
        @Input() instance: Instance;
        @Input() designConfig: DesignConfig;
        @Input() menuItemList: MenuItem[];
    }

    let component: InstanceComponent;
    let fixture: ComponentFixture<InstanceComponent>;
    let store: MockStore;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule, HttpClientTestingModule],
            declarations: [
                InstanceComponent,
                NavbarStubComponent
            ],
            providers: [
                provideMockStore({})
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(InstanceComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        const spy = jest.spyOn(store, 'dispatch');
        component.ngOnInit();
        Promise.resolve(null).then(function() {
            expect(spy).toHaveBeenCalledTimes(4);
            done();
        });
    });

    it('#login() should dispatch login action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.login();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(authActions.login({ redirectUri: window.location.toString() }));
    });

    it('#logout() should dispatch logout action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.logout();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(authActions.logout());
    });

    it('#openEditProfile() should dispatch open edit profile action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.openEditProfile();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(authActions.openEditProfile());
    });
});
