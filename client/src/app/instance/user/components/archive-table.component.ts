/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    Component,
    Input,
    Output,
    ChangeDetectionStrategy,
    EventEmitter,
} from '@angular/core';

import { Archive } from 'src/app/user/store/models';
import { getHost } from 'src/app/shared/utils';
import { DesignConfig } from 'src/app/metamodel/models';

@Component({
    selector: 'app-archive-table',
    templateUrl: 'archive-table.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ArchiveTableComponent {
    @Input() designConfig: DesignConfig;
    @Input() archiveList: Archive[];
    @Input() apiUrl: string;
    @Output() downloadArchive: EventEmitter<{ url: string; filename: string }> =
        new EventEmitter();

    emitDownload(archive: Archive): void {
        this.downloadArchive.emit({
            url: `${getHost(this.apiUrl)}/archive/download-archive/${
                archive.id
            }`,
            filename: archive.name,
        });
    }
}
