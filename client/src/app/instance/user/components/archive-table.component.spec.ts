/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ArchiveTableComponent } from './archive-table.component';
import * as utils from 'src/app/shared/utils';
import { Archive } from 'src/app/user/store/models';

describe('[Instance][User][Components] ArchiveTableComponent', () => {
    let fixture: ComponentFixture<ArchiveTableComponent>;
    let component: ArchiveTableComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ArchiveTableComponent],
        });
        fixture = TestBed.createComponent(ArchiveTableComponent);
        component = fixture.componentInstance;
    });

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should be truthy', () => {
        expect(component).toBeTruthy();
    });

    it('should call #emitDowloadProperly', () => {
        const spyDowload = jest
            .spyOn(component.downloadArchive, 'emit')
            .mockImplementation(jest.fn());
        const archive: Archive = {
            id: 12,
            name: 'archive_default_observations_2023-09-22T13:59:28.zip',
            created: '2023-09-22T15:08:59',
            status: 'created',
            file_size: 12,
            email: 'someone@lam.fr',
            instance_name: 'default',
            dataset_name: 'default_dataset',
        };
        const spyGetHost = jest
            .spyOn(utils, 'getHost')
            .mockImplementation(() => 'https://somewhere.lam.fr');

        component.emitDownload(archive);

        expect(spyGetHost).toHaveBeenCalled();
        expect(spyDowload).toHaveBeenCalledWith({
            url: 'https://somewhere.lam.fr/archive/download-archive/12',
            filename: 'archive_default_observations_2023-09-22T13:59:28.zip',
        });
    });
});
