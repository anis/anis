/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Archive } from 'src/app/user/store/models';
import * as archiveActions from 'src/app/user/store/actions/archive.actions';
import * as archiveSelector from 'src/app/user/store/selectors/archive.selector';
import * as searchActions from 'src/app/instance/store/actions/search.actions';
import * as designConfigSelector from 'src/app/metamodel/selectors/design-config.selector';
import { AppConfigService } from 'src/app/app-config.service';
import { DesignConfig } from 'src/app/metamodel/models';

@Component({
    selector: 'app-archive-list',
    templateUrl: 'archive-list.component.html',
})
export class ArchiveListComponent implements OnInit {
    public designConfig: Observable<DesignConfig>;
    public archives: Observable<Archive[]>;
    public archiveListIsLoading: Observable<boolean>;
    public archiveListIsLoaded: Observable<boolean>;

    constructor(
        private store: Store<{}>,
        private config: AppConfigService,
    ) {
        this.designConfig = store.select(
            designConfigSelector.selectDesignConfig,
        );
        this.archives = store.select(
            archiveSelector.selectArchiveListByInstanceName,
        );
        this.archiveListIsLoading = store.select(
            archiveSelector.selectArchiveListIsLoading,
        );
        this.archiveListIsLoaded = store.select(
            archiveSelector.selectArchiveListIsLoaded,
        );
    }

    ngOnInit(): void {
        this.store.dispatch(archiveActions.loadArchiveList());
    }

    getApiUrl(): string {
        return this.config.apiUrl;
    }

    downloadArchive(file: { url: string; filename: string }): void {
        this.store.dispatch(searchActions.downloadFile(file));
    }
}
