/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserComponent } from './user.component';
import { ArchiveListComponent } from './containers/archive-list.component';

const routes: Routes = [
    {
        path: '',
        component: UserComponent,
        children: [{ path: 'archive-list', component: ArchiveListComponent }],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserRoutingModule {}

export const routedComponents = [UserComponent, ArchiveListComponent];
