/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import TusOptions from 'src/app/tus-upload/tus/tus-options';

let tusOptions: TusOptions = {
    endpoint: 'http://localhost:8080/upload/default',
    chunkSize: 2000,
};

export default tusOptions;
