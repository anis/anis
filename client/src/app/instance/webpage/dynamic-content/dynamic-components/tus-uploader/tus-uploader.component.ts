/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';

import { ToastrService } from 'ngx-toastr';
import { Observable, map } from 'rxjs';
import { AppConfigService } from 'src/app/app-config.service';
import { TusStorageService } from 'src/app/tus-upload/tus-storage.service';
import * as authSelector from 'src/app/user/store/selectors/auth.selector';
import TusUpload, { UploadKind } from 'src/app/tus-upload/tus/tus-upload';
import { FormGroup, FormControl, Validators } from '@angular/forms';

/**
 * Manage the list of files uploaded via the tus protocol
 * Start uploads by instantiating child components `TusUploaderProgressComponent`
 * Make the clean up on the localStorage after deletion or completion of uploads directly.
 * Resuming uploads is not managed properly yet.
 * @class
 * @see [tus](https://tus.io/protocols/resumable-upload)
 */
@Component({
    selector: 'app-uploader',
    templateUrl: './tus-uploader.component.html',
    styleUrls: ['./tus-uploader.component.scss'],
})
export class TusUploaderComponent {
    /** @property url endpoint on which to upload the file. */
    @Input() endpoint: string = null;
    /** @property check if upload is possible or not */
    isPossible = false;
    isCurrentUpload = false;

    // TODO(lmenou): clarify the situation for various products
    // NOTE(lmenou): FROM an `UploadKind` decided from the component => retrieve all
    // available types from the database and construct a select-menu from there to
    // get the `type`.
    toUpload = new FormGroup({
        fileMetaData: new FormGroup({
            identifier: new FormControl<string>('', Validators.required),
            product_type: new FormControl<UploadKind>(UploadKind.SPECTRUM),
            version: new FormControl<number>(null, Validators.required),
            level: new FormControl<string>('Lg0', Validators.required),
            description: new FormControl<string>(''),
            template_id: new FormControl<string>('harps', Validators.required),
            dataset_tag: new FormControl<string>(''),
            provider_tag: new FormControl<string>(''),
        }),
        file: new FormControl(null, Validators.required),
    });

    /**
     * @property upload to perform
     * @public
     */
    currentUpload: TusUpload = {
        file: null,
        fileMetadata: null,
    };

    constructor(
        private toastr: ToastrService,
        private tusStorage: TusStorageService,
        private store: Store<any>,
        private appConfig: AppConfigService,
    ) {}

    // TODO(lmenou): implement this
    /** Shall retrieve information about previous uploads */
    // ngOnInit() {}

    /**
     * Check if the upload is possible.
     * User must be logged in, and app not in dev mode.
     * @method
     */
    uploadIsPossible(): Observable<boolean> {
        return this.store.select(authSelector.selectIsAuthenticated).pipe(
            map((isAuthenticated: boolean) => {
                this.isPossible =
                    !this.appConfig.authenticationEnabled || isAuthenticated
                        ? true
                        : false;
                return this.isPossible;
            }),
        );
    }

    /**
     * Assign file to upload on form change.
     * @method
     */
    onFileSelected(event: Event) {
        // NOTE(lmenou): This is necessary for a weird reason, as Angular cannot perform file input binding within reactive
        // form nor template driven forms.
        // See:
        //  - https://netbasal.com/how-to-implement-file-uploading-in-angular-reactive-forms-89a3fffa1a03
        //  - https://stackoverflow.com/questions/51536752/angular-5-how-to-get-file-name-from-input-with-type-file
        const target = event.target as HTMLInputElement;
        if (target.files && target.files.length > 0) {
            this.currentUpload.file = target.files[0];
        }
    }

    /**
     * Trigger the upload of the file in the form.
     * This internally trigger the creation of a `TusUploaderProgressComponent` that manages the upload process.
     * Files can be uploaded one by one as of now.
     * @method
     */
    upload(): void {
        this.currentUpload.fileMetadata = {
            identifier: this.toUpload.value.fileMetaData.identifier,
            product_type: this.toUpload.value.fileMetaData.product_type,
            version: this.toUpload.value.fileMetaData.version,
            level: this.toUpload.value.fileMetaData.level,
            description: this.toUpload.value.fileMetaData.description,
            template_id: this.toUpload.value.fileMetaData.template_id,
            dataset_tag: this.toUpload.value.fileMetaData.dataset_tag,
            provider_tag: this.toUpload.value.fileMetaData.provider_tag,
        };
        this.isCurrentUpload = true;
    }

    /**
     * Is the callback when an upload is finished to notify the user.
     * Clean up the localStorage of all the completed upload information.
     * @method
     * @param file {File} - Receive the uploaded file (for the cleanup) via an emission of `TusUploaderProgressComponent`
     */
    completion(upload: TusUpload) {
        // If file uploaded, delete the reference in the localStorage BUT NOT the view
        try {
            this.tusStorage.removeUpload(upload.file);
            this.toastr.success(`File ${upload.file.name} uploaded.`, 'Upload');
        } catch (e) {
            console.error(e.message);
        }
        this.isCurrentUpload = false;
    }

    /**
     * Is the callback when an upload in progress is deleted by the user.
     * Clean up the localStorage of all the deleted upload information.
     * Also delete file from the {@link TusUploaderComponent#files}.
     * Consequently delete the view of the `TusUploaderProgressComponent` instance managing the upload.
     * @method
     * @param upload {Upload} - Receive the uploaded uploads (for the cleanup) via an emission of `TusUploaderProgressComponent`
     */
    deletion(upload: TusUpload) {
        try {
            this.tusStorage.removeUpload(upload.file);
            this.toastr.success(`File ${upload.file.name} deleted.`, 'Upload');
        } catch (e) {
            console.error(e.message);
            // could not delete resources, end here
            return;
        }
        // If file deleted, delete the reference in the localStorage AND the view
        this.isCurrentUpload = false;
    }
}
