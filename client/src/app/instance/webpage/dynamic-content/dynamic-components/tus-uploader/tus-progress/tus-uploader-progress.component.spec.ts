import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
    HttpResponse,
    HttpHeaderResponse,
    HttpHeaders,
} from '@angular/common/http';

import { TusUploaderProgressComponent } from './tus-uploader-progress.component';
import { TusUploadService } from 'src/app/tus-upload/tus-upload.service';
import { AppConfigService } from 'src/app/app-config.service';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import TusUpload from 'src/app/tus-upload/tus/tus-upload';
import { UploadKind } from 'src/app/tus-upload/tus/tus-upload';

jest.mock('src/app/tus-upload/tus-upload.service');
jest.mock('src/app/app-config.service');

describe('UploaderProgressComponent', () => {
    let component: TusUploaderProgressComponent;
    let fixture: ComponentFixture<TusUploaderProgressComponent>;

    const appConfig = {};

    const file = new File(['<p></p>'], 'someHtml', { type: 'text/html' });
    const upload: TusUpload = {
        file: file,
        fileMetadata: {
            identifier: 'coconut',
            product_type: UploadKind.SPECTRUM,
            level: 'Lg0',
            version: 1,
            template_id: 'HARPS',
        },
    };

    const tusHeaders = new HttpHeaders({
        Host: 'tus',
        'Tus-Resumable': '1.0.0',
        'Upload-Offset': 7,
    });
    const tusResponse = new HttpResponse<HttpHeaderResponse>({
        headers: tusHeaders,
        status: 200,
        url: 'tus.example.org',
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [TusUploaderProgressComponent],
            providers: [
                TusUploadService,
                { provide: AppConfigService, useValue: appConfig },
            ],
        });
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    describe('should create', () => {
        it('should start an upload directly and finish it properly', () => {
            jest.spyOn(TusUploadService.prototype, 'upload').mockImplementation(
                () => of(tusResponse),
            );

            fixture = TestBed.createComponent(TusUploaderProgressComponent);
            component = fixture.componentInstance;
            component.upload = upload;
            fixture.detectChanges();

            expect(component).toBeTruthy();
            expect(component.upload.file.size).toEqual(
                (component as any).contentLength,
            );
            expect(component.percentageUploaded).toEqual(100);
            expect(component.isCompleted).toBeTruthy();
        });
    });

    describe('should handle all other tus methods', () => {
        it('abort', () => {
            tusHeaders.set('Upload-Offset', '3');
            jest.spyOn(TusUploadService.prototype, 'upload').mockImplementation(
                () => of(tusResponse),
            );

            fixture = TestBed.createComponent(TusUploaderProgressComponent);
            component = fixture.componentInstance;
            component.upload = upload;
            fixture.detectChanges();

            const spyAbortion = jest.spyOn(component, 'abort');
            const buttonAbort = fixture.debugElement.query(By.css('button'));
            buttonAbort.triggerEventHandler('click', null);
            expect(spyAbortion).toHaveBeenCalled();
        });

        it('resume', () => {
            tusHeaders.set('Upload-Offset', '3');
            jest.spyOn(TusUploadService.prototype, 'upload').mockImplementation(
                () => of(tusResponse),
            );
            jest.spyOn(TusUploadService.prototype, 'resume').mockImplementation(
                () => of(tusResponse),
            );

            fixture = TestBed.createComponent(TusUploaderProgressComponent);
            component = fixture.componentInstance;
            component.upload = upload;
            fixture.detectChanges();

            const spyResume = jest.spyOn(component, 'resume');
            const buttons = fixture.debugElement.queryAll(By.css('button'));
            // click the second button which is the resume
            buttons[1].triggerEventHandler('click', null);
            expect(spyResume).toHaveBeenCalled();
        });

        it('delete', () => {
            tusHeaders.set('Upload-Offset', '3');
            jest.spyOn(TusUploadService.prototype, 'upload').mockImplementation(
                () => of(tusResponse),
            );
            jest.spyOn(TusUploadService.prototype, 'delete').mockImplementation(
                () => of(tusResponse),
            );

            fixture = TestBed.createComponent(TusUploaderProgressComponent);
            component = fixture.componentInstance;
            component.upload = upload;
            fixture.detectChanges();

            const spyDelete = jest.spyOn(component, 'delete');
            const buttons = fixture.debugElement.queryAll(By.css('button'));
            // click the second button which is the resume
            buttons[2].triggerEventHandler('click', null);
            expect(spyDelete).toHaveBeenCalled();
        });
    });
});
