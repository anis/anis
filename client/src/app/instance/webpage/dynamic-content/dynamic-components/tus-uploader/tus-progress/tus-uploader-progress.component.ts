/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { HttpHeaderResponse, HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { TusUploadService } from 'src/app/tus-upload/tus-upload.service';
import TusOptions from 'src/app/tus-upload/tus/tus-options';
import TusUpload from 'src/app/tus-upload/tus/tus-upload';

/**
 * Visualise the progress of the upload per file.
 * One component per upload. One component per upload.
 * Dialog with the parent component `TusUploaderComponent` via `EventEmitter`.
 * @class
 */
@Component({
    selector: 'app-uploader-progress',
    templateUrl: './tus-uploader-progress.component.html',
    styleUrls: ['./tus-uploader-progress.component.scss'],
    providers: [TusUploadService],
})
export class TusUploaderProgressComponent implements OnInit {
    /** The file from which upload is managed by the instance of the component. */
    @Input() upload: TusUpload = null;
    /** The url endpoint on which to upload the file. */
    @Input() endpoint: string = null;
    /** The event when the upload is completed. */
    @Output() completed: EventEmitter<TusUpload> = new EventEmitter();
    /** The event when the upload is deleted. */
    @Output() deleted: EventEmitter<TusUpload> = new EventEmitter();

    /** The total size of the file to upload. */
    private contentLength: number = null;
    /** The offset already uploaded onto the server. */
    private contentOffset: number = null;
    /** The percentage of the file uploaded to the server. */
    percentageUploaded = 0.0;

    /**
     * The Observer managing the responses from the server.
     * Managing the progress.
     */
    private _progressObserver = {
        next: (response: HttpResponse<HttpHeaderResponse>) => {
            this.contentOffset = +response.headers.get('Upload-Offset');
            this.percentageUploaded = Math.round(
                (this.contentOffset / this.contentLength) * 100,
            );
        },
        complete: () => {
            if (this.percentageUploaded === 100) {
                this.complete();
            }
        },
    };

    /** If the upload is proceeding. */
    public isUploading = true;
    /** If the upload is completed. */
    public isCompleted = false;

    constructor(private tusUpload: TusUploadService) {}

    /**
     * Start the upload immediately after the component is instantiated.
     * @method
     */
    ngOnInit(): void {
        const endpoint = this.endpoint ? `${this.endpoint}` : null;
        const options: TusOptions = {
            endpoint: endpoint,
            chunkSize: 268435456, // 1/4 GB
        };
        this.tusUpload.options(options);

        this.contentLength = this.upload.file.size;
        this.contentOffset = 0;
        this.tusUpload.upload(this.upload).subscribe(this._progressObserver);
    }

    /**
     * Abort the upload but do not terminate it.
     * @method
     */
    abort(): void {
        this.isUploading = false;
        this.tusUpload.abort();
    }

    /**
     * Resume the upload.
     * @method
     */
    resume(): void {
        this.isUploading = true;
        this.tusUpload.resume().subscribe(this._progressObserver);
    }

    /**
     * Emit the completion of the upload.
     * @method
     */
    complete(): void {
        this.isCompleted = true;
        this.completed.emit(this.upload);
    }

    /**
     * Emit the deletion of the upload.
     * @method
     */
    delete(): void {
        this.tusUpload
            .delete()
            .subscribe((response: HttpResponse<HttpHeaderResponse>) => {
                // If no response, error is logged by the service
                if (response.ok) {
                    this.deleted.emit(this.upload);
                }
            });
    }
}
