import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Input } from '@angular/core';
import { By } from '@angular/platform-browser';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { TusUploaderComponent } from './tus-uploader.component';
import { ToastrService } from 'ngx-toastr';
import { TusStorageService } from 'src/app/tus-upload/tus-storage.service';
import { AppConfigService } from 'src/app/app-config.service';
import * as authSelector from 'src/app/user/store/selectors/auth.selector';
import TusUpload from 'src/app/tus-upload/tus/tus-upload';

jest.mock('ngx-toastr');
jest.mock('src/app/tus-upload/tus-storage.service');
jest.mock('src/app/app-config.service');

describe('UploaderComponent', () => {
    let component: TusUploaderComponent;
    let fixture: ComponentFixture<TusUploaderComponent>;
    let store: MockStore;

    const toastr = {
        error: () => null,
        success: () => null,
    };
    const tusStorage = {
        removeUpload: (file: File) => null,
    };
    const appConfig = {};

    @Component({
        selector: 'app-uploader-progress',
        template: '<p></p>',
    })
    class TusUploaderProgressComponent {
        @Input() upload: TusUpload;
        @Input() endpoint: string = null;
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [TusUploaderComponent, TusUploaderProgressComponent],
            imports: [ReactiveFormsModule],
            providers: [
                { provide: TusStorageService, useValue: tusStorage },
                { provide: ToastrService, useValue: toastr },
                { provide: AppConfigService, useValue: appConfig },
                provideMockStore({
                    selectors: [
                        {
                            selector: authSelector.selectIsAuthenticated,
                            value: true,
                        },
                    ],
                }),
            ],
        });

        fixture = TestBed.createComponent(TusUploaderComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        fixture.detectChanges();
    });

    afterEach(() => {
        jest.resetAllMocks();
        store?.resetSelectors();
    });

    describe('component creation', () => {
        it('should be truthy', () => {
            expect(component).toBeTruthy();
        });
    });

    describe('public method upload', () => {
        it('should push files onto the property files', () => {
            // mocking a good fileList
            const file = new File([''], 'filename', { type: 'text/html' });
            (component as any).currentUpload = {
                ...(component as any).currentUpload,
                file: file,
            };
            component.upload();
            fixture.detectChanges();

            expect(component.isCurrentUpload).toEqual(true);
        });

        it('should create a view child', () => {
            const file = new File([''], 'filename', { type: 'text/html' });
            (component as any).currentUpload = {
                ...(component as any).currentUpload,
                file: file,
            };
            component.upload();
            fixture.detectChanges();

            const childComponent = fixture.debugElement.query(
                By.css('app-uploader-progress'),
            );
            expect(childComponent).toBeTruthy();
        });
    });

    describe('public method completion', () => {
        it('should remove file from the localStorage', () => {
            const file = new File([''], 'filename', { type: 'text/html' });
            const upload = {
                ...(component as any).currentUpload,
                file: file,
            };
            const spyStorage = jest.spyOn(tusStorage, 'removeUpload');
            const spyToastr = jest.spyOn(toastr, 'success');

            component.completion(upload);
            expect(spyStorage).toHaveBeenCalled();
            expect(spyToastr).toHaveBeenCalled();
        });

        it('should log an error on error', () => {
            const file = new File([''], 'filename', { type: 'text/html' });
            const upload = {
                ...(component as any).currentUpload,
                file: file,
            };
            // mock an error
            tusStorage.removeUpload = () => {
                throw new Error('Error');
            };

            const spyConsole = jest
                .spyOn(console, 'error')
                .mockImplementation(() => null);
            component.completion(upload);
            expect(spyConsole).toHaveBeenCalledTimes(1);
        });
    });

    describe('public method deletion', () => {
        it('should log an error on error', () => {
            const file = new File([''], 'filename', { type: 'text/html' });
            const upload = {
                ...(component as any).currentUpload,
                file: file,
            };

            const spyConsole = jest
                .spyOn(console, 'error')
                .mockImplementation(() => null);
            component.completion(upload);
            expect(spyConsole).toHaveBeenCalled();
        });

        it('should remove file from the localStorage', () => {
            const file1 = new File([''], 'filename', { type: 'text/html' });
            const upload1 = {
                ...(component as any).currentUpload,
                file: file1,
            };
            // re-mock the success
            tusStorage.removeUpload = () => null;

            const spyStorage = jest.spyOn(tusStorage, 'removeUpload');
            const spyToastr = jest.spyOn(toastr, 'success');
            component.currentUpload = upload1
            fixture.detectChanges();

            component.deletion(upload1);

            expect(spyStorage).toHaveBeenCalled();
            expect(spyToastr).toHaveBeenCalled();
        });
    });
});
