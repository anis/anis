/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, switchMap } from 'rxjs';

import { Instance, Dataset, Attribute } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';
import { DatasetService } from 'src/app/metamodel/services/dataset.service';
import { AttributeService } from 'src/app/metamodel/services/attribute.service';

@Component({
    selector: 'app-dataset-sample',
    templateUrl: 'dataset-sample.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetSampleComponent implements OnInit {
    @Input() instance: Instance;
    @Input() datasetName: string;
    @Input() a: string;
    @Input() nbItems: number;
    @Input() renderersEnabled: boolean;

    dataset: Observable<Dataset>;
    attributeList: Observable<Attribute[]>;
    data: Observable<any[]>;

    constructor(private datasetService: DatasetService, private attributeService: AttributeService, private http: HttpClient, private config: AppConfigService) { }

    ngOnInit() {
        this.data = (this.dataset = this.datasetService.retrieveDataset(this.instance.name, this.datasetName)).pipe(
            switchMap(dataset => {
                const query = `a=${this.a}&o=${dataset.default_order_by}:${dataset.default_order_by_direction}&p=${this.nbItems}:1`;
                return this.http.get<any[]>(`${this.config.apiUrl}/search/${this.instance.name}/${this.datasetName}?${query}`);
            })
        );
        this.attributeList = this.attributeService.retrieveAttributeList(this.instance.name, this.datasetName);
    }
}
