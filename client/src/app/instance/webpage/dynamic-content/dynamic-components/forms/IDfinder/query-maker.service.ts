/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


import { Injectable } from '@angular/core';
import { merge, Observable, switchMap } from 'rxjs';

import { SearchService } from 'src/app/instance/store/services/search.service';
import { GetterAliases } from './aliases.store';
import { Attribute } from 'src/app/metamodel/models/attribute.model';
import { AttributeService } from 'src/app/metamodel/services/attribute.service';

@Injectable()
export class QueryMakerService {
    constructor(
        private search: SearchService,
        private attribute: AttributeService,
    ) {}

    private inputIsString(input: string): boolean {
        const num = parseInt(input);
        return isNaN(num);
    }

    aliasesRetriever(id: GetterAliases): Observable<Object[]> {
        const a = id.attributesToSearch.toString().replace(/\,/g, ';');
        const alias = id.alias.toString().trim().toUpperCase();
        let observables: Observable<any[]>[] = [];
        let query: string = null;

        if (!this.inputIsString(id.alias)) {
            for (const el of id.attributesToSearch) {
                query = `a=${a}&c=${el}::eq::${alias}`;
                observables.push(this.search.retrieveData(id.instanceName, id.datasetName, query));
            }

            return merge(...observables);
        }

        return this.attribute.retrieveAttributeList(id.instanceName, id.datasetName).pipe(
            switchMap((attr: Attribute[]) => {
                for (const at of attr) {
                    if (at.type === 'text') {
                        query = `a=${a}&c=${at.id}::eq::${alias}`;
                        observables.push(this.search.retrieveData(id.instanceName, id.datasetName, query));
                    }
                }
                return merge(...observables);
            })
        );
    }
}
