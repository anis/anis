/*
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { cold, } from 'jasmine-marbles';
import { of } from 'rxjs';

import { AliasesState, AliasesStore, GetterAliases } from './aliases.store';
import { QueryMakerService } from './query-maker.service';

describe('AliasesStore', () => {
    let store: AliasesStore;

    const state: AliasesState = {
        aliases: ['coucou'],
        areAliasesLoaded: false,
    };

    let getter: GetterAliases = {
        alias: 'coucou',
        attributesToSearch: [1, 2, 3],
        instanceName: 'instance',
        datasetName: 'dataset',
    };

    const query = {
        aliasesRetriever: jest.fn(),
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                AliasesStore,
                { provide: QueryMakerService, useValue: query },
            ],
        });

        store = TestBed.inject(AliasesStore);
        store.setState(state);
    });

    it('should be created', () => {
        expect(store).toBeTruthy();
    });

    it('should implement two pure selectors aliases$ and areAliasesLoaded$', () => {
        const alias = store.aliases$;
        expect(alias).toBeObservable(cold('a', { a: ['coucou'] }));

        const bool_alias = store.areAliasesLoaded$;
        expect(bool_alias).toBeObservable(cold('a', { a: false }));
    });

    it('should implement one pure getter getAliases that return a new state', () => {
        const spy = jest.spyOn(query, 'aliasesRetriever').mockReturnValue(
            of([{ coucou: 'coucou', coconut: 'coconut' }])
        );

        store.getAliases(of(getter));
        expect(spy).toHaveBeenCalled();

        const alias = store.aliases$;
        expect(alias).toBeObservable(cold('a', { a: ['coucou', 'coconut'] }));

        const bool_alias = store.areAliasesLoaded$;
        expect(bool_alias).toBeObservable(cold('a', { a: true }));
    });
});
