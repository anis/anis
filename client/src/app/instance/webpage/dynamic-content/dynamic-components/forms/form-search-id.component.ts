/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ToastrService } from 'ngx-toastr';

import * as searchActions from 'src/app/instance/store/actions/search.actions';
import { SearchService } from 'src/app/instance/store/services/search.service';
import { AbstractFormSampleComponent } from './abstract-form-sample.component';

@Component({
    selector: 'app-form-search-id',
    templateUrl: './form-search-id.component.html',
    styleUrls: ['./form-search-id.component.scss'],
})
export class FormSearchIDComponent extends AbstractFormSampleComponent<
    NgForm,
    void
> {
    constructor(
        private router: Router,
        private search: SearchService,
        private toastr: ToastrService,
        private store: Store<{ }>,
    ) {
        super();
    }

    @Input() criteriaNum: number = null;

    private createAttributesquery(queryResult: any): string {
        const size = Object.keys(queryResult).length;
        let a = '';
        let concat = '';
        for (let i = 1; i <= size; ++i) {
            concat = i === size ? i.toString() : i.toString().concat(';');
            a = a.concat(concat);
        }
        return a;
    }

    submit(f: NgForm): void {
        const trimmedInput = (f.value.inputVal as string).replace(/\s/g, '');
        const query: string = `a=all&c=${this.criteriaNum}::in::${trimmedInput}`;
        this.search
            .retrieveData(this.instanceName, this.datasetName, query)
            .subscribe((data) => {
                try {
                    switch (data.length > 0) {
                        case false:
                            this.toastr.error(
                                'Target ID does not exist',
                                'Navigation error'
                            );
                            break;
                        case true:
                            // User come back to home for a new search: reload initial state
                            this.store.dispatch(searchActions.resetSearch());
                            const a = this.createAttributesquery(data[0])
                            this.router.navigate(
                                [
                                    `${this.instanceName}/search/result/${this.datasetName}`,
                                ],
                                {
                                    queryParams: {
                                        a: a,
                                        c: `${this.criteriaNum}::in::${trimmedInput}`,
                                    },
                                }
                            );
                            break;
                    }
                } catch (err) {
                    console.error(err);
                }
            });
    }
}
