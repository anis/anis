/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { SearchService } from 'src/app/instance/store/services/search.service';
import { AbstractFormSampleComponent } from './abstract-form-sample.component';

/**
 * @class
 * @description Implement a one-liner form to go directly to a detail page
 * @extends `AbstractFormSampleComponent<NgForm, void>`
 */
@Component({
    selector: 'app-form-goto',
    templateUrl: './form-goto.component.html',
})
export class FormGoToComponent extends AbstractFormSampleComponent<
    NgForm,
    void
> {
    constructor(
        private router: Router,
        private search: SearchService,
        private toastr: ToastrService
    ) {
        super();
    }

    /**
     * @method Submit the form with a template driven model that shall receive an ID number
     * @throws If the ID given is not unique, an error is thrown to the console
     *
     * @description Use the service SearchService injected privately to go directly to the detail page afterwards
     */
    submit(f: NgForm): void {
        const query: string = `a=count&c=1::eq::${f.value.inputVal}`;
        this.search.retrieveDataLength(this.instanceName, this.datasetName, query).subscribe((data) => {
            try {
                switch (data[0].nb) {
                    case 0:
                        this.toastr.error(
                            'Target ID does not exist',
                            'Navigation error'
                        );
                        break;
                    case 1:
                        const route: string = `/${this.instanceName}/search/detail/${this.datasetName}/${f.value.inputVal}`;
                        this.router.navigate([route]);
                        break;
                    default:
                        throw new Error(
                            'Problem occured during the navigation: \n The ID given to go to the detail page seems to be not unique'
                        );
                }
            } catch (err) {
                console.error(err);
            }
        });
    }
}
