/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { FormSearchIDComponent } from './form-search-id.component';
import { FormsModule, NgForm } from '@angular/forms';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { SearchService } from 'src/app/instance/store/services/search.service';
import { of } from 'rxjs';

jest.mock('src/app/instance/store/services/search.service');

describe('FormSearchIDComponent', () => {
    let component: FormSearchIDComponent;
    let fixture: ComponentFixture<FormSearchIDComponent>;
    let store: MockStore;
    let search: SearchService;

    const router = {
        navigate: jest.fn(),
    };

    const toastr = {
        error: jest.fn(),
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [FormSearchIDComponent],
            imports: [FormsModule, ToastrModule],
            providers: [
                { provide: Router, useValue: router },
                { provide: ToastrService, useValue: toastr },
                SearchService,
                provideMockStore({}),
            ],
        });
        fixture = TestBed.createComponent(FormSearchIDComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        store = TestBed.inject(MockStore);
        search = TestBed.inject(SearchService);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should create Attributes list properly', () => {
        const queryResult = {
            coucou: 'allo',
            coconut: 'hello',
            catchup: 'hej',
        };
        const result = (component as any).createAttributesquery(queryResult);

        expect(result).toEqual('1;2;3');
    });

    describe('submit', () => {
        it('should call toastr.error on bad query', () => {
            const f: NgForm = <NgForm>{ value: { inputVal: 'POI-3451.01' } };
            component.instanceName = 'coconut';
            component.datasetName = 'data';
            component.criteriaNum = 2;
            search.retrieveData = jest.fn(() => {
                return of([]);
            });

            const spyError = jest.spyOn(toastr, 'error');
            const spySearch = jest.spyOn(search, 'retrieveData');

            component.submit(f);
            expect(spySearch).toHaveBeenCalledWith(
                'coconut',
                'data',
                'a=all&c=2::in::POI-3451.01'
            );
            expect(spyError).toHaveBeenCalled();
        });

        it('should navigate on good query', () => {
            const f: NgForm = <NgForm>{ value: { inputVal: 'POI-3451.01' } };
            component.instanceName = 'coconut';
            component.datasetName = 'data';
            component.criteriaNum = 2;
            search.retrieveData = jest.fn(() => {
                return of([{ hej: 1, salute: 3 }]);
            });

            const spyStore = jest.spyOn(store, 'dispatch');
            const spyRouter = jest.spyOn(router, 'navigate');

            component.submit(f);
            expect(spyStore).toHaveBeenCalled();
            expect(spyRouter).toHaveBeenCalled();
        });

        it('should throw Error on problems', () => {
            const f: NgForm = <NgForm>{ value: { inputVal: 'POI-3451.01' } };
            component.instanceName = 'coconut';
            component.datasetName = 'data';
            component.criteriaNum = 2;
            search.retrieveData = jest.fn(() => {
                return of(null);
            });

            const spyConsole = jest
                .spyOn(console, 'error')
                .mockImplementation();
            component.submit(f);
            expect(spyConsole).toHaveBeenCalled();
        });
    });
});
