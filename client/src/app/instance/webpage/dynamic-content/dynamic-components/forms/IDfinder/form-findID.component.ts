/*
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

import { AttributeService } from 'src/app/metamodel/services/attribute.service';
import { AbstractFormSampleComponent } from '../abstract-form-sample.component';
import { AliasesStore, GetterAliases } from './aliases.store';
import { QueryMakerService } from './query-maker.service';

/**
 * Component to find aliases or IDs for a data from a dataset or a view (e.g. a star name).
 *
 * @todo Make this component available for "all".
 * @class
 * @extends AbstractFormSampleComponent
 */
@Component({
    selector: 'app-form-findID',
    templateUrl: './form-findID.component.html',
    providers: [AliasesStore, QueryMakerService, AttributeService],
})
export class FormFindIDComponent extends AbstractFormSampleComponent<
    NgForm,
    void
> {
    @Input() attributesToSearch: number[] = null;

    aliases$: Observable<string[]> = this.store.aliases$;
    areAliasesLoaded$: Observable<boolean> = this.store.areAliasesLoaded$;

    constructor(private readonly store: AliasesStore) {
        super();
        this.store.setState({ aliases: [], areAliasesLoaded: false });
    }

    submit(f: NgForm): void {
        const getterObject: GetterAliases = {
            alias: f.value.alias,
            attributesToSearch: this.attributesToSearch,
            instanceName: this.instanceName,
            datasetName: this.datasetName,
        };
        this.store.getAliases(getterObject);
    }
}
