/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatasetSampleComponent } from './dataset-sample.component';
import { FormGoToComponent } from './forms/form-goto.component';
import { FormFindIDComponent } from './forms/IDfinder/form-findID.component';
import { FormSearchIDComponent } from './forms/form-search-id.component';
import { TusUploaderComponent } from './tus-uploader/tus-uploader.component';
import { TusUploaderProgressComponent } from './tus-uploader/tus-progress/tus-uploader-progress.component';

export const dynamicComponents = [
    DatasetSampleComponent,
    FormGoToComponent,
    FormFindIDComponent,
    FormSearchIDComponent,
    TusUploaderComponent,
    TusUploaderProgressComponent,
];
