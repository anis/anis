/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from "@angular/core/testing";
import { MockStore, provideMockStore } from "@ngrx/store/testing";
import { cold } from "jasmine-marbles";

import { WebpageTitleResolver } from "./webpage-title.resolver";
import { Instance, Webpage } from "src/app/metamodel/models";
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as menuItemActions from 'src/app/metamodel/actions/menu-item.actions';
import * as menuItemSelector from 'src/app/metamodel/selectors/menu-item.selector';

describe('Instance][SearchMultiple] WebpageTitleResolver', () => {
    let webpageTitleResolver: WebpageTitleResolver;
    let store: MockStore;
    let mockWebpageSelectorSelectWebpageListIsLoaded;
    let mockInstanceSelectorSelectInstanceByRouteName;
    let mockWebpageSelectorSelectWebpageByRouteName;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                provideMockStore({}),
            ]
        })

        store = TestBed.inject(MockStore);
        webpageTitleResolver = TestBed.inject(WebpageTitleResolver);
    });

    it('should be created', () => {
        expect(webpageTitleResolver).toBeTruthy();
    });

    it('#resolve() should dispatch loadMenuItemList action if menuItemListIsLoaded is false', () => {
        mockWebpageSelectorSelectWebpageListIsLoaded = store.overrideSelector(menuItemSelector.selectMenuItemListIsLoaded, false);
        let spy = jest.spyOn(store, 'dispatch');
        let result = cold('a', { a: webpageTitleResolver.resolve(null, null) });
        const expected = cold('a', { a: [] });
        expect(result).toBeObservable(expected);
        expect(spy).toHaveBeenCalledWith(menuItemActions.loadMenuItemList());
    });
    
    it('#resolve() should dispatch loadWebpageList action if webpageListIsLoaded is false', () => {
        let instance: Instance;
        let webpage: Webpage;
        mockWebpageSelectorSelectWebpageListIsLoaded = store.overrideSelector(menuItemSelector.selectMenuItemListIsLoaded, true);
        mockInstanceSelectorSelectInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, { ...instance, label: 'test' });
        mockWebpageSelectorSelectWebpageByRouteName = store.overrideSelector(menuItemSelector.selectWebpageByRouteName, { ...webpage, title: 'test' });
        let result = webpageTitleResolver.resolve(null, null);
        const expected = cold('a', { a: 'test - test' });
        expect(result).toBeObservable(expected);
    });
});
