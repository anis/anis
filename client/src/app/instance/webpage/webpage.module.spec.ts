/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { WebpageModule } from "./webpage.module"

describe('[Instance][Webpage] WebpageModule', () => {
    it('test webPageModule', () => {
        expect(WebpageModule.name).toEqual('WebpageModule');
    });
});
