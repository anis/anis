/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnChanges, SimpleChanges, Input, ChangeDetectionStrategy } from '@angular/core';

import { Instance, Webpage } from 'src/app/metamodel/models';
import { globalParsers } from 'src/app/shared/dynamic-content';
import { componentParsers } from '../dynamic-content';
import { StyleService } from 'src/app/shared/services/style.service';

@Component({
    selector: 'app-webpage-content',
    templateUrl: 'webpage-content.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WebpageComponent implements OnChanges {
    @Input() webpage: Webpage;
    @Input() instance: Instance;

    constructor(private style: StyleService) { }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['webpage'] && changes['webpage'].currentValue && changes['webpage'].currentValue.style_sheet) {
            const webpage = changes['webpage'].currentValue;
            this.style.addCSS(
                webpage.style_sheet.replace(
                    /(\h*\..*{)/g,
                    (match, $1) => `.webpage-${this.instance.name}-${webpage.name} ${$1}`
                ),
                'webpage'
            );
        }
    }

    getParsers() {
        return [
            ...globalParsers,
            ...componentParsers
        ];
    }

    getContext() {
        return {
            instance: this.instance
        };
    }
}
