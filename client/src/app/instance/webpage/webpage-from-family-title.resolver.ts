/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { map, switchMap, skipWhile } from 'rxjs/operators';

import { Store, select } from '@ngrx/store';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as menuItemActions from 'src/app/metamodel/actions/menu-item.actions';
import * as menuItemSelector from 'src/app/metamodel/selectors/menu-item.selector';

@Injectable({
    providedIn: 'root'
})
export class WebpageFromFamilyTitleResolver implements Resolve<string> {
    constructor(private store: Store<{ }>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string | Observable<string> | Promise<string> {
        return this.store.select(menuItemSelector.selectMenuItemListIsLoaded).pipe(
            map(menuItemListIsLoaded => {
                if (!menuItemListIsLoaded) {
                    this.store.dispatch(menuItemActions.loadMenuItemList());
                }
                return menuItemListIsLoaded;
            }),
            skipWhile(menuItemListIsLoaded => !menuItemListIsLoaded),
            switchMap(() => {
                return combineLatest([
                    this.store.pipe(select(instanceSelector.selectInstanceByRouteName)),
                    this.store.pipe(select(menuItemSelector.selectWebpageFromFamilyByRouteName))
                ]).pipe(
                    map(([instance, webpage]) => `${instance.label} - ${webpage.title}`)
                );
            })
        );
    }
}
