/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { TusUploadModule } from 'src/app/tus-upload/tus-upload.module';
import { WebpageRoutingModule, routedComponents } from './webpage-routing.module';
import { DatatableRendererModule } from '../datatable-renderer/datatable-renderer.module';
import { dummiesComponents } from './components';
import { hookParsers, dynamicComponents } from './dynamic-content';

@NgModule({
    imports: [
        SharedModule,
        WebpageRoutingModule,
        DatatableRendererModule,
        TusUploadModule,
    ],
    declarations: [
        routedComponents,
        dummiesComponents,
        dynamicComponents,
    ],
    providers: [
        hookParsers
    ]
})
export class WebpageModule { }
