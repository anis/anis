/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule, inject } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WebpageComponent } from './containers/webpage.component';
import { WebpageFromFamilyComponent } from './containers/webpage-from-family.component';
import { WebpageTitleResolver } from './webpage-title.resolver';
import { WebpageFromFamilyTitleResolver } from './webpage-from-family-title.resolver';
import { WebpageRedirectService } from './webpage-redirect.service';

const routes: Routes = [
    { path: '', pathMatch: 'full', canActivate: [ () => inject(WebpageRedirectService).redirect() ], children: [] },
    { path: ':name', component: WebpageComponent, title: WebpageTitleResolver },
    { path: ':fname/:name', component: WebpageFromFamilyComponent, title: WebpageFromFamilyTitleResolver }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class WebpageRoutingModule { }

export const routedComponents = [
    WebpageComponent,
    WebpageFromFamilyComponent
];
