import { Injectable } from '@angular/core';
import { Router, UrlTree } from '@angular/router';

import { Store, select } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { switchMap, map, skipWhile } from 'rxjs/operators';

import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';

@Injectable({
    providedIn: 'root',
})
export class WebpageRedirectService {
    constructor(
        private store: Store<{ }>,
        private router: Router
    ) {}

    redirect(): Observable<boolean | UrlTree> {
        return this.store.pipe(select(instanceSelector.selectInstanceListIsLoaded)).pipe(
            skipWhile(instanceListIsLoaded => !instanceListIsLoaded),
            switchMap(() =>  {
                return combineLatest([
                    this.store.pipe(select(instanceSelector.selectInstanceByRouteName))
                ]).pipe(
                    map(([instance]) => {
                        if (instance.default_redirect) {
                            return this.router.parseUrl(`${instance.name}${instance.default_redirect}`);
                        } else {
                            return true;
                        }
                    })
                );
            })
        );
    }
}
