/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Instance, Webpage } from 'src/app/metamodel/models';
import * as menuItemSelector from 'src/app/metamodel/selectors/menu-item.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';

@Component({
    selector: 'app-webpage-from-family',
    templateUrl: 'webpage-from-family.component.html'
})
export class WebpageFromFamilyComponent {
    public instance: Observable<Instance>;
    public menuItemListIsLoading: Observable<boolean>;
    public menuItemListIsLoaded: Observable<boolean>;
    public webpage: Observable<Webpage>;

    constructor(private store: Store<{ }>) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.menuItemListIsLoading = store.select(menuItemSelector.selectMenuItemListIsLoading);
        this.menuItemListIsLoaded = store.select(menuItemSelector.selectMenuItemListIsLoaded);
        this.webpage = this.store.select(menuItemSelector.selectWebpageFromFamilyByRouteName);
    }
}
