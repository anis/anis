/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Router } from '@angular/router';

import { WebpageRedirectService } from './webpage-redirect.service';
import { AppRoutingModule } from 'src/app/app-routing.module';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { INSTANCE } from 'src/test-data';

describe('[webpage] WebpageRedirectService', () => {
    let store: MockStore;
    let router: Router;
    let navigateSpy: jest.SpyInstance;
    let webpageRedirectService: WebpageRedirectService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [AppRoutingModule],
            providers: [provideMockStore({})],
        });
        store = TestBed.inject(MockStore);
        router = TestBed.inject(Router);
        navigateSpy = jest.spyOn(router, 'createUrlTree');
        webpageRedirectService = TestBed.inject(WebpageRedirectService);
    });

    it('should create webpageRedirectService', () => { 
        expect(webpageRedirectService).toBeTruthy();
    });

    it('redirect() should return instance default redirect URL', () => {
        store.overrideSelector(
            instanceSelector.selectInstanceListIsLoaded,
            true
        );
        store.overrideSelector(instanceSelector.selectInstanceByRouteName, {
            ...INSTANCE
        });

        webpageRedirectService.redirect().subscribe({
            next: result => {
                expect(navigateSpy).toHaveBeenCalledWith(['default/home']);
            }
        });
    });

    it('redirect() should return true if default redirect URL is not defined', () => {
        store.overrideSelector(
            instanceSelector.selectInstanceListIsLoaded,
            true
        );
        store.overrideSelector(instanceSelector.selectInstanceByRouteName, {
            ...INSTANCE,
            default_redirect: null
        });

        webpageRedirectService.redirect().subscribe({
            next: result => {
                expect(result).toBeTruthy();
            }
        });
    });
});
