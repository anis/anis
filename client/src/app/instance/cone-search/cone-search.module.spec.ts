/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ConeSearchModule } from './cone-search.module';

describe('[Instance][ConeSearch] ConeSearchModule', () => {
    it('should test cone-search module', () => {
        expect(ConeSearchModule.name).toBe('ConeSearchModule');
    })
});