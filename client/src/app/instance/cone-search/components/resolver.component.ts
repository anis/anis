/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { ConeSearch } from 'src/app/instance/store/models';

@Component({
    selector: 'app-resolver',
    templateUrl: 'resolver.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResolverComponent {
    @Input() coneSearch: ConeSearch;
    @Input() resolverIsLoading: boolean;
    @Input() resolverIsLoaded: boolean;
    @Output() retrieveCoordinates: EventEmitter<string> = new EventEmitter();

    public form = new UntypedFormGroup({
        name: new UntypedFormControl('', [Validators.required])
    });

    /**
     * Emits event to retrieve coordinates.
     *
     * @fires EventEmitter<string>
     */
    submit(): void {
        this.retrieveCoordinates.emit(this.form.controls['name'].value);
        this.form.controls['name'].setValue('');
    }
}
