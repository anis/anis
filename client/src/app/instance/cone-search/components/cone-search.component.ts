/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnChanges, SimpleChanges, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { debounceTime, Subscription } from 'rxjs';

import { ConeSearch } from 'src/app/instance/store/models';
import { nanValidator, rangeValidator } from '../validators';

@Component({
    selector: 'app-cone-search',
    templateUrl: 'cone-search.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConeSearchComponent implements OnChanges, OnInit, OnDestroy {
    @Input() coneSearch: ConeSearch;
    @Input() defaultRadius: number;
    @Input() defaultRaDecUnit: string;
    @Input() resolverEnabled: boolean;
    @Input() resolverIsLoading: boolean;
    @Input() resolverIsLoaded: boolean;
    @Output() retrieveCoordinates: EventEmitter<string> = new EventEmitter();
    @Output() emitAdd: EventEmitter<{}> = new EventEmitter<{}>();

    private formValueChangesSubscription: Subscription;

    public form = new UntypedFormGroup({
        ra: new UntypedFormControl(null, [Validators.required, nanValidator, rangeValidator(0, 360, 'RA')]),
        ra_hms: new UntypedFormGroup({
            h: new UntypedFormControl(null, [Validators.required, nanValidator, rangeValidator(0, 24, 'Hours')]),
            m: new UntypedFormControl(null, [Validators.required, nanValidator, rangeValidator(0, 60, 'Minutes')]),
            s: new UntypedFormControl(null, [Validators.required, nanValidator, rangeValidator(0, 60, 'Seconds')])
        }),
        dec: new UntypedFormControl(null, [Validators.required, nanValidator, rangeValidator(-90, 90, 'DEC')]),
        dec_dms: new UntypedFormGroup({
            d: new UntypedFormControl(null, [Validators.required, nanValidator, rangeValidator(-90, 90, 'Degree')]),
            m: new UntypedFormControl(null, [Validators.required, nanValidator, rangeValidator(0, 60, 'Minutes')]),
            s: new UntypedFormControl(null, [Validators.required, nanValidator, rangeValidator(0, 60, 'Seconds')])
        }),
        radius: new UntypedFormControl(2, [Validators.required, rangeValidator(0, 150, 'Radius')])
    });

    public unit = 'degree';

    ngOnInit() {
        this.formValueChangesSubscription = this.form.valueChanges.pipe(
            debounceTime(300),
        )
        .subscribe(() => {
            if (this.form.valid) {
                this.emitAdd.emit();
            }
        });

        if (!this.coneSearch && this.defaultRadius) {
            this.form.controls['radius'].setValue(this.defaultRadius);
        }

        if (this.defaultRaDecUnit) {
            this.unit = this.defaultRaDecUnit;
        }
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['coneSearch'] && !changes['coneSearch'].currentValue) {
            if (this.unit == 'degree') {
                this.form.controls['ra'].enable();
                this.form.controls['dec'].enable();
                this.form.controls['radius'].enable();
            } else {
                this.form.controls['ra_hms'].enable();
                this.form.controls['dec_dms'].enable();
            }
            this.form.reset();
            this.form.controls['radius'].setValue(2);
        }

        if (changes['coneSearch'] && changes['coneSearch'].currentValue) {
            const radius = changes['coneSearch'].currentValue.radius;
            this.form.controls['radius'].setValue(radius);
        }
    }

    /**
     * Returns cone-search from form.
     *
     * @return ConeSearch
     */
    getConeSearch(): ConeSearch {
        return {
            ra: this.form.controls['ra'].value,
            dec: this.form.controls['dec'].value,
            radius: this.form.controls['radius'].value
        };
    }

    ngOnDestroy() {
        this.formValueChangesSubscription.unsubscribe();
    }
}
