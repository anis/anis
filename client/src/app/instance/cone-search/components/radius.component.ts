/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';

@Component({
    selector: 'app-radius',
    templateUrl: 'radius.component.html',
    styleUrls: ['radius.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RadiusComponent {
    @Input() form: UntypedFormGroup;
}
