/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Attribute } from "src/app/metamodel/models";
import { Criterion } from "../../store/models";
import * as fromStoreModels from "../../store/models";
import { CriteriaListParametersComponent } from "./criteria-list-parameters.component"

describe('[instance][search][components] CriteriaListParametersComponent', () => {
    let component: CriteriaListParametersComponent;
    let fixture: ComponentFixture<CriteriaListParametersComponent>;
    TestBed.configureTestingModule({
        declarations: [
            CriteriaListParametersComponent
        ]
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(CriteriaListParametersComponent);
        component = fixture.componentInstance;
    });

    it('component should be created', () => {
        expect(component).toBeTruthy();
    });

    it('getAttribute(2) should return attribute with id  2', () => {
        let attribute: Attribute;
        component.attributeList = [{ ...attribute, id: 1 }, { ...attribute, id: 2 }];
        let result = component.getAttribute(2);
        expect(result.id).toEqual(2);
    });

    it('printCriterion(criterion: Criterion) should call getPrettyCriterion(criterion)', () => {
        let criterion: Criterion = { id: 1, type: 'test' };
        let spy = jest.spyOn(fromStoreModels, 'getPrettyCriterion');
        component.printCriterion(criterion);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(criterion);
    });
});
