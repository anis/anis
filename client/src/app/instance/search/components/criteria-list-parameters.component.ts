/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

import { Criterion, getPrettyCriterion } from 'src/app/instance/store/models';
import { Attribute } from 'src/app/metamodel/models';

@Component({
    selector: 'app-criteria-list-parameters',
    templateUrl: 'criteria-list-parameters.component.html',
    styleUrls: ['criteria-list-parameters.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CriteriaListParametersComponent {
    @Input() readonly: boolean = true;
    @Input() criteriaList: Criterion[];
    @Input() attributeList: Attribute[];
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    /**
     * Returns attribute for the given attribute ID.
     *
     * @param  {number} id - The attribute ID.
     *
     * @return Attribute
     */
    getAttribute(id: number): Attribute {
        return this.attributeList.find(attribute => attribute.id === id);
    }

    /**
     * Returns pretty print of the given criterion.
     *
     * @param  {Criterion} criterion - The criterion.
     *
     * @return string
     */
    printCriterion(criterion: Criterion): string {
        return getPrettyCriterion(criterion);
    }
}
