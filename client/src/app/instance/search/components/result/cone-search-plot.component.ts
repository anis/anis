/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy, ViewEncapsulation, OnInit, Output, EventEmitter, SimpleChanges } from '@angular/core';

import * as d3 from 'd3';

import { Dataset } from 'src/app/metamodel/models';
import { ConeSearch } from 'src/app/instance/store/models';

/**
 * @class
 * @classdesc Cone-search plot component.
 */
@Component({
    selector: 'app-cone-search-plot',
    templateUrl: 'cone-search-plot.component.html',
    styleUrls: [ 'cone-search-plot.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush,
    encapsulation: ViewEncapsulation.None
})
export class ConeSearchPlotComponent implements OnInit {
    @Input() coneSearch: ConeSearch;
    @Input() dataset: Dataset;
    @Input() data: {id: any, x: number, y: number}[];
    @Input() backgroundHref: string;
    @Output() selectId: EventEmitter<any> = new EventEmitter();

    // Interactive variables intialisation
    margin = { top: 50, right: 50, bottom: 50 , left: 50 };
    width = 500;
    height = 500;
    degTorad = 0.0174532925;
    image;
    x: d3.ScaleLinear<number, number>;
    y: d3.ScaleLinear<number, number>;

    ngOnInit(): void {
        this.coneSearchPlot();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['backgroundHref'] && !changes['backgroundHref'].firstChange) {
            d3.select('#plot svg').remove();
            this.coneSearchPlot();
        }
    }

    coneSearchPlot(): void {
        // Init SVG
        const svg = d3.select('#plot').append('svg')
            .attr('id', 'plot')
            .attr('width', this.width + this.margin.left + this.margin.right)
            .attr('height', this.height + this.margin.top + this.margin.bottom)
            .append('g')
            .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

        // Set Domain  RA->DEG, DEC->DEG, RADIUS->ARCSEC
        const coneSearchDomain = this.getConeSearchDomain();
        this.x = d3.scaleLinear().range([this.width, 0]).domain([coneSearchDomain.raMin, coneSearchDomain.raMax]);
        this.y = d3.scaleLinear().range([this.height, 0]).domain([coneSearchDomain.decMin, coneSearchDomain.decMax]);

        // Background image
        this.image = svg.append('image');
        this.image.attr('xlink:href', this.backgroundHref)
            .attr('width', this.width)
            .attr('height', this.height)
            .attr('class', 'image');

        // Add X axe
        svg.append("g")
            .attr("transform", "translate(0," + this.height + ")")
            .call(d3.axisBottom(this.x));

        // Add Y axe
        svg.append("g")
            .call(d3.axisLeft(this.y));

        svg.selectAll("circle")
            .data(this.data)
            .enter()
            .append("rect")
            .attr('class','star')
            .attr("x", d => this.x(d.x) - 3)
            .attr("y", d => this.y(d.y) - 3)
            .attr('width', 6)
            .attr('height', 6)
            .on('mouseover', function (event,d) {
                const tooltip = d3.select('body')
                    .append('div')
                    .attr('class', 'tooltip')
                    .style('opacity', 1);
                tooltip.html('Ra: ' + d.x + '<br> Dec: ' + d.y)
                    .style('left', (event.pageX + 10) + 'px')
                    .style('top', (event.pageY - 5) + 'px');
                d3.select(this).style("cursor", "pointer"); 
            })
            .on('mouseout', function() {
                d3.select('.tooltip').remove();
                d3.select(this).style("cursor", "default"); 
            })
            .on('click', function(event, d) {
                selectId(d.id);
                d3.select('.star-selected').attr('class', 'star');
                d3.select(this).attr('class', 'star star-selected');
            });

        const selectId = (id: any) => {
            this.selectId.emit(id);
        }
    }

    private getConeSearchDomain() {
        const radiusInDegrees = this.coneSearch.radius / 3600;

        const decMin = this.coneSearch.dec - radiusInDegrees;
        const decMax = this.coneSearch.dec + radiusInDegrees;

        const raCorrectedRadius = radiusInDegrees / Math.cos(this.degTorad * (Math.abs(this.coneSearch.dec) + radiusInDegrees));
        const raMin = this.coneSearch.ra - raCorrectedRadius;
        const raMax = this.coneSearch.ra + raCorrectedRadius;
        
        return { raMin, raMax, decMin, decMax };
    }
}