/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing'
import { ConeSearchConfig } from 'src/app/metamodel/models';
import { ConeSearchImageComponent } from './cone-search-image.component';

import { DATASET, ATTRIBUTE_LIST } from 'src/test-data';

describe('[instance][search][components][result] ConeSearchImageComponent', () => {
    let component: ConeSearchImageComponent;
    let fixture: ComponentFixture<ConeSearchImageComponent>;
    let coneSearchConfig: ConeSearchConfig;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConeSearchImageComponent
            ]
        })
        fixture = TestBed.createComponent(ConeSearchImageComponent);
        component = fixture.componentInstance;
        component.dataset = { ...DATASET };
        component.attributeList = [ ...ATTRIBUTE_LIST ];
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('getData should return a array with the objet id = 1, x = ra_label, y = dec_label', () => {
        component.coneSearchConfig = { ...coneSearchConfig, column_ra: 2, column_dec: 3 };
        component.data = [
            { label_one: 1, label_two: 1, label_three: 3 },
        ];
        let expected = [{ 'id': 1, 'x': 1, 'y': 3 }];
        expect(component.getData()).toEqual(expected);
    });

    it('should raises closeConeSearchPlotImageOutPut and selectId events', () => {
        let spyOncloseConeSearchPlotImageOutPut = jest.spyOn(component.closeConeSearchPlotImageOutPut, 'emit');
        let spyOnSelectId = jest.spyOn(component.selectId, 'emit');
        component.closeConeSearchPlotImage();
        expect(spyOncloseConeSearchPlotImageOutPut).toHaveBeenCalledWith(false);
        expect(spyOnSelectId).toHaveBeenCalledWith(null);
    });
});
