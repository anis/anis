/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppConfigService } from 'src/app/app-config.service';
import { ImageListResultComponent } from './image-list-result.component';
import { CONE_SEARCH, IMAGE } from 'src/test-data';
import { Image } from 'src/app/metamodel/models';

interface ConeSearchLimits {
    ra_min: number;
    ra_max: number;
    dec_min: number;
    dec_max: number;
}

describe('[Instance][Search][Component][Result] ImageListResultComponent', () => {
    let component: ImageListResultComponent;
    let fixture: ComponentFixture<ImageListResultComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ImageListResultComponent
            ],
            providers: [
                { provide: AppConfigService, useValue: { servicesUrl: 'test' } }
            ]
        });
        fixture = TestBed.createComponent(ImageListResultComponent);
        component = fixture.componentInstance;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('should init properly', () => {
        const spyOnSelectImage = jest.spyOn(component as any, 'selectImagesToShowInList');
        component.coneSearch = CONE_SEARCH;
        component.imageList = [IMAGE, IMAGE];
        component.ngOnInit();
        expect(spyOnSelectImage).toHaveBeenCalled();
        expect(component.componentIsShown).toBeFalsy();

        jest.resetAllMocks();

        const spyOnToBeShown = jest.spyOn(component as any, 'imageIsToBeShown').mockReturnValue(true);
        component.ngOnInit();
        expect(spyOnSelectImage).toHaveBeenCalled();
        expect(spyOnToBeShown).toHaveBeenCalled();
        expect(component.componentIsShown).toBeTruthy();
    });

    it('should call #imageIsToBeShown properly', () => {
        let image: Image = <Image>{ ra_min: 4.5, ra_max: 4.5, dec_min: 4.5, dec_max: 4.3 };
        let limits: ConeSearchLimits = { ra_min: 4.5, ra_max: 4.5, dec_min: 4.5, dec_max: 4.3 };

        let res = (component as any).imageIsToBeShown(image, limits);
        expect(res).toBeTruthy();

        image = <Image>{ ra_min: 1.0, ra_max: 4.5, dec_min: 4.5, dec_max: 4.3 };
        limits = { ra_min: 1.0, ra_max: 4.5, dec_min: 1.0, dec_max: 4.3 };

        res = (component as any).imageIsToBeShown(image, limits);
        expect(res).toBeFalsy();
    });
});
