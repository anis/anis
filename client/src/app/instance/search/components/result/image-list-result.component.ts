/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    Component,
    Input,
    Output,
    EventEmitter,
    ChangeDetectionStrategy,
    OnInit,
} from '@angular/core';

import { Instance, Dataset, Image } from 'src/app/metamodel/models';
import { ConeSearch } from 'src/app/instance/store/models';

/**
 * Interface for ConeSearchLimits
 *
 * @interface ConeSearchLimits
 * @param ra_min {number} Minimal right ascension
 * @param ra_max {number} Maximal right ascension
 * @param dec_min {number} Minimal declination
 * @param dec_max {number} Maximal declination
 */
interface ConeSearchLimits {
    ra_min: number;
    ra_max: number;
    dec_min: number;
    dec_max: number;
}

@Component({
    selector: 'app-image-list-result',
    templateUrl: 'image-list-result.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageListResultComponent implements OnInit {
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() coneSearch: ConeSearch;
    @Input() imageList: Image[];
    @Input() imageListIsLoading: boolean;
    @Input() imageListIsLoaded: boolean;
    @Input() sampRegistered: boolean;
    @Output() broadcastImage: EventEmitter<string> = new EventEmitter();
    @Output() downloadFile: EventEmitter<{ url: string; filename: string }> =
        new EventEmitter();
    @Output() emitBackGroundHref: EventEmitter<string> = new EventEmitter();
    @Output() openPlotImage: EventEmitter<boolean> = new EventEmitter();

    /**
     * @memberof ImageListResultComponent
     * @property {Image[]} The array of images to show in the component.
     */
    imageListToShow: Image[] = [];
    /**
     * @memberof ImageListResultComponent
     * @property {boolean} If the array is to be shown or not. The latter is
     * set to true if `this.imageListToShow` is not empty
     */
    componentIsShown: boolean = false;

    ngOnInit() {
        this.selectImagesToShowInList();
        if (this.imageListToShow.length != 0) {
            this.componentIsShown = true;
        }
    }

    /**
     * Compute the search limits of the cone-search as a rectangle.
     * This is an approximation of the sphere. Cone research is assumed tiny.
     * @memberof ImageListResultComponent
     * @method
     * @returns {ConeSearchLimits} The limit of the cone (as a rectangle).
     */
    private computeSearchLimits(): ConeSearchLimits {
        const radiusInDeg = this.coneSearch.radius / 3600;

        let ra_min = this.coneSearch.ra - radiusInDeg;
        let ra_max = this.coneSearch.ra + radiusInDeg;
        let dec_min = this.coneSearch.dec + radiusInDeg;
        let dec_max = this.coneSearch.dec - radiusInDeg;

        return {
            ra_min: ra_min,
            ra_max: ra_max,
            dec_min: dec_min,
            dec_max: dec_max,
        };
    }

    /**
     * Create the array of images to show in the component. All images that
     * overlap the cone-search limits are shown.
     * If the previous array is non-empty, the component is shown.
     * @memberof ImageListResultComponent
     * @method
     */
    private selectImagesToShowInList() {
        const limits = this.computeSearchLimits();
        for (let image of this.imageList) {
            if (this.imageIsToBeShown(image, limits)) {
                this.imageListToShow.push(image);
            }
        }
    }

    /**
     * Compute effectively if an image overlap the cone-search.
     * Assuming the two are rectangles (approximation), first verify one of the
     * two is not on the left of the other. Then verify that one of the tow is
     * not above.
     * If not, then return true.
     * @memberof ImageListResultComponent
     * @method
     * @param image { Image } The image to check on the overlap.
     * @param limits { ConeSearchLimits } The limits of the cone-search for the component.
     */
    private imageIsToBeShown(image: Image, limits: ConeSearchLimits) {
        if (image.ra_min > limits.ra_max || limits.ra_min > image.ra_max) {
            return false;
        }

        if (image.dec_max > limits.dec_min || limits.dec_max > image.dec_min) {
            return false;
        }

        return true;
    }
}
