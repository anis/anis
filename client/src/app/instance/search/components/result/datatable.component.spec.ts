/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Component, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { DatatableComponent } from './datatable.component';
import { Pagination, PaginationOrder, SearchQueryParams } from 'src/app/instance/store/models';
import {
    DetailLinkRendererConfig,
    DownloadRendererConfig,
    ImageRendererConfig,
    LinkRendererConfig,
    RendererConfig
} from 'src/app/metamodel/models/renderers';
import { ATTRIBUTE, ATTRIBUTE_LIST, DATASET } from 'src/test-data';

describe('[Instance][Search][Component][Result] DatatableComponent', () => {
    @Component({ selector: 'app-spinner', template: '' })
    class SpinnerStubComponent { }

    @Component({ selector: 'app-detail-renderer', template: '' })
    class DetailRendererStubComponent {
        @Input() value: string | number;
        @Input() datasetName: string;
        @Input() instanceSelected: string;
        @Input() queryParams: SearchQueryParams;
        @Input() config: DetailLinkRendererConfig;
    }

    @Component({ selector: 'app-link-renderer', template: '' })
    class LinkRendererStubComponent {
        @Input() value: string | number;
        @Input() datasetName: string;
        @Input() config: LinkRendererConfig;
    }

    @Component({ selector: 'app-download-renderer', template: '' })
    class DownloadRendererStubComponent {
        @Input() value: string;
        @Input() datasetName: string;
        @Input() datasetPublic: boolean;
        @Input() config: DownloadRendererConfig;
    }

    @Component({ selector: 'app-image-renderer', template: '' })
    class ImageRendererStubComponent {
        @Input() value: string | number;
        @Input() datasetName: string;
        @Input() config: ImageRendererConfig;
    }

    @Component({ selector: 'app-json-renderer', template: '' })
    class JsonRendererStubComponent {
        @Input() value: string | number;
        @Input() attributeLabel: string;
        @Input() config: RendererConfig;
    }

    @Component({ selector: 'pagination', template: '' })
    class PaginationStubComponent {
        @Input() totalItems: number;
        @Input() boundaryLinks: boolean;
        @Input() rotate: boolean;
        @Input() maxSize: number;
        @Input() itemsPerPage: number;
    }

    let component: DatatableComponent;
    let fixture: ComponentFixture<DatatableComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatatableComponent,
                SpinnerStubComponent,
                DetailRendererStubComponent,
                LinkRendererStubComponent,
                DownloadRendererStubComponent,
                ImageRendererStubComponent,
                JsonRendererStubComponent,
                PaginationStubComponent
            ],
            imports: [FormsModule]
        });
        fixture = TestBed.createComponent(DatatableComponent);
        component = fixture.componentInstance;
        component.dataset = { ...DATASET };
        component.attributeList = [ ...ATTRIBUTE_LIST ];
        component.pagination = {
            dname: component.dataset.name,
            page: 1,
            nbItems: 10,
            sortedCol: component.dataset.default_order_by,
            order: component.dataset.default_order_by_direction
        };
    }));

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#ngOnInit() should init sortedCol value and raise retrieveData event ', (done) => {
        const spy = jest.spyOn(component.retrieveData, 'emit');
        component.ngOnInit();
        Promise.resolve(null).then(function () {
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith({
                dname: component.dataset.name,
                page: 1,
                nbItems: 10,
                sortedCol: component.dataset.default_order_by,
                order: component.dataset.default_order_by_direction
            });
            done();
        });
    });

    // it('#refreshData() should init sortedCol value and raise retrieveData event ', (done) => {
    //     const spy = jest.spyOn(component.retrieveData, 'emit');
    //     const expectedPagination: Pagination = {
    //         dname: 'my-dataset',
    //         page: 1,
    //         nbItems: 10,
    //         sortedCol: 1,
    //         order: PaginationOrder.a
    //     }
    //     component.refreshData();
    //     Promise.resolve(null).then(function () {
    //         expect(spy).toHaveBeenCalledTimes(1);
    //         expect(spy).toHaveBeenCalledWith(expectedPagination);
    //         done();
    //     });
    // });

    it('#getRendererConfig() should return attribute renderer configuration', () => {
        let attribute = { ...ATTRIBUTE };
        expect(component.getRendererConfig(attribute)).toBeNull();
        const detailLinkRendererConfig: DetailLinkRendererConfig = {
            id: 'renderer-config',
            display: 'display',
            component: 'detail'
        };
        attribute.renderer = 'detail-link';
        attribute.renderer_config = detailLinkRendererConfig;
        expect(component.getRendererConfig(attribute)).toEqual(detailLinkRendererConfig);
        const linkRendererConfig: LinkRendererConfig = {
            id: 'renderer-config',
            href: 'href',
            display: 'display',
            text: 'text',
            icon: 'icon',
            blank: true
        };
        attribute.renderer = 'link';
        attribute.renderer_config = linkRendererConfig;
        expect(component.getRendererConfig(attribute)).toEqual(linkRendererConfig);
        const downloadRendererConfig: DownloadRendererConfig = {
            id: 'renderer-config',
            display: 'display',
            text: 'text',
            icon: 'icon',
            samp: false
        };
        attribute.renderer = 'download';
        attribute.renderer_config = downloadRendererConfig;
        expect(component.getRendererConfig(attribute)).toEqual(downloadRendererConfig);
        const imageRendererConfig: ImageRendererConfig = {
            id: 'renderer-config',
            display: 'display',
            type: 'type',
            hdu_number: null,
            width: 'width',
            height: 'height'
        };
        attribute.renderer = 'image';
        attribute.renderer_config = imageRendererConfig;
        expect(component.getRendererConfig(attribute)).toEqual(imageRendererConfig);
        const jsonRendererConfig: RendererConfig = { id: 'renderer-config' };
        attribute.renderer = 'json';
        attribute.renderer_config = jsonRendererConfig;
        expect(component.getRendererConfig(attribute)).toEqual(jsonRendererConfig);
    });

    it('#getOutputList() should return filtered output list', () => {
        component.outputList = [2]
        expect(component.getOutputList().length).toBe(1);
    });

    it('#toggleSelection(datum) should return added datum to selectedData', () => {
        const datum = { label_one: 123456 };
        component.selectedData = [];
        component.addSelectedData.subscribe((event: any) => expect(event).toBe(123456));
        component.toggleSelection(datum);
    });

    it('#toggleSelection(datum) should return remove datum to selectedData', () => {
        const datum = { label_one: 123456 };
        component.selectedData = [123456];
        component.deleteSelectedData.subscribe((event: any) => expect(event).toBe(123456));
        component.toggleSelection(datum);
    });

    it('#isSelected(datum) should return true datum is selected', () => {
        const datum = { label_one: 123456 };
        component.selectedData = [123456];
        expect(component.isSelected(datum)).toBeTruthy();
    });

    it('#isSelected(datum) should return false datum is not selected', () => {
        const datum = { label_one: 123456 };
        component.selectedData = [];
        expect(component.isSelected(datum)).toBeFalsy();
    });

    it('#changePage() should change page value and raise retrieveData event', () => {
        component.changePage(2);
        expect(component.pagination.page).toBe(2);
    });

    it('#changeNbItems() should change nbItems value and raise retrieveData event', () => {
        const mockEvent: Event = <Event><any>{
            currentTarget: {
                value: 20
            }
        };
        component.changeNbItems(mockEvent);
        expect(component.pagination.nbItems).toBe(20);
    });

    it('#sort() should raise retrieveData event with correct parameters', () => {
        component.sort(2);
        expect(component.pagination.sortedCol).toBe(2);
        expect(component.pagination.order).toBe('a');
        component.sort(2);
        expect(component.pagination.order).toBe('d');
    });

    it('datumSelectedInPlot should return true', () => {
        component.selectId = 1;
        let datum = { label_one: 1 };
        expect(component.datumSelectedInPlot(datum)).toBe(true);
    });

    it('savePaginationIntoSessionStorage', () => {
        const pagination = {
            dname: 'observations',
            page: 5,
            nbItems: 20,
            sortedCol: 5,
            order: 'd'
        };
        component.pagination = pagination;
        component.savePaginationIntoSessionStorage();
        expect(sessionStorage.getItem('datatable-pagination')).toBe(JSON.stringify(pagination));
    });

    it('restorePaginationIntoSessionStorage', () => {
        const pagination = {
            dname: 'observations',
            page: 5,
            nbItems: 20,
            sortedCol: 5,
            order: 'd'
        };
        sessionStorage.setItem('datatable-pagination', JSON.stringify(pagination));
        component.restorePaginationIntoSessionStorage();
        expect(component.pagination.dname).toBe('observations');
    });
});
