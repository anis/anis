/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TemplateRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BsModalService } from 'ngx-bootstrap/modal';

import { AppConfigService } from 'src/app/app-config.service';
import { DownloadResultComponent } from './download-result.component'
import { ATTRIBUTE_LIST } from 'src/test-data';

describe('[Instance][Search][Component][Result] DownloadResultComponent', () => {
    let component: DownloadResultComponent;
    let fixture: ComponentFixture<DownloadResultComponent>;

    const modalRefStub = {
        hide: jest.fn()
    };

    const modalServiceStub = {
        show: jest.fn(() => modalRefStub)
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DownloadResultComponent
            ],
            providers: [
                { provide: BsModalService, useValue: modalServiceStub },
                AppConfigService
            ]
        });
        fixture = TestBed.createComponent(DownloadResultComponent);
        component = fixture.componentInstance;
        component.attributeList = [ ...ATTRIBUTE_LIST ];
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('isArchiveIsAvailable should return true', () => {
        component.outputList = [4];
        expect(component.isArchiveIsAvailable()).toBe(true);
    });

    it('should raises broadcastvotable event', () => {
        let spy = jest.spyOn(component.broadcastVotable, 'emit');
        component.getUrl = jest.fn().mockImplementationOnce(() => 'test.fr')
        component.broadcastResult();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('test.fr');
    });

    it('should raises startTaskCreateArchive event', () => {
        let template: TemplateRef<any> = null;
        component.openModal(template);
        let spy = jest.spyOn(component.startTaskCreateArchive, 'emit');
        component.getQuery = jest.fn().mockImplementationOnce(() => 'test/test')
        component.downloadArchive();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('test/test');
    });
});
