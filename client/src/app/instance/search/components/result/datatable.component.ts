/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges,
} from '@angular/core';

import {
    Instance,
    Attribute,
    Dataset,
    DetailLinkRendererConfig,
    DownloadRendererConfig,
    ImageRendererConfig,
    LinkRendererConfig,
    DesignConfig,
    FormatNumberRendererConfig,
} from 'src/app/metamodel/models';
import {
    Pagination,
    PaginationOrder,
    SearchQueryParams,
} from 'src/app/instance/store/models';

@Component({
    selector: 'app-datatable',
    templateUrl: 'datatable.component.html',
    styleUrls: ['datatable.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DatatableComponent implements OnInit, OnChanges {
    @Input() dataset: Dataset;
    @Input() instance: Instance;
    @Input() designConfig: DesignConfig;
    @Input() attributeList: Attribute[];
    @Input() outputList: number[];
    @Input() queryParams: SearchQueryParams;
    @Input() dataLength: number;
    @Input() data: any[];
    @Input() dataIsLoading: boolean;
    @Input() dataIsLoaded: boolean;
    @Input() selectedData: any[] = [];
    @Input() selectId: any;
    @Input() sampRegistered: boolean;
    @Output() retrieveData: EventEmitter<Pagination> = new EventEmitter();
    @Output() updateOutputList: EventEmitter<number[]> = new EventEmitter();
    @Output() addSelectedData: EventEmitter<number | string> =
        new EventEmitter();
    @Output() deleteSelectedData: EventEmitter<number | string> =
        new EventEmitter();
    @Output() downloadFile: EventEmitter<{ url: string; filename: string }> =
        new EventEmitter();
    @Output() broadcastImage: EventEmitter<string> = new EventEmitter();
    @Output() broadcastVotable: EventEmitter<string> = new EventEmitter();

    public pagination: Pagination;

    ngOnInit(): void {
        // Set pagination values by default
        this.pagination = {
            dname: this.dataset.name,
            page: 1,
            nbItems: 10,
            sortedCol: this.dataset.default_order_by,
            order: this.dataset.default_order_by_direction
        };

        // Retrieve pagination information from session storage if exists
        this.restorePaginationIntoSessionStorage();

        // Update ordered outputList if exists
        if (sessionStorage.getItem('datatable-output-list')) {
            this.updateOutputList.emit(JSON.parse(sessionStorage.getItem('datatable-output-list')));
        }

        // Retrieve data
        this.refreshData();
    }

    ngOnChanges(changes: SimpleChanges) {
        if (changes['dataIsLoaded'] && changes['dataIsLoaded'].currentValue) {
            Promise.resolve(null).then(() => {
                // Query the table
                const table = document.getElementById('datatable');

                let dragSrcEl;
                let outputSrcId: number;
                let clonedOutputList: number[];
                let direction = '';
                const updateOutputList = this.updateOutputList;

                const getOutputList = () => [...this.outputList];

                function handleDragStart(e) {
                    this.style.opacity = '0.4';
                    clonedOutputList = getOutputList();

                    dragSrcEl = this;
                    for (let i = 0; i < items.length; i++) {
                        if (items[i] === this) {
                            outputSrcId = clonedOutputList[i];
                        }
                    }
                }

                function handleDragOver(e) {
                    e.preventDefault();

                    let rect = this.getBoundingClientRect();
                    let posisitionX = e.pageX;

                    let half = rect.x + rect.width / 2;

                    if (posisitionX < half) {
                        this.classList.remove('over-right');
                        this.classList.add('over-left');
                        direction = 'left';
                    } else {
                        this.classList.remove('over-left');
                        this.classList.add('over-right');
                        direction = 'right';
                    }

                    return false;
                }

                function handleDragEnter(e) {}

                function handleDragLeave(e) {
                    this.classList.remove('over-left');
                    this.classList.remove('over-right');
                }

                function handleDragEnd(e) {
                    this.style.opacity = '1';

                    items.forEach(function (item) {
                        item.classList.remove('over-left');
                        item.classList.remove('over-right');
                    });
                }

                function handleDrop(e) {
                    e.stopPropagation(); // stops the browser from redirecting.

                    if (dragSrcEl !== this) {
                        // Remove src attribute ID into cloned output list
                        const index = clonedOutputList.indexOf(outputSrcId);
                        clonedOutputList.splice(index, 1);

                        // Add src attribute ID
                        for (let i = 0; i < items.length; i++) {
                            if (items[i] === this) {
                                clonedOutputList.splice(i, 0, outputSrcId);
                            }
                        }

                        // Dispatch new output list
                        updateOutputList.emit(clonedOutputList);
                        sessionStorage.setItem('datatable-output-list', JSON.stringify(clonedOutputList));
                    }

                    return false;
                }

                let items = table.querySelectorAll('.datatable-title');
                items.forEach(function (item) {
                    item.addEventListener('dragstart', handleDragStart);
                    item.addEventListener('dragover', handleDragOver);
                    item.addEventListener('dragenter', handleDragEnter);
                    item.addEventListener('dragleave', handleDragLeave);
                    item.addEventListener('dragend', handleDragEnd);
                    item.addEventListener('drop', handleDrop);
                });
            });
        }

        if (changes['outputList'] && !changes['outputList'].firstChange) {
            this.refreshData();
        }
    }

    /**
     * Returns renderer configuration for the given attribute.
     *
     * @param  {Attribute} attribute - The attribute.
     */
    getRendererConfig(attribute: Attribute) {
        let config = null;
        switch (attribute.renderer) {
            case 'detail-link':
                config = attribute.renderer_config as DetailLinkRendererConfig;
                break;
            case 'link':
                config = attribute.renderer_config as LinkRendererConfig;
                break;
            case 'download':
                config = attribute.renderer_config as DownloadRendererConfig;
                break;
            case 'image':
                config = attribute.renderer_config as ImageRendererConfig;
                break;
            case 'format':
                config = attribute.renderer_config as FormatNumberRendererConfig;
                break;
            case 'json':
                config = attribute.renderer_config;
                break;
            default:
                config = null;
        }
        return config;
    }

    /**
     * Returns output list from attribute list.
     *
     * @return Attribute[]
     */
    getOutputList(): Attribute[] {
        return this.outputList.map((attributeId) =>
            this.attributeList.find((a) => a.id === attributeId),
        );
    }

    /**
     * Emits events to select or unselect data.
     *
     * @param  {any} datum - The data to select or unselect.
     *
     * @fires EventEmitter<number | string>
     */
    toggleSelection(datum: any): void {
        const attribute = this.attributeList.find(
            (attribute) => attribute.id === this.dataset.primary_key,
        );
        const pk = datum[attribute.label];

        const index = this.selectedData.indexOf(pk);
        if (index > -1) {
            this.deleteSelectedData.emit(pk);
        } else {
            this.addSelectedData.emit(pk);
        }
    }

    /**
     * Checks if data is selected.
     *
     * @param  {any} datum - The data.
     *
     * @return boolean
     */
    isSelected(datum: any): boolean {
        const attribute = this.attributeList.find(
            (attribute) => attribute.id === this.dataset.primary_key,
        );
        const pk = datum[attribute.label];

        if (this.selectedData.indexOf(pk) > -1) {
            return true;
        }
        return false;
    }

    /**
     * Emits event to change datatable page.
     *
     * @param  {number} nb - The page number to access.
     *
     * @fires EventEmitter<Pagination>
     */
    changePage(nb: number): void {
        this.pagination.page = nb;
        this.refreshData();
    }

    /**
     * Emits event to change datatable displayed items.
     *
     * @param  {Event} event - The event that contains number of items to display.
     *
     * @fires EventEmitter<Pagination>
     */
    changeNbItems(event: Event): void {
        // Retrieve value from html element
        const element = event.currentTarget as HTMLInputElement;
        this.pagination.nbItems = +element.value;
        this.pagination.page = 1;

        this.refreshData();
    }

    /**
     * Emits event to change the sorted order and the sorted column of the datatable.
     *
     * @param  {number} id - The id of the column to sort.
     *
     * @fires EventEmitter<Pagination>
     */
    sort(id: number): void {
        if (id === this.pagination.sortedCol) {
            this.pagination.order = this.pagination.order === PaginationOrder.a ? PaginationOrder.d : PaginationOrder.a;
        } else {
            this.pagination.sortedCol = id;
            this.pagination.order = PaginationOrder.a;
        }
        this.pagination.page = 1;

        this.refreshData();
    }

    datumSelectedInPlot(datum: any) {
        let datumSelectedInPlot = false;

        if (this.selectId) {
            const columnId = this.attributeList.find(
                (a) => a.id === this.dataset.primary_key,
            );
            const id = datum[columnId.label];
            if (this.selectId === id) {
                datumSelectedInPlot = true;
            }
        }

        return datumSelectedInPlot;
    }

    refreshData(): void {
        this.savePaginationIntoSessionStorage();
        Promise.resolve(null).then(() => this.retrieveData.emit({
            ...this.pagination
        }));
    }

    savePaginationIntoSessionStorage(): void {
        sessionStorage.setItem('datatable-pagination', JSON.stringify(this.pagination));
    }

    restorePaginationIntoSessionStorage(): void {
        if (sessionStorage.getItem('datatable-pagination')) {
            this.pagination = JSON.parse(sessionStorage.getItem('datatable-pagination'));
        }
    }
}
