/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Instance, Dataset, Image } from 'src/app/metamodel/models';
import { ConeSearch } from 'src/app/instance/store/models';
import { AppConfigService } from 'src/app/app-config.service';
import { getHost } from 'src/app/shared/utils';

@Component({
    selector: 'app-image-display',
    templateUrl: 'image-display.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageDisplayComponent {
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() coneSearch: ConeSearch;
    @Input() image: Image;
    @Input() sampRegistered: boolean;
    @Output() broadcastImage: EventEmitter<string> = new EventEmitter();
    @Output() downloadFile: EventEmitter<{url: string, filename: string}> = new EventEmitter();
    @Output() emitBackGroundHref: EventEmitter<string> = new EventEmitter();
    @Output() openPlotImage: EventEmitter<boolean> = new EventEmitter();

    loading = true;
    error = false;

    constructor(private config: AppConfigService) { }

    onLoad() {
        this.loading = false;
    }

    onError(event) {
        if ((this.instance.public && this.dataset.public) || ((!this.instance.public || !this.dataset.public) && event.target.attributes['src'].value === 'not_found')) {
            this.loading = false;
            this.error = true;
        }
    }

    getHref(image: Image) {
        let href = `${getHost(this.config.servicesUrl)}/fits/fits-cut-to-png/${this.instance.name}/${this.dataset.name}?filename=${image.file_path}`;
        href += `&ra=${this.coneSearch.ra}`;
        href += `&dec=${this.coneSearch.dec}`;
        href += `&radius=${this.coneSearch.radius}`;
        href += `&stretch=${image.stretch}`;
        href += `&pmin=${image.pmin}`;
        href += `&pmax=${image.pmax}`;
        href += `&axes=false`;

        if (image.hdu_number) {
            href += `&hdu_number=${image.hdu_number}`
        }

        return href;
    }

    getFitsCutUrl(image: Image) {
        let url = `${getHost(this.config.servicesUrl)}/fits/fits-cut/${this.instance.name}/${this.dataset.name}?filename=${image.file_path}`;
        url += `&ra=${this.coneSearch.ra}`;
        url += `&dec=${this.coneSearch.dec}`;
        url += `&radius=${this.coneSearch.radius}`;
        return url;
    }

    saveFitsCutFile(event, image: Image) {
        event.preventDefault();

        const url = this.getFitsCutUrl(image);
        const filename = `fits_cut_${this.coneSearch.ra}_${this.coneSearch.dec}_${this.coneSearch.radius}_${image.file_path.substring(image.file_path.lastIndexOf('/') + 1)}`;

        this.downloadFile.emit({url, filename});
    }

    broadcast(image: Image) {
        this.broadcastImage.emit(
            this.getFitsCutUrl(image)
        );
    }

    openConeSearch(backgroundHref: string): void {
        this.emitBackGroundHref.emit(backgroundHref);
        this.openPlotImage.emit(true);
    }
}
