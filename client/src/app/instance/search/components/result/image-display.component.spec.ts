/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppConfigService } from 'src/app/app-config.service';
import { ImageDisplayComponent } from './image-display.component';

import { DATASET, IMAGE, INSTANCE } from 'src/test-data';

describe('[Instance][Search][Component][Result] ImageDisplayComponent', () => {
    let component: ImageDisplayComponent;
    let fixture: ComponentFixture<ImageDisplayComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ImageDisplayComponent],
            providers: [
                {
                    provide: AppConfigService,
                    useValue: { servicesUrl: 'http://test.fr' },
                },
            ],
        });
        fixture = TestBed.createComponent(ImageDisplayComponent);
        component = fixture.componentInstance;
        component.dataset = { ...DATASET };
        component.instance = { ...INSTANCE };
        component.image = { ...IMAGE };
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('getHref() should return the href', () => {
        let expected =
            'http://test.fr/fits/fits-cut-to-png/my-instance/my-dataset?filename=test&ra=5&dec=10&radius=3&stretch=test&pmin=5&pmax=10&axes=false';
        component.coneSearch = { dec: 10, ra: 5, radius: 3 };
        expect(
            component.getHref({
                ...IMAGE,
                file_path: 'test',
                stretch: 'test',
                pmax: 10,
                pmin: 5,
            })
        ).toEqual(expected);
    });

    it('getFitsCutUrl() should return test/fits/fits-cut/test?filename=test&ra=5&dec=10&radius=3', () => {
        let expected =
            'http://test.fr/fits/fits-cut/my-instance/my-dataset?filename=test&ra=5&dec=10&radius=3';
        component.coneSearch = { dec: 10, ra: 5, radius: 3 };
        expect(
            component.getFitsCutUrl({
                ...IMAGE,
                file_path: 'test',
                stretch: 'test',
                pmax: 10,
                pmin: 5,
            })
        ).toEqual(expected);
    });

    it('should raises download file event', () => {
        let event = { preventDefault: jest.fn() };
        component.coneSearch = { dec: 10, ra: 5, radius: 3 };
        component.getFitsCutUrl = jest
            .fn()
            .mockImplementationOnce(() => 'test');
        let spy = jest.spyOn(component.downloadFile, 'emit');
        component.saveFitsCutFile(event, { ...IMAGE, file_path: 'test/test' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ url: 'test', filename: 'fits_cut_5_10_3_test' });
    });

    it('should raises download file event', () => {
        component.getFitsCutUrl = jest
            .fn()
            .mockImplementationOnce(() => 'test');
        let spy = jest.spyOn(component.broadcastImage, 'emit');
        component.broadcast({ ...IMAGE, file_path: 'test/test' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith('test');
    });

    it('should raises emitBackGroundHref and openPlotImage events', () => {
        let spyOnemitBackGroundHref = jest.spyOn(
            component.emitBackGroundHref,
            'emit'
        );
        let spyOnopenPlotImage = jest.spyOn(component.openPlotImage, 'emit');
        component.openConeSearch('test');
        expect(spyOnemitBackGroundHref).toHaveBeenCalledWith('test');
        expect(spyOnopenPlotImage).toHaveBeenCalledWith(true);
    });
});
