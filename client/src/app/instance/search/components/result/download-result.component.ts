/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AppConfigService } from 'src/app/app-config.service';
import { AbstractDownloadComponent } from './abstract-download.component';
import { Attribute, DesignConfig } from 'src/app/metamodel/models';

@Component({
    selector: 'app-download-result',
    templateUrl: 'download-result.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DownloadResultComponent extends AbstractDownloadComponent{
    @Input() designConfig: DesignConfig;
    @Input() attributeList: Attribute[];
    @Input() selectedData: any[] = [];
    @Input() sampRegistered: boolean;
    @Input() isAuthenticated: boolean;
    @Output() sampRegister: EventEmitter<{}> = new EventEmitter();
    @Output() sampUnregister: EventEmitter<{}> = new EventEmitter();
    @Output() broadcastVotable: EventEmitter<string> = new EventEmitter();
    @Output() startTaskCreateArchive: EventEmitter<string> = new EventEmitter();

    modalRef: BsModalRef;

    constructor(private modalService: BsModalService, appConfig: AppConfigService) {
        super(appConfig);
    }

    /**
     * Opens modal.
     *
     * @param  {TemplateRef<any>} template - The modal template to open.
     */
    openModal(template: TemplateRef<any>): void {
        this.modalRef = this.modalService.show(
            template,
            Object.assign({}, { class: 'gray modal-lg' })
        );
    }

    isArchiveIsAvailable() {
        return this.attributeList
            .filter(attribute => this.outputList.includes(attribute.id))
            .filter(attribute => attribute.archive)
            .length > 0;
    }

    broadcastResult() {
        const url = this.getDatatableUrl('votable');
        this.broadcastVotable.emit(url);
    }

    downloadArchive() {
        let query: string;
        if (this.selectedData.length > 0) {
            const attributeId = this.attributeList.find(a => a.id === this.dataset.primary_key);
            query = this.getQuery(`${attributeId.id}::in::${this.selectedData.join('|')}`);
        } else {
            query = this.getQuery();
        }
        this.startTaskCreateArchive.emit(query);
        this.modalRef.hide();
    }

    isDownloadEnabled() {
        return this.dataset.download_json
            || this.dataset.download_csv
            || this.dataset.download_ascii
            || this.dataset.download_vo;
    }

    getDatatableUrl(format: string): string {
        if (this.selectedData.length > 0) {
            const attributeId = this.attributeList.find(a => a.id === this.dataset.primary_key);
            return this.getUrl(format, `${attributeId.id}::in::${this.selectedData.join('|')}`);
        } else {
            return this.getUrl(format);
        }
    }
}