/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { Instance, Dataset, Attribute } from 'src/app/metamodel/models';
import { ConeSearch, Criterion } from 'src/app/instance/store/models';

@Component({
    selector: 'app-result-info',
    templateUrl: 'result-info.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultInfoComponent {
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() attributeList: Attribute[];
    @Input() dataLength: number;
    @Input() criteriaList: Criterion[];
    @Input() coneSearch: ConeSearch;

    getCriteriaLength() {
        let length = this.criteriaList.length;
        if (this.coneSearch) {
            length += 1;
        }
        return length;
    }
}
