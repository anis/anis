/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { Dataset, DatasetFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-dataset-tabs',
    templateUrl: 'dataset-tabs.component.html',
    styleUrls: ['dataset-tabs.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetTabsComponent {
    @Input() datasetList: Dataset[];
    @Input() datasetFamilyList: DatasetFamily[];
    @Input() instanceSelected: string;
    @Input() datasetSelected: string;
    @Input() authenticationEnabled: boolean;
    @Input() isAuthenticated: boolean;
    @Input() userRoles: string[];
    @Input() adminRoles: string[];

    getDatasetFamilyList() {
        return this.datasetFamilyList
            .filter(datasetFamily => this.getDatasetListByDatasetFamily(datasetFamily).length > 0);
    }

    getDatasetListByDatasetFamily(datasetFamily: DatasetFamily) {
        return this.datasetList.filter(dataset => 
            dataset.id_dataset_family === datasetFamily.id &&
            dataset.display !== 0
        );
    }

    checkIfDasetFamillyShouldBeOpened(datasetFamily: DatasetFamily): boolean {
        if (datasetFamily.opened) {
            return true;
        } else {
            return this.getDatasetListByDatasetFamily(datasetFamily)
                .filter(dataset => dataset.name === this.datasetSelected)
                .length > 0;
        }
    }
}
