/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';

import { Dataset } from 'src/app/metamodel/models';
import { isAdmin, isDatasetAccessible } from 'src/app/shared/utils';

@Component({
    selector: 'app-dataset-card',
    templateUrl: 'dataset-card.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetCardComponent {
    @Input() dataset: Dataset;
    @Input() instanceSelected: string;
    @Input() datasetSelected: string;
    @Input() authenticationEnabled: boolean;
    @Input() isAuthenticated: boolean;
    @Input() userRoles: string[];
    @Input() adminRoles: string[];

    constructor(private router: Router) { }

    isDatasetAccessible() {
        return isDatasetAccessible(
            this.dataset, 
            this.authenticationEnabled, 
            this.isAuthenticated,
            this.adminRoles,
            this.userRoles
        );
    }

    /**
     * Returns true if user is admin
     *
     * @returns boolean
     */
    isAdmin() {
        return isAdmin(this.adminRoles, this.userRoles);
    }

    /**
     * Navigates to search form corresponding to the given dataset.
     *
     * @param  {string} datasetName - The dataset name.
     */
    selectDataset(datasetName: string): void {
        this.router.navigate([`/${this.instanceSelected}/search/criteria/${datasetName}`]);
    }
}
