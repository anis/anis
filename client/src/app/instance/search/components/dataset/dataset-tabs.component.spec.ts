/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { DatasetTabsComponent } from './dataset-tabs.component';
import { Dataset, DatasetGroup } from 'src/app/metamodel/models';
import { DatasetListByFamilyPipe } from 'src/app/shared/pipes/dataset-list-by-family.pipe';

describe('[Instance][Search][Component][Dataset] DatasetTabsComponent', () => {
    @Component({ selector: 'app-dataset-card', template: '' })
    class DatasetCardStubComponent {
        @Input() dataset: Dataset;
        @Input() instanceSelected: string;
        @Input() datasetSelected: string;
        @Input() authenticationEnabled: boolean;
        @Input() isAuthenticated: boolean;
        @Input() userRoles: string[];
        @Input() adminRoles: string[];
        @Input() datasetGroupList: DatasetGroup[];
    }

    let component: DatasetTabsComponent;
    let fixture: ComponentFixture<DatasetTabsComponent>;
    let dataset: Dataset;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetTabsComponent,
                DatasetCardStubComponent,
                DatasetListByFamilyPipe
            ],
            imports: [
                AccordionModule.forRoot(),
                BrowserAnimationsModule
            ]
        });
        fixture = TestBed.createComponent(DatasetTabsComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('getDatasetFamilyList() should return an array with one datasetFamily with id 2', () => {
        let dataset: Dataset;
        component.datasetFamilyList = [
            { display: 5, id: 1, label: 'test1', opened: true },
            { display: 10, id: 2, label: 'test2', opened: false }
        ]
        component.datasetList = [{ ...dataset, id_dataset_family: 2 }];
        let result = component.getDatasetFamilyList();
        expect(result.length).toEqual(1);
        expect(result[0].id).toEqual(2);
    });

    it('getDatasetListByDatasetFamily(datasetFamily: DatasetFamily) should return an array with two  dataset with', () => {

        component.datasetList = [{ ...dataset, id_dataset_family: 1 }, { ...dataset, id_dataset_family: 2 }, { ...dataset, id_dataset_family: 2 }];
        let result = component.getDatasetListByDatasetFamily({ display: 10, id: 2, label: 'test', opened: false });
        expect(result.length).toEqual(2);
    });

    it('should return true when datasetFamily opened property is set to true', () => {
        expect(component.checkIfDasetFamillyShouldBeOpened({ display: 10, id: 1, label: 'test', opened: true })).toBe(true);
    });
    
    it('should return true when the dataset is selected', () => {
        component.datasetList = [
            { ...dataset, id_dataset_family: 1, name: 'test' },
            { ...dataset, id_dataset_family: 2 },
            { ...dataset, id_dataset_family: 2 }
        ];
        component.datasetSelected = 'test';
        expect(component.checkIfDasetFamillyShouldBeOpened({ display: 10, id: 1, label: 'test', opened: false })).toBe(true);
    });
});
