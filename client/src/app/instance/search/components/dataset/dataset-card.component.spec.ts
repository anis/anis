/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Router } from '@angular/router';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PopoverModule } from 'ngx-bootstrap/popover';

import { DatasetCardComponent } from './dataset-card.component';
import { Dataset } from 'src/app/metamodel/models';
import * as utils from 'src/app/shared/utils';
import { DATASET } from 'src/test-data';

describe('[Instance][Search][Component][Dataset] DatasetCardComponent', () => {
    let component: DatasetCardComponent;
    let fixture: ComponentFixture<DatasetCardComponent>;
    let router: Router;
    let dataset: Dataset;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DatasetCardComponent],
            imports: [PopoverModule.forRoot()],
            providers: [{ provide: Router, useValue: { navigate: jest.fn() } }]
        });
        fixture = TestBed.createComponent(DatasetCardComponent);
        component = fixture.componentInstance;
        router = TestBed.inject(Router);
        dataset = { ...DATASET };
        component.dataset = dataset;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should create the component', () => {
        component.instanceSelected = 'myInstance';
        const spy = jest.spyOn(router, 'navigate');
        component.selectDataset('myDataset');
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(["/myInstance/search/criteria/myDataset"]);
    });

    it('should return false when authentication is enabled  and dataset is not public and user is not admin', () => {
        component.authenticationEnabled = true;
        component.dataset.public = false;
        const addStub = jest.spyOn(utils, 'isAdmin').mockReturnValueOnce(false);
        expect(component.isDatasetAccessible()).toBe(false);
        addStub.mockRestore();
    })

    it('should return true when authentication is Enabled, dataset is not public and user is not admin and is autheticated', () => {
        component.authenticationEnabled = true;
        component.isAuthenticated = true;
        component.dataset.public = false;
        component.userRoles = ['DTEST'];
        const addStub = jest.spyOn(utils, 'isAdmin').mockReturnValueOnce(false);
        expect(component.isDatasetAccessible()).toBe(true);
        addStub.mockRestore();
    });

    it('should return true', () => {
        component.userRoles = ['test'];
        component.adminRoles = ['test'];
        const addStub = jest.spyOn(utils, 'isAdmin').mockReturnValueOnce(true);
        expect(component.isAdmin()).toBe(true);
        addStub.mockRestore();
    });
});
