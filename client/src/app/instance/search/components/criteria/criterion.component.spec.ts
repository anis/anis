/**
 * component file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with component source code.
 */

import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ComponentRef, ElementRef, EmbeddedViewRef, Injector, Input, SimpleChange, Type, ViewContainerRef, ViewRef } from '@angular/core';
import { FormGroup } from "@angular/forms";
import { ComponentFixture, TestBed } from "@angular/core/testing"
import { MockStore, provideMockStore } from "@ngrx/store/testing";

import { CriterionComponent } from "./criterion.component";
import { Attribute } from "src/app/metamodel/models";
import { AbstractSearchTypeComponent } from "./search-type";
import { Criterion, FieldCriterion } from "src/app/instance/store/models";

class MockAbstractSearchTypeComponent extends AbstractSearchTypeComponent {
    getCriterion = jest.fn().mockImplementation(() => ({
        id: 1,
        type: 'field',
        operator: "test"
    } as FieldCriterion));
    override setCriterion = jest.fn();
}

class MockComponentRef extends ComponentRef<AbstractSearchTypeComponent> {

    form: FormGroup = new FormGroup({});

    setInput(name: string, value: unknown): void {
        return null;
    }

    get location(): ElementRef<any> {
        return null;
    }

    get injector(): Injector {
        return null;
    }

    get instance(): AbstractSearchTypeComponent {
        return new MockAbstractSearchTypeComponent()
    }

    get hostView(): ViewRef {
        return null;
    }

    get changeDetectorRef(): ChangeDetectorRef {
        return null;
    }

    get componentType(): Type<any> {
        return null;
    }
    destroy(): void {
        return null;
    }

    onDestroy(callback: Function): void {
        return null;
    }
}

class MockViewContainerRef extends ViewContainerRef {
    get element(): ElementRef<any> {
        return null;
    }

    get injector(): Injector {
        return null;
    }

    get parentInjector(): Injector {
        return null;
    }

    clear(): void {
        return null;
    }

    get(index: number): ViewRef {
        return null;
    }

    get length(): number {
        return null;
    }

    createEmbeddedView(templateRef: unknown, context?: unknown, index?: unknown): EmbeddedViewRef<any> {
        return null;
    }

    createComponent(componentFactory: unknown, index?: unknown, injector?: unknown, projectableNodes?: unknown, environmentInjector?: unknown): ComponentRef<any> | ComponentRef<any> {
        return new MockComponentRef();
    }

    insert(viewRef: ViewRef, index?: number): ViewRef {
        return null;
    }

    move(viewRef: ViewRef, currentIndex: number): ViewRef {
        return null;
    }

    indexOf(viewRef: ViewRef): number {
        return null;
    }

    remove(index?: number): void {
        return null;
    }

    detach(index?: number): ViewRef {
        return null;
    }
}

describe('[instance][search][component][criteria] CriterionComponent', () => {
    let component: CriterionComponent;
    let fixture: ComponentFixture<CriterionComponent>;
    let store: MockStore;
    let viewContainerRef: MockViewContainerRef = new MockViewContainerRef();
    let attribute: Attribute;

    @Component({
        selector: 'app-attribute-label',
        template: '',
    })
    class AttributeLabelComponent {
        @Input() label: string;
        @Input() description: string;
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                CriterionComponent,
                AttributeLabelComponent,
            ],
            providers: [
                provideMockStore({}),
            ],
        }).overrideComponent(CriterionComponent, {
            set: { changeDetection: ChangeDetectionStrategy.Default }
        }).compileComponents();
        fixture = TestBed.createComponent(CriterionComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        component.SearchTypeLoaderDirective = {
            viewContainerRef: viewContainerRef
        }
        component.attribute = { ...attribute, search_type: 'test' };
        component.criteriaList = [{ id: 1, type: 'testCriterion' }, { id: 2, type: 'test2' }];
        component.searchTypeComponent = new MockAbstractSearchTypeComponent();
    });

    it('should create component', async () => {
        fixture.detectChanges();
        expect(component).toBeTruthy();
    });

    it('should call ngOnChanges and apply changes', () => {
        fixture.detectChanges();
        const spy = jest.spyOn(component, 'ngOnChanges');
        expect(spy).toHaveBeenCalledTimes(0);
        component.ngOnChanges(
            {
                criterion: new SimpleChange(component.criterion, { id: 1, type: 'test' }, false),
                criteriaList: new SimpleChange(component.criteriaList, [{ id: 1, type: 'test' }, { id: 2, type: 'test2' }], false),
            }
        );
        fixture.detectChanges();
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('emitAdd() raises addCriterion event', () => {
        let spy = jest.spyOn(component.addCriterion, 'emit');
        component.searchTypeComponent.nullOrNotNull = 'test';
        let criterion: Criterion = {
            id: component.attribute.id,
            type: 'field',
            operator: "test"
        } as FieldCriterion;
        component.emitAdd();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(criterion);
    });

    it('emitAdd() raises addCriterion event', () => {
        component.searchTypeComponent.nullOrNotNull = '';
        let spy = jest.spyOn(component.updateCriterion, 'emit');
        let criterion: Criterion = {
            id: 1,
            type: 'field',
            operator: "test"
        } as FieldCriterion;
        component.emitAdd();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(criterion);
    });
});
