/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators, UntypedFormArray, FormArray } from '@angular/forms';

import { Attribute } from 'src/app/metamodel/models';
import { Criterion, JsonCriteriaList, JsonCriterion } from 'src/app/instance/store/models';

@Component({
    selector: 'app-json-list',
    templateUrl: 'json-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class JsonListComponent implements OnInit {
    @Input() instanceSelected: string;
    @Input() datasetSelected: string;
    @Input() attribute: Attribute;
    @Input() criterion: Criterion;
    @Input() criteriaList: Criterion[];
    @Output() addCriterion: EventEmitter<Criterion> = new EventEmitter();
    @Output() updateCriterion: EventEmitter<Criterion> = new EventEmitter();
    @Output() deleteCriterion: EventEmitter<number> = new EventEmitter();

    form: UntypedFormGroup = new UntypedFormGroup({
        label: new UntypedFormControl({value: 'json', disabled: true}),
        list: new UntypedFormArray([this.createJsonCriterionForm()])
    });

    ngOnInit(): void {
        if (this.criterion) {
            this.form.controls['list'] = new UntypedFormArray([]);
            (this.criterion as JsonCriteriaList).list.forEach(jsonCriterion => {
                this.getJsonCriteriaListFormArray().push(this.createJsonCriterionForm(
                    jsonCriterion.path,
                    jsonCriterion.operator,
                    jsonCriterion.value
                ));
            });
            this.getJsonCriteriaListFormArray().push(this.createJsonCriterionForm());
        }
    }

    addJsonCriterion(jsonCriterion: JsonCriterion) {
        if (this.criterion) {
            const updateJsonCriteriaList: JsonCriteriaList = {
                ...this.criterion,
                list: [...(this.criterion as JsonCriteriaList).list, jsonCriterion]
            };
            this.updateCriterion.emit(updateJsonCriteriaList);
        } else {
            const newJsonCriteriaList: JsonCriteriaList = {
                id: this.attribute.id,
                type: 'json',
                list: [
                    jsonCriterion
                ]
            };
            this.addCriterion.emit(newJsonCriteriaList);
        }
        (this.form.controls['list'] as FormArray).push(this.createJsonCriterionForm());
    }

    updateJsonCriterion(jsonCriterion: any) {
        const list = (this.criterion as JsonCriteriaList).list.map((value, index) => {
            if (jsonCriterion.index === index) {
                return jsonCriterion;
            } else {
                return value;
            }
        });

        const updateJsonCriteriaList: JsonCriteriaList = {
            ...this.criterion,
            list
        };
        this.updateCriterion.emit(updateJsonCriteriaList);
    }

    deleteJsonCriterion(jsonCriterionIndex: number) {
        if ((this.criterion as JsonCriteriaList).list.length < 2) {
            this.deleteCrierion();
        } else {
            const list = (this.criterion as JsonCriteriaList).list.filter((value, index) => jsonCriterionIndex !== index);

            const updateJsonCriteriaList: JsonCriteriaList = {
                ...this.criterion,
                list
            };
            this.updateCriterion.emit(updateJsonCriteriaList);
            this.getJsonCriteriaListFormArray().removeAt(jsonCriterionIndex);
        }
    }

    deleteCrierion() {
        this.deleteCriterion.emit(this.attribute.id);
        this.form.controls['list'] = new UntypedFormArray([]);
        this.getJsonCriteriaListFormArray().push(this.createJsonCriterionForm());
    }

    createJsonCriterionForm(path: string = null, operator: string = null, value: string = null): UntypedFormGroup {
        return new UntypedFormGroup({
            path: new UntypedFormControl(path, [Validators.required]),
            operator: new UntypedFormControl(operator, [Validators.required]),
            value: new UntypedFormControl(value, [Validators.required])
        });
    }

    getJsonCriteriaListFormArray() {
        return this.form.controls['list'] as FormArray;
    }

    getJsonCriterionForm(jsonCriterion) {
        return jsonCriterion as UntypedFormGroup;
    }
}
