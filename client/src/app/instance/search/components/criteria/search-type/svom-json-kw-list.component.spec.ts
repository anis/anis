/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, Validators, UntypedFormControl, UntypedFormGroup, UntypedFormArray } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { of } from 'rxjs';

import { AppConfigService } from 'src/app/app-config.service';
import { SvomJsonKwListComponent } from './svom-json-kw-list.component';
import { FieldCriterion, SvomKeyword } from 'src/app/instance/store/models';
import { ATTRIBUTE } from 'src/test-data';

class MockHttpClient extends HttpClient {
    override get = jest.fn().mockImplementation(() => of([
        {
            json_schema: 
                {
                    product_keywords: 
                        [{
                            data_type: 'test',
                            default: '',
                            extension: 'test',
                            sdb_search: true,
                            name: '',
                            expected_values: {
                                mode: 'enumeration',
                                values: []
                            }
                        }]
                }
        }
    ]));
}

describe('[Instance][search][components][criteria][search-type] SvomJsonKwListComponent', () => {
    let component: SvomJsonKwListComponent;
    let fixture: ComponentFixture<SvomJsonKwListComponent>;
    let httpClient: MockHttpClient = new MockHttpClient(null);

    @Component({ selector: 'app-attribute-label', template: '' })
    class AttributeLabelStubComponent {
        @Input() label: string;
        @Input() description: string;
    }

    @Component({ selector: 'app-svom-json-criterion', template: '' })
    class SvomJsonCriterionStubComponent {
        @Input() form: UntypedFormGroup;
        @Input() index: number;
        @Input() isFirst: boolean;
        @Input() isLast: boolean;
        @Input() svomKeywords: SvomKeyword[];
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                SvomJsonKwListComponent,
                AttributeLabelStubComponent,
                SvomJsonCriterionStubComponent
            ],
            imports: [
                ReactiveFormsModule,
                NgSelectModule
            ],
            providers: [
                { provide: AppConfigService, useValue: { authenticationEnabled: false } },
                ChangeDetectorRef,
                { provide: HttpClient, useValue: httpClient }
            ]
        });
        fixture = TestBed.createComponent(SvomJsonKwListComponent);
        component = fixture.componentInstance;
        component.criteriaList = [{
            id: 3,
            type: 'string',
            value: 'OBLC_ECL',
            operator: 'eq'
        } as FieldCriterion]
        component.attribute = { ...ATTRIBUTE };
        component.svomKeywords = [];
        const list = new UntypedFormGroup({
            path: new UntypedFormControl(null, [Validators.required]),
            operator: new UntypedFormControl(null, [Validators.required]),
            value: new UntypedFormControl('', [Validators.required])
        });
        component.form = new UntypedFormGroup({
            label: new UntypedFormControl({value: 'json', disabled: true}),
            list: new UntypedFormArray([list])
        });
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('updateSvomKeywords()', () => {
        component.updateSvomKeywords();
        expect(component.svomKeywords.length).toBe(1);
    });
});
