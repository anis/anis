/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit, OnDestroy } from '@angular/core';
import { UntypedFormGroup} from '@angular/forms';

import { debounceTime, Subscription } from 'rxjs';

@Component({
    selector: 'app-json-criterion',
    templateUrl: 'json-criterion.component.html',
    styles: [`
        .btn-outline-danger.disabled, .btn-outline-danger:disabled {
            color: gray;
            border-color: gray;
        }
    `],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class JsonCriterionComponent implements OnInit, OnDestroy {
    @Input() form: UntypedFormGroup;
    @Input() index: number;
    @Input() isFirst: boolean;
    @Input() isLast: boolean;
    @Output() addJsonCriterion = new EventEmitter<any>();
    @Output() updateJsonCriterion = new EventEmitter<any>();
    @Output() deleteJsonCriterion = new EventEmitter<number>();

    formValueChangesSubscription: Subscription;

    ngOnInit(): void {
        this.formValueChangesSubscription = this.form.valueChanges.pipe(
            debounceTime(600),
        )
        .subscribe(() => {
            if (this.form.valid) {
                if (this.isLast) {
                    this.addJsonCriterion.emit({index: this.index, ...this.form.value});
                } else {
                    this.updateJsonCriterion.emit({index: this.index, ...this.form.value});
                }
            }
        });
    }

    ngOnDestroy(): void {
        this.formValueChangesSubscription.unsubscribe();
    }
}
