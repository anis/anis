/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { FieldCriterion } from 'src/app/instance/store/models';
import { Attribute } from 'src/app/metamodel/models';
import { SelectComponent } from './select.component';

describe('[Instance][search][components][criteria][search-type] SelectComponent', () => {
    let component: SelectComponent;
    let fixture: ComponentFixture<SelectComponent>;
    let attribute: Attribute;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [SelectComponent],
            imports: [
                ReactiveFormsModule,
                NgSelectModule
            ]
        });
        fixture = TestBed.createComponent(SelectComponent);
        component = fixture.componentInstance;
        component.attribute = {
            ...attribute, options: [
                { label: 'test1', display: 1, value: 'test1' },
                { label: 'test2', display: 2, value: 'test2' }
            ],
            operator: 'test',
            placeholder_min: 'min'
        }
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('setCriterion(criterion: Criterion) should set select value and call operatorOnChange when criterion param  defined', () => {
        let spy = jest.spyOn(component, 'operatorOnChange');
        expect(component.form.controls['select'].value).toEqual('');
        component.setCriterion({ id: 1, value: 'test', type: 'test' } as FieldCriterion);
        expect(component.form.controls['select'].value).toEqual('test');
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('setCriterion(criterion: Criterion) should set operator value to test value when criterion param is not defined', () => {
        expect(component.form.controls['operator'].value).toEqual('');
        component.setCriterion(null);
        expect(component.form.controls['operator'].value).toEqual('test');
    });

    it('getCriterion() should return a criterion with type list', () => {
        component.form.controls['select'].setValue('test1');
        expect(component.getCriterion().type).toEqual('field');
    });

    it('isValid() should true when form is valid or when operator value is nl or nnl', () => {
        expect(component.isValid()).toBe(false);
        component.form.controls['operator'].setValue('test');
        component.form.controls['select'].setValue('test');
        expect(component.isValid()).toBe(true);
        component.form.controls['operator'].setValue('nnl');
        expect(component.isValid()).toBe(true);
        component.form.controls['operator'].setValue('nl');
        expect(component.isValid()).toBe(true);
    });

    it('operatorOnChange() should disable select formcontrol when operator value is nl or nnl', () => {
        expect(component.form.controls['select'].disabled).toBe(false);
        component.form.controls['operator'].setValue('nl');
        component.operatorOnChange();
        expect(component.form.controls['select'].disabled).toBe(true);
    });

    it('operatorOnChange() should enable select formcontrol when operator value is not  nl or nnl', () => {
        component.form.controls['select'].disable();
        expect(component.form.controls['select'].enabled).toBe(false);
        component.operatorOnChange();
        expect(component.form.controls['select'].enabled).toBe(true);
    });
});
