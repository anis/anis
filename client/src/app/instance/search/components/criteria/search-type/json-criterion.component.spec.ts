/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { JsonCriterionComponent } from './json-criterion.component';

describe('[Instance][search][components][criteria][search-type] JsonCriterionComponent', () => {
    let component: JsonCriterionComponent;

    TestBed.configureTestingModule({
        declarations: [JsonCriterionComponent],
        imports: [
            ReactiveFormsModule,
            NgSelectModule
        ]
    });

    beforeEach(() => {
        component = new JsonCriterionComponent();
        component.form = new UntypedFormGroup({
            test: new UntypedFormControl('test')
        });
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
