/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, ViewChild, TemplateRef } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { Observable, Subject, concat, of, forkJoin } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, map, switchMap, tap, filter } from 'rxjs/operators';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import FileSaver from 'file-saver';

import { AbstractSearchTypeComponent } from './abstract-search-type.component';
import { Criterion, FieldCriterion, SelectMultipleCriterion } from 'src/app/instance/store/models';
import { AppConfigService } from 'src/app/app-config.service';
import { Alias } from 'src/app/instance/store/models';

@Component({
    selector: 'app-select-multiple-alias',
    templateUrl: 'select-multiple-alias.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SelectMultipleAliasComponent extends AbstractSearchTypeComponent {
    @ViewChild('template') template: TemplateRef<HTMLDivElement>;

    labels = [
        { value: 'in', label: 'in' }
    ];

    aliases: Observable<Alias[]>;
    aliasesLoading = false;
    aliasInput$ = new Subject<string>();

    formList: UntypedFormGroup;
    listFound: {search: string, alias: Alias[], exact: boolean}[];
    valuesFound: Alias[];
    modalRef: BsModalRef;

    constructor(private http: HttpClient, private config: AppConfigService, private modalService: BsModalService) {
        super();
        this.form = new UntypedFormGroup({
            label: new UntypedFormControl(''),
            select: new UntypedFormControl('', [Validators.required])
        });

        this.formList = new UntypedFormGroup({
            list: new UntypedFormControl('', [Validators.required])
        });
    }

    changeSearchMode(): void {
        const listEnabled = this.formList.controls['list'].enabled;
        this.emitDelete.emit();
        setTimeout(() => {
            if (listEnabled) {
                this.form.controls['select'].enable();
                this.formList.controls['list'].disable();
            } else {
                this.form.controls['select'].disable();
                this.formList.controls['list'].enable();
            }
        }, 100);
    }

    searchByList() {
        const list = this.formList.controls['list'].value
            .split('\n')
            .filter((element: string) => String(element || '').trim());
        const calls = [];
        list.forEach(element => {
            calls.push(this.getHttpAliases(element));
        });
        forkJoin<Alias[][]>(calls).subscribe(fullArray => {
            this.listFound = [];
            this.valuesFound = [];
            for (let i = 0; i < fullArray.length; i++) {
                const searchedTerm = list[i]
                const aliasesReturned = fullArray[i]
                let exactMatch = false  // Is the alias exactly what the user asked for?
                if (aliasesReturned.length === 1) {
                    if (aliasesReturned[0].alias === searchedTerm.toLowerCase().replace(/[^a-z0-9+.]/g, '')) {
                        exactMatch = true
                    }
                }
                // Push a notindb alias to be used when the alias search is
                // ambiguous (not exact with possibly several values) so the
                // user can say which  one is the right one.
                // This alias must be the first one because if the user does
                // not select the right value for a name the first value of the
                // list is taken for the CSV export.
                if (!exactMatch && aliasesReturned.length > 0) {
                    aliasesReturned.unshift(
                        {
                            alias: "notindb",
                            name: "None of these",
                            alias_long: "None of these"
                        }
                    )
                }
                this.listFound.push({
                    search: list[i],
                    alias: fullArray[i],
                    exact: exactMatch
                });
                if (fullArray[i].length > 0) {
                    this.valuesFound.push(fullArray[i][0]);
                } else {
                    this.valuesFound.push(null);
                }
            }
            this.modalRef = this.modalService.show(this.template, { class: 'modal-lg' });
        });
    }

    onSelected(index: number, event) {
        const aliasIndex = event.target.value;
        this.valuesFound[index] = this.listFound[index].alias[aliasIndex];
    }

    addAliases() {
        // update the search form only with the right values
        this.form.controls['select'].setValue(this.valuesFound.filter(value => value && value.alias != "notindb"));
        this.modalRef.hide();
    }

    saveAliases() {
        const values = [];
        values.push('your_search,alias_found,diva_database_name');

        for (let i = 0; i < this.valuesFound.length; i++) {
            // only put the known aliases in the CSV
            if (this.valuesFound[i] && this.valuesFound[i].alias != "notindb") {
                values.push(this.listFound[i].search + "," + this.valuesFound[i].alias_long + "," + this.valuesFound[i].name);
            }
        }

        const blob = new Blob([values.join('\n')], {type: "text/csv;charset=utf-8"});
        FileSaver.saveAs(blob, "file.csv");
    }

    override ngOnInit(): void {
        super.ngOnInit();
        this.loadAliases();
        this.formList.controls['list'].disable();
    }

    override isValid() {
        return this.form.controls['select'].value;
    }

    override setCriterion(criterion: Criterion) {
        super.setCriterion(criterion);
        if (criterion) {
            if (criterion.type === 'multiple') {
                const multipleCriterion = criterion as SelectMultipleCriterion;
                const values = multipleCriterion.options.map(option => ({
                    name: option.value,
                    alias: option.value,
                    alias_long: option.value
                }));
                this.form.controls['select'].setValue(values);
                this.form.controls['label'].setValue('in');
                this.loadAliases();
            } else {
                const fieldCriterion = criterion as FieldCriterion;
                this.form.controls['label'].setValue(fieldCriterion.operator);
                this.labelOnChange();
            }
        } else {
            this.form.controls['label'].setValue('in');
            this.formList.reset();
            this.formList.controls['list'].disable();
            this.loadAliases();
        }
    }

    /**
     * Return new criterion
     *
     * @return Criterion
     */
    getCriterion(): Criterion {
        const values = this.form.getRawValue().select as Alias[];
        const options = values.map(alias => ({
            label: alias.name,
            value: alias.name,
            display: 10
        }));

        return {
            id: this.attribute.id,
            type: 'multiple',
            options
        } as SelectMultipleCriterion;
    }

    labelOnChange() {
        if (this.form.controls['label'].value === 'nl' || this.form.controls['label'].value === 'nnl') {
            this.nullOrNotNull = this.form.controls['label'].value;
            this.form.controls['select'].disable();
        } else {
            this.nullOrNotNull = '';
            this.form.controls['select'].enable();
        }
    }

    trackByFn(item: Alias) {
        return item.name;
    }

    getSelectedObjects() {
        let getSelectedObjects = [];
        const aliases = this.form.value.select as Alias[];
        if (aliases) {
            getSelectedObjects = aliases.map(alias => alias.name);
        }
        return getSelectedObjects;
    }

    private loadAliases() {
        this.aliases = concat(
            of([]), // default items
            this.aliasInput$.pipe(
                distinctUntilChanged(),
                debounceTime(200),
                filter(term => !!term),
                tap(() => this.aliasesLoading = true),
                switchMap(term => this.getHttpAliases(term).pipe(
                    map(value => value.filter(alias => !this.getSelectedObjects().includes(alias.name))),
                    catchError(() => of([])), // empty list on error
                    tap(() => {
                        this.aliasesLoading = false
                    })
                ))
            )
        );
    }

    getHttpAliases(term: string = null): Observable<Alias[]> {
        const value = term.toLowerCase().replace(/[^a-z0-9+.]/g, '');
        return this.http.get<{status: number, data: Alias[]}>(`${this.config.apiUrl}/search-alias/${this.instanceSelected}/${this.datasetSelected}/${value}`).pipe(
            map(response => response.data)
        );
    }
}
