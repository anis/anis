/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    ComponentFixture,
    TestBed,
    fakeAsync,
    tick,
} from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

import { SelectAliasComponent } from './select-alias.component';
import { AppConfigService } from 'src/app/app-config.service';
import { ALIAS, ATTRIBUTE, CRITERION } from 'src/test-data';
import { FieldCriterion } from 'src/app/instance/store/models/criterion/field-criterion.model';

jest.mock('@angular/common/http');
jest.mock('src/app/app-config.service');

describe('[Instance][search][components][criteria][search-type] SelectAliasComponent', () => {
    let component: SelectAliasComponent;
    let fixture: ComponentFixture<SelectAliasComponent>;

    const get = jest.fn(() => of({status: 200, data: [ALIAS, ALIAS]}));

    const apiUrl = 'http://coconut';

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [SelectAliasComponent],
            imports: [FormsModule, ReactiveFormsModule, NgSelectModule],
            providers: [
                { provide: HttpClient, useValue: { get } },
                { provide: AppConfigService, useValue: { apiUrl } },
            ],
        });

        fixture = TestBed.createComponent(SelectAliasComponent);
        component = fixture.componentInstance;
        component.attribute = ATTRIBUTE;
        component.datasetSelected = 'datapeach';
        fixture.detectChanges();
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should call loadAliases onInit', () => {
        const loadSpy = jest.spyOn(component as any, 'loadAliases');
        component.ngOnInit();
        expect(loadSpy).toHaveBeenCalled();
    });

    it('should call loadAliases onInit', () => {
        const loadSpy = jest.spyOn(component as any, 'loadAliases');
        component.ngOnInit();
        expect(loadSpy).toHaveBeenCalled();
    });

    it('should be valid', () => {
        expect(component.isValid());
    });

    it('should enable or disable properly the form', () => {
        component.form.controls['operator'].setValue('nl');
        component.operatorOnChange();
        expect(component.form.controls['select'].disabled);

        component.form.controls['operator'].setValue('eq');
        component.operatorOnChange();
        expect(component.form.controls['select'].enabled);
    });

    // it('should know aliases by calling the httpClient service', () => {
    //     component.getHttpAliases('coucou');
    //     // (@lmenou): NOTE Here we are testing the mock, which is pretty useless,
    //     // I do not want to connect to the database to test the client
    //     expect(get).toHaveBeenCalled();
    // });

    it('should get the criterion via #getCriterion', () => {
        component.form.controls['operator'].setValue('eq');
        fixture.detectChanges();

        const res = component.getCriterion();
        expect(res).toEqual({
            id: 1,
            type: 'field',
            operator: 'eq',
            value: undefined,
        } as FieldCriterion);
    });

    it('should #setCriterion properly', () => {
        component.setCriterion(CRITERION);
        expect(component.form.controls['select'].getRawValue()).toEqual({
            name: 'hello',
            alias: 'hello',
            alias_long: 'hello',
        });

        component.form.controls['operator'].setValue('eq');
        const spyLoadAliases = jest.spyOn(component as any, 'loadAliases');
        component.setCriterion(null);
        expect(spyLoadAliases).toHaveBeenCalled();
    });

    it('should #loadAliases properly', fakeAsync(() => {
        let spyGetAliases = jest
            .spyOn(component, 'getHttpAliases')
            .mockImplementation(() => of([ALIAS, ALIAS]));

        (component as any).loadAliases();
        component.aliasInput$.next('al');
        tick(300);
        expect(component.aliasesLoading).toBeFalsy();
        expect(spyGetAliases).toHaveBeenCalled();
    }));
});
