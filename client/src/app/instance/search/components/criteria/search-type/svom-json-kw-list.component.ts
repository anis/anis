/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectorRef, ChangeDetectionStrategy, SimpleChanges, OnChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map, concatMap, distinct, reduce, switchMap, filter } from 'rxjs/operators';

import { JsonListComponent } from './json-list.component';
import { AppConfigService } from 'src/app/app-config.service';
import { Criterion, FieldCriterion, SvomKeyword } from 'src/app/instance/store/models';
import { from } from 'rxjs';

@Component({
    selector: 'app-svom-json-kw-list',
    templateUrl: 'svom-json-kw-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SvomJsonKwListComponent extends JsonListComponent implements OnChanges {
    defaultSvomKeywords: SvomKeyword[] = [];
    svomKeywords: SvomKeyword[] = [];

    constructor(private changeDetectorRef: ChangeDetectorRef, private http: HttpClient, private config: AppConfigService) {
        super();
    }

    override ngOnInit(): void {
        super.ngOnInit();
        this.http.get<{json_schema: { product_keywords: SvomKeyword[]}}[]>(`${this.config.apiUrl}/search/${this.instanceSelected}/sp_cards?a=7`).pipe(
            switchMap(data => data.filter(data => data.json_schema).map(data => data.json_schema.product_keywords)),
            concatMap(data => from(data)),
            filter(svomKeyword => svomKeyword.sdb_search),
            distinct(svomKeyword => svomKeyword.name),
            reduce((svomKeywords, svomKeyword) => [...svomKeywords, svomKeyword], [])
        ).subscribe(svomKeywords => {
            this.defaultSvomKeywords = svomKeywords;
            if (this.svomKeywords.length < 1) {
                this.svomKeywords = this.defaultSvomKeywords;
            }
            this.changeDetectorRef.detectChanges();
        });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['criteriaList'] && changes['criteriaList'].currentValue) {
            this.updateSvomKeywords();
        }
    }

    updateSvomKeywords(): void {
        if (this.criteriaList.find((c: Criterion) => c.id === 3)) {
            const acronym = (this.criteriaList.find((c: Criterion) => c.id === 3) as FieldCriterion).value;
            this.http.get<{json_schema: { product_keywords: SvomKeyword[]}}[]>(`${this.config.apiUrl}/search/${this.instanceSelected}/sp_cards?a=7&c=1::eq::${acronym}`).pipe(
                map(data => data[0].json_schema ? data[0].json_schema.product_keywords: []),
                concatMap(data => from(data)),
                filter(svomKeyword => svomKeyword.sdb_search),
                reduce((svomKeywords, svomKeyword) => [...svomKeywords, svomKeyword], [])
            ).subscribe(svomKeywords => {
                this.svomKeywords = svomKeywords;
                this.changeDetectorRef.detectChanges();
            });
        }

        if (this.svomKeywords.length > 0 && !this.criteriaList.find((c: Criterion) => c.id === 3)) {
            this.svomKeywords = this.defaultSvomKeywords;
        }
    }
}
