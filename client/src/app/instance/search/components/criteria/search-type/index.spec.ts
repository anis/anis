/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { getSearchTypeComponent } from "."
import { BetweenDateComponent } from "./between-date.component";
import { BetweenComponent } from "./between.component";
import { CheckboxComponent } from "./checkbox.component";
import { DatalistComponent } from "./datalist.component";
import { DateComponent } from "./date.component";
import { DateTimeComponent } from "./datetime.component";
import { FieldComponent } from "./field.component";
import { ListComponent } from "./list.component";
import { RadioComponent } from "./radio.component";
import { SelectMultipleComponent } from "./select-multiple.component";
import { SelectComponent } from "./select.component";
import { TimeComponent } from "./time.component";

describe('[Instance][search][components][criteria][search-type] index', () => {
    let theFunction = getSearchTypeComponent;

    it('it should return FieldComponent', () => {
        expect(theFunction('field')).toEqual(FieldComponent);
    });

    it('it should return BetweenComponent', () => {
        expect(theFunction('between')).toEqual(BetweenComponent);
    });

    it('it should return SelectComponent', () => {
        expect(theFunction('select')).toEqual(SelectComponent);
    });

    it('it should return SelectMultipleComponent', () => {
        expect(theFunction('select-multiple')).toEqual(SelectMultipleComponent);
    });

    it('it should return DatalistComponent', () => {
        expect(theFunction('field')).toEqual(FieldComponent);
    });

    it('it should return DatalistComponent', () => {
        expect(theFunction('datalist')).toEqual(DatalistComponent);
    });

    it('it should return ListComponent', () => {
        expect(theFunction('list')).toEqual(ListComponent);
    });

    it('it should return RadioComponent', () => {
        expect(theFunction('radio')).toEqual(RadioComponent);
    });

    it('it should return CheckboxComponent', () => {
        expect(theFunction('checkbox')).toEqual(CheckboxComponent);
    });

    it('it should return BetweenDateComponent', () => {
        expect(theFunction('between-date')).toEqual(BetweenDateComponent);
    });

    it('it should return DateComponent', () => {
        expect(theFunction('date')).toEqual(DateComponent);
    });

    it('it should return TimeComponent', () => {
        expect(theFunction('time')).toEqual(TimeComponent);
    });

    it('it should return FieldComponent', () => {
        expect(theFunction('field')).toEqual(FieldComponent);
    });

    it('it should return DateTimeComponent', () => {
        expect(theFunction('date-time')).toEqual(DateTimeComponent);
    });
});
