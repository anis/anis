/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { SvomJsonCriterionComponent } from './svom-json-criterion.component';

describe('[Instance][search][components][criteria][search-type] SvomJsonCriterionComponent', () => {
    let component: SvomJsonCriterionComponent;

    TestBed.configureTestingModule({
        declarations: [SvomJsonCriterionComponent],
        imports: [
            ReactiveFormsModule,
            NgSelectModule
        ]
    });

    beforeEach(() => {
        component = new SvomJsonCriterionComponent();
        component.form = new UntypedFormGroup({
            test: new UntypedFormControl('test')
        });
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('getOrderedKeywords', () => {
        const keyword1 = {
            name: "ORIGIN",
            default: "FSC",
            data_type: "string",
            extension: "PrimaryHDU",
            sdb_search: true,
            description: 'ORIGIN',
            expected_values: {
                mode: 'enumeration',
                values: []
            }
        };
        const keyword2 = {
            name: "AV_AG_LOC",
            default: "FSC",
            data_type: "string",
            extension: "PrimaryHDU",
            sdb_search: true,
            description: 'AV_AG_LOC',
            expected_values: {
                mode: 'enumeration',
                values: []
            }
        };

        component.svomKeywords = [keyword1, keyword2];
        expect(component.getOrderedKeywords()).toEqual([keyword2, keyword1]);
    });

    it('getKeywordValue() should return a svom string keyword', () => {
        const keyword = {
            name: "ORIGIN",
            default: "FSC",
            data_type: "string",
            extension: "PrimaryHDU",
            sdb_search: true,
            description: 'ORIGIN',
            expected_values: {
                mode: 'enumeration',
                values: []
            }
        };
        expect(component.getKeywordValue(keyword)).toEqual('PrimaryHDU,ORIGIN');
    });
});
