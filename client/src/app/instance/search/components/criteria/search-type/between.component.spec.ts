/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { Criterion } from 'src/app/instance/store/models';
import { Attribute } from 'src/app/metamodel/models';
import { BetweenComponent } from './between.component';

describe('[Instance][search][components][criteria][search-type] BetweenComponent', () => {
    let component: BetweenComponent;
    let fixture: ComponentFixture<BetweenComponent>;
    let spyOnLabel;
    let attribute: Attribute;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [BetweenComponent],
            imports: [ReactiveFormsModule]
        });
        fixture = TestBed.createComponent(BetweenComponent);
        component = fixture.componentInstance;
        component.attribute = { ...attribute, id: 1, placeholder_min: 'min', placeholder_max: 'max' };
        fixture.detectChanges();
        spyOnLabel = jest.spyOn(component.form.controls['label'], 'setValue');
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('setCriterion(criterion: Criterion) should set values in form on dateRange and  label with bw', () => {
        let criterion: Criterion = { id: 1, type: 'between' };
        component.setCriterion(criterion);
        expect(spyOnLabel).toHaveBeenCalledTimes(1);
        expect(spyOnLabel).toHaveBeenCalledWith('bw');
    });

    it('setCriterion(criterion: Criterion) should set value on label when the  criterion type is not between', () => {
        let criterion: Criterion = { id: 1, type: 'test' };
        component.setCriterion(criterion);
        expect(spyOnLabel).toHaveBeenCalledTimes(1);
    });

    it('setCriterion(criterion: Criterion) should set value on label when criterion param is undefined', () => {
        component.setCriterion(null);
        expect(spyOnLabel).toHaveBeenCalledWith('bw');
    });

    it('getCriterion() should return an criterion of type between', () => {
        expect(component.getCriterion().type).toEqual('between');
    });

    it('getPlaceholderMin() should return "min"', () => {
        expect(component.getPlaceholderMin()).toEqual('min');
    });

    it('getPlaceholderMin() should return ""', () => {
        component.attribute = { ...component.attribute, placeholder_min: null };
        expect(component.getPlaceholderMin()).toEqual('');
    });

    it('getPlaceholderMax() should return "max"', () => {
        component.attribute = { ...component.attribute, placeholder_max: null };
        expect(component.getPlaceholderMax()).toEqual('');
    });

    it('getPlaceholderMax() should return ""', () => {
        expect(component.getPlaceholderMax()).toEqual('max');
    });

    it('isValid() should return true', () => {
        component.form.controls['min'].setValue(1);
        expect(component.isValid()).toBe(1);
    });

    it('labelOnChange() should disable max and min', () => {
        component.form.controls['label'].setValue('nl');
        expect(component.form.controls['min'].disabled).toBe(false);
        expect(component.form.controls['max'].disabled).toBe(false);
        component.labelOnChange();
        expect(component.form.controls['min'].disabled).toBe(true);
        expect(component.form.controls['max'].disabled).toBe(true);
    });
    it('labelOnChange() should enable max and min', () => {
        component.form.controls['max'].disable();
        component.form.controls['min'].disable();
        expect(component.form.controls['max'].enabled).toBe(false);
        expect(component.form.controls['min'].enabled).toBe(false);
        component.labelOnChange();
        expect(component.form.controls['max'].enabled).toBe(true);
        expect(component.form.controls['min'].enabled).toBe(true);
    });
});
