/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { FieldCriterion } from 'src/app/instance/store/models';
import { Attribute } from 'src/app/metamodel/models';
import { RadioComponent } from './radio.component';

describe('[Instance][search][components][criteria][search-type] RadioComponent', () => {
    let component: RadioComponent;
    let fixture: ComponentFixture<RadioComponent>;
    let attribute: Attribute;
    
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [RadioComponent],
            imports: [ReactiveFormsModule]
        });
        fixture = TestBed.createComponent(RadioComponent);
        component = fixture.componentInstance;
        component.attribute = {
            ...attribute, options: [
                { label: 'test1', display: 1, value: 'test1' },
                { label: 'test2', display: 2, value: 'test2' }
            ],
            operator: 'test',
            placeholder_min: 'min'
        }
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('setCriterion(criterion: Criterion) should set radio value and call operatorOnChange when criterion param  defined', () => {
        let spy = jest.spyOn(component, 'operatorOnChange');
        expect(component.form.controls['radio'].value).toEqual('');
        component.setCriterion({ id: 1, value: 'test', type: 'test' } as FieldCriterion);
        expect(component.form.controls['radio'].value).toEqual('test');
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('setCriterion(criterion: Criterion) should set operator value to test value when criterion param is not defined', () => {
        expect(component.form.controls['operator'].value).toEqual('');
        component.setCriterion(null);
        expect(component.form.controls['operator'].value).toEqual('test');
    });

    it('getCriterion() should return a criterion of type FieldCriterion', () => {
        component.form.controls['radio'].setValue('test1');
        expect(component.getCriterion().type).toEqual('field');
    });

    it('isValid() should true when form is valid or when operator value is nl or nnl', () => {
        expect(component.isValid()).toBe(false);
        component.form.controls['operator'].setValue('test');
        component.form.controls['radio'].setValue('test');
        expect(component.isValid()).toBe(true);
        component.form.controls['operator'].setValue('nnl');
        expect(component.isValid()).toBe(true);
        component.form.controls['operator'].setValue('nl');
        expect(component.isValid()).toBe(true);
    });

    it('operatorOnChange() should disable radio formcontrol when operator value is nl or nnl', () => {
        expect(component.form.controls['radio'].disabled).toBe(false);
        component.form.controls['operator'].setValue('nl');
        component.operatorOnChange();
        expect(component.form.controls['radio'].disabled).toBe(true);
    });

    it('operatorOnChange() should enable radio formcontrol when operator value is not  nl or nnl', () => {
        component.form.controls['radio'].disable();
        expect(component.form.controls['radio'].enabled).toBe(false);
        component.operatorOnChange();
        expect(component.form.controls['radio'].enabled).toBe(true);
    });
});
