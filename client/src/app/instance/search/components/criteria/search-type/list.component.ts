/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { AbstractSearchTypeComponent } from './abstract-search-type.component';
import { Criterion, ListCriterion, FieldCriterion } from 'src/app/instance/store/models';

@Component({
    selector: 'app-list',
    templateUrl: 'list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListComponent extends AbstractSearchTypeComponent {
    labels = [
        { value: 'in', label: 'in' },
        { value: 'nl', label: 'null' },
        { value: 'nnl', label: 'not null' }
    ];

    constructor() {
        super();
        this.form = new UntypedFormGroup({
            label: new UntypedFormControl(''),
            list: new UntypedFormControl('', [Validators.required])
        });
    }

    override setCriterion(criterion: Criterion) {
        super.setCriterion(criterion);
        if (criterion) {
            if (criterion.type === 'list') {
                const listCriterion = criterion as ListCriterion;
                this.form.controls['list'].setValue(listCriterion.values.join('\n'));
                this.form.controls['label'].setValue('in');
            } else {
                const fieldCriterion = criterion as FieldCriterion;
                this.form.controls['label'].setValue(fieldCriterion.operator);
            }
        } else {
            this.form.controls['label'].setValue('in');
        }
        if (!this.attribute.null_operators_enabled) {
            this.labels = this.labels.filter(operator => !['nl', 'nnl'].includes(operator.value));
        }
    }

    /**
     * Return new criterion
     *
     * @return Criterion
     */
    getCriterion(): Criterion {
        return {
            id: this.attribute.id,
            type: 'list',
            values: this.form.value.list.split('\n')
        } as ListCriterion;
    }

    /**
     * Return placeholder.
     *
     * @return string
     */
    getPlaceholder(): string {
        if (!this.attribute.placeholder_min) {
            return '';
        } else {
            return this.attribute.placeholder_min;
        }
    }

    labelOnChange() {
        if (this.form.controls['label'].value === 'nl' || this.form.controls['label'].value === 'nnl') {
            this.nullOrNotNull = this.form.controls['label'].value;
            this.form.controls['list'].disable();
        } else {
            this.nullOrNotNull = '';
            this.form.controls['list'].enable();
        }
    }
}
