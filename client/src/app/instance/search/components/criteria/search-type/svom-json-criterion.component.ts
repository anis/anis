/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy, OnChanges, SimpleChanges } from '@angular/core';

import { JsonCriterionComponent } from './json-criterion.component';
import { SvomKeyword } from 'src/app/instance/store/models';

@Component({
    selector: 'app-svom-json-criterion',
    templateUrl: 'svom-json-criterion.component.html',
    styleUrls: [ 'svom-json-criterion.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SvomJsonCriterionComponent extends JsonCriterionComponent implements OnChanges {
    @Input() svomKeywords: SvomKeyword[];

    expectedValues: string[] = [];

    ngOnChanges(changes: SimpleChanges): void {
        if (changes['svomKeywords'] && changes['svomKeywords'].currentValue.length > 1 && this.form.controls['path'].value) {
            this.changeKeyword(this.form.controls['path'].value);
        }
    }

    getOrderedKeywords(): SvomKeyword[] {
        return this.svomKeywords.sort(
            (a, b) => this.getKeywordValue(a).localeCompare(this.getKeywordValue(b))
        );
    }

    changeKeyword(pathValue: string): void {
        this.expectedValues = [];
        if (pathValue) {
            const [extension, name] = pathValue.split(',');
            const svomKeyword = this.svomKeywords.find(svomKeyword => 
                svomKeyword.name === name && svomKeyword.extension === extension
            );
            if (svomKeyword && svomKeyword.expected_values && svomKeyword.expected_values.values) {
                this.expectedValues = svomKeyword.expected_values.values.map(value => value.toString());
            }
        }
    }

    /**
     * Transform a SVOM json Keyword to as path value (anis json search)
     * 
     * @param svomKeyword Keyword selected by user
     * @returns string path value
     */
    getKeywordValue(svomKeyword: SvomKeyword): string {
        return `${svomKeyword.extension},${svomKeyword.name}`
    }
}
