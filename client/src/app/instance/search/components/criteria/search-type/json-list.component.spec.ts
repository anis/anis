/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { UntypedFormGroup} from '@angular/forms';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

import { Attribute } from 'src/app/metamodel/models';
import { JsonListComponent } from './json-list.component';

describe('[Instance][search][components][criteria][search-type] JsonComponent', () => {
    let component: JsonListComponent;
    let fixture: ComponentFixture<JsonListComponent>;
    let attribute: Attribute;

    beforeEach(() => {
        @Component({ selector: 'app-json-criterion', template: '' })
        class JsonCriterionStubComponent {
            @Input() form: UntypedFormGroup;
            @Input() index: number;
            @Input() isFirst: boolean;
            @Input() isLast: boolean;
        }

        @Component({ selector: 'app-attribute-label', template: '' })
        class AttributeLabelStubComponent {
            @Input() label: string;
            @Input() description: string;
        }

        TestBed.configureTestingModule({
            declarations: [
                JsonListComponent,
                JsonCriterionStubComponent,
                AttributeLabelStubComponent
            ],
            imports: [
                ReactiveFormsModule,
                NgSelectModule
            ],
        });
        fixture = TestBed.createComponent(JsonListComponent);
        component = fixture.componentInstance;
        component.attribute = {
            ...attribute, options: [
                { label: 'test1', display: 1, value: 'test1' },
                { label: 'test2', display: 2, value: 'test2' }
            ],
            operator: 'test',
            placeholder_min: 'min'
        }
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('createJsonCriterionForm() should return a new form group', () => {
        const formGroup = component.createJsonCriterionForm();
        expect(Object.keys(formGroup.controls).length).toBe(3);
    });

    it('getJsonCriteriaListFormArray() should return list of criteria', () => {
        const formArray = component.getJsonCriteriaListFormArray();
        expect(formArray.length).toBe(1);
    });

    it('getJsonCriterionForm() should return one criterion form', () => {
        const formArray = component.getJsonCriteriaListFormArray();
        const formGroup = component.getJsonCriterionForm(formArray.at(0));
        expect(Object.keys(formGroup.controls).length).toBe(3);
    });
});
