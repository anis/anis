/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { OutputTabsComponent } from './output-tabs.component';
import { Attribute, OutputCategory, OutputFamily } from 'src/app/metamodel/models';
import { OUTPUT_FAMILY_LIST } from 'src/test-data';

describe('[Instance][Search][Component][Output] OutputTabsComponent', () => {
    @Component({ selector: 'app-output-by-family', template: '' })
    class OutputByFamilyStubComponent {
        @Input() outputFamily: OutputFamily;
        @Input() outputCategoryList: OutputCategory[];
        @Input() attributeList: Attribute[];
        @Input() outputList: number[];
        @Input() designColor: string;
    }

    let component: OutputTabsComponent;
    let fixture: ComponentFixture<OutputTabsComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                OutputTabsComponent,
                OutputByFamilyStubComponent
            ],
            imports: [
                AccordionModule.forRoot(),
                BrowserAnimationsModule
            ]
        });
        fixture = TestBed.createComponent(OutputTabsComponent);
        component = fixture.componentInstance;
        component.outputFamilyList = [ ...OUTPUT_FAMILY_LIST ];
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('getOutputFamilyList() should return an array with one outputFamilyList with id 2', () => {
        let result = component.getOutputFamilyList();
        expect(result.length).toEqual(1);
        expect(result[0].id).toEqual(1);
    })
});
