/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OutputByCategoryComponent } from './output-by-category.component';
import { DATASET, ATTRIBUTE_LIST } from 'src/test-data';

describe('[Instance][Search][Component][Output] OutputByCategoryComponent', () => {
    let component: OutputByCategoryComponent;
    let fixture: ComponentFixture<OutputByCategoryComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [OutputByCategoryComponent]
        });
        fixture = TestBed.createComponent(OutputByCategoryComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#isSelected(idOutput) should return true if output is selected', () => {
        component.outputList = [1];
        expect(component.isSelected(1)).toBeTruthy();
    });

    it('#isSelected(idOutput) should return false if output is not selected', () => {
        component.outputList = [1];
        expect(component.isSelected(2)).toBeFalsy();
    });

    it('#toggleSelection(idOutput) should remove idOutput from outputList and raise change event', () => {
        component.outputList = [1];
        const idOutput = 1;
        const expectedOutputList = [];
        component.change.subscribe((event: number[]) => expect(event).toEqual(expectedOutputList));
        component.toggleSelection(idOutput);
    });

    it('#toggleSelection(idOutput) should add idOutput to outputList and raise change event', () => {
        component.outputList = [];
        const idOutput = 1;
        const expectedOutputList = [1];
        component.change.subscribe((event: number[]) => expect(event).toEqual(expectedOutputList));
        component.toggleSelection(idOutput);
    });

    it('#selectAll() should add all outputs to outputList and raise change event', () => {
        component.attributeList = [ ...ATTRIBUTE_LIST ];
        component.outputList = [];
        const expectedOutputList = [1, 2, 3, 4];
        component.change.subscribe((event: number[]) => expect(event).toEqual(expectedOutputList));
        component.selectAll();
    });

    it('#unselectAll() should remove all outputs to outputList and raise change event', () => {
        component.attributeList = [ ...ATTRIBUTE_LIST ];
        component.dataset = { ...DATASET };
        component.outputList = [1, 2];
        const expectedOutputList = [];
        component.change.subscribe((event: number[]) => expect(event).toEqual(expectedOutputList));
        component.unselectAll();
    });
});
