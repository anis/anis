/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { OutputFamily, OutputCategory, Attribute, Dataset } from 'src/app/metamodel/models';

@Component({
    selector: 'app-output-by-family',
    templateUrl: 'output-by-family.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutputByFamilyComponent {
    @Input() dataset: Dataset;
    @Input() outputFamily: OutputFamily;
    @Input() attributeList: Attribute[];
    @Input() outputList: number[];
    @Output() change: EventEmitter<number[]> = new EventEmitter();

    /**
     * Returns category list not empty
     *
     * @return Category[]
     */
    getCategoryListByFamily(): OutputCategory[] {
        return this.outputFamily.output_categories
            .filter(category => this.getAttributeByCategory(category.id).length > 0);
    }

    /**
     * Returns output list that belongs to the given category ID.
     *
     * @param  {number} idCategory - The output category ID.
     *
     * @return Attribute[]
     */
    getAttributeByCategory(idCategory: number): Attribute[] {
        return this.attributeList.filter(attribute => attribute.id_output_category === `${this.outputFamily.id}_${idCategory}`);
    }

    /**
     * Checks if all outputs for the given category ID are selected.
     *
     * @param  {number} idCategory - The output category ID.
     *
     * @return boolean
     */
    getIsAllSelected(idCategory: number): boolean {
        const attributeListId = this.getAttributeByCategory(idCategory).map(a => a.id);
        const filteredOutputList = this.outputList.filter(id => attributeListId.indexOf(id) > -1);
        return attributeListId.length === filteredOutputList.length;
    }

    /**
     * Checks if all outputs for the given category ID are unselected.
     *
     * @param  {number} idCategory - The output category ID.
     *
     * @return boolean
     */
    getIsAllUnselected(idCategory: number): boolean {
        const attributeListId = this.getAttributeByCategory(idCategory).map(a => a.id);
        const filteredOutputList = this.outputList.filter(id => attributeListId.indexOf(id) > -1);
        return filteredOutputList.length === 0;
    }

    /**
     * Emits update output list event with updated sorted output list given.
     *
     * @param  {number[]} clonedOutputList - The updated output list.
     *
     * @fires EventEmitter<number[]>
     */
    emitChange(clonedOutputList: number[]): void {
        this.change.emit(
            this.attributeList
                .filter(a => clonedOutputList.indexOf(a.id) > -1)
                .map(a => a.id)
        );
    }
}
