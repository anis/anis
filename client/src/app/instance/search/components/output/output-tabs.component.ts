/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { OutputFamily, Attribute, Dataset } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Search output tabs component.
 */
@Component({
    selector: 'app-output-tabs',
    templateUrl: 'output-tabs.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OutputTabsComponent {
    @Input() dataset: Dataset;
    @Input() outputFamilyList: OutputFamily[];
    @Input() attributeList: Attribute[];
    @Input() outputList: number[];
    @Output() change: EventEmitter<number[]> = new EventEmitter();

    getOutputFamilyList() {
        return this.outputFamilyList
            .filter(outputFamily => outputFamily.output_categories.length > 0);
    }
}
