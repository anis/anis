/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { OutputByFamilyComponent } from './output-by-family.component';
import { Attribute, OutputCategory } from 'src/app/metamodel/models';
import { ATTRIBUTE_LIST, OUTPUT_FAMILY } from 'src/test-data';

describe('[Instance][Search][Component][Output] OutputByFamilyComponent', () => {
    @Component({ selector: 'app-output-by-category', template: '' })
    class OutputByCategoryStubComponent {
        @Input() categoryLabel: string;
        @Input() attributeList: Attribute[];
        @Input() outputList: number[];
        @Input() designColor: string;
        @Input() isAllSelected: boolean;
        @Input() isAllUnselected: boolean;
    }

    let component: OutputByFamilyComponent;
    let fixture: ComponentFixture<OutputByFamilyComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                OutputByFamilyComponent,
                OutputByCategoryStubComponent
            ],
            imports: [
                AccordionModule.forRoot(),
                BrowserAnimationsModule
            ]
        });
        fixture = TestBed.createComponent(OutputByFamilyComponent);
        component = fixture.componentInstance;
        component.attributeList = [ ...ATTRIBUTE_LIST ];
        component.outputFamily = { ...OUTPUT_FAMILY };
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getCategoryListByFamily(idFamily) should return categories belonging to idFamily', () => {
        const sortedCategoryList: OutputCategory[] = component.getCategoryListByFamily();
        expect(sortedCategoryList.length).toBe(2);
        expect(sortedCategoryList[0].id).not.toBe(3);
        expect(sortedCategoryList[1].id).not.toBe(3);
    });

    it('#getAttributeByCategory(idCategory) should return attributes belonging to idCategory', () => {
        expect(component.getAttributeByCategory(2).length).toBe(1);
        expect(component.getAttributeByCategory(1)[0].id).toBe(2);
    });

    it('#getIsAllSelected(idCategory) should return true if all outputs of idCategory are selected', () => {
        component.outputList = [2, 4];
        expect(component.getIsAllSelected(1)).toBeTruthy();
    });

    it('#getIsAllSelected(idCategory) should return false if not all outputs of idCategory are selected', () => {
        component.outputList = [2];
        expect(component.getIsAllSelected(1)).toBeFalsy();
    });

    it('#getIsAllUnselected(idCategory) should return true if all outputs of idCategory are not selected', () => {
        component.outputList = [];
        expect(component.getIsAllUnselected(1)).toBeTruthy();
    });

    it('#getIsAllUnselected(idCategory) should return false if not all outputs of idCategory are not selected', () => {
        component.outputList = [2];
        expect(component.getIsAllUnselected(1)).toBeFalsy();
    });

    it('#emitChange(outputList) should raise change event', () => {
        const expectedOutputList = [1];
        component.change.subscribe((event: number[]) => expect(event).toEqual(expectedOutputList));
        component.emitChange([1]);
    });
});
