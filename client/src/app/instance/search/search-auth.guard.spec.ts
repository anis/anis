/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold } from 'jasmine-marbles';
import { AppConfigService } from 'src/app/app-config.service';
import { SearchAuthGuard } from './search-auth.guard';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as datasetGroupSelector from 'src/app/metamodel/selectors/dataset-group.selector';
import * as authSelector from 'src/app/user/store/selectors/auth.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { Dataset } from 'src/app/metamodel/models';
import { DATASET } from 'src/test-data';

describe('[instance][search] SearchAuthGuard', () => {
    let store: MockStore;
    let searchAuthGuard: SearchAuthGuard;
    let config: AppConfigService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                provideMockStore({}),
                {
                    provide: AppConfigService,
                    useValue: { authenticationEnabled: false },
                },
            ],
        });
        config = TestBed.inject(AppConfigService);
        store = TestBed.inject(MockStore);
        searchAuthGuard = TestBed.inject(SearchAuthGuard);
    });

    it('should create component', () => {
        expect(searchAuthGuard).toBeTruthy();
    });

    it('should  return [] when datasetListIsLoaded and datasetGroupListIsLoaded are false', () => {
        store.overrideSelector(
            datasetSelector.selectDatasetListIsLoaded,
            false,
        );
        store.overrideSelector(
            datasetGroupSelector.selectDatasetGroupListIsLoaded,
            false,
        );
        let result = cold('a', { a: searchAuthGuard.canActivate() });
        let expected = cold('a', { a: [] });
        expect(result).toBeObservable(expected);
    });

    it('should return true when authenticationEnabled is true', () => {
        config.authenticationEnabled = true;

        store.overrideSelector(datasetSelector.selectDatasetListIsLoaded, true);
        store.overrideSelector(
            datasetGroupSelector.selectDatasetGroupListIsLoaded,
            true,
        );
        store.overrideSelector(
            datasetSelector.selectDatasetByRouteName,
            DATASET,
        );
        store.overrideSelector(authSelector.selectUserRoles, ['test']);
        store.overrideSelector(authSelector.selectIsAuthenticated, true);
        store.overrideSelector(datasetGroupSelector.selectAllDatasetGroups, [
            {
                datasets: [],
                id: 1,
                instance_name: 'test',
                role: 'test',
            },
        ]);
        store.overrideSelector(
            instanceSelector.selectInstanceNameByRoute,
            'my-instance',
        );
        let result = searchAuthGuard.canActivate();
        let expected = cold('a', { a: true });
        expect(result).toBeObservable(expected);
    });

    it('should  return true when user is authenticated and accessible is true', () => {
        store.overrideSelector(datasetSelector.selectDatasetListIsLoaded, true);
        store.overrideSelector(
            datasetGroupSelector.selectDatasetGroupListIsLoaded,
            true,
        );
        store.overrideSelector(
            datasetSelector.selectDatasetByRouteName,
            DATASET,
        );
        store.overrideSelector(authSelector.selectUserRoles, ['test']);
        store.overrideSelector(authSelector.selectIsAuthenticated, true);
        store.overrideSelector(datasetGroupSelector.selectAllDatasetGroups, [
            {
                datasets: ['dataset_name', 'test2'],
                id: 1,
                instance_name: 'test',
                role: 'test',
            },
        ]);
        store.overrideSelector(
            instanceSelector.selectInstanceNameByRoute,
            'my-instance',
        );
        let result = searchAuthGuard.canActivate();
        let expected = cold('a', { a: true });
        expect(result).toBeObservable(expected);
    });
});
