/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchComponent } from './search.component';
import { DatasetComponent } from './containers/dataset.component';
import { CriteriaComponent } from './containers/criteria.component';
import { OutputComponent } from './containers/output.component';
import { ResultComponent } from './containers/result.component';
import { SearchAuthGuard } from './search-auth.guard';
import { SearchTitleResolver } from './search-title.resolver';

const routes: Routes = [
    {
        path: '', component: SearchComponent, children: [
            { path: '', redirectTo: 'dataset', pathMatch: 'full' },
            { path: 'dataset', component: DatasetComponent, title: SearchTitleResolver },
            { path: 'dataset/:dname', canActivate: [SearchAuthGuard], component: DatasetComponent, title: SearchTitleResolver },
            { path: 'criteria/:dname', canActivate: [SearchAuthGuard], component: CriteriaComponent, title: SearchTitleResolver },
            { path: 'output/:dname', canActivate: [SearchAuthGuard], component: OutputComponent, title: SearchTitleResolver },
            { path: 'result/:dname', canActivate: [SearchAuthGuard], component: ResultComponent, title: SearchTitleResolver }
        ]
    },
    { path: 'detail', loadChildren: () => import('./detail/detail.module').then(m => m.DetailModule) }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SearchRoutingModule { }

export const routedComponents = [
    SearchComponent,
    DatasetComponent,
    CriteriaComponent,
    OutputComponent,
    ResultComponent
];
