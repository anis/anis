/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SortByOutputDisplayPipe } from './sort-by-output-display.pipe';
import { ATTRIBUTE_LIST} from 'src/test-data';

describe('[Instance][Search][Pipe] SortByOutputDisplayPipe', () => {
    let pipe = new SortByOutputDisplayPipe();
    let attributeList = [
        ATTRIBUTE_LIST[1],
        ATTRIBUTE_LIST[0]
    ];

    it('orders attributeList by output display', () => {
        expect(pipe.transform(attributeList)[0].id).toBe(1);
        expect(pipe.transform(attributeList)[1].id).toBe(2);
    });
});
