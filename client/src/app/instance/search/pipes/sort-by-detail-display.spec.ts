/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SortByDetailDisplay } from './sort-by-detail-display';
import { ATTRIBUTE_LIST } from 'src/test-data';

describe('[Instance][Search][Pipe] SortByDetailDisplay', () => {
    let pipe = new SortByDetailDisplay();
    let attributeList = [
        ATTRIBUTE_LIST[1],
        ATTRIBUTE_LIST[0]
    ];

    it('sorts attributeList by display detail', () => {
        expect(pipe.transform(attributeList)[0].id).toBe(1);
        expect(pipe.transform(attributeList)[1].id).toBe(2);
    });
});
