/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Store } from '@ngrx/store';

import { Attribute, Dataset, Instance, OutputCategory, OutputFamily } from 'src/app/metamodel/models';
import { SearchQueryParams } from 'src/app/instance/store/models';
import * as searchActions from 'src/app/instance/store/actions/search.actions';
import * as sampActions from 'src/app/samp/samp.actions';

@Component({
    selector: 'app-display-object-by-output-family',
    templateUrl: 'display-object-by-output-family.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayObjectByOutputFamilyComponent {
    @Input() object: any;
    @Input() dataset: Dataset;
    @Input() instance: Instance;
    @Input() attributeList: Attribute[];
    @Input() outputFamilyList: OutputFamily[];
    @Input() queryParams: SearchQueryParams;
    @Input() sampRegistered: boolean;
    @Input() outputFamilyId: number;
    @Input() isOpened: boolean;

    constructor(protected store: Store<{}>) { }

    getFamily() {
        return this.outputFamilyList.find(outputFamily => outputFamily.id === this.outputFamilyId);
    }

    getOutputCategoryListByFamily(idOutputFamily: number): OutputCategory[] {
        return this.outputFamilyList
            .find(outputFamily => outputFamily.id === idOutputFamily).output_categories
            .filter(outputCategory => this.getAttributeListByOutputCategory(outputCategory.id).length > 0);
    }

    getAttributeListByOutputCategory(idOutputCategory: number): Attribute[] {
        return this.attributeList.filter(attribute => attribute.id_detail_output_category === `${this.outputFamilyId}_${idOutputCategory}`);
    }

    /**
     * Dispatches action to launch the file download
     * 
     * @param { url: string, filename: string } download
     */
    downloadFile(download: { url: string, filename: string }): void {
        this.store.dispatch(searchActions.downloadFile(download));
    }

    /**
     * Dispatches action to broadcast data.
     *
     * @param  {string} url - The broadcast URL.
     */
    broadcastVotable(url: string): void {
        this.store.dispatch(sampActions.broadcastVotable({ url }));
    }

    /**
     * Dispatches action to broadcast image.
     *
     * @param  {string} url - The broadcast URL.
     */
    broadcastImage(url: string): void {
        this.store.dispatch(sampActions.broadcastImage({ url }));
    }
}
