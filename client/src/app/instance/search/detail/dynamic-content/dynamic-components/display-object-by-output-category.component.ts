/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Store } from '@ngrx/store';

import { Attribute, Dataset, Instance, OutputFamily } from 'src/app/metamodel/models';
import { SearchQueryParams } from 'src/app/instance/store/models';
import * as searchActions from 'src/app/instance/store/actions/search.actions';
import * as sampActions from 'src/app/samp/samp.actions';

@Component({
    selector: 'app-display-object-by-output-category',
    templateUrl: 'display-object-by-output-category.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayObjectByOutputCategoryComponent {
    @Input() object: any;
    @Input() dataset: Dataset;
    @Input() instance: Instance;
    @Input() attributeList: Attribute[];
    @Input() outputFamilyList: OutputFamily[];
    @Input() queryParams: SearchQueryParams;
    @Input() sampRegistered: boolean;
    @Input() outputFamilyId: number;
    @Input() outputCategoryId: number;
    @Input() isOpened: boolean;

    constructor(protected store: Store<{}>) { }

    getCategory() {
        return this.outputFamilyList
            .find(outputFamily => outputFamily.id === this.outputFamilyId).output_categories
            .find(outputCategory => outputCategory.id === this.outputCategoryId);
    }

    getAttributeListByOutputCategory(): Attribute[] {
        return this.attributeList.filter(attribute => attribute.id_detail_output_category === `${this.outputFamilyId}_${this.outputCategoryId}`);
    }

    /**
     * Dispatches action to launch the file download
     * 
     * @param { url: string, filename: string } download
     */
    downloadFile(download: { url: string, filename: string }): void {
        this.store.dispatch(searchActions.downloadFile(download));
    }

    /**
     * Dispatches action to broadcast data.
     *
     * @param  {string} url - The broadcast URL.
     */
    broadcastVotable(url: string): void {
        this.store.dispatch(sampActions.broadcastVotable({ url }));
    }

    /**
     * Dispatches action to broadcast image.
     *
     * @param  {string} url - The broadcast URL.
     */
    broadcastImage(url: string): void {
        this.store.dispatch(sampActions.broadcastImage({ url }));
    }
}
