/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Attribute } from 'src/app/metamodel/models';

@Component({
    selector: 'app-display-value-geobox',
    templateUrl: 'display-value-geobox.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayValueGeoboxComponent {
    @Input() object: any;
    @Input() attributeList: Attribute[];
    @Input() attributeId: number;
    @Input() separator: string;

    getAttributeById() {
        return this.attributeList.find(attribute => attribute.id === this.attributeId);
    }

    getGeoValues() {
        const value = this.object[this.getAttributeById().label] as string;
        if (!value) {
            return [];
        } else {
            return value.split(this.separator);
        }
    }
}
