/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as searchActions from 'src/app/instance/store/actions/search.actions';
import * as sampActions from 'src/app/samp/samp.actions';
import * as sampSelector from 'src/app/samp/samp.selector';
import { Instance, Dataset, Attribute, Image } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';
import { getHost } from 'src/app/shared/utils';

@Component({
    selector: 'app-display-fits-cut',
    templateUrl: 'display-fits-cut.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayFitsCutComponent {
    @Input() object: any;
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() imageList: Image[];
    @Input() imageId: number;
    @Input() attributeList: Attribute[];
    @Input() attributeRaId: number;
    @Input() attributeDecId: number;
    @Input() radius: number;
    @Input() hduNumber: string;

    loading = true;
    error = false;

    public sampRegistered: Observable<boolean>;

    constructor(private appConfig: AppConfigService, private store: Store<{}>) {
        this.sampRegistered = this.store.select(sampSelector.selectRegistered);
    }

    onLoad() {
        this.loading = false;
    }

    onError(event) {
        if (this.dataset.public || ((!this.instance.public || !this.dataset.public) && event.target.attributes['src'].value === 'not_found')) {
            this.loading = false;
            this.error = true;
        }
    }

    getHref() {
        const image = this.getImage();
        let href = `${getHost(this.appConfig.servicesUrl)}/fits/fits-cut-to-png/${this.instance.name}/${this.dataset.name}?filename=${image.file_path}`;
        href += `&ra=${this.getRaValue()}`;
        href += `&dec=${this.getDecValue()}`;
        href += `&radius=${this.radius}`;
        href += `&stretch=${image.stretch}`;
        href += `&pmin=${image.pmin}`;
        href += `&pmax=${image.pmax}`;
        href += `&axes=false`;

        if (this.hduNumber) {
            href += `&hdu_number=${this.hduNumber}`;
        } else if (image.hdu_number) {
            href += `&hdu_number=${image.hdu_number}`;
        }

        return href;
    }

    getFitsCutUrl() {
        const image = this.getImage();
        let url = `${getHost(this.appConfig.servicesUrl)}/fits/fits-cut/${this.instance.name}/${this.dataset.name}?filename=${image.file_path}`;
        url += `&ra=${this.getRaValue()}`;
        url += `&dec=${this.getDecValue()}`;
        url += `&radius=${this.radius}`;

        if (this.hduNumber) {
            url += `&hdu_number=${this.hduNumber}`;
        }

        return url;
    }

    saveFitsCutFile(event) {
        event.preventDefault();

        const url = this.getFitsCutUrl();
        const image = this.getImage();
        const filename = `fits_cut_${this.getRaValue()}_${this.getDecValue()}_${this.radius}_${image.file_path.substring(image.file_path.lastIndexOf('/') + 1)}`;

        this.store.dispatch(searchActions.downloadFile({url, filename}));
    }

    broadcast() {
        this.store.dispatch(sampActions.broadcastImage({ url: this.getFitsCutUrl() }));
    }

    getImage() {
        return this.imageList.find(image => image.id === this.imageId);
    }

    getRaValue() {
        const attributeRa = this.attributeList.find(attribute => attribute.id === this.attributeRaId);
        return this.object[attributeRa.label];
    }

    getDecValue() {
        const attributeDec = this.attributeList.find(attribute => attribute.id === this.attributeDecId);
        return this.object[attributeDec.label];
    }
}
