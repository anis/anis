/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";

import { AppConfigService } from "src/app/app-config.service";
import { Attribute, Dataset } from "src/app/metamodel/models";
import { DisplayImageComponent } from "./display-image.component";
import * as utils from 'src/app/shared/utils';
import { DATASET, INSTANCE } from "src/test-data";

describe('[instance][search][detail][dynamic-content][dynamic-components] DisplayImageComponent', () => {
    let component: DisplayImageComponent;
    let fixture: ComponentFixture<DisplayImageComponent>;
    let attribute: Attribute;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DisplayImageComponent],
            providers: [
                { provide: AppConfigService, useValue: { servicesUrl: 'test', apiUrl: 'test' } }
            ]
        });
        fixture = TestBed.createComponent(DisplayImageComponent);
        component = fixture.componentInstance;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('getValue() should return the source image properly if componen.type == \"fits\"', () => {
        let expected = 'test/fits/fits-to-png/my-instance/my-dataset?filename=test&stretch=linear&pmin=0.25&pmax=99.75&axes=true';
        component.dataset = { ...DATASET };
        component.instance = { ...INSTANCE };
        component.type = 'fits';
        component.getPath = jest.fn().mockImplementationOnce(() => 'test');
        expect(component.getValue()).toEqual(expected);
    });

    it('getValue() should return the source image properly if componen.type == \"image\"', () => {
        let expected = 'test/instance/my-instance/dataset/my-dataset/file-explorertest';
        component.dataset = { ...DATASET };
        component.instance = { ...INSTANCE };
        component.getPath = jest.fn().mockImplementationOnce(() => 'test');
        let spy = jest.spyOn(utils, 'getHost');
        spy.mockImplementationOnce(() => 'test');
        component.type = 'image';
        expect(component.getValue()).toEqual(expected);
    });

    it('getValue() should return test_result if componen.type is not specified', () => {
        let expected = 'test_result';
        component.getAttributeImage = jest.fn().mockImplementationOnce(() => {
            return { ...attribute, label: 'test' } as Attribute
        });
        component.object = {
            test: 'test_result'
        }
        expect(component.getValue()).toEqual(expected);
    });

    it('getPath() should return /test', () => {
        component.object = { test: 'test' };
        component.getAttributeImage = jest.fn().mockImplementationOnce(() => {
            return { ...attribute, label: 'test' } as Attribute
        });
        expect(component.getPath()).toEqual('/test');
    });

    it('getAttributeImage() should return an attribute with id 2 ', () => {
        component.attributeList = [
            { ...attribute, id: 2 },
            { ...attribute, id: 1 }
        ];
        component.attributeImageId = 2;
        expect(component.getAttributeImage().id).toEqual(2);
    });

    it('getStyle() should return {"width": "10", "height": "5" }', () => {
        component.width = '10';
        component.height = '5';
        expect(component.getStyle()).toEqual({ "width": "10", "height": "5" });
    });
});
