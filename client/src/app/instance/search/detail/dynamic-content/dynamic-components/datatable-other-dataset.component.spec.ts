/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { DatasetService } from 'src/app/metamodel/services/dataset.service';
import { AttributeService } from 'src/app/metamodel/services/attribute.service';
import { AnisHttpClientService } from 'src/app/metamodel/anis-http-client.service';
import { AppConfigService } from 'src/app/app-config.service';
import { DatatableOtherDatasetComponent } from './datatable-other-dataset.component';
import { INSTANCE } from 'src/test-data';
import { Instance, Dataset, Attribute } from 'src/app/metamodel/models';

describe('DatatableOtherDatasetComponent', () => {
    let component: DatatableOtherDatasetComponent;
    let fixture: ComponentFixture<DatatableOtherDatasetComponent>;

    @Component({
        selector: 'app-detail-datatable',
        template: '',
    })
    class DetailDatatableComponent {
        @Input() instance: Instance;
        @Input() dataset: Dataset;
        @Input() a: string;
        @Input() attributeList: Attribute[];
        @Input() data: any[];
        @Input() renderersEnabled: boolean;
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DatatableOtherDatasetComponent, DetailDatatableComponent],
            providers: [
                HttpClient,
                DatasetService,
                AttributeService,
                AnisHttpClientService,
                { provide: AppConfigService, useValue: { apiUrl: 'test' } },
            ],
            imports: [HttpClientModule],
        });
        fixture = TestBed.createComponent(DatatableOtherDatasetComponent);
        component = fixture.componentInstance;
        component.instance = { ...INSTANCE };

        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
