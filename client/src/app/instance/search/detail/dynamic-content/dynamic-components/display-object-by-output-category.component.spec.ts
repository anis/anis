/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { DisplayObjectByOutputCategoryComponent } from './display-object-by-output-category.component';
import * as searchActions from 'src/app/instance/store/actions/search.actions';
import { ATTRIBUTE_LIST, OUTPUT_FAMILY_LIST } from 'src/test-data';
import { AccordionModule } from 'ngx-bootstrap/accordion';

describe('[instance][search][detail][dynamic-content][dynamic-components] DisplayObjectByOutputCategoryComponent', () => {
    let component: DisplayObjectByOutputCategoryComponent;
    let fixture: ComponentFixture<DisplayObjectByOutputCategoryComponent>;
    let store: MockStore;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DisplayObjectByOutputCategoryComponent
            ],
            providers: [
                provideMockStore({}),
            ],
            imports: [
                BrowserAnimationsModule,
                AccordionModule,
            ],
        })
        fixture = TestBed.createComponent(DisplayObjectByOutputCategoryComponent);
        component = fixture.componentInstance;
        component.attributeList = [ ...ATTRIBUTE_LIST ];
        component.outputFamilyList = [ ...OUTPUT_FAMILY_LIST ];
        component.outputFamilyId = 1;
        component.outputCategoryId = 2;
        store = TestBed.inject(MockStore);
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('getCategory() should return output category with id 2', () => {
        expect(component.getCategory().id).toEqual(2);
    });

    it('should return an array with two elements', () => {
        expect(component.getAttributeListByOutputCategory().length).toEqual(1);
    });

    it('should raises store dispatch event with download file action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.downloadFile({ url: 'test.fr', filename: 'test' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(searchActions.downloadFile({ url: 'test.fr', filename: 'test' }));
    });
});
