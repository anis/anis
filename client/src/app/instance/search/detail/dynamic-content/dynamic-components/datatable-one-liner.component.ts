/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, OnInit } from '@angular/core';
import { Attribute, Dataset, DesignConfig, Instance } from 'src/app/metamodel/models';
import { SearchService } from 'src/app/instance/store/services/search.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-datatable-one-liner',
    templateUrl: './datatable-one-liner.component.html',
})
export class DatatableOneLinerComponent implements OnInit {
    @Input() instance: Instance = null;
    @Input() dataset: Dataset = null;
    @Input() designConfig: DesignConfig = null;
    @Input() attributeList: Attribute[] = null;
    @Input() attributeToList: Array<number> | string = null;
    @Input() title = "";
    @Input() isOpened = false;
    @Input() object: any = null;

    dataArrived: boolean = null;
    attributesToShow: Attribute[] = [];
    data: any[] = [];

    /**
     * Create a one-liner array
     * @param {SearchService} search - The service to query data
     */
    constructor(private search: SearchService) {}

    /**
     * @method
     * Format the list of attributes accordingly given in the input attributeToList to be shown
     * attributeToList is in fact the list of data to be visible in the array
     * @returns {string} - The properly formatted string for the query
     * @fires an Error if the input attributeToList is wrongly formatted (shall be either 'all' or an array of numbers)
     */
    formatAttributesQuery(): string | never {
        if (this.attributeToList === 'all') {
            if (this.attributeList.length != 1) {
                const all = [...Array(this.attributeList.length + 1).keys()];
                return all
                    .slice(1, this.attributeList.length + 1)
                    .toString()
                    .replace(/,/g, ';');
            } else {
                return '1';
            }
        } else if (
            this.attributeToList &&
            this.attributeToList.length != 0 &&
            typeof this.attributeToList[0] === 'number'
        ) {
            return this.attributeToList.toString().replace(/,/g, ';');
        } else {
            throw new Error(
                `Error: attributeToList Input parsing error \n attributeToList shall be either an Array of numbers of "all"`
            );
        }
    }

    /**
     * @method Create the array of Attribute to show
     * Manipulate the property attributesToShow
     * Fill it with the wished array of attributeListn to be shown from the dataset
     * @returns {void}
     */
    createAttributesArrayToShow(): void {
        if (this.attributeToList === 'all') {
            this.attributesToShow = [...this.attributeList];
        } else {
            (this.attributeToList as number[]).forEach((elem) =>
                this.attributesToShow.push(this.attributeList[elem - 1])
            );
        }
    }

    /**
     * @method Make the subscription to the server hosting the database
     * Manipulate the property data to fill it with the wished array of data from the dataset
     * @returns {void}
    */
    getData(formattedListOfAttributes: string): Observable<any[]> | null {
        if (formattedListOfAttributes != null) {
            const query = `a=${formattedListOfAttributes}&c=1::eq::${this.object.id}&f=json`;
            this.search.retrieveData(this.instance.name, this.dataset.name, query).subscribe((data) => {
                this.data = data[0];
                this.dataArrived = true;
            });
        }
        return null;
    }

    /**
     * @method Calls in order and successively the methods:
        - DatatableOneLinerComponent.formatAttributesQuery
        - DatatableOneLinerComponent.createAttributesArrayToShow
        - DatatableOneLinerComponent.getData
     * @implements OnInit
     */
    ngOnInit(): void {
        this.dataArrived = false;
        let formattedListOfAttributes: string = null;
        try {
            formattedListOfAttributes = this.formatAttributesQuery();
        } catch (error) {
            return;
        }
        this.createAttributesArrayToShow();
        this.getData(formattedListOfAttributes);
    }
}
