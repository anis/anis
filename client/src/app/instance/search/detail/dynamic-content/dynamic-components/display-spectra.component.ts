/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy, OnInit } from '@angular/core';

import { Observable } from 'rxjs';

import { Instance, Attribute } from 'src/app/metamodel/models';
import { DetailService } from 'src/app/instance/store/services/detail.service';

@Component({
    selector: 'app-display-spectra',
    templateUrl: 'display-spectra.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplaySpectraComponent implements OnInit {
    @Input() object: any;
    @Input() instance: Instance;
    @Input() datasetName: string;
    @Input() attributeList: Attribute[];
    @Input() attributeSpectraId: number;
    @Input() attributeZId: number;

    spectraCSV: Observable<string>;

    constructor(private detailService: DetailService) { }

    ngOnInit() {
        const spectraFile = this.object[this.attributeList.find(attribute => attribute.id === this.attributeSpectraId).label];
        this.spectraCSV = this.detailService.retrieveSpectra(this.instance.name, this.datasetName, spectraFile);
    }

    getZ() {
        const z = this.object[this.attributeList.find(attribute => attribute.id === this.attributeZId).label];
        return +z;
    }
}
