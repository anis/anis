/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Attribute } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';
import { GraphDataType } from './simple-graph/graph-data-type';

/**
 * @class
 * @classdesc Display a simple graph component.
 */
@Component({
    selector: 'app-display-graph',
    templateUrl: 'display-graph.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayGraphComponent {
    @Input() object: any;
    @Input() instanceName: string;
    @Input() datasetName: string;
    @Input() attributeList: Attribute[];
    @Input() fitsFileId: number;
    @Input() hdu: number;
    @Input() dataType: GraphDataType;
    @Input() xColumn: string;
    @Input() yColumn: string;
    @Input() xColumnTitle: string;
    @Input() yColumnTitle: string;
    @Input() xTicksNumber: number;
    @Input() yTicksNumber: number;
    @Input() axisBottomTicksFormat: string;
    @Input() axisLeftTicksFormat: string;
    @Input() attributeZId: number;
    @Input() tooltipXlabel: string = 'X';
    @Input() tooltipYlabel: string = 'Y';
    @Input() setGrid: boolean;
    @Input() setGraphArea: boolean;
    @Input() setRays: boolean;
    @Input() setRaysButton: boolean;
    @Input() setBrush: boolean;
    @Input() setTooltip: boolean;
    @Input() xLabelColor: string;
    @Input() yLabelColor: string;
    @Input() graphAreaColor: string;

    data: Observable<any>;

    constructor(private http: HttpClient, private config: AppConfigService) { }

    ngOnInit() {
        const file = this.object[this.attributeList.find(attribute => attribute.id === this.fitsFileId).label];

        this.data = this.http.get(
            `${this.config.servicesUrl}/fits/read-fits/${this.instanceName}/${this.datasetName}?filename=${file}&xcolumn=${this.xColumn}&ycolumn=${this.yColumn}&hdu=${this.hdu}`,
            { responseType: 'text' }
        );
    }
}
