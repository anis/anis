/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { Attribute } from 'src/app/metamodel/models';

@Component({
    selector: 'app-display-value-html',
    templateUrl: 'display-value-html.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayValueHtmlComponent {
    @Input() object: any;
    @Input() attributeList: Attribute[];
    @Input() attributeId: number;

    constructor(private sanitizer: DomSanitizer) {}

    getAttributeById() {
        return this.attributeList.find(attribute => attribute.id === this.attributeId);
    }

    getSafeHtmlContent() {
        return this.sanitizer.bypassSecurityTrustHtml(this.object[this.getAttributeById().label]);
    }
}
