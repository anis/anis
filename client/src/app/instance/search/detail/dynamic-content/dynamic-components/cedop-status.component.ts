/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Attribute } from 'src/app/metamodel/models';

@Component({
    selector: 'app-cedop-status',
    templateUrl: 'cedop-status.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CedopStatusComponent {
    @Input() object: any;
    @Input() attributeList: Attribute[];
    @Input() attributeId: number;
    @Input() pill: boolean;
    @Input() block: boolean;

    getAttributeById() {
        return this.attributeList.find(attribute => attribute.id === this.attributeId);
    }

    getColor() {
        let color = '';
        const value = this.object[this.getAttributeById().label];

        if (value === 'Completed') {
            color = 'success';
        } else if (value === 'On Going') {
            color = 'primary';
        } else if (value === 'Archived') {
            color = 'warning';
        } else {
            color = 'light';
        }

        return color;
    }
}
