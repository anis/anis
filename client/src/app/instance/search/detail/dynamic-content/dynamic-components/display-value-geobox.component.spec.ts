/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";

import { Attribute } from "src/app/metamodel/models";
import { DisplayValueGeoboxComponent } from './display-value-geobox.component';

describe('[instance][search][detail][dynamic-content][dynamic-components] DisplayValueGeoboxComponent', () => {
    let component: DisplayValueGeoboxComponent;
    let fixture: ComponentFixture<DisplayValueGeoboxComponent>;
    let attribute: Attribute;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DisplayValueGeoboxComponent]
        });
        fixture = TestBed.createComponent(DisplayValueGeoboxComponent);
        component = fixture.componentInstance;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('getAttributeById() should return an attribute with id 2', () => {
        component.attributeList = [
            { ...attribute, id: 1 },
            { ...attribute, id: 2 }
        ];
        component.attributeId = 2;
        expect(component.getAttributeById().id).toEqual(2);
    });

    it('getGeoValues() should return an array with geo values', () => {
        component.attributeList = [
            { ...attribute, id: 1, label: 'test' }
        ];
        component.attributeId = 1;
        component.object = { test: '-18.07032178###-15.51050733###13.87506132###15.32092453'};
        component.separator = '###';
        expect(component.getGeoValues()[2]).toEqual('13.87506132');
    });
});
