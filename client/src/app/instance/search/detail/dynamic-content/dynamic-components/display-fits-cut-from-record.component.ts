/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as searchActions from 'src/app/instance/store/actions/search.actions';
import * as sampActions from 'src/app/samp/samp.actions';
import * as sampSelector from 'src/app/samp/samp.selector';
import { Instance, Dataset, Attribute } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';
import { getHost } from 'src/app/shared/utils';

@Component({
    selector: 'app-display-fits-cut-from-record',
    templateUrl: 'display-fits-cut-from-record.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayFitsCutFromRecordComponent {
    @Input() object: any;
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() attributeList: Attribute[];
    @Input() imageLabel: string;
    @Input() attributeImageId: number;
    @Input() attributeRaId: number;
    @Input() attributeDecId: number;
    @Input() attributeRadiusId: number;
    @Input() hduNumber = '0';
    @Input() stretch = 'linear';
    @Input() pmin = 0.2;
    @Input() pmax = 99;

    loading = true;
    error = false;

    public sampRegistered: Observable<boolean>;

    constructor(private appConfig: AppConfigService, private store: Store<{}>) {
        this.sampRegistered = this.store.select(sampSelector.selectRegistered);
    }

    onLoad() {
        this.loading = false;
    }

    onError(event) {
        if (this.dataset.public || ((!this.instance.public || !this.dataset.public) && event.target.attributes['src'].value === 'not_found')) {
            this.loading = false;
            this.error = true;
        }
    }

    getHref() {
        let href = `${getHost(this.appConfig.servicesUrl)}/fits/fits-cut-to-png/${this.instance.name}/${this.dataset.name}?filename=${this.getImageValue()}`;
        href += `&ra=${this.getRaValue()}`;
        href += `&dec=${this.getDecValue()}`;
        href += `&radius=${this.getRadiusValue()}`;
        href += `&stretch=${this.stretch}`;
        href += `&pmin=${this.pmin}`;
        href += `&pmax=${this.pmax}`;
        href += `&axes=false`;
        href += `&hdu_number=${this.hduNumber}`;

        return href;
    }

    getFitsCutUrl() {
        let url = `${getHost(this.appConfig.servicesUrl)}/fits/fits-cut/${this.instance.name}/${this.dataset.name}?filename=${this.getImageValue()}`;
        url += `&ra=${this.getRaValue()}`;
        url += `&dec=${this.getDecValue()}`;
        url += `&radius=${this.getRadiusValue()}`;

        if (this.hduNumber) {
            url += `&hdu_number=${this.hduNumber}`;
        }

        return url;
    }

    saveFitsCutFile(event) {
        event.preventDefault();

        const url = this.getFitsCutUrl();
        const imageFilePath = this.getImageValue();
        const filename = `fits_cut_${this.getRaValue()}_${this.getDecValue()}_${this.getRadiusValue()}_${imageFilePath.substring(imageFilePath.lastIndexOf('/') + 1)}`;

        this.store.dispatch(searchActions.downloadFile({url, filename}));
    }

    broadcast() {
        this.store.dispatch(sampActions.broadcastImage({ url: this.getFitsCutUrl() }));
    }

    getImageValue() {
        const attributeImage = this.attributeList.find(attribute => attribute.id === this.attributeImageId);
        return this.object[attributeImage.label];
    }

    getRaValue() {
        const attributeRa = this.attributeList.find(attribute => attribute.id === this.attributeRaId);
        return this.object[attributeRa.label];
    }

    getDecValue() {
        const attributeDec = this.attributeList.find(attribute => attribute.id === this.attributeDecId);
        return this.object[attributeDec.label];
    }

    getRadiusValue() {
        const attributeRadius = this.attributeList.find(attribute => attribute.id === this.attributeRadiusId);
        return this.object[attributeRadius.label];
    }
}
