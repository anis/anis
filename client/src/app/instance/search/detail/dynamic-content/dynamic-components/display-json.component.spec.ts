/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";

import { DisplayJsonComponent } from "./display-json.component";
import { ATTRIBUTE_LIST } from "src/test-data";
import { NgxJsonViewerModule } from "ngx-json-viewer";

describe('[instance][search][detail][dynamic-content][dynamic-components] DisplayJsonComponent', () => {
    let component: DisplayJsonComponent;
    let fixture: ComponentFixture<DisplayJsonComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DisplayJsonComponent],
            imports: [NgxJsonViewerModule]
        });
        fixture = TestBed.createComponent(DisplayJsonComponent);
        component = fixture.componentInstance;
        component.attributeList = [ ...ATTRIBUTE_LIST ];
        component.attributeJsonId = 2;
        component.object = { label_two: 'test' };
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('getValue() should return test', () => {
        expect(component.getValue()).toEqual('test');
    });
})
