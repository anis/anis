/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Attribute } from 'src/app/metamodel/models';

@Component({
    selector: 'app-display-value-multi-links',
    templateUrl: 'display-value-multi-links.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayValueMultiLinksComponent {
    @Input() object: any;
    @Input() attributeList: Attribute[];
    @Input() attributeId: number;
    @Input() separator: string;
    @Input() textLink: string;
    @Input() blank: boolean;

    getAttributeById() {
        return this.attributeList.find(attribute => attribute.id === this.attributeId);
    }

    getLinksValues() {
        const value = this.object[this.getAttributeById().label] as string;
        if (!value) {
            return [];
        } else {
            return value.split(this.separator);
        }
    }

    getTextLinkValue(value: string, index: number) {
        if (this.textLink) {
            return `${this.textLink} ${index + 1}`;
        } else {
            return value
        }
    }
}
