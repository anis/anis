/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { Attribute } from 'src/app/metamodel/models';
import * as searchActions from 'src/app/instance/store/actions/search.actions';
import { DisplayObjectByOutputFamilyComponent } from './display-object-by-output-family.component';
import { ATTRIBUTE_LIST, OUTPUT_FAMILY_LIST } from 'src/test-data';
import { AccordionModule } from 'ngx-bootstrap/accordion';

describe('[instance][search][detail][dynamic-content][dynamic-components] DisplayObjectByOutputFamilyComponent', () => {
    let component: DisplayObjectByOutputFamilyComponent;
    let fixture: ComponentFixture<DisplayObjectByOutputFamilyComponent>;
    let store: MockStore;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DisplayObjectByOutputFamilyComponent
            ],
            providers: [
                provideMockStore({}),
            ],
            imports: [
                BrowserAnimationsModule,
                AccordionModule,
            ],
        })
        fixture = TestBed.createComponent(DisplayObjectByOutputFamilyComponent);
        component = fixture.componentInstance;
        component.attributeList = [ ...ATTRIBUTE_LIST ];
        component.outputFamilyList = [ ...OUTPUT_FAMILY_LIST ];
        component.outputFamilyId = 1;
        store = TestBed.inject(MockStore);
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('getFamily() should return output family with id 1', () => {
        expect(component.getFamily().id).toEqual(1);
    });

    it('getOutputCategoryListByFamily should return an array with two elements', () => {
        component.getAttributeListByOutputCategory = jest.fn().mockImplementation(() => [1])
        expect(component.getOutputCategoryListByFamily(1).length).toEqual(2);
    });

    it('getAttributeListByOutputCategory should return an array with two elements', () => {
        expect(component.getAttributeListByOutputCategory(1).length).toEqual(2);
    });

    it('should raises store dispatch event with download file action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.downloadFile({ url: 'test.fr', filename: 'test' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(searchActions.downloadFile({ url: 'test.fr', filename: 'test' }));
    });
});
