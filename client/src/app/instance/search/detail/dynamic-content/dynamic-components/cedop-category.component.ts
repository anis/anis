/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Attribute } from 'src/app/metamodel/models';

@Component({
    selector: 'app-cedop-category',
    templateUrl: 'cedop-category.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CedopCategoryComponent {
    @Input() object: any;
    @Input() attributeList: Attribute[];
    @Input() attributeId: number;

    getAttributeById() {
        return this.attributeList.find(attribute => attribute.id === this.attributeId);
    }

    getFaName() {
        let faName = '';

        const value = this.object[this.getAttributeById().label];
        if (value === 'Astronomie-Astrophysique') {
            faName = 'satellite-dish';
        } else if (value === 'Océan-Atmosphère') {
            faName = 'water';
        } else if (value === 'Surfaces et Interfaces Continentales') {
            faName = 'earth-americas';
        } else if (value === 'Environment') {
            faName = 'tree';
        } else if (value === 'Demography') {
            faName = 'users-viewfinder';
        } else {
            faName = 'notdef';
        }

        return faName;
    }
}
