/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, switchMap } from 'rxjs';

import { Instance, Dataset, Attribute } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';
import { DatasetService } from 'src/app/metamodel/services/dataset.service';
import { AttributeService } from 'src/app/metamodel/services/attribute.service';

@Component({
    selector: 'app-datatable-other-dataset',
    templateUrl: './datatable-other-dataset.component.html',
})
export class DatatableOtherDatasetComponent implements OnInit {
    @Input() object: any;
    @Input() instance: Instance;
    @Input() datasetName: string;
    @Input() a: string;
    @Input() c: string;
    @Input() values: string[];
    @Input() renderersEnabled: boolean;
    @Input() noDataMessage = 'No data available';
    
    dataset: Observable<Dataset>;
    attributeList: Observable<Attribute[]>;
    data: Observable<any[]>;

    constructor(private datasetService: DatasetService, private attributeService: AttributeService, private http: HttpClient, private config: AppConfigService) { }

    ngOnInit() {
        this.data = (this.dataset = this.datasetService.retrieveDataset(this.instance.name, this.datasetName)).pipe(
            switchMap(dataset => {
                const query = `a=${this.a}&c=${this.getCriteria()}&o=${dataset.default_order_by}:${dataset.default_order_by_direction}`;
                return this.http.get<any[]>(`${this.config.apiUrl}/search/${this.instance.name}/${this.datasetName}?${query}`);
            })
        );
        this.attributeList = this.attributeService.retrieveAttributeList(this.instance.name, this.datasetName);
    }

    getCriteria() {
        // Counter for tracking replacement panel items
        let replacementIndex = 0;

        // Retrieving the values to be used in the replace function (scope)
        let values = this.values;
        let object = this.object;

        // Replacement function that takes into account the order of the elements in the table
        function replaceFunction() {
            // If the counter exceeds the length of the replacement array, we return to the beginning of the array
            if (replacementIndex >= values.length) {
                replacementIndex = 0;
            }
            // Retrieving the current element from the replacement table from object
            const replacement = object[values[replacementIndex]];
            // Counter incrementation
            replacementIndex++;
            // Return of replacement value
            return replacement;
        }

        // Using the `replace` method with a global regular expression to replace all occurrences
        return this.c.replace(new RegExp('\\$value', 'g'), replaceFunction);
    }
}
