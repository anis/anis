/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import * as searchActions from 'src/app/instance/store/actions/search.actions';
import { DisplayObjectComponent } from './display-object.component';
import { ATTRIBUTE_LIST, OUTPUT_FAMILY_LIST } from 'src/test-data';
import { AccordionModule } from 'ngx-bootstrap/accordion';

describe('[instance][search][detail][dynamic-content][dynamic-components] DisplayObjectComponent', () => {
    let component: DisplayObjectComponent;
    let fixture: ComponentFixture<DisplayObjectComponent>;
    let store: MockStore;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DisplayObjectComponent
            ],
            providers: [
                provideMockStore({}),
            ],
            imports: [
                BrowserAnimationsModule,
                AccordionModule,
            ],
        })
        fixture = TestBed.createComponent(DisplayObjectComponent);
        component = fixture.componentInstance;
        component.attributeList = [ ...ATTRIBUTE_LIST ];
        component.outputFamilyList = [ ...OUTPUT_FAMILY_LIST ];
        store = TestBed.inject(MockStore);
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('getOutputFamilyList() should return an array with two elements', () => {
        component.getOutputCategoryListByFamily = jest.fn().mockImplementation(() => [1])
        expect(component.getOutputFamilyList().length).toEqual(2);
    });

    it('getOutputCategoryListByFamily() should return an array with one element', () => {
        component.getAttributeListByOutputCategory = jest.fn().mockImplementation(() => [1])
        expect(component.getOutputCategoryListByFamily(1).length).toEqual(2);
    });

    it('getAttributeListByOutputCategory should return an array with two elements', () => {
        expect(component.getAttributeListByOutputCategory(1, 1).length).toEqual(2);
    });

    it('should raises store dispatch event with download file action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.downloadFile({ url: 'test.fr', filename: 'test' });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(searchActions.downloadFile({ url: 'test.fr', filename: 'test' }));
    });
});
