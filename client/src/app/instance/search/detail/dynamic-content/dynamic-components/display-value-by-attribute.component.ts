/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Store } from '@ngrx/store';

import { Attribute, Dataset, Instance } from 'src/app/metamodel/models';
import { SearchQueryParams } from 'src/app/instance/store/models';
import * as searchActions from 'src/app/instance/store/actions/search.actions';
import * as sampActions from 'src/app/samp/samp.actions';

@Component({
    selector: 'app-display-value-by-attribute',
    templateUrl: 'display-value-by-attribute.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayValueByAttributeComponent {
    @Input() object: any;
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() attributeList: Attribute[];
    @Input() queryParams: SearchQueryParams;
    @Input() sampRegistered: boolean;
    @Input() attributeId: number;
    @Input() rendererEnabled: boolean;
    @Input() separator: string;

    constructor(protected store: Store<{}>) { }

    getAttributeById() {
        return this.attributeList.find(attribute => attribute.id === this.attributeId);
    }

    /**
     * Dispatches action to launch the file download
     * 
     * @param { url: string, filename: string } download
     */
    downloadFile(download: { url: string, filename: string }): void {
        this.store.dispatch(searchActions.downloadFile(download));
    }

    /**
     * Dispatches action to broadcast data.
     *
     * @param  {string} url - The broadcast URL.
     */
    broadcastVotable(url: string): void {
        this.store.dispatch(sampActions.broadcastVotable({ url }));
    }

    /**
     * Dispatches action to broadcast image.
     *
     * @param  {string} url - The broadcast URL.
     */
    broadcastImage(url: string): void {
        this.store.dispatch(sampActions.broadcastImage({ url }));
    }

    splitValueWithSeparator(): string[] {
        const value = this.object[this.getAttributeById().label] as string;
        if (!value) {
            return [];
        } else {
            return value.split(this.separator);
        }
    }
}
