/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, ViewEncapsulation, OnInit, Input } from '@angular/core';
import * as d3 from 'd3';

import { Point } from '../spectra-graph/point';
import { absorptionLines, emissionLines } from '../spectra-graph/rays';
import { SpectraType } from '../spectra-graph/spectra-type';
import { GraphDataType } from './graph-data-type';

@Component({
    selector: 'app-simple-graph',
    encapsulation: ViewEncapsulation.None,
    templateUrl: 'simple-graph.component.html',
    styleUrls: ['simple-graph.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpleGraphComponent implements OnInit {
    @Input() data: any;
    @Input() dataType: GraphDataType;
    @Input() xColumnTitle: string;
    @Input() yColumnTitle: string;
    @Input() xTicksNumber: number;
    @Input() yTicksNumber: number;
    @Input() axisBottomTicksFormat: string;
    @Input() axisLeftTicksFormat: string;
    @Input() tooltipXLabel: string;
    @Input() tooltipYLabel: string;
    @Input() setGrid: boolean;
    @Input() setGraphArea: boolean;
    @Input() setRays: boolean;
    @Input() setRaysButton: boolean;
    @Input() setBrush: boolean;
    @Input() setTooltip: boolean;
    @Input() z: number;
    @Input() xLabelColor: string;
    @Input() yLabelColor: string;
    @Input() graphAreaColor: string;

    svg: d3.Selection<d3.BaseType, unknown, HTMLElement, any>;
    focus: d3.Selection<SVGGElement, unknown, HTMLElement, any>;
    width: number;
    height: number;
    brushHeight: number;
    margin = { top: 50, right: 10, bottom: 150, left: 100 };
    x: d3.ScaleLinear<number, number>;
    xAxis: d3.Axis<number | { valueOf(): number }>;
    yAxis: d3.Axis<number | { valueOf(): number }>;
    y: d3.ScaleLinear<number, number>;
    graphLine: d3.Line<Point>;
    graphArea: d3.Area<Point>;
    displayEmissionLines: boolean = true;
    dispayAbsorptionLines: boolean = true;

    ngOnInit() {
        this.width = document.getElementById("svg-simple-graph-container").offsetWidth * 1 - this.margin.left - this.margin.right;
        this.height = 600 - this.margin.top - this.margin.bottom;
        this.brushHeight = 50;
        this.x = d3.scaleLinear().range([0, this.width]);
        this.y = d3.scaleLinear().range([this.height, 0]);
        const dataset = this.parseData();
        this.initSvg();
        this.addTitles();
        this.setupDomain(dataset);
        if (this.setGrid) {
            this.addGrid();
        }
        this.addGraphLine(dataset);
        if (this.setGraphArea) {
            this.addGraphArea(dataset);
        }
        if (this.setRays) {
            this.addRays();
        }
        this.addAxis();
        if (this.setRaysButton) {
            this.addRaysButtons();
        }
        if (this.setBrush) {
            this.addBrush(dataset);
        }
        if (this.setTooltip) {
            this.addTooltip(this.width, this.height, dataset, this.x, this.y);
        }
    }

    private initSvg(): void {
        this.svg = d3.select("svg#simple-graph")
            .attr("width", this.width + this.margin.left + this.margin.right)
            .attr("height", this.height + this.margin.top + this.margin.bottom);

        this.svg.append("defs").append("clipPath")
            .attr("id", "clip")
            .append("rect")
            .attr("width", this.width)
            .attr("height", this.height);

        this.focus = this.svg.append("g")
            .attr("class", "focus")
            .attr("transform", `translate(${this.margin.left},${this.margin.top})`);
    }

    private addTitles(): void {
        const titles = this.focus.append("g")
            .attr("class", "titles");

        titles.append("text")
            .attr("x", (this.width / 2))
            .attr("y", this.height + this.margin.bottom - 20)
            .attr("text-anchor", "middle")
            .style('fill', this.xLabelColor)
            .text(this.xColumnTitle);

        titles.append("text")
            .attr("x", (this.margin.left * 0.75) * -1)
            .attr("y", this.height / 2)
            .attr("text-anchor", "middle")
            .attr("transform", `rotate(-90,${((this.margin.left * 0.75) * -1)},${this.height / 2})`)
            .style('fill', this.yLabelColor)
            .text(this.yColumnTitle);
    }

    private setupDomain(dataset: d3.DSVParsedArray<Point>): void {
        const xMin = d3.min(dataset, (d) => d.x);
        const xMax = d3.max(dataset, (d) => d.x);
        this.x.domain([xMin, xMax]);

        const yMin = d3.min(dataset, (d) => d.y);
        const yMax = d3.max(dataset, (d) => d.y) * 1.1;
        this.y.domain([yMin, yMax]);
    }

    private addGrid(): void {
        const grid = this.focus.append("g")
            .attr("class", "grid");

        grid.append("g")
            .attr("class", "grid-x")
            .selectAll()
            .data(this.x.ticks())
            .enter()
            .append("line")
            .attr("class", "grid-line-x")
            .attr("x1", (d: number) => this.x(d))
            .attr("x2", (d: number) => this.x(d))
            .attr("y1", 0)
            .attr("y2", this.height);

        grid.append("g")
            .attr("class", "grid-y")
            .selectAll()
            .data(this.y.ticks())
            .enter()
            .append("line")
            .attr("class", "grid-line-y")
            .attr("x1", 0)
            .attr("x2", this.width)
            .attr("y1", (d: number) => this.y(d))
            .attr("y2", (d: number) => this.y(d));
    }

    private addGraphLine(dataset: d3.DSVParsedArray<Point>): void {
        this.graphLine = d3.line<Point>()
            .x((d) => this.x(d.x))
            .y((d) => this.y(d.y));

        this.focus.append("g")
            .attr("clip-path", "url(#clip)")
            .append("path")
            .datum(dataset)
            .attr("class", "graph-line")
            .attr("d", this.graphLine);
    }

    private addGraphArea(dataset: d3.DSVParsedArray<Point>): void {
        this.graphArea = d3.area<Point>()
            .x((d) => this.x(d.x))
            .y0(this.height)
            .y1((d) => this.y(d.y));

        this.focus.append("g")
            .attr("clip-path", "url(#clip)")
            .append("path")
            .datum(dataset)
            .attr("class", "graph-area")
            .attr("d", this.graphArea);
    }

    private addAxis(): void {
        this.xAxis = d3.axisBottom(this.x);
        if (this.axisBottomTicksFormat) {
            this.focus.append("g")
                .attr("class", "axis axis-x")
                .attr("transform", `translate(0,${this.height})`)
                .call(this.xAxis.tickFormat(d3.format(this.axisBottomTicksFormat)).ticks(this.xTicksNumber));
        } else {
            this.focus.append("g")
                .attr("class", "axis axis-x")
                .attr("transform", `translate(0,${this.height})`)
                .call(this.xAxis.ticks(this.xTicksNumber));
        }
        this.yAxis = d3.axisLeft(this.y);
        if (this.axisLeftTicksFormat) {
            this.focus.append("g")
                .attr("class", "axis axis-y")
                .call(this.yAxis.tickFormat(d3.format(this.axisLeftTicksFormat)).ticks(this.yTicksNumber))
        } else {
            this.focus.append("g")
                .attr("class", "axis axis-y")
                .call(this.yAxis.ticks(this.yTicksNumber))
        }
    }

    private addRays(): void {
        let coef = 1 
        if(this.z){
           coef += this.z;
        }
        const er = this.focus.append("g")
            .style("display", "block")
            .attr("class", "ray emission")
            .attr("clip-path", "url(#clip)");

        er.selectAll()
            .data(emissionLines)
            .enter()
            .append("line")
            .attr("x1", r => this.x(r.wavelength * coef))
            .attr("x2", r => this.x(r.wavelength * coef))
            .attr("y1", this.height)
            .attr("y2", 0);

        er.selectAll()
            .data(emissionLines)
            .enter()
            .append("text")
            .attr("x", r => this.x(r.wavelength * coef) - 5)
            .attr("y", this.height * 0.2)
            .attr("transform", r => `rotate(-90,${(this.x(r.wavelength * coef) - 5)},${this.height * 0.2})`)
            .text(r => r.name);

        const ar = this.focus.append("g")
            .style("display", "block")
            .attr("class", "ray absorption")
            .attr("clip-path", "url(#clip)");

        ar.selectAll()
            .data(absorptionLines)
            .enter()
            .append("line")
            .attr("x1", r => this.x(r.wavelength * coef))
            .attr("x2", r => this.x(r.wavelength * coef))
            .attr("y1", this.height)
            .attr("y2", 0);

        ar.selectAll()
            .data(absorptionLines)
            .enter()
            .append("text")
            .attr("x", r => this.x(r.wavelength * coef) - 5)
            .attr("y", this.height * 0.8)
            .attr("transform", r => `rotate(-90,${(this.x(r.wavelength * coef) - 5)},${this.height * 0.8})`)
            .text(r => r.name);
    }

    private addRaysButtons(): void {
        const gButtons = this.svg.append("g")
            .attr("class", "rays-buttons")
            .attr("transform", `translate(${this.margin.left},25)`);

        const emission = gButtons.append("g")
            .attr("class", "emission-button emission-button-on");

        const circleEmission = emission.append("circle")
            .attr("cx", 20)
            .attr("cy", 0)
            .attr("r", 10);

        circleEmission.on("click", () => {
            const e = this.focus.select(".emission");
            if (this.displayEmissionLines) {
                e.style("display", "none");
                emission.attr("class", "emission-button button-off");
            } else {
                e.style("display", "block");
                emission.attr("class", "emission-button emission-button-on");
            }
            this.displayEmissionLines = !this.displayEmissionLines;
        });

        emission.append("text")
            .attr("x", 35)
            .attr("y", 5)
            .text("Display emission lines")

        const absorption = gButtons.append("g")
            .attr("class", "absorption-button absorption-button-on");

        const circleAbsorption = absorption.append("circle")
            .attr("cx", 215)
            .attr("cy", 0)
            .attr("r", 10);

        circleAbsorption.on("click", () => {
            const a = this.focus.select(".absorption");
            if (this.dispayAbsorptionLines) {
                a.style("display", "none");
                absorption.attr("class", "absorption-button button-off");
            } else {
                a.style("display", "block");
                absorption.attr("class", "absorption-button absorption-button-on");
            }
            this.dispayAbsorptionLines = !this.dispayAbsorptionLines;
        });

        absorption.append("text")
            .attr("x", 230)
            .attr("y", 5)
            .text("Display absorption lines")
    }

    private addBrush(dataset: d3.DSVParsedArray<Point>): void {
        const context = this.svg.append("g")
            .attr("class", "context")
            .attr("transform", `translate(${this.margin.left},480)`);

        const xBrush = d3.scaleLinear().range([0, this.width]);
        const yBrush = d3.scaleLinear().range([this.brushHeight, 0]);
        xBrush.domain(this.x.domain());
        yBrush.domain(this.y.domain());

        const xBrushAxis = d3.axisBottom(xBrush);

        const lineBrush = d3.line<Point>()
            .x((d) => xBrush(d.x))
            .y((d) => yBrush(d.y));

        context.append("g")
            .attr("class", "line")
            .append("path")
            .datum(dataset)
            .attr("d", lineBrush);

        const areaBrush = d3.area<Point>()
            .x((d) => xBrush(d.x))
            .y0(this.brushHeight)
            .y1((d) => yBrush(d.y));

        context.append("g")
            .attr("class", "area")
            .append("path")
            .datum(dataset)
            .attr("d", areaBrush);

        context.append("g")
            .attr("class", "axis")
            .attr("transform", `translate(0,${this.brushHeight})`)
            .call(xBrushAxis);

        const brush = d3.brushX()
            .extent([[0, 0], [this.width, this.brushHeight]])
            .on("end", (event) => {
                const selection = event.selection || xBrush.range();
                this.x.domain(selection.map(xBrush.invert, xBrush));

                // Update spectra graph
                this.focus.select(".graph-line")
                    .attr("d", this.graphLine);
                this.focus.select(".spectra-area")
                    .attr("d", this.graphArea);

                // Update axis
                this.focus.select<SVGGElement>(".axis-x").call(this.xAxis);

                // Update grid
                this.focus.selectAll(".grid-line-x")
                    .remove();
                this.focus.select(".grid-x")
                    .selectAll()
                    .data(this.x.ticks())
                    .enter()
                    .append("line")
                    .attr("class", "grid-line-x")
                    .attr("x1", (d: number) => this.x(d))
                    .attr("x2", (d: number) => this.x(d))
                    .attr("y1", 0)
                    .attr("y2", this.height);

                // Update rays
                let coef = 1 
                if(this.z){
                    coef += this.z;
                 }
                this.focus.select(".emission")
                    .selectAll("line")
                    .data(emissionLines)
                    .attr("x1", r => this.x(r.wavelength * coef))
                    .attr("x2", r => this.x(r.wavelength * coef));
                this.focus.select(".emission")
                    .selectAll("text")
                    .data(emissionLines)
                    .attr("x", r => this.x(r.wavelength * coef) - 5)
                    .attr("transform", r => `rotate(-90,${(this.x(r.wavelength * coef) - 5)},${this.height * 0.2})`);

                this.focus.select(".absorption")
                    .selectAll("line")
                    .data(absorptionLines)
                    .attr("x1", r => this.x(r.wavelength * coef))
                    .attr("x2", r => this.x(r.wavelength * coef));
                this.focus.select(".absorption")
                    .selectAll("text")
                    .data(absorptionLines)
                    .attr("x", r => this.x(r.wavelength * coef) - 5)
                    .attr("transform", r => `rotate(-90,${(this.x(r.wavelength * coef) - 5)},${this.height * 0.8})`);
            });

        context.append("g")
            .attr("class", "brush")
            .call(brush)
            .call(brush.move, this.x.range() as d3.BrushSelection);
    }

    private addTooltip(
        width: number,
        height: number,
        dataset: d3.DSVParsedArray<Point>,
        x: d3.ScaleLinear<number, number>,
        y: d3.ScaleLinear<number, number>
    ): void {
        const tooltip = this.focus.append("g")
            .style("display", "none");

        tooltip.append("circle")
            .attr("class", "big-circle-tootlip")
            .attr("r", 10);

        tooltip.append("circle")
            .attr("class", "little-circle-tooltip")
            .attr("r", 4);

        tooltip.append("polyline")
            .attr("points", "0,0 0,40 55,40 60,45 65,40 135,40 135,0 0,0")
            .attr("class", "rect-tootlip")
            .attr("transform", "translate(-60, -55)");

        const xValue = tooltip.append("text")
            .attr("class", "text-tooltip")
            .attr("transform", "translate(-55, -40)")
            .append("tspan")
            .text(`${this.tooltipXLabel} : `)
            .append("tspan");

        const yValue = tooltip.append("text")
            .attr("class", "text-tooltip")
            .attr("transform", "translate(-55, -24)")
            .append("tspan")
            .text(`${this.tooltipYLabel} : `)
            .append("tspan")
            .attr("class", "text-y-value")

        const bisectX = d3.bisector((p: Point) => p.x).left;
        this.focus.append("rect")
            .attr("class", "overlay")
            .attr("width", width)
            .attr("height", height)
            .on("mouseover", () => tooltip.style("display", null))
            .on("mouseout", () => tooltip.style("display", "none"))
            .on("mousemove", (event, d) => {
                const mouse = d3.pointer(event);
                const x0 = x.invert(mouse[0]);
                const index = bisectX(dataset, x0);
                const datum = dataset[index];
                tooltip.attr("transform", `translate(${x(datum.x)},${y(datum.y)})`);
                xValue.text(datum.x);
                yValue.text(datum.y);
            });
    }

    private parseData() {
        if (this.dataType === 'json') {
            let dataset = JSON.parse(this.data);
            dataset = dataset.x.map((xValue: number, index: number) => {
                return { x: xValue, y: dataset.y[index] }
            })
            dataset.sort((a: Point, b: Point) => {
                return d3.ascending(a.x, b.x)
            })
            return dataset;

        } else if (this.dataType === 'spectra') {

            const dataset = d3.csvParse<Point, SpectraType>(this.data, (row) => {
                return {
                    x: parseFloat(row.x),
                    y: parseFloat(row.Flux)
                };
            });
            return dataset;
        }
    }
}
