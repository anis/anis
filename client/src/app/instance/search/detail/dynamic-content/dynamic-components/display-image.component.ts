/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { getHost } from 'src/app/shared/utils';
import { Instance, Dataset, Attribute } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';

@Component({
    selector: 'app-display-image',
    templateUrl: 'display-image.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayImageComponent {
    @Input() object: any;
    @Input() attributeList: Attribute[];
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() attributeImageId: number;
    @Input() type: string;
    @Input() hduNumber: string;
    @Input() width: string;
    @Input() height: string;

    loading = true;
    error = false;

    constructor(private appConfig: AppConfigService) { }

    onLoad() {
        this.loading = false;
    }

    onError(event) {
        if ((this.instance.public && this.dataset.public) || ((!this.instance.public || !this.dataset.public) && event.target.attributes['src'].value === 'not_found')) {
            this.loading = false;
            this.error = true;
        }
    }

    /**
     * Returns source image.
     *
     * @return string
     */
    getValue(): string {
        if (this.type === 'fits') {
            let url = `${this.appConfig.servicesUrl}/fits/fits-to-png/${this.instance.name}/${this.dataset.name}?filename=${this.getPath()}`
                + `&stretch=linear&pmin=0.25&pmax=99.75&axes=true`;
            if (this.hduNumber) {
                url += `&hdu_number=${this.hduNumber}`;
            }
            return url;
        } else if (this.type === 'image') {
            return `${getHost(this.appConfig.apiUrl)}/instance/${this.instance.name}/dataset/${this.dataset.name}/file-explorer${this.getPath()}`;
        } else {
            return this.object[this.getAttributeImage().label] as string;
        }
    }

    getPath() {
        let path = this.object[this.getAttributeImage().label];
        if (path[0] !== '/') {
            path = '/' + path;
        }
        return path;
    }

    getAttributeImage() {
        return this.attributeList.find(attribute => attribute.id === this.attributeImageId);
    }

    getStyle() {
        let style = {
            "width": '100%'
        } as any;
        if (this.width && this.height) {
            style = {
                "width": this.width,
                "height": this.height
            };
        }
        return style;
    }
}
