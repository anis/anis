/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { BrowserModule } from "@angular/platform-browser";
import { DetailService } from 'src/app/instance/store/services/detail.service';

import { AppConfigService } from "src/app/app-config.service";
import { Attribute } from "src/app/metamodel/models";
import { DisplaySpectraComponent } from "./display-spectra.component";
import { INSTANCE } from "src/test-data";
import { SpinnerComponent } from 'src/app/shared/components/spinner.component';

jest.mock('src/app/instance/store/services/detail.service');

describe('[instance][search][detail][dynamic-content][dynamic-components] DisplaySpectraComponent', () => {
    let component: DisplaySpectraComponent;
    let fixture: ComponentFixture<DisplaySpectraComponent>;
    let attribute: Attribute;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DisplaySpectraComponent, SpinnerComponent],
            imports: [
                BrowserModule,
            ],
            providers: [
                DetailService,
                { provide: AppConfigService, useValue: { servicesUrl: 'test' } },
            ]
        });
        fixture = TestBed.createComponent(DisplaySpectraComponent);
        component = fixture.componentInstance;
        component.attributeList = [
            { ...attribute, id: 1, label: 'test' }
        ];
        component.object = {};
        component.instance = { ...INSTANCE };
        component.attributeSpectraId = 1;
        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('getZ() should return test', () => {
        component.attributeZId = 1;
        component.object = { test: 5 };
        expect(component.getZ()).toEqual(5);
    });
});
