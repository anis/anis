/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";

import { Attribute } from "src/app/metamodel/models";
import { CedopStatusComponent } from './cedop-status.component';

describe('[instance][search][detail][dynamic-content][dynamic-components] CedopStatusComponent', () => {
    let component: CedopStatusComponent;
    let fixture: ComponentFixture<CedopStatusComponent>;
    let attribute: Attribute;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [CedopStatusComponent]
        });
        fixture = TestBed.createComponent(CedopStatusComponent);
        component = fixture.componentInstance;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('getAttributeById() should return an attribute with id 2', () => {
        component.attributeList = [
            { ...attribute, id: 1 },
            { ...attribute, id: 2 }
        ];
        component.attributeId = 2;
        expect(component.getAttributeById().id).toEqual(2);
    });

    it('getColor() should return the correct color for badge', () => {
        component.attributeList = [
            { ...attribute, id: 1, label: 'test' }
        ];
        component.attributeId = 1;
        component.object = { test: 'Completed'};
        expect(component.getColor()).toEqual('success');

        component.object = { test: 'On Going'};
        expect(component.getColor()).toEqual('primary');

        component.object = { test: 'Archived'};
        expect(component.getColor()).toEqual('warning');

        component.object = { test: 'test'};
        expect(component.getColor()).toEqual('light');
    });
});
