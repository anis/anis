/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { DomSanitizer } from '@angular/platform-browser';

import { Attribute } from "src/app/metamodel/models";
import { DisplayValueHtmlComponent } from "./display-value-html.component";

describe('[instance][search][detail][dynamic-content][dynamic-components] DisplayValueHtmlComponent', () => {
    let component: DisplayValueHtmlComponent;
    let fixture: ComponentFixture<DisplayValueHtmlComponent>;
    let attribute: Attribute;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DisplayValueHtmlComponent],
            providers: [
                DomSanitizer,
            ]
        });
        fixture = TestBed.createComponent(DisplayValueHtmlComponent);
        component = fixture.componentInstance;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('getAttributeById() should return an attribute with id 2', () => {
        component.attributeList = [
            { ...attribute, id: 1 },
            { ...attribute, id: 2 }
        ];
        component.attributeId = 2;
        expect(component.getAttributeById().id).toEqual(2);
    });
});
