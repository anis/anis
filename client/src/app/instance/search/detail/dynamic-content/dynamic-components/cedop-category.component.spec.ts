/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";

import { Attribute } from "src/app/metamodel/models";
import { CedopCategoryComponent } from './cedop-category.component';

describe('[instance][search][detail][dynamic-content][dynamic-components] CedopCategoryComponent', () => {
    let component: CedopCategoryComponent;
    let fixture: ComponentFixture<CedopCategoryComponent>;
    let attribute: Attribute;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [CedopCategoryComponent]
        });
        fixture = TestBed.createComponent(CedopCategoryComponent);
        component = fixture.componentInstance;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('getAttributeById() should return an attribute with id 2', () => {
        component.attributeList = [
            { ...attribute, id: 1 },
            { ...attribute, id: 2 }
        ];
        component.attributeId = 2;
        expect(component.getAttributeById().id).toEqual(2);
    });

    it('getFaName() should return the correct name of font awesome to be used', () => {
        component.attributeList = [
            { ...attribute, id: 1, label: 'test' }
        ];
        component.attributeId = 1;
        component.object = { test: 'Astronomie-Astrophysique'};
        expect(component.getFaName()).toEqual('satellite-dish');

        component.object = { test: 'Océan-Atmosphère'};
        expect(component.getFaName()).toEqual('water');

        component.object = { test: 'Surfaces et Interfaces Continentales'};
        expect(component.getFaName()).toEqual('earth-americas');

        component.object = { test: 'Environment'};
        expect(component.getFaName()).toEqual('tree');

        component.object = { test: 'Demography'};
        expect(component.getFaName()).toEqual('users-viewfinder');

        component.object = { test: 'test'};
        expect(component.getFaName()).toEqual('notdef');
    });
});
