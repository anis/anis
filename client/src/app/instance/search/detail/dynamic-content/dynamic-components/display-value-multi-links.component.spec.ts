/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";

import { Attribute } from "src/app/metamodel/models";
import { DisplayValueMultiLinksComponent } from './display-value-multi-links.component';

describe('[instance][search][detail][dynamic-content][dynamic-components] DisplayValueMultiLinksComponent', () => {
    let component: DisplayValueMultiLinksComponent;
    let fixture: ComponentFixture<DisplayValueMultiLinksComponent>;
    let attribute: Attribute;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DisplayValueMultiLinksComponent]
        });
        fixture = TestBed.createComponent(DisplayValueMultiLinksComponent);
        component = fixture.componentInstance;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('getAttributeById() should return an attribute with id 2', () => {
        component.attributeList = [
            { ...attribute, id: 1 },
            { ...attribute, id: 2 }
        ];
        component.attributeId = 2;
        expect(component.getAttributeById().id).toEqual(2);
    });

    it('getLinksValues() should return an array with links values', () => {
        component.attributeList = [
            { ...attribute, id: 1, label: 'test' }
        ];
        component.attributeId = 1;
        component.object = { test: 'https://test.lam.fr###https://test2.lam.fr'};
        component.separator = '###';
        expect(component.getLinksValues()[1]).toEqual('https://test2.lam.fr');
    });

    it('getLinksValues() should return an array with links values', () => {
        expect(component.getTextLinkValue('https://test.fr', 0)).toEqual('https://test.fr');
        component.textLink = 'data-link';
        expect(component.getTextLinkValue('https://test.fr', 0)).toEqual('data-link 1');
    });
});
