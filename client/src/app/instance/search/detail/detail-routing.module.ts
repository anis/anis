/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailComponent } from './containers/detail.component';

const routes: Routes = [
    { path: '', redirectTo: ':dname/:id', pathMatch: 'full' },
    { path: ':dname/:id', component: DetailComponent, title: 'Detail' }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DetailRoutingModule { }

export const routedComponents = [
    DetailComponent
];
