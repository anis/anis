/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    Component,
    OnInit,
    Input,
    ChangeDetectionStrategy,
} from '@angular/core';

import {
    DetailConfig,
    Attribute,
    Dataset,
    OutputFamily,
    OutputCategory,
    Instance,
    Image,
    DesignConfig,
} from 'src/app/metamodel/models';
import { SearchQueryParams } from 'src/app/instance/store/models';
import { globalParsers } from 'src/app/shared/dynamic-content';
import { componentParsers } from '../dynamic-content';
import { StyleService } from 'src/app/shared/services/style.service';

@Component({
    selector: 'app-detail-content',
    templateUrl: 'detail-content.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailContentComponent implements OnInit {
    @Input() detailConfig: DetailConfig;
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() designConfig: DesignConfig;
    @Input() object: any;
    @Input() attributeList: Attribute[];
    @Input() outputFamilyList: OutputFamily[];
    @Input() imageList: Image[];
    @Input() queryParams: SearchQueryParams;
    @Input() sampRegistered: boolean;

    constructor(private style: StyleService) {}

    ngOnInit() {
        if (this.detailConfig.style_sheet) {
            this.style.addCSS(
                this.detailConfig.style_sheet.replace(
                    /(.+{)/g,
                    (match, $1) => `.detail-${this.dataset.name} ${$1}`
                ),
                'detail'
            );
        }
    }

    getParsers() {
        return [...globalParsers, ...componentParsers];
    }

    getContext() {
        return {
            object: this.object,
            instance: this.instance,
            dataset: this.dataset,
            designConfig: this.designConfig,
            attributeList: this.attributeList,
            outputFamilyList: this.outputFamilyList,
            imageList: this.imageList,
            queryParams: this.queryParams,
            sampRegistered: this.sampRegistered
        };
    }
}
