/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { Store } from '@ngrx/store';

import { Instance, Dataset, Attribute } from 'src/app/metamodel/models';
import * as searchActions from 'src/app/instance/store/actions/search.actions';

@Component({
    selector: 'app-detail-datatable',
    templateUrl: 'detail-datatable.component.html',
    styleUrls: [ 'detail-datatable.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailDatatableComponent {
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() a: string;
    @Input() attributeList: Attribute[];
    @Input() data: any[];
    @Input() renderersEnabled: boolean;

    constructor(protected store: Store<{}>) { }

    downloadFile(download: { url: string, filename: string }): void {
        this.store.dispatch(searchActions.downloadFile(download));
    }

    getAttributeList(): Attribute[] {
        const list = this.a.split(';');
        return list.map(a => this.attributeList.find(attribute => attribute.id.toString() === a));
    }
}
