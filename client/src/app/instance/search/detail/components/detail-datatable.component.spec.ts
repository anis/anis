/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { StyleService } from "src/app/shared/services/style.service";
import { DetailDatatableComponent } from "./detail-datatable.component";
import { SharedRendererModule } from 'src/app/instance/shared-renderer/shared-renderer.module';
import { DynamicHooksModule } from "ngx-dynamic-hooks";

describe('DetailDatatableComponent', () => {
    let component: DetailDatatableComponent;
    let fixture: ComponentFixture<DetailDatatableComponent>;
    let store: MockStore;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DetailDatatableComponent,
            ],
            providers: [
                { provide: StyleService, useValue: new StyleService() },
                provideMockStore({})
            ],
            imports: [SharedRendererModule, DynamicHooksModule]
        });
        fixture = TestBed.createComponent(DetailDatatableComponent);
        store = TestBed.inject(MockStore);
        component = fixture.componentInstance;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
