/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Type } from '@angular/core';

import { AbstractDisplayRendererComponent } from 'src/app/instance/shared-renderer/abstract-display-renderer.component';
import { AbstractRendererComponent } from 'src/app/instance/shared-renderer/abstract-renderer.component';
import { getDetailRendererComponent } from './renderer';

@Component({
    selector: 'app-display-detail-renderer',
    templateUrl: 'display-detail-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayDetailRendererComponent extends AbstractDisplayRendererComponent {
    getRendererComponent(renderer: string): Type<AbstractRendererComponent> {
        return getDetailRendererComponent(renderer);
    }
}
