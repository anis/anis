/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as fromSharedRender from 'src/app/instance/shared-renderer/components';
import { AbstractRendererComponent } from 'src/app/instance/shared-renderer/abstract-renderer.component';
import { getDetailRendererComponent } from '.';

class TestClass extends AbstractRendererComponent { }
describe('[instance][search][detail][components][renderer] getDetailRendererComponent', () => {

    it('should test getDetailRendererComponent', () => {
        let spy = jest.spyOn(fromSharedRender, 'getRendererComponent');
        spy.mockImplementation(() => TestClass);
        expect(getDetailRendererComponent('test')).toEqual(TestClass);
        expect(spy).toHaveBeenCalledTimes(1);
    });
});
