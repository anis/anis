/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Dataset } from 'src/app/metamodel/models';
import { AbstractSearchComponent } from './abstract-search.component';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as searchActions from '../../store/actions/search.actions';

@Component({
    selector: 'app-output',
    templateUrl: 'output.component.html'
})
export class OutputComponent extends AbstractSearchComponent {
    public dataset: Observable<Dataset>;

    constructor(protected override store: Store<{ }>) {
        super(store);
        this.dataset = store.select(datasetSelector.selectDatasetByRouteName);
    }

    override ngOnInit(): void {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(searchActions.changeStep({ step: 'output' })));
        Promise.resolve(null).then(() => this.store.dispatch(searchActions.checkOutput()));
        super.ngOnInit();
    }

    /**
     * Dispatches action to update output list selection with the given updated output list.
     *
     * @param  {number[]} outputList - The updated output list.
     */
    updateOutputList(outputList: number[]): void {
        sessionStorage.removeItem('datatable-pagination');
        sessionStorage.removeItem('datatable-output-list');
        this.store.dispatch(searchActions.updateOutputList({ outputList }));
    }
}
