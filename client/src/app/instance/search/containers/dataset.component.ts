/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component} from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AbstractSearchComponent } from './abstract-search.component';
import { DatasetFamily } from 'src/app/metamodel/models';
import * as searchActions from '../../store/actions/search.actions';
import * as authSelector from 'src/app/user/store/selectors/auth.selector';
import * as datasetFamilySelector from 'src/app/metamodel/selectors/dataset-family.selector';
import { AppConfigService } from 'src/app/app-config.service';

@Component({
    selector: 'app-dataset',
    templateUrl: 'dataset.component.html'
})
export class DatasetComponent extends AbstractSearchComponent {
    public isAuthenticated: Observable<boolean>;
    public datasetFamilyListIsLoading: Observable<boolean>;
    public datasetFamilyListIsLoaded: Observable<boolean>;
    public datasetFamilyList: Observable<DatasetFamily[]>;
    public userRoles: Observable<string[]>;

    constructor(protected override store: Store<{ }>, private config: AppConfigService) {
        super(store);
        this.isAuthenticated = store.select(authSelector.selectIsAuthenticated);
        this.datasetFamilyListIsLoading = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoading);
        this.datasetFamilyListIsLoaded = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoaded);
        this.datasetFamilyList = store.select(datasetFamilySelector.selectAllDatasetFamilies);
        this.userRoles = store.select(authSelector.selectUserRoles);
    }

    override ngOnInit(): void {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(searchActions.changeStep({ step: 'dataset' })));
        super.ngOnInit();
    }

    /**
     * Checks if authentication is enabled.
     *
     * @return boolean
     */
    getAuthenticationEnabled(): boolean {
        return this.config.authenticationEnabled;
    }

    /**
     * Returns admin roles list
     * 
     * @returns string[]
     */
    getAdminRoles(): string[] {
        return this.config.adminRoles;
    }
}
