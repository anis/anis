/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold } from 'jasmine-marbles';

import { SearchMultipleTitleResolver } from './search-multiple-title.resolver';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { Instance } from 'src/app/metamodel/models';

describe('Instance][SearchMultiple] SearchMultipleTitleResolver', () => {
    let searchMultipleTitleResolver: SearchMultipleTitleResolver;
    let store: MockStore;
    let mockInstanceSelectorSelectInstanceByRouteName;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                provideMockStore({}),
            ]
        })

        store = TestBed.inject(MockStore);
        searchMultipleTitleResolver = TestBed.inject(SearchMultipleTitleResolver);
    });

    it('should be created', () => {
        expect(searchMultipleTitleResolver).toBeTruthy();
    });

    it('should return test - search Multiple - ', () => {
        let route: any = { component: { name: 'ComponentMultiple' } };
        let instance: Instance;
        mockInstanceSelectorSelectInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, { ...instance, label: 'test' })

        let result = searchMultipleTitleResolver.resolve(route, null);
        const expected = cold('a', { a: 'test - Search Multiple - ' });
        expect(result).toBeObservable(expected);
    });
});
