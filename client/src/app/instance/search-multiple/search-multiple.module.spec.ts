/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SearchMultipleModule } from "./search-multiple.module";


describe('[Instance][SearchMultiple] SearchMultipleModule', () => {
    it('test Search multiple module', () => {
        expect(SearchMultipleModule.name).toEqual('SearchMultipleModule');
    })
});
