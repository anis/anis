/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { ConeSearch, SearchMultipleQueryParams } from '../store/models';
import { DesignConfig, Instance } from 'src/app/metamodel/models';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as designConfigSelector from 'src/app/metamodel/selectors/design-config.selector';
import * as searchMultipleSelector from '../store/selectors/search-multiple.selector';
import * as coneSearchSelector from '../store/selectors/cone-search.selector';

@Component({
    selector: 'app-search-multiple',
    templateUrl: 'search-multiple.component.html'
})
export class SearchMultipleComponent {
    public instance: Observable<Instance>;
    public designConfig: Observable<DesignConfig>;
    public currentStep: Observable<string>;
    public datasetsStepChecked: Observable<boolean>;
    public resultStepChecked: Observable<boolean>;
    public coneSearch: Observable<ConeSearch>;
    public selectedDatasets: Observable<string[]>;
    public queryParams: Observable<SearchMultipleQueryParams>;

    constructor(private store: Store<{ }>) {
        this.instance = this.store.select(instanceSelector.selectInstanceByRouteName);
        this.designConfig = this.store.select(designConfigSelector.selectDesignConfig);
        this.currentStep = this.store.select(searchMultipleSelector.selectCurrentStep);
        this.datasetsStepChecked = this.store.select(searchMultipleSelector.selectDatasetsStepChecked);
        this.resultStepChecked = this.store.select(searchMultipleSelector.selectResultStepChecked);
        this.coneSearch = this.store.select(coneSearchSelector.selectConeSearch);
        this.selectedDatasets = this.store.select(searchMultipleSelector.selectSelectedDatasets);
        this.queryParams = this.store.select(searchMultipleSelector.selectQueryParams);
    }
}
