/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { MultiDownloadResultComponent } from './multi-download-result.component';
import { AppConfigService } from "src/app/app-config.service";
import { CONE_SEARCH, DATASET, DESIGN_CONFIG } from 'src/test-data';

describe('[Instance][SearchMultiple][Component][Result] MultiDownloadResultComponent', () => {
    let component: MultiDownloadResultComponent;
    let fixture: ComponentFixture<MultiDownloadResultComponent>;

    beforeEach(() => {
        let appService: AppConfigService = {
            apiUrl: "http://test.fr",
            servicesUrl: "",
            baseHref: "",
            authenticationEnabled: false,
            mailerEnabled: true,
            ssoAuthUrl: "",
            ssoRealm: "",
            ssoClientId: "",
            adminRoles: [],
            matomoEnabled: false,
            matomoSiteId: 0,
            matomoTrackerUrl: ""
        };
        TestBed.configureTestingModule({
            declarations: [MultiDownloadResultComponent],
            providers: [ { provide: AppConfigService, useValue: appService } ],
            imports: [RouterTestingModule]
        });
        fixture = TestBed.createComponent(MultiDownloadResultComponent);
        component = fixture.componentInstance;
        component.dataset = DATASET;
        component.designConfig = DESIGN_CONFIG;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should call the method #isDownloadEnabled properly', () => {
        const res = component.isDownloadEnabled();
        expect(res).toEqual(true);
    });

    it('should call the method #getUrl properly', () => {
        const res = component.getUrl('csv');
        expect(res).toEqual('http://test.fr/search/my-dataset?a=all&f=csv')
    });

    it('should call the method #getQuery properly', () => {
        const res = component.getQuery();
        expect(res).toEqual("my-dataset?a=all");
    });

    it('should call the method #getQuery properly', () => {
        let res = component.getQuery();
        expect(res).toEqual("my-dataset?a=all");
        component.coneSearch = CONE_SEARCH;
        res = component.getQuery();
        expect(res).toEqual("my-dataset?a=all&cs=1:2:3");
    });

    it('should call the method #download properly', () => {
        const spyEmit = jest.spyOn(component.downloadFile, 'emit');
        const event = { preventDefault: jest.fn() };
        component.download(event, 'http://server', 'csv');
        expect(spyEmit).toHaveBeenCalled();
    });

    it('should call the method #formatToExtension properly', () => {
        const formats = {json: 'json', csv: 'csv', ascii: 'txt', votable: 'xml' }
        let res: string = null;
        for(var key in formats) {
            res = component.formatToExtension(key);
            expect(res).toEqual(formats[key]);
        }

        res = component.formatToExtension('coucou')
        expect(res).toEqual('json');
    });

    it('should call the method #broadcastResult properly', () => {
        const spyBroadcast = jest.spyOn(component.broadcastVotable, 'emit');
        component.broadcastResult();
        expect(spyBroadcast).toHaveBeenCalled();
    });

});
