/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter } from '@angular/core';

import { Instance, Dataset, DesignConfig } from 'src/app/metamodel/models';
import { ConeSearch } from 'src/app/instance/store/models';
import { AppConfigService } from 'src/app/app-config.service';
import { getHost } from 'src/app/shared/utils';

@Component({
    selector: 'app-multi-download-result',
    templateUrl: 'multi-download-result.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiDownloadResultComponent {
    @Input() instance: Instance;
    @Input() designConfig: DesignConfig;
    @Input() dataset: Dataset;
    @Input() coneSearch: ConeSearch;
    @Input() sampRegistered: boolean;
    @Output() downloadFile: EventEmitter<{url: string, filename: string}> = new EventEmitter();
    @Output() sampRegister: EventEmitter<{}> = new EventEmitter();
    @Output() sampUnregister: EventEmitter<{}> = new EventEmitter();
    @Output() broadcastVotable: EventEmitter<string> = new EventEmitter();

    constructor(private appConfig: AppConfigService) { }

    isDownloadEnabled() {
        return this.dataset.download_json
            || this.dataset.download_csv
            || this.dataset.download_ascii
            || this.dataset.download_vo;
    }

    getUrl(format: string): string {
        return `${getHost(this.appConfig.apiUrl)}/search/${this.getQuery()}&f=${format}`;
    }

    getQuery() {
        let query = `${this.dataset.name}?a=all`;
        if (this.coneSearch) {
            query += `&cs=${this.coneSearch.ra}:${this.coneSearch.dec}:${this.coneSearch.radius}`;
        }
        return query;
    }

    download(event, url: string, format: string) {
        event.preventDefault();

        const timeElapsed = Date.now();
        const today = new Date(timeElapsed);
        const filename = `result_${this.dataset.name}_${today.toISOString()}.${this.formatToExtension(format)}`;

        this.downloadFile.emit({ url, filename });
    }

    formatToExtension(format: string) {
        let extension: string;
        switch (format) {
            case 'json': {
                extension = 'json';
                break;
            }
            case 'csv': {
                extension = 'csv';
                break;
            }
            case 'ascii': {
                extension = 'txt';
                break;
            }
            case 'votable': {
                extension = 'xml';
                break;
            }
            default: {
                extension = 'json';
                break;
            }
        }
        return extension;
    }

    broadcastResult() {
        const url = this.getUrl('votable');
        this.broadcastVotable.emit(url);
    }
}
