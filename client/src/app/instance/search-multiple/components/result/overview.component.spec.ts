/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Component, Input } from '@angular/core';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { OverviewComponent } from './overview.component';
import { ConeSearch, SearchMultipleDatasetLength } from 'src/app/instance/store/models';

describe('[Instance][SearchMultiple][Component][Result] OverviewComponent', () => {
    let component: OverviewComponent;
    let fixture: ComponentFixture<OverviewComponent>;

    const searchMultipleDatasetLenght: SearchMultipleDatasetLength[] = [
        { datasetName: 'my-dataset', length: 3 },
        { datasetName: 'hello, world', length: 2 },
    ];

    @Component({
        selector: 'app-cone-search-parameters',
        template: '',
    })
    class ConeSearchParametersComponent {
        @Input() coneSearch: ConeSearch;
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [OverviewComponent, ConeSearchParametersComponent],
            imports: [RouterTestingModule, AccordionModule, BrowserAnimationsModule],
        });
        fixture = TestBed.createComponent(OverviewComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should call the method #getLengthByDatasetName properly', () => {
        component.dataLength = searchMultipleDatasetLenght;
        fixture.detectChanges();
        const res = component.getLengthByDatasetName('my-dataset');
        expect(res).toEqual(3);
    });
});
