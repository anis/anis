/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MultiResultByDatasetComponent } from './multi-result-by-dataset.component';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { SearchMultipleDatasetLength } from 'src/app/instance/store/models/search-multiple-dataset-length';
import {
    DATASET,
    DATASET_FAMILY,
    DATASET_FAMILY_LIST,
    DATASET_LIST,
} from 'src/test-data';
import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    Output,
} from '@angular/core';
import { Dataset, DesignConfig, Instance } from 'src/app/metamodel/models';
import { ConeSearch } from 'src/app/instance/store/models';

describe('[Instance][SearchMultiple][Component][Result] MultiResultByDatasetComponent', () => {
    let component: MultiResultByDatasetComponent;
    let fixture: ComponentFixture<MultiResultByDatasetComponent>;

    const searchMultipleDatasetLenght: SearchMultipleDatasetLength[] = [
        { datasetName: 'my-dataset', length: 3 },
        { datasetName: 'hello, world', length: 2 },
    ];

    @Component({
        selector: 'app-multi-dataset-card',
        template: '',
        changeDetection: ChangeDetectionStrategy.OnPush,
    })
    class MultiDatasetCardComponent {
        @Input() instance: Instance;
        @Input() designConfig: DesignConfig;
        @Input() dataset: Dataset;
        @Input() coneSearch: ConeSearch;
        @Input() dataLength: number;
        @Input() sampRegistered: boolean;
        @Output() downloadFile: EventEmitter<{
            url: string;
            filename: string;
        }> = new EventEmitter();
        @Output() sampRegister: EventEmitter<{}> = new EventEmitter();
        @Output() sampUnregister: EventEmitter<{}> = new EventEmitter();
        @Output() broadcastVotable: EventEmitter<string> = new EventEmitter();
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                MultiResultByDatasetComponent,
                MultiDatasetCardComponent,
            ],
            imports: [
                RouterTestingModule,
                AccordionModule,
                BrowserAnimationsModule,
            ],
        });
        fixture = TestBed.createComponent(MultiResultByDatasetComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute #getSortedDatasetFamilyList properly', () => {
        component.dataLength = searchMultipleDatasetLenght;
        const res = component.getCountByDataset('my-dataset');
        expect(res).toBe(3);
    });

    it('should execute #getSortedDatasetFamilyList properly', () => {
        component.datasetFamilyList = DATASET_FAMILY_LIST;
        component.datasetList = DATASET_LIST;
        component.dataLength = searchMultipleDatasetLenght;
        component.selectedDatasets = ['my-dataset'];
        fixture.detectChanges();
        const res = component.getSortedDatasetFamilyList();
        expect(res).toEqual([DATASET_FAMILY]);
    });

    it('should execute #getSelectedDatasetsByFamily properly', () => {
        component.datasetFamilyList = DATASET_FAMILY_LIST;
        component.datasetList = DATASET_LIST;
        component.dataLength = searchMultipleDatasetLenght;
        component.selectedDatasets = ['my-dataset'];
        fixture.detectChanges();
        const res = component.getSelectedDatasetsByFamily(1);
        expect(res).toEqual([DATASET]);
    });
});
