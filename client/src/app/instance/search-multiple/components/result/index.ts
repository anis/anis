/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { OverviewComponent } from './overview.component';
import { MultiResultByDatasetComponent } from './multi-result-by-dataset.component';
import { MultiDatasetCardComponent } from './multi-dataset-card.component';
import { MultiDownloadResultComponent }from './multi-download-result.component';

export const resultComponents = [
    OverviewComponent,
    MultiResultByDatasetComponent,
    MultiDatasetCardComponent,
    MultiDownloadResultComponent
];
