/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { MultiDatasetCardComponent } from './multi-dataset-card.component';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Dataset, DesignConfig, Instance } from 'src/app/metamodel/models';
import { ConeSearch } from 'src/app/instance/store/models';
import { CONE_SEARCH } from 'src/test-data';

describe('[Instance][SearchMultiple][Component][Result] MultiResultByDatasetComponent', () => {
    let component: MultiDatasetCardComponent;
    let fixture: ComponentFixture<MultiDatasetCardComponent>;

    @Component({
        selector: 'app-multi-download-result',
        template: '',
    })
    class MultiDownloadResultComponent {
        @Input() instance: Instance;
        @Input() designConfig: DesignConfig;
        @Input() dataset: Dataset;
        @Input() coneSearch: ConeSearch;
        @Input() sampRegistered: boolean;
        @Output() downloadFile: EventEmitter<{
            url: string;
            filename: string;
        }> = new EventEmitter();
        @Output() sampRegister: EventEmitter<{}> = new EventEmitter();
        @Output() sampUnregister: EventEmitter<{}> = new EventEmitter();
        @Output() broadcastVotable: EventEmitter<string> = new EventEmitter();
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                MultiDatasetCardComponent,
                MultiDownloadResultComponent,
            ],
            imports: [RouterTestingModule],
        });
        fixture = TestBed.createComponent(MultiDatasetCardComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should call the method #getCsQueryParams properly', () => {
        component.coneSearch = CONE_SEARCH;
        const res = component.getCsQueryParams();
        expect(res).toEqual({ cs: '1:2:3' });
    });
});
