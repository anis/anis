/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatasetListComponent } from './dataset-list.component';
import { DatasetsByFamilyComponent } from './datasets-by-family.component';
import { MultiDatasetSelectComponent } from './multi-dataset-select.component';

export const datasetsComponents = [
    DatasetListComponent,
    DatasetsByFamilyComponent,
    MultiDatasetSelectComponent
];
