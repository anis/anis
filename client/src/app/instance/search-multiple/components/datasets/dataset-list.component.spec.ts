/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { DatasetListComponent } from './dataset-list.component';
import { DATASET_FAMILY_LIST, DATASET_LIST } from 'src/test-data';
import { Dataset, DatasetFamily } from 'src/app/metamodel/models';

describe('[Instance][SearchMultiple][Component][Datasets] DatasetListComponent', () => {
    @Component({ selector: 'app-datasets-by-family', template: '' })
    class DatasetsByFamilyStubComponent {
        @Input() datasetFamily: DatasetFamily;
        @Input() datasetList: Dataset[];
        @Input() selectedDatasets: string[];
        @Input() isAllSelected: boolean;
        @Input() isAllUnselected: boolean;
    }

    let component: DatasetListComponent;
    let fixture: ComponentFixture<DatasetListComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetListComponent,
                DatasetsByFamilyStubComponent
            ],
            imports: [TooltipModule.forRoot()]
        });
        fixture = TestBed.createComponent(DatasetListComponent);
        component = fixture.componentInstance;
        component.datasetList = [ ...DATASET_LIST ];
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getDatasetFamilyList() should return sorted survey list', () => {
        component.datasetFamilyList = [ ...DATASET_FAMILY_LIST ];
        const sortedDatasetFamilyList: DatasetFamily[] = component.getDatasetFamilyList();
        expect(sortedDatasetFamilyList.length).toBe(1);
        expect(sortedDatasetFamilyList[0].label).toBe('My first dataset family');
    });

    it('#getDatasetsByFamily() should return dataset list for the given dataset family', () => {
        const datasetList: Dataset[] = component.getDatasetsByFamily(1);
        expect(datasetList.length).toBe(2);
        expect(datasetList[0].name).toBe('my-dataset');
        expect(datasetList[1].name).toBe('another-dataset');
    });

    it('#getIsAllSelected() should return true if all datasets of the given dataset family are selected', () => {
        component.selectedDatasets = ['my-dataset', 'another-dataset'];
        expect(component.getIsAllSelected(1)).toBeTruthy();
    });

    it('#getIsAllSelected() should return false if not all datasets of the given dataset family are selected', () => {
        component.selectedDatasets = ['my-dataset'];
        expect(component.getIsAllSelected(1)).toBeFalsy();
    });

    it('#getIsAllUnselected() should return true if all datasets of the given dataset family are not selected', () => {
        component.selectedDatasets = [];
        expect(component.getIsAllUnselected(1)).toBeTruthy();
    });

    it('#getIsAllUnselected() should return false if not all datasets of the given dataset family are not selected', () => {
        component.selectedDatasets = ['my-dataset'];
        expect(component.getIsAllUnselected(1)).toBeFalsy();
    });
});
