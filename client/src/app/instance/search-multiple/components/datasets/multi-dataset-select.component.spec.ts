/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import { MultiDatasetSelectComponent } from './multi-dataset-select.component';

describe('[Instance][SearchMultiple][Component][Datasets] MultiDatasetSelectComponent', () => {
    let component: MultiDatasetSelectComponent;
    let fixture: ComponentFixture<MultiDatasetSelectComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [MultiDatasetSelectComponent],
            imports: [TooltipModule.forRoot()]
        });
        fixture = TestBed.createComponent(MultiDatasetSelectComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
