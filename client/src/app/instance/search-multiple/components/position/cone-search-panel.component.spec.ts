/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ConeSearchPanelComponent } from "./cone-search-panel.component";
import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ConeSearch } from "src/app/instance/store/models";
import { AccordionModule } from "ngx-bootstrap/accordion";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

describe('Instance][SearchMultiple][Component][Position] ConeSearchPanelComponent', () => {
    let component: ConeSearchPanelComponent;
    let fixture: ComponentFixture<ConeSearchPanelComponent>;

    @Component({
        selector: 'app-cone-search',
        template: '',
    })
    class ConeSearchComponent {
        @Input() coneSearch: ConeSearch;
        @Input() defaultRadius: number;
        @Input() defaultRaDecUnit: string;
        @Input() resolverEnabled: boolean;
        @Input() resolverIsLoading: boolean;
        @Input() resolverIsLoaded: boolean;
        @Output() retrieveCoordinates: EventEmitter<string> = new EventEmitter();
        @Output() emitAdd: EventEmitter<{}> = new EventEmitter<{}>();
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConeSearchPanelComponent,
                ConeSearchComponent,
            ],
            imports: [AccordionModule, BrowserAnimationsModule],
        });
        fixture = TestBed.createComponent(ConeSearchPanelComponent);
        component = fixture.componentInstance;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('should emit addConeSearch event with the new cone search when conesearch property is undefined', () => {
        let spy = jest.spyOn(component.addConeSearch, 'emit');
        component.emitAdd({ ra: 10, dec: 5, radius: 10 });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ra: 10, dec: 5, radius: 10 });
    });

    it('should emit updateConeSearch event with the new cone search when conesearch property is defined', () => {
        let spy = jest.spyOn(component.updateConeSearch, 'emit');
        component.coneSearch = { ra: 1, dec: 5, radius: 1 };
        component.emitAdd({ ra: 10, dec: 5, radius: 10 });
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ra: 10, dec: 5, radius: 10 });
    });
});
