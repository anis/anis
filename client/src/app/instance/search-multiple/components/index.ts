/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ProgressBarMultipleComponent } from './progress-bar-multiple.component';
import { positionComponents } from './position';
import { datasetsComponents } from './datasets';
import { resultComponents } from './result';

export const dummiesComponents = [
    ProgressBarMultipleComponent,
    positionComponents,
    datasetsComponents,
    resultComponents
];
