/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AbstractSearchMultipleComponent } from './abstract-search-multiple.component';
import { DatasetGroup } from 'src/app/metamodel/models';
import * as searchMultipleActions from 'src/app/instance/store/actions/search-multiple.actions';
import * as authSelector from 'src/app/user/store/selectors/auth.selector';
import * as datasetGroupSelector from 'src/app/metamodel/selectors/dataset-group.selector';
import { AppConfigService } from 'src/app/app-config.service';

@Component({
    selector: 'app-datasets',
    templateUrl: 'datasets.component.html'
})
export class DatasetsComponent extends AbstractSearchMultipleComponent implements OnInit {
    public isAuthenticated: Observable<boolean>;
    public userRoles: Observable<string[]>;
    public datasetGroupList: Observable<DatasetGroup[]>;
    public datasetGroupListIsLoading: Observable<boolean>;
    public datasetGroupListIsLoaded: Observable<boolean>;

    constructor(protected override store: Store<{ }>, private config: AppConfigService) {
        super(store);
        this.isAuthenticated = store.select(authSelector.selectIsAuthenticated);
        this.userRoles = store.select(authSelector.selectUserRoles);
        this.datasetGroupList = store.select(datasetGroupSelector.selectAllDatasetGroups);
        this.datasetGroupListIsLoading = store.select(datasetGroupSelector.selectDatasetGroupListIsLoading);
        this.datasetGroupListIsLoaded = store.select(datasetGroupSelector.selectDatasetGroupListIsLoaded);
    }

    override ngOnInit(): void {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(searchMultipleActions.changeStep({ step: 'datasets' })));
        Promise.resolve(null).then(() => this.store.dispatch(searchMultipleActions.checkDatasets()));
        super.ngOnInit();
    }

    /**
     * Checks if authentication is enabled.
     *
     * @return boolean
     */
    getAuthenticationEnabled(): boolean {
        return this.config.authenticationEnabled;
    }

    /**
     * Returns admin roles list
     * 
     * @returns string[]
     */
    getAdminRoles(): string[] {
        return this.config.adminRoles;
    }

    /**
     * Dispatches action to update selected datasets.
     *
     * @param  {string[]} selectedDatasets - The selected datasets.
     */
    updateSelectedDatasets(selectedDatasets: string[]): void {
        this.store.dispatch(searchMultipleActions.updateSelectedDatasets({ selectedDatasets }));
    }
}
