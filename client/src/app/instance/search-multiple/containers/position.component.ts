/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AbstractSearchMultipleComponent } from './abstract-search-multiple.component';
import { ConeSearch } from 'src/app/instance/store/models';
import * as searchMultipleActions from 'src/app/instance/store/actions/search-multiple.actions';
import * as coneSearchActions from 'src/app/instance/store/actions/cone-search.actions';
import * as coneSearchSelector from 'src/app/instance/store/selectors/cone-search.selector';

@Component({
    selector: 'app-position',
    templateUrl: 'position.component.html'
})
export class PositionComponent extends AbstractSearchMultipleComponent implements OnInit {
    public resolverIsLoading: Observable<boolean>;
    public resolverIsLoaded: Observable<boolean>;

    constructor(protected override store: Store<{ }>) {
        super(store);
        this.resolverIsLoading = this.store.select(coneSearchSelector.selectResolverIsLoading);
        this.resolverIsLoaded = this.store.select(coneSearchSelector.selectResolverIsLoaded);
    }

    override ngOnInit(): void {
        // Create a micro task that is processed after the current synchronous code
        // This micro task prevent the expression has changed after view init error
        Promise.resolve(null).then(() => this.store.dispatch(searchMultipleActions.changeStep({ step: 'position' })));
        super.ngOnInit();
    }

    /**
     * Dispatches action to add the given cone search.
     *
     * @param  {ConeSearch} coneSearch - The cone search.
     */
    addConeSearch(coneSearch: ConeSearch): void {
        this.store.dispatch(coneSearchActions.addConeSearch({ coneSearch }));
    }

    /**
     * Dispatches action to update cone search.
     *
     * @param  {ConeSearch} coneSearch - The cone search.
     */
    updateConeSearch(coneSearch: ConeSearch): void {
        this.store.dispatch(coneSearchActions.updateConeSearch({ coneSearch }));
    }

    /**
     * Dispatches action to remove cone search.
     */
    deleteConeSearch(): void {
        this.store.dispatch(coneSearchActions.deleteConeSearch());
    }

    /**
     * Dispatches action to retrieve coordinates of object from its name.
     *
     * @param  {string} name - The object name.
     */
    retrieveCoordinates(name: string): void {
        this.store.dispatch(coneSearchActions.retrieveCoordinates({ name }));
    }
}
