/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { TestBed, waitForAsync, ComponentFixture  } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { SearchMultipleComponent } from './search-multiple.component';
import { Instance } from '../../metamodel/models';
import { ConeSearch, SearchMultipleQueryParams } from '../store/models';

describe('[Instance][SearchMultiple] SearchMultipleComponent', () => {
    @Component({ selector: 'app-progress-bar-multiple', template: '' })
    class ProgressBarMultipleStubComponent {
        @Input() instance: Instance;
        @Input() currentStep: string;
        @Input() positionStepChecked: boolean;
        @Input() datasetsStepChecked: boolean;
        @Input() resultStepChecked: boolean;
        @Input() coneSearch: ConeSearch;
        @Input() selectedDatasets: string[];
        @Input() queryParams: SearchMultipleQueryParams;
    }

    let component: SearchMultipleComponent;
    let fixture: ComponentFixture<SearchMultipleComponent>;
    let store: MockStore;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                SearchMultipleComponent,
                ProgressBarMultipleStubComponent
            ],
            providers: [provideMockStore({ })]
        }).compileComponents();
        fixture = TestBed.createComponent(SearchMultipleComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });
});
