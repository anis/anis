import { Injectable } from '@angular/core';

import { DesignConfig } from 'src/app/metamodel/models';
import { StyleService } from 'src/app/shared/services/style.service';

@Injectable({
    providedIn: 'root'
})
export class InstanceStyleService {
    constructor(private style: StyleService) { }

    public applyInstanceStyle(designConfig: DesignConfig) {
        this.generalStyle(designConfig);
        this.navbarStyle(designConfig);
        this.footerStyle(designConfig);
        this.searchProgressBarStyle(designConfig);
        this.searchNextBackButtonsStyle(designConfig);
        this.searchDeleteStyle(designConfig);
        this.searchFamilyStyle(designConfig);
        this.searchInfoStyle(designConfig);
        this.searchDatasetSelectionStyle(designConfig);
        this.searchCriteriaStyle(designConfig);
        this.searchOutputColumnsStyle(designConfig);
        this.searchResultPanel(designConfig);
        this.searchResultTable(designConfig);
    }

    private generalStyle(designConfig: DesignConfig) {
        this.style.setStyles('.instance-main', {
            'background-color': designConfig.design_background_color,
            'color': designConfig.design_text_color,
            'font-family': designConfig.design_font_family
        });
        this.style.setStyle('.instance-main a', 'color', designConfig.design_link_color);
        this.style.setStyle('.instance-main a:hover', 'color', designConfig.design_link_hover_color);
        this.style.setStyle('.instance-main a.btn', 'color', '#212529');
    }

    private navbarStyle(designConfig: DesignConfig) {
        this.style.setStyles('.navbar-instance', {
            'background-color': designConfig.navbar_background_color,
            'border-bottom': `1px solid ${designConfig.navbar_border_bottom_color}`,
            'font-family': designConfig.navbar_font_family
        });
        this.style.setStyle('.navbar-instance a, .navbar-instance .webpage-family-nav-mobile-label', 'color', designConfig.navbar_color_href);
        this.style.setStyle('.navbar-instance a:hover', 'color', designConfig.navbar_color_href);
        this.style.setStyle('.navbar-instance a.active', 'font-weight', 'bold');
        this.style.setStyles('.navbar-instance .dropdown-menu', {
            'background-color': designConfig.navbar_background_color
        });
        this.style.setStyles('.navbar-instance .dropdown-item:hover', {
            'background-color': designConfig.navbar_background_color,
            'color': designConfig.navbar_color_href
        })
        this.style.setStyles('.navbar-instance .dropdown-item.active', {
            'background-color': designConfig.navbar_background_color,
            'color': designConfig.navbar_color_href
        });
        this.style.setStyle('.navbar-instance .navbar-toggler', 'color', designConfig.navbar_color_href);
        this.style.setStyles('.navbar-instance #button-sign-in', {
            'color': designConfig.navbar_sign_in_btn_color,
            'border-color': designConfig.navbar_sign_in_btn_color
        });
        this.style.setStyles('.navbar-instance #button-sign-in:hover', {
            'background-color': designConfig.navbar_background_color
        });
        this.style.setStyles('.navbar-instance #button-basic', {
            'background-color': designConfig.navbar_background_color,
            'border': 'none'
        });
        this.style.setStyle('.navbar-instance #button-user', 'color', designConfig.navbar_user_btn_color);
    }

    private footerStyle(designConfig: DesignConfig) {
        this.style.setStyles('.footer', {
            'background-color': designConfig.footer_background_color,
            'border-top': `1px solid ${designConfig.footer_border_top_color}`,
            'color': designConfig.footer_text_color,
        });
    }

    private searchProgressBarStyle(designConfig: DesignConfig) {
        this.style.setStyle('.progress-bar-title', 'color', designConfig.progress_bar_title_color);
        this.style.setStyle('.progress-bar-subtitle', 'color', designConfig.progress_bar_subtitle_color);
        this.style.setStyle('.progress-navigation .progress.progress-with-circle', 'background-color', designConfig.progress_bar_color);
        this.style.setStyle('.progress-navigation .progress.progress-with-circle .progress-bar', 'background-color', designConfig.progress_bar_active_color);
        const progressTitleBold = (designConfig.progress_bar_text_bold) ? 'bold' : 'normal';
        this.style.setStyles('.progress-navigation .nav-link, .progress-navigation .nav-link.disabled', {
            'color': designConfig.progress_bar_text_color,
            'font-weight': progressTitleBold
        });
        this.style.setStyle('.progress-navigation .nav-link:hover', 'color', designConfig.progress_bar_text_color);
        this.style.setStyle('.progress-navigation .nav-item.checked .nav-link, .progress-navigation.nav-item.active .nav-link', 'color', designConfig.progress_bar_active_color);
        this.style.setStyles('.progress-navigation .nav-item .icon-circle', {
            'border-color': designConfig.progress_bar_color,
            'background-color': designConfig.progress_bar_circle_color,
            'color': designConfig.progress_bar_circle_icon_color
        });
        this.style.setStyles('.progress-navigation .nav-item.checked .icon-circle', {
            'background-color': designConfig.progress_bar_circle_color,
            'border-color': designConfig.progress_bar_active_color,
            'color': designConfig.progress_bar_active_color
        });
        this.style.setStyles('.progress-navigation .nav-item.active .icon-circle', {
            'border-color': designConfig.progress_bar_active_color,
            'background-color': designConfig.progress_bar_active_color,
            'color': designConfig.progress_bar_circle_icon_active_color
        });
    }

    private searchNextBackButtonsStyle(designConfig: DesignConfig) {
        // Next button
        this.style.setStyles('.search-next.btn.btn-outline-primary, .search-info a.btn.btn-outline-primary', {
            'color': designConfig.search_next_btn_color,
            'border-color': designConfig.search_next_btn_color
        });
        this.style.setStyles('.search-next:hover.btn.btn-outline-primary, .search-info a:hover.btn.btn-outline-primary', {
            'color': designConfig.search_next_btn_hover_text_color,
            'background-color': designConfig.search_next_btn_hover_color,
            'border-color': designConfig.search_next_btn_hover_color
        });

        // Back button
        this.style.setStyles('.search-back.btn.btn-outline-secondary', {
            'color': designConfig.search_back_btn_color,
            'border-color': designConfig.search_back_btn_color
        });
        this.style.setStyles('.search-back:hover.btn.btn-outline-secondary', {
            'color': designConfig.search_back_btn_hover_text_color,
            'background-color': designConfig.search_back_btn_hover_color,
            'border-color': designConfig.search_back_btn_hover_color
        });
    }

    private searchDeleteStyle(designConfig: DesignConfig) {
        // Delete current query button
        this.style.setStyles('.delete-current-query.btn.btn-outline-primary', {
            'color': designConfig.delete_current_query_btn_color,
            'border-color': designConfig.delete_current_query_btn_color
        });
        this.style.setStyles('.delete-current-query:hover.btn.btn-outline-primary', {
            'color': designConfig.delete_current_query_btn_hover_text_color,
            'background-color': designConfig.delete_current_query_btn_hover_color,
            'border-color': designConfig.delete_current_query_btn_hover_color
        });

        this.style.setStyle('.search_criterium .delete-cross', 'color', designConfig.delete_criterion_cross_color);
    }

    private searchFamilyStyle(designConfig: DesignConfig) {
        this.style.setStyle('.panel.card.custom-accordion', 'border-color', designConfig.family_border_color);
        this.style.setStyle('.panel.card.custom-accordion .card-header', 'border-bottom-color', designConfig.family_border_color);
        this.style.setStyle('.custom-accordion .panel-heading', 'background-color', designConfig.family_header_background_color);
        this.style.setStyle('.custom-accordion .panel-heading .btn-link', 'color', designConfig.family_title_color);
        const familyTitleBold = (designConfig.family_title_bold) ? 'bold' : 'normal';
        this.style.setStyle('.custom-accordion .panel-heading .btn-link', 'font-weight', familyTitleBold);
        this.style.setStyles('.custom-accordion .panel-body', {
            'color': designConfig.family_text_color,
            'background-color': `${designConfig.family_background_color}`
        });
        this.style.setStyle('.custom-accordion .panel-body .card', 'background-color', designConfig.family_background_color);
    }

    private searchInfoStyle(designConfig: DesignConfig) {
        this.style.setStyles('.search-info.jumbotron', {
            'background-color': designConfig.search_info_background_color,
            'color': designConfig.search_info_text_color
        });
        this.style.setStyle('.search-info .btn.btn-outline-primary', 'color', '#007BFF');
        this.style.setStyle('.search-info .btn.btn-outline-primary:hover', 'color', '#FFFFFF');
    }

    private searchDatasetSelectionStyle(designConfig: DesignConfig) {
        this.style.setStyles('.dataset-select-btn.btn.btn-outline-secondary', {
            'color': designConfig.dataset_select_btn_color,
            'border-color': designConfig.dataset_select_btn_color
        });
        this.style.setStyles('.dataset-select-btn:hover.btn.btn-outline-secondary', {
            'color': designConfig.dataset_select_btn_hover_text_color,
            'background-color': designConfig.dataset_select_btn_hover_color,
            'border-color': designConfig.dataset_select_btn_hover_color
        });
        this.style.setStyle('.search-dataset-selected', 'color', designConfig.dataset_selected_icon_color);
    }

    private searchCriteriaStyle(designConfig: DesignConfig) {
        this.style.setStyles('.search_criterium', {
            'background-color': designConfig.search_criterion_background_color,
            'color': designConfig.search_criterion_text_color
        });
    }

    private searchOutputColumnsStyle(designConfig: DesignConfig) {
        this.style.setStyle('.output_columns_selected', 'color', designConfig.output_columns_selected_color);

        this.style.setStyles('.select-all.btn.btn-outline-secondary', {
            'color': designConfig.output_columns_select_all_btn_color,
            'border-color': designConfig.output_columns_select_all_btn_color
        });
        this.style.setStyles('.select-all:not([disabled]):hover.btn.btn-outline-secondary', {
            'color': designConfig.output_columns_select_all_btn_hover_text_color,
            'background-color': designConfig.output_columns_select_all_btn_hover_color,
            'border-color': designConfig.output_columns_select_all_btn_hover_color
        });
    }

    private searchResultPanel(designConfig: DesignConfig) {
        this.style.setStyles('.result-panel', {
            'border': `${designConfig.result_panel_border_size} solid ${designConfig.result_panel_border_color}`,
            'background-color': designConfig.result_panel_background_color,
            'color': designConfig.result_panel_text_color
        });
        this.style.setStyles('.result-panel h3, .search-info h3', {
            'color': designConfig.result_panel_title_color,
            'font-weight': 'bold',
            'border-bottom': `1px solid ${designConfig.result_panel_border_color}`,
            'margin-bottom': '20px',
            'padding-left': '10px'
        });
    }

    private searchResultTable(designConfig: DesignConfig) {
        // Result header (download + SAMP)
        this.style.setStyles('.search-info .btn.btn-primary', {
            'background-color': designConfig.result_download_btn_color,
            'border-color': designConfig.result_download_btn_color,
            'color': designConfig.result_download_btn_text_color
        });
        this.style.setStyles('.search-info .btn.btn-primary:hover', {
            'background-color': designConfig.result_download_btn_hover_color,
            'border-color': designConfig.result_download_btn_hover_color,
            'color': designConfig.result_download_btn_text_color
        });

        // Datatable button actions
        this.style.setStyles('.btn-datatable-actions.btn.btn-primary', {
            'color': designConfig.result_datatable_actions_btn_text_color,
            'background-color': designConfig.result_datatable_actions_btn_color,
            'border-color': designConfig.result_datatable_actions_btn_color
        });
        this.style.setStyles('.btn-datatable-actions:not([disabled]):hover.btn.btn-primary', {
            'background-color': designConfig.result_datatable_actions_btn_hover_color,
            'border-color': designConfig.result_datatable_actions_btn_hover_color,
        });
        this.style.setStyles('.btn-datatable-actions:not([disabled]):focus.btn.btn-primary', {
            'box-shadow': 'none'
        });

        // Datatable
        if (designConfig.result_datatable_bordered) {
            this.style.setStyle('#datatable.table-bordered th, #datatable.table-bordered td', 'border', `1px solid ${designConfig.result_datatable_border_color}`);
            this.style.setStyle('#datatable.table-bordered thead th', 'border-bottom', `2px solid ${designConfig.result_datatable_border_color}`);
            if (designConfig.result_datatable_bordered_radius) {
                this.style.setStyles('.datatable-responsive.table-responsive', {
                    'border-top-left-radius': '0.3rem',
                    'border-top-right-radius': '0.3rem',
                });
            }
        } else {
            this.style.setStyle('#datatable.table th, #datatable.table td', 'border-top', `1px solid ${designConfig.result_datatable_border_color}`);
            this.style.setStyle('#datatable.table thead th', 'border-bottom', `2px solid ${designConfig.result_datatable_border_color}`);
        }
        this.style.setStyles('#datatable.table thead tr', {
            'background-color': designConfig.result_datatable_header_background_color,
            'color': designConfig.result_datatable_header_text_color
        });
        this.style.setStyles('#datatable.table thead tr .column-sorted', {
            'color': designConfig.result_datatable_sorted_active_color,
            'background-color': designConfig.result_datatable_header_background_color
        });
        this.style.setStyle('#datatable.table thead tr .click-to-sort .unsorted', 'color', designConfig.result_datatable_sorted_color);
        this.style.setStyles('#datatable.table thead tr .click-to-sort .on-hover', {
            'color': designConfig.result_datatable_sorted_color,
            'background-color': designConfig.result_datatable_header_background_color
        });
        this.style.setStyles('#datatable.table tbody tr', {
            'background-color': designConfig.result_datatable_rows_background_color,
            'color': designConfig.result_datatable_rows_text_color
        });
        this.style.setStyle('#datatable.table tbody tr.datum-selected-in-plot', 'background-color', designConfig.result_datatable_rows_selected_color);
        this.style.setStyle('#datatable.table tbody tr .checked', 'color', designConfig.result_datatable_rows_selected_color);
        this.style.setStyle('#datatable.table a, .detail a.btn', 'color', designConfig.result_datatable_link_color);
        this.style.setStyle('#datatable.table a, .detail a', 'text-decoration', 'none');
        this.style.setStyle('#datatable.table a.btn-outline-primary, .detail a.btn-outline-primary', 'border-color', designConfig.result_datatable_link_color);
        this.style.setStyle('#datatable.table a:hover, .detail a:hover', 'color', designConfig.result_datatable_link_hover_color);
        this.style.setStyle('#datatable.table a.btn-outline-primary:hover, .detail a.btn-outline-primary:hover', 'color', designConfig.result_datatable_link_hover_color);
        this.style.setStyle('#datatable.table a.btn-outline-primary:hover, .detail a.btn-outline-primary:hover', 'background-color', designConfig.result_datatable_rows_background_color);
        this.style.setStyle('#datatable.table a.btn-outline-primary:hover, .detail a.btn-outline-primary:hover', 'border-color', designConfig.result_datatable_link_hover_color);
    
        // Datatable pagination
        this.style.setStyle('.page-item > .page-link', 'color', designConfig.result_datatable_pagination_link_color);
        this.style.setStyles('.pagination-page.page-item.active > .page-link', {
            'backgroundColor': designConfig.result_datatable_pagination_active_bck_color,
            'borderColor': designConfig.result_datatable_pagination_active_bck_color,
            'color': designConfig.result_datatable_pagination_active_text_color
        });
    }
}
