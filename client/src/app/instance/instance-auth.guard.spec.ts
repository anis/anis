/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold } from 'jasmine-marbles';
import { Router } from '@angular/router';

import * as authSelector from 'src/app/user/store/selectors/auth.selector';
import { InstanceAuthGuard } from './instance-auth.guard';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as fromSharedUtils from 'src/app/shared/utils';
import { AppConfigService } from '../app-config.service';
import { Instance } from '../metamodel/models';
import { AppRoutingModule } from 'src/app/app-routing.module';

describe('[instance] InstanceAuthGuard', () => {
    let store: MockStore;
    let instance: Instance;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [AppRoutingModule],
            providers: [provideMockStore({})],
        });
    });

    it('should create instanceAuthGuard', () => {
        TestBed.overrideProvider(AppConfigService, {
            useValue: { authenticationEnabled: true },
        });
        store = TestBed.inject(MockStore);
        TestBed.inject(Router);

        let instanceAuthGuard = TestBed.inject(InstanceAuthGuard);
        expect(instanceAuthGuard).toBeTruthy();
    });

    it('canActivate() should return true if authenticationEnabled is false', () => {
        TestBed.overrideProvider(AppConfigService, {
            useValue: { authenticationEnabled: true },
        });
        store = TestBed.inject(MockStore);
        store.overrideSelector(
            instanceSelector.selectInstanceListIsLoaded,
            true
        );
        store.overrideSelector(instanceSelector.selectInstanceByRouteName, {
            ...instance,
            name: 'test',
            groups: [{ role: 'test' }],
        });
        store.overrideSelector(authSelector.selectUserRoles, []);
        store.overrideSelector(authSelector.selectIsAuthenticated, true);
        let spyOnIsAdmin = jest.spyOn(fromSharedUtils, 'isAdmin');
        spyOnIsAdmin.mockImplementation(() => true);

        let instanceAuthGuard = TestBed.inject(InstanceAuthGuard);
        let expected = cold('a', { a: true });
        expect(instanceAuthGuard.canActivate()).toBeObservable(expected);
    });

    it('canActivate() should return true if user is authenticated and authorized ', () => {
        TestBed.overrideProvider(AppConfigService, {
            useValue: { authenticationEnabled: true },
        });
        store = TestBed.inject(MockStore);
        store.overrideSelector(
            instanceSelector.selectInstanceListIsLoaded,
            true
        );
        store.overrideSelector(instanceSelector.selectInstanceByRouteName, {
            ...instance,
            name: 'test',
            groups: [{ role: 'test' }],
        });
        store.overrideSelector(authSelector.selectUserRoles, ['test']);
        store.overrideSelector(authSelector.selectIsAuthenticated, true);
        let spyOnIsAdmin = jest.spyOn(fromSharedUtils, 'isAdmin');
        spyOnIsAdmin.mockImplementation(() => false);
        let instanceAuthGuard = TestBed.inject(InstanceAuthGuard);
        let expected = cold('a', { a: true });
        expect(instanceAuthGuard.canActivate()).toBeObservable(expected);
    });

    it('canActivate() should return false', () => {
        TestBed.overrideProvider(AppConfigService, {
            useValue: { authenticationEnabled: true },
        });
        store = TestBed.inject(MockStore);
        store.overrideSelector(
            instanceSelector.selectInstanceListIsLoaded,
            true
        );
        store.overrideSelector(instanceSelector.selectInstanceByRouteName, {
            ...instance,
            name: 'test',
            groups: [{ role: 'test' }],
        });
        store.overrideSelector(authSelector.selectUserRoles, ['test']);
        store.overrideSelector(authSelector.selectIsAuthenticated, false);
        let spyOnIsAdmin = jest.spyOn(fromSharedUtils, 'isAdmin');
        spyOnIsAdmin.mockImplementation(() => false);
        let instanceAuthGuard = TestBed.inject(InstanceAuthGuard);
        let expected = cold('a', { a: false });
        expect(instanceAuthGuard.canActivate()).toBeObservable(expected);
    });
});
