/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Directive, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Instance, DesignConfig } from 'src/app/metamodel/models';
import { InstanceStyleService } from '../instance-style.service';
import { AppConfigService } from 'src/app/app-config.service';

@Directive({
   selector: 'app-instance-template'
})
export class InstanceTemplateDirective implements OnInit {
    @Input() instance: Instance;
    @Input() designConfig: DesignConfig;
    
    public favIcon: HTMLLinkElement = document.querySelector('#favicon');
    public body: HTMLBodyElement = document.querySelector('body');

    constructor(
        private style: InstanceStyleService,
        private config: AppConfigService,
        private http: HttpClient,
    ) { }

    ngOnInit(): void {
        if (this.designConfig) {
            if (this.designConfig.design_favicon !== '') {
                this.setFaviconHref();
            }
            if(document.styleSheets.length > 0) {
                this.body.style.backgroundColor = this.designConfig.design_background_color;
                this.style.applyInstanceStyle(this.designConfig);
            }
        }
    }

    setFaviconHref() {
        const src = `${this.config.apiUrl}/instance/${this.instance.name}/file-explorer${this.designConfig.design_favicon}`;
        if (this.instance.public) {
            this.favIcon.href = src;
        } else {
            this.http.get(src, { responseType: 'blob' }).subscribe(data => {
                const reader = new FileReader();
                reader.readAsDataURL(data);
                reader.onloadend = () => {
                    const base64data = reader.result;                
                    this.favIcon.href = base64data as string;
                }
            });
        }
    }
}
