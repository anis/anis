/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { Instance, DesignConfig, Logo } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';

@Component({
    selector: 'app-instance-footer',
    templateUrl: 'instance-footer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstanceFooterComponent {
    @Input() instance: Instance;
    @Input() designConfig: DesignConfig;

    public anisClientVersion = '3.16.0';
    public year: number = (new Date()).getFullYear();

    constructor(private config: AppConfigService) { }

    /**
     * Returns logo href.
     *
     * @return string
     */
    getLogoHref(logo: Logo): string {
        return `${this.config.apiUrl}/instance/${this.instance.name}/file-explorer${logo.file}`;
    }
}
