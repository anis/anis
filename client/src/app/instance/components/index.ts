/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InstanceTemplateDirective } from './instance-template.directive';
import { InstanceNavbarComponent } from './instance-navbar.component';
import { InstanceFooterComponent } from './instance-footer.component';
import { menuItemComponents } from './menu-item';
import { menuItemMobileComponents } from './menu-item-mobile';

export const dummiesComponents = [
    InstanceTemplateDirective,
    InstanceNavbarComponent,
    InstanceFooterComponent,
    menuItemComponents,
    menuItemMobileComponents
];
