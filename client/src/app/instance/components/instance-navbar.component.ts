/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

import { UserProfile } from 'src/app/user/store/models/user-profile.model';
import { DesignConfig, Instance, MenuItem } from 'src/app/metamodel/models';
import { isAdmin } from 'src/app/shared/utils';
import { AppConfigService } from 'src/app/app-config.service';

@Component({
    selector: 'app-instance-navbar',
    templateUrl: 'instance-navbar.component.html',
    styleUrls: [ 'instance-navbar.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstanceNavbarComponent {
    @Input() isAuthenticated: boolean;
    @Input() userProfile: UserProfile = null;
    @Input() userRoles: string[];
    @Input() instance: Instance;
    @Input() designConfig: DesignConfig;
    @Input() menuItemList: MenuItem[];
    @Output() login: EventEmitter<any> = new EventEmitter();
    @Output() logout: EventEmitter<any> = new EventEmitter();
    @Output() openEditProfile: EventEmitter<any> = new EventEmitter();

    constructor(private config: AppConfigService) { }

    /**
     * Checks if authentication is enabled.
     *
     * @return boolean
     */
    getAuthenticationEnabled(): boolean {
        return this.config.authenticationEnabled;
    }

    /**
     * Returns true if user is admin
     * 
     * @returns boolean
     */
    isAdmin() {
        return isAdmin(this.config.adminRoles, this.userRoles);
    }

    /**
     * Returns logo URL.
     *
     * @return  string
     */
    getLogoURL(): string {
        if (this.designConfig.design_logo) {
            return `${this.config.apiUrl}/instance/${this.instance.name}/file-explorer${this.designConfig.design_logo}`;
        }
        return 'assets/cesam_anis40.png';
    }

    getLogoHref(): string {
        if (this.designConfig.design_logo_href) {
            return this.designConfig.design_logo_href;
        } else {
            return this.getInstanceBaseHref();
        }
    }

    private getInstanceBaseHref() {
        return `/${this.instance.name}`;
    }
}
