/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuItemFamilyComponent } from './menu-item-family.component';
import { MENU_FAMILY } from 'src/test-data';

describe('[instance][components] MenuItemFamilyComponent', () => {
    let component: MenuItemFamilyComponent;
    let fixture: ComponentFixture<MenuItemFamilyComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                MenuItemFamilyComponent
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(MenuItemFamilyComponent);
        component = fixture.componentInstance;
        component.menuFamily = { ...MENU_FAMILY };
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
