import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { Instance, Url } from 'src/app/metamodel/models';

@Component({
    selector: 'app-menu-item-url',
    templateUrl: 'menu-item-url.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuItemUrlComponent {
    @Input() url: Url;
    @Input() instance: Instance;

    getInternalUrl() {
        return `/${this.instance.name}${this.url.href}`;
    }
}
