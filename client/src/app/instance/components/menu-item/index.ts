import { MenuItemNavComponent } from './menu-item-nav.component';
import { MenuItemWebpageComponent } from './menu-item-webpage.component';
import { MenuItemUrlComponent } from './menu-item-url.component';
import { MenuItemFamilyComponent } from './menu-item-family.component';

export const menuItemComponents = [
    MenuItemNavComponent,
    MenuItemWebpageComponent,
    MenuItemUrlComponent,
    MenuItemFamilyComponent
];
