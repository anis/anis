/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuItemNavComponent } from './menu-item-nav.component';
import { WEBPAGE } from 'src/test-data';

describe('[instance][components] MenuItemNavComponent', () => {
    let component: MenuItemNavComponent;
    let fixture: ComponentFixture<MenuItemNavComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                MenuItemNavComponent
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(MenuItemNavComponent);
        component = fixture.componentInstance;
        component.menuItem = WEBPAGE;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
