import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { MenuFamily, Webpage } from 'src/app/metamodel/models';

@Component({
    selector: 'app-menu-item-webpage',
    templateUrl: 'menu-item-webpage.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuItemWebpageComponent {
    @Input() webpage: Webpage;
    @Input() menuFamily: MenuFamily;

    getRouterLink(): string {
        if (this.menuFamily) {
            return `${this.menuFamily.name}/${this.webpage.name}`; 
        } else {
            return `${this.webpage.name}`;
        }
    }
}
