import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { MenuItem, Webpage, Url, MenuFamily, Instance } from 'src/app/metamodel/models';

@Component({
    selector: 'app-menu-item-nav',
    templateUrl: 'menu-item-nav.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuItemNavComponent {
    @Input() menuItem: MenuItem;
    @Input() instance: Instance;
    @Input() menuFamily: MenuFamily;

    getWebpage() {
        return this.menuItem as Webpage;
    }

    getUrl() {
        return this.menuItem as Url;
    }

    getMenuFamily() {
        return this.menuItem as MenuFamily;
    }
}
