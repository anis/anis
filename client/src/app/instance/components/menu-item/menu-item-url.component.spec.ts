/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuItemUrlComponent } from './menu-item-url.component';
import { URL } from 'src/test-data';

describe('[instance][components] MenuItemUrlComponent', () => {
    let component: MenuItemUrlComponent;
    let fixture: ComponentFixture<MenuItemUrlComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                MenuItemUrlComponent
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(MenuItemUrlComponent);
        component = fixture.componentInstance;
        component.url = { ...URL };
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
