/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { menuItemComponents } from './index';

describe('[instance][components][menu-item] index', () => {
    it('should test index', () => {
        expect(menuItemComponents.length).toEqual(4);
    });
});
