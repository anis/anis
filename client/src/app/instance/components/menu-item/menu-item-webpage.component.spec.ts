/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuItemWebpageComponent } from './menu-item-webpage.component';
import { WEBPAGE } from 'src/test-data';

describe('[instance][components] MenuItemWebpageComponent', () => {
    let component: MenuItemWebpageComponent;
    let fixture: ComponentFixture<MenuItemWebpageComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                MenuItemWebpageComponent
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(MenuItemWebpageComponent);
        component = fixture.componentInstance;
        component.webpage = { ...WEBPAGE };
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
