import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { Instance, MenuFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-menu-item-family',
    templateUrl: 'menu-item-family.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuItemFamilyComponent {
    @Input() menuFamily: MenuFamily;
    @Input() instance: Instance;

    getMenuFamilyItemsByDisplay() {
        return [...this.menuFamily.items].sort((a, b) => a.display - b.display);
    }
}
