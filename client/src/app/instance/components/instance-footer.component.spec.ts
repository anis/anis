/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstanceFooterComponent } from './instance-footer.component';
import { DesignConfig, Instance } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';
import { INSTANCE, DESIGN_CONFIG } from 'src/test-data';

describe('[instance][components] InstanceFooterComponent', () => {
    let component: InstanceFooterComponent;
    let fixture: ComponentFixture<InstanceFooterComponent>;
    let appConfigServiceStub = new AppConfigService();
    let instance: Instance;
    let designConfig: DesignConfig;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InstanceFooterComponent
            ],
            providers: [
                { provide: AppConfigService, useValue: appConfigServiceStub },
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(InstanceFooterComponent);
        component = fixture.componentInstance;
        instance = { ...INSTANCE };
        component.instance = instance;
        designConfig = { ...DESIGN_CONFIG };
        component.designConfig = designConfig;
        appConfigServiceStub.apiUrl = 'http://localhost:8080/';
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('sould return the first logo href', () => {
        let result = component.getLogoHref(designConfig.footer_logos[0]);
        expect(result).toEqual(`${appConfigServiceStub.apiUrl}/instance/${instance.name}/file-explorer${designConfig.footer_logos[0].file}`);
    });
});
