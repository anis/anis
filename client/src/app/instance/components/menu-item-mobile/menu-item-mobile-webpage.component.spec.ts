/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuItemMobileWebpageComponent } from './menu-item-mobile-webpage.component';
import { MENU_FAMILY, WEBPAGE } from 'src/test-data';

describe('[instance][components] MenuItemMobileWebpageComponent', () => {
    let component: MenuItemMobileWebpageComponent;
    let fixture: ComponentFixture<MenuItemMobileWebpageComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [MenuItemMobileWebpageComponent],
        }).compileComponents();
        fixture = TestBed.createComponent(MenuItemMobileWebpageComponent);
        component = fixture.componentInstance;
        component.webpage = { ...WEBPAGE };
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('should use getRouterLink correctly', () => {
        let result: string = component.getRouterLink()
        expect(result).toBe('home')

        component.menuFamily = { ...MENU_FAMILY };
        result = component.getRouterLink()
        expect(result).toBe('search/home')
    });
});
