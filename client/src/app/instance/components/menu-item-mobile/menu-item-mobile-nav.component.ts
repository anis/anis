import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { MenuItem, Instance, Webpage, Url, MenuFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-menu-item-mobile-nav',
    templateUrl: 'menu-item-mobile-nav.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuItemMobileNavComponent {
    @Input() menuItem: MenuItem;
    @Input() instance: Instance;
    @Input() menuFamily: MenuFamily;

    getWebpage() {
        return this.menuItem as Webpage;
    }

    getUrl() {
        return this.menuItem as Url;
    }

    getMenuFamily() {
        return this.menuItem as MenuFamily;
    }
}
