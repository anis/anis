import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { Instance, Url } from 'src/app/metamodel/models';

@Component({
    selector: 'app-menu-item-mobile-url',
    templateUrl: 'menu-item-mobile-url.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuItemMobileUrlComponent {
    @Input() url: Url;
    @Input() instance: Instance;

    getInternalUrl() {
        return `/${this.instance.name}${this.url.href}`;
    }
}
