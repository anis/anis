/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuItemMobileUrlComponent } from './menu-item-mobile-url.component';
import { URL } from 'src/test-data';

describe('[instance][components] MenuItemMobileUrlComponent', () => {
    let component: MenuItemMobileUrlComponent;
    let fixture: ComponentFixture<MenuItemMobileUrlComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                MenuItemMobileUrlComponent
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(MenuItemMobileUrlComponent);
        component = fixture.componentInstance;
        component.url = { ...URL };
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
