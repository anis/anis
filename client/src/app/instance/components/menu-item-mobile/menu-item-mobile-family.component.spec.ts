/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuItemMobileFamilyComponent } from './menu-item-mobile-family.component';
import { MENU_FAMILY } from 'src/test-data';

describe('[instance][components] MenuItemMobileFamilyComponent', () => {
    let component: MenuItemMobileFamilyComponent;
    let fixture: ComponentFixture<MenuItemMobileFamilyComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                MenuItemMobileFamilyComponent
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(MenuItemMobileFamilyComponent);
        component = fixture.componentInstance;
        component.menuFamily = { ...MENU_FAMILY };
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
