import { MenuItemMobileNavComponent } from './menu-item-mobile-nav.component';
import { MenuItemMobileWebpageComponent } from './menu-item-mobile-webpage.component'
import { MenuItemMobileUrlComponent } from './menu-item-mobile-url.component';
import { MenuItemMobileFamilyComponent } from './menu-item-mobile-family.component';

export const menuItemMobileComponents = [
    MenuItemMobileNavComponent,
    MenuItemMobileWebpageComponent,
    MenuItemMobileUrlComponent,
    MenuItemMobileFamilyComponent
];
