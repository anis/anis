import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { Instance, MenuFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-menu-item-mobile-family',
    templateUrl: 'menu-item-mobile-family.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuItemMobileFamilyComponent {
    @Input() menuFamily: MenuFamily;
    @Input() instance: Instance;

    getMenuFamilyItemsByDisplay() {
        return [...this.menuFamily.items].sort((a, b) => a.display - b.display);
    }
}
