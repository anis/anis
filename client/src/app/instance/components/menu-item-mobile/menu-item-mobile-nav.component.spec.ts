/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuItemMobileNavComponent } from './menu-item-mobile-nav.component';
import { WEBPAGE } from 'src/test-data';

describe('[instance][components] MenuItemMobileNavComponent', () => {
    let component: MenuItemMobileNavComponent;
    let fixture: ComponentFixture<MenuItemMobileNavComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                MenuItemMobileNavComponent
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(MenuItemMobileNavComponent);
        component = fixture.componentInstance;
        component.menuItem = WEBPAGE;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
