import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { Webpage, MenuFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-menu-item-mobile-webpage',
    templateUrl: 'menu-item-mobile-webpage.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuItemMobileWebpageComponent {
    @Input() webpage: Webpage;
    @Input() menuFamily: MenuFamily;

    getRouterLink(): string {
        if (this.menuFamily) {
            return `${this.menuFamily.name}/${this.webpage.name}`; 
        } else {
            return `${this.webpage.name}`;
        }
    }
}
