/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstanceNavbarComponent } from './instance-navbar.component';
import { DesignConfig, Instance, MenuItem } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';
import { INSTANCE, DESIGN_CONFIG, MENU_ITEM_LIST } from 'src/test-data';

describe('[instance][components] InstanceNavbarComponent', () => {
    let component: InstanceNavbarComponent;
    let fixture: ComponentFixture<InstanceNavbarComponent>;
    let appConfigServiceStub = new AppConfigService();
    let instance: Instance;
    let designConfig: DesignConfig;
    let menuItemList: MenuItem[];

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InstanceNavbarComponent
            ],
            providers: [
                { provide: AppConfigService, useValue: appConfigServiceStub },
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(InstanceNavbarComponent);
        component = fixture.componentInstance;
        component.isAuthenticated = true;
        component.userProfile = null;
        component.userRoles = ['TEST', 'ADMIN'];
        instance = { ...INSTANCE };
        component.instance = instance;
        designConfig = { ...DESIGN_CONFIG };
        component.designConfig = designConfig;
        menuItemList = [ ... MENU_ITEM_LIST ];
        component.menuItemList = menuItemList;
        appConfigServiceStub.apiUrl = 'http://localhost:8080/';
        appConfigServiceStub.authenticationEnabled = true;
        appConfigServiceStub.adminRoles = ['ADMIN'];
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('sould return true if authentication is enabled', () => {
        let result = component.getAuthenticationEnabled();
        expect(result).toBeTruthy();
    });

    it('sould return true if user is an admin', () => {
        let result = component.isAdmin();
        expect(result).toBeTruthy();
    });

    it('sould return the logo URL', () => {
        let result = component.getLogoURL();
        expect(result).toEqual(`${appConfigServiceStub.apiUrl}/instance/${instance.name}/file-explorer${designConfig.design_logo}`);
    });

    it('sould return the instance base href if logo href is null', () => {
        let result = component.getLogoHref();
        expect(result).toEqual(`/${instance.name}`);
    });
});
