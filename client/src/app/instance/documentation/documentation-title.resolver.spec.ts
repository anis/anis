/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold } from 'jasmine-marbles';

import { DocumentationTitleResolver } from './documentation-title.resolver';
import { Dataset, Instance } from 'src/app/metamodel/models';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';

describe('Instance][Documentation] DocumentationTitleResolver', () => {
    let documentationTitleResolver: DocumentationTitleResolver;
    let store: MockStore;
    let instance: Instance;
    let mockInstanceSelectorSelectInstanceByRouteName;
    let mockDatasetSelectorSelectDatasetByRouteName;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                provideMockStore({}),
            ]
        })

        store = TestBed.inject(MockStore);
        documentationTitleResolver = TestBed.inject(DocumentationTitleResolver);
    });

    it('should be created', () => {
        expect(documentationTitleResolver).toBeTruthy();
    });

    it('should return "test - Documentation"', () => {
        let route: any = { paramMap: { keys: [] } };
        mockInstanceSelectorSelectInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, { ...instance, label: 'test' })
        let result = documentationTitleResolver.resolve(route, null);
        const expected = cold('a', { a: 'test - Documentation' });
        expect(result).toBeObservable(expected);
    });

    it('should return "test - Documentation test"', () => {
        let dataset: Dataset;
        let route: any = { paramMap: { keys: [1, 2] } };
        mockInstanceSelectorSelectInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, { ...instance, label: 'test' })
        mockDatasetSelectorSelectDatasetByRouteName = store.overrideSelector(datasetSelector.selectDatasetByRouteName, { ...dataset, label: 'test' })
        let result = documentationTitleResolver.resolve(route, null);
        const expected = cold('a', { a: 'test - Documentation test' });
        expect(result).toBeObservable(expected);
    });
});
