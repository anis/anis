/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, waitForAsync, ComponentFixture  } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { OutputListComponent } from './output-list.component';

describe('[Instance][Documentation][Component] OutputListComponent', () => {
    let component: OutputListComponent;
    let fixture: ComponentFixture<OutputListComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [OutputListComponent]
        }).compileComponents();
        fixture = TestBed.createComponent(OutputListComponent);
        component = fixture.componentInstance;
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });
});
