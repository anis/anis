/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, waitForAsync, ComponentFixture } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { PopoverModule } from 'ngx-bootstrap/popover';

import { DatasetCardDocComponent } from './dataset-card-doc.component';
import * as fromSharedUtils from 'src/app/shared/utils';
import { DATASET } from 'src/test-data';

describe('[Instance][Documentation][Component] DatasetCardDocComponent', () => {
    let component: DatasetCardDocComponent;
    let fixture: ComponentFixture<DatasetCardDocComponent>;
    let router = { routeReuseStrategy: { shouldReuseRoute: undefined }, navigate: jest.fn() };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                PopoverModule.forRoot()
            ],
            providers: [
                { provide: Router, useValue: router }
            ],
            declarations: [DatasetCardDocComponent],

        }).compileComponents();
        fixture = TestBed.createComponent(DatasetCardDocComponent);
        component = fixture.componentInstance;
        component.dataset = { ...DATASET };
        component.instanceSelected = 'myInstance';
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });

    it('isDatasetAccessible() should return false', () => {
        component.authenticationEnabled = true;
        component.dataset.public = false;
        //component.isAdmin = jest.fn().mockImplementation(() => false);
        const addStub = jest.spyOn(fromSharedUtils, 'isAdmin').mockReturnValueOnce(false);
        expect(component.isDatasetAccessible()).toBe(false);
        addStub.mockRestore();
    });

    it('isDatasetAccessible() should return false', () => {
        component.authenticationEnabled = true;
        component.dataset.public = false;
        //component.isAdmin = jest.fn().mockImplementation(() => false);
        const addStub = jest.spyOn(fromSharedUtils, 'isAdmin').mockReturnValueOnce(false);
        component.isAuthenticated = true;
        component.userRoles = ['test'];
        expect(component.isDatasetAccessible()).toBe(false);
        addStub.mockRestore();
    });

    it('isAdmin should return call isAdmin from shared/ utils and return true', () => {
        let spy = jest.spyOn(fromSharedUtils, 'isAdmin');
        spy.mockImplementation(() => true);
        expect(component.isAdmin()).toEqual(true);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('selectDataset() should call router.navigate()', async () => {
        let spy = jest.spyOn(router, 'navigate');
        await component.selectDataset('test');
        expect(spy).toHaveBeenCalled();
    });
});
