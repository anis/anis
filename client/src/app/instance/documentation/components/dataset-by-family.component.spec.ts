/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, waitForAsync, ComponentFixture  } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { AccordionModule } from 'ngx-bootstrap/accordion';

import { DatasetByFamilyComponent } from './dataset-by-family.component';
import { sharedPipes } from 'src/app/shared/pipes'
import { Dataset } from 'src/app/metamodel/models';
import { DATASET_LIST, DATASET_FAMILY_LIST } from 'src/test-data';

describe('[Instance][Documentation][Component] DatasetByFamilyComponent', () => {
    @Component({ selector: '<app-dataset-card-doc', template: '' })
    class DatasetCardDocStubComponent {
        @Input() dataset: Dataset;
        @Input() instanceSelected: string;
    }

    let component: DatasetByFamilyComponent;
    let fixture: ComponentFixture<DatasetByFamilyComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [AccordionModule.forRoot()],
            declarations: [
                DatasetByFamilyComponent,
                DatasetCardDocStubComponent,
                sharedPipes
            ]

        }).compileComponents();
        fixture = TestBed.createComponent(DatasetByFamilyComponent);
        component = fixture.componentInstance;

        component.datasetList = [ ...DATASET_LIST ];
        component.datasetFamilyList = [ ...DATASET_FAMILY_LIST ];
        component.instanceSelected = 'my-instance';
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });
});
