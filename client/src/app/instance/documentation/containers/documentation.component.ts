/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppConfigService } from 'src/app/app-config.service';
import { Attribute } from 'src/app/metamodel/models';
import * as attributeActions from 'src/app/metamodel/actions/attribute.actions';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as attributeSelector from 'src/app/metamodel/selectors/attribute.selector';

@Component({
    styleUrls: ['../documentation.component.scss'],
    selector: 'app-documentation',
    templateUrl: 'documentation.component.html'
})
export class DocumentationComponent implements OnInit {
    public instanceSelected: Observable<string>;
    public datasetSelected: Observable<string>;
    public attributeListIsLoading: Observable<boolean>;
    public attributeListIsLoaded: Observable<boolean>;
    public attributeList: Observable<Attribute[]>;

    constructor(private store: Store<{ }>, private config: AppConfigService) {
        this.instanceSelected = store.select(instanceSelector.selectInstanceNameByRoute);
        this.datasetSelected = store.select(datasetSelector.selectDatasetNameByRoute);
        this.attributeListIsLoading = store.select(attributeSelector.selectAttributeListIsLoading);
        this.attributeListIsLoaded = store.select(attributeSelector.selectAttributeListIsLoaded);
        this.attributeList = store.select(attributeSelector.selectAllAttributes);
    }

    ngOnInit(): void {
        this.store.dispatch(attributeActions.loadAttributeList());
    }

    /**
     * Returns strict url address.
     *
     * @return string
     */
    getUrlServer(): string {
        if (!this.config.apiUrl.startsWith('http')) {
            const url = window.location;
            return `${url.protocol}//${url.host}${this.config.apiUrl}`;
        }
        return this.config.apiUrl;
    }
}
