/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import * as datasetFamilySelector from 'src/app/metamodel/selectors/dataset-family.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import { Dataset, DatasetFamily } from 'src/app/metamodel/models';
import * as authSelector from 'src/app/user/store/selectors/auth.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { AppConfigService } from 'src/app/app-config.service';

@Component({
    selector: 'app-dataset-list',
    templateUrl: 'dataset-list.component.html',
    styleUrls: ['../documentation.component.scss']
})
export class DatasetListComponent {
    public instanceSelected: Observable<string>;
    public isAuthenticated: Observable<boolean>;
    public userRoles: Observable<string[]>;
    public datasetFamilyListIsLoading: Observable<boolean>;
    public datasetFamilyListIsLoaded: Observable<boolean>;
    public datasetFamilyList: Observable<DatasetFamily[]>;
    public datasetList: Observable<Dataset[]>;

    constructor(private store: Store<{ }>, private config: AppConfigService) {
        this.instanceSelected = store.select(instanceSelector.selectInstanceNameByRoute);
        this.isAuthenticated = store.select(authSelector.selectIsAuthenticated);
        this.userRoles = store.select(authSelector.selectUserRoles);
        this.datasetFamilyListIsLoading = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoading);
        this.datasetFamilyListIsLoaded = store.select(datasetFamilySelector.selectDatasetFamilyListIsLoaded);
        this.datasetFamilyList = store.select(datasetFamilySelector.selectAllDatasetFamilies);
        this.datasetList = store.select(datasetSelector.selectAllDatasets);
    }

    /**
     * Checks if authentication is enabled.
     *
     * @return boolean
     */
    getAuthenticationEnabled(): boolean {
        return this.config.authenticationEnabled;
    }

    /**
     * Returns admin roles list
     * 
     * @returns string[]
     */
    getAdminRoles(): string[] {
        return this.config.adminRoles;
    }
}
