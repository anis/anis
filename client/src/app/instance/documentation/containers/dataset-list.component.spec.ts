/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { TestBed, waitForAsync, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { DatasetListComponent } from './dataset-list.component';
import { AppConfigService } from 'src/app/app-config.service';
import { Dataset, DatasetFamily } from 'src/app/metamodel/models';

describe('[Instance][Documentation][Container] DatasetListComponent', () => {
    @Component({ selector: 'app-spinner', template: '' })
    class SpinnerStubComponent { }

    @Component({ selector: '<app-dataset-by-family', template: '' })
    class DatasetByFamilyStubComponent {
        @Input() datasetList: Dataset[];
        @Input() datasetFamilyList: DatasetFamily[];
        @Input() instanceSelected: string;
    }

    let component: DatasetListComponent;
    let fixture: ComponentFixture<DatasetListComponent>;
    let appConfigServiceStub = new AppConfigService();
    appConfigServiceStub.authenticationEnabled = true;
    appConfigServiceStub.adminRoles = ['test']

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                DatasetListComponent,
                SpinnerStubComponent,
                DatasetByFamilyStubComponent,
            ],
            providers: [
                provideMockStore({}),
                { provide: AppConfigService, useValue: appConfigServiceStub },
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(DatasetListComponent);
        component = fixture.componentInstance;
        TestBed.inject(MockStore);
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });

    it('getAuthenticationEnabled() should return true', () => {
        expect(component.getAuthenticationEnabled()).toBe(true);
    });

    it('getAdminRoles() should return an array with one element', () => {
        expect(component.getAdminRoles().length).toBe(1);
    });
});
