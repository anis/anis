/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatatableRendererModule } from './datatable-renderer.module';

describe('[Instance][Datatable-renderer] DatatableRendererModule', () => {
    it('test DatatableRendererModule', () => {
        expect(DatatableRendererModule.name).toEqual('DatatableRendererModule');
    });
});
