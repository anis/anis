/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router'

import { SharedModule } from 'src/app/shared/shared.module';
import { rendererComponents } from './components';
import { DisplayResultRendererComponent } from './display-result-renderer.component';
import { SharedRendererModule } from '../shared-renderer/shared-renderer.module';

@NgModule({
    declarations: [
        rendererComponents,
        DisplayResultRendererComponent
    ],
    imports: [
        SharedModule,
        RouterModule,
        SharedRendererModule
    ],
    exports: [
        rendererComponents,
        DisplayResultRendererComponent
    ]
})
export class DatatableRendererModule { }
