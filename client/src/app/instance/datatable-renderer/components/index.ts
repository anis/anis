/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Type } from '@angular/core';

import { DetailLinkRendererComponent } from './detail-link-renderer.component';
import { LinkSearchRendererComponent } from './link-search-renderer.component';
import { getRendererComponent } from 'src/app/instance/shared-renderer/components';
import { AbstractRendererComponent } from 'src/app/instance/shared-renderer/abstract-renderer.component';

export const rendererComponents = [
    DetailLinkRendererComponent,
    LinkSearchRendererComponent
];

export const getResultRendererComponent = (renderer: string): Type<AbstractRendererComponent> => {
    let nameOfRendererComponent = getRendererComponent(renderer);
    switch(renderer) {
        case 'detail-link': {
            nameOfRendererComponent = DetailLinkRendererComponent;
            break;
        }
        case 'link-search': {
            nameOfRendererComponent = LinkSearchRendererComponent;
            break;
        }
    }
    return nameOfRendererComponent;
}
