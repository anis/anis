/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy } from '@angular/core';

import { AbstractRendererComponent } from 'src/app/instance/shared-renderer/abstract-renderer.component';
import { LinkSearchRendererConfig } from 'src/app/metamodel/models/renderers/link-search-renderer-config.model';

@Component({
    selector: 'app-link-search-renderer',
    templateUrl: 'link-search-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LinkSearchRendererComponent extends AbstractRendererComponent {
    override getConfig() {
        return this.attribute.renderer_config as LinkSearchRendererConfig;
    }

    getLinkQueryParams() {
        const criteria = this.getConfig().criteria
            .replace('$value', this.value.toString())
            .replace('$ws_value', this.value.toString().replace(/\s+/g, ''));

        return {
            s: '111',
            a: this.getConfig().attributes,
            c: criteria
        };
    }

    /**
     * Returns config text.
     *
     * @return string
     */
    getText(): string {
        return this.getConfig().text.replace('$value', this.value.toString());
    }
}
