/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy } from '@angular/core';

import { AbstractRendererComponent } from 'src/app/instance/shared-renderer/abstract-renderer.component';
import { DetailLinkRendererConfig } from 'src/app/metamodel/models/renderers/detail-link-renderer-config.model';

@Component({
    selector: 'app-detail-link-renderer',
    templateUrl: 'detail-link-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailLinkRendererComponent extends AbstractRendererComponent {
    override getConfig() {
        return this.attribute.renderer_config as DetailLinkRendererConfig;
    }
}
