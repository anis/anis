/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkSearchRendererComponent } from './link-search-renderer.component';
import { LinkSearchRendererConfig } from 'src/app/metamodel/models';
import { ATTRIBUTE } from 'src/test-data';

describe('[Instance][Search][Component][Result][Renderer] LinkSearchRendererComponent', () => {
    let component: LinkSearchRendererComponent;
    let fixture: ComponentFixture<LinkSearchRendererComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [LinkSearchRendererComponent]
        });
        fixture = TestBed.createComponent(LinkSearchRendererComponent);
        component = fixture.componentInstance;
        component.rendererType = 'result';
        component.attribute = {
            ...ATTRIBUTE,
            renderer_config: {
                dataset: 'iris_obspack',
                attributes: '1;2;3',
                criteria: '1::eq::$value',
                display: 'text',
                text: '$value',
                icon: 'fas fa-link'
            } as LinkSearchRendererConfig
        };
        component.value = '86';
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getValue() should return link url', () => {
        expect(component.getLinkQueryParams()).toEqual({
            s: '111',
            a: '1;2;3',
            c: '1::eq::86'
        });
    });

    it('#getText() should return link text', () => {
        expect(component.getText()).toEqual('86');
    });
});
