/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy } from '@angular/core';

import { BadgeRendererConfig } from 'src/app/metamodel/models/renderers/badge-renderer-config.model';
import { AbstractRendererComponent } from '../abstract-renderer.component';

@Component({
    selector: 'app-badge-renderer',
    templateUrl: 'badge-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BadgeRendererComponent extends AbstractRendererComponent {
    override getConfig() {
        return super.getConfig() as BadgeRendererConfig;
    }
}
