/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { getHost } from 'src/app/shared/utils';
import { ImageRendererConfig } from 'src/app/metamodel/models/renderers';
import { AppConfigService } from 'src/app/app-config.service';
import { AbstractRendererComponent } from '../abstract-renderer.component';

@Component({
    selector: 'app-image-renderer',
    templateUrl: 'image-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageRendererComponent extends AbstractRendererComponent {
    modalRef: BsModalRef;
    loading = true;
    error = false;

    constructor(private modalService: BsModalService, private appConfig: AppConfigService) {
        super();
    }

    onLoad() {
        this.loading = false;
    }
    
    onError(event) {
        if ((this.instance.public && this.dataset.public) || ((!this.instance.public || !this.dataset.public) && event.target.attributes['src'].value === 'not_found')) {
            this.loading = false;
            this.error = true;
        }
    }

    override getConfig() {
        return super.getConfig() as ImageRendererConfig;
    }

    /**
     * Opens modal.
     *
     * @param  {TemplateRef<any>} template - The modal template to open.
     */
    openModal(template: TemplateRef<any>): void {
        this.modalRef = this.modalService.show(template);
    }

    /**
     * Returns source image.
     *
     * @return string
     */
    getValue(): string {
        if (this.getConfig().type === 'fits') {
            let url = `${this.appConfig.servicesUrl}/fits/fits-to-png/${this.instance.name}/${this.dataset.name}?filename=${this.getPath()}`
                + `&stretch=linear&pmin=0.25&pmax=99.75&axes=true`;
            if (this.getConfig().hdu_number) {
                url += `&hdu_number=${this.getConfig().hdu_number}`;
            }
            return url;
        } else if (this.getConfig().type === 'image') {
            return `${getHost(this.appConfig.apiUrl)}/instance/${this.instance.name}/dataset/${this.dataset.name}/file-explorer${this.getPath()}`;
        } else {
            return this.value as string;
        }
    }

    getPath() {
        let path = this.value;
        if (path[0] !== '/') {
            path = '/' + path;
        }
        return path;
    }

    getNgStyle() {
        let style = 'btn btn-outline-primary btn-sm';
        if (this.getConfig().display === 'modal-block') {
            style += ' btn-block';
        }
        return style;
    }
}
