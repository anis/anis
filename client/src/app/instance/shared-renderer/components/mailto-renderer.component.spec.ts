/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MailtoRendererComponent } from './mailto-renderer.component';
import { ATTRIBUTE } from 'src/test-data';

describe('[Instance][Search][Component][Result][Renderer] MailtoRendererComponent', () => {
    let component: MailtoRendererComponent;
    let fixture: ComponentFixture<MailtoRendererComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [MailtoRendererComponent]
        });
        fixture = TestBed.createComponent(MailtoRendererComponent);
        component = fixture.componentInstance;
        component.rendererType = 'result';
        component.attribute = {
            ...ATTRIBUTE
        };
        component.value = 'M81';
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
