/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BadgeRendererComponent } from './badge-renderer.component';
import { BadgeRendererConfig } from 'src/app/metamodel/models';
import { ATTRIBUTE } from 'src/test-data';

describe('[Instance][Search][Component][Result][Renderer] BadgeRendererComponent', () => {
    let component: BadgeRendererComponent;
    let fixture: ComponentFixture<BadgeRendererComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [BadgeRendererComponent],
        });
        fixture = TestBed.createComponent(BadgeRendererComponent);
        component = fixture.componentInstance;
        component.rendererType = 'result';
        component.attribute = {
            ...ATTRIBUTE,
            renderer_config: {
                pill: true,
                color: 'primary',
            } as BadgeRendererConfig,
        };
        component.value = '4.569835827';
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
