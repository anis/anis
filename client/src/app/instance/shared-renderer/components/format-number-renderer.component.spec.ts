/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormatNumberRendererComponent } from './format-number-renderer.component';
import { FormatNumberRendererConfig } from 'src/app/metamodel/models';
import { ATTRIBUTE } from 'src/test-data';

describe('[Instance][Search][Component][Result][Renderer] FormatNumberRendererComponent', () => {
    let component: FormatNumberRendererComponent;
    let fixture: ComponentFixture<FormatNumberRendererComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [FormatNumberRendererComponent],
        });
        fixture = TestBed.createComponent(FormatNumberRendererComponent);
        component = fixture.componentInstance;
        component.rendererType = 'result';
        component.attribute = {
            ...ATTRIBUTE,
            renderer_config: {
                format: 2,
                exponential: false,
            } as FormatNumberRendererConfig,
        };
        component.value = '4.569835827';
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getValue() should return the value properly formatted if no exponential', () => {
        expect(component.getValue()).toEqual('4.57');
    });

    it('#getValue() should return the value properly formatted if exponential', () => {
        component.attribute = {
            ...ATTRIBUTE,
            renderer_config: {
                format: 2,
                exponential: true,
            } as FormatNumberRendererConfig,
        };
        expect(component.getValue()).toEqual('4.57e+0');
    });

    it('#getValue() should return ERROR if value unparseable', () => {
        component.value = 'coucou';
        const spyConsole = jest
            .spyOn(console, 'error')
            .mockImplementation(jest.fn());

        expect(component.getValue()).toEqual('ERROR');
        expect(spyConsole).toHaveBeenCalledTimes(1);
    });
});
