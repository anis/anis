/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy } from '@angular/core';

import { FormatNumberRendererConfig } from 'src/app/metamodel/models/renderers/format-number-renderer-config.model';
import { AbstractRendererComponent } from '../abstract-renderer.component';

@Component({
    selector: 'app-format-number-renderer',
    templateUrl: 'format-number-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormatNumberRendererComponent extends AbstractRendererComponent {
    override getConfig() {
        return super.getConfig() as FormatNumberRendererConfig;
    }

    /**
     * Returns value properly formatted
     *
     * @return string
     */
    getValue(): string {
        const value = parseFloat(this.value as string);

        if (isNaN(value)) {
            console.error(
                `(FormatNumberRendererComponent): could not parse, hence format ${this.value} properly`,
            );
            return 'ERROR';
        }

        if (this.getConfig().exponential) {
            return value.toExponential(this.getConfig().format);
        }

        return value.toFixed(this.getConfig().format);
    }
}
