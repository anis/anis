/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { DownloadRendererComponent } from './download-renderer.component';
import { AppConfigService } from 'src/app/app-config.service';
import { Dataset, DownloadRendererConfig, Instance } from 'src/app/metamodel/models';
import { INSTANCE, DATASET, ATTRIBUTE } from 'src/test-data';

describe('[Instance][Search][Component][Result][Renderer] DownloadRendererComponent', () => {
    let component: DownloadRendererComponent;
    let fixture: ComponentFixture<DownloadRendererComponent>;
    let appConfigServiceStub = new AppConfigService();
    let instance: Instance;
    let dataset: Dataset;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DownloadRendererComponent],
            imports: [HttpClientTestingModule],
            providers: [{ provide: AppConfigService, useValue: appConfigServiceStub }]
        });
        fixture = TestBed.createComponent(DownloadRendererComponent);
        component = fixture.componentInstance;
        instance = { ...INSTANCE };
        dataset = { ...DATASET };
        appConfigServiceStub.apiUrl = 'http://localhost:8080';
        component.rendererType = 'result';
        component.instance = instance;
        component.dataset = dataset;
        component.attribute = {
            ...ATTRIBUTE,
            renderer_config: {
                display: 'icon-button',
                text: 'DOWNLOAD',
                icon: 'fas fa-download'
            } as DownloadRendererConfig
        };
        component.value = '/files/1234.fits';
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getHref() should return file url', () => {
        expect(component.getHref()).toBe(`http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/file-explorer/files/1234.fits`);
    });

    it('#getText() should return link text', () => {
        expect(component.getText()).toEqual('DOWNLOAD');
    });
});
