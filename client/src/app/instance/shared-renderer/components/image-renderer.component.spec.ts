/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalModule } from 'ngx-bootstrap/modal';

import { ImageRendererComponent } from './image-renderer.component';
import { AppConfigService } from 'src/app/app-config.service';
import { Dataset, ImageRendererConfig, Instance } from 'src/app/metamodel/models';
import { INSTANCE, DATASET, ATTRIBUTE } from 'src/test-data';

describe('[Instance][Search][Component][Result][Renderer] ImageRendererComponent', () => {
    let component: ImageRendererComponent;
    let fixture: ComponentFixture<ImageRendererComponent>;
    let appConfigServiceStub = new AppConfigService();
    let instance: Instance;
    let dataset: Dataset;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ImageRendererComponent],
            imports: [ModalModule.forRoot()],
            providers: [{ provide: AppConfigService, useValue: appConfigServiceStub }]
        });
        fixture = TestBed.createComponent(ImageRendererComponent);
        instance = { ...INSTANCE };
        dataset = { ...DATASET };
        appConfigServiceStub.servicesUrl = 'http://localhost:5000';
        appConfigServiceStub.apiUrl = 'http://localhost:8080';
        component = fixture.componentInstance;
        component.rendererType = 'result';
        component.attribute = {
            ...ATTRIBUTE,
            renderer_config: {
                type: 'fits',
                display: 'modal',
                width: '50',
                height: '50'
            } as ImageRendererConfig
        };
        component.instance = instance;
        component.dataset = dataset;
        component.value = '/files/1234.fits';
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getValue() should return image url', () => {
        let expectedValue = `http://localhost:5000/fits/fits-to-png/${instance.name}/${dataset.name}?filename=/files/1234.fits&stretch=linear&pmin=0.25&pmax=99.75&axes=true`;
        expect(component.getValue()).toEqual(expectedValue);
        (component.attribute.renderer_config as ImageRendererConfig).type = 'image';
        component.value = '/files/1234.png';
        expectedValue = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/file-explorer/files/1234.png`;
        expect(component.getValue()).toEqual(expectedValue);
    });
});
