/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LinkRendererComponent } from './link-renderer.component';
import { LinkRendererConfig } from 'src/app/metamodel/models';
import { ATTRIBUTE } from 'src/test-data';

describe('[Instance][Search][Component][Result][Renderer] LinkRendererComponent', () => {
    let component: LinkRendererComponent;
    let fixture: ComponentFixture<LinkRendererComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [LinkRendererComponent]
        });
        fixture = TestBed.createComponent(LinkRendererComponent);
        component = fixture.componentInstance;
        component.rendererType = 'result';
        component.attribute = {
            ...ATTRIBUTE,
            renderer_config: {
                href: 'http://cdsportal.u-strasbg.fr/?target=$value',
                display: 'text',
                text: '$value',
                icon: 'fas fa-link',
                blank: true
            } as LinkRendererConfig
        };
        component.value = 'M81';
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getValue() should return link url', () => {
        expect(component.getValue()).toEqual('http://cdsportal.u-strasbg.fr/?target=M81');
    });

    it('#getText() should return link text', () => {
        expect(component.getText()).toEqual('M81');
    });
});
