/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy } from '@angular/core';

import { MailtoRendererConfig } from 'src/app/metamodel/models/renderers/mailto-renderer-config.model';
import { AbstractRendererComponent } from '../abstract-renderer.component';

@Component({
    selector: 'app-mailto-renderer',
    templateUrl: 'mailto-renderer.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MailtoRendererComponent extends AbstractRendererComponent {
    override getConfig() {
        return super.getConfig() as MailtoRendererConfig;
    }

    getHref() {
        let href = `mailto:${this.value.toString()}`;
        if (this.getConfig().subject) {
            href += `?subject=${this.getConfig().subject}`;
        }
        return href;
    }
}
