/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router'

import { SharedModule } from 'src/app/shared/shared.module';
import { sharedRendererComponents } from './components';
import { RendererLoaderDirective } from './renderer-loader.directive';

@NgModule({
    declarations: [
        sharedRendererComponents,
        RendererLoaderDirective
    ],
    imports: [
        SharedModule,
        RouterModule
    ],
    exports: [
        sharedRendererComponents,
        RendererLoaderDirective
    ]
})
export class SharedRendererModule { }
