/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Directive, Input } from '@angular/core';

import { Attribute, Dataset, Instance, RendererConfig } from 'src/app/metamodel/models';
import { SearchQueryParams } from 'src/app/instance/store/models';

@Directive()
export abstract class AbstractRendererComponent {
    @Input() rendererType: string;
    @Input() value: string | number;
    @Input() dataset: Dataset;
    @Input() instance: Instance;
    @Input() attribute: Attribute;
    @Input() queryParams: SearchQueryParams;

    getConfig(): RendererConfig {
        if (this.rendererType === 'result') {
            return this.attribute.renderer_config;
        } else {
            return this.attribute.detail_renderer_config;
        }
    }
}
