/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { APP_INITIALIZER, isDevMode } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { KeycloakService } from 'keycloak-angular';
import { Store } from '@ngrx/store';
import { firstValueFrom } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatomoInitializerService } from '@ngx-matomo/tracker';

import { AppConfigService } from './app-config.service';
import { initializeKeycloak } from 'src/app/user/init.keycloak';

function appInit(http: HttpClient, appConfigService: AppConfigService, keycloak: KeycloakService, store: Store<any>, matomo: MatomoInitializerService) {
    return () => {
        function getApiUrl() {
            if (!isDevMode()) {
                return 'server';
            } else {
                return 'http://localhost:8080';
            }
        }

        const apiUrl = getApiUrl();
        const source$ = http.get<{statusCode: number, data: any}>(getClientSettingsUrl(apiUrl)).pipe(map(response => response.data));

        return firstValueFrom(source$)
            .then(data => {
                Object.assign(appConfigService, data);
                appConfigService.apiUrl = apiUrl;
                appConfigService.adminRoles = data['adminRoles'].split(',');

                if (appConfigService.matomoEnabled) {
                    matomo.initializeTracker({
                        siteId: appConfigService.matomoSiteId,
                        trackerUrl: appConfigService.matomoTrackerUrl
                    });
                }

                return initializeKeycloak(keycloak, store, appConfigService)
            });
    }
}

function getClientSettingsUrl(apiUrl: string): string {
    let url: string;
    if (!isDevMode()) {
        url = `${document.getElementsByTagName('base')[0].getAttribute('href')}${apiUrl}/client-settings`;
    } else {
        url = `${apiUrl}/client-settings`;
    }
    return url;
}

export const appInitializer = {
    provide: APP_INITIALIZER,
    useFactory: appInit,
    multi: true,
    deps: [ HttpClient, AppConfigService, KeycloakService, Store, MatomoInitializerService ]
};
