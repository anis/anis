/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold } from 'jasmine-marbles';
import { Router } from '@angular/router';

jest.mock('@angular/router');

import { AppConfigService } from '../app-config.service';
import { AdminAuthGuard } from './admin-auth.guard';
import * as authSelector from 'src/app/user/store/selectors/auth.selector';

describe('[admin] AdminAuthGuard', () => {
    let store: MockStore;
    const route = {
        navigateByUrl: jest.fn(),
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                provideMockStore({}),
                { provide: Router, useValue: route },
            ],
        });
    });

    it('should create component', () => {
        TestBed.overrideProvider(AppConfigService, {
            useValue: { authenticationEnabled: true },
        });
        store = TestBed.inject(MockStore);
        store.overrideSelector(authSelector.selectIsAuthenticated, true);
        store.overrideSelector(authSelector.selectUserRoles, ['test']);
        let adminAuthGuard = TestBed.inject(AdminAuthGuard);
        expect(adminAuthGuard).toBeTruthy();
    });

    it('canActivate() should return true when authentifiacation is not enabled', () => {
        TestBed.overrideProvider(AppConfigService, {
            useValue: { authenticationEnabled: false },
        });
        store = TestBed.inject(MockStore);
        store.overrideSelector(authSelector.selectIsAuthenticated, true);
        store.overrideSelector(authSelector.selectUserRoles, ['test']);
        let adminAuthGuard = TestBed.inject(AdminAuthGuard);
        let result = adminAuthGuard.canActivate();
        let expected = cold('a', { a: true });
        expect(result).toBeObservable(expected);
    });

    it('canActivate() should return false when authentifiacationEnabled is true and user is not Admin ', () => {
        TestBed.overrideProvider(AppConfigService, {
            useValue: { authenticationEnabled: true, adminRoles: [] },
        });
        store = TestBed.inject(MockStore);
        store.overrideSelector(authSelector.selectIsAuthenticated, true);
        store.overrideSelector(authSelector.selectUserRoles, ['test']);
        let adminAuthGuard = TestBed.inject(AdminAuthGuard);
        let result = adminAuthGuard.canActivate();
        let expected = cold('a', { a: false });
        expect(result).toBeObservable(expected);
    });

    it('canActivate() should return false when authentifiacationEnabled is true and user is  Admin ', () => {
        TestBed.overrideProvider(AppConfigService, {
            useValue: { authenticationEnabled: true, adminRoles: ['test'] },
        });
        store = TestBed.inject(MockStore);
        store.overrideSelector(authSelector.selectIsAuthenticated, true);
        store.overrideSelector(authSelector.selectUserRoles, ['test']);
        let adminAuthGuard = TestBed.inject(AdminAuthGuard);
        let result = adminAuthGuard.canActivate();
        let expected = cold('a', { a: true });
        expect(result).toBeObservable(expected);
    });
});
