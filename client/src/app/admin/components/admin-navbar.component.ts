/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

import { UserProfile } from 'src/app/user/store/models/user-profile.model';
import { AppConfigService } from 'src/app/app-config.service';

@Component({
    selector: 'app-admin-navbar',
    templateUrl: 'admin-navbar.component.html',
    styleUrls: [ 'admin-navbar.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminNavbarComponent {
    @Input() isAuthenticated: boolean;
    @Input() userProfile: UserProfile = null;
    @Output() login: EventEmitter<any> = new EventEmitter();
    @Output() logout: EventEmitter<any> = new EventEmitter();
    @Output() openEditProfile: EventEmitter<any> = new EventEmitter();

    constructor(private config: AppConfigService) { }

    /**
     * Checks if authentication is enabled.
     *
     * @return boolean
     */
    getAuthenticationEnabled(): boolean {
        return this.config.authenticationEnabled;
    }
}
