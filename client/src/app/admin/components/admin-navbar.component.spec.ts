/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AdminNavbarComponent } from './admin-navbar.component';
import { AppConfigService } from 'src/app/app-config.service';

describe('[admin][components] AdminNavbarComponent', () => {
    let component: AdminNavbarComponent;
    let fixture: ComponentFixture<AdminNavbarComponent>;
    let appConfigServiceStub = new AppConfigService();

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                AdminNavbarComponent
            ],
            imports: [
                BrowserAnimationsModule,
            ],
            providers: [
                { provide: AppConfigService, useValue: appConfigServiceStub },
            ]
        });
        fixture = TestBed.createComponent(AdminNavbarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
