/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { Store, select } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import * as authSelector from 'src/app/user/store/selectors/auth.selector';
import { AppConfigService } from 'src/app/app-config.service';

@Injectable({
    providedIn: 'root',
})
export class AdminAuthGuard implements CanActivate {
    constructor(
        protected readonly router: Router,
        private store: Store<{ }>,
        private config: AppConfigService
    ) { }

    canActivate(): Observable<boolean> {
        return combineLatest([
            this.store.pipe(select(authSelector.selectUserRoles)),
            this.store.pipe(select(authSelector.selectIsAuthenticated))
        ]).pipe(
            map(([userRoles, isAuthenticated]) => {
                // Auth disabled
                if (!this.config.authenticationEnabled) {
                    return true;
                }

                // If user is authenticated and authorized so admin changes to true
                let admin = false;
                if (isAuthenticated) {
                    for (let i = 0; i < this.config.adminRoles.length; i++) {
                        admin = userRoles.includes(this.config.adminRoles[i]);
                        if (admin) break;
                    }
                }

                // If user is not authorized to continue go to unauthorized page
                if (!admin) {
                    sessionStorage.setItem('redirect_uri', window.location.toString());
                    this.router.navigateByUrl('/unauthorized');
                    return false;
                }

                // Let "Router" allow user entering the page
                return true;
            })
        );
    }
}
