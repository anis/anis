/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from 'src/app/shared/shared.module';
import { AdminSharedModule } from './admin-shared/admin-shared.module';
import { AdminRoutingModule, routedComponents } from './admin-routing.module';
import { dummiesComponents } from './components';
import { adminReducer } from './admin.reducer';
import { adminEffects } from './store/effects';
import { adminServices } from './store/services';

@NgModule({
    imports: [
        SharedModule,
        AdminSharedModule,
        AdminRoutingModule,
        StoreModule.forFeature('admin', adminReducer),
        EffectsModule.forFeature(adminEffects)
    ],
    declarations: [
        routedComponents,
        dummiesComponents
    ],
    providers: [
        adminServices
    ]
})
export class AdminModule { }
