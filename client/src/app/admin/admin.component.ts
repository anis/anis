/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { UserProfile } from 'src/app/user/store/models/user-profile.model';
import * as authActions from 'src/app/user/store/actions/auth.actions';
import * as authSelector from 'src/app/user/store/selectors/auth.selector';
import { StyleService } from 'src/app/shared/services/style.service';

@Component({
    selector: 'app-admin',
    templateUrl: 'admin.component.html'
})
export class AdminComponent implements OnInit {
    public favIcon: HTMLLinkElement = document.querySelector('#favicon');
    public body: HTMLBodyElement = document.querySelector('body');
    public isAuthenticated: Observable<boolean>;
    public userProfile: Observable<UserProfile>;

    constructor(private store: Store<{ }>, private style: StyleService) {
        this.isAuthenticated = store.select(authSelector.selectIsAuthenticated);
        this.userProfile = store.select(authSelector.selectUserProfile);
    }

    ngOnInit() {
        this.favIcon.href = 'favicon.ico';
        this.body.style.backgroundColor = 'white';
        this.style.setStyles('.footer', {
            'background-color': '#F8F9FA',
            'border-top': 'none',
            'color': 'black',
        });
    }

    login(): void {
        this.store.dispatch(authActions.login({ redirectUri: window.location.toString() }));
    }

    logout(): void {
        this.store.dispatch(authActions.logout());
    }

    openEditProfile(): void {
        this.store.dispatch(authActions.openEditProfile());
    }
}
