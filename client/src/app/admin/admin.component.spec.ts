/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { AppConfigService } from '../app-config.service';
import { StyleService } from '../shared/services/style.service';
import { AdminComponent } from './admin.component';
import * as authActions from 'src/app/user/store/actions/auth.actions';
import { UserProfile } from '../user/store/models/user-profile.model';

describe('[admin] AdminComponent', () => {
    @Component({ selector: 'app-admin-navbar', template: '' })
    class AdminNavbarStubComponent {
        @Input() isAuthenticated: boolean;
        @Input() userProfile: UserProfile = null;
    }

    @Component({ selector: 'app-footer', template: '' })
    class FooterStubComponent { }
    
    let component: AdminComponent;
    let fixture: ComponentFixture<AdminComponent>;
    let store: MockStore;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                AdminComponent,
                AdminNavbarStubComponent,
                FooterStubComponent
            ],
            providers: [
                provideMockStore({}),
                { provide: AppConfigService, useValue: { authenticationEnabled: false } },
                { provide: StyleService, useValue: { setStyles: jest.fn() } }
            ],
            imports: [
                BrowserAnimationsModule,
                RouterTestingModule
            ]
        });

        fixture = TestBed.createComponent(AdminComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        let favIcon: HTMLLinkElement;
        let body: HTMLBodyElement;
        let style: CSSStyleDeclaration;
        component.favIcon = { ...favIcon, href: '' }
        component.body = { ...body, style: { ...style, backgroundColor: '' } }
        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('login() should dispatch login action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.login();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(authActions.login({ redirectUri: window.location.toString() }));
    });

    it('logout() should dispatch logout action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.logout();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(authActions.logout());
    });
    
    it('openEditProfile() should dispatch logout action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.openEditProfile();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(authActions.openEditProfile());
    });
});
