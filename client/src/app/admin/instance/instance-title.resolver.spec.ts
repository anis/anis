/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jasmine-marbles';
import { Instance } from 'src/app/metamodel/models';
import { InstanceTitleResolver } from './instance-title.resolver';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as instanceActions from 'src/app/metamodel/actions/instance.actions';

describe('[admin][instance] InstanceTitleResolver', () => {
    let instanceTitleResolver: InstanceTitleResolver;
    let store: MockStore;
    let mockInstanceSelectorInstance;
    let mockInstanceSelectorInstanceListIsLoaded;
    let instance: Instance;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                InstanceTitleResolver,
                provideMockStore({}),
            ]
        })
        store = TestBed.inject(MockStore);
        instanceTitleResolver = TestBed.inject(InstanceTitleResolver);
        instance = { ...instance, label: 'instance_label' };
        mockInstanceSelectorInstance = store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
    });

    it('should be created', () => {
        expect(instanceTitleResolver).toBeTruthy();
    });

    it('should return "Edit database test"', () => {
        mockInstanceSelectorInstanceListIsLoaded = store.overrideSelector(instanceSelector.selectInstanceListIsLoaded, false);
        let spy = jest.spyOn(store, 'dispatch');
        let result = hot('a', { a: instanceTitleResolver.resolve(null, null) });
        const expected = cold('a', { a: [] });
        expect(result).toBeObservable(expected);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(instanceActions.loadInstanceList())
    });
    
    it('should return "Edit database test"', () => {
        mockInstanceSelectorInstanceListIsLoaded = store.overrideSelector(instanceSelector.selectInstanceListIsLoaded, true);

        let result = instanceTitleResolver.resolve(null, null);
        const expected = cold('a', { a: 'Edit instance instance_label' });
        expect(result).toBeObservable(expected);
    });
});
