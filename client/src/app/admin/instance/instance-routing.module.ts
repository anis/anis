/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InstanceListComponent } from './containers/instance-list.component';
import { NewInstanceComponent } from './containers/new-instance.component';
import { EditInstanceComponent } from './containers/edit-instance.component';
import { EditDesignConfigComponent } from './containers/edit-design-config.component';
import { ConfigureInstanceComponent } from './containers/configure-instance.component';
import { InstanceTitleResolver } from './instance-title.resolver';

const routes: Routes = [
    { path: 'instance-list', component: InstanceListComponent, title: 'Instances list' },
    { path: 'new-instance', component: NewInstanceComponent, title: 'New instance' },
    { path: 'edit-instance/:iname', component: EditInstanceComponent, title: InstanceTitleResolver },
    { path: 'edit-design-config/:iname', component: EditDesignConfigComponent, title: InstanceTitleResolver },
    { path: 'configure-instance/:iname', component: ConfigureInstanceComponent, children: 
        [
            { path: '', redirectTo: 'dataset/dataset-list', pathMatch: 'full' },
            { path: 'dataset', loadChildren: () => import('./dataset/dataset.module').then(m => m.DatasetModule) },
            { path: 'database', loadChildren: () => import('./database/database.module').then(m => m.DatabaseModule) },
            { path: 'menu', loadChildren: () => import('./menu/menu.module').then(m => m.MenuModule) },
            { path: 'instance-group', loadChildren: () => import('./instance-group/instance-group.module').then(m => m.InstanceGroupModule) }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class InstanceRoutingModule { }

export const routedComponents = [
    InstanceListComponent,
    NewInstanceComponent,
    EditInstanceComponent,
    EditDesignConfigComponent,
    ConfigureInstanceComponent
];
