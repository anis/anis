/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { EditInstanceComponent } from './edit-instance.component';
import { FileInfo } from '../../store/models';
import { Instance } from 'src/app/metamodel/models';
import * as instanceActions from 'src/app/metamodel/actions/instance.actions';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as adminFileExplorerActions from 'src/app/admin/store/actions/admin-file-explorer.actions';
import { INSTANCE } from 'src/test-data';

describe('[admin][instance][containers] EditInstanceComponent', () => {
    @Component({ selector: 'app-instance-form', template: ''})
    class InstanceFormStubComponent {
        @Input() instance: Instance;
        @Input() files: FileInfo[];
        @Input() filesIsLoading: boolean;
        @Input() filesIsLoaded: boolean;
        form = new UntypedFormGroup({});
    }

    let component: EditInstanceComponent;
    let fixture: ComponentFixture<EditInstanceComponent>;
    let store: MockStore;
    let mockInstanceSelectorInstance;
    let instance: Instance;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                EditInstanceComponent,
                InstanceFormStubComponent
            ],
            imports: [
                BrowserAnimationsModule,
            ],
            providers: [
                provideMockStore({})
            ]
        });
        fixture = TestBed.createComponent(EditInstanceComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        instance = { ...INSTANCE };
        mockInstanceSelectorInstance = store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('editInstance(instance: Instance)  should dispatch editInstance action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let editedInstance = { ...instance, 'description': 'Instance edited' };
        component.editInstance(editedInstance);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(instanceActions.editInstance({ instance: editedInstance }));
    });

    it('loadRootDirectory(path: string) should dispatch loadFiles action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.loadRootDirectory('test');
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(adminFileExplorerActions.loadFiles({ path: 'test' }));
    });
});
