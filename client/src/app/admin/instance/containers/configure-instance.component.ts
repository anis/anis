/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import * as datasetFamilyActions from 'src/app/metamodel/actions/dataset-family.actions';
import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';
import * as databaseActions from 'src/app/metamodel/actions/database.actions';
import * as menuItemsActions from 'src/app/metamodel/actions/menu-item.actions';
import * as instanceGroupActions from 'src/app/metamodel/actions/instance-group.actions';

@Component({
    selector: 'app-configure-instance',
    templateUrl: 'configure-instance.component.html',
})
export class ConfigureInstanceComponent implements OnInit {
    constructor(private store: Store<{ }>) { }

    ngOnInit() {
        Promise.resolve(null).then(() => this.store.dispatch(datasetFamilyActions.loadDatasetFamilyList()));
        Promise.resolve(null).then(() => this.store.dispatch(datasetActions.loadDatasetList()));
        Promise.resolve(null).then(() => this.store.dispatch(databaseActions.loadDatabaseList()));
        Promise.resolve(null).then(() => this.store.dispatch(menuItemsActions.loadMenuItemList()));
        Promise.resolve(null).then(() => this.store.dispatch(instanceGroupActions.loadInstanceGroupList()));
    }
}
