/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { EditDesignConfigComponent } from './edit-design-config.component';
import { FileInfo } from '../../store/models';
import { DesignConfig, Instance } from 'src/app/metamodel/models';
import * as designConfigActions from 'src/app/metamodel/actions/design-config.actions';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as adminFileExplorerActions from 'src/app/admin/store/actions/admin-file-explorer.actions';
import { INSTANCE, DESIGN_CONFIG } from 'src/test-data';

describe('[admin][instance][containers] EditDesignConfigComponent', () => {
    @Component({ selector: 'app-design-config-form', template: ''})
    class DesignConfigFormStubComponent {
        @Input() instance: Instance;
        @Input() designConfig: DesignConfig;
        @Input() files: FileInfo[];
        @Input() filesIsLoading: boolean;
        @Input() filesIsLoaded: boolean;
        form = new UntypedFormGroup({});
    }

    let component: EditDesignConfigComponent;
    let fixture: ComponentFixture<EditDesignConfigComponent>;
    let store: MockStore;
    let mockInstanceSelectorInstance;
    let instance: Instance;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                EditDesignConfigComponent,
                DesignConfigFormStubComponent
            ],
            imports: [
                BrowserAnimationsModule,
            ],
            providers: [
                provideMockStore({})
            ]
        });
        fixture = TestBed.createComponent(EditDesignConfigComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        instance = { ...INSTANCE };
        mockInstanceSelectorInstance = store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('ngOnInit()', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.ngOnInit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(designConfigActions.loadDesignConfig());
    });

    it('editDesignConfig(designConfig: DesignConfig)  should dispatch editDesignConfig action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let editedDesignConfig= { ...DESIGN_CONFIG };
        component.editDesignConfig(editedDesignConfig);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(designConfigActions.editDesignConfig({ designConfig: editedDesignConfig }));
    });

    it('loadRootDirectory(path: string) should dispatch loadFiles action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.loadRootDirectory('test');
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(adminFileExplorerActions.loadFiles({ path: 'test' }));
    });
});
