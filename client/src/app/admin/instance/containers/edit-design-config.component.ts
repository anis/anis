/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Instance, DesignConfig } from 'src/app/metamodel/models';
import { FileInfo } from 'src/app/admin/store/models';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as designConfigActions from 'src/app/metamodel/actions/design-config.actions';
import * as designConfigSelector from 'src/app/metamodel/selectors/design-config.selector';
import * as adminFileExplorerActions from 'src/app/admin/store/actions/admin-file-explorer.actions';
import * as adminFileExplorerSelector from 'src/app/admin/store/selectors/admin-file-explorer.selector';

@Component({
    selector: 'app-edit-design-config',
    templateUrl: 'edit-design-config.component.html'
})
export class EditDesignConfigComponent implements OnInit {
    public instance: Observable<Instance>;
    public designConfig: Observable<DesignConfig>;
    public designConfigIsLoading: Observable<boolean>;
    public designConfigIsLoaded: Observable<boolean>;
    public files: Observable<FileInfo[]>;
    public filesIsLoading: Observable<boolean>;
    public filesIsLoaded: Observable<boolean>;

    constructor(private store: Store<{ }>) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.designConfig = store.select(designConfigSelector.selectDesignConfig);
        this.designConfigIsLoading = store.select(designConfigSelector.selectDesignConfigIsLoading);
        this.designConfigIsLoaded = store.select(designConfigSelector.selectDesignConfigIsLoaded);
        this.files = store.select(adminFileExplorerSelector.selectFiles);
        this.filesIsLoading = store.select(adminFileExplorerSelector.selectFilesIsLoading);
        this.filesIsLoaded = store.select(adminFileExplorerSelector.selectFilesIsLoaded);
    }

    ngOnInit(): void {
        this.store.dispatch(designConfigActions.loadDesignConfig());
    }

    editDesignConfig(designConfig: DesignConfig): void {
        this.store.dispatch(designConfigActions.editDesignConfig({ designConfig }));
    }

    loadRootDirectory(path: string) {
        this.store.dispatch(adminFileExplorerActions.loadFiles({ path }));
    }
}
