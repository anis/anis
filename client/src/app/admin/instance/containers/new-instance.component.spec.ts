/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { Instance } from 'src/app/metamodel/models';
import { NewInstanceComponent } from './new-instance.component';
import * as instanceActions from 'src/app/metamodel/actions/instance.actions';
import * as adminFileExplorerActions from 'src/app/admin/store/actions/admin-file-explorer.actions';
import { FileInfo } from '../../store/models';
import { INSTANCE } from 'src/test-data';
import { TabsModule } from 'ngx-bootstrap/tabs';

describe('[admin][instance][containers] NewInstanceComponent', () => {
    @Component({ selector: 'app-instance-form', template: ''})
    class InstanceFormStubComponent {
        @Input() instance: Instance;
        @Input() files: FileInfo[];
        @Input() filesIsLoading: boolean;
        @Input() filesIsLoaded: boolean;
        form = new UntypedFormGroup({});
    }

    let component: NewInstanceComponent;
    let fixture: ComponentFixture<NewInstanceComponent>;
    let store: MockStore;
    let instance: Instance;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                NewInstanceComponent,
                InstanceFormStubComponent
            ],
            imports: [
                BrowserAnimationsModule,
                TabsModule,
            ],
            providers: [
                provideMockStore({})
            ]
        })

        fixture = TestBed.createComponent(NewInstanceComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        instance = { ...INSTANCE };
        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('addNewInstance(instance: Instance) should dispatch addInstance action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.addNewInstance(instance);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(instanceActions.addInstance({ instance }));
    });

    it('loadRootDirectory(path: string)should dispatch loadFiles action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        component.loadRootDirectory('test');
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(adminFileExplorerActions.loadFiles({ path: 'test' }));
    });
});
