/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold } from 'jasmine-marbles';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { ActivatedRouteSnapshot } from '@angular/router';
import { DatasetTitleResolver } from './dataset-title.resolver';
import { Instance } from 'src/app/metamodel/models';
import * as instanceActions from 'src/app/metamodel/actions/instance.actions';

describe('[admin][instance][dataset] DatasetTitleResolver', () => {
    let datasetTitleResolver: DatasetTitleResolver;
    let store: MockStore;
    let mockInstanceSelectorInstanceByRouteName;
    let instance: Instance;
    let mockInstanceSelectorSelectInstanceListIsLoaded;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                DatasetTitleResolver,
                provideMockStore({}),
            ]
        })

        store = TestBed.inject(MockStore);
        datasetTitleResolver = TestBed.inject(DatasetTitleResolver);
        mockInstanceSelectorInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
        instance = { ...instance, label: 'test_instance_label' }
    });

    it('should be created', () => {
        expect(datasetTitleResolver).toBeTruthy();
    });

    it('should return false and dispatch loadInstanceList', () => {
        instance = { ...instance, label: 'test_instance_label' }
        const route: ActivatedRouteSnapshot = null;

        mockInstanceSelectorInstanceByRouteName = store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
        mockInstanceSelectorSelectInstanceListIsLoaded = store.overrideSelector(instanceSelector.selectInstanceListIsLoaded, false);
        let spy = jest.spyOn(store, 'dispatch');
        let result = cold('a', { a: datasetTitleResolver.resolve(route, null) });
        const expected = cold('a', { a: [] });
        expect(result).toBeObservable(expected);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(instanceActions.loadInstanceList())
    });

    it('should return test_instance_label -Datasets list', () =>{
        mockInstanceSelectorSelectInstanceListIsLoaded = store.overrideSelector(instanceSelector.selectInstanceListIsLoaded, true);
        const route = <ActivatedRouteSnapshot>{
            component: { name: 'DatasetListComponent' },
        };
        let result = datasetTitleResolver.resolve(route, null);
        let expected = cold('a', {a: 'test_instance_label - Datasets list'});
        expect(result).toBeObservable(expected);


    });

    it('should return test_instance_label - New dataset', () =>{

        mockInstanceSelectorSelectInstanceListIsLoaded = store.overrideSelector(instanceSelector.selectInstanceListIsLoaded, true);
        const route = <ActivatedRouteSnapshot>{
            component: { name: 'TestComponent' },
        };

        let result = datasetTitleResolver.resolve(route, null);
        let expected = cold('a', {a: 'test_instance_label - New dataset'});
        expect(result).toBeObservable(expected);
    });
});
