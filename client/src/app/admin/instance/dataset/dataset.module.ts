/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { DatasetRoutingModule, routedComponents } from './dataset-routing.module';
import { dummiesComponents } from './components';

import { AdminSharedModule } from 'src/app/admin/admin-shared/admin-shared.module';

@NgModule({
    imports: [
        SharedModule,
        DatasetRoutingModule,
        AdminSharedModule
    ],
    declarations: [
        routedComponents,
        dummiesComponents
    ]
})
export class DatasetModule { }
