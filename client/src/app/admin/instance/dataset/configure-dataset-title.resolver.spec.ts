/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { ActivatedRouteSnapshot } from '@angular/router';

import { cold, hot } from 'jasmine-marbles';
import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';
import { ConfigureDatasetTitleResolver } from './configure-dataset-title.resolver';
import { Dataset, Instance } from 'src/app/metamodel/models';
import { DATASET, INSTANCE } from 'src/test-data';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';

describe('[admin][instance][dataset] ConfigureDatasetTitleResolver', () => {
    let configureDatasetTitleResolver: ConfigureDatasetTitleResolver;
    let store: MockStore;
    let dataset: Dataset = DATASET;
    dataset = { ...dataset, label: 'test_dataset_label' };
    let instance: Instance;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [ConfigureDatasetTitleResolver, provideMockStore({})],
        });

        store = TestBed.inject(MockStore);
        configureDatasetTitleResolver = TestBed.inject(
            ConfigureDatasetTitleResolver
        );
    });

    it('should be created', () => {
        expect(configureDatasetTitleResolver).toBeTruthy();
    });

    it('should dispatch datasetActions loadDatasetList action and return datasetListIsLoaded ', () => {
        store.overrideSelector(datasetSelector.selectDatasetListIsLoaded, false);
        const expected = cold('a', { a: [] });
        let spy = jest.spyOn(store, 'dispatch');
        let result = hot('a', {
            a: configureDatasetTitleResolver.resolve(null, null),
        });
        expect(result).toBeObservable(expected);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(datasetActions.loadDatasetList());
    });

    it('should return test_instance_label - Edit dataset test_dataset_label ', () => {
        store.overrideSelector(datasetSelector.selectDatasetListIsLoaded, true);
        store.overrideSelector(datasetSelector.selectDatasetByRouteName, DATASET);
        instance = { ...INSTANCE, label: 'test_instance_label' };
        store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
        const expected = cold('a', {
            a: `${instance.label} - Edit dataset ${DATASET.label}`,
        });
        const route = <ActivatedRouteSnapshot>{
            component: { name: 'test' },
        };
        let result = configureDatasetTitleResolver.resolve(route, null);
        expect(result).toBeObservable(expected);
    });

    it('should return test_instance_label - Edit dataset test_dataset_label ', () => {
        store.overrideSelector(datasetSelector.selectDatasetListIsLoaded, true);
        store.overrideSelector(datasetSelector.selectDatasetByRouteName, DATASET);
        instance = { ...INSTANCE, label: 'test_instance_label' };
        store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
        const expected = cold('a', {
            a: `${instance.label} - Configure dataset ${DATASET.label}`,
        });
        const route = <ActivatedRouteSnapshot>{
            component: { name: 'ConfigureDatasetComponent' },
        };
        let result = configureDatasetTitleResolver.resolve(route, null);
        expect(result).toBeObservable(expected);
    });
});
