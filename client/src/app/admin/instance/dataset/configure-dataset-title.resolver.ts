/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';

import { Observable, combineLatest } from 'rxjs';
import { map, switchMap, skipWhile } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';

import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';

@Injectable({
    providedIn: 'root'
})
export class ConfigureDatasetTitleResolver implements Resolve<string> {
    constructor(private store: Store<{ }>) { }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): string | Observable<string> | Promise<string> {
        return this.store.select(datasetSelector.selectDatasetListIsLoaded).pipe(
            map(datasetListIsLoaded => {
                if (!datasetListIsLoaded) {
                    this.store.dispatch(datasetActions.loadDatasetList());
                }
                return datasetListIsLoaded;
            }),
            skipWhile(datasetListIsLoaded => !datasetListIsLoaded),
            switchMap(() => {
                return combineLatest([
                    this.store.pipe(select(instanceSelector.selectInstanceByRouteName)),
                    this.store.pipe(select(datasetSelector.selectDatasetByRouteName))
                ]).pipe(
                    map(([instance, dataset]) => {
                        if (route.component.name === 'ConfigureDatasetComponent') {
                            return `${instance.label} - Configure dataset ${dataset.label}`;
                        } else {
                            return `${instance.label} - Edit dataset ${dataset.label}`;
                        }
                    })
                );
            })
        );
    }
}
