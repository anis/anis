/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { EditDatasetComponent } from './edit-dataset.component';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import {
    Database,
    Dataset,
    DatasetFamily,
    Instance,
} from 'src/app/metamodel/models';
import * as tableActions from 'src/app/admin/store/actions/table.actions';
import * as adminFileExplorerActions from 'src/app/admin/store/actions/admin-file-explorer.actions';
import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';
import { DATASET } from 'src/test-data';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FileInfo } from 'src/app/admin/store/models';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

describe('[admin][instance][dataset][containers] EditDatasetComponent', () => {
    let component: EditDatasetComponent;
    let fixture: ComponentFixture<EditDatasetComponent>;
    let store: MockStore;
    let mockinstanceSelectorInstanceByRouteName;

    @Component({
        selector: 'app-dataset-form',
        template: '',
    })
    class DatasetFormComponent {
        @Input() instance: Instance;
        @Input() dataset: Dataset;
        @Input() databaseList: Database[];
        @Input() tableListIsLoading: boolean;
        @Input() tableListIsLoaded: boolean;
        @Input() tableList: string[];
        @Input() datasetFamilyList: DatasetFamily[];
        @Input() idDatasetFamily: number;
        @Input() files: FileInfo[];
        @Input() filesIsLoading: boolean;
        @Input() filesIsLoaded: boolean;
        @Output() changeDatabase: EventEmitter<number> = new EventEmitter();
        @Output() loadRootDirectory: EventEmitter<string> = new EventEmitter();
        @Output() onSubmit: EventEmitter<Dataset> = new EventEmitter();
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [EditDatasetComponent, DatasetFormComponent],
            providers: [provideMockStore({})],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule,
                RouterTestingModule.withRoutes([]),
            ],
        });

        fixture = TestBed.createComponent(EditDatasetComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        TestBed.inject(Router);
        let instance: Instance;
        mockinstanceSelectorInstanceByRouteName = store.overrideSelector(
            instanceSelector.selectInstanceByRouteName,
            { ...instance, name: 'test' }
        );
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('store should dispatch tableActions.loadTableList', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let idDatabase: number = 1;
        component.loadTableList(idDatabase);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(
            tableActions.loadTableList({ idDatabase })
        );
    });

    it('store should dispatch adminFileExplorerActions.loadFiles', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let path: string = 'test';
        component.loadRootDirectory(path);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(
            adminFileExplorerActions.loadFiles({ path })
        );
    });

    it('store should dispatch datasetActions.editDataset', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let dataset: Dataset = {
            ...DATASET,
        };
        component.editDataset(dataset);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(
            datasetActions.editDataset({ dataset })
        );
    });
});
