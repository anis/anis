/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { DatasetListComponent } from './dataset-list.component';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { Dataset, DatasetFamily, Instance } from 'src/app/metamodel/models';
import * as datasetFamilyActions from 'src/app/metamodel/actions/dataset-family.actions';
import * as datasetActions from 'src/app/metamodel/actions/dataset.actions';
import { DATASET, DATASET_FAMILY } from 'src/test-data';

describe('[admin][instance][dataset][containers] DatasetListComponent', () => {
    let component: DatasetListComponent;
    let fixture: ComponentFixture<DatasetListComponent>;
    let store: MockStore;
    let mockinstanceSelectorInstanceByRouteName;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DatasetListComponent],
            providers: [provideMockStore({})],

            imports: [BrowserAnimationsModule, ReactiveFormsModule],
        });

        fixture = TestBed.createComponent(DatasetListComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        let instance: Instance;
        mockinstanceSelectorInstanceByRouteName = store.overrideSelector(
            instanceSelector.selectInstanceByRouteName,
            { ...instance, label: 'test' }
        );
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('store should dispatch datasetFamilyActions.addDatasetFamily action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let datasetFamily: DatasetFamily = {
            ...DATASET_FAMILY
        };
        component.addDatasetFamily(datasetFamily);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(
            datasetFamilyActions.addDatasetFamily({ datasetFamily })
        );
    });

    it('store should dispatch datasetFamilyActions.editDatasetFamily action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let datasetFamily: DatasetFamily = {
            ...DATASET_FAMILY
        };
        component.editDatasetFamily(datasetFamily);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(
            datasetFamilyActions.editDatasetFamily({ datasetFamily })
        );
    });

    it('store should dispatch datasetFamilyActions.deleteDatasetFamily action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let datasetFamily: DatasetFamily = {
            ...DATASET_FAMILY
        };
        component.deleteDatasetFamily(datasetFamily);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(
            datasetFamilyActions.deleteDatasetFamily({ datasetFamily })
        );
    });

    it('store should dispatch datasetActions.deleteDataset action', () => {
        let spy = jest.spyOn(store, 'dispatch');
        let dataset: Dataset = {
            ...DATASET,
        };
        component.deleteDataset(dataset);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(
            datasetActions.deleteDataset({ dataset })
        );
    });
});
