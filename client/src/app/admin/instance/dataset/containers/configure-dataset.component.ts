/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { Instance, Dataset, Attribute, CriteriaFamily, OutputCategory, OutputFamily, Image, File, ConeSearchConfig, DetailConfig, AliasConfig, DatasetGroup } from 'src/app/metamodel/models';
import { Column, FileInfo, FitsImageLimits } from 'src/app/admin/store/models';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import * as datasetGroupActions from 'src/app/metamodel/actions/dataset-group.actions';
import * as datasetGroupSelector from 'src/app/metamodel/selectors/dataset-group.selector';
import * as attributeActions from 'src/app/metamodel/actions/attribute.actions';
import * as attributeSelector from 'src/app/metamodel/selectors/attribute.selector';
import * as criteriaFamilyActions from 'src/app/metamodel/actions/criteria-family.actions';
import * as criteriaFamilySelector from 'src/app/metamodel/selectors/criteria-family.selector';
import * as outputFamilyActions from 'src/app/metamodel/actions/output-family.actions';
import * as outputFamilySelector from 'src/app/metamodel/selectors/output-family.selector';
import * as outputCategoryActions from 'src/app/metamodel/actions/output-category.actions';
import * as outputCategorySelector from 'src/app/metamodel/selectors/output-category.selector';
import * as tableActions from 'src/app/admin/store/actions/table.actions';
import * as tableSelector from 'src/app/admin/store/selectors/table.selector';
import * as columnActions from 'src/app/admin/store/actions/column.actions';
import * as columnSelector from 'src/app/admin/store/selectors/column.selector';
import * as attributeDistinctActions from 'src/app/admin/store/actions/attribute-distinct.actions';
import * as attributeDistinctSelector from 'src/app/admin/store/selectors/attribute-distinct.selector';
import * as imageActions from 'src/app/metamodel/actions/image.actions';
import * as imageSelector from 'src/app/metamodel/selectors/image.selector';
import * as fileActions from 'src/app/metamodel/actions/file.actions';
import * as fileSelector from 'src/app/metamodel/selectors/file.selector';
import * as adminFileExplorerActions from 'src/app/admin/store/actions/admin-file-explorer.actions';
import * as adminFileExplorerSelector from 'src/app/admin/store/selectors/admin-file-explorer.selector';
import * as fitsImageActions from 'src/app/admin/store/actions/fits-image.actions';
import * as fitsImageSelector from 'src/app/admin/store/selectors/fits-image.selector';
import * as coneSearchConfigActions from 'src/app/metamodel/actions/cone-search-config.actions';
import * as coneSearchConfigSelector from 'src/app/metamodel/selectors/cone-search-config.selector';
import * as detailConfigActions from 'src/app/metamodel/actions/detail-config.actions';
import * as detailConfigSelector from 'src/app/metamodel/selectors/detail-config.selector';
import * as aliasConfigActions from 'src/app/metamodel/actions/alias-config.actions';
import * as aliasConfigSelector from 'src/app/metamodel/selectors/alias-config.selector';
import * as monacoEditorActions from 'src/app/admin/store/actions/monaco-editor.actions';
import * as monacoEditorSelector from 'src/app/admin/store/selectors/monaco-editor.selector';

@Component({
    selector: 'app-configure-dataset',
    templateUrl: 'configure-dataset.component.html',
    styleUrls: [ 'configure-dataset.component.scss' ]
})
export class ConfigureDatasetComponent implements OnInit {
    public tabSelected: Observable<string>;
    public instance: Observable<Instance>;
    public dataset: Observable<Dataset>;
    public datasetListIsLoading: Observable<boolean>;
    public datasetListIsLoaded: Observable<boolean>;
    public datasetGroupList: Observable<DatasetGroup[]>;
    public datasetGroupListIsLoading: Observable<boolean>;
    public datasetGroupListIsLoaded: Observable<boolean>;
    public attributeList: Observable<Attribute[]>;
    public attributeListIsLoading: Observable<boolean>;
    public attributeListIsLoaded: Observable<boolean>;
    public nextCriteriaFamilyId: Observable<number>;
    public criteriaFamilyList: Observable<CriteriaFamily[]>;
    public criteriaFamilyListIsLoading: Observable<boolean>;
    public criteriaFamilyListIsLoaded: Observable<boolean>;
    public nextOutputFamilyId: Observable<number>;
    public outputFamilyList: Observable<OutputFamily[]>;
    public outputFamilyListIsLoading: Observable<boolean>;
    public outputFamilyListIsLoaded: Observable<boolean>;
    public nextOutputCategoryId: Observable<number>;
    public outputCategoryList: Observable<OutputCategory[]>;
    public outputCategoryListIsLoading: Observable<boolean>;
    public outputCategoryListIsLoaded: Observable<boolean>;
    public tableList: Observable<string[]>;
    public tableListIsLoading: Observable<boolean>;
    public tableListIsLoaded: Observable<boolean>;
    public columnList: Observable<Column[]>;
    public columnListIsLoading: Observable<boolean>;
    public columnListIsLoaded: Observable<boolean>;
    public attributeDistinctList: Observable<string[]>;
    public attributeDistinctListIsLoading: Observable<boolean>;
    public attributeDistinctListIsLoaded: Observable<boolean>;
    public nextImageId: Observable<number>;
    public imageList: Observable<Image[]>;
    public imageListIsLoading: Observable<boolean>;
    public imageListIsLoaded: Observable<boolean>;
    public nextFileId: Observable<number>;
    public fileList: Observable<File[]>;
    public fileListIsLoading: Observable<boolean>;
    public fileListIsLoaded: Observable<boolean>;
    public files: Observable<FileInfo[]>;
    public filesIsLoading: Observable<boolean>;
    public filesIsLoaded: Observable<boolean>;
    public fitsImageLimits: Observable<FitsImageLimits>;
    public fitsImageLimitsIsLoading: Observable<boolean>;
    public fitsImageLimitsIsLoaded: Observable<boolean>;
    public coneSearchConfig: Observable<ConeSearchConfig>;
    public coneSearchConfigIsLoading: Observable<boolean>;
    public coneSearchConfigIsLoaded: Observable<boolean>;
    public detailConfig: Observable<DetailConfig>;
    public detailConfigIsLoading: Observable<boolean>;
    public detailConfigIsLoaded: Observable<boolean>;
    public aliasConfig: Observable<AliasConfig>;
    public aliasConfigIsLoading: Observable<boolean>;
    public aliasConfigIsLoaded: Observable<boolean>;
    public monacoEditorIsLoading: Observable<boolean>;
    public monacoEditorIsLoaded: Observable<boolean>;

    currentOutputFamilyId = null;

    constructor(private store: Store<{ }>, private route: ActivatedRoute) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.dataset = store.select(datasetSelector.selectDatasetByRouteName);
        this.datasetListIsLoading = store.select(datasetSelector.selectDatasetListIsLoading);
        this.datasetListIsLoaded = store.select(datasetSelector.selectDatasetListIsLoaded);
        this.datasetGroupList = store.select(datasetGroupSelector.selectAllDatasetGroups);
        this.datasetGroupListIsLoading = store.select(datasetGroupSelector.selectDatasetGroupListIsLoading);
        this.datasetGroupListIsLoaded = store.select(datasetGroupSelector.selectDatasetGroupListIsLoaded);
        this.attributeList = store.select(attributeSelector.selectAllAttributesOrderById);
        this.attributeListIsLoading = store.select(attributeSelector.selectAttributeListIsLoading);
        this.attributeListIsLoaded = store.select(attributeSelector.selectAttributeListIsLoaded);
        this.nextCriteriaFamilyId = store.select(criteriaFamilySelector.selectNextCriteriaFamilyId);
        this.criteriaFamilyList = store.select(criteriaFamilySelector.selectAllCriteriaFamilies);
        this.criteriaFamilyListIsLoading = store.select(criteriaFamilySelector.selectCriteriaFamilyListIsLoading);
        this.criteriaFamilyListIsLoaded = store.select(criteriaFamilySelector.selectCriteriaFamilyListIsLoaded);
        this.nextOutputFamilyId = store.select(outputFamilySelector.selectNextOutputFamilyId);
        this.outputFamilyList = store.select(outputFamilySelector.selectAllOutputFamilies);
        this.outputFamilyListIsLoading = store.select(outputFamilySelector.selectOutputFamilyListIsLoading);
        this.outputFamilyListIsLoaded = store.select(outputFamilySelector.selectOutputFamilyListIsLoaded);
        this.nextOutputCategoryId = store.select(outputCategorySelector.selectNextOutputCategoryId);
        this.outputCategoryList = store.select(outputCategorySelector.selectAllOutputCategories);
        this.outputCategoryListIsLoading = store.select(outputCategorySelector.selectOutputCategoryListIsLoading);
        this.outputCategoryListIsLoaded = store.select(outputCategorySelector.selectOutputCategoryListIsLoaded);
        this.tableList = store.select(tableSelector.selectAllTables);
        this.tableListIsLoading = store.select(tableSelector.selectTableListIsLoading);
        this.tableListIsLoaded = store.select(tableSelector.selectTableListIsLoaded);
        this.columnList = store.select(columnSelector.selectAllColumns);
        this.columnListIsLoading = store.select(columnSelector.selectColumnListIsLoading);
        this.columnListIsLoaded = store.select(columnSelector.selectColumnListIsLoaded);
        this.attributeDistinctList = store.select(attributeDistinctSelector.selectAllAttributeDistincts);
        this.attributeDistinctListIsLoading = store.select(attributeDistinctSelector.selectAttributeDistinctListIsLoading);
        this.attributeDistinctListIsLoaded = store.select(attributeDistinctSelector.selectAttributeDistinctListIsLoaded);
        this.nextImageId = store.select(imageSelector.selectNextImageId);
        this.imageList = store.select(imageSelector.selectAllImages);
        this.imageListIsLoading = store.select(imageSelector.selectImageListIsLoading);
        this.imageListIsLoaded = store.select(imageSelector.selectImageListIsLoaded);
        this.nextFileId = store.select(fileSelector.selectNextFileId);
        this.fileList = store.select(fileSelector.selectAllFiles);
        this.fileListIsLoading = store.select(fileSelector.selectFileListIsLoading);
        this.fileListIsLoaded = store.select(fileSelector.selectFileListIsLoaded);
        this.files = store.select(adminFileExplorerSelector.selectFiles);
        this.filesIsLoading = store.select(adminFileExplorerSelector.selectFilesIsLoading);
        this.filesIsLoaded = store.select(adminFileExplorerSelector.selectFilesIsLoaded);
        this.fitsImageLimits = store.select(fitsImageSelector.selectFitsImageLimits);
        this.fitsImageLimitsIsLoading = store.select(fitsImageSelector.selectFitsImageLimitsIsLoading);
        this.fitsImageLimitsIsLoaded = store.select(fitsImageSelector.selectFitsImageLimitsIsLoaded);
        this.coneSearchConfig = store.select(coneSearchConfigSelector.selectConeSearchConfig);
        this.coneSearchConfigIsLoading = store.select(coneSearchConfigSelector.selectConeSearchConfigIsLoading);
        this.coneSearchConfigIsLoaded = store.select(coneSearchConfigSelector.selectConeSearchConfigIsLoaded);
        this.detailConfig = store.select(detailConfigSelector.selectDetailConfig);
        this.detailConfigIsLoading = store.select(detailConfigSelector.selectDetailConfigIsLoading);
        this.detailConfigIsLoaded = store.select(detailConfigSelector.selectDetailConfigIsLoaded);
        this.aliasConfig = store.select(aliasConfigSelector.selectAliasConfig);
        this.aliasConfigIsLoading = store.select(aliasConfigSelector.selectAliasConfigIsLoading);
        this.aliasConfigIsLoaded = store.select(aliasConfigSelector.selectAliasConfigIsLoaded);
        this.monacoEditorIsLoading = store.select(monacoEditorSelector.selectMonacoEditorIsLoading);
        this.monacoEditorIsLoaded = store.select(monacoEditorSelector.selectMonacoEditorIsLoaded);
    }

    ngOnInit() {
        Promise.resolve(null).then(() => this.store.dispatch(datasetGroupActions.loadDatasetGroupList()));
        Promise.resolve(null).then(() => this.store.dispatch(attributeActions.loadAttributeList()));
        Promise.resolve(null).then(() => this.store.dispatch(criteriaFamilyActions.loadCriteriaFamilyList()));
        Promise.resolve(null).then(() => this.store.dispatch(outputFamilyActions.loadOutputFamilyList()));
        Promise.resolve(null).then(() => this.store.dispatch(imageActions.loadImageList()));
        Promise.resolve(null).then(() => this.store.dispatch(coneSearchConfigActions.loadConeSearchConfig()));
        Promise.resolve(null).then(() => this.store.dispatch(detailConfigActions.loadDetailConfig()));
        Promise.resolve(null).then(() => this.store.dispatch(aliasConfigActions.loadAliasConfig()));
        Promise.resolve(null).then(() => this.store.dispatch(monacoEditorActions.loadMonacoEditor()));
        this.tabSelected = this.route.queryParamMap.pipe(
            map(params => params.get('tab_selected'))
        );
    }

    loadTableList(idDatabase: number) {
        this.store.dispatch(tableActions.loadTableList({ idDatabase }));
    }

    loadColumnList() {
        this.store.dispatch(columnActions.loadColumnList());
    }

    loadAttributeDistinctList(attribute: Attribute) {
        this.store.dispatch(attributeDistinctActions.loadAttributeDistinctList({ attribute }));
    }

    getVoEnabled(): boolean {
        return true;
    }

    addDatasetGroup(datasetGroup: DatasetGroup) {
        this.store.dispatch(datasetGroupActions.addDatasetGroup({ datasetGroup }));
    }

    deleteDatasetGroup(datasetGroup: DatasetGroup) {
        this.store.dispatch(datasetGroupActions.deleteDatasetGroup({ datasetGroup }));
    }

    addCriteriaFamily(criteriaFamily: CriteriaFamily) {
        this.store.dispatch(criteriaFamilyActions.addCriteriaFamily({ criteriaFamily }));
    }

    editCriteriaFamily(criteriaFamily: CriteriaFamily) {
        this.store.dispatch(criteriaFamilyActions.editCriteriaFamily({ criteriaFamily }));
    }

    deleteCriteriaFamily(criteriaFamily: CriteriaFamily) {
        this.store.dispatch(criteriaFamilyActions.deleteCriteriaFamily({ criteriaFamily }));
    }

    addOutputFamily(outputFamily: OutputFamily) {
        this.store.dispatch(outputFamilyActions.addOutputFamily({ outputFamily }));
    }

    editOutputFamily(outputFamily: OutputFamily) {
        this.store.dispatch(outputFamilyActions.editOutputFamily({ outputFamily }));
    }

    deleteOutputFamily(outputFamily: OutputFamily) {
        this.store.dispatch(outputFamilyActions.deleteOutputFamily({ outputFamily }));
    }

    loadOutputCategoryList(outputFamilyId: number) {
        this.currentOutputFamilyId = outputFamilyId;
        this.store.dispatch(outputCategoryActions.loadOutputCategoryList({ outputFamilyId }));
    }

    addOutputCategory(outputCategory: OutputCategory) {
        this.store.dispatch(outputCategoryActions.addOutputCategory({ outputFamilyId: this.currentOutputFamilyId, outputCategory }));
    }

    editOutputCategory(outputCategory: OutputCategory) {
        this.store.dispatch(outputCategoryActions.editOutputCategory({ outputFamilyId: this.currentOutputFamilyId, outputCategory }));
    }

    deleteOutputCategory(outputCategory: OutputCategory) {
        this.store.dispatch(outputCategoryActions.deleteOutputCategory({ outputFamilyId: this.currentOutputFamilyId, outputCategory }));
    }

    addAttribute(attribute: Attribute) {
        this.store.dispatch(attributeActions.addAttribute({ attribute }));
    }

    addAttributeList(attributeList: Attribute[]) {
        this.store.dispatch(attributeActions.addAttributeList({ attributeList }));
    }

    editAttribute(attribute: Attribute) {
        this.store.dispatch(attributeActions.editAttribute({ attribute }));
    }

    deleteAttribute(attribute: Attribute) {
        this.store.dispatch(attributeActions.deleteAttribute({ attribute }));
    }

    loadRootDirectory(path: string) {
        this.store.dispatch(adminFileExplorerActions.loadFiles({ path }));
    }

    retrieveFitsImageLimits(filePath: string) {
        this.store.dispatch(fitsImageActions.retrieveFitsImageLimits({ filePath }));
    }

    addImage(image: Image) {
        this.store.dispatch(imageActions.addImage({ image }));
    }

    editImage(image: Image) {
        this.store.dispatch(imageActions.editImage({ image }));
    }

    deleteImage(image: Image) {
        this.store.dispatch(imageActions.deleteImage({ image }));
    }

    addFile(file: File) {
        this.store.dispatch(fileActions.addFile({ file }));
    }

    editFile(file: File) {
        this.store.dispatch(fileActions.editFile({ file }));
    }

    deleteFile(file: File) {
        this.store.dispatch(fileActions.deleteFile({ file }));
    }

    addConeSearchConfig(coneSearchConfig: ConeSearchConfig) {
        this.store.dispatch(coneSearchConfigActions.addConeSearchConfig({ coneSearchConfig }));
    }

    editConeSearchConfig(coneSearchConfig: ConeSearchConfig) {
        this.store.dispatch(coneSearchConfigActions.editConeSearchConfig({ coneSearchConfig }));
    }

    addDetailConfig(detailConfig: DetailConfig) {
        this.store.dispatch(detailConfigActions.addDetailConfig({ detailConfig }));
    }

    editDetailConfig(detailConfig: DetailConfig) {
        this.store.dispatch(detailConfigActions.editDetailConfig({ detailConfig }));
    }

    addAliasConfig(aliasConfig: AliasConfig) {
        this.store.dispatch(aliasConfigActions.addAliasConfig({ aliasConfig }));
    }

    editAliasConfig(aliasConfig: AliasConfig) {
        this.store.dispatch(aliasConfigActions.editAliasConfig({ aliasConfig }));
    }
}
