/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ImageListComponent } from './image-list.component';

@Component({
    selector: 'app-add-image',
})
export class AddImageComponent { }

describe('[admin][instance][dataset][components][image] ImageListComponent', () => {
    let component: ImageListComponent;
    let fixture: ComponentFixture<ImageListComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ImageListComponent,
                AddImageComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(ImageListComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
