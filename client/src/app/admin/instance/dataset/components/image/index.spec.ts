/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { imageComponents } from './index';

describe('[admin][instance][dataset][components][image] index', () => {
    it('should test image index components', () => {
        expect(imageComponents.length).toEqual(4);
    });
});
