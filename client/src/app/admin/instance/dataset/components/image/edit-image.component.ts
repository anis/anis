/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy, TemplateRef, Output, EventEmitter } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { Dataset, Image, Instance } from 'src/app/metamodel/models';
import { FileInfo, FitsImageLimits } from 'src/app/admin/store/models';

@Component({
    selector: 'app-edit-image',
    templateUrl: 'edit-image.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditImageComponent {
    @Input() image: Image;
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Input() files: FileInfo[];
    @Input() filesIsLoading: boolean;
    @Input() filesIsLoaded: boolean;
    @Input() fitsImageLimits: FitsImageLimits;
    @Input() fitsImageLimitsIsLoading: boolean;
    @Input() fitsImageLimitsIsLoaded: boolean;
    @Output() loadRootDirectory: EventEmitter<string> = new EventEmitter();
    @Output() retrieveFitsImageLimits: EventEmitter<string> = new EventEmitter();
    @Output() edit: EventEmitter<Image> = new EventEmitter();

    modalRef: BsModalRef;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }
}
