/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { OutputFamilyFormComponent } from './output-family-form.component';
import { OUTPUT_FAMILY } from 'src/test-data';

describe('[admin][instance][dataset][components][output-family] OutputFamilyFormComponent', () => {
    let component: OutputFamilyFormComponent;
    let fixture: ComponentFixture<OutputFamilyFormComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                OutputFamilyFormComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(OutputFamilyFormComponent);
        component = fixture.componentInstance;
        component.outputFamily = {
          ...OUTPUT_FAMILY
        };
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('submit() should emit outputCategory and form.value()', () => {
        let spy = jest.spyOn(component.onSubmit, 'emit');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.outputFamily, ...component.form.value });
    });

    it('submit() should emit only form.value()', () => {
        let spy = jest.spyOn(component.onSubmit, 'emit');
        component.outputFamily = null;
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.form.value });
    })
});
