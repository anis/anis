/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OutputCategoryListComponent } from './output-category-list.component';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { OutputCategory } from 'src/app/metamodel/models';

describe('[admin][instance][dataset][components][output-category] OutputCategoryListComponent', () => {
    let component: OutputCategoryListComponent;
    let fixture: ComponentFixture<OutputCategoryListComponent>;

    @Component({
        selector: 'app-add-output-category',
        template: '',
    })
    class AddOutputCategoryComponent {
        @Input() nextOutputCategoryId: number;
        @Output() add: EventEmitter<OutputCategory> = new EventEmitter();
    }
    @Component({
        selector: 'app-edit-output-category',
        template: '',
    })
    class EditOutputCategoryComponent {
        @Input() outputCategory: OutputCategory;
        @Output() edit: EventEmitter<OutputCategory> = new EventEmitter();
    }
    @Component({
        selector: 'app-delete-btn',
        template: '',
    })
    class DeleteBtnComponent {
        @Input() type: string;
        @Input() label: string;
        @Input() disabled: boolean = false;
        @Output() confirm: EventEmitter<{}> = new EventEmitter();
        @Output() abort: EventEmitter<{}> = new EventEmitter();
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                OutputCategoryListComponent,
                AddOutputCategoryComponent,
                EditOutputCategoryComponent,
                DeleteBtnComponent,
            ],
            imports: [BrowserAnimationsModule, ReactiveFormsModule],
        });
        fixture = TestBed.createComponent(OutputCategoryListComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
