/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { OutputCategoryListComponent } from './output-category-list.component';
import { OutputCategoryFormComponent } from './output-category-form.component';
import { AddOutputCategoryComponent } from './add-output-category.component';
import { EditOutputCategoryComponent } from './edit-output-category.component';

export const outputCategoryComponents = [
    OutputCategoryListComponent,
    OutputCategoryFormComponent,
    AddOutputCategoryComponent,
    EditOutputCategoryComponent
];
