/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy, TemplateRef, Output, EventEmitter } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { OutputFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-edit-output-family',
    templateUrl: 'edit-output-family.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditOutputFamilyComponent {
    @Input() outputFamily: OutputFamily;
    @Output() edit: EventEmitter<OutputFamily> = new EventEmitter();

    modalRef: BsModalRef;

    constructor(private modalService: BsModalService) { }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }
}
