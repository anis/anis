/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TemplateRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { AddOutputFamilyComponent } from './add-output-family.component';

describe('[admin][instance][dataset][components][output-family] AddOutputFamilyComponent', () => {
    let component: AddOutputFamilyComponent;
    let fixture: ComponentFixture<AddOutputFamilyComponent>;
    const modalServiceStub = {
        show: jest.fn(),
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                AddOutputFamilyComponent,
            ],
            providers: [
                BsModalRef,
                { provide: BsModalService, useValue: modalServiceStub }
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(AddOutputFamilyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should call modalRef.show(template)', () => {
        let template: TemplateRef<any> = null;
        let spy = jest.spyOn(modalServiceStub, 'show');
        component.openModal(template);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(template);
    });
});
