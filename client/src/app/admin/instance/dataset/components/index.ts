/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { attributeComponents } from './attribute';
import { datasetGroupComponents } from './dataset-group';
import { criteriaFamilyComponents } from './criteria-family';
import { datasetComponents } from './dataset';
import { datasetFamilyComponents } from './dataset-family';
import { imageComponents } from './image';
import { fileComponents } from './file';
import { coneSearchConfigComponents } from './cone-search-config';
import { detailConfigComponents } from './detail-page';
import { aliasConfigComponents } from './alias-config';
import { outputFamilyComponents } from './output-family';
import { AddAttributeComponent } from './add-attribute.component';
import { InstanceButtonsComponent } from './instance-buttons.component';

export const dummiesComponents = [
    attributeComponents,
    datasetGroupComponents,
    criteriaFamilyComponents,
    datasetComponents,
    datasetFamilyComponents,
    imageComponents,
    fileComponents,
    coneSearchConfigComponents,
    detailConfigComponents,
    aliasConfigComponents,
    outputFamilyComponents,
    AddAttributeComponent,
    InstanceButtonsComponent
];
