/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { DetailConfig } from 'src/app/metamodel/models';

@Component({
    selector: 'app-detail-config',
    templateUrl: 'detail-config.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailConfigComponent {
    @Input() detailConfig: DetailConfig;
    @Output() addDetailConfig: EventEmitter<DetailConfig> = new EventEmitter();
    @Output() editDetailConfig: EventEmitter<DetailConfig> = new EventEmitter();

    save(detailConfig: DetailConfig) {
        if (this.detailConfig) {
            this.editDetailConfig.emit({
                ...this.detailConfig,
                ...detailConfig
            });
        } else {
            this.addDetailConfig.emit(detailConfig);
        }
    }
}
