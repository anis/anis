/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
    ReactiveFormsModule,
    UntypedFormControl,
    UntypedFormGroup,
} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DetailConfig } from 'src/app/metamodel/models';
import { DetailConfigComponent } from './detail-config.component';
import { DETAIL_CONFIG } from 'src/test-data';

@Component({
    selector: 'app-detail-config-form',
    templateUrl: 'detail-config-form.component.html',
})
class DetailConfigFormComponent {
    form = new UntypedFormGroup({
        content: new UntypedFormControl(),
        style_sheet: new UntypedFormControl(null),
    });
}

describe('[admin][instance][dataset][components][detail-page] DetailConfigComponent', () => {
    let component: DetailConfigComponent;
    let fixture: ComponentFixture<DetailConfigComponent>;

    @Component({
        selector: 'app-detail-config-form',
        template: '',
    })
    class DetailConfigFormComponent {
        @Input() detailConfig: DetailConfig;
        @Output() onSubmit: EventEmitter<DetailConfig> = new EventEmitter();

        public form = new UntypedFormGroup({
            content: new UntypedFormControl(),
            style_sheet: new UntypedFormControl(null),
        });
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DetailConfigComponent, DetailConfigFormComponent],
            imports: [BrowserAnimationsModule, ReactiveFormsModule],
        });
        fixture = TestBed.createComponent(DetailConfigComponent);
        component = fixture.componentInstance;
        component.detailConfig = { ...DETAIL_CONFIG };
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('save(detailConfig) should emit component.detailConfig and detaiConfig', () => {
        let spy = jest.spyOn(component.editDetailConfig, 'emit');
        let detailConfig: DetailConfig = { ...DETAIL_CONFIG };
        component.save(detailConfig);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({
            ...component.detailConfig,
            ...detailConfig,
        });
    });

    it('save(detailConfig) should emit  detaiConfig only', () => {
        let spy = jest.spyOn(component.addDetailConfig, 'emit');
        let detailConfig: DetailConfig = { ...DETAIL_CONFIG };
        component.detailConfig = null;
        component.save(detailConfig);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...detailConfig });
    });
});
