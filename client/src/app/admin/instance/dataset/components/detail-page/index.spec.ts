/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { detailConfigComponents } from './index';

describe('[admin][instance][dataset][components]detail-page] index', () => {
    it('should test detail-page index components', () => {
        expect(detailConfigComponents.length).toEqual(2);
    });
});
