/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DatasetFamilyFormComponent } from './dataset-family-form.component';

describe('[admin][instance][dataset][components][dataset-family] DatasetFamilyFormComponent', () => {
    let component: DatasetFamilyFormComponent;
    let fixture: ComponentFixture<DatasetFamilyFormComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetFamilyFormComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(DatasetFamilyFormComponent);
        component = fixture.componentInstance;
        component.datasetFamily = { display: 10, id: 1, label: 'test', opened: false };
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('submit() should emit databaseFamily and form.value', () => {
        let spy = jest.spyOn(component.onSubmit, 'emit');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.datasetFamily, ...component.form.value });
    });

    it('submit() should emitform.value only', () => {
        let spy = jest.spyOn(component.onSubmit, 'emit');
        component.datasetFamily = null;
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.form.value });
    });
});
