/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TableOutputComponent } from "./table-output.component";
import { TrOutputComponent } from "./tr-output.component";

export const outputComponents = [
    TableOutputComponent,
    TrOutputComponent
];
