/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export const searchTypeList = [
    { label: 'Field', value: 'field', display: 10 },
    { label: 'Between', value: 'between', display: 20 },
    { label: 'Select', value: 'select', display: 30 },
    { label: 'Select multiple', value: 'select-multiple', display: 40 },
    { label: 'Datalist', value: 'datalist', display: 50 },
    { label: 'List', value: 'list', display: 60 },
    { label: 'Radio', value: 'radio', display: 70 },
    { label: 'Checkbox', value: 'checkbox', display: 80 },
    { label: 'Between date', value: 'between-date', display: 90 },
    { label: 'Date', value: 'date', display: 100 },
    { label: 'Time', value: 'time', display: 110 },
    { label: 'Date time', value: 'date-time', display: 120 },
    { label: 'Select by alias', value: 'select-alias', display: 130 },
    { label: 'Select multiple by alias', value: 'select-multiple-alias', display: 140 },
    { label: 'JSON', value: 'json', display: 150 },
    { label: 'SVOM JSON KW', value: 'svom_json_kw', display: 160 }
];
