
/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TableResultComponent } from './table-result.component';
import { TrResultComponent } from './tr-result.component';

describe('[admin][instance][dataset][components][attribute][result] TableResultComponent ', () => {
    let component: TableResultComponent;
    let fixture: ComponentFixture<TableResultComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                TableResultComponent,
            ],
            imports: [
                BrowserAnimationsModule
            ],
        });
        fixture = TestBed.createComponent(TableResultComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('saveAll() should call the submit method one time', () => {
        const trResultComponent = new TrResultComponent();
        trResultComponent.form = new UntypedFormGroup({
            test: new UntypedFormControl({ value: 'test' })
        })
        trResultComponent.form.markAsDirty();
        const spy = jest.spyOn(trResultComponent, 'submit');
        Object.assign((component as any).trResultList, { _results: [{ ...trResultComponent }] });
        component.saveAll();
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('saveAllDisabled() should return false', () => {
        const trResultComponent = new TrResultComponent();
        trResultComponent.form = new UntypedFormGroup({
            test: new UntypedFormControl({ value: 'test' })
        });
        trResultComponent.form.markAsDirty();
        Object.assign((component as any).trResultList, { _results: [trResultComponent] });
        expect(component.saveAllDisabled()).toBe(false);
    });
});
