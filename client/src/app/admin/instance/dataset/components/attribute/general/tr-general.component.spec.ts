/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { Attribute } from 'src/app/metamodel/models';
import { TrGeneralComponent } from './tr-general.component';

describe('[admin][instance][dataset][components][attribute][general] TrDetailComponent', () => {
    let component: TrGeneralComponent;
    let fixture: ComponentFixture<TrGeneralComponent>;

    @Component({
        selector: 'app-delete-btn',
        template: '',
    })
    class DeleteBtnComponent {
        @Input() type: string;
        @Input() label: string;
        @Input() disabled: boolean = false;
        @Output() confirm: EventEmitter<{}> = new EventEmitter();
        @Output() abort: EventEmitter<{}> = new EventEmitter();
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [TrGeneralComponent, DeleteBtnComponent],
            imports: [BrowserAnimationsModule, ReactiveFormsModule],
        });
        fixture = TestBed.createComponent(TrGeneralComponent);
        component = fixture.componentInstance;
        let attribute: Attribute;
        component.attribute = {
            ...attribute,
            name: 'test',
            id: 0,
            label: 'test',
            form_label: 'test',
            datatable_label: 'test',
            description: 'test'
        };
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('submit() should emit with attribute and form.value', () => {
        let spy = jest.spyOn(component.save, 'emit');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({
            ...component.attribute,
            ...component.form.value,
        });
    });
});
