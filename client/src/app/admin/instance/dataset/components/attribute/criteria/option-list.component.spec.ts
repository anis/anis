/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
    ReactiveFormsModule,
    UntypedFormArray,
    UntypedFormControl,
    UntypedFormGroup,
    Validators,
} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { OptionListComponent } from './option-list.component';

describe('[admin][instance][dataset][components][attribute][criteria] OptionListComponent', () => {
    let component: OptionListComponent;
    let fixture: ComponentFixture<OptionListComponent>;
    let newOptionFormGroup: UntypedFormGroup = new UntypedFormGroup({
        label: new UntypedFormControl('', [Validators.required]),
        value: new UntypedFormControl('', [Validators.required]),
        display: new UntypedFormControl('', [Validators.required]),
    });

    @Component({
        selector: 'app-option-form',
        template: '',
    })
    class OptionFormComponent {
        @Input() form: UntypedFormGroup;
    }

    @Component({
        selector: 'app-generate-option-list',
        template: '',
    })
    class GenerateOptionListComponent {
        @Input() optionListIsEmpty: boolean;
        @Input() attributeDistinctList: string[];
        @Input() attributeDistinctListIsLoading: boolean;
        @Input() attributeDistinctListIsLoaded: boolean;
        @Output() loadAttributeDistinctList: EventEmitter<{}> =
            new EventEmitter();
        @Output() generateOptionList: EventEmitter<string[]> =
            new EventEmitter();
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                OptionListComponent,
                OptionFormComponent,
                GenerateOptionListComponent,
            ],
            imports: [BrowserAnimationsModule, ReactiveFormsModule],
        });
        fixture = TestBed.createComponent(OptionListComponent);
        component = fixture.componentInstance;
        component.optionList = [{ label: '', display: 10, value: null }];
        component.form = new UntypedFormArray([]);
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('buildFormGroup() return an instance of UntypedFormGroup', () => {
        let result = component.buildFormGroup();
        expect(result).toBeInstanceOf(UntypedFormGroup);
    });

    it('addOption() should add newOptionFormGroup in form array , call buildFormGroup() and form.markAsDirty()', () => {
        component.form = new UntypedFormArray([]);
        component.newOptionFormGroup = newOptionFormGroup;
        let spyOnBuildFormGroup = jest.spyOn(component, 'buildFormGroup');
        let spyOnFormDirty = jest.spyOn(component.form, 'markAsDirty');
        expect(component.form.controls.length).toEqual(0);
        component.addOption();
        expect(component.form.controls.length).toEqual(1);
        expect(spyOnBuildFormGroup).toHaveBeenCalledTimes(1);
        expect(spyOnFormDirty).toHaveBeenCalledTimes(1);
    });

    it('removeOption(index: number) should remove the element on the index in the form array and call form.markAsDirty()', () => {
        component.form = new UntypedFormArray([newOptionFormGroup]);
        let spyOnForm = jest.spyOn(component.form, 'markAsDirty');
        expect(component.form.controls.length).toEqual(1);
        component.removeOption(0);
        expect(component.form.controls.length).toEqual(0);
        expect(spyOnForm).toHaveBeenCalledTimes(1);
    });

    it('generateOptionList(attributeDistinctList: string[]) should call form.clear()', () => {
        component.form = new UntypedFormArray([]);
        let spyOnFormClear = jest.spyOn(component.form, 'clear');
        let spyOnFormDirty = jest.spyOn(component.form, 'markAsDirty');
        let attributeDistinctList: string[] = ['test1', 'test2', 'test3'];
        expect(component.form.controls.length).toEqual(0);
        component.generateOptionList(attributeDistinctList);
        expect(spyOnFormClear).toHaveBeenCalledTimes(1);
        expect(component.form.controls.length).toEqual(
            attributeDistinctList.length
        );
        expect(spyOnFormDirty).toHaveBeenCalledTimes(1);
    });
});
