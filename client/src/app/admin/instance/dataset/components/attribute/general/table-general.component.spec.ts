/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TableGeneralComponent } from './table-general.component';
import { TrGeneralComponent } from './tr-general.component';

describe('[admin][instance][dataset][components][attribute][general] TableGeneralComponent', () => {
    let component: TableGeneralComponent;
    let fixture: ComponentFixture<TableGeneralComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                TableGeneralComponent,
            ],
            imports: [
                BrowserAnimationsModule,
            ],
        });
        fixture = TestBed.createComponent(TableGeneralComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('saveAll() should call the submit method one time', () => {
        const trGeneralComponent1 = new TrGeneralComponent();
        trGeneralComponent1.form = new UntypedFormGroup({
            test: new UntypedFormControl({ value: 'test' }),
            datatable_label: new UntypedFormControl({value: '' })
        });
        trGeneralComponent1.form.markAsDirty();
        const spy = jest.spyOn(trGeneralComponent1, 'submit');
        Object.assign((component as any).trGeneralList, { _results: [{ ...trGeneralComponent1 }] });
        component.saveAll();
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('saveAllDisabled() should return false', () => {
        const trGeneralComponent1 = new TrGeneralComponent();
        trGeneralComponent1.form = new UntypedFormGroup({
            test: new UntypedFormControl({ value: 'test' })
        })
        trGeneralComponent1.form.markAsDirty();
        Object.assign((component as any).trGeneralList, { _results: [trGeneralComponent1] });
        expect(component.saveAllDisabled()).toBe(false);
    });
});
