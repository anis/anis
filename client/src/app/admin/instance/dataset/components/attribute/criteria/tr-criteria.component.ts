/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup } from '@angular/forms';

import { Attribute, CriteriaFamily } from 'src/app/metamodel/models';
import { searchTypeOperators } from 'src/app/shared/utils';
import { searchTypeList } from './search-type-list';

@Component({
    selector: '[criteria]',
    templateUrl: 'tr-criteria.component.html',
    styleUrls: [ '../tr.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrCriteriaComponent implements OnInit {
    @Input() attribute: Attribute;
    @Input() criteriaFamilyList: CriteriaFamily[];
    @Input() attributeDistinctList: string[];
    @Input() attributeDistinctListIsLoading: boolean;
    @Input() attributeDistinctListIsLoaded: boolean;
    @Output() save: EventEmitter<Attribute> = new EventEmitter();
    @Output() loadAttributeDistinctList: EventEmitter<{}> = new EventEmitter();

    searchTypeList = searchTypeList;
    operatorList = searchTypeOperators;
    optionsFormArray = new UntypedFormArray([]);

    public form = new UntypedFormGroup({
        name: new UntypedFormControl({ value: '', disabled: true }),
        type: new UntypedFormControl({ value: '', disabled: true }),
        id_criteria_family: new UntypedFormControl(),
        search_type: new UntypedFormControl(),
        operator: new UntypedFormControl(),
        dynamic_operator: new UntypedFormControl(),
        null_operators_enabled: new UntypedFormControl(),
        min: new UntypedFormControl(),
        max: new UntypedFormControl(),
        options: this.optionsFormArray,
        placeholder_min: new UntypedFormControl(),
        placeholder_max: new UntypedFormControl(),
        criteria_display: new UntypedFormControl()
    });

    ngOnInit() {
        if (this.attribute) {
            this.form.patchValue(this.attribute);
        }
    }
    
    criteriaFamilyOnChange(): void {
        if (this.form.controls['id_criteria_family'].value === '') {
            this.form.controls['id_criteria_family'].setValue(null);
            this.searchTypeOnChange();
        }
    }
    
    searchTypeOnChange(): void {
        if (this.form.controls['search_type'].value === '') {
            this.form.controls['search_type'].setValue(null);
            this.operatorOnChange();
        }
    }

    operatorOnChange(): void {
        if (this.form.controls['operator'].value === '') {
            this.form.controls['operator'].setValue(null);
            this.form.controls['min'].setValue(null);
            this.form.controls['max'].setValue(null);
            this.form.controls['placeholder_min'].setValue(null);
            this.form.controls['placeholder_max'].setValue(null);
            this.optionsFormArray.clear();
        }
    }

    getMinValuePlaceholder(searchType: string): string {
        if (searchType === 'between' || searchType === 'between-date') {
            return 'Default min value (optional)';
        } else {
            return 'Default value (optional)';
        }
    }

    submit(): void {
        this.save.emit({
            ...this.attribute,
            ...this.form.value
        });
        this.form.markAsPristine();
    }
}
