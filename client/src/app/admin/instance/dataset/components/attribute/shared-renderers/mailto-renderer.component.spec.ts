/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MailtoRendererComponent } from './mailto-renderer.component';

describe('[admin][instance][dataset][components][attribute][result][renderers] MailtoRendererComponent', () => {
    let component: MailtoRendererComponent;
    let fixture: ComponentFixture<MailtoRendererComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                MailtoRendererComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(MailtoRendererComponent);
        component = fixture.componentInstance;
        component.form = new UntypedFormGroup({
            subject: new UntypedFormControl()
        });
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
