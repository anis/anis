/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { generalComponents } from './index';

describe('[admin][instance][dataset][components][attribute][general] index', () => {
    it('Test general index components', () => {
        expect(generalComponents.length).toEqual(2);
    });
});
