/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { generalComponents } from './general';
import { criteriaComponents } from './criteria';
import { outputComponents } from './output';
import { resultComponents } from './result';
import { detailComponents } from './detail';
import { voComponents } from './vo';
import { sharedRenderers } from './shared-renderers';

export const attributeComponents = [
    generalComponents,
    criteriaComponents,
    outputComponents,
    resultComponents,
    detailComponents,
    voComponents,
    sharedRenderers
];
