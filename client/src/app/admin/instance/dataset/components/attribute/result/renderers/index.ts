/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DetailLinkRendererComponent } from './detail-link-renderer.component';
import { LinkSearchRendererComponent } from './link-search-renderer.component';
import { sharedRendererList } from '../../shared-renderers';

export const renderers = [
    DetailLinkRendererComponent,
    LinkSearchRendererComponent
];

export * from './renderer-form-factory';

export const rendererList = [
    ...sharedRendererList,
    { label: 'Link detail page', value: 'detail-link', display: 10 },
    { label: 'Link to another search (result page)', value: 'link-search', display: 70 }
];
