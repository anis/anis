/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';

import { Attribute, OutputFamily, OutputCategory, Dataset } from 'src/app/metamodel/models';

@Component({
    selector: '[output]',
    templateUrl: 'tr-output.component.html',
    styleUrls: ['../tr.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrOutputComponent implements OnInit {
    @Input() dataset: Dataset;
    @Input() attribute: Attribute;
    @Input() outputFamilyList: OutputFamily[];
    @Output() save: EventEmitter<Attribute> = new EventEmitter();

    public form = new UntypedFormGroup({
        name: new UntypedFormControl({ value: '', disabled: true }),
        output_display: new UntypedFormControl(),
        id_output_family: new UntypedFormControl(),
        id_output_category: new UntypedFormControl(),
        selected: new UntypedFormControl()
    });

    ngOnInit() {
        if (this.attribute) {
            this.form.patchValue(this.attribute);
            if (this.attribute.id_output_category) {
                const [ id_output_family, id_output_category ] = this.attribute.id_output_category.split('_');
                this.form.controls['id_output_family'].setValue(+id_output_family);
                this.form.controls['id_output_category'].setValue(+id_output_category);
            }
        }
        if (this.dataset.primary_key === this.attribute.id) {
            this.form.controls['selected'].setValue(true);
            this.form.controls['selected'].disable();
        }
    }

    outputCategoryOnChange(): void {
        if (this.form.controls['id_output_category'].value === '') {
            this.form.controls['id_output_category'].setValue(null);
        }
    }

    getOutputCategory(): OutputCategory[] {
        return this.outputFamilyList
            .find(outputFamily => outputFamily.id === this.form.controls['id_output_family'].value)
            .output_categories;
    }

    submit(): void {
        let id_output_category = null;
        if (this.form.controls['id_output_family'].value && this.form.controls['id_output_category'].value) {
            id_output_category = `${this.form.controls['id_output_family'].value}_${this.form.controls['id_output_category'].value}`;
        }
        this.save.emit({
            ...this.attribute,
            ...this.form.value,
            id_output_category
        });
        this.form.markAsPristine();
    }
}
