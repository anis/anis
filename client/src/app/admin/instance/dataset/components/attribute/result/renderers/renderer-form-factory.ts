/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { UntypedFormControl } from '@angular/forms';

import { SharedRendererFormFactory } from '../../shared-renderers';

export abstract class RendererFormFactory {
    static create(renderer: string) {
        const sharedRendererForm = SharedRendererFormFactory.create(renderer);

        let rendererForm = null;
        switch (renderer) {
            case 'detail-link':
                rendererForm = {
                    display: new UntypedFormControl('text'),
                };
                break;
            case 'link-search':
                return {
                    dataset: new UntypedFormControl(''),
                    attributes: new UntypedFormControl(''),
                    criteria: new UntypedFormControl(''),
                    display: new UntypedFormControl('icon-button'),
                    text: new UntypedFormControl('$value'),
                    icon: new UntypedFormControl('fas fa-link')
                };
        }

        return rendererForm ? rendererForm : sharedRendererForm;
    }
}
