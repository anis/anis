/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DownloadRendererComponent } from './download-renderer.component';
import { ImageRendererComponent } from './image-renderer.component';
import { LinkRendererComponent } from './link-renderer.component';
import { FormatNumberRendererComponent } from './format-number-renderer.component';
import { BadgeRendererComponent } from './badge-renderer.component';
import { MailtoRendererComponent } from './mailto-renderer.component';

export const sharedRenderers = [
    DownloadRendererComponent,
    ImageRendererComponent,
    LinkRendererComponent,
    FormatNumberRendererComponent,
    BadgeRendererComponent,
    MailtoRendererComponent
];

export * from './shared-renderer-form-factory';

export const sharedRendererList = [
    { label: 'Download file', value: 'download', display: 20 },
    { label: 'Link (file, web page...)', value: 'link', display: 30 },
    { label: 'Display image (png, jpg...)', value: 'image', display: 40 },
    { label: 'Display json', value: 'json', display: 50 },
    { label: 'Format number value', value: 'format', display: 60 },
    { label: 'Badge', value: 'badge', display: 70 },
    { label: 'mailto', value: 'mailto', display: 80}
];
