/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TrOutputComponent } from './tr-output.component';
import {
    DATASET,
    ATTRIBUTE,
    OUTPUT_FAMILY_LIST,
    OUTPUT_CATEGORY_LIST,
} from 'src/test-data';
import { OutputCategory } from 'src/app/metamodel/models';

describe('[admin][instance][dataset][components][attribute][] TrOutputComponent', () => {
    let component: TrOutputComponent;
    let fixture: ComponentFixture<TrOutputComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [TrOutputComponent],
            imports: [BrowserAnimationsModule, ReactiveFormsModule],
        });
        fixture = TestBed.createComponent(TrOutputComponent);
        component = fixture.componentInstance;
        component.dataset = { ...DATASET };
        component.attribute = { ...ATTRIBUTE };
        component.outputFamilyList = [...OUTPUT_FAMILY_LIST];
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('outputCategoryOnChange() should set form.id_output_category to null', () => {
        component.form.controls['id_output_category'].setValue('');
        expect(component.form.controls['id_output_category'].value).toEqual('');
        component.outputCategoryOnChange();
        expect(component.form.controls['id_output_category'].value).toEqual(
            null
        );
    });

    it('getOutputFamilyLabel(idOutputFamilly: number) should return the given output category', () => {
        component.form.value.id_output_category = 1;
        let result: OutputCategory[] = component.getOutputCategory();
        expect(result).toEqual(OUTPUT_CATEGORY_LIST);
    });

    it('submit() should emit with attribute and form.value', () => {
        let spy = jest.spyOn(component.save, 'emit');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({
            ...component.attribute,
            ...component.form.value,
            id_output_category: '1_2',
        });
    });
});
