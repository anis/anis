/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import {
    ReactiveFormsModule,
    UntypedFormControl,
    UntypedFormGroup,
} from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormatNumberRendererComponent } from './format-number-renderer.component';

describe('[admin][instance][dataset][components][attribute][result][renderers] FormatNumberRendererComponent', () => {
    let component: FormatNumberRendererComponent;
    let fixture: ComponentFixture<FormatNumberRendererComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [FormatNumberRendererComponent],
            imports: [BrowserAnimationsModule, ReactiveFormsModule],
        });
        fixture = TestBed.createComponent(FormatNumberRendererComponent);
        component = fixture.componentInstance;
        const form = new UntypedFormGroup({
            format: new UntypedFormControl(),
            exponential: new UntypedFormControl(),
        });
        component.form = form;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
