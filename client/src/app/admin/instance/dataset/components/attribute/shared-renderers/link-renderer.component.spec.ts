/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LinkRendererComponent } from './link-renderer.component';

describe('[admin][instance][dataset][components][attribute][result][renderers] LinkRendererComponent', () => {
    let component: LinkRendererComponent;
    let fixture: ComponentFixture<LinkRendererComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                LinkRendererComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(LinkRendererComponent);
        component = fixture.componentInstance;
        component.form = new UntypedFormGroup({
            href: new UntypedFormControl(),
            display: new UntypedFormControl(),
            blank: new UntypedFormControl(),
            text: new UntypedFormControl()
        });
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
