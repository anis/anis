/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TableCriteriaComponent } from './table-criteria.component';
import { TrCriteriaComponent } from './tr-criteria.component';
import { OptionListComponent } from './option-list.component';
import { OptionFormComponent } from './option-form.component';
import { GenerateOptionListComponent } from './generate-option-list.component';

export const criteriaComponents = [
    TableCriteriaComponent,
    TrCriteriaComponent,
    OptionListComponent,
    OptionFormComponent,
    GenerateOptionListComponent
];
