/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DetailLinkRendererComponent } from './detail-link-renderer.component';

describe('[admin][instance][dataset][components][attribute][result][renderers] DetailLinkRendererComponent', () => {
    let component: DetailLinkRendererComponent;
    let fixture: ComponentFixture<DetailLinkRendererComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DetailLinkRendererComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(DetailLinkRendererComponent);
        component = fixture.componentInstance;
        component.form = new UntypedFormGroup({
            display: new UntypedFormControl(),
            text: new UntypedFormControl()
        });
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
