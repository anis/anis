/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BadgeRendererComponent } from './badge-renderer.component';

describe('[admin][instance][dataset][components][attribute][result][renderers] BadgeRendererComponent', () => {
    let component: BadgeRendererComponent;
    let fixture: ComponentFixture<BadgeRendererComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                BadgeRendererComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(BadgeRendererComponent);
        component = fixture.componentInstance;
        component.form = new UntypedFormGroup({
            pill: new UntypedFormControl(false),
            color: new UntypedFormControl(''),
            block: new UntypedFormControl(false)
        });
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
