/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { UntypedFormControl } from '@angular/forms';
import { RendererFormFactory } from './renderer-form-factory';

class TestClass extends RendererFormFactory { }

describe('[admin][instance][dataset][components][attribute][result][renderers] RendererFormFactory', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [TestClass],
        })
    });

    it('should test case detail-link', () => {
        const spy = jest.spyOn(TestClass, 'create');
        const form = { display: new UntypedFormControl('text') };
        const result = TestClass.create('detail-link');
        expect(spy).toHaveBeenCalled();
        expect(JSON.stringify(result)).toEqual(JSON.stringify(form));
    })

    it('should test case link', () => {
        const spy = jest.spyOn(TestClass, 'create');
        const result = TestClass.create('link');
        const form = {
            href: new UntypedFormControl('$value'),
            display: new UntypedFormControl('text'),
            text: new UntypedFormControl('$value'),
            icon: new UntypedFormControl('fas fa-link'),
            blank: new UntypedFormControl(true)
        };
        expect(spy).toHaveBeenCalled();
        expect(JSON.stringify(result)).toEqual(JSON.stringify(form));
    })

    it('should test case download', () => {
        const spy = jest.spyOn(TestClass, 'create');
        const result = TestClass.create('download');
        const form = {
            display: new UntypedFormControl('icon-button'),
            text: new UntypedFormControl('DOWNLOAD'),
            icon: new UntypedFormControl('fas fa-download'),
            samp: new UntypedFormControl(false)
        };
        expect(spy).toHaveBeenCalled();
        expect(JSON.stringify(result)).toEqual(JSON.stringify(form));
    })

    it('should test case image', () => {
        const spy = jest.spyOn(TestClass, 'create');
        const result = TestClass.create('image');
        const form = {
            type: new UntypedFormControl('fits'),
            hdu_number: new UntypedFormControl(null),
            display: new UntypedFormControl('modal'),
            width: new UntypedFormControl(''),
            height: new UntypedFormControl('')
        }
        expect(spy).toHaveBeenCalled();
        expect(JSON.stringify(result)).toEqual(JSON.stringify(form));

    })

    it('should test case default', () => {
        const spy = jest.spyOn(TestClass, 'create');
        const result = TestClass.create('');
        expect(spy).toHaveBeenCalled();
        expect(result).toEqual({});
    })
});
