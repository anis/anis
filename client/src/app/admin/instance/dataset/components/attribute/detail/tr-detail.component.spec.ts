/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TrDetailComponent } from './tr-detail.component';
import * as fromRenders from './renderers';
import { ATTRIBUTE, OUTPUT_FAMILY_LIST } from 'src/test-data';

describe('[admin][instance][dataset][components][attribute][detail] TrDetailComponent', () => {
    let component: TrDetailComponent;
    let fixture: ComponentFixture<TrDetailComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [TrDetailComponent],
            imports: [BrowserAnimationsModule, ReactiveFormsModule],
        });
        fixture = TestBed.createComponent(TrDetailComponent);
        component = fixture.componentInstance;
        component.attribute = { ...ATTRIBUTE };
        component.outputFamilyList = [...OUTPUT_FAMILY_LIST];
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('getDetailRendererConfigForm() should return ', () => {
        expect(component.getDetailRendererConfigForm()).toBeInstanceOf(
            UntypedFormGroup
        );
    });

    it('detailRendererOnChange() should set detail_renderer to null and call form.setControl', () => {
        let spy = jest.spyOn(component.form, 'setControl');
        component.form.controls['detail_renderer'].setValue('');
        expect(component.form.controls['detail_renderer'].value).toEqual('');
        component.detailRendererOnChange();
        expect(component.form.controls['detail_renderer'].value).toEqual(null);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('detailRendererOnChange() should set detail_renderer to null and call form.setControl', () => {
        let spy = jest.spyOn(fromRenders.DetailRendererFormFactory, 'create');
        component.form.controls['detail_renderer'].setValue('test');
        component.detailRendererOnChange();
        expect(spy).toHaveBeenCalledWith('test');
    });

    it('detailOutputCategoryOnChange() should set form.id_detail_output_category to null', () => {
        component.form.controls['id_detail_output_category'].setValue('');
        expect(
            component.form.controls['id_detail_output_category'].value
        ).toEqual('');
        component.detailOutputCategoryOnChange();
        expect(
            component.form.controls['id_detail_output_category'].value
        ).toEqual(null);
    });

    it('getOutputFamilyLabel(idOutputFamilly: number) should return test1', () => {
        let result: string = component.getOutputFamilyLabel(2);
        expect(result).toEqual(OUTPUT_FAMILY_LIST[1].label);
    });

    it('submit() should emit with attribute and form.value', () => {
        let spy = jest.spyOn(component.save, 'emit');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({
            ...component.attribute,
            ...component.form.value,
            id_detail_output_category: '1_2',
        });
    });
});
