/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, ViewChildren, QueryList, Input, Output, EventEmitter } from '@angular/core';

import { Attribute, Dataset, OutputFamily } from 'src/app/metamodel/models';
import { TrOutputComponent } from './tr-output.component';

@Component({
    selector: 'app-table-output',
    templateUrl: 'table-output.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TableOutputComponent {
    @Input() dataset: Dataset;
    @Input() attributeList: Attribute[];
    @Input() outputFamilyList: OutputFamily[];
    @Output() save: EventEmitter<Attribute> = new EventEmitter();
    @ViewChildren(TrOutputComponent) private trOutputList: QueryList<TrOutputComponent>;

    saveAll() {
        this.trOutputList.forEach(trOutput => {
            if (trOutput.form.dirty && trOutput.form.valid) {
                trOutput.submit();
            }
        });
    }

    saveAllDisabled() {
        let disabled = true;
        if (this.trOutputList) {
            disabled = this.trOutputList.filter(trOutput => trOutput.form.dirty && trOutput.form.valid).length === 0;
        }
        return disabled;
    }
}
