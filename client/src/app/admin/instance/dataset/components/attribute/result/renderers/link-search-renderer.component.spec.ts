/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { LinkSearchRendererComponent } from './link-search-renderer.component';

describe('[admin][instance][dataset][components][attribute][result][renderers] LinkSearchRendererComponent', () => {
    let component: LinkSearchRendererComponent;
    let fixture: ComponentFixture<LinkSearchRendererComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                LinkSearchRendererComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(LinkSearchRendererComponent);
        component = fixture.componentInstance;
        component.form = new UntypedFormGroup({
            dataset: new UntypedFormControl(),
            attributes: new UntypedFormControl(),
            criteria: new UntypedFormControl(),
            display: new UntypedFormControl(),
            text: new UntypedFormControl(),
            icon: new UntypedFormControl()
        });
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
