/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, OnInit, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';

import { Option } from 'src/app/metamodel/models';

@Component({
    selector: 'app-option-list',
    templateUrl: 'option-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class OptionListComponent implements OnInit {
    @Input() form: UntypedFormArray;
    @Input() optionList: Option[];
    @Input() attributeDistinctList: string[];
    @Input() attributeDistinctListIsLoading: boolean;
    @Input() attributeDistinctListIsLoaded: boolean;
    @Output() loadAttributeDistinctList: EventEmitter<{}> = new EventEmitter();

    newOptionFormGroup: UntypedFormGroup;

    ngOnInit() {
        if (this.form.controls.length < 1 && this.optionList && this.optionList.length > 0) {
            for (const option of this.optionList) {
                const optionForm = this.buildFormGroup();
                optionForm.patchValue(option);
                this.form.push(optionForm);
            }
        }
        this.newOptionFormGroup = this.buildFormGroup();
    }

    buildFormGroup() {
        return new UntypedFormGroup({
            label: new UntypedFormControl('', [Validators.required]),
            value: new UntypedFormControl('', [Validators.required]),
            display: new UntypedFormControl('', [Validators.required]),
        });
    }

    getOptionFormGroup(option) {
        return option as UntypedFormGroup;
    }

    addOption() {
        this.form.push(this.newOptionFormGroup);
        this.newOptionFormGroup = this.buildFormGroup();
        this.form.markAsDirty();
    }

    removeOption(index: number) {
        this.form.removeAt(index);
        this.form.markAsDirty();
    }

    generateOptionList(attributeDistinctList: string[]) {
        this.form.clear();
        for (let i = 0; i < attributeDistinctList.length; i++) {
            const optionForm = this.buildFormGroup();
            optionForm.patchValue({
                label: attributeDistinctList[i],
                value: attributeDistinctList[i],
                display: (i + 1) * 10
            });
            this.form.push(optionForm);
        }
        this.form.markAsDirty();
    }
}
