/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FileListComponent } from './file-list.component';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Dataset, Instance } from 'src/app/metamodel/models';
import { FileInfo } from 'src/app/admin/store/models';

describe('[admin][instance][dataset][components][file] FileListComponent', () => {
    let component: FileListComponent;
    let fixture: ComponentFixture<FileListComponent>;

    @Component({
        selector: 'app-add-file',
        template: '',
    })
    class AddFileComponent {
        @Input() nextFileId: number;
        @Input() instance: Instance;
        @Input() dataset: Dataset;
        @Input() files: FileInfo[];
        @Input() filesIsLoading: boolean;
        @Input() filesIsLoaded: boolean;
        @Output() loadRootDirectory: EventEmitter<string> = new EventEmitter();
        @Output() add: EventEmitter<File> = new EventEmitter();
    }

    @Component({
        selector: 'app-edit-file',
        template: '',
    })
    class EditFileComponent {
        @Input() file: File;
        @Input() instance: Instance;
        @Input() dataset: Dataset;
        @Input() files: FileInfo[];
        @Input() filesIsLoading: boolean;
        @Input() filesIsLoaded: boolean;
        @Output() loadRootDirectory: EventEmitter<string> = new EventEmitter();
        @Output() edit: EventEmitter<File> = new EventEmitter();
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                FileListComponent,
                AddFileComponent,
                EditFileComponent,
            ],
            imports: [BrowserAnimationsModule, ReactiveFormsModule],
        });
        fixture = TestBed.createComponent(FileListComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
