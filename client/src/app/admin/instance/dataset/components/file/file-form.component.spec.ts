/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FileInfo } from 'src/app/admin/store/models';
import { Dataset, Instance } from 'src/app/metamodel/models';
import { FileFormComponent } from './file-form.component';
import { Component, EventEmitter, Input, Output } from '@angular/core';

describe('[admin][instance][dataset][components][file] FileFormComponent', () => {
    let component: FileFormComponent;
    let fixture: ComponentFixture<FileFormComponent>;

    @Component({
        selector: 'app-path-select-form-control',
        template: '',
    })
    class PathSelectFormControlComponent {
        @Input() form: UntypedFormGroup;
        @Input() disabled: boolean = false;
        @Input() controlName: string;
        @Input() controlLabel: string;
        @Input() files: FileInfo[];
        @Input() filesIsLoading: boolean;
        @Input() filesIsLoaded: boolean;
        @Input() selectType: string;
        @Output() loadDirectory: EventEmitter<string> = new EventEmitter();
        @Output() select: EventEmitter<FileInfo> = new EventEmitter();
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [FileFormComponent, PathSelectFormControlComponent],
            imports: [BrowserAnimationsModule, ReactiveFormsModule],
        });
        fixture = TestBed.createComponent(FileFormComponent);
        component = fixture.componentInstance;
        component.file = {
            id: 1,
            file_path: 'test',
            file_size: 1000,
            label: 'test',
            type: 'test',
        };
        let instance: Instance;
        let dataset: Dataset;
        component.instance = { ...instance, data_path: 'test3' };
        component.dataset = { ...dataset, data_path: 'test_dataset' };
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('onChangeFileSelect(path: string) should emit instance.data_path, dataset.data_path and path', () => {
        let spy = jest.spyOn(component.loadRootDirectory, 'emit');
        let path: string = 'test1';
        component.onChangeFileSelect(path);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(
            component.instance.data_path + component.dataset.data_path + path
        );
    });

    it(' onFileSelect(fileInfo: FileInfo) should set form.file_size to 2000', () => {
        expect(component.form.controls['file_size'].value).toEqual(1000);
        let fileInfo: FileInfo = {
            mimetype: 'test',
            name: 'test',
            size: 2000,
            type: 'test',
        };
        component.onFileSelect(fileInfo);
        expect(component.form.controls['file_size'].value).toEqual(2000);
    });

    it('submit() should emit file and form.getRawValue()', () => {
        let spy = jest.spyOn(component.onSubmit, 'emit');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({
            ...component.file,
            ...component.form.getRawValue(),
        });
    });

    it('submit() should emit only form.getRawValue()', () => {
        let spy = jest.spyOn(component.onSubmit, 'emit');
        component.file = null;
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.form.getRawValue() });
    });
});
