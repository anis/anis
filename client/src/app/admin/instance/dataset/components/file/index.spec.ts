/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { fileComponents } from './index';

describe('[admin][instance][dataset][components][file] index', () => {
    it('should test file index components', () => {
        expect(fileComponents.length).toEqual(4);
    });
});
