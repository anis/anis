/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { Instance, Dataset } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';
import { getHost } from 'src/app/shared/utils';

@Component({
    selector: 'app-dataset-card',
    templateUrl: 'dataset-card.component.html',
    styleUrls: [ 'dataset-card.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetCardComponent {
    @Input() instance: Instance;
    @Input() dataset: Dataset;
    @Output() downloadExport: EventEmitter<{url: string, filename: string }> = new EventEmitter();
    @Output() deleteDataset: EventEmitter<Dataset> = new EventEmitter();

    constructor(private appConfig: AppConfigService) { }

    getExportDatasetUrl() {
        return `${getHost(this.appConfig.apiUrl)}/instance/${this.instance.name}/dataset/${this.dataset.name}/export`;
    }

    download(event, url: string) {
        event.preventDefault();

        const timeElapsed = Date.now();
        const today = new Date(timeElapsed);
        const filename = `export_dataset_${this.instance.name}_${this.dataset.name}_${today.toISOString()}.json`;

        this.downloadExport.emit({ url, filename });
    }
}
