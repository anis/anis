/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';

import { Dataset, Instance } from 'src/app/metamodel/models';
import { DatasetCardComponent } from './dataset-card.component';
import { AppConfigService } from 'src/app/app-config.service';

describe('[admin][instance][dataset][components][dataset] DatasetCardComponent', () => {
    let component: DatasetCardComponent;
    let fixture: ComponentFixture<DatasetCardComponent>;
    const appConfigServiceStub = new AppConfigService();
    appConfigServiceStub.apiUrl = 'http://localhost:8080';

    @Component({
        selector: 'app-delete-btn',
        template: '',
    })
    class DeleteBtnComponent {
        @Input() type: string;
        @Input() label: string;
        @Input() disabled: boolean = false;
        @Output() confirm: EventEmitter<{}> = new EventEmitter();
        @Output() abort: EventEmitter<{}> = new EventEmitter();
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [DatasetCardComponent, DeleteBtnComponent],
            imports: [
                BrowserAnimationsModule,
                RouterTestingModule.withRoutes([
                    { path: 'test', component: DeleteBtnComponent },
                ]),
            ],
            providers: [
                { provide: AppConfigService, useValue: appConfigServiceStub },
            ],
        });
        fixture = TestBed.createComponent(DatasetCardComponent);
        component = fixture.componentInstance;
        let dataset: Dataset;
        let instance: Instance;
        component.dataset = { ...dataset, label: 'test' };
        component.instance = { ...instance, name: 'test' };
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should call the #download method properly', () => {
        const event = { preventDefault: jest.fn() }
        const spyEvent = jest.spyOn(event, 'preventDefault');
        const spyDownload = jest.spyOn(component.downloadExport, 'emit')
        component.download(event, 'coconut');
        expect(spyDownload).toHaveBeenCalled();
        expect(spyEvent).toHaveBeenCalled();
    })
});
