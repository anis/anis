/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportDatasetFormComponent } from './import-dataset-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { By } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

describe('[admin][instance][dataset][components][dataset] ImportDatasetFormComponent', () => {
    let component: ImportDatasetFormComponent;
    let fixture: ComponentFixture<ImportDatasetFormComponent>;

    @Component({
        selector: 'app-monaco-editor',
        template: '',
    })
    class MonacoEditorComponent {
        @Input() value: string;
        @Input() codeType: string;
        @Output() onChange: EventEmitter<string> = new EventEmitter();
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [ImportDatasetFormComponent, MonacoEditorComponent],
            imports: [
                BrowserAnimationsModule,
                AccordionModule,
                ReactiveFormsModule,
            ],
        });
        fixture = TestBed.createComponent(ImportDatasetFormComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should call #editorChange method properly', () => {
        const spyFormMark = jest.spyOn(component.form, 'markAsDirty');
        component.editorChange('coconut');
        const val = component.form.value;
        expect(val['full_dataset']).toEqual('coconut');
        expect(spyFormMark).toHaveBeenCalled();
    });

    it('should call #fileChanged method properly', () => {
        const file = new File(['{"test": "test"}'], 'test.txt');
        const event = { target: { files: [file] } };

        component.fileChanged(event as any);
        expect(component.customFileLabel).toEqual('test.txt');
    });

    it('should call #submit method properly', () => {
        component.form.patchValue({
            full_dataset: '{"test": "a sample JSON"}',
        });
        const spySubmit = jest.spyOn(component.onSubmit, 'emit');
        component.submit();
        expect(spySubmit).toHaveBeenCalled();
    });
});
