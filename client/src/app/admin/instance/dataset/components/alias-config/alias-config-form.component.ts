/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { AliasConfig } from 'src/app/metamodel/models';

@Component({
    selector: 'app-alias-config-form',
    templateUrl: 'alias-config-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AliasConfigFormComponent implements OnInit {
    @Input() aliasConfig: AliasConfig;
    @Input() tableListIsLoading: boolean;
    @Input() tableListIsLoaded: boolean;
    @Input() tableList: string[];
    @Output() onSubmit: EventEmitter<AliasConfig> = new EventEmitter();

    public form = new UntypedFormGroup({
        table_alias: new UntypedFormControl('', [Validators.required]),
        column_alias: new UntypedFormControl('', [Validators.required]),
        column_name: new UntypedFormControl('', [Validators.required]),
        column_alias_long: new UntypedFormControl('', [Validators.required])
    });

    ngOnInit() {
        this.form.patchValue(this.aliasConfig);
    }

    submit() {
        this.onSubmit.emit({
            ...this.form.getRawValue()
        });
        this.form.markAsPristine();
    }
}
