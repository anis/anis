/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ConeSearchConfigFormComponent } from './cone-search-config-form.component';

describe('[admin][instance][dataset][components][cone-search-config] ConeSearchConfigFormComponent', () => {
    let component: ConeSearchConfigFormComponent;
    let fixture: ComponentFixture<ConeSearchConfigFormComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConeSearchConfigFormComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(ConeSearchConfigFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('enable() should set (opened, column_ra, colomn_dec, plot_enabled) disabled property to false', () => {
        component.form.controls['enabled'].setValue(true);
        expect(component.form.controls['opened'].disabled).toEqual(true)
        expect(component.form.controls['column_ra'].disabled).toEqual(true)
        expect(component.form.controls['plot_enabled'].disabled).toEqual(true)
        expect(component.form.controls['column_dec'].disabled).toEqual(true);
        component.checkConeSearchDisableOpened();
        expect(component.form.controls['opened'].disabled).toEqual(false)
        expect(component.form.controls['column_ra'].disabled).toEqual(false)
        expect(component.form.controls['plot_enabled'].disabled).toEqual(false)
        expect(component.form.controls['column_dec'].disabled).toEqual(false);
    });

    it('should emit form.getRawValue and call makAsPristine', () => {
        let spyOnSubmit = jest.spyOn(component.onSubmit, 'emit');
        let spyForm = jest.spyOn(component.form, 'markAsPristine');
        component.submit();
        expect(spyOnSubmit).toHaveBeenCalledTimes(1);
        expect(spyOnSubmit).toHaveBeenCalledWith({ ...component.form.getRawValue() })
        expect(spyForm).toHaveBeenCalledTimes(1);
    });
});
