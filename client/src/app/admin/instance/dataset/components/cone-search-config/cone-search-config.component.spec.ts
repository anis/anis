/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import {
    Attribute,
    Component,
    EventEmitter,
    Input,
    Output,
} from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UntypedFormGroup, UntypedFormControl } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ConeSearchConfig } from 'src/app/metamodel/models';
import { ConeSearchConfigComponent } from './cone-search-config.component';
import { CONE_SEARCH_CONFIG } from 'src/test-data';

describe('[admin][instance][dataset][components][cone-search-config] ConeSearchConfigComponent', () => {
    let component: ConeSearchConfigComponent;
    let fixture: ComponentFixture<ConeSearchConfigComponent>;
    @Component({
        selector: 'app-cone-search-config-form',
        template: '',
    })
    class ConeSearchConfigFormComponent {
        @Input() coneSearchConfig: ConeSearchConfig;
        @Input() attributeList: Attribute[];
        @Output() onSubmit: EventEmitter<ConeSearchConfig> = new EventEmitter();

        public form = new UntypedFormGroup({
            enabled: new UntypedFormControl(false),
            opened: new UntypedFormControl({ value: false, disabled: true }),
            column_ra: new UntypedFormControl({ value: false, disabled: true }),
            column_dec: new UntypedFormControl({
                value: false,
                disabled: true,
            }),
            resolver_enabled: new UntypedFormControl({
                value: false,
                disabled: true,
            }),
            default_ra: new UntypedFormControl({ value: null, disabled: true }),
            default_dec: new UntypedFormControl({
                value: null,
                disabled: true,
            }),
            default_radius: new UntypedFormControl({
                value: 2.0,
                disabled: true,
            }),
            default_ra_dec_unit: new UntypedFormControl({
                value: 'degree',
                disabled: true,
            }),
            plot_enabled: new UntypedFormControl({
                value: false,
                disabled: true,
            }),
        });
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                ConeSearchConfigComponent,
                ConeSearchConfigFormComponent,
            ],
            imports: [BrowserAnimationsModule],
        });
        fixture = TestBed.createComponent(ConeSearchConfigComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('save(coneSearchConfig: ConeSearchConfig) should emit the current coneSearchConfig, coneSearchConfig in args', () => {
        let spy = jest.spyOn(component.editConeSearchConfig, 'emit');
        let coneSearchConfig: ConeSearchConfig = {
            ...CONE_SEARCH_CONFIG,
        };
        component.coneSearchConfig = coneSearchConfig;
        let param: ConeSearchConfig = { ...coneSearchConfig, opened: false };
        component.save(param);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({
            ...component.coneSearchConfig,
            ...param,
        });
    });

    it('save(coneSearchConfig: ConeSearchConfig) should emit the coneSearchConfig passed in args', () => {
        let spy = jest.spyOn(component.addConeSearchConfig, 'emit');
        let coneSearchConfig: ConeSearchConfig = {
            ...CONE_SEARCH_CONFIG,
        };
        component.save(coneSearchConfig);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(coneSearchConfig);
    });
});
