/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CriteriaFamilyFormComponent } from './criteria-family-form.component';

describe('[admin][instance][dataset][components][criteria-family] CriteriaFamilyFormComponent', () => {
    let component: CriteriaFamilyFormComponent;
    let fixture: ComponentFixture<CriteriaFamilyFormComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                CriteriaFamilyFormComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        });
        fixture = TestBed.createComponent(CriteriaFamilyFormComponent);
        component = fixture.componentInstance;
        component.criteriaFamily = { display: 10, id: 1, label: 'test', opened: false };
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it(' submit() should emit criterFamily and form.value', () => {
        let spy = jest.spyOn(component.onSubmit, 'emit');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith({ ...component.criteriaFamily, ...component.form.value });
    });
    
    it(' submit() should emit form.value', () => {
        component.criteriaFamily = null;
        let spy = jest.spyOn(component.onSubmit, 'emit');
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(component.form.value);
    });
});
