/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { CriteriaFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-criteria-family-list',
    templateUrl: 'criteria-family-list.component.html',
    styleUrls: [ 'criteria-family-list.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CriteriaFamilyListComponent {
    @Input() criteriaFamilyList: CriteriaFamily[];
    @Input() nextCriteriaFamilyId: number;
    @Output() add: EventEmitter<CriteriaFamily> = new EventEmitter();
    @Output() edit: EventEmitter<CriteriaFamily> = new EventEmitter();
    @Output() delete: EventEmitter<CriteriaFamily> = new EventEmitter();
}
