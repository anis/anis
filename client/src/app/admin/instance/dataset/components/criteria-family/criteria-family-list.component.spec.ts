/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { CriteriaFamilyListComponent } from './criteria-family-list.component';
import { CriteriaFamily } from 'src/app/metamodel/models';

describe('[admin][instance][dataset][components][criteria-family] CriteriaFamilyListComponent', () => {
    let component: CriteriaFamilyListComponent;
    let fixture: ComponentFixture<CriteriaFamilyListComponent>;

    @Component({
        selector: 'app-add-criteria-family',
        template: '',
    })
    class AddCriteriaFamilyComponent {
        @Input() nextCriteriaFamilyId: number;
        @Output() add: EventEmitter<CriteriaFamily> = new EventEmitter();
    }
    @Component({
        selector: 'app-edit-criteria-family',
        template: '',
    })
    class EditCriteriaFamilyComponent {
        @Input() criteriaFamily: CriteriaFamily;
        @Output() edit: EventEmitter<CriteriaFamily> = new EventEmitter();
    }
    @Component({
        selector: 'app-delete-btn',
        template: '',
    })
    class DeleteBtnComponent {
        @Input() type: string;
        @Input() label: string;
        @Input() disabled: boolean = false;
        @Output() confirm: EventEmitter<{}> = new EventEmitter();
        @Output() abort: EventEmitter<{}> = new EventEmitter();
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                CriteriaFamilyListComponent,
                AddCriteriaFamilyComponent,
                EditCriteriaFamilyComponent,
                DeleteBtnComponent,
            ],
            imports: [BrowserAnimationsModule],
        });
        fixture = TestBed.createComponent(CriteriaFamilyListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
