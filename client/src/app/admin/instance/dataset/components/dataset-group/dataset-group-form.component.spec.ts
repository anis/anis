/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DatasetGroupFormComponent } from './dataset-group-form.component';
import { DATASET } from 'src/test-data';

describe('[admin][instance][dataset][components][dataset-group] DatasetGroupFormComponent', () => {
    let component: DatasetGroupFormComponent;
    let fixture: ComponentFixture<DatasetGroupFormComponent>;
    let spy;
    
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetGroupFormComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ]
        });
        fixture = TestBed.createComponent(DatasetGroupFormComponent);
        component = fixture.componentInstance;
        component.datasetGroup = { ...DATASET.groups[1] };
        fixture.detectChanges();
        spy= jest.spyOn(component.onSubmit, 'emit');
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it(`should emit dataset group and form values on subumit`, () => {
       component.submit();
       expect(spy).toHaveBeenCalledTimes(1);
    });

    it(`should emit dataset group on subumit`, () => {
        component.datasetGroup = null;
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
     });
});
