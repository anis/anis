/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatasetGroupListComponent } from './dataset-group-list.component';
import { DatasetGroupFormComponent } from './dataset-group-form.component';
import { AddDatasetGroupComponent } from './add-dataset-group.component';

export const datasetGroupComponents = [
    DatasetGroupListComponent,
    DatasetGroupFormComponent,
    AddDatasetGroupComponent
];
