/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { DatasetGroup } from 'src/app/metamodel/models';

@Component({
    selector: 'app-dataset-group-list',
    templateUrl: 'dataset-group-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetGroupListComponent {
    @Input() datasetGroupList: DatasetGroup[];
    @Output() add: EventEmitter<DatasetGroup> = new EventEmitter();
    @Output() delete: EventEmitter<DatasetGroup> = new EventEmitter();
}
