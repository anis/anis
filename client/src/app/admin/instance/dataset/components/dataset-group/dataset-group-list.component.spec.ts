/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { DatasetGroupListComponent } from './dataset-group-list.component';

describe('[admin][instance][dataset][components][dataset-group] DatasetGroupListComponent', () => {
    @Component({selector: 'app-add-dataset-group', template: ''})
    class AddDatasetGroupStubComponent { }

    let component: DatasetGroupListComponent;
    let fixture: ComponentFixture<DatasetGroupListComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatasetGroupListComponent,
                AddDatasetGroupStubComponent
            ],
            imports: [
                BrowserAnimationsModule,
            ]
        });
        fixture = TestBed.createComponent(DatasetGroupListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
