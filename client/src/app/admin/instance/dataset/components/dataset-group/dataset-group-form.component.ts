/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { DatasetGroup } from 'src/app/metamodel/models';

@Component({
    selector: 'app-dataset-group-form',
    templateUrl: 'dataset-group-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetGroupFormComponent implements OnInit {
    @Input() datasetGroup: DatasetGroup;
    @Output() onSubmit: EventEmitter<DatasetGroup> = new EventEmitter();

    public form = new UntypedFormGroup({
        role: new UntypedFormControl('', [Validators.required])
    });

    ngOnInit() {
        if (this.datasetGroup) {
            this.form.patchValue(this.datasetGroup);
        }
    }

    submit() {
        if (this.datasetGroup) {
            this.onSubmit.emit({
                ...this.datasetGroup,
                ...this.form.value
            });
        } else {
            this.onSubmit.emit(this.form.value);
        }
    }
}
