/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

import { UntypedFormGroup } from '@angular/forms';

@Component({
    selector: 'app-footer-logo-form',
    templateUrl: 'footer-logo-form.component.html',
    styleUrls: [ 'footer-logo-form.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterLogoFormComponent {
    @Input() form: UntypedFormGroup;
}
