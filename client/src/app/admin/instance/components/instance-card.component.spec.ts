/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { InstanceCardComponent } from './instance-card.component';
import { AppConfigService } from 'src/app/app-config.service';
import { INSTANCE } from 'src/test-data';

describe('[admin][instance][components] InstanceCardComponent', () => {
    @Component({ selector: 'app-delete-btn', template: ''})
    class DeleteBtnStubComponent {
        @Input() type: string;
        @Input() label: string;
        @Input() disabled: boolean = false;
    }

    let component: InstanceCardComponent;
    let fixture: ComponentFixture<InstanceCardComponent>;
    const appConfigServiceStub = new AppConfigService();
    appConfigServiceStub.apiUrl = 'http://server';

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InstanceCardComponent,
                DeleteBtnStubComponent
            ],
            imports: [
                BrowserAnimationsModule,
            ],
            providers: [
                { provide: AppConfigService, useValue: appConfigServiceStub }
            ]
        })
        fixture = TestBed.createComponent(InstanceCardComponent);
        component = fixture.componentInstance;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('should call method #getExportInstanceUrl component properly', () => {
        component.instance = INSTANCE;
        const res = component.getExportInstanceUrl();
    });

    it('should call method #download properly', () => {
        component.instance = INSTANCE;
        const event = { preventDefault: jest.fn() };
        const spyDownload = jest.spyOn(component.downloadExport, 'emit');
        component.download(event, 'coconut');
        expect(spyDownload).toHaveBeenCalled();
    });
});
