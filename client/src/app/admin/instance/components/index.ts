/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InstanceCardComponent } from './instance-card.component';
import { InstanceFormComponent } from './instance-form.component';
import { ImportInstanceFormComponent } from './import-instance-form.component';
import { DesignConfigFormComponent } from './design-config-form.component';
import { FooterLogoFormComponent } from './footer-logo-form.component';
import { FooterLogosListComponent } from './footer-logos-list.component';

export const dummiesComponents = [
    InstanceCardComponent,
    InstanceFormComponent,
    ImportInstanceFormComponent,
    DesignConfigFormComponent,
    FooterLogoFormComponent,
    FooterLogosListComponent
];
