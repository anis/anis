/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Output, EventEmitter, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { MonacoEditorComponent } from 'src/app/admin/admin-shared/components/monaco-editor.component';

@Component({
    selector: 'app-import-instance-form',
    templateUrl: 'import-instance-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImportInstanceFormComponent {
    @Output() onSubmit: EventEmitter<any> = new EventEmitter();
    @ViewChild('monacoEditor', { static: true }) _monacoEditor: MonacoEditorComponent;

    customFileLabel = 'Choose a file..';

    public form = new UntypedFormGroup({
        full_instance: new UntypedFormControl(''),
    });

    fileChanged(e: Event) {
        const uploadedFile = (e.target as HTMLInputElement).files[0];
        this.customFileLabel = uploadedFile.name;
        const fileReader = new FileReader();
        fileReader.onload = (e) => {
            const json = JSON.parse(fileReader.result as string);
            this._monacoEditor.setValue(JSON.stringify(json, undefined, 4));
            this.form.markAsDirty();
        }
        fileReader.readAsText(uploadedFile);
    }

    editorChange(value: string) {
        this.form.controls['full_instance'].setValue(value);
        this.form.markAsDirty();
    }

    submit() {
        this.onSubmit.emit(JSON.parse(this.form.controls['full_instance'].value));
    }
}
