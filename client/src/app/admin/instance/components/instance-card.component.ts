/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { Instance } from 'src/app/metamodel/models';
import { AppConfigService } from 'src/app/app-config.service';
import { getHost } from 'src/app/shared/utils';

@Component({
    selector: 'app-instance-card',
    templateUrl: 'instance-card.component.html',
    styleUrls: [ 'instance-card.component.scss' ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstanceCardComponent {
    @Input() instance: Instance;
    @Output() downloadExport: EventEmitter<{url: string, filename: string }> = new EventEmitter();
    @Output() deleteInstance: EventEmitter<Instance> = new EventEmitter();

    constructor(private appConfig: AppConfigService) { }

    getExportInstanceUrl() {
        return `${getHost(this.appConfig.apiUrl)}/instance/${this.instance.name}/export`;
    }

    download(event, url: string) {
        event.preventDefault();

        const timeElapsed = Date.now();
        const today = new Date(timeElapsed);
        const filename = `export_instance_${this.instance.name}_${today.toISOString()}.json`;

        this.downloadExport.emit({ url, filename });
    }
}
