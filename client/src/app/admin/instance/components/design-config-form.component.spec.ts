/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormGroup, UntypedFormArray } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'ngx-bootstrap/accordion';

import { DesignConfigFormComponent } from './design-config-form.component';
import { Logo } from 'src/app/metamodel/models';
import { FileInfo } from 'src/app/admin/store/models';
import { INSTANCE, DESIGN_CONFIG } from 'src/test-data';

describe('[admin][instance][components] DesignConfigFormComponent', () => {
    @Component({ selector: 'app-path-select-form-control', template: ''})
    class PathSelectFormControlStubComponent {
        @Input() form: UntypedFormGroup;
        @Input() disabled: boolean = false;
        @Input() controlName: string;
        @Input() controlLabel: string;
        @Input() files: FileInfo[];
        @Input() filesIsLoading: boolean;
        @Input() filesIsLoaded: boolean;
        @Input() selectType: string;
    }

    @Component({ selector: 'app-footer-logos-list', template: ''})
    class FooterLogosListStubComponent {
        @Input() form: UntypedFormArray;
        @Input() logoList: Logo[];
    }

    let component: DesignConfigFormComponent;
    let fixture: ComponentFixture<DesignConfigFormComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DesignConfigFormComponent,
                PathSelectFormControlStubComponent,
                FooterLogosListStubComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule,
                AccordionModule
            ]
        });

        fixture = TestBed.createComponent(DesignConfigFormComponent);
        component = fixture.componentInstance;
        component.instance = { ...INSTANCE };
        component.designConfig = { ...DESIGN_CONFIG };
        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('isFilesPathEmpty() should return false', () => {
        let result = component.isFilesPathEmpty();
        expect(result).toBe(false);
    });

    it('onChangeFileSelect(path: string) should call emit on loadRootDirectory', () => {
        let spy = jest.spyOn(component.loadRootDirectory, 'emit');
        component.onChangeFileSelect('test_path');
        let param = `${component.instance.data_path}${component.instance.files_path}${'test_path'}`
        expect(spy).toHaveBeenCalledWith(param);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('getFooterLogoListByDisplay() return a array of logo sorted by display ', () => {
        let footer_logos: Logo[] = [
            { display: 4, file: 'test1', title: 'test1', href: 'test1' },
            { display: 2, file: 'test2', title: 'test2', href: 'test2' },
            { display: 3, file: 'test3', title: 'test3', href: 'test4' },
        ];
        component.designConfig.footer_logos = footer_logos;
        let result = component.getFooterLogoListByDisplay();
        let expected = [
            { display: 2, file: 'test2', title: 'test2', href: 'test2' },
            { display: 3, file: 'test3', title: 'test3', href: 'test4' },
            { display: 4, file: 'test1', title: 'test1', href: 'test1' },
        ]
        expect(result).toEqual(expected);
    });

    it('submit() should emit instance and form.getRawValue()', () => {
        let spyOnOnSubmit = jest.spyOn(component.onSubmit, 'emit');
        let spyOnForm = jest.spyOn(component.form, 'markAsPristine');
        component.submit();
        expect(spyOnOnSubmit).toHaveBeenCalledTimes(1);
        expect(spyOnOnSubmit).toHaveBeenCalledWith({ ...component.designConfig, ...component.form.getRawValue() });
        expect(spyOnForm).toHaveBeenCalledTimes(1);
    });

    it('submit() should emit  form.getRawValue()', () => {
        let spyOnOnSubmit = jest.spyOn(component.onSubmit, 'emit');
        component.instance = null;
        component.submit();
        expect(spyOnOnSubmit).toHaveBeenCalledTimes(1);
        expect(spyOnOnSubmit).toHaveBeenCalledWith({ ...component.form.getRawValue() });
    });
});
