/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormArray, FormGroup, ReactiveFormsModule, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FooterLogosListComponent } from './footer-logos-list.component';

describe('[admin][instance][Components] FooterLogosListComponent', () => {
    @Component({ selector: 'app-footer-logo-form', template: '' })
    class FooterLogoFormStubComponent {
        @Input() form: UntypedFormGroup;
    }

    let component: FooterLogosListComponent;
    let fixture: ComponentFixture<FooterLogosListComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                FooterLogosListComponent,
                FooterLogoFormStubComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ],
        })

        fixture = TestBed.createComponent(FooterLogosListComponent);
        component = fixture.componentInstance;
        component.form = new FormArray([]);
        component.logoList = [{ display: 10, file: 'test', href: 'test', title: 'test' }];
        component.newLogoFormGroup = new FormGroup({});
        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('buildFormGroup() should return an instance of UntypedFormGroup', () => {
        let result = component.buildFormGroup();
        expect(result instanceof UntypedFormGroup).toBe(true);
    });

    it('addLogo() should add newLogoFormGroup in form', () => {
        let spyOnForm = jest.spyOn(component.form, 'markAsDirty');
        let spyOnBuildFormGroup = jest.spyOn(component, 'buildFormGroup');
        expect(component.form.controls.length).toEqual(1);
        component.addLogo();
        expect(spyOnBuildFormGroup).toHaveBeenCalledTimes(1);
        expect(spyOnForm).toHaveBeenCalledTimes(1);
        expect(component.form.controls.length).toEqual(2);
    });

    it('removeLogo(index: number) should remove a logo from form', () => {
        let spyOnForm = jest.spyOn(component.form, 'markAsDirty');
        expect(component.form.controls.length).toEqual(1);
        component.removeLogo(0);
        expect(spyOnForm).toHaveBeenCalledTimes(1);
        expect(component.form.controls.length).toEqual(0);
    });
});
