/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, UntypedFormGroup } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'ngx-bootstrap/accordion';

import { InstanceFormComponent } from './instance-form.component';
import { FileInfo } from 'src/app/admin/store/models';
import { INSTANCE } from 'src/test-data';

describe('[admin][instance][components] InstanceFormComponent', () => {
    @Component({ selector: 'app-path-select-form-control', template: ''})
    class PathSelectFormControlStubComponent {
        @Input() form: UntypedFormGroup;
        @Input() disabled: boolean = false;
        @Input() controlName: string;
        @Input() controlLabel: string;
        @Input() files: FileInfo[];
        @Input() filesIsLoading: boolean;
        @Input() filesIsLoaded: boolean;
        @Input() selectType: string;
    }

    let component: InstanceFormComponent;
    let fixture: ComponentFixture<InstanceFormComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InstanceFormComponent,
                PathSelectFormControlStubComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule,
                AccordionModule
            ]
        });

        fixture = TestBed.createComponent(InstanceFormComponent);
        component = fixture.componentInstance;
        component.instance = { ...INSTANCE };
        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('isDataPathEmpty() should return false', () => {
        component.form.controls['data_path'].setValue('test');
        let result = component.isDataPathEmpty();
        expect(result).toBe(false);
    });

    it('isFilesPathEmpty() should return false', () => {
        component.form.controls['files_path'].setValue('test');
        let result = component.isFilesPathEmpty();
        expect(result).toBe(false);
    });

    it('onChangeFilesPath(path: string) should call emit on loadRootDirectory', () => {
        let spy = jest.spyOn(component.loadRootDirectory, 'emit');
        component.form.controls['data_path'].setValue('test_data_path');
        component.form.controls['files_path'].setValue('test_files_path');

        component.onChangeFilesPath('test_path');
        let param = `${component.form.controls['data_path'].value}${'test_path'}`
        expect(spy).toHaveBeenCalledWith(param);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('onChangeFileSelect(path: string) should call emit on loadRootDirectory', () => {
        let spy = jest.spyOn(component.loadRootDirectory, 'emit');
        component.onChangeFileSelect('test_path');
        let param = `${component.form.controls['data_path'].value}${component.form.controls['files_path'].value}${'test_path'}`
        expect(spy).toHaveBeenCalledWith(param);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('submit() should emit instance and form.getRawValue()', () => {
        let spyOnOnSubmit = jest.spyOn(component.onSubmit, 'emit');
        let spyOnForm = jest.spyOn(component.form, 'markAsPristine');
        component.submit();
        expect(spyOnOnSubmit).toHaveBeenCalledTimes(1);
        expect(spyOnOnSubmit).toHaveBeenCalledWith({ ...component.instance, ...component.form.getRawValue() });
        expect(spyOnForm).toHaveBeenCalledTimes(1);
    });

    it('submit() should emit  form.getRawValue()', () => {
        let spyOnOnSubmit = jest.spyOn(component.onSubmit, 'emit');
        component.instance = null;
        component.submit();
        expect(spyOnOnSubmit).toHaveBeenCalledTimes(1);
        expect(spyOnOnSubmit).toHaveBeenCalledWith({ ...component.form.getRawValue() });
    });
});
