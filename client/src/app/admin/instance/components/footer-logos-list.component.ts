/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy, OnInit, Input } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';

import { Logo } from 'src/app/metamodel/models';

@Component({
    selector: 'app-footer-logos-list',
    templateUrl: 'footer-logos-list.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterLogosListComponent implements OnInit {
    @Input() form: UntypedFormArray;
    @Input() logoList: Logo[];

    newLogoFormGroup: UntypedFormGroup;

    ngOnInit() {
        if (this.form.controls.length < 1 && this.logoList && this.logoList.length > 0) {
            for (const logo of this.logoList) {
                const logoForm = this.buildFormGroup();
                logoForm.patchValue(logo);
                this.form.push(logoForm);
            }
        }
        this.newLogoFormGroup = this.buildFormGroup();
    }

    buildFormGroup() {
        return new UntypedFormGroup({
            href: new UntypedFormControl('', [Validators.required]),
            title: new UntypedFormControl('', [Validators.required]),
            file: new UntypedFormControl('', [Validators.required]),
            display: new UntypedFormControl('', [Validators.required])
        });
    }

    getLogoFormGroup(logo) {
        return logo as UntypedFormGroup;
    }

    addLogo() {
        this.form.push(this.newLogoFormGroup);
        this.newLogoFormGroup = this.buildFormGroup();
        this.form.markAsDirty();
    }

    removeLogo(index: number) {
        this.form.removeAt(index);
        this.form.markAsDirty();
    }
}
