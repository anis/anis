/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { Instance } from 'src/app/metamodel/models';
import { FileInfo } from 'src/app/admin/store/models';

@Component({
    selector: 'app-instance-form',
    templateUrl: 'instance-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstanceFormComponent implements OnInit {
    @Input() instance: Instance;
    @Input() files: FileInfo[];
    @Input() filesIsLoading: boolean;
    @Input() filesIsLoaded: boolean;
    @Output() loadRootDirectory: EventEmitter<string> = new EventEmitter();
    @Output() onSubmit: EventEmitter<Instance> = new EventEmitter();

    public form = new UntypedFormGroup({
        name: new UntypedFormControl('', [Validators.required]),
        label: new UntypedFormControl('', [Validators.required]),
        description: new UntypedFormControl('', [Validators.required]),
        scientific_manager: new UntypedFormControl('', [Validators.required]),
        instrument: new UntypedFormControl('', [Validators.required]),
        wavelength_domain: new UntypedFormControl('', [Validators.required]),
        display: new UntypedFormControl('', [Validators.required]),
        data_path: new UntypedFormControl(''),
        files_path: new UntypedFormControl(''),
        public: new UntypedFormControl(true, [Validators.required]),
        portal_logo: new UntypedFormControl(''),
        portal_color: new UntypedFormControl('#7AC29A', [Validators.required]),
        default_redirect: new UntypedFormControl('')
    });

    ngOnInit() {
        if (this.instance) {
            this.form.patchValue(this.instance);
            this.form.controls['name'].disable();
        }
    }

    isDataPathEmpty() {
        return this.form.controls['data_path'].value == '';
    }

    isFilesPathEmpty() {
        return this.form.controls['files_path'].value == '';
    }

    onChangeFilesPath(path: string) {
        this.loadRootDirectory.emit(`${this.form.controls['data_path'].value}${path}`)
    }

    onChangeFileSelect(path: string) {
        this.loadRootDirectory.emit(`${this.form.controls['data_path'].value}${this.form.controls['files_path'].value}${path}`);
    }

    submit() {
        if (this.instance) {
            this.onSubmit.emit({
                ...this.instance,
                ...this.form.getRawValue()
            });
            this.form.markAsPristine();
        } else {
            this.onSubmit.emit(this.form.getRawValue());
        }
    }
}
