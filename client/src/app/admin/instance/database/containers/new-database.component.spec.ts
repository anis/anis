/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { Instance } from 'src/app/metamodel/models';
import * as databaseActions from 'src/app/metamodel/actions/database.actions';
import { NewDatabaseComponent } from './new-database.component';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { INSTANCE, DATABASE } from 'src/test-data';

describe('[admin][Database][Containers] NewDatabaseComponent', () => {
    @Component({ selector: 'app-database-form', template: '' })
    class DatabaseStubForm{}

    let component: NewDatabaseComponent;
    let fixture: ComponentFixture<NewDatabaseComponent>;
    let store: MockStore;
    let mockInstanceSelectorInstance;
    let instance: Instance;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                NewDatabaseComponent,
                DatabaseStubForm
            ],
            imports: [
                BrowserAnimationsModule,
                RouterTestingModule
            ],
            providers: [
                provideMockStore({})
            ]
        });
        fixture = TestBed.createComponent(NewDatabaseComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        instance = { ...INSTANCE };
        mockInstanceSelectorInstance = store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('#addNewDatabase(database) should dispatch addDatabase action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        let database = { ...DATABASE };
        component.addNewDatabase(database);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(databaseActions.addDatabase({database}));
    });
});
