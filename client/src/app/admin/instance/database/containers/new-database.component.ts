/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Instance, Database } from 'src/app/metamodel/models';
import * as databaseActions from 'src/app/metamodel/actions/database.actions';
import * as databaseSelector from 'src/app/metamodel/selectors/database.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';

@Component({
    selector: 'app-new-database',
    templateUrl: 'new-database.component.html'
})
export class NewDatabaseComponent {
    public instance: Observable<Instance>;
    public databaseListIsLoading: Observable<boolean>;
    public databaseListIsLoaded: Observable<boolean>;
    public nextDatabaseId: Observable<number>;

    constructor(private store: Store<{ }>) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.databaseListIsLoading = store.select(databaseSelector.selectDatabaseListIsLoading);
        this.databaseListIsLoaded = store.select(databaseSelector.selectDatabaseListIsLoaded);
        this.nextDatabaseId = store.select(databaseSelector.selectNextDatabaseId);
    }

    addNewDatabase(database: Database) {
        this.store.dispatch(databaseActions.addDatabase({ database }));
    }
}
