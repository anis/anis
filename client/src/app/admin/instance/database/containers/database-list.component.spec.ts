/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { Instance } from 'src/app/metamodel/models';
import * as databaseActions from 'src/app/metamodel/actions/database.actions';
import { DatabaseListComponent } from './database-list.component';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { INSTANCE, DATABASE } from 'src/test-data';

describe('[admin][Database][Containers] DatabaseListComponent', () => {
    let component : DatabaseListComponent;
    let fixture : ComponentFixture<DatabaseListComponent>;
    let store: MockStore;
    let mockInstanceSelectorInstance;
    let instance: Instance;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                DatabaseListComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                RouterTestingModule
            ],
            providers: [
                provideMockStore({})
            ]
        });
        fixture = TestBed.createComponent(DatabaseListComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        instance = { ...INSTANCE };
        mockInstanceSelectorInstance = store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#deleteDatabase(database) should dispatch deletedatabase action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        let database = { ...DATABASE };
        component.deleteDatabase(database);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(databaseActions.deleteDatabase({database}));
    });
});
