/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { Database } from 'src/app/metamodel/models';
import { atLeastOneFilledValidator } from 'src/app/shared/validators/at-least-one-filled.validator';

@Component({
    selector: 'app-database-form',
    templateUrl: 'database-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatabaseFormComponent implements OnInit {
    @Input() nextDatabaseId: number;
    @Input() database: Database;
    @Output() onSubmit: EventEmitter<Database> = new EventEmitter();

    public form = new UntypedFormGroup({
        label: new UntypedFormControl('', [Validators.required]),
        dbname: new UntypedFormControl('', []),
        dbtype: new UntypedFormControl('', []),
        dbhost: new UntypedFormControl('', []),
        dbport: new UntypedFormControl('', []),
        dblogin: new UntypedFormControl('', []),
        dbpassword: new UntypedFormControl('', []),
        dbdsn_file: new UntypedFormControl('', [])
    }, {
        validators: [atLeastOneFilledValidator([['dbname', 'dbtype', 'dbhost', 'dbport', 'dblogin', 'dbpassword'], ['dbdsn_file']])]
    });

    ngOnInit() {
        if (this.database) {
            this.form.patchValue(this.database);
        }
    }

    submit() {
        if (this.database) {
            this.onSubmit.emit({
                ...this.database,
                ...this.form.value
            });
        } else {
            this.onSubmit.emit({ id: this.nextDatabaseId, ...this.form.value });
        }
    }
}
