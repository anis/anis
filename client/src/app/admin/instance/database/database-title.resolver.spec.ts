/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold, hot } from 'jasmine-marbles';

import * as databaseActions from 'src/app/metamodel/actions/database.actions';
import * as databaseSelector from 'src/app/metamodel/selectors/database.selector';
import { Database } from 'src/app/metamodel/models';
import { DatabaseTitleResolver } from './database-title.resolver';

describe('[Database] DatabaseTitleResolver', () => {
    let databaseTitleResolver: DatabaseTitleResolver;
    let store: MockStore;
    let mockDatabaseSelectorDatabaseListIsLoaded;
    let mockDatabaseSelectorDatabaseByRouteId;
    let database: Database = {
        id: 1,
        dbhost: 'test',
        dblogin: 'test@test.fr',
        dbname: 'test',
        dbpassword: 'test',
        dbport: 808080,
        dbtype: '',
        label: 'test',
        'dbdsn_file': '/mnt/dbpassword'
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                DatabaseTitleResolver,
                provideMockStore({}),
            ]
        })

        store = TestBed.inject(MockStore);
        databaseTitleResolver = TestBed.inject(DatabaseTitleResolver);
        mockDatabaseSelectorDatabaseListIsLoaded = store.overrideSelector(databaseSelector.selectDatabaseListIsLoaded, false);
        mockDatabaseSelectorDatabaseByRouteId = store.overrideSelector(databaseSelector.selectDatabaseByRouteId, database);
    });

    it('should be created', () => {
        expect(databaseTitleResolver).toBeTruthy();
    });

    it('should dispatch databaseActions loadDatabaseList action and return databaseListIsLoaded', () => {
        const expected = cold('a', { a: [] });
        let spy = jest.spyOn(store, 'dispatch');
        let result = hot('a', { a: databaseTitleResolver.resolve(null, null) });
        expect(result).toBeObservable(expected);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(databaseActions.loadDatabaseList());
    });
    
    it('should return "Edit database test"', () => {
        mockDatabaseSelectorDatabaseListIsLoaded = store.overrideSelector(databaseSelector.selectDatabaseListIsLoaded, true);
        let result = databaseTitleResolver.resolve(null, null);
        const expected = cold('a', { a: 'Edit database test' });
        expect(result).toBeObservable(expected);
    });
});
