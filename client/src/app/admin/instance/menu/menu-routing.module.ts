/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MenuItemListComponent } from './containers/menu-item-list.component';
import { NewWebpageComponent } from './containers/new-webpage.component';
import { EditWebpageComponent } from './containers/edit-webpage.component';
import { NewMenuFamilyComponent } from './containers/new-menu-family.component';
import { EditMenuFamilyComponent } from './containers/edit-menu-family.component';
import { NewUrlComponent } from './containers/new-url.component';
import { EditUrlComponent } from './containers/edit-url.component';
import { MenuFamilyComponent } from './containers/menu-family.component';
import { MenuFamilyItemListComponent } from './containers/menu-family-item-list.component';
import { MenuFamilyNewWebpageComponent } from './containers/menu-family-new-webpage.component';
import { MenuFamilyNewUrlComponent } from './containers/menu-family-new-url.component';
import { MenuFamilyEditWebpageComponent } from './containers/menu-family-edit-webpage.component';
import { MenuFamilyEditUrlComponent } from './containers/menu-family-edit-url.component';

const routes: Routes = [
    { path: '', component: MenuItemListComponent, title: 'Menu item list' },
    { path: 'new-webpage', component: NewWebpageComponent, title: 'New webpage' },
    { path: 'edit-webpage/:id', component: EditWebpageComponent, title: 'Edit webpage'},
    { path: 'new-menu-family', component: NewMenuFamilyComponent, title: 'New menu family' },
    { path: 'edit-menu-family/:id', component: EditMenuFamilyComponent, title: 'New menu family' },
    { path: 'new-url', component: NewUrlComponent, title: 'New URL' },
    { path: 'edit-url/:id', component: EditUrlComponent, title: 'Edit URL' },
    { path: 'menu-family-item-list/:id', component: MenuFamilyComponent, children:
        [
            { path: '', component: MenuFamilyItemListComponent, title: 'Menu family items' },
            { path: 'new-webpage', component: MenuFamilyNewWebpageComponent, title: 'New webpage'  },
            { path: 'new-url', component: MenuFamilyNewUrlComponent, title: 'New URL' },
            { path: 'edit-webpage/:fid', component: MenuFamilyEditWebpageComponent, title: 'Edit webpage' },
            { path: 'edit-url/:fid', component: MenuFamilyEditUrlComponent, title: 'Edit URL' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MenuRoutingModule { }

export const routedComponents = [
    MenuItemListComponent,
    NewWebpageComponent,
    EditWebpageComponent,
    NewMenuFamilyComponent,
    EditMenuFamilyComponent,
    NewUrlComponent,
    EditUrlComponent,
    MenuFamilyComponent,
    MenuFamilyItemListComponent,
    MenuFamilyNewWebpageComponent ,
    MenuFamilyNewUrlComponent,
    MenuFamilyEditWebpageComponent,
    MenuFamilyEditUrlComponent
];
