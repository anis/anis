/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, ComponentFixture } from '@angular/core/testing';
import { EditMenuFamilyComponent } from './edit-menu-family.component';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { MenuFamily } from 'src/app/metamodel/models';
import * as menuItemsActions from 'src/app/metamodel/actions/menu-item.actions';
import { MENU_FAMILY } from 'src/test-data';

describe('[Admin][Instance][Menu][Containers] EditMenuFamilyComponent', () => {
    let fixture: ComponentFixture<EditMenuFamilyComponent>;
    let component: EditMenuFamilyComponent;
    let store: MockStore;

    afterEach(() => {
        store.resetSelectors();
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [EditMenuFamilyComponent],
            providers: [provideMockStore({})],
        });
        fixture = TestBed.createComponent(EditMenuFamilyComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#editMenuFamily() should dispatch addMenuItem action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        const menu: MenuFamily = { ...MENU_FAMILY };
        component.editMenuFamily(menu);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(
            menuItemsActions.editMenuItem({ menuItem: menu })
        );
    });
});
