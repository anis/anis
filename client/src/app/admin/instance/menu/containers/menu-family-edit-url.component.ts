/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Instance, Url } from 'src/app/metamodel/models';
import * as menuFamilyItemsActions from 'src/app/metamodel/actions/menu-family-item.actions';
import * as menuFamilyItemsSelector from 'src/app/metamodel/selectors/menu-family-item.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';

@Component({
    selector: 'app-menu-family-edit-url',
    templateUrl: 'menu-family-edit-url.component.html'
})
export class MenuFamilyEditUrlComponent {
    public instance: Observable<Instance>;
    public url: Observable<Url>;

    constructor(private store: Store<{ }>) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.url = store.select(menuFamilyItemsSelector.selectUrlByRouteId);
    }

    editUrl(url: Url) {
        this.store.dispatch(menuFamilyItemsActions.editMenuFamilyItem({ menuFamilyItem: url }));
    }
}
