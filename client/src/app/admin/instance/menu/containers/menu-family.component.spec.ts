/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, ComponentFixture } from '@angular/core/testing';
import { MenuFamilyComponent } from './menu-family.component';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

import * as menuFamilyItemsActions from 'src/app/metamodel/actions/menu-family-item.actions';
import { MENU_ITEM_LIST } from 'src/test-data';
import { MenuItem } from 'src/app/metamodel/models';
import { Component, EventEmitter, Input, Output } from '@angular/core';

describe('[Admin][Instance][Menu][Containers] MenuFamilyComponent', () => {
    let fixture: ComponentFixture<MenuFamilyComponent>;
    let component: MenuFamilyComponent;
    let store: MockStore;

    @Component({
        selector: 'app-menu-item-list-table',
        template: '',
    })
    class MenuItemListTableComponent {
        @Input() menuItemList: MenuItem[];
        @Output() deleteMenuItem: EventEmitter<MenuItem> = new EventEmitter();
    }

    afterEach(() => {
        store.resetSelectors();
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [MenuFamilyComponent, MenuItemListTableComponent],
            providers: [provideMockStore({})],
        });
        fixture = TestBed.createComponent(MenuFamilyComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.ngOnInit();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(
            menuFamilyItemsActions.loadMenuFamilyItemList()
        );
    });
});
