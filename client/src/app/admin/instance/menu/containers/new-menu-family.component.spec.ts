/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, ComponentFixture } from '@angular/core/testing';
import { NewMenuFamilyComponent } from './new-menu-family.component';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { MenuFamily } from 'src/app/metamodel/models';
import * as menuItemsActions from 'src/app/metamodel/actions/menu-item.actions';
import { MENU_FAMILY } from 'src/test-data';

describe('[Admin][Instance][Menu][Containers] NewMenuFamilyComponent', () => {
    let fixture: ComponentFixture<NewMenuFamilyComponent>;
    let component: NewMenuFamilyComponent;
    let store: MockStore;

    afterEach(() => {
        store.resetSelectors();
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [NewMenuFamilyComponent],
            providers: [provideMockStore({})],
        });
        fixture = TestBed.createComponent(NewMenuFamilyComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#addNewMenuFamily() should dispatch addMenuItem action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        const menu: MenuFamily = { ...MENU_FAMILY };
        component.addNewMenuFamily(menu);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(
            menuItemsActions.addMenuItem({ menuItem: menu })
        );
    });
});
