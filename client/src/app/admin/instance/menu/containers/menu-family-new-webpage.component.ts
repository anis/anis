/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Instance, Webpage } from 'src/app/metamodel/models';
import * as menuFamilyItemActions from 'src/app/metamodel/actions/menu-family-item.actions';
import * as menuFamilyItemSelector from 'src/app/metamodel/selectors/menu-family-item.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as monacoEditorActions from 'src/app/admin/store/actions/monaco-editor.actions';
import * as monacoEditorSelector from 'src/app/admin/store/selectors/monaco-editor.selector';

@Component({
    selector: 'app-menu-family-new-webpage',
    templateUrl: 'menu-family-new-webpage.component.html'
})
export class MenuFamilyNewWebpageComponent implements OnInit {
    public instance: Observable<Instance>;
    public nextMenuFamilyItemId: Observable<number>;
    public monacoEditorIsLoading: Observable<boolean>;
    public monacoEditorIsLoaded: Observable<boolean>;

    constructor(private store: Store<{ }>) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.nextMenuFamilyItemId = store.select(menuFamilyItemSelector.selectNextMenuFamilyItemId);
        this.monacoEditorIsLoading = store.select(monacoEditorSelector.selectMonacoEditorIsLoading);
        this.monacoEditorIsLoaded = store.select(monacoEditorSelector.selectMonacoEditorIsLoaded);
    }

    ngOnInit(): void {
        Promise.resolve(null).then(() => this.store.dispatch(monacoEditorActions.loadMonacoEditor()));
    }

    addNewWebpage(webpage: Webpage) {
        this.store.dispatch(menuFamilyItemActions.addMenuFamilyItem({ menuFamilyItem: webpage }));
    }
}
