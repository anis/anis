/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Webpage, Instance } from 'src/app/metamodel/models';
import * as menuItemsActions from 'src/app/metamodel/actions/menu-item.actions';
import * as menuItemsSelector from 'src/app/metamodel/selectors/menu-item.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as monacoEditorActions from 'src/app/admin/store/actions/monaco-editor.actions';
import * as monacoEditorSelector from 'src/app/admin/store/selectors/monaco-editor.selector';

@Component({
    selector: 'app-edit-webpage',
    templateUrl: 'edit-webpage.component.html'
})
export class EditWebpageComponent implements OnInit {
    public instance: Observable<Instance>;
    public menuItemListIsLoading: Observable<boolean>;
    public menuItemListIsLoaded: Observable<boolean>;
    public webpage: Observable<Webpage>;
    public monacoEditorIsLoading: Observable<boolean>;
    public monacoEditorIsLoaded: Observable<boolean>;

    constructor(private store: Store<{ }>) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.menuItemListIsLoading = store.select(menuItemsSelector.selectMenuItemListIsLoading);
        this.menuItemListIsLoaded = store.select(menuItemsSelector.selectMenuItemListIsLoaded);
        this.webpage = store.select(menuItemsSelector.selectWebpageByRouteId);
        this.monacoEditorIsLoading = store.select(monacoEditorSelector.selectMonacoEditorIsLoading);
        this.monacoEditorIsLoaded = store.select(monacoEditorSelector.selectMonacoEditorIsLoaded);
    }

    ngOnInit(): void {
        Promise.resolve(null).then(() => this.store.dispatch(monacoEditorActions.loadMonacoEditor()));
    }

    editWebpage(webpage: Webpage) {
        this.store.dispatch(menuItemsActions.editMenuItem({ menuItem: webpage }));
    }
}
