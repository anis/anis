/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, ComponentFixture } from '@angular/core/testing';
import { EditWebpageComponent } from './edit-webpage.component';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { Webpage } from 'src/app/metamodel/models/webpage.model';
import * as menuItemsActions from 'src/app/metamodel/actions/menu-item.actions';
import { WEBPAGE } from 'src/test-data';

describe('[Admin][Instance][Menu][Containers] EditWebpageComponent', () => {
    let fixture: ComponentFixture<EditWebpageComponent>;
    let component: EditWebpageComponent;
    let store: MockStore;

    afterEach(() => {
        store.resetSelectors();
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [EditWebpageComponent],
            providers: [provideMockStore({})],
        });
        fixture = TestBed.createComponent(EditWebpageComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', (done) => {
        const spy = jest.spyOn(store, 'dispatch');
        component.ngOnInit();
        Promise.resolve(null).then(function () {
            expect(spy).toHaveBeenCalledTimes(1);
            done();
        });
    });

    it('#editWebpage() should dispatch addMenuItem action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        const webpage: Webpage = { ...WEBPAGE };
        component.editWebpage(webpage);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(
            menuItemsActions.editMenuItem({ menuItem: webpage })
        );
    });
});
