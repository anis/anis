/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { MenuItem, Instance } from 'src/app/metamodel/models';
import * as menuFamilyItemActions from 'src/app/metamodel/actions/menu-family-item.actions';
import * as menuFamilyItemSelector from 'src/app/metamodel/selectors/menu-family-item.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';

@Component({
    selector: 'app-menu-family-item-list',
    templateUrl: 'menu-family-item-list.component.html'
})
export class MenuFamilyItemListComponent {
    public instance: Observable<Instance>;
    public menuFamilyItemList: Observable<MenuItem[]>;

    constructor(private store: Store<{ }>) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.menuFamilyItemList = store.select(menuFamilyItemSelector.selectAllMenuFamilyItems);
    }

    deleteMenuFamilyItem(menuFamilyItem: MenuItem) {
        this.store.dispatch(menuFamilyItemActions.deleteMenuFamilyItem({ menuFamilyItem }));
    }
}
