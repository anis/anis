/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, ComponentFixture } from '@angular/core/testing';
import { MenuFamilyEditUrlComponent } from './menu-family-edit-url.component';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { Url } from 'src/app/metamodel/models';
import * as menuFamilyItemsActions from 'src/app/metamodel/actions/menu-family-item.actions';
import { URL } from 'src/test-data';
import { Component, EventEmitter, Input, Output } from '@angular/core';

describe('[Admin][Instance][Menu][Containers] MenuFamilyEditUrlComponent', () => {
    let fixture: ComponentFixture<MenuFamilyEditUrlComponent>;
    let component: MenuFamilyEditUrlComponent;
    let store: MockStore;

    @Component({
        selector: 'app-url-form',
        template: '',
    })
    class UrlFormComponent {
        @Input() nextMenuItemId: number;
        @Input() url: Url;
        @Output() onSubmit: EventEmitter<Url> = new EventEmitter();
    }

    afterEach(() => {
        store.resetSelectors();
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [MenuFamilyEditUrlComponent, UrlFormComponent],
            providers: [provideMockStore({})],
        });
        fixture = TestBed.createComponent(MenuFamilyEditUrlComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#addNewUrl() should dispatch addMenuItem action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        const url: Url = { ...URL };
        component.editUrl(url);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(
            menuFamilyItemsActions.editMenuFamilyItem({ menuFamilyItem: url })
        );
    });
});
