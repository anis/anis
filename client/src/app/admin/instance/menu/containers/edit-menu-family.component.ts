/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Instance, MenuFamily } from 'src/app/metamodel/models';
import * as menuItemsActions from 'src/app/metamodel/actions/menu-item.actions';
import * as menuItemsSelector from 'src/app/metamodel/selectors/menu-item.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';

@Component({
    selector: 'app-edit-menu-family',
    templateUrl: 'edit-menu-family.component.html'
})
export class EditMenuFamilyComponent {
    public instance: Observable<Instance>;
    public menuItemListIsLoading: Observable<boolean>;
    public menuItemListIsLoaded: Observable<boolean>;
    public menuFamily: Observable<MenuFamily>;

    constructor(private store: Store<{ }>) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.menuItemListIsLoading = store.select(menuItemsSelector.selectMenuItemListIsLoading);
        this.menuItemListIsLoaded = store.select(menuItemsSelector.selectMenuItemListIsLoaded);
        this.menuFamily = store.select(menuItemsSelector.selectMenuFamilyByRouteId);
    }

    editMenuFamily(menuFamily: MenuFamily) {
        this.store.dispatch(menuItemsActions.editMenuItem({ menuItem: menuFamily }));
    }
}
