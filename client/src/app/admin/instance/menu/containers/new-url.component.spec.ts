/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, ComponentFixture } from '@angular/core/testing';
import { NewUrlComponent } from './new-url.component';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { Url } from 'src/app/metamodel/models';
import * as menuItemsActions from 'src/app/metamodel/actions/menu-item.actions';
import { URL } from 'src/test-data';

describe('[Admin][Instance][Menu][Containers] NewUrlComponent', () => {
    let fixture: ComponentFixture<NewUrlComponent>;
    let component: NewUrlComponent;
    let store: MockStore;

    afterEach(() => {
        store.resetSelectors();
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [NewUrlComponent],
            providers: [provideMockStore({})],
        });
        fixture = TestBed.createComponent(NewUrlComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#addNewUrl() should dispatch addMenuItem action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        const url: Url = { ...URL };
        component.addNewUrl(url);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(
            menuItemsActions.addMenuItem({ menuItem: url })
        );
    });
});
