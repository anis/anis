/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, ComponentFixture } from '@angular/core/testing';
import { MenuItemListComponent } from './menu-item-list.component';
import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { MenuItem } from 'src/app/metamodel/models';
import * as menuItemsActions from 'src/app/metamodel/actions/menu-item.actions';
import { WEBPAGE } from 'src/test-data';

describe('[Admin][Instance][Menu][Containers] MenuItemListComponent', () => {
    let fixture: ComponentFixture<MenuItemListComponent>;
    let component: MenuItemListComponent;
    let store: MockStore;

    afterEach(() => {
        store.resetSelectors();
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [MenuItemListComponent],
            providers: [provideMockStore({})],
        });
        fixture = TestBed.createComponent(MenuItemListComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#deleteMenuItem() should dispatch addMenuItem action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        const menu: MenuItem = { ...WEBPAGE };
        component.deleteMenuItem(menu);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(
            menuItemsActions.deleteMenuItem({ menuItem: menu })
        );
    });
});
