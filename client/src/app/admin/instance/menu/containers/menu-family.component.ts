/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as menuFamilyItemActions from 'src/app/metamodel/actions/menu-family-item.actions';
import * as menuFamilyItemSelector from 'src/app/metamodel/selectors/menu-family-item.selector';

@Component({
    selector: 'app-menu-family',
    templateUrl: 'menu-family.component.html'
})
export class MenuFamilyComponent implements OnInit {
    public menuFamilyItemListIsLoading: Observable<boolean>;
    public menuFamilyItemListIsLoaded: Observable<boolean>;

    constructor(private store: Store<{ }>) {
        this.menuFamilyItemListIsLoading = store.select(menuFamilyItemSelector.selectMenuFamilyItemListIsLoading);
        this.menuFamilyItemListIsLoaded = store.select(menuFamilyItemSelector.selectMenuFamilyItemListIsLoaded);
    }

    ngOnInit() {
        this.store.dispatch(menuFamilyItemActions.loadMenuFamilyItemList());
    }
}
