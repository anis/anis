/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, ComponentFixture } from '@angular/core/testing';
import { WebpageFormComponent } from './webpage-form.component';
import {
    FormsModule,
    ReactiveFormsModule,
    UntypedFormGroup,
} from '@angular/forms';
import { Component, Input } from '@angular/core';

import { Webpage } from 'src/app/metamodel/models';
import { WEBPAGE } from 'src/test-data';

describe('[Admin][Instance][Menu][Components] WebpageFormComponent', () => {
    let fixture: ComponentFixture<WebpageFormComponent>;
    let component: WebpageFormComponent;

    @Component({
        selector: 'app-webpage-form-content',
        template: '',
    })
    class WebpageFormContentComponent {
        @Input() form: UntypedFormGroup;
        @Input() controlName: string;
        @Input() controlLabel: string;
        @Input() codeType: string;
    }

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, ReactiveFormsModule],
            declarations: [WebpageFormComponent, WebpageFormContentComponent],
        });
        fixture = TestBed.createComponent(WebpageFormComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', () => {
        const patch = jest.spyOn(component.form, 'patchValue');
        fixture.detectChanges();

        component.ngOnInit();
        expect(patch).not.toHaveBeenCalled();

        jest.resetAllMocks();

        const webpage: Webpage = { ...WEBPAGE };
        component.webpage = webpage;
        fixture.detectChanges();
        component.ngOnInit();
        expect(patch).toHaveBeenCalledWith(webpage);
    });

    it('should submit the webpage if present or nextMenuItemId', () => {
        const emit = jest.spyOn(component.onSubmit, 'emit');
        const pristine = jest.spyOn(component.form, 'markAsPristine');
        const form = jest.spyOn(component.form, 'getRawValue').mockReturnValue({
            label: 'Home',
            icon: 'fas fa-home',
            display: 10,
            name: 'home',
            title: 'Home',
            content: '<p>TEST</p>',
            style_sheet: '.lead {\n    color: #b60707;\n}',
        });

        component.nextMenuItemId = 1;
        fixture.detectChanges();
        const webpage: Webpage = { ...WEBPAGE };
        component.submit();
        expect(emit).toHaveBeenCalledWith(webpage);
        expect(form).toHaveBeenCalled();
        expect(pristine).not.toHaveBeenCalled();

        jest.resetAllMocks();
        form.mockReturnValue({
            label: 'Home',
            icon: 'fas fa-home',
            display: 10,
            name: 'home',
            title: 'Home',
            content: '<p>TEST</p>',
            style_sheet: '.lead {\n    color: #b60707;\n}',
        });
        component.webpage = { ...WEBPAGE };
        component.nextMenuItemId = 1;
        fixture.detectChanges();
        component.submit();
        expect(emit).toHaveBeenCalledWith(webpage);
        expect(form).toHaveBeenCalled();
        expect(pristine).toHaveBeenCalled();
    });
});
