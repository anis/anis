/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { MenuItem } from 'src/app/metamodel/models';

@Component({
    selector: 'app-menu-item-list-table',
    templateUrl: 'menu-item-list-table.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuItemListTableComponent {
    @Input() menuItemList: MenuItem[];
    @Output() deleteMenuItem: EventEmitter<MenuItem> = new EventEmitter();

    getEditRouterLink(menuItem: MenuItem) {
        let type = menuItem.type.replace('_', '-');
        return `edit-${type}/${menuItem.id}`;
    }
}
