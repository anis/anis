/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, ComponentFixture } from '@angular/core/testing';
import { UrlFormComponent } from './url-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Url } from 'src/app/metamodel/models';

describe('[Admin][Instance][Menu][Components] UrlFormComponent', () => {
    let fixture: ComponentFixture<UrlFormComponent>;
    let component: UrlFormComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, ReactiveFormsModule],
            declarations: [UrlFormComponent],
        });
        fixture = TestBed.createComponent(UrlFormComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', () => {
        const patch = jest.spyOn(component.form, 'patchValue');
        fixture.detectChanges();

        component.ngOnInit();
        expect(patch).not.toHaveBeenCalled();

        jest.resetAllMocks();

        const url: Url = <Url>{
            type_url: 'Hej!',
            href: 'anis.com',
        };
        component.url = url;
        fixture.detectChanges();
        component.ngOnInit();
        expect(patch).toHaveBeenCalledWith(url);
    });

    it('should submit the url if present or nextMenuItemId', () => {
        const emit = jest.spyOn(component.onSubmit, 'emit');
        const form = jest.spyOn(component.form, 'getRawValue').mockReturnValue({
            label: 'coconut',
            icon: 'fa fa-search',
            display: 12,
            type_url: 'Hey!',
            href: 'anis.com',
        });
        const pristine = jest.spyOn(component.form, 'markAsPristine');

        component.nextMenuItemId = 4;
        fixture.detectChanges();
        const mockUrl: Url = <Url>{
            id: 4,
            label: 'coconut',
            icon: 'fa fa-search',
            display: 12,
            type_url: 'Hey!',
            type: 'url',
            href: 'anis.com',
        };

        component.submit();
        expect(emit).toHaveBeenCalledWith(mockUrl);
        expect(form).toHaveBeenCalled();
        expect(pristine).not.toHaveBeenCalled();

        jest.resetAllMocks();
        form.mockReturnValue({
            label: 'coco',
            icon: 'fa fa-search',
            display: 12,
            type_url: 'Hej!',
            href: 'anis-dev.com',
        });

        component.url = <Url>{ id: 4 };
        component.nextMenuItemId = null;
        fixture.detectChanges();
        const obj: Url = <Url>{
            id: 4,
            label: 'coco',
            icon: 'fa fa-search',
            display: 12,
            type_url: 'Hej!',
            href: 'anis-dev.com',
        };

        component.submit();
        expect(emit).toHaveBeenCalledWith(obj);
        expect(form).toHaveBeenCalled();
        expect(pristine).toHaveBeenCalled();
    });
});
