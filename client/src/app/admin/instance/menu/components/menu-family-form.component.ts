/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { MenuFamily } from 'src/app/metamodel/models';

@Component({
    selector: 'app-menu-family-form',
    templateUrl: 'menu-family-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuFamilyFormComponent implements OnInit {
    @Input() nextMenuItemId: number;
    @Input() menuFamily: MenuFamily;
    @Output() onSubmit: EventEmitter<MenuFamily> = new EventEmitter();

    public form = new UntypedFormGroup({
        name: new UntypedFormControl('', [Validators.required]),
        label: new UntypedFormControl('', [Validators.required]),
        icon: new UntypedFormControl(null),
        display: new UntypedFormControl('', [Validators.required]),
    });

    ngOnInit() {
        if (this.menuFamily) {
            this.form.patchValue(this.menuFamily);
        }
    }

    submit() {
        if (this.menuFamily) {
            this.onSubmit.emit({
                ...this.menuFamily,
                ...this.form.getRawValue()
            });
            this.form.markAsPristine();
        } else {
            this.onSubmit.emit({
                id: this.nextMenuItemId,
                ...this.form.getRawValue(),
                type: 'menu_family'
            });
        }
    }
}
