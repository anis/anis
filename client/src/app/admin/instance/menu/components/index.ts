/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { MenuItemListTableComponent } from './menu-item-list-table.component';
import { WebpageFormComponent } from './webpage-form.component';
import { MenuFamilyFormComponent } from './menu-family-form.component';
import { UrlFormComponent } from './url-form.component';

export const dummiesComponents = [
    MenuItemListTableComponent,
    WebpageFormComponent,
    MenuFamilyFormComponent,
    UrlFormComponent
];
