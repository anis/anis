/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, ComponentFixture } from '@angular/core/testing';
import { MenuFamilyFormComponent } from './menu-family-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MenuFamily, MenuItem } from 'src/app/metamodel/models';
import { MENU_FAMILY } from 'src/test-data';

describe('[Admin][Instance][Menu][Components] MenuFamilyFormComponent', () => {
    let fixture: ComponentFixture<MenuFamilyFormComponent>;
    let component: MenuFamilyFormComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, ReactiveFormsModule],
            declarations: [MenuFamilyFormComponent],
        });
        fixture = TestBed.createComponent(MenuFamilyFormComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('should execute ngOnInit lifecycle', () => {
        const patch = jest.spyOn(component.form, 'patchValue');
        fixture.detectChanges();

        component.ngOnInit();
        expect(patch).not.toHaveBeenCalled();

        jest.resetAllMocks();

        const menus: MenuFamily = { ...MENU_FAMILY };
        component.menuFamily = menus;
        fixture.detectChanges();
        component.ngOnInit();
        expect(patch).toHaveBeenCalledWith(menus);
    });

    it('should submit the menuFamily if present or nextMenuItemId', () => {
        const emit = jest.spyOn(component.onSubmit, 'emit');
        const form = jest.spyOn(component.form, 'getRawValue').mockReturnValue({
            label: 'coconut',
            icon: 'fa fa-search',
            display: 12,
        });
        const pristine = jest.spyOn(component.form, 'markAsPristine');

        component.nextMenuItemId = 4;
        fixture.detectChanges();
        const menuFamily: MenuFamily = <MenuFamily>{
            id: 4,
            label: 'coconut',
            icon: 'fa fa-search',
            display: 12,
            type: 'menu_family',
        };

        component.submit();
        expect(emit).toHaveBeenCalledWith(menuFamily);
        expect(form).toHaveBeenCalled();
        expect(pristine).not.toHaveBeenCalled();

        jest.resetAllMocks();
        form.mockReturnValue({
            label: 'coucou',
            icon: 'fa fa-search',
            display: 15,
        });

        const menu: MenuItem = {
            id: 5,
            label: 'coucou',
            icon: 'fa fa-search',
            display: 12,
            type: 'menu-item',
        };
        const menus: MenuFamily = {
            ...{
                id: 2,
                name: 'coucou',
                label: 'coucou',
                icon: 'fa fa-search',
                display: 15,
                type: 'menu_family',
            },
            items: [menu, menu],
        };
        component.nextMenuItemId = 2;
        component.menuFamily = menus;
        fixture.detectChanges();

        component.submit();
        expect(emit).toHaveBeenCalledWith(menus);
        expect(form).toHaveBeenCalled();
        expect(pristine).toHaveBeenCalled();
    });
});
