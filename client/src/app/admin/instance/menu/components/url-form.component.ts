/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { Url } from 'src/app/metamodel/models';

@Component({
    selector: 'app-url-form',
    templateUrl: 'url-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UrlFormComponent implements OnInit {
    @Input() nextMenuItemId: number;
    @Input() url: Url;
    @Output() onSubmit: EventEmitter<Url> = new EventEmitter();

    public form = new UntypedFormGroup({
        label: new UntypedFormControl('', [Validators.required]),
        icon: new UntypedFormControl(null),
        display: new UntypedFormControl('', [Validators.required]),
        type_url: new UntypedFormControl('', [Validators.required]),
        href: new UntypedFormControl('', [Validators.required])
    });

    ngOnInit() {
        if (this.url) {
            this.form.patchValue(this.url);
        }
    }

    submit() {
        if (this.url) {
            this.onSubmit.emit({
                ...this.url,
                ...this.form.getRawValue()
            });
            this.form.markAsPristine();
        } else {
            this.onSubmit.emit({
                id: this.nextMenuItemId,
                ...this.form.getRawValue(),
                type: 'url'
            });
        }
    }
}
