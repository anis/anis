/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, ComponentFixture } from '@angular/core/testing';
import { MenuItemListTableComponent } from './menu-item-list-table.component';

import { MenuItem } from 'src/app/metamodel/models';
import { WEBPAGE } from 'src/test-data';

describe('[Admin][Instance][Menu][Components] MenuItemListTableComponent', () => {
    let fixture: ComponentFixture<MenuItemListTableComponent>;
    let component: MenuItemListTableComponent;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [MenuItemListTableComponent]
        });
        fixture = TestBed.createComponent(MenuItemListTableComponent);
        component = fixture.componentInstance;
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#getEditRouterLink() should return an id-string', () => {
        const menu: MenuItem = { ...WEBPAGE };

        const res: string = component.getEditRouterLink(menu);
        expect(res).toBe('edit-webpage/1');
    });
});
