/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { Webpage } from 'src/app/metamodel/models';

@Component({
    selector: 'app-webpage-form',
    templateUrl: 'webpage-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WebpageFormComponent implements OnInit {
    @Input() nextMenuItemId: number;
    @Input() webpage: Webpage;
    @Output() onSubmit: EventEmitter<Webpage> = new EventEmitter();

    public form = new UntypedFormGroup({
        name: new UntypedFormControl('', [Validators.required]),
        label: new UntypedFormControl('', [Validators.required]),
        icon: new UntypedFormControl(null),
        display: new UntypedFormControl('', [Validators.required]),
        title: new UntypedFormControl(''),
        content: new UntypedFormControl(''),
        style_sheet: new UntypedFormControl(null)
    });

    ngOnInit() {
        if (this.webpage) {
            this.form.patchValue(this.webpage);
        }
    }

    submit() {
        if (this.webpage) {
            this.onSubmit.emit({
                ...this.webpage,
                ...this.form.getRawValue()
            });
            this.form.markAsPristine();
        } else {
            this.onSubmit.emit({
                id: this.nextMenuItemId,
                ...this.form.getRawValue(),
                type: 'webpage'
            });
        }
    }
}
