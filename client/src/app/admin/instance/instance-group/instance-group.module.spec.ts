/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InstanceGroupModule } from './instance-group.module';

describe('[admin][instance][instance-group] InstanceGroupModule', () => {
    it('Test instance group module', () => {
        expect(InstanceGroupModule.name).toEqual('InstanceGroupModule');
    });
});
