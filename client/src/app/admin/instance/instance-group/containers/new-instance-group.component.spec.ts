/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { Instance } from 'src/app/metamodel/models';
import * as instanceGroupActions from 'src/app/metamodel/actions/instance-group.actions';
import { NewInstanceGroupComponent } from './new-instance-group.component';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { INSTANCE } from 'src/test-data';

describe('[admin][instance][instance-group][containers] NewInstanceGroupComponent', () => {
    @Component({ selector: 'app-instance-group-form', template: '' })
    class InstanceGroupFormStubComponent{ }

    let component: NewInstanceGroupComponent;
    let fixture: ComponentFixture<NewInstanceGroupComponent>;
    let store: MockStore;
    let mockInstanceSelectorInstance;
    let instance: Instance;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                NewInstanceGroupComponent,
                InstanceGroupFormStubComponent
            ],
            imports: [
                BrowserAnimationsModule,
                RouterTestingModule
            ],
            providers: [
                provideMockStore({})
            ]
        });
        fixture = TestBed.createComponent(NewInstanceGroupComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        instance = { ...INSTANCE };
        mockInstanceSelectorInstance = store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
        fixture.detectChanges();
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });

    it('#addNewInstanceGroup(instanceGroup) should dispatch addInstanceGroup action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        let instanceGroup = { ...INSTANCE.groups[1] };
        component.addNewInstanceGroup(instanceGroup);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(instanceGroupActions.addInstanceGroup({ instanceGroup }));
    });
});
