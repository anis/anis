/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { Instance, InstanceGroup } from 'src/app/metamodel/models';
import * as instanceGroupActions from 'src/app/metamodel/actions/instance-group.actions';
import * as instanceGroupSelector from 'src/app/metamodel/selectors/instance-group.selector';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';

@Component({
    selector: 'app-instance-group-list',
    templateUrl: 'instance-group-list.component.html'
})
export class InstanceGroupListComponent {
    public instance: Observable<Instance>;
    public instanceGroupList: Observable<InstanceGroup[]>;
    public instanceGroupListIsLoading: Observable<boolean>;
    public instanceGroupListIsLoaded: Observable<boolean>;

    constructor(private store: Store<{ }>) {
        this.instance = store.select(instanceSelector.selectInstanceByRouteName);
        this.instanceGroupList = store.select(instanceGroupSelector.selectAllInstanceGroups);
        this.instanceGroupListIsLoading = store.select(instanceGroupSelector.selectInstanceGroupListIsLoading);
        this.instanceGroupListIsLoaded = store.select(instanceGroupSelector.selectInstanceGroupListIsLoaded);
    }

    deleteInstanceGroup(instanceGroup: InstanceGroup) {
        this.store.dispatch(instanceGroupActions.deleteInstanceGroup({ instanceGroup }));
    }
}
