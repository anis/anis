/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { Instance } from 'src/app/metamodel/models';
import * as instanceGroupActions from 'src/app/metamodel/actions/instance-group.actions';
import { InstanceGroupListComponent } from './instance-group-list.component';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { INSTANCE } from 'src/test-data';

describe('[admin][instance][instance-group][containers] InstanceGroupListComponent', () => {
    let component : InstanceGroupListComponent;
    let fixture : ComponentFixture<InstanceGroupListComponent>;
    let store: MockStore;
    let mockInstanceSelectorInstance;
    let instance: Instance;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InstanceGroupListComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                RouterTestingModule
            ],
            providers: [
                provideMockStore({})
            ]
        });
        fixture = TestBed.createComponent(InstanceGroupListComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        instance = { ...INSTANCE };
        mockInstanceSelectorInstance = store.overrideSelector(instanceSelector.selectInstanceByRouteName, instance);
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('#deleteInstanceGroup(instanceGroup) should dispatch deleteInstanceGroup action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        let instanceGroup = { ...INSTANCE.groups[1] };
        component.deleteInstanceGroup(instanceGroup);
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(instanceGroupActions.deleteInstanceGroup({ instanceGroup }));
    });
});
