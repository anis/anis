/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InstanceGroupListComponent } from './containers/instance-group-list.component';
import { NewInstanceGroupComponent } from './containers/new-instance-group.component';

const routes: Routes = [
    { path: '', component: InstanceGroupListComponent, title: 'Instance group list' },
    { path: 'new-instance-group', component: NewInstanceGroupComponent, title: 'New instance group' },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class InstanceGroupRoutingModule { }

export const routedComponents = [
    InstanceGroupListComponent,
    NewInstanceGroupComponent
];
