/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, ChangeDetectionStrategy, EventEmitter } from '@angular/core';

import { InstanceGroup } from 'src/app/metamodel/models';

@Component({
    selector: 'app-instance-group-list-table',
    templateUrl: 'instance-group-list-table.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstanceGroupListTableComponent {
    @Input() instanceGroupList: InstanceGroup[];
    @Output() deleteInstanceGroup: EventEmitter<InstanceGroup> = new EventEmitter();
}
