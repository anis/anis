/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { InstanceGroupFormComponent } from './instance-group-form.component';
import { INSTANCE } from 'src/test-data';

describe('[admin][instance][instance-group][components] instanceGroupFormComponent', () => {
    let component: InstanceGroupFormComponent;
    let fixture: ComponentFixture<InstanceGroupFormComponent>;
    let spy;
    
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InstanceGroupFormComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ]
        });
        fixture = TestBed.createComponent(InstanceGroupFormComponent);
        component = fixture.componentInstance;
        component.instanceGroup= { ...INSTANCE.groups[1] };
        fixture.detectChanges();
        spy= jest.spyOn(component.onSubmit, 'emit');
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it(`should emit intance group and form values on subumit`, () => {
       component.submit();
       expect(spy).toHaveBeenCalledTimes(1);
    });

    it(`should emit instance group on subumit`, () => {
        component.instanceGroup = null;
        component.submit();
        expect(spy).toHaveBeenCalledTimes(1);
     });
});
