/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { dummiesComponents } from './index';

describe('[admin][instance][instance-group][components] Index components', () => {
    it('Test index components', () => {
        expect(dummiesComponents.length).toEqual(2);
    });
});
