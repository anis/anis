/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, Output, EventEmitter, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { InstanceGroup } from 'src/app/metamodel/models';

@Component({
    selector: 'app-instance-group-form',
    templateUrl: 'instance-group-form.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstanceGroupFormComponent implements OnInit {
    @Input() instanceGroup: InstanceGroup;
    @Output() onSubmit: EventEmitter<InstanceGroup> = new EventEmitter();

    public form = new UntypedFormGroup({
        role: new UntypedFormControl('', [Validators.required])
    });

    ngOnInit() {
        if (this.instanceGroup) {
            this.form.patchValue(this.instanceGroup);
        }
    }

    submit() {
        if (this.instanceGroup) {
            this.onSubmit.emit({
                ...this.instanceGroup,
                ...this.form.value
            });
        } else {
            this.onSubmit.emit(this.form.value);
        }
    }
}
