/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { InstanceGroupListTableComponent } from './instance-group-list-table.component';

describe('[admin][instance][instance-group][components] instanceGroupListTableComponent', () => {
    let component: InstanceGroupListTableComponent;
    let fixture: ComponentFixture<InstanceGroupListTableComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                InstanceGroupListTableComponent,
            ],
            imports: [
                BrowserAnimationsModule
            ]
        });
        fixture = TestBed.createComponent(InstanceGroupListTableComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });
});
