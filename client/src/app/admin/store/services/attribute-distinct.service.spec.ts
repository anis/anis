/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AttributeDistinctService } from './attribute-distinct.service';
import { AppConfigService } from 'src/app/app-config.service';
import { Dataset, Instance } from 'src/app/metamodel/models';
import { AnisHttpClientService } from 'src/app/metamodel/anis-http-client.service';
import { INSTANCE, DATASET } from 'src/test-data';

describe('[Admin][store][services] TableService', () => {
    let service: AttributeDistinctService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let dataset: Dataset;
    let attributeId: number;
    let distinctValues: string[];

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                AttributeDistinctService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(AttributeDistinctService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        dataset = { ...DATASET };
        attributeId = 1;
        distinctValues = [
            'value1',
            'value2'
        ];
    }));

    it('#retrieveAttributeDistinctList() should return an Observable<string[]>', () => {
        service.retrieveAttributeDistinctList(instance.name, dataset.name, attributeId).subscribe((res: string[]) => {
            expect(res).toEqual(distinctValues);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/attribute/${attributeId}/distinct`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: distinctValues});
    });
});
