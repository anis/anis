/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Column } from '../models';
import { AnisHttpClientService } from 'src/app/metamodel/anis-http-client.service';

@Injectable()
export class ColumnService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves column list for the given dataset.
     *
     * @param string instanceName - The name of the current instance
     * @param string idDatabase - The database ID
     * @param string tableName - The table name
     *
     * @return Observable<Column[]>
     */
    retrieveColumns(instanceName: string, idDatabase: number, tableName: string): Observable<Column[]> {
        return this.anisHttpClientService.get<Column[]>(`/instance/${instanceName}/database/${idDatabase}/table/${tableName}/column`);
    }
}
