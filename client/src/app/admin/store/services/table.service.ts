/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { AnisHttpClientService } from 'src/app/metamodel/anis-http-client.service';

@Injectable()
export class TableService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves tables or views list for this database
     *
     * @param string instanceName - The name of the current instance
     * @param number idDatabase - The database ID.
     *
     * @return Observable<string[]>
     */
    retrieveTableList(instanceName: string, idDatabase: number): Observable<string[]> {
        return this.anisHttpClientService.get<string[]>(`/instance/${instanceName}/database/${idDatabase}/table`);
    }
}
