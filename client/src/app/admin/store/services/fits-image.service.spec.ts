/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AppConfigService } from 'src/app/app-config.service';
import { FitsImageService } from './fits-image.service';
import { FitsImageLimits } from '../models';
import { Instance, Dataset } from 'src/app/metamodel/models';
import { INSTANCE, DATASET, FITS_IMAGE_LIMITS } from 'src/test-data';

describe('[Admin][store][services] FitsImageService', () => {
    let service: FitsImageService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let dataset: Dataset;
    let filePath: string;
    let fitsImageLimits: FitsImageLimits;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                FitsImageService,
                {
                    provide: AppConfigService, useValue: {
                        servicesUrl: 'http://localhost:5000'
                    }
                }
            ]
        });
        service = TestBed.inject(FitsImageService);
        httpController = TestBed.inject(HttpTestingController);
        TestBed.inject(AppConfigService);
        instance = { ...INSTANCE };
        dataset = { ...DATASET };
        filePath = 'files/my-file.fits';
        fitsImageLimits = { ...FITS_IMAGE_LIMITS };
    });

    it('#retrieveFitsImageLimits() should return an Observable<FitsImageLimits>', () => {
        service.retrieveFitsImageLimits(instance.name, dataset.name, filePath).subscribe((res: FitsImageLimits) => {
            expect(res).toEqual(fitsImageLimits);
        });
        const url = `http://localhost:5000/fits/get-fits-image-limits/${instance.name}/${dataset.name}?filename=${filePath}`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush(fitsImageLimits);
    });
});
