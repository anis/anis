/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { FileInfo } from '../models';
import { AnisHttpClientService } from 'src/app/metamodel/anis-http-client.service';

@Injectable()
export class AdminFileExplorerService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves files with the given path.
     *
     * @param string path - The path.
     *
     * @return Observable<FileInfo[]>
     */
    retrieveFiles(path: string): Observable<FileInfo[]> {
        return this.anisHttpClientService.get<FileInfo[]>(`/admin-file-explorer${path}`);
    }
}
