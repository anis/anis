/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { AnisHttpClientService } from 'src/app/metamodel/anis-http-client.service';

@Injectable()
export class AttributeDistinctService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves distinct attribute list for the given dataset.
     *
     * @param string instanceName - The current instance name
     * @param string datasetName - The current dataset name
     * @param number attributeId - The attribute ID
     *
     * @return Observable<string[]>
     */
    retrieveAttributeDistinctList(instanceName: string, datasetName: string, attributeId: number): Observable<string[]> {
        return this.anisHttpClientService.get<string[]>(`/instance/${instanceName}/dataset/${datasetName}/attribute/${attributeId}/distinct`);
    }
}
