/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { TableService } from './table.service';
import { AppConfigService } from 'src/app/app-config.service';
import { Database, Instance } from 'src/app/metamodel/models';
import { AnisHttpClientService } from 'src/app/metamodel/anis-http-client.service';
import { INSTANCE, DATABASE, TABLE_LIST } from 'src/test-data';

describe('[Admin][store][services] TableService', () => {
    let service: TableService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let database: Database;
    let tableList: string[];

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                TableService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(TableService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        database = { ...DATABASE };
        tableList = [ ...TABLE_LIST ];
    }));

    it('#retrieveTableList() should return an Observable<string[]>', () => {
        service.retrieveTableList(instance.name, database.id).subscribe((res: string[]) => {
            expect(res).toEqual(tableList);
        });
        const url = `http://localhost:8080/instance/${instance.name}/database/${database.id}/table`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: tableList});
    });
});
