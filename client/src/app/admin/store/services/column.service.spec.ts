/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ColumnService } from './column.service';
import { Column } from '../models';
import { AppConfigService } from 'src/app/app-config.service';
import { Database, Instance } from 'src/app/metamodel/models';
import { AnisHttpClientService } from 'src/app/metamodel/anis-http-client.service';
import { INSTANCE, DATABASE, COLUMN_LIST } from 'src/test-data';

describe('[Admin][store][services] ColumnService', () => {
    let service: ColumnService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let database: Database;
    let tableName: string;
    let columnList: Column[];

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                ColumnService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(ColumnService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        database = { ...DATABASE };
        tableName = 'table1';
        columnList = [ ...COLUMN_LIST ];
    }));

    it('#retrieveTableList() should return an Observable<string[]>', () => {
        service.retrieveColumns(instance.name, database.id, tableName).subscribe((res: Column[]) => {
            expect(res).toEqual(columnList);
        });
        const url = `http://localhost:8080/instance/${instance.name}/database/${database.id}/table/${tableName}/column`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: columnList});
    });
});
