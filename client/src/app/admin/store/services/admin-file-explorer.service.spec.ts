/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AdminFileExplorerService } from './admin-file-explorer.service';
import { FileInfo } from '../models';
import { AppConfigService } from 'src/app/app-config.service';
import { AnisHttpClientService } from 'src/app/metamodel/anis-http-client.service';
import { FILE_INFO_LIST } from 'src/test-data';

describe('[Admin][store][services] TableService', () => {
    let service: AdminFileExplorerService;
    let httpController: HttpTestingController;
    let path: string;
    let fileInfoList: FileInfo[];

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                AdminFileExplorerService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(AdminFileExplorerService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        path = 'files/spec1d';
        fileInfoList = [ ...FILE_INFO_LIST ];
    }));

    it('#retrieveFiles() should return an Observable<string[]>', () => {
        service.retrieveFiles(path).subscribe((res: FileInfo[]) => {
            expect(res).toEqual(fileInfoList);
        });
        const url = `http://localhost:8080/admin-file-explorer${path}`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: fileInfoList});
    });
});
