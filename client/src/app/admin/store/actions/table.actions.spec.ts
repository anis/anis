/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as tableActions from './table.actions';
import { TABLE_LIST } from 'src/test-data';

describe('[Admin][store][actions] Table actions', () => {
    it('should create loadTableList action', () => {
        const action = tableActions.loadTableList({ idDatabase: 1 });
        expect(action).toEqual({ type: '[Admin] Load Table List', idDatabase: 1 });
    });

    it('should create loadTableListSuccess action', () => {
        const action = tableActions.loadTableListSuccess({ tables: TABLE_LIST });
        expect(action).toEqual({ type: '[Admin] Load Table List Success', tables: TABLE_LIST });
    });

    it('should create loadTableListFail action', () => {
        const action = tableActions.loadTableListFail();
        expect(action).toEqual({ type: '[Admin] Load Table List Fail' });
    });
});
