/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as fitsImageActions from './fits-image.actions';
import { FITS_IMAGE_LIMITS } from 'src/test-data';

describe('[Admin][store][actions] Fits image actions', () => {
    it('should create retrieveFitsImageLimits action', () => {
        const action = fitsImageActions.retrieveFitsImageLimits({ filePath: 'files/my-file.fits' });
        expect(action).toEqual({ type: '[Admin] Retrieve Fits Image Limits', filePath: 'files/my-file.fits' });
    });

    it('should create retrieveFitsImageLimitsSuccess action', () => {
        const action = fitsImageActions.retrieveFitsImageLimitsSuccess({ fitsImageLimits: FITS_IMAGE_LIMITS });
        expect(action).toEqual({ type: '[Admin] Retrieve Fits Image Limits Success', fitsImageLimits: FITS_IMAGE_LIMITS });
    });

    it('should create retrieveFitsImageLimitsFail action', () => {
        const action = fitsImageActions.retrieveFitsImageLimitsFail();
        expect(action).toEqual({ type: '[Admin] Retrieve Fits Image Limits Fail' });
    });
});
