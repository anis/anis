/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as monacoEditorAction from './monaco-editor.actions';

describe('[Admin][store][actions] Monaco editor actions', () => {
    it('should create loadMonacoEditor action', () => {
        const action = monacoEditorAction.loadMonacoEditor();
        expect(action).toEqual({ type: '[Admin] Load Monaco Editor' });
    });

    it('should create loadMonacoEditorInProgress action', () => {
        const action = monacoEditorAction.loadMonacoEditorInProgress();
        expect(action).toEqual({ type: '[Admin] Load Monaco Editor In Progress'});
    });

    it('should create loadMonacoEditorSuccess action', () => {
        const action = monacoEditorAction.loadMonacoEditorSuccess();
        expect(action).toEqual({ type: '[Admin] Load Monaco Editor Success' });
    });
});
