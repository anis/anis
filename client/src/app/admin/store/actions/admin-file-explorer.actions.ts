/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { FileInfo } from '../models';

export const loadFiles = createAction('[Admin] Load Files', props<{ path: string }>());
export const loadFilesSuccess = createAction('[Admin] Load Files Success', props<{ files: FileInfo[] }>());
export const loadFilesFail = createAction('[Admin] Load Files Fail');
