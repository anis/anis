/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as adminFileExplorerActions from './admin-file-explorer.actions';
import { FILE_INFO_LIST } from 'src/test-data';

describe('[Admin][store][actions] Admin file explorer actions', () => {
    it('should create loadFiles action', () => {
        const action = adminFileExplorerActions.loadFiles({ path: 'files/spec1d' });
        expect(action).toEqual({ type: '[Admin] Load Files', path: 'files/spec1d' });
    });

    it('should create loadFilesSuccess action', () => {
        const action = adminFileExplorerActions.loadFilesSuccess({ files: FILE_INFO_LIST });
        expect(action).toEqual({ type: '[Admin] Load Files Success', files: FILE_INFO_LIST });
    });

    it('should create loadFilesFail action', () => {
        const action = adminFileExplorerActions.loadFilesFail();
        expect(action).toEqual({ type: '[Admin] Load Files Fail' });
    });
});
