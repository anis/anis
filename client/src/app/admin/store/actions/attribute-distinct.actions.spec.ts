/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as attributeDistinctActions from './attribute-distinct.actions';
import { ATTRIBUTE } from 'src/test-data';

describe('[Admin][store][actions] Attribute distinct actions', () => {
    it('should create loadAttributeDistinctList action', () => {
        const action = attributeDistinctActions.loadAttributeDistinctList({ attribute: ATTRIBUTE });
        expect(action).toEqual({ type: '[Admin] Load Attribute Distinct List', attribute: ATTRIBUTE });
    });

    it('should create loadAttributeDistinctListSuccess action', () => {
        const action = attributeDistinctActions.loadAttributeDistinctListSuccess({ values: ['value1', 'value2'] });
        expect(action).toEqual({ type: '[Admin] Load Attribute Distinct List Success', values: ['value1', 'value2'] });
    });

    it('should create loadAttributeDistinctListFail action', () => {
        const action = attributeDistinctActions.loadAttributeDistinctListFail();
        expect(action).toEqual({ type: '[Admin] Load Attribute Distinct List Fail' });
    });
});
