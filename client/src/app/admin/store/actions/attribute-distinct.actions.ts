/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';

import { Attribute } from 'src/app/metamodel/models';

export const loadAttributeDistinctList = createAction('[Admin] Load Attribute Distinct List', props<{ attribute: Attribute }>());
export const loadAttributeDistinctListSuccess = createAction('[Admin] Load Attribute Distinct List Success', props<{ values: string[] }>());
export const loadAttributeDistinctListFail = createAction('[Admin] Load Attribute Distinct List Fail');
