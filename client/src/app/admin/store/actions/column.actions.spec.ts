/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as columnActions from './column.actions';
import { COLUMN_LIST } from 'src/test-data';

describe('[Admin][store][actions] Columns actions', () => {
    it('should create loadColumnList action', () => {
        const action = columnActions.loadColumnList();
        expect(action).toEqual({ type: '[Admin] Load Column List' });
    });

    it('should create loadColumnListSuccess action', () => {
        const action = columnActions.loadColumnListSuccess({ columns: COLUMN_LIST });
        expect(action).toEqual({ type: '[Admin] Load Column List Success', columns: COLUMN_LIST });
    });

    it('should create loadColumnListFail action', () => {
        const action = columnActions.loadColumnListFail();
        expect(action).toEqual({ type: '[Admin] Load Column List Fail' });
    });
});
