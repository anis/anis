/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';

export const loadTableList = createAction('[Admin] Load Table List', props<{ idDatabase: number }>());
export const loadTableListSuccess = createAction('[Admin] Load Table List Success', props<{ tables: string[] }>());
export const loadTableListFail = createAction('[Admin] Load Table List Fail');
