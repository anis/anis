/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as exportActions from './export.actions';

describe('[Admin][store][actions] Export actions', () => {
    it('should create downloadExport action', () => {
        const action = exportActions.downloadExport({ url: 'http://localhost:8080/instance/default/export', filename: 'export_instance.json' });
        expect(action).toEqual({ type: '[Admin] Download export', url: 'http://localhost:8080/instance/default/export', filename: 'export_instance.json' });
    });
});
