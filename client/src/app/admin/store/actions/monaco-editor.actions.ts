/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction } from '@ngrx/store';

export const loadMonacoEditor = createAction('[Admin] Load Monaco Editor');
export const loadMonacoEditorInProgress = createAction('[Admin] Load Monaco Editor In Progress');
export const loadMonacoEditorSuccess = createAction('[Admin] Load Monaco Editor Success');
