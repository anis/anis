/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as monacoEditorSelector from './monaco-editor.selector';
import * as fromMonacoEditor from '../reducers/monaco-editor.reducer';

describe('[Admin][store][selectors] Monaco editor selector', () => {
    let state: any;

    beforeEach(() => {
        state = {
            admin: {
                monacoEditor: {
                    ...fromMonacoEditor.initialState,
                    monacoEditorIsLoading: false,
                    monacoEditorIsLoaded: true,
                },
            }
        };
    });

    it('should get monacoEditorState', () => {
        expect(monacoEditorSelector.selectMonacoEditorState(state)).toEqual({
            monacoEditorIsLoading: false,
            monacoEditorIsLoaded: true
        });
    });

    it('should get monacoEditorIsLoading', () => {
        expect(monacoEditorSelector.selectMonacoEditorIsLoading(state)).toBe(false);
    });

    it('should get monacoEditorIsLoaded', () => {
        expect(monacoEditorSelector.selectMonacoEditorIsLoaded(state)).toBe(true);
    });
});
