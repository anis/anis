/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../../admin.reducer';
import * as fromMonacoEditor from '../reducers/monaco-editor.reducer';

export const selectMonacoEditorState = createSelector(
    reducer.getAdminState,
    (state: reducer.State) => state.monacoEditor
);

export const selectMonacoEditorIsLoading = createSelector(
    selectMonacoEditorState,
    fromMonacoEditor.selectMonacoEditorIsLoading
);

export const selectMonacoEditorIsLoaded = createSelector(
    selectMonacoEditorState,
    fromMonacoEditor.selectMonacoEditorIsLoaded
);
