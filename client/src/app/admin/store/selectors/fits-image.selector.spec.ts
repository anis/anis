/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as fitsImageSelector from './fits-image.selector';
import { FITS_IMAGE_LIMITS } from 'src/test-data';

describe('[Admin][store][selectors] FitsImage selector', () => {
    let state: any;
    let fitsImageLimits = FITS_IMAGE_LIMITS;

    beforeEach(() => {
        state = {
            admin: {
                fitsImage: {
                    fitsImageLimits,
                    fitsImageLimitsIsLoading: false,
                    fitsImageLimitsIsLoaded: true
                },
            }
        };
    });

    it('should get fitsImage state', () => {
        expect(fitsImageSelector.selectFitsImageState(state)).toEqual(state.admin.fitsImage);
    });

    it('should get fitsImageLimits', () => {
        expect(fitsImageSelector.selectFitsImageLimits(state)).toEqual(fitsImageLimits);
    });

    it('should get fitsImageLimitsIsLoading', () => {
        expect(fitsImageSelector.selectFitsImageLimitsIsLoading(state)).toBe(false);
    });

    it('should get fitsImageLimitsIsLoaded', () => {
        expect(fitsImageSelector.selectFitsImageLimitsIsLoaded(state)).toBe(true);
    });
});
