/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../../admin.reducer';
import * as fromColumn from '../reducers/column.reducer';

export const selectColumnState = createSelector(
    reducer.getAdminState,
    (state: reducer.State) => state.column
);

export const selectColumnIds = createSelector(
    selectColumnState,
    fromColumn.selectColumnIds
);

export const selectColumnEntities = createSelector(
    selectColumnState,
    fromColumn.selectColumnEntities
);

export const selectAllColumns = createSelector(
    selectColumnState,
    fromColumn.selectAllColumns
);

export const selectColumnTotal = createSelector(
    selectColumnState,
    fromColumn.selectColumnTotal
);

export const selectColumnListIsLoading = createSelector(
    selectColumnState,
    fromColumn.selectColumnListIsLoading
);

export const selectColumnListIsLoaded = createSelector(
    selectColumnState,
    fromColumn.selectColumnListIsLoaded
);
