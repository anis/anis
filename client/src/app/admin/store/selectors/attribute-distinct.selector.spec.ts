/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as attributeDistinctSelector from './attribute-distinct.selector';
import * as fromAttributeDistinct from '../reducers/attribute-distinct.reducer';

describe('[Admin][store][selectors] AttributeDistinct selector', () => {
    let state: any;
    let value: string;

    beforeEach(() => {
        value = 'value1';
        state = {
            admin: {
                attributeDistinct: {
                    ...fromAttributeDistinct.initialState,
                    attributeDistinctListIsLoaded: true,
                    ids: [value],
                    entities: { [value]: value }
                },
            }
        };
    });

    it('should get attributeDistinct state', () => {
        expect(attributeDistinctSelector.selectAttributeDistinctState(state)).toEqual(state.admin.attributeDistinct);
    });

    it('should get attributeDistinct IDs', () => {
        expect(attributeDistinctSelector.selectAttributeDistinctIds(state).length).toEqual(1);
    });

    it('should get attributeDistinct entities', () => {
        expect(attributeDistinctSelector.selectAttributeDistinctEntities(state)).toEqual(state.admin.attributeDistinct.entities);
    });

    it('should get all attributeDistincts', () => {
        expect(attributeDistinctSelector.selectAllAttributeDistincts(state).length).toEqual(1);
    });

    it('should get attributeDistinct count', () => {
        expect(attributeDistinctSelector.selectAttributeDistinctTotal(state)).toEqual(1);
    });

    it('should get attributeDistinctListIsLoading', () => {
        expect(attributeDistinctSelector.selectAttributeDistinctListIsLoading(state)).toBe(false);
    });

    it('should get attributeDistinctListIsLoaded', () => {
        expect(attributeDistinctSelector.selectAttributeDistinctListIsLoaded(state)).toBe(true);
    });
});
