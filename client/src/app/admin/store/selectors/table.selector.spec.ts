/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as tableSelector from './table.selector';
import * as fromTable from '../reducers/table.reducer';
import { TABLE_LIST } from 'src/test-data';

describe('[Admin][store][selectors] Table selector', () => {
    let state: any;
    let table: string;

    beforeEach(() => {
        table = TABLE_LIST[0];
        state = {
            admin: {
                table: {
                    ...fromTable.initialState,
                    tableListIsLoaded: true,
                    ids: [table],
                    entities: { [table]: table }
                },
            }
        };
    });

    it('should get table state', () => {
        expect(tableSelector.selectTableState(state)).toEqual(state.admin.table);
    });

    it('should get table IDs', () => {
        expect(tableSelector.selectTableIds(state).length).toEqual(1);
    });

    it('should get table entities', () => {
        expect(tableSelector.selectTableEntities(state)).toEqual({ [table]: table });
    });

    it('should get all tables', () => {
        expect(tableSelector.selectAllTables(state).length).toEqual(1);
    });

    it('should get table count', () => {
        expect(tableSelector.selectTableTotal(state)).toEqual(1);
    });

    it('should get tableListIsLoading', () => {
        expect(tableSelector.selectTableListIsLoading(state)).toBe(false);
    });

    it('should get tableListIsLoaded', () => {
        expect(tableSelector.selectTableListIsLoaded(state)).toBe(true);
    });
});
