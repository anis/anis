/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../../admin.reducer';
import * as fromFitsImage from '../reducers/fits-image.reducer';

export const selectFitsImageState = createSelector(
    reducer.getAdminState,
    (state: reducer.State) => state.fitsImage
);

export const selectFitsImageLimits = createSelector(
    selectFitsImageState,
    fromFitsImage.selectFitsImageLimits
);

export const selectFitsImageLimitsIsLoading = createSelector(
    selectFitsImageState,
    fromFitsImage.selectFitsImageLimitsIsLoading
);

export const selectFitsImageLimitsIsLoaded = createSelector(
    selectFitsImageState,
    fromFitsImage.selectFitsImageLimitsIsLoaded
);
