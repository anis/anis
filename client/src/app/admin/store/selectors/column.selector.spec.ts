/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as columnSelector from './column.selector';
import * as fromColumn from '../reducers/column.reducer';
import { Column } from '../models';
import { COLUMN_LIST } from 'src/test-data';

describe('[Admin][store][selectors] Column selector', () => {
    let state: any;
    let column: Column;

    beforeEach(() => {
        column = COLUMN_LIST[0];
        state = {
            admin: {
                column: {
                    ...fromColumn.initialState,
                    columnListIsLoaded: true,
                    ids: [column.name],
                    entities: { [column.name]: column }
                },
            }
        };
    });

    it('should get column state', () => {
        expect(columnSelector.selectColumnState(state)).toEqual(state.admin.column);
    });

    it('should get column IDs', () => {
        expect(columnSelector.selectColumnIds(state).length).toEqual(1);
    });

    it('should get column entities', () => {
        expect(columnSelector.selectColumnEntities(state)).toEqual(state.admin.column.entities);
    });

    it('should get all columns', () => {
        expect(columnSelector.selectAllColumns(state).length).toEqual(1);
    });

    it('should get column count', () => {
        expect(columnSelector.selectColumnTotal(state)).toEqual(1);
    });

    it('should get columnListIsLoading', () => {
        expect(columnSelector.selectColumnListIsLoading(state)).toBe(false);
    });

    it('should get columnListIsLoaded', () => {
        expect(columnSelector.selectColumnListIsLoaded(state)).toBe(true);
    });
});
