/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { FileInfo } from '../models';
import * as adminFileExplorerSelector from './admin-file-explorer.selector';
import { FILE_INFO_LIST } from 'src/test-data';

describe('[Admin][store][selectors] AdminFileExplorer selector', () => {
    let state: any;
    let files: FileInfo[];

    beforeEach(() => {
        files = FILE_INFO_LIST;
        state = {
            admin: {
                adminFileExplorer: {
                    files,
                    filesIsLoading: false,
                    filesIsLoaded: true
                },
            }
        };
    });

    it('should get adminFileExplorer state', () => {
        expect(adminFileExplorerSelector.selectAdminFileExplorerState(state)).toEqual(state.admin.adminFileExplorer);
    });

    it('should get files', () => {
        expect(adminFileExplorerSelector.selectFiles(state)).toEqual(files);
    });

    it('should get filesIsLoading', () => {
        expect(adminFileExplorerSelector.selectFilesIsLoading(state)).toBe(false);
    });

    it('should get filesIsLoaded', () => {
        expect(adminFileExplorerSelector.selectFilesIsLoaded(state)).toBe(true);
    });
});
