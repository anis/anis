/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';

import * as fitsImageActions from '../actions/fits-image.actions';
import { FitsImageLimits } from '../models';

export interface State {
    fitsImageLimits: FitsImageLimits;
    fitsImageLimitsIsLoading: boolean;
    fitsImageLimitsIsLoaded: boolean;
}

export const initialState: State = {
    fitsImageLimits: null,
    fitsImageLimitsIsLoading: false,
    fitsImageLimitsIsLoaded: false
};

export const fitsImageReducer = createReducer(
    initialState,
    on(fitsImageActions.retrieveFitsImageLimits, (state) => ({
        ...state,
        fitsImageLimits: null,
        fitsImageLimitsIsLoading: true,
        fitsImageLimitsIsLoaded: false
    })),
    on(fitsImageActions.retrieveFitsImageLimitsSuccess, (state, { fitsImageLimits }) => ({
        ...state,
        fitsImageLimits,
        fitsImageLimitsIsLoading: false,
        fitsImageLimitsIsLoaded: true
    })),
    on(fitsImageActions.retrieveFitsImageLimitsFail, state => ({
        ...state,
        fitsImageLimitsIsLoading: false,
        fitsImageLimitsIsLoaded: false
    }))
);

export const selectFitsImageLimits = (state: State) => state.fitsImageLimits;
export const selectFitsImageLimitsIsLoading = (state: State) => state.fitsImageLimitsIsLoading;
export const selectFitsImageLimitsIsLoaded = (state: State) => state.fitsImageLimitsIsLoaded;
