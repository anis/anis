/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromFitsImage from './fits-image.reducer';
import * as fitsImageActions from '../actions/fits-image.actions';
import { FITS_IMAGE_LIMITS } from 'src/test-data';

describe('[Admin][store][reducers] FitsImageReducer reducer', () => {
    it('retrieveFitsImageLimits action should set fitsImageLimitsIsLoading to true ', () => {
        const action = fitsImageActions.retrieveFitsImageLimits({ filePath: 'test' });
        const state = fromFitsImage.fitsImageReducer(fromFitsImage.initialState, action);

        expect(state.fitsImageLimitsIsLoading).toBe(true);
        expect(state.fitsImageLimitsIsLoaded).toBe(false);
        expect(state).not.toBe(fromFitsImage.initialState);
    });

    it('retrieveFitsImageLimitsSuccess action should set fitsImageLimitsIsLoaded to true and fitsImageLimitsIsLoading to false ', () => {
        const action = fitsImageActions.retrieveFitsImageLimitsSuccess({ fitsImageLimits: FITS_IMAGE_LIMITS });
        const state = fromFitsImage.fitsImageReducer(fromFitsImage.initialState, action);

        expect(state.fitsImageLimitsIsLoading).toBe(false);
        expect(state.fitsImageLimitsIsLoaded).toBe(true);
        expect(state.fitsImageLimits.ra_min).toEqual(FITS_IMAGE_LIMITS.ra_min);
        expect(state).not.toBe(fromFitsImage.initialState);
    });

    it('retrieveFitsImageLimitsFail action should set fitsImageLimitsIsLoaded to false and fitsImageLimitsIsLoading to false ', () => {
        const action = fitsImageActions.retrieveFitsImageLimitsFail();
        const state = fromFitsImage.fitsImageReducer(fromFitsImage.initialState, action);

        expect(state.fitsImageLimitsIsLoading).toBe(false);
        expect(state.fitsImageLimitsIsLoaded).toBe(false);
    });

    it('should get fitsImageLimitsIs', () => {
        const action = fitsImageActions.retrieveFitsImageLimitsSuccess({ fitsImageLimits: FITS_IMAGE_LIMITS });
        const state = fromFitsImage.fitsImageReducer(fromFitsImage.initialState, action);

        expect(fromFitsImage.selectFitsImageLimits(state)).toEqual(FITS_IMAGE_LIMITS);
    });

    it('should get fitsImageLimitsIsLoading', () => {
        const action = {} as Action;
        const state = fromFitsImage.fitsImageReducer(undefined, action);

        expect(fromFitsImage.selectFitsImageLimitsIsLoading(state)).toEqual(false);
    });
    
    it('should get fitsImageLimitsIsLoaded', () => {
        const action = {} as Action;
        const state = fromFitsImage.fitsImageReducer(undefined, action);

        expect(fromFitsImage.selectFitsImageLimitsIsLoaded(state)).toEqual(false);
    });
});
