/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromColumn from './column.reducer';
import * as columnActions from '../actions/column.actions';
import { COLUMN_LIST } from 'src/test-data';

describe('[Admin][store][reducers] Column reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromColumn.columnReducer(fromColumn.initialState, action);

        expect(state).toBe(fromColumn.initialState);
    });

    it('loadColumnList action should set columnListIsLoading to true', () => {
        const action = columnActions.loadColumnList();
        const state = fromColumn.columnReducer(fromColumn.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.columnListIsLoading).toEqual(true);
        expect(state.columnListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromColumn.initialState);
    });

    it('loadColumnListSuccess action should add column list, set columnListIsLoading to false and set columnListIsLoaded to true', () => {
        const action = columnActions.loadColumnListSuccess({ columns: COLUMN_LIST });
        const state = fromColumn.columnReducer(fromColumn.initialState, action);

        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain(COLUMN_LIST[0].name);
        expect(state.ids).toContain(COLUMN_LIST[1].name);
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.columnListIsLoading).toEqual(false);
        expect(state.columnListIsLoaded).toEqual(true);
        expect(state).not.toBe(fromColumn.initialState);
    });

    it('loadColumnListFail action should set columnListIsLoading to false', () => {
        const action = columnActions.loadColumnListFail();
        const state = fromColumn.columnReducer(fromColumn.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.columnListIsLoading).toEqual(false);
        expect(state.columnListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromColumn.initialState);
    });

    it('should get columnListIsLoading', () => {
        const action = {} as Action;
        const state =  fromColumn.columnReducer(undefined, action);

        expect(fromColumn.selectColumnListIsLoading(state)).toEqual(false);
    });

    it('should get columnListIsLoaded', () => {
        const action = {} as Action;
        const state = fromColumn.columnReducer(undefined, action);

        expect(fromColumn.selectColumnListIsLoaded(state)).toEqual(false);
    });
});
