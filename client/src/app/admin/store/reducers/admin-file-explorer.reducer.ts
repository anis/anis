/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';

import * as adminFileExplorerActions from '../actions/admin-file-explorer.actions';
import { FileInfo } from '../models';

export interface State {
    files: FileInfo[];
    filesIsLoading: boolean;
    filesIsLoaded: boolean;
}

export const initialState: State = {
    files: null,
    filesIsLoading: false,
    filesIsLoaded: false
};

export const adminFileExplorerReducer = createReducer(
    initialState,
    on(adminFileExplorerActions.loadFiles, (state) => ({
        ...state,
        files: null,
        filesIsLoading: true,
        filesIsLoaded: false
    })),
    on(adminFileExplorerActions.loadFilesSuccess, (state, { files }) => ({
        ...state,
        files,
        filesIsLoading: false,
        filesIsLoaded: true
    })),
    on(adminFileExplorerActions.loadFilesFail, state => ({
        ...state,
        filesIsLoading: false,
        filesIsLoaded: false
    }))
);

export const selectFiles = (state: State) => state.files;
export const selectFilesIsLoading = (state: State) => state.filesIsLoading;
export const selectFilesIsLoaded = (state: State) => state.filesIsLoaded;
