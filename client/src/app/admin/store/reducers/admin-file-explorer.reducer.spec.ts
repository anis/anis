/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromAdminFileExplorer from './admin-file-explorer.reducer';
import * as adminFileExplorerActions from '../actions/admin-file-explorer.actions';
import { FILE_INFO_LIST } from 'src/test-data';

describe('[Admin][store][reducers] AdminFileExplorerReducer reducer', () => {
    it('loadFiles action should set filesIsLoading to true ', () => {
        const action = adminFileExplorerActions.loadFiles({ path: 'test' });
        const state = fromAdminFileExplorer.adminFileExplorerReducer(fromAdminFileExplorer.initialState, action);

        expect(state.filesIsLoading).toBe(true);
        expect(state.filesIsLoaded).toBe(false);
        expect(state).not.toBe(fromAdminFileExplorer.initialState);
    });

    it('loadFilesSuccess action should set filesIsLoaded to true and filesIsLoading to false ', () => {
        const action = adminFileExplorerActions.loadFilesSuccess({ files: FILE_INFO_LIST });
        const state = fromAdminFileExplorer.adminFileExplorerReducer(fromAdminFileExplorer.initialState, action);

        expect(state.filesIsLoading).toBe(false);
        expect(state.filesIsLoaded).toBe(true);
        expect(state).not.toBe(fromAdminFileExplorer.initialState);
    });

    it('loadFilesFail action should set filesIsLoading to false and filesIsLoaded to false ', () => {
        const action = adminFileExplorerActions.loadFilesFail();
        const state = fromAdminFileExplorer.adminFileExplorerReducer(fromAdminFileExplorer.initialState, action);

        expect(state.filesIsLoading).toBe(false);
        expect(state.filesIsLoaded).toBe(false);
    });

    it('should get files', () => {
        const action = adminFileExplorerActions.loadFilesSuccess({ files: FILE_INFO_LIST });
        const state = fromAdminFileExplorer.adminFileExplorerReducer(fromAdminFileExplorer.initialState, action);

        expect(fromAdminFileExplorer.selectFiles(state)).toEqual(FILE_INFO_LIST);
    });

    it('should get filesIsLoading', () => {
        const action = {} as Action;
        const state = fromAdminFileExplorer.adminFileExplorerReducer(undefined, action);

        expect(fromAdminFileExplorer.selectFilesIsLoading(state)).toEqual(false);
    });

    it('should get filesIsLoaded', () => {
        const action = {} as Action;
        const state = fromAdminFileExplorer.adminFileExplorerReducer(undefined, action);

        expect(fromAdminFileExplorer.selectFilesIsLoaded(state)).toEqual(false);
    });
});
