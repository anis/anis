/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';

import { FitsImageEffects } from './fits-image.effects';
import { FitsImageService } from '../services/fits-image.service';
import * as fitsImageActions from '../actions/fits-image.actions';
import { FitsImageLimits } from '../models';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import { FITS_IMAGE_LIMITS } from 'src/test-data';

describe('[Admin][store][effects] FitsImageEffects', () => {
    let actions = new Observable();
    let effects: FitsImageEffects;
    let metadata: EffectsMetadata<FitsImageEffects>;
    let service: FitsImageService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let router: Router;
    let mockInstanceSelectorSelectInstanceNameByRoute;
    let mockDatasetSelectorDatasetName;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                FitsImageEffects,
                { provide: FitsImageService, useValue: {} },
                { provide: Router, useValue: { navigate: jest.fn() } },
                {
                    provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }
                },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(FitsImageEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(FitsImageService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
            instanceSelector.selectInstanceNameByRoute,
            'default'
        );
        mockDatasetSelectorDatasetName = store.overrideSelector(
            datasetSelector.selectDatasetNameByRoute,
            'observations'
        );
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadFiles$ effect', () => {
        it('should dispatch the retrieveFitsImageLimitsSuccess action on success', () => {
            const action = fitsImageActions.retrieveFitsImageLimits({ filePath: 'files/image.fits' });
            const outcome = fitsImageActions.retrieveFitsImageLimitsSuccess({ fitsImageLimits: FITS_IMAGE_LIMITS });
            actions = hot('a', { a: action });
            const response = cold('a', { a: FITS_IMAGE_LIMITS });
            const expected = cold('b', { b: outcome });
            service.retrieveFitsImageLimits = jest.fn(() => response);
            expect(effects.retrieveFitsImageLimits$).toBeObservable(expected);
        });

        it('should dispatch the retrieveFitsImageLimitsFail action on failure', () => {
            const action = fitsImageActions.retrieveFitsImageLimits({ filePath: 'files/image.fits' });
            const error = new Error();
            const outcome = fitsImageActions.retrieveFitsImageLimitsFail();
            actions = hot('a', { a: action });
            const response = cold('#', {}, error);
            const expected = cold('b', { b: outcome });
            service.retrieveFitsImageLimits = jest.fn(() => response);
            expect(effects.retrieveFitsImageLimits$).toBeObservable(expected);
        });
    });
});
