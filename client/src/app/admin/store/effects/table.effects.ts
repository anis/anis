/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';

import * as tableActions from '../actions/table.actions';
import { TableService } from '../services/table.service';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';

@Injectable()
export class TableEffects {
    /**
     * Calls action to retrieve table list.
     */
    loadTables$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(tableActions.loadTableList),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            mergeMap(([action, instanceName]) => this.tableService.retrieveTableList(instanceName, action.idDatabase)
                .pipe(
                    map(tables => tableActions.loadTableListSuccess({ tables })),
                    catchError(() => of(tableActions.loadTableListFail()))
                )
            )
        )
    );

    constructor(
        private actions$: Actions,
        private tableService: TableService,
        private store: Store<{ }>
    ) { }
}
