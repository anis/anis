/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';

import { ColumnEffects } from './column.effects';
import { ColumnService } from '../services/column.service';
import * as columnActions from '../actions/column.actions';
import { DATASET, COLUMN_LIST } from 'src/test-data';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import { Dataset } from 'src/app/metamodel/models';

describe('[Admin][store][effects] ColumnEffects', () => {
    let actions = new Observable();
    let effects: ColumnEffects;
    let metadata: EffectsMetadata<ColumnEffects>;
    let service: ColumnService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: { } };
    let dataset: Dataset;
    let mockInstanceSelectorSelectInstanceNameByRoute;
    let mockDatasetSelectorSelectDatasetByRouteName;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ColumnEffects,
                { provide: ColumnService, useValue: { }},
                { provide: Router, useValue: { navigate: jest.fn() }},
                { provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }},
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(ColumnEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(ColumnService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        dataset = { ...DATASET };
        mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
            instanceSelector.selectInstanceNameByRoute,
            'default'
        );
        mockDatasetSelectorSelectDatasetByRouteName = store.overrideSelector(
            datasetSelector.selectDatasetByRouteName,
            dataset
        );
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadColumns$ effect', () => {
        it('should dispatch the loadColumnListSuccess action on success', () => {
            const action = columnActions.loadColumnList();
            const outcome = columnActions.loadColumnListSuccess({ columns: COLUMN_LIST });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: COLUMN_LIST });
            const expected = cold('--b', { b: outcome });
            service.retrieveColumns = jest.fn(() => response);

            expect(effects.loadColumns$).toBeObservable(expected);
            expect(service.retrieveColumns).toHaveBeenCalledWith('default', dataset.id_database, dataset.table_ref);
        });

        it('should dispatch the loadColumnListFail action on HTTP failure', () => {
            const action = columnActions.loadColumnList();
            const error = new Error();
            const outcome = columnActions.loadColumnListFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.retrieveColumns = jest.fn(() => response);

            expect(effects.loadColumns$).toBeObservable(expected);
        });
    });
});
