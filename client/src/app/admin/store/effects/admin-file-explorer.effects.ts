/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';

import * as adminFileExplorerActions from '../actions/admin-file-explorer.actions';
import { AdminFileExplorerService } from '../services/admin-file-explorer.service';

@Injectable()
export class AdminFileExplorerEffects {
    /**
     * Calls action to retrieve file list for the given root directory.
     */
    loadFiles$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(adminFileExplorerActions.loadFiles),
            mergeMap(action => this.adminFileExplorerService.retrieveFiles(action.path)
                .pipe(
                    map(files => adminFileExplorerActions.loadFilesSuccess({ files })),
                    catchError(() => of(adminFileExplorerActions.loadFilesFail()))
                )
            )
        )
    );

    constructor(
        private actions$: Actions,
        private adminFileExplorerService: AdminFileExplorerService
    ) {}
}
