/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';

import { AdminFileExplorerEffects } from './admin-file-explorer.effects';
import { AdminFileExplorerService } from '../services/admin-file-explorer.service';
import * as adminFileExplorerActions from '../actions/admin-file-explorer.actions';
import { FileInfo } from '../models';

describe('[Admin][store][effects] AdminFileExplorerEffects', () => {
    let actions = new Observable();
    let effects: AdminFileExplorerEffects;
    let metadata: EffectsMetadata<AdminFileExplorerEffects>;
    let service: AdminFileExplorerService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                AdminFileExplorerEffects,
                { provide: AdminFileExplorerService, useValue: {} },
                { provide: Router, useValue: { navigate: jest.fn() } },
                {
                    provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }
                },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(AdminFileExplorerEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(AdminFileExplorerService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadFiles$ effect', () => {
        it('should dispatch the loadFilesSuccess action on success', () => {
            const action = adminFileExplorerActions.loadFiles({ path: 'test' });
            let fileInfo: FileInfo = { mimetype: 'test', name: 'test', size: 1000, type: 'test' };
            const outcome = adminFileExplorerActions.loadFilesSuccess({ files: [{ ...fileInfo }] });
            actions = hot('a', { a: action });
            const response = cold('a', { a: [{ ...fileInfo }] });
            const expected = cold('b', { b: outcome });
            service.retrieveFiles = jest.fn(() => response);

            expect(effects.loadFiles$).toBeObservable(expected);
            expect(service.retrieveFiles).toHaveBeenCalledWith('test');
        });
        
        it('should dispatch the loadFilesFail action on failure', () => {
            const action = adminFileExplorerActions.loadFiles({ path: '' });
            const error = new Error();
            const outcome = adminFileExplorerActions.loadFilesFail();
            actions = hot('a', { a: action });
            const response = cold('#', {}, error);
            const expected = cold('b', { b: outcome });
            service.retrieveFiles = jest.fn(() => response);
            expect(effects.loadFiles$).toBeObservable(expected);
        });
    });
});
