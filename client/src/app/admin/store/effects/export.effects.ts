/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { KeycloakService } from 'keycloak-angular';
import FileSaver from 'file-saver';

import * as exportActions from '../actions/export.actions';
import * as authSelector from 'src/app/user/store/selectors/auth.selector';

@Injectable()
export class ExportEffects {
    downloadExport$ = createEffect(() =>
        this.actions$.pipe(
            ofType(exportActions.downloadExport),
            concatLatestFrom(() => this.store.select(authSelector.selectIsAuthenticated)),
            tap(([action, isAuthenticated]) => {
                let url = action.url;
                if (isAuthenticated) {
                    this.keycloak.getToken().then(token => {
                        let separator = '?';
                        if (action.url.indexOf('?') > -1) {
                            separator = '&';
                        }
                        url += `${separator}token=${token}`;
                    });
                }
                this.http.get(url, { responseType: 'blob' }).subscribe(blob => FileSaver.saveAs(blob, action.filename));
            })
        ), { dispatch: false}
    );

    constructor(
        private actions$: Actions,
        private store: Store<{ }>,
        private keycloak: KeycloakService,
        private http: HttpClient
    ) {}
}
