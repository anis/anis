/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';

import * as columnActions from '../actions/column.actions';
import { ColumnService } from '../services/column.service';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';

@Injectable()
export class ColumnEffects {
    /**
     * Calls action to retrieve survey list.
     */
    loadColumns$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(columnActions.loadColumnList),
            concatLatestFrom(() => [
                this.store.select(instanceSelector.selectInstanceNameByRoute),
                this.store.select(datasetSelector.selectDatasetByRouteName)
            ]),
            mergeMap(([, instanceName, dataset]) => this.columnService.retrieveColumns(instanceName, dataset.id_database, dataset.table_ref)
                .pipe(
                    map(columns => columnActions.loadColumnListSuccess({ columns })),
                    catchError(() => of(columnActions.loadColumnListFail()))
                )
            )
        )
    );

    constructor(
        private actions$: Actions,
        private columnService: ColumnService,
        private store: Store<{ }>
    ) {}
}
