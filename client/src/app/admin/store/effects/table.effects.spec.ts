/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';

import { TableEffects } from './table.effects';
import { TableService } from '../services/table.service';
import * as tableActions from '../actions/table.actions';
import * as instanceSelector from 'src/app/metamodel/selectors/instance.selector';
import { TABLE_LIST } from 'src/test-data';

describe('[Admin][store][effects] TableEffects', () => {
    let actions = new Observable();
    let effects: TableEffects;
    let metadata: EffectsMetadata<TableEffects>;
    let service: TableService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let mockInstanceSelectorSelectInstanceNameByRoute;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                TableEffects,
                { provide: TableService, useValue: { }},
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(TableEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(TableService);
        store = TestBed.inject(MockStore);
        mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
            instanceSelector.selectInstanceNameByRoute,
            'default'
        );
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadTables$ effect', () => {
        it('should dispatch the loadTableListSuccess action on success', () => {
            const action = tableActions.loadTableList({ idDatabase: 1 });
            const outcome = tableActions.loadTableListSuccess({ tables: TABLE_LIST });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: TABLE_LIST });
            const expected = cold('--b', { b: outcome });
            service.retrieveTableList = jest.fn(() => response);

            expect(effects.loadTables$).toBeObservable(expected);
        });

        it('should dispatch the loadTableListFail action on HTTP failure', () => {
            const action = tableActions.loadTableList({ idDatabase: 1 });
            const error = new Error();
            const outcome = tableActions.loadTableListFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.retrieveTableList = jest.fn(() => response);

            expect(effects.loadTables$).toBeObservable(expected);
        });
    });
});
