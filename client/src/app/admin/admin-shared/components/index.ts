/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DeleteBtnComponent } from './delete-btn.component';
import { PathSelectFormControlComponent } from './path-select-form-control.component';
import { WebpageFormContentComponent } from './webpage-form-content.component';
import { MonacoEditorComponent } from './monaco-editor.component';

export const adminSharedComponents = [
    DeleteBtnComponent,
    PathSelectFormControlComponent,
    WebpageFormContentComponent,
    MonacoEditorComponent
];
