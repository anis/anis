/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { WebpageFormContentComponent } from './webpage-form-content.component';

describe('[admin][admin-shared][components] WebpageFormContentComponent', () => {
    @Component({ selector: 'app-monaco-editor', template: ''})
    class MonacoEditorStubComponent {
        @Input() value: string;
        @Input() codeType: string;
    }

    let component: WebpageFormContentComponent;
    let fixture: ComponentFixture<WebpageFormContentComponent>;
    
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                WebpageFormContentComponent,
                MonacoEditorStubComponent
            ],
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule
            ]
        });
        fixture = TestBed.createComponent(WebpageFormContentComponent);
        component = fixture.componentInstance;
        component.controlName = 'test';
        component.form = new FormGroup({
            test: new FormControl('test')
        });
        fixture.detectChanges();
    });

    it('should create the component', () => {
        expect(component).toBeTruthy();
    });

    it('onChange(value: string) should set test value to test1 and call markAsDirty()', () => {
        let spy = jest.spyOn(component.form, 'markAsDirty');
        expect(component.form.controls['test'].value).not.toEqual('test1');
        component.onChange('test1');
        expect(component.form.controls['test'].value).toEqual('test1');
        expect(spy).toHaveBeenCalledTimes(1);
    });
});
