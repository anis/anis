/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { SharedModule } from 'src/app/shared/shared.module';
import { adminSharedComponents } from './components';
import { adminServices } from './services';

@NgModule({
    imports: [
        SharedModule
    ],
    declarations: [
        adminSharedComponents
    ],
    providers: [
        adminServices
    ],
    exports: [
        adminSharedComponents
    ]
})
export class AdminSharedModule { }
