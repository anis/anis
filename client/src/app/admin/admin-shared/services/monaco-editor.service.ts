/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { AppConfigService } from 'src/app/app-config.service';

@Injectable({
    providedIn: 'root',
})
export class MonacoEditorService {
    loaded: boolean = false;
    loading: boolean = false;

    public loadingFinished: Subject<void> = new Subject<void>();

    constructor(private config: AppConfigService) {}

    private finishLoading() {
        this.loaded = true;
        this.loading = false;
        this.loadingFinished.next();
    }

    public load() {
        if (this.loading) {
            return;
        }

        this.loading = true;

        // load the assets
        let baseUrl = '/assets/monaco-editor/min/vs';
        if (this.config.baseHref !== '/') {
            baseUrl = this.config.baseHref + baseUrl;
        }

        if (typeof (<any>window).monaco === 'object') {
            this.finishLoading();
            return;
        }

        const onGotAmdLoader: any = () => {
            // load Monaco
            (<any>window).require.config({ paths: { vs: `${baseUrl}` } });
            (<any>window).require([`vs/editor/editor.main`], () => {
                this.finishLoading();
            });
        };

        // load AMD loader, if necessary
        if (!(<any>window).require) {
            const loaderScript: HTMLScriptElement = document.createElement('script');
            loaderScript.type = 'text/javascript';
            loaderScript.src = `${baseUrl}/loader.js`;
            loaderScript.addEventListener('load', onGotAmdLoader);
            document.body.appendChild(loaderScript);
        } else {
            onGotAmdLoader();
        }
    }
}
