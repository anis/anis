/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import TusStoredUpload from './tus/tus-stored-upload';
import TusError from './tus/tus-error';

/**
 * Manage information about uploads with the localStorage.
 * @class
 */
@Injectable()
export class TusStorageService {
    /** tus prefix for keys*/
    private _tus = 'tus';

    /**
     * Add upload information on the localStorage.
     * @method
     * @param {File} file - The file to upload
     * @param {string} uploadUrl - The url to point for the upload
     * @returns {string} The key pointing to the information in the local storage
     */
    addUpload(file: File, uploadUrl: string): string {
        const fingerprint = this._fingerprint(file);

        // Make a specific tus key to not collide with other resources
        const id = Math.round(Math.random() * 1e12);
        const key = `${this._tus}::${fingerprint}::${id}`;

        const upload: TusStoredUpload = {
            fileName: file.name,
            fileSize: file.size,
            creationTime: new Date().toString(),
            uploadUrl: uploadUrl,
        };

        localStorage.setItem(key, JSON.stringify(upload));
        return key;
    }

    /**
     * Remove upload information on the localStorage.
     * @method
     * @param {File} file - The file to upload
     */
    removeUpload(file: File): void {
        const fingerprint = this._fingerprint(file);
        const key = this._findEntries(fingerprint);
        if (key !== '') {
            localStorage.removeItem(key);
            return;
        }

        throw new TusError(
            `Failed to remove detailed informations on the upload for the file:\nkey on the localStorage not retrieved`,
        );
    }

    /**
     * Implement fingerprint for Tus-Upload
     * This is necessary to retrieve information about running uploads.
     * @method
     * @private
     * @param file {File} - The file to compute an id from
     */
    private _fingerprint(file: File) {
        return [
            'tus-br',
            file.name,
            file.type,
            file.size,
            file.lastModified,
        ].join('-');
    }

    /**
     * Retrieve key for a given uploaded file on the localStorage.
     * This method guarantees the unicity for the tus created keys.
     * Minimize the risk for resources collisions on the localStorage.
     * @method
     * @private
     * @param file {File} - The file to compute an id from
     */
    private _findEntries(fingerprint: string): string {
        const prefix = `${this._tus}::${fingerprint}`;
        for (let i = 0; i < localStorage.length; i++) {
            const key = localStorage.key(i);
            // must be unique and found on the first place
            if (key.indexOf(prefix) === 0) {
                return key;
            }
        }

        return '';
    }
}
