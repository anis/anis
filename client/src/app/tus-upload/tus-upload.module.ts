/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TusUploadService } from './tus-upload.service';
import { TusStorageService } from './tus-storage.service';

@NgModule({
    declarations: [],
    imports: [CommonModule],
    providers: [TusUploadService, TusStorageService],
})
export class TusUploadModule {}
