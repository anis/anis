/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import TusOptions from './tus-options';

/**
 * Default Tus Options
 */
export const tusOptions: TusOptions = {
    endpoint: null,
    metadata: {},
    uploadSize: null,
    chunkSize: Infinity,
    retryDelays: { time: 4, delay: 1000 },
    uploadDataDuringCreation: true,
    onProgress: null,
    onSuccess: null,
    onError: null,
    withChecksum: false,
};
