/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Represents data to be stored in the local storage before or during the upload
 * @interface
 */
export default interface TusStoredUpload {
    /** @property name of the file */
    fileName: string;
    /** @property total size of the file */
    fileSize: number;
    /** @property creation time of the upload */
    creationTime: string;
    /** @property file metadata */
    fileMetadata?: { [key: string]: string };
    /** @property upload url for the file*/
    uploadUrl?: string;
}
