/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * An Error that can arise while uploading via TusProtocol
 * @class
 */
export default class TusError extends Error {
    /**
     * @constructor
     * @param {string} msg - The error message
     */
    constructor(msg: string) {
        const tus = '(tus): ';
        const message = tus + msg;
        super(message);
        Object.setPrototypeOf(this, TusError.prototype);
    }
}
