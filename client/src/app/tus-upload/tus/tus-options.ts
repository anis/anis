/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import TusError from './tus-error';

/**
 * Define Tus Options before calls
 * @interface
 */
export default interface TusOptions {
    /** @property where to upload files */
    endpoint?: string | null;
    /** @property metadata to append about files */
    metadata?: { [key: string]: string };
    /** @property maximal size to upload files */
    uploadSize?: number | null;
    /** @property chunkSize in bytes to upload per patch request */
    chunkSize?: number;
    /**
     * @property Object containing the number of trial `time` and the `delay` between each trial
     */
    retryDelays?: { time?: number; delay?: number };
    /** @property upload file silmutaneously while Tus-Creation request to the server */
    uploadDataDuringCreation?: boolean;
    /** @property provide checksum in upload request */
    withChecksum?: boolean;

    /**
     * @property callback on progress
     * @function
     * @param bytesSent {number} The number of bytes sent to the server
     * @param bytesTotal {number} The total number of bytes to send to the server
     */
    onProgress?: ((bytesSent: number, bytesTotal: number) => void) | null;
    /**
     * @property callback on success
     * @function
     * @param key {string} The key in the local storage of the file which is fully uploaded
     */
    onSuccess?: ((key: string) => void) | null;
    /**
     * @property callback on error
     * @function
     * @param error { Error | TusError } The error that happened to handle
     */
    onError?: ((error: Error | TusError) => void) | null;

    /** NOTE(lmenou): Some ideas left out for now.
     * No implementations are provided and options are not supported.
     */
    /** @property send the full size of the file uploaded later */
    // uploadLengthDeferred?: boolean;
    /** @property function generating an ID for files to be uploaded, the latter shall mandatorily be bijective */
    // fingerprint?: (file: File) => string;
    // urlStorage?: string;
    // uploadUrl?: string | null;
    // overridePatchMethod?: boolean;
    // headers?: { [key: string]: string };
    // addRequestId?: boolean;
    // storeFingerprintForResuming?: boolean;
    // removeFingerprintOnSuccess?: boolean;
    // onUploadUrlAvailable?: (() => void) | null;
    /** @property maximal number of uploads in parallel in case of parallel uploads */
    // parallelUploads?: number;
    /** @property beginnning and end to define a parallel uploads part */
    // parallelUploadBoundaries?: { start: number; end: number }[] | null;
    /** @property callback on should retry */
    // onShouldRetry?:
    //     | ((
    //           error: TusError,
    //           retryAttempt: number,
    //           options: TusOptions
    //       ) => boolean)
    //     | null;
    /** @property callback before sending the request */
    // onBeforeRequest?: (req: HttpRequest<any>) => void | Observable<void>;
    /** @property callback before after receiving a response */
    // onAfterResponse?: (
    //     req: HttpRequest<any>,
    //     res: HttpResponse<any>
    // ) => void | Observable<void>;
    /** @property callback on on-chunk complete */
    // onChunkComplete?:
    //     | ((
    //           chunkSize: number,
    //           bytesAccepted: number,
    //           bytesTotal: number
    //       ) => void)
    //     | null;
}
