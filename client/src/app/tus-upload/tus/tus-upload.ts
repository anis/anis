/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Represents the kind of the file uploaded, what this is effectively.
 * @enum
 */
export enum UploadKind {
    SPECTRUM = 'observed spectrum',
    IMAGE = 'image',
}

/**
 * Represents metadata associated to the upload.
 * Using snake_case here to ease the use in python backend.
 * @interface
 */
export interface FileMetaData {
    /** @property identifier of the target associated with the product file */
    identifier: string;
    /** @property product_type of data contained in the file (spectrum, image, etc)*/
    product_type: UploadKind;
    /** @property version of the product sent */
    version: number;
    /** @property level of the product sent */
    level: string;
    /** @property description of the product sent */
    description?: string;
    /**
     @property template_id under which data are presented (instrument/observatory
     dependent unique identifier to get information about processing)
     */
    template_id: string;
    /**
     @property datasetTag under which data can be classified
     */
    dataset_tag?: string;
    /**
     @property providerTag under which provider data can be classified
     */
    provider_tag?: string;
}

/**
 * Contains data and information necessary for the upload.
 * @interface
 */
export default interface TusUpload {
    /** @property file to upload */
    file: File;
    /** @property file metadata */
    fileMetadata: FileMetaData;
}
