/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

// TODO(lmenou): Come back to this when have time

import { TestBed } from '@angular/core/testing';
import {
    HttpClientTestingModule,
    HttpTestingController,
} from '@angular/common/http/testing';

import { TusUploadService } from './tus-upload.service';
import { TusStorageService } from './tus-storage.service';
import TusOptions from './tus/tus-options';
import { EMPTY } from 'rxjs';
import {
    HttpHeaderResponse,
    HttpHeaders,
    HttpResponse,
} from '@angular/common/http';

import TusUpload, { UploadKind, FileMetaData } from './tus/tus-upload';

describe('TusUploadService', () => {
    let service: TusUploadService;
    let httpTestingController: HttpTestingController;

    const tusStorage = {
        addUpload: jest.fn(),
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                TusUploadService,
                { provide: TusStorageService, useValue: tusStorage },
            ],
        });
        service = TestBed.inject(TusUploadService);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should call options properly', () => {
        const options: TusOptions = {
            endpoint: 'https://coucou',
        };
        service.options(options);
        const allOptions = {
            endpoint: 'https://coucou',
            metadata: {},
            uploadSize: null,
            chunkSize: Infinity,
            withChecksum: false,
            retryDelays: { time: 4, delay: 1000 },
            uploadDataDuringCreation: true,
            onProgress: null,
            onSuccess: null,
            onError: null,
        };

        expect((service as any)._options).toEqual(allOptions);
    });

    describe('#upload method', () => {
        it('should error if options is not called', () => {
            const file = new File(['<p></p>'], 'myFile', { type: 'text/html' });
            const metaData: FileMetaData = {
                identifier: 'asdf',
                product_type: UploadKind.SPECTRUM,
                version: 1,
                level: 'Lg0',
                template_id: 'HARPS',
            };
            const upload: TusUpload = {
                file: file,
                fileMetadata: metaData,
            };
            const spyConsole = jest
                .spyOn(console, 'error')
                .mockImplementation(jest.fn());
            const empty = service.upload(upload);

            expect(empty).toEqual(EMPTY);
            expect(spyConsole).toHaveBeenCalledWith(
                '(tus): No options was provided,\nPlease call options method to setup the upload first.',
            );
        });

        it('should error if no endpoint is provided', () => {
            const file = new File(['<p></p>'], 'myFile', { type: 'text/html' });
            const metaData: FileMetaData = {
                identifier: 'asdf',
                product_type: UploadKind.SPECTRUM,
                version: 1,
                level: 'Lg0',
                template_id: 'HARPS',
            };
            const upload: TusUpload = {
                file: file,
                fileMetadata: metaData,
            };
            const spyConsole = jest
                .spyOn(console, 'error')
                .mockImplementation(jest.fn());
            service.options({});
            const empty = service.upload(upload);

            expect(empty).toEqual(EMPTY);
            expect(spyConsole).toHaveBeenCalledWith(
                '(tus): No upload endpoint was provided!',
            );
        });

        it('should error if uploadSize is greater than the file size', () => {
            const file = new File(['<p></p>'], 'myFile', { type: 'text/html' });
            const metaData: FileMetaData = {
                identifier: 'asdf',
                product_type: UploadKind.SPECTRUM,
                version: 1,
                level: 'Lg0',
                template_id: 'HARPS',
            };
            const upload: TusUpload = {
                file: file,
                fileMetadata: metaData,
            };
            const options: TusOptions = {
                endpoint: 'endpoint',
                uploadSize: 3,
                uploadDataDuringCreation: false,
            };
            service.options(options);

            const spyConsole = jest
                .spyOn(console, 'error')
                .mockImplementation(jest.fn());

            const empty = service.upload(upload);

            expect(empty).toEqual(EMPTY);
            expect(spyConsole).toHaveBeenCalledWith(
                '(tus): The given file to upload has a size bigger than the maximal authorized one',
            );
        });

        /* testing a single one-shot upload here for the flow */
        // comments are here for demonstration and explanation purpose
        describe('should return an observable if everything ok', () => {
            it('without checksum', () => {
                // mock the server POST response
                const postHeader = new HttpHeaders().set(
                    'location',
                    'endpoint/key',
                );
                const postResponse = new HttpResponse<HttpHeaderResponse>({
                    body: null,
                    headers: postHeader,
                });

                const spyEmitSuccess = jest.spyOn(
                    service as any,
                    '_emitSuccess',
                );

                /* File to upload with 7 bytes in it */
                const file = new File(['<p></p>'], 'myFile', {
                    type: 'text/html',
                });
                const metaData: FileMetaData = {
                    identifier: 'asdf',
                    product_type: UploadKind.SPECTRUM,
                    version: 1,
                    level: 'Lg0',
                    template_id: 'HARPS',
                };
                const upload: TusUpload = {
                    file: file,
                    fileMetadata: metaData,
                };
                // Fake endpoint
                const options: TusOptions = {
                    endpoint: 'endpoint',
                    uploadDataDuringCreation: false,
                    retryDelays: { time: 0, delay: 0 },
                    onSuccess: jest.fn(),
                    onProgress: jest.fn(),
                };
                service.options(options);
                // Subscribe to start a request to the mock server
                // Observables emission are configured later on
                service.upload(upload).subscribe();

                // The request shall be on the fake endpoint
                const req = httpTestingController.expectOne('endpoint');
                // It shall be a POST
                expect(req.request.method).toEqual('POST');

                /* Make the fake server respond the POST request with postResponse */
                req.event(postResponse);

                // Mock the server PATCH response
                const patchHeader = new HttpHeaders().set('Upload-Offset', '7');
                const patchResponse = new HttpResponse<HttpHeaderResponse>({
                    headers: patchHeader,
                });

                // The following request shall be on the endpoint *key*
                const secondReq =
                    httpTestingController.expectOne('endpoint/key');
                // It shall be a PATCH
                expect(secondReq.request.method).toEqual('PATCH');

                /* Make the fake server respond the PATCH request with patchResponse */
                secondReq.event(patchResponse);

                /* Check that there is not pending request
                 * i.e success of the upload
                 */
                httpTestingController.verify();

                expect(spyEmitSuccess).toHaveBeenCalled();
            });
        });

        describe('shall fail gracefully if an error occur', () => {
            it('if server error', () => {
                // bad response
                const postResponse = new HttpResponse<HttpHeaderResponse>({
                    status: 500,
                });

                const spyConsole = jest
                    .spyOn(console, 'error')
                    .mockImplementation(jest.fn());

                const spyEmitError = jest.spyOn(service as any, '_emitError');

                const file = new File(['<p></p>'], 'myFile', {
                    type: 'text/html',
                });
                const metaData: FileMetaData = {
                    identifier: 'asdf',
                    product_type: UploadKind.SPECTRUM,
                    version: 1,
                    level: 'Lg0',
                    template_id: 'HARPS',
                };
                const upload: TusUpload = {
                    file: file,
                    fileMetadata: metaData,
                };
                const options: TusOptions = {
                    endpoint: 'endpoint',
                    uploadDataDuringCreation: false,
                    retryDelays: { time: 0, delay: 0 },
                    onError: jest.fn(),
                };
                service.options(options);
                service.upload(upload).subscribe();

                const req = httpTestingController.expectOne('endpoint');
                expect(req.request.method).toEqual('POST');

                req.event(postResponse);

                httpTestingController.verify();

                expect(spyEmitError).toHaveBeenCalledTimes(1);
                expect(spyConsole).toHaveBeenCalledWith(
                    '(tus): Unexpected response while creating upload',
                );
            });

            it('if wrong response (no location for instance)', () => {
                // uncomplete response => no location
                const postResponse = new HttpResponse<HttpHeaderResponse>({
                    status: 200,
                });

                const spyConsole = jest
                    .spyOn(console, 'error')
                    .mockImplementation(jest.fn());

                const spyEmitError = jest.spyOn(service as any, '_emitError');

                const file = new File(['<p></p>'], 'myFile', {
                    type: 'text/html',
                });
                const metaData: FileMetaData = {
                    identifier: 'asdf',
                    product_type: UploadKind.SPECTRUM,
                    version: 1,
                    level: 'Lg0',
                    template_id: 'HARPS',
                };
                const upload: TusUpload = {
                    file: file,
                    fileMetadata: metaData,
                };
                const options: TusOptions = {
                    endpoint: 'endpoint',
                    uploadDataDuringCreation: false,
                    retryDelays: { time: 0, delay: 0 },
                    onError: jest.fn(),
                };
                service.options(options);
                service.upload(upload).subscribe();

                const req = httpTestingController.expectOne('endpoint');
                expect(req.request.method).toEqual('POST');

                req.event(postResponse);

                httpTestingController.verify();

                expect(spyEmitError).toHaveBeenCalledTimes(1);
                expect(spyConsole).toHaveBeenCalledWith(
                    '(tus): Invalid or missing Location header in server response',
                );
            });

            it('if wrong response (Upload-Offset IsNaN)', () => {
                // bad response => Upload-Offset is not a number
                const postHeaders = new HttpHeaders({
                    location: 'endpoint/key',
                });
                const postResponse = new HttpResponse<HttpHeaderResponse>({
                    headers: postHeaders,
                    status: 200,
                });

                const spyConsole = jest
                    .spyOn(console, 'error')
                    .mockImplementation(jest.fn());

                const spyEmitError = jest.spyOn(service as any, '_emitError');

                const file = new File(['<p></p>'], 'myFile', {
                    type: 'text/html',
                });
                const metaData: FileMetaData = {
                    identifier: 'asdf',
                    product_type: UploadKind.SPECTRUM,
                    version: 1,
                    level: 'Lg0',
                    template_id: 'HARPS',
                };
                const upload: TusUpload = {
                    file: file,
                    fileMetadata: metaData,
                };
                const options: TusOptions = {
                    endpoint: 'endpoint',
                    uploadDataDuringCreation: false,
                    retryDelays: { time: 0, delay: 0 },
                    onError: jest.fn(),
                };
                service.options(options);
                service.upload(upload).subscribe();

                const req = httpTestingController.expectOne('endpoint');
                expect(req.request.method).toEqual('POST');

                req.event(postResponse);

                let patchHeader = new HttpHeaders({
                    'Upload-Offset': '1',
                });
                let patchResponse = new HttpResponse<HttpHeaderResponse>({
                    headers: patchHeader,
                });

                const secondReq =
                    httpTestingController.expectOne('endpoint/key');
                // FIXME(lmenou): Clarify why I have to send a post here
                expect(req.request.method).toEqual('POST');

                secondReq.event(patchResponse);

                patchHeader = new HttpHeaders({
                    'Upload-Offset': 'coucou',
                });
                patchResponse = new HttpResponse<HttpHeaderResponse>({
                    headers: patchHeader,
                });
                const thirdReq =
                    httpTestingController.expectOne('endpoint/key');
                expect(thirdReq.request.method).toEqual('PATCH');

                thirdReq.event(patchResponse);

                httpTestingController.verify();

                expect(spyEmitError).toHaveBeenCalledTimes(1);
                expect(spyConsole).toHaveBeenCalledWith('(tus): Offset isNaN');
            });
        });

        it('should abort properly', () => {
            const postHeader = new HttpHeaders().set(
                'location',
                'endpoint/key',
            );
            const postResponse = new HttpResponse<HttpHeaderResponse>({
                body: null,
                headers: postHeader,
            });

            const file = new File(['<p></p>'], 'myFile', {
                type: 'text/html',
            });
            const metaData: FileMetaData = {
                identifier: 'asdf',
                product_type: UploadKind.SPECTRUM,
                version: 1,
                level: 'Lg0',
                template_id: 'HARPS',
            };
            const upload: TusUpload = {
                file: file,
                fileMetadata: metaData,
            };
            const options: TusOptions = {
                endpoint: 'endpoint',
                uploadDataDuringCreation: false,
                retryDelays: { time: 0, delay: 0 },
                onProgress: jest.fn(),
            };
            service.options(options);

            service.abort();
            expect((service as any)._abort).toBeTruthy();

            service.upload(upload).subscribe();

            const req = httpTestingController.expectOne('endpoint');
            expect(req.request.method).toEqual('POST');

            req.event(postResponse);

            // Mock the server PATCH response
            const patchHeader = new HttpHeaders().set('Upload-Offset', '7');
            const patchResponse = new HttpResponse<HttpHeaderResponse>({
                headers: patchHeader,
            });

            // The following request shall be on the endpoint *key*
            const secondReq = httpTestingController.expectOne('endpoint/key');
            // It shall be a PATCH
            expect(secondReq.request.method).toEqual('PATCH');

            /* Make the fake server respond the PATCH request with patchResponse */
            secondReq.event(patchResponse);

            httpTestingController.verify();
        });
    });

    describe('#resume method', () => {
        it('shall resume if everything ok', () => {
            const spyEmitSuccess = jest.spyOn(service as any, '_emitSuccess');

            // setting the endpoint if we resume
            (service as any)._url = 'endpoint/key';
            // resume the following file
            const file = new File(['<p></p>'], 'myFile', { type: 'text/html' });
            (service as any)._file = file;

            // server respond it has 4 bytes
            const headHeaders = new HttpHeaders().set('Upload-Offset', '4');
            const headResponse = new HttpResponse<HttpHeaderResponse>({
                headers: headHeaders,
            });

            const options: TusOptions = {
                endpoint: 'url',
                uploadDataDuringCreation: false,
                retryDelays: { time: 0, delay: 0 },
            };
            service.options(options);

            // upload shall be aborted
            service.abort();

            // resume
            service.resume().subscribe();

            const req = httpTestingController.expectOne('endpoint/key');
            expect(req.request.method).toEqual('HEAD');

            // send the server respond
            req.event(headResponse);

            // upload completed
            const patchHeaders = new HttpHeaders().set('Upload-Offset', '7');
            const patchResponse = new HttpResponse<HttpHeaderResponse>({
                headers: patchHeaders,
            });

            const newReq = httpTestingController.expectOne('endpoint/key');
            expect(newReq.request.method).toEqual('PATCH');

            // server respond upload is complete
            newReq.event(patchResponse);

            httpTestingController.verify();

            expect(spyEmitSuccess).toHaveBeenCalled();
        });

        it('shall raise an error in no endpoint!', () => {
            const tusOptions: TusOptions = {};
            service.options(tusOptions);
            const spyConsole = jest
                .spyOn(console, 'error')
                .mockImplementation(jest.fn());

            service.resume().subscribe();
            expect(spyConsole).toHaveBeenCalled();
        });

        it('shall abort properly if needed', () => {
            const spyEmitSuccess = jest.spyOn(service as any, '_emitSuccess');

            // setting the endpoint if we resume
            (service as any)._url = 'endpoint/key';
            // resume the following file
            const file = new File(['<p></p>'], 'myFile', { type: 'text/html' });
            (service as any)._file = file;

            // server respond it has 4 bytes
            const headHeaders = new HttpHeaders().set('Upload-Offset', '4');
            const headResponse = new HttpResponse<HttpHeaderResponse>({
                headers: headHeaders,
            });

            const options: TusOptions = {
                endpoint: 'url',
                uploadDataDuringCreation: false,
                retryDelays: { time: 0, delay: 0 },
            };
            service.options(options);
            // upload shall be aborted
            service.abort();
            // resume
            service.resume().subscribe();

            const req = httpTestingController.expectOne('endpoint/key');
            expect(req.request.method).toEqual('HEAD');

            // send the server respond
            req.event(headResponse);

            // upload completed
            const patchHeaders = new HttpHeaders().set('Upload-Offset', '7');
            const patchResponse = new HttpResponse<HttpHeaderResponse>({
                headers: patchHeaders,
            });

            // re-abort the upload
            service.abort();

            const newReq = httpTestingController.expectOne('endpoint/key');
            expect(newReq.request.method).toEqual('PATCH');

            // server respond upload is aborted
            newReq.event(patchResponse);

            // no third req because abortion

            httpTestingController.verify();

            expect(spyEmitSuccess).toHaveBeenCalledTimes(1);
        });

        it('shall handle error gracefully', () => {
            // setting the endpoint if we resume
            (service as any)._url = 'endpoint/key';
            const file = new File(['<p></p>'], 'myFile', { type: 'text/html' });
            (service as any)._file = file;

            const spyConsole = jest
                .spyOn(console, 'error')
                .mockImplementation(jest.fn());

            const spyEmitError = jest.spyOn(service as any, '_emitError');

            // bad response
            const headResponse = new HttpResponse<HttpHeaderResponse>({
                status: 500,
            });

            const options: TusOptions = {
                endpoint: 'url',
                uploadDataDuringCreation: false,
                retryDelays: { time: 0, delay: 0 },
                onError: jest.fn(),
            };
            service.options(options);

            service.abort();
            service.resume().subscribe();

            const req = httpTestingController.expectOne('endpoint/key');
            expect(req.request.method).toEqual('HEAD');

            req.event(headResponse);

            httpTestingController.verify();

            expect(spyEmitError).toHaveBeenCalledTimes(1);
            expect(spyConsole).toHaveBeenCalledWith(
                '(tus): Unexpected response while performing the upload',
            );
        });
    });

    describe('#delete method', () => {
        it('shall delete if everything ok', () => {
            // server has the termination extension
            const optionsHeader = new HttpHeaders({
                'Tus-Extension': 'termination',
            });
            const optionsResponse = new HttpResponse<HttpHeaderResponse>({
                headers: optionsHeader,
            });
            const tusOptions: TusOptions = {
                endpoint: 'endpoint',
            };
            service.options(tusOptions);
            service.delete().subscribe();

            const req = httpTestingController.expectOne('endpoint');
            expect(req.request.method).toEqual('OPTIONS');

            req.event(optionsResponse);

            httpTestingController.verify();
        });

        it('shall raise an error if no endpoint!', () => {
            // boom! no endpoint!
            const tusOptions: TusOptions = {};

            const spyConsole = jest
                .spyOn(console, 'error')
                .mockImplementation(jest.fn());
            service.options(tusOptions);
            service.delete().subscribe();

            expect(spyConsole).toHaveBeenCalled();
        });

        it('shall handle error gracefully', () => {
            const spyEmitError = jest.spyOn(service as any, '_emitError');
            const consoleError = jest
                .spyOn(console, 'error')
                .mockImplementation(jest.fn());

            const optionsResponse = new HttpResponse({
                status: 500,
            });
            const tusOptions: TusOptions = {
                endpoint: 'endpoint',
                retryDelays: { time: 0, delay: 0 },
            };
            service.options(tusOptions);
            service.delete().subscribe();

            const req = httpTestingController.expectOne('endpoint');
            expect(req.request.method).toEqual('OPTIONS');

            req.event(optionsResponse);

            httpTestingController.verify();

            expect(spyEmitError).toHaveBeenCalledTimes(1);
            expect(consoleError).toHaveBeenCalledTimes(1);
        });

        it('shall handle no termination extension gracefully', () => {
            const spyEmitError = jest.spyOn(service as any, '_emitError');
            const consoleError = jest
                .spyOn(console, 'error')
                .mockImplementation(jest.fn());

            // server has the termination extension
            const optionsHeader = new HttpHeaders({
                'Tus-Extension': 'creation',
            });
            const optionsResponse = new HttpResponse<HttpHeaderResponse>({
                headers: optionsHeader,
            });
            const tusOptions: TusOptions = {
                endpoint: 'endpoint',
                retryDelays: { time: 0, delay: 0 },
            };
            service.options(tusOptions);
            service.delete().subscribe();

            const req = httpTestingController.expectOne('endpoint');
            expect(req.request.method).toEqual('OPTIONS');

            req.event(optionsResponse);

            httpTestingController.verify();

            expect(spyEmitError).toHaveBeenCalledTimes(1);
            expect(consoleError).toHaveBeenCalledTimes(1);
        });
    });

    describe('#abort method', () => {
        it('shall raise an error if no endpoint!', () => {
            const tusOptions: TusOptions = {};
            service.options(tusOptions);

            const spyConsole = jest
                .spyOn(console, 'error')
                .mockImplementation(jest.fn());

            service.abort();
            expect(spyConsole).toHaveBeenCalled();
        });

        it('shall set abort properties properly', () => {
            const tusOptions: TusOptions = {
                endpoint: 'upload/here',
            };
            service.options(tusOptions);

            const spyConsole = jest
                .spyOn(console, 'error')
                .mockImplementation(jest.fn());

            service.abort(true);
            expect(spyConsole).not.toHaveBeenCalled();
            expect((service as any)._abort).toBeTruthy();
            expect((service as any)._toTerminate).toBeTruthy();
        });
    });

    describe('#_createMetaDataHeader method', () => {
        it('should concatenate properly metadata', () => {
            (service as any)._options = {};
            (service as any)._options.metadata = {
                identifier: 'asdf',
                product_type: 'spectrum',
                filename: 'coconut',
            };
            const res = (service as any)._createMetaDataHeader();

            expect(res).toEqual(
                'identifier asdf,product_type spectrum,filename coconut',
            );
        });
    });
});
