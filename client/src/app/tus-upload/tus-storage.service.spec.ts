/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';

import { TusStorageService } from './tus-storage.service';

describe('TusStorageService', () => {
    let service: TusStorageService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [TusStorageService],
        });
        service = TestBed.inject(TusStorageService);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should add upload information', () => {
        const file = new File(['<p></p>'], 'myFile', { type: 'text/html' });
        const url = 'https://tus.io/upload';

        const spyStorage = jest
            .spyOn(Storage.prototype, 'setItem')
            .mockImplementation(() => null);

        service.addUpload(file, url);
        expect(spyStorage).toHaveBeenCalled();
    });

    describe('should delete upload information', () => {
        it('and manage an error by throwing it', () => {
            const file = new File(['<p></p>'], 'myFile', { type: 'text/html' });

            expect(() => {
                service.removeUpload(file);
            }).toThrow();
        });

        it('or delete it properly if no error', () => {
            const file = new File(['<p></p>'], 'myFile', { type: 'text/html' });

            const spyFindEntries = jest
                .spyOn(service as any, '_findEntries')
                .mockImplementation(() => 'key');

            const spyStorage = jest
                .spyOn(Storage.prototype, 'removeItem')
                .mockImplementation(() => null);

            service.removeUpload(file);
            expect(spyFindEntries).toHaveBeenCalled();
            expect(spyStorage).toHaveBeenCalled();
        });
    });
});
