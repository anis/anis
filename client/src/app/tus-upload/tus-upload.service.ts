/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import {
    HttpClient,
    HttpHeaderResponse,
    HttpHeaders,
    HttpResponse,
} from '@angular/common/http';

import TusOptions from './tus/tus-options';
import TusError from './tus/tus-error';
import {
    switchMap,
    expand,
    catchError,
    EMPTY,
    Observable,
    retry,
    from,
    map,
} from 'rxjs';

import { tusOptions } from './tus/defaults';
import { TusStorageService } from './tus-storage.service';
import TusUpload from './tus/tus-upload';

/**
 * Manage the tus upload protocol.
 * This class shall be injected on a component basis via the `providers` array of the `@Component` decorator argument.
 * One instance of a service manages one upload.
 * @class
 */
@Injectable()
export class TusUploadService {
    /** The options of the upload. */
    private _options: TusOptions = null;
    /** The file to upload. */
    private _file: File = null;
    /** The key to retrieve information about the upload in the local storage. */
    private _key: string = null;
    /** The offset at which the current upload is. */
    private _offset: number = null;
    /** The url on which to perform the upload. */
    private _url = null;
    /** The flag to abort the upload or not. */
    private _abort = false;
    /** The flag to abort and terminate the upload or not. */
    private _toTerminate = false;

    constructor(
        private httpClient: HttpClient,
        private tusStorage: TusStorageService,
    ) {}

    /**
     * Set the options of the upload itself.
     * This method is to be called before any call to the upload method to set properly all the options of the upload method.
     * @method
     */
    options(options: TusOptions) {
        this._options = { ...tusOptions, ...options };
    }

    /**
     * Performs the upload itself.
     * @method
     * @returns An observable of the header response of the server or EMPTY if the upload is finished or aborted.
     */
    upload(
        upload: TusUpload,
    ): Observable<HttpResponse<HttpHeaderResponse> | never> {
        if (this._options === null) {
            const err = new TusError(
                'No options was provided,\nPlease call options method to setup the upload first.',
            );
            console.error(err.message);
            return EMPTY;
        }

        if (!this._options.endpoint) {
            const err = new TusError('No upload endpoint was provided!');
            console.error(err.message);
            return EMPTY;
        }

        if (
            this._options.uploadSize !== null &&
            this._options.uploadSize < upload.file.size
        ) {
            const err = new TusError(
                'The given file to upload has a size bigger than the maximal authorized one',
            );
            console.error(err.message);
            return EMPTY;
        }

        this._file = upload.file;

        if (typeof btoa !== 'function') {
            const err = new TusError('btoa unavailable on the platform');
            console.error(err.message);
            return EMPTY;
        }

        this._options.metadata = {
            filename: btoa(this._file.name),
        };

        for (let [key, value] of Object.entries(upload.fileMetadata)) {
            this._options.metadata[key] = btoa(value);
        }

        let initialRequest: Observable<HttpResponse<HttpHeaderResponse>> = null;
        if (!this._options.uploadDataDuringCreation) {
            initialRequest = this.httpClient.post<HttpHeaderResponse>(
                this._options.endpoint,
                null,
                { headers: this._createUploadHeaders(), observe: 'response' },
            );
        } else {
            initialRequest = this._createWithUploadHeaders().pipe(
                switchMap(([body, headers]) =>
                    this.httpClient.post<HttpHeaderResponse>(
                        this._options.endpoint,
                        body,
                        { headers: headers, observe: 'response' },
                    ),
                ),
            );
        }

        return initialRequest.pipe(
            switchMap((response: HttpResponse<HttpHeaderResponse>) =>
                this._startUpload(response),
            ),
            expand((response: HttpResponse<HttpHeaderResponse>) => {
                this._readOffset(response);
                if (this._offset === this._file.size) {
                    // Upload is done here
                    return this._emitSuccess();
                }
                if (this._abort) {
                    return this._performAbortion(response);
                }
                return this._performUpload();
            }),
            retry({
                count: this._options.retryDelays.time,
                delay: this._options.retryDelays.delay,
            }),
            catchError((err: TusError | Error) => this._emitError(err)),
        );
    }

    /**
     * Abort the upload immediately.
     * @method
     * @param shouldTerminate {boolean} - If set to true, terminate the upload by sending a `delete` request to the server.
     */
    abort(shouldTerminate?: boolean): void {
        if (!this._options.endpoint) {
            const err = new TusError('No upload endpoint was provided!');
            console.error(err.message);
            return;
        }
        this._abort = true;
        if (shouldTerminate && shouldTerminate === true) {
            this._toTerminate = true;
        }
    }

    /**
     * Tus upload are resumable.
     * Tus-Core take place here.
     * Resume the upload after an abort or a fail if upload was not terminated.
     * @method
     * @returns An observable of the header response of the server or EMPTY if the upload is finished or aborted.
     */
    resume(): Observable<HttpResponse<HttpHeaderResponse> | never> {
        if (!this._options.endpoint) {
            const err = new TusError('No upload endpoint was provided!');
            console.error(err.message);
            return EMPTY;
        }

        const headers = new HttpHeaders({
            'Tus-Resumable': '1.0.0',
        });

        this._abort = false;

        return this.httpClient
            .head<HttpHeaderResponse>(this._url, {
                headers: headers,
                observe: 'response',
            })
            .pipe(
                expand((response: HttpResponse<HttpHeaderResponse>) => {
                    this._readOffset(response);
                    if (this._offset === this._file.size) {
                        return this._emitSuccess();
                    }
                    if (this._abort) {
                        return this._performAbortion(response);
                    }
                    return this._performUpload();
                }),
                retry({
                    count: this._options.retryDelays.time,
                    delay: this._options.retryDelays.delay,
                }),
                catchError((err) => this._emitError(err)),
            );
    }

    /**
     * Terminate the upload by sending a `delete` request to the server.
     * Tus-Termination Extension take place here.
     * @returns An observable of the header response of the server or `EMPTY` if the deletion is finished or aborted.
     */
    delete(): Observable<HttpResponse<HttpHeaderResponse> | never> {
        if (!this._options.endpoint) {
            const err = new TusError('No upload endpoint was provided!');
            console.error(err.message);
            return EMPTY;
        }

        // Delete on the server
        return this.httpClient
            .options<HttpHeaderResponse>(this._options.endpoint, {
                observe: 'response',
            })
            .pipe(
                switchMap((response: HttpResponse<HttpHeaderResponse>) => {
                    if (!response.ok) {
                        throw new TusError(
                            'An error happened while requesting tus extensions',
                        );
                    }
                    const isTermination: boolean =
                        this._isTermination(response);
                    if (isTermination) {
                        return this._delete();
                    }

                    throw new TusError(
                        'Tus-Extension termination is not supported by the server',
                    );
                }),
                retry({
                    count: this._options.retryDelays.time,
                    delay: this._options.retryDelays.delay,
                }),
                catchError((err) => this._emitError(err)),
            );
    }

    /**
     * Create the `Upload-Metadata` header from the metadata provided with the upload.
     * @method
     * @private
     */
    private _createMetaDataHeader(): string {
        let header: string = '';
        Object.entries(this._options.metadata).forEach(([key, value]) => {
            header = header.concat(',', `${key} ${value}`);
        });
        return header.slice(1);
    }

    /**
     * Set the headers for a request that handles the tus creation extension
     * @method
     * @private
     * @throws a `TusError` if the `btoa` function is not available on the platform
     */
    private _createUploadHeaders(): HttpHeaders | never {
        const headers = new HttpHeaders({
            'Tus-Resumable': '1.0.0',
            'Upload-Metadata': this._createMetaDataHeader(),
            'Upload-Length': this._file.size.toString(),
        });
        return headers;
    }

    private _createWithUploadHeaders(): Observable<[Blob, HttpHeaders]> {
        const blob: Blob = this._file.slice(0, this._options.chunkSize);
        const headers = new HttpHeaders({
            'Tus-Resumable': '1.0.0',
            'Upload-Metadata': this._createMetaDataHeader(),
            'Upload-Length': this._file.size.toString(),
            'Content-Type': 'application/offset+octet-stream',
            'Content-Length': this._file.size.toString(),
        });

        return from(this._checksumOfData(blob)).pipe(
            map((hash: string) => [blob, headers.set('Upload-Checksum', hash)]),
        );
    }

    /**
     * Start the upload after the Tus-Creation extension succeeded.
     * @method
     * @private
     * @param response {HttpResponse<HttpHeaderResponse>} - The response of the tus creation request to the server.
     * @returns An observable of the header response of the server or EMPTY if the upload is finished or aborted.
     * @throws An `Observable<never>` type error with an error message if an error occurs.
     */
    private _startUpload(
        response: HttpResponse<HttpHeaderResponse>,
    ): Observable<HttpResponse<HttpHeaderResponse> | never> {
        if (!response.ok) {
            throw new TusError('Unexpected response while creating upload');
        }

        const location: string = response.headers.get('location');
        if (location == null) {
            throw new TusError(
                'Invalid or missing Location header in server response',
            );
        }
        this._url = location;

        // Store the upload here for resumability
        const key = this.tusStorage.addUpload(this._file, this._url);
        this._key = key;

        const _offset = response.headers.get('Upload-Offset');
        this._offset = _offset === null ? 0 : +_offset;

        const request = this._performUpload();
        return request;
    }

    /**
     * Perform the upload itself
     * if `withChecksum === true` a checksum of the data is added in the header of the request
     * @method
     * @private
     * @param response {HttpResponse<HttpHeaderResponse>} - The response from the server after an upload(PATCH) request if any.
     * @returns An observable of the header response of the server or EMPTY if the upload is finished or aborted.
     * @throws An `Observable<never>` type error with an error message if an error occurs.
     */
    private _performUpload(): Observable<
        HttpResponse<HttpHeaderResponse> | never
    > {
        const start: number = this._offset;
        let end: number = this._offset + this._options.chunkSize;
        if (end === Infinity || end > this._file.size) {
            end = this._file.size;
        }
        const blob: Blob = this._file.slice(start, end);

        const headers: HttpHeaders = new HttpHeaders({
            'Tus-Resumable': '1.0.0',
            'Content-Type': 'application/offset+octet-stream',
            'Upload-Offset': this._offset.toString(),
        });

        if (this._options.withChecksum) {
            return from(this._checksumOfData(blob)).pipe(
                switchMap((hash: string) =>
                    this.httpClient.patch<HttpHeaderResponse>(this._url, blob, {
                        headers: headers.set('Upload-Checksum', hash),
                        observe: 'response',
                    }),
                ),
            );
        }

        return this.httpClient.patch<HttpHeaderResponse>(this._url, blob, {
            headers: headers,
            observe: 'response',
        });
    }

    /**
     * Perform the abortion of the upload itself
     * @method
     * @private
     * @param response {HttpResponse<HttpHeaderResponse>} - The response from the server after a delete request.
     This is used to terminate the upload if asked.
     * @returns An observable of the header response of the server or EMPTY if the upload is finished or aborted.
     * @throws An `Observable<never>` type error with an error message if an error occurs.
     */
    private _performAbortion(
        response?: HttpResponse<HttpHeaderResponse>,
    ): Observable<HttpResponse<HttpHeaderResponse> | never> {
        if (this._toTerminate) {
            // Deletion should have happened
            if (response) {
                if (response.ok) {
                    return this._emitSuccess();
                }

                throw new TusError(
                    'An error happened while terminating or aborting the upload',
                );
            }

            return this._delete();
        }

        return this._emitSuccess();
    }

    /**
     * Read the offset from the response. Set the according property properly.
     * @method
     * @private
     * @param reponse {HttpResponse<HttpHeaderResponse>} - The response returned by the server containing the new offset.
     * @throws An `Observable<never>` type error with an error message if an error occurs.
     */
    private _readOffset(response: HttpResponse<HttpHeaderResponse>): void {
        if (!response.ok) {
            throw new TusError(
                'Unexpected response while performing the upload',
            );
        }

        const new_offset = +response.headers.get('Upload-Offset');

        if (typeof this._options.onProgress === 'function') {
            this._options.onProgress(new_offset, this._file.size);
        }

        if (Number.isNaN(new_offset)) {
            throw new TusError('Offset isNaN');
        }

        this._offset = new_offset;
    }

    /**
     * Add the checksum hash couple under the form `algo checksum` for use in the tus header "Upload-Checksum".
     * The use of this function is somehow dangerous, as the browser WebCrypto API has not reached a critical use mass yet.
     * @method
     * @private
     * @param blob {Blob} - The blob data on which to compute the checksum.
     * @returns {Promise<string>} The promise of the latter hash couple.
     */
    private async _checksumOfData(blob: Blob): Promise<string> {
        // NOTE(lmenou): arrayBuffer is untestable with `jest`
        const arrayBuffer = await blob.arrayBuffer();
        const hash = await crypto.subtle.digest('SHA-256', arrayBuffer);

        // NOTE(lmenou): ArrayBuffer does not offer method to manipulate the memory it contains.
        // It is necessary to create a view of it and manipulate the viewed data to get a string representation.
        // https://developer.mozilla.org/en-US/docs/Web/API/Web_Crypto_API/Non-cryptographic_uses_of_subtle_crypto#hashing_a_file
        const uint8ViewOfHash = new Uint8Array(hash);
        const stringifiedEncodedHash = Array.from(uint8ViewOfHash)
            .map((b) => b.toString(16).padStart(2, '0'))
            .join('');

        // NOTE(lmenou): btoa existence is previously checked before this function use
        return `sha256 ${btoa(stringifiedEncodedHash)}`;
    }

    /**
     * Check if the tus termination extension is supported by the server.
     * @method
     * @private
     * @param response {HttpResponse<HttpHeaderResponse>} - The response from the server after an options request.
     * @returns {boolean} `true` if the extension is supported, false otherwise.
     */
    private _isTermination(
        response: HttpResponse<HttpHeaderResponse>,
    ): boolean {
        const extensions = response.headers.get('Tus-Extension');
        const isTermination = extensions.includes('termination');
        return isTermination;
    }

    /**
     * Perform the deletion of the upload itself.
     * @method
     * @private
     * @returns An observable of the header response of the server.
     */
    private _delete(): Observable<HttpResponse<HttpHeaderResponse>> {
        const headers: HttpHeaders = new HttpHeaders({
            'Tus-Resumable': '1.0.0',
        });

        return this.httpClient.delete<HttpHeaderResponse>(this._url, {
            headers: headers,
            observe: 'response',
        });
    }

    /**
     * Finish the upload by emitting an `EMPTY` observable.
     * If `tus.options.onSuccess` is a function, the latter is called by this method in particular.
     * @method
     * @private
     * @returns {Observable<never>} the observable `EMPTY`.
     */
    private _emitSuccess(): Observable<never> {
        if (typeof this._options.onSuccess === 'function') {
            this._options.onSuccess(this._key);
        }

        return EMPTY;
    }

    /**
     * Finish the upload by emitting an `EMPTY` observable.
     * If `tus.options.onError` is a function, the latter is called by this method in particular.
     * @method
     * @private
     * @returns {Observable<never>} the observable `EMPTY`.
     */
    private _emitError(err: TusError | Error): Observable<never> {
        if (typeof this._options.onError === 'function') {
            this._options.onError(err);
        }
        console.error(err.message);
        return EMPTY;
    }
}
