/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, waitForAsync, ComponentFixture  } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { InstanceCardComponent } from './instance-card.component';
import { INSTANCE } from 'src/test-data';

describe('[Portal][Components] InstanceCardComponent', () => {
    let component: InstanceCardComponent;
    let fixture: ComponentFixture<InstanceCardComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [InstanceCardComponent]
        }).compileComponents();
        fixture = TestBed.createComponent(InstanceCardComponent);
        component = fixture.componentInstance;
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });

    it('should call the method #getLogoSrc properly', () => {
        component.instance = INSTANCE;
        component.apiUrl = 'coconut';
        const res = component.getLogoSrc();
        expect(res).toEqual('coconut/instance/my-instance/file-explorerlogo.png')
    })

    it('should call the method #isInstanceAccessible properly', () => {
        component.instance = INSTANCE;
        component.apiUrl = 'coconut';
        let res = component.isInstanceAccessible();
        expect(res).toEqual(true)

        const spyIsAdmin = jest.spyOn(component, 'isAdmin').mockReturnValue(false);
        component.authenticationEnabled = true;
        component.instance.public= false;
        component.isAuthenticated = true;
        component.adminRoles = ['me', 'divin'];
        component.userRoles = ['me', 'divin', 'hello'];
        res = component.isInstanceAccessible();
        expect(spyIsAdmin).toHaveBeenCalled();
        expect(res).toEqual(false);
    })

    it('should call the method #isAdmin properly', () => {
        component.adminRoles = ['me', 'divin'];
        component.userRoles = ['me', 'divin', 'hello'];
        const res = component.isAdmin();
        expect(res).toEqual(true);
    })
});
