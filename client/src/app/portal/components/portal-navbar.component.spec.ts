/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, waitForAsync, ComponentFixture  } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { PortalNavbarComponent } from './portal-navbar.component';

describe('[Portal][Components] PortalNavbarComponent', () => {
    let component: PortalNavbarComponent;
    let fixture: ComponentFixture<PortalNavbarComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [PortalNavbarComponent]
        }).compileComponents();
        fixture = TestBed.createComponent(PortalNavbarComponent);
        component = fixture.componentInstance;
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });
});
