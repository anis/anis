/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

import { Instance } from 'src/app/metamodel/models';
import { isAdmin } from 'src/app/shared/utils';

@Component({
    selector: 'app-instance-card',
    templateUrl: 'instance-card.component.html',
    styleUrls: ['instance-card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InstanceCardComponent {
    @Input() instance: Instance;
    @Input() authenticationEnabled: boolean;
    @Input() isAuthenticated: boolean;
    @Input() userRoles: string[];
    @Input() adminRoles: string[];
    @Input() apiUrl: string;

    /**
     * Returns the logo url.
     *
     * @return string
     */
    getLogoSrc(): string {
        return `${this.apiUrl}/instance/${this.instance.name}/file-explorer${this.instance.portal_logo}`;
    }

    /**
     * Returns true if user can enter in the instance
     *
     * @returns boolean
     */
    isInstanceAccessible(): boolean {
        let accessible = true;

        if (
            this.authenticationEnabled &&
            !this.instance.public &&
            !this.isAdmin()
        ) {
            accessible = false;
            if (this.isAuthenticated) {
                accessible =
                    this.instance.groups.filter((instanceGroup) =>
                        this.userRoles.includes(instanceGroup.role),
                    ).length > 0;
            }
        }

        return accessible;
    }

    /**
     * Returns true if user is admin
     *
     * @returns boolean
     */
    isAdmin(): boolean {
        return isAdmin(this.adminRoles, this.userRoles);
    }
}
