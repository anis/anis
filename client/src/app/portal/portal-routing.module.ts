/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PortalHomeComponent } from './containers/portal-home.component';

const routes: Routes = [
    { path: '', component: PortalHomeComponent, title: 'ANIS - Portal' }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PortalRoutingModule { }

export const routedComponents = [
    PortalHomeComponent
];
