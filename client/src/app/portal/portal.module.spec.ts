/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { PortalModule } from './portal.module';

describe('[Portal] PortalModule', () => {
    it('Test portal module', () => {
        expect(PortalModule.name).toEqual('PortalModule');
    });
});
