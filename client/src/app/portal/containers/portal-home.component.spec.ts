/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';
import { TestBed, waitForAsync, ComponentFixture  } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { provideMockStore, MockStore } from '@ngrx/store/testing';

import { PortalHomeComponent } from './portal-home.component';
import { AppConfigService } from 'src/app/app-config.service';
import { UserProfile } from 'src/app/user/store/models/user-profile.model';
import { Instance } from 'src/app/metamodel/models';
import * as authActions from 'src/app/user/store/actions/auth.actions';

describe('[Instance][Portal][Containers] PortalHomeComponent', () => {
    @Component({ selector: 'app-portal-navbar', template: '' })
    class PortalNavbarStubComponent {
        @Input() isAuthenticated: boolean;
        @Input() userProfile: UserProfile = null;
        @Input() userRoles: string[];
        @Input() authenticationEnabled: boolean;
        @Input() adminRoles: string[];
    }

    @Component({ selector: 'app-instance-card', template: '' })
    class InstanceCardStubComponent {
        @Input() instance: Instance;
        @Input() authenticationEnabled: boolean;
        @Input() isAuthenticated: boolean;
        @Input() userRoles: string[];
        @Input() adminRoles: string[];
        @Input() apiUrl: string;
    }

    @Component({ selector: 'app-footer', template: '' })
    class FooterStubComponent {
        @Input() instance: Instance;
        @Input() authenticationEnabled: boolean;
        @Input() isAuthenticated: boolean;
        @Input() userRoles: string[];
        @Input() adminRoles: string[];
        @Input() apiUrl: string;
    }

    let component: PortalHomeComponent;
    let fixture: ComponentFixture<PortalHomeComponent>;
    let store: MockStore;
    const appConfigServiceStub = new AppConfigService();

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                PortalHomeComponent,
                PortalNavbarStubComponent,
                InstanceCardStubComponent,
                FooterStubComponent
            ],
            providers: [
                provideMockStore({ }),
                { provide: AppConfigService, useValue: appConfigServiceStub }
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(PortalHomeComponent);
        component = fixture.componentInstance;
        store = TestBed.inject(MockStore);
        document.body.innerHTML = '<link id="favicon" href="">';
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });

    it('#authenticationEnabled() should return authentication enabled config key value', () => {
        appConfigServiceStub.authenticationEnabled = true;
        expect(component.getAuthenticationEnabled()).toBeTruthy();
    });

    it('#login() should dispatch login action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.login();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(authActions.login({ redirectUri: window.location.toString() }));
    });

    it('#logout() should dispatch logout action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.logout();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(authActions.logout());
    });

    it('#openEditProfile() should dispatch open edit profile action', () => {
        const spy = jest.spyOn(store, 'dispatch');
        component.openEditProfile();
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(authActions.openEditProfile());
    });
});
