/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { SharedModule } from './shared.module';

describe('[Shared] SharedModule', () => {
    it('Test shared module', () => {
        expect(SharedModule.name).toEqual('SharedModule');
    });
});
