/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function atLeastOneFilledValidator(controlNames: [string[], string[]]): ValidatorFn {
    return (group: AbstractControl): null | ValidationErrors => {
        const g0 = controlNames[0].filter(name => group.get(name)?.value);
        const g1 = controlNames[1].filter(name => group.get(name)?.value);

        return g0.length === controlNames[0].length || g1.length === controlNames[1].length 
            ? null : { atLeastOneFilled: true };
    };
}
