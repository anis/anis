/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { StyleService } from './style.service';
import { PrismService } from './prism.service';

export const sharedServices = [
    StyleService,
    PrismService
];
