/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttributeLabelComponent } from './attribute-label.component'

describe('[Shared][Components] AttributeLabelComponent', () => {
    let component: AttributeLabelComponent;
    let fixture: ComponentFixture<AttributeLabelComponent>;

    TestBed.configureTestingModule({
        declarations: [
            AttributeLabelComponent
        ]
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(AttributeLabelComponent);
        component = fixture.componentInstance;
    });

    it('should create component', () => {
        expect(component).toBeTruthy();
    });
});
