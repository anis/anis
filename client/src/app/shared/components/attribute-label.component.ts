/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-attribute-label',
    templateUrl: 'attribute-label.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AttributeLabelComponent {
    @Input() label: string;
    @Input() description: string;
}
