/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, ChangeDetectionStrategy } from '@angular/core';

/**
 * @class
 * @classdesc Spinner component.
 */
@Component({
    selector: 'app-spinner',
    templateUrl: 'spinner.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpinnerComponent { }
