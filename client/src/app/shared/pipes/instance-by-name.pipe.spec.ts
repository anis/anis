/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InstanceByNamePipe } from './instance-by-name.pipe';
import { INSTANCE_LIST } from 'src/test-data';

describe('[Shared][Pipes] InstanceByNamePipe', () => {
    const pipe = new InstanceByNamePipe();

    it('should return survey corresponding to the given name', () => {
        expect(pipe.transform(INSTANCE_LIST, 'my-other-instance')).toEqual(INSTANCE_LIST[0]);
    });
});
