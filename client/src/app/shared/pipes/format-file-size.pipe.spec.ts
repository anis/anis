/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { FormatFileSizePipe } from './format-file-size.pipe';

describe('FormatFileSizePipe', () => {
    const formatFileSizePipe = new FormatFileSizePipe();

    it('transforms "1000" to "1 kb"', () => {
        expect(formatFileSizePipe.transform(1500)).toEqual('1.5 KB');
    });

    it('transforms "1000000" to 1 Megabytes', () => {
        expect(formatFileSizePipe.transform(1024 * 1024)).toEqual('1.0 MB');
    });
});
