/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Pipe, PipeTransform } from '@angular/core';

import { Dataset } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Returns dataset corresponding to the given name.
 *
 * @example
 * // returns dataset object that matching with the 'myDataset' name among the dataset list
 * {{ datasetList | datasetByName:'myDataset' }}
 */
@Pipe({ name: 'datasetByName' })
export class DatasetByNamePipe implements PipeTransform {
    transform(datasetList: Dataset[], datasetName: string): Dataset {
        return datasetList.find(dataset => dataset.name === datasetName);
    }
}
