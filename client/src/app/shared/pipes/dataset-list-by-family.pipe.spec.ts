/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DatasetListByFamilyPipe } from './dataset-list-by-family.pipe';
import { DATASET_LIST } from 'src/test-data';

describe('[Shared][Pipes] DatasetListByFamilyPipe', () => {
    const pipe = new DatasetListByFamilyPipe();

    it('should return datasets corresponding to the given dataset family ID', () => {
        expect(pipe.transform(DATASET_LIST, 1)).toEqual(DATASET_LIST);
    });
});
