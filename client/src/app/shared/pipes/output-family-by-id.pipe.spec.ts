/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { OutputFamilyByIdPipe } from './output-family-by-id.pipe';
import { OUTPUT_FAMILY_LIST } from 'src/test-data';

describe('[Shared][Pipes] OutputFamilyByIdPipe', () => {
    const pipe = new OutputFamilyByIdPipe();

    it('should return output family corresponding to the given ID', () => {
        expect(pipe.transform(OUTPUT_FAMILY_LIST, 1)).toEqual(OUTPUT_FAMILY_LIST[0]);
    });
});
