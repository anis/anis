/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Pipe, PipeTransform } from '@angular/core';

import { Dataset } from 'src/app/metamodel/models';

/**
 * @class
 * @classdesc Returns datasets corresponding to the given dataset family ID.
 *
 * @example
 * // returns datasets that matching with the dataset family ID among the dataset list
 * {{ datasetList | datasetListByFamily:1 }}
 */
@Pipe({ name: 'datasetListByFamily' })
export class DatasetListByFamilyPipe implements PipeTransform {
    transform(datasetList: Dataset[], idDatasetFamily: number): Dataset[] {
        return datasetList.filter(dataset => dataset.id_dataset_family === idDatasetFamily);
    }
}
