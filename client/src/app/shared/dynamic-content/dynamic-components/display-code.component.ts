/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, AfterViewInit, Input } from '@angular/core';

import { PrismService } from 'src/app/shared/services/prism.service';

@Component({
    selector: 'app-display-code',
    templateUrl: 'display-code.component.html',
    styleUrls: ['display-code.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayCodeComponent implements AfterViewInit {
    @Input() codeType: string;

    constructor(private prismService: PrismService) { }

    ngAfterViewInit() {
        this.prismService.highlightAll();
    }
}