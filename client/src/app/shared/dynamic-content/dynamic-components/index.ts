/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { DynamicRouterLinkComponent } from './dynamic-router-link.component';
import { DynamicImageComponent } from './dynamic-image.component';
import { DynamicAccordionComponent } from './dynamic-accordion.component';
import { DisplayCodeComponent } from './display-code.component';
import { DisplayFileLinkPrivateDatasetComponent } from './display-file-link-private-dataset';

export const dynamicComponents = [
    DynamicRouterLinkComponent,
    DynamicImageComponent,
    DynamicAccordionComponent,
    DisplayCodeComponent,
    DisplayFileLinkPrivateDatasetComponent
];
