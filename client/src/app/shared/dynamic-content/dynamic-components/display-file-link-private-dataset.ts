/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { getHost } from 'src/app/shared/utils';
import { AppConfigService } from 'src/app/app-config.service';
import * as searchActions from 'src/app/instance/store/actions/search.actions';
import * as authSelector from 'src/app/user/store/selectors/auth.selector';
import * as datasetSelector from 'src/app/metamodel/selectors/dataset.selector';
import { Instance } from 'src/app/metamodel/models';
import { isDatasetAccessible } from 'src/app/shared/utils';

@Component({
    selector: 'app-display-file-link-private-dataset',
    templateUrl: 'display-file-link-private-dataset.html',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DisplayFileLinkPrivateDatasetComponent {
    @Input() instance: Instance;
    @Input() datasetName: string;
    @Input() path: string;
    @Input() class: string;

    constructor(private appConfig: AppConfigService, private store: Store<{}>) { }

    /**
     * Returns link href.
     *
     * @return string
     */
    getHref(): string {
        let path = this.path;
        if (path[0] !== '/') {
            path = '/' + path;
        }
        return `${getHost(this.appConfig.apiUrl)}/instance/${this.instance.name}/dataset/${this.datasetName}/file-explorer${path}`;
    }

    /**
     * Returns class + disabled if dataset is not accessible by the user
     * 
     * @returns Observable<string>
     */
    getClass(): Observable<string> {
        return combineLatest([
            this.store.select(authSelector.selectIsAuthenticated),
            this.store.select(authSelector.selectUserRoles),
            this.store.select(datasetSelector.selectAllDatasets)
        ]).pipe(
            map(([isAuthenticated, userRoles, datasetList]) => {
                if (!isDatasetAccessible(
                    datasetList.find(dataset => dataset.name === this.datasetName),
                    this.appConfig.authenticationEnabled,
                    isAuthenticated,
                    this.appConfig.adminRoles,
                    userRoles
                )) {
                    return `${this.class} disabled`;
                } else {
                    return this.class;
                }
            }
        ));
    }

    /**
     * Fires download file action
     */
    downloadFile(event): void {
        event.preventDefault();

        const url = this.getHref();
        const filename = url.substring(url.lastIndexOf('/') + 1);

        this.store.dispatch(searchActions.downloadFile({ url, filename }));
    }
}