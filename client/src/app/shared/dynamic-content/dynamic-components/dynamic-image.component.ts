/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, Input } from '@angular/core';

import { Instance } from 'src/app/metamodel/models';

@Component({
    selector: 'app-dynamic-image',
    templateUrl: 'dynamic-image.component.html'
})
export class DynamicImageComponent {
    @Input() src: string;
    @Input() css: string;
    @Input() alt: string;
    @Input() instance: Instance;

    loading = true;
    error = false;

    onLoad() {
        this.loading = false;
    }
    
    onError(event) {
        if (this.instance.public || (!this.instance.public && event.target.attributes['src'].value === 'not_found')) {
            this.loading = false;
            this.error = true;
        }
    }
}
