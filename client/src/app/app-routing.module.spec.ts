/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


import { AppRoutingModule } from './app-routing.module';

describe('AppRoutingModule', () => {
    it('Test app-routing module', () => {
        expect(AppRoutingModule.name).toEqual('AppRoutingModule');
    });
});
