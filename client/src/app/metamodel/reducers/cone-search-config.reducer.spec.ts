/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromConeSearchConfig from './cone-search-config.reducer';
import * as coneSearchConfigActions from '../actions/cone-search-config.actions';
import { CONE_SEARCH_CONFIG } from 'src/test-data';

describe('[Metamodel][Reducers] ConeSearch config reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromConeSearchConfig.coneSearchConfigReducer(fromConeSearchConfig.initialState, action);

        expect(state).toBe(fromConeSearchConfig.initialState);
    });

    it('loadConeSearchConfig action should set coneSearchConfigIsLoading to true', () => {
        const action = coneSearchConfigActions.loadConeSearchConfig();
        const state = fromConeSearchConfig.coneSearchConfigReducer(fromConeSearchConfig.initialState, action);

        expect(state.coneSearchConfig).toBeNull();
        expect(state.coneSearchConfigIsLoading).toEqual(true);
        expect(state.coneSearchConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(fromConeSearchConfig.initialState);
    });

    it('loadConeSearchConfigSuccess action should add coneSearch config, set coneSearchConfigIsLoading to false and set coneSearchConfigIsLoaded to true', () => {
        const action = coneSearchConfigActions.loadConeSearchConfigSuccess({ coneSearchConfig: CONE_SEARCH_CONFIG });
        const state = fromConeSearchConfig.coneSearchConfigReducer(fromConeSearchConfig.initialState, action);

        expect(state.coneSearchConfig).toEqual(CONE_SEARCH_CONFIG);
        expect(state.coneSearchConfigIsLoading).toEqual(false);
        expect(state.coneSearchConfigIsLoaded).toEqual(true);
        expect(state).not.toBe(fromConeSearchConfig.initialState);
    });

    it('loadConeSearchConfigFail action should set coneSearchConfigIsLoading to false', () => {
        const action = coneSearchConfigActions.loadConeSearchConfigFail();
        const state = fromConeSearchConfig.coneSearchConfigReducer(fromConeSearchConfig.initialState, action);

        expect(state.coneSearchConfig).toBeNull();
        expect(state.coneSearchConfigIsLoading).toEqual(false);
        expect(state.coneSearchConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(fromConeSearchConfig.initialState);
    });

    it('addConeSearchConfigSuccess action should add coneSearch config', () => {
        const action = coneSearchConfigActions.addConeSearchConfigSuccess({ coneSearchConfig: CONE_SEARCH_CONFIG });
        const state = fromConeSearchConfig.coneSearchConfigReducer(fromConeSearchConfig.initialState, action);

        expect(state.coneSearchConfig).toEqual(CONE_SEARCH_CONFIG);
        expect(state.coneSearchConfigIsLoading).toEqual(false);
        expect(state.coneSearchConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(fromConeSearchConfig.initialState);
    });

    it('editConeSearchConfigSuccess action should edit coneSearch config', () => {
        const initialState = {
            ...fromConeSearchConfig.initialState,
            coneSearchConfig: { ...CONE_SEARCH_CONFIG, coneSearch_background_color: 'blue' }
        };
        const action = coneSearchConfigActions.editConeSearchConfigSuccess({ coneSearchConfig: CONE_SEARCH_CONFIG });
        const state = fromConeSearchConfig.coneSearchConfigReducer(initialState, action);

        expect(state.coneSearchConfig).toEqual(CONE_SEARCH_CONFIG);
        expect(state.coneSearchConfigIsLoading).toEqual(false);
        expect(state.coneSearchConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get coneSearchConfig', () => {
        const action = coneSearchConfigActions.loadConeSearchConfigSuccess({ coneSearchConfig: CONE_SEARCH_CONFIG });
        const state = fromConeSearchConfig.coneSearchConfigReducer(fromConeSearchConfig.initialState, action);

        expect(fromConeSearchConfig.selectConeSearchConfig(state)).toEqual(CONE_SEARCH_CONFIG);
    });

    it('should get coneSearchConfigIsLoading', () => {
        const action = {} as Action;
        const state =  fromConeSearchConfig.coneSearchConfigReducer(undefined, action);

        expect(fromConeSearchConfig.selectConeSearchConfigIsLoading(state)).toEqual(false);
    });

    it('should get instanceListIsLoaded', () => {
        const action = {} as Action;
        const state =  fromConeSearchConfig.coneSearchConfigReducer(undefined, action);

        expect(fromConeSearchConfig.selectConeSearchConfigIsLoaded(state)).toEqual(false);
    });
});
