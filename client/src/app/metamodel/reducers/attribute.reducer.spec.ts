/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromAttribute from './attribute.reducer';
import * as attributeActions from '../actions/attribute.actions';
import { ATTRIBUTE, ATTRIBUTE_LIST } from 'src/test-data';

describe('[Metamodel][Reducers] Attribute reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromAttribute.attributeReducer(fromAttribute.initialState, action);

        expect(state).toBe(fromAttribute.initialState);
    });

    it('loadAttributeList action should set attributeListIsLoading to true', () => {
        const action = attributeActions.loadAttributeList();
        const state = fromAttribute.attributeReducer(fromAttribute.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.attributeListIsLoading).toEqual(true);
        expect(state.attributeListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromAttribute.initialState);
    });

    it('loadAttributeListSuccess action should add attribute list, set attributeListIsLoading to false and set attributeListIsLoaded to true', () => {
        const action = attributeActions.loadAttributeListSuccess({ attributes: ATTRIBUTE_LIST });
        const state = fromAttribute.attributeReducer(fromAttribute.initialState, action);

        expect(state.ids.length).toEqual(4);
        expect(state.ids).toContain(ATTRIBUTE_LIST[0].id);
        expect(state.ids).toContain(ATTRIBUTE_LIST[1].id);
        expect(Object.keys(state.entities).length).toEqual(4);
        expect(state.attributeListIsLoading).toEqual(false);
        expect(state.attributeListIsLoaded).toEqual(true);
        expect(state).not.toBe(fromAttribute.initialState);
    });

    it('loadAttributeListFail action should set attributeListIsLoading to false', () => {
        const action = attributeActions.loadAttributeListFail();
        const state = fromAttribute.attributeReducer(fromAttribute.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.attributeListIsLoading).toEqual(false);
        expect(state.attributeListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromAttribute.initialState);
    });

    it('addAttributeSuccess action should add a attribute', () => {
        const action = attributeActions.addAttributeSuccess({ attribute: ATTRIBUTE });
        const state = fromAttribute.attributeReducer(fromAttribute.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(ATTRIBUTE.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.attributeListIsLoading).toEqual(false);
        expect(state.attributeListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromAttribute.initialState);
    });

    it('editAttributeSuccess action should modify a attribute', () => {
        const initialState = {
            ...fromAttribute.initialState,
            ids: [ATTRIBUTE.id],
            entities: { [ATTRIBUTE.id]: { ...ATTRIBUTE, label: 'New label' }}
        };
        const action = attributeActions.editAttributeSuccess({ attribute: ATTRIBUTE });
        const state = fromAttribute.attributeReducer(initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(ATTRIBUTE.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[ATTRIBUTE.id]).toEqual(ATTRIBUTE);
        expect(state.attributeListIsLoading).toEqual(false);
        expect(state.attributeListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteAttributeSuccess action should delete a attribute', () => {
        const initialState = {
            ...fromAttribute.initialState,
            ids: [ATTRIBUTE.id],
            entities: { [ATTRIBUTE.id]: ATTRIBUTE }
        };
        const action = attributeActions.deleteAttributeSuccess({ attribute: ATTRIBUTE });
        const state = fromAttribute.attributeReducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.attributeListIsLoading).toEqual(false);
        expect(state.attributeListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get attributeListIsLoading', () => {
        const action = {} as Action;
        const state =  fromAttribute.attributeReducer(undefined, action);

        expect(fromAttribute.selectAttributeListIsLoading(state)).toEqual(false);
    });

    it('should get attributeListIsLoaded', () => {
        const action = {} as Action;
        const state = fromAttribute.attributeReducer(undefined, action);

        expect(fromAttribute.selectAttributeListIsLoaded(state)).toEqual(false);
    });
});
