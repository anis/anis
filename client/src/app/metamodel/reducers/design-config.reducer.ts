/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';

import { DesignConfig } from '../models';
import * as designConfigActions from '../actions/design-config.actions';

export interface State {
    designConfig: DesignConfig;
    designConfigIsLoading: boolean;
    designConfigIsLoaded: boolean;
}

export const initialState: State = {
    designConfig: null,
    designConfigIsLoading: false,
    designConfigIsLoaded: false
};

export const designConfigReducer = createReducer(
    initialState,
    on(designConfigActions.loadDesignConfig, (state) => {
        return {
            ...state,
            designConfig: null,
            designConfigIsLoading: true,
            designConfigIsLoaded: false
        }
    }),
    on(designConfigActions.loadDesignConfigSuccess, (state, { designConfig }) => {
        return {
            ...state,
            designConfig,
            designConfigIsLoading: false,
            designConfigIsLoaded: true
        }
    }),
    on(designConfigActions.loadDesignConfigFail, (state) => {
        return {
            ...state,
            designConfigIsLoading: false
        }
    }),
    on(designConfigActions.addDesignConfigSuccess, (state, { designConfig }) => {
        return {
            ...state,
            designConfig
        }
    }),
    on(designConfigActions.editDesignConfigSuccess, (state, { designConfig }) => {
        return {
            ...state,
            designConfig
        }
    })
);

export const selectDesignConfig = (state: State) => state.designConfig;
export const selectDesignConfigIsLoading = (state: State) => state.designConfigIsLoading;
export const selectDesignConfigIsLoaded = (state: State) => state.designConfigIsLoaded;
