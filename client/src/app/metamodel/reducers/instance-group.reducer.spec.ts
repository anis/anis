/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromInstanceGroup from './instance-group.reducer';
import * as instanceGroupActions from '../actions/instance-group.actions';
import { INSTANCE } from 'src/test-data';

describe('[Metamodel][Reducers] InstanceGroup reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromInstanceGroup.instanceGroupReducer(fromInstanceGroup.initialState, action);

        expect(state).toBe(fromInstanceGroup.initialState);
    });

    it('loadInstanceGroupList action should set instanceGroupListIsLoading to true', () => {
        const action = instanceGroupActions.loadInstanceGroupList();
        const state = fromInstanceGroup.instanceGroupReducer(fromInstanceGroup.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.instanceGroupListIsLoading).toEqual(true);
        expect(state.instanceGroupListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromInstanceGroup.initialState);
    });

    it('loadInstanceGroupListSuccess action should add instance group list, set instanceGroupListIsLoading to false and set instanceGroupListIsLoaded to true', () => {
        const action = instanceGroupActions.loadInstanceGroupListSuccess({ instanceGroups: INSTANCE.groups });
        const state = fromInstanceGroup.instanceGroupReducer(fromInstanceGroup.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(INSTANCE.groups[0].role);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.instanceGroupListIsLoading).toEqual(false);
        expect(state.instanceGroupListIsLoaded).toEqual(true);
        expect(state).not.toBe(fromInstanceGroup.initialState);
    });

    it('loadInstanceGroupListFail action should set instanceGroupListIsLoading to false', () => {
        const action = instanceGroupActions.loadInstanceGroupListFail();
        const state = fromInstanceGroup.instanceGroupReducer(fromInstanceGroup.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.instanceGroupListIsLoading).toEqual(false);
        expect(state.instanceGroupListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromInstanceGroup.initialState);
    });

    it('addInstanceGroupSuccess action should add a instance group', () => {
        const action = instanceGroupActions.addInstanceGroupSuccess({ instanceGroup: INSTANCE.groups[0] });
        const state = fromInstanceGroup.instanceGroupReducer(fromInstanceGroup.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(INSTANCE.groups[0].role);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.instanceGroupListIsLoading).toEqual(false);
        expect(state.instanceGroupListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromInstanceGroup.initialState);
    });

    it('deleteInstanceGroupSuccess action should delete an instance group', () => {
        const initialState = {
            ...fromInstanceGroup.initialState,
            ids: [INSTANCE.groups[0].role],
            entities: { [INSTANCE.groups[0].role]: INSTANCE.groups[0] }
        };
        const action = instanceGroupActions.deleteInstanceGroupSuccess({ instanceGroup: INSTANCE.groups[0] });
        const state = fromInstanceGroup.instanceGroupReducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.instanceGroupListIsLoading).toEqual(false);
        expect(state.instanceGroupListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get instanceGroupListIsLoading', () => {
        const action = {} as Action;
        const state =  fromInstanceGroup.instanceGroupReducer(undefined, action);

        expect(fromInstanceGroup.selectInstanceGroupListIsLoading(state)).toEqual(false);
    });

    it('should get instanceGroupListIsLoaded', () => {
        const action = {} as Action;
        const state = fromInstanceGroup.instanceGroupReducer(undefined, action);

        expect(fromInstanceGroup.selectInstanceGroupListIsLoaded(state)).toEqual(false);
    });
});
