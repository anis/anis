/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromAliasConfig from './alias-config.reducer';
import * as aliasConfigActions from '../actions/alias-config.actions';
import { ALIAS_CONFIG } from 'src/test-data';

describe('[Metamodel][Reducers] Alias config reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromAliasConfig.aliasConfigReducer(fromAliasConfig.initialState, action);

        expect(state).toBe(fromAliasConfig.initialState);
    });

    it('loadAliasConfig action should set aliasConfigIsLoading to true', () => {
        const action = aliasConfigActions.loadAliasConfig();
        const state = fromAliasConfig.aliasConfigReducer(fromAliasConfig.initialState, action);

        expect(state.aliasConfig).toBeNull();
        expect(state.aliasConfigIsLoading).toEqual(true);
        expect(state.aliasConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(fromAliasConfig.initialState);
    });

    it('loadAliasConfigSuccess action should add alias config, set aliasConfigIsLoading to false and set aliasConfigIsLoaded to true', () => {
        const action = aliasConfigActions.loadAliasConfigSuccess({ aliasConfig: ALIAS_CONFIG });
        const state = fromAliasConfig.aliasConfigReducer(fromAliasConfig.initialState, action);

        expect(state.aliasConfig).toEqual(ALIAS_CONFIG);
        expect(state.aliasConfigIsLoading).toEqual(false);
        expect(state.aliasConfigIsLoaded).toEqual(true);
        expect(state).not.toBe(fromAliasConfig.initialState);
    });

    it('loadAliasConfigFail action should set aliasConfigIsLoading to false', () => {
        const action = aliasConfigActions.loadAliasConfigFail();
        const state = fromAliasConfig.aliasConfigReducer(fromAliasConfig.initialState, action);

        expect(state.aliasConfig).toBeNull();
        expect(state.aliasConfigIsLoading).toEqual(false);
        expect(state.aliasConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(fromAliasConfig.initialState);
    });

    it('addAliasConfigSuccess action should add alias config', () => {
        const action = aliasConfigActions.addAliasConfigSuccess({ aliasConfig: ALIAS_CONFIG });
        const state = fromAliasConfig.aliasConfigReducer(fromAliasConfig.initialState, action);

        expect(state.aliasConfig).toEqual(ALIAS_CONFIG);
        expect(state.aliasConfigIsLoading).toEqual(false);
        expect(state.aliasConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(fromAliasConfig.initialState);
    });

    it('editAliasConfigSuccess action should edit alias config', () => {
        const initialState = {
            ...fromAliasConfig.initialState,
            aliasConfig: { ...ALIAS_CONFIG, alias_background_color: 'blue' }
        };
        const action = aliasConfigActions.editAliasConfigSuccess({ aliasConfig: ALIAS_CONFIG });
        const state = fromAliasConfig.aliasConfigReducer(initialState, action);

        expect(state.aliasConfig).toEqual(ALIAS_CONFIG);
        expect(state.aliasConfigIsLoading).toEqual(false);
        expect(state.aliasConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get aliasConfig', () => {
        const action = aliasConfigActions.loadAliasConfigSuccess({ aliasConfig: ALIAS_CONFIG });
        const state = fromAliasConfig.aliasConfigReducer(fromAliasConfig.initialState, action);

        expect(fromAliasConfig.selectAliasConfig(state)).toEqual(ALIAS_CONFIG);
    });

    it('should get aliasConfigIsLoading', () => {
        const action = {} as Action;
        const state =  fromAliasConfig.aliasConfigReducer(undefined, action);

        expect(fromAliasConfig.selectAliasConfigIsLoading(state)).toEqual(false);
    });

    it('should get instanceListIsLoaded', () => {
        const action = {} as Action;
        const state =  fromAliasConfig.aliasConfigReducer(undefined, action);

        expect(fromAliasConfig.selectAliasConfigIsLoaded(state)).toEqual(false);
    });
});
