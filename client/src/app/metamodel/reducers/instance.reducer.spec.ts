/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromInstance from './instance.reducer';
import * as instanceActions from '../actions/instance.actions';
import { INSTANCE, INSTANCE_LIST } from 'src/test-data';

describe('[Metamodel][Reducers] Instance reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromInstance.instanceReducer(fromInstance.initialState, action);

        expect(state).toBe(fromInstance.initialState);
    });

    it('loadInstanceList action should set instanceListIsLoading to true', () => {
        const action = instanceActions.loadInstanceList();
        const state = fromInstance.instanceReducer(fromInstance.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.instanceListIsLoading).toEqual(true);
        expect(state.instanceListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromInstance.initialState);
    });

    it('loadInstanceListSuccess action should add instance list, set instanceListIsLoading to false and set instanceListIsLoaded to true', () => {
        const action = instanceActions.loadInstanceListSuccess({ instances: INSTANCE_LIST });
        const state = fromInstance.instanceReducer(fromInstance.initialState, action);

        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain(INSTANCE_LIST[0].name);
        expect(state.ids).toContain(INSTANCE_LIST[1].name);
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.instanceListIsLoading).toEqual(false);
        expect(state.instanceListIsLoaded).toEqual(true);
        expect(state).not.toBe(fromInstance.initialState);
    });

    it('loadInstanceListFail action should set instanceListIsLoading to false', () => {
        const action = instanceActions.loadInstanceListFail();
        const state = fromInstance.instanceReducer(fromInstance.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.instanceListIsLoading).toEqual(false);
        expect(state.instanceListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromInstance.initialState);
    });

    it('importInstanceSuccess action should add a instance', () => {
        const action = instanceActions.importInstanceSuccess({ instance: INSTANCE });
        const state = fromInstance.instanceReducer(fromInstance.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(INSTANCE.name);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.instanceListIsLoading).toEqual(false);
        expect(state.instanceListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromInstance.initialState);
    });

    it('addInstanceSuccess action should add a instance', () => {
        const action = instanceActions.addInstanceSuccess({ instance: INSTANCE });
        const state = fromInstance.instanceReducer(fromInstance.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(INSTANCE.name);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.instanceListIsLoading).toEqual(false);
        expect(state.instanceListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromInstance.initialState);
    });

    it('editInstanceSuccess action should modify a instance', () => {
        const initialState = {
            ...fromInstance.initialState,
            ids: [INSTANCE.name],
            entities: { [INSTANCE.name]: { ...INSTANCE, label: 'New label' }}
        };
        const action = instanceActions.editInstanceSuccess({ instance: INSTANCE });
        const state = fromInstance.instanceReducer(initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(INSTANCE.name);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[INSTANCE.name]).toEqual(INSTANCE);
        expect(state.instanceListIsLoading).toEqual(false);
        expect(state.instanceListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteInstanceSuccess action should delete an instance', () => {
        const initialState = {
            ...fromInstance.initialState,
            ids: [INSTANCE.name],
            entities: { [INSTANCE.name]: INSTANCE }
        };
        const action = instanceActions.deleteInstanceSuccess({ instance: INSTANCE });
        const state = fromInstance.instanceReducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.instanceListIsLoading).toEqual(false);
        expect(state.instanceListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get instanceListIsLoading', () => {
        const action = {} as Action;
        const state =  fromInstance.instanceReducer(undefined, action);

        expect(fromInstance.selectInstanceListIsLoading(state)).toEqual(false);
    });

    it('should get instanceListIsLoaded', () => {
        const action = {} as Action;
        const state = fromInstance.instanceReducer(undefined, action);

        expect(fromInstance.selectInstanceListIsLoaded(state)).toEqual(false);
    });
});
