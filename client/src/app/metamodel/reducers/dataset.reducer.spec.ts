/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromDataset from './dataset.reducer';
import * as datasetActions from '../actions/dataset.actions';
import { DATASET, DATASET_LIST } from 'src/test-data';

describe('[Metamodel][Reducers] Dataset reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromDataset.datasetReducer(fromDataset.initialState, action);

        expect(state).toBe(fromDataset.initialState);
    });

    it('loadDatasetList action should set datasetListIsLoading to true', () => {
        const action = datasetActions.loadDatasetList();
        const state = fromDataset.datasetReducer(fromDataset.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.datasetListIsLoading).toEqual(true);
        expect(state.datasetListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDataset.initialState);
    });

    it('loadDatasetListSuccess action should add dataset list, set datasetListIsLoading to false and set datasetListIsLoaded to true', () => {
        const action = datasetActions.loadDatasetListSuccess({ datasets: DATASET_LIST });
        const state = fromDataset.datasetReducer(fromDataset.initialState, action);

        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain(DATASET_LIST[0].name);
        expect(state.ids).toContain(DATASET_LIST[1].name);
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.datasetListIsLoading).toEqual(false);
        expect(state.datasetListIsLoaded).toEqual(true);
        expect(state).not.toBe(fromDataset.initialState);
    });

    it('loadDatasetListFail action should set datasetListIsLoading to false', () => {
        const action = datasetActions.loadDatasetListFail();
        const state = fromDataset.datasetReducer(fromDataset.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.datasetListIsLoading).toEqual(false);
        expect(state.datasetListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDataset.initialState);
    });

    it('importDatasetSuccess action should add a dataset', () => {
        const action = datasetActions.importDatasetSuccess({ dataset: DATASET });
        const state = fromDataset.datasetReducer(fromDataset.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(DATASET.name);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.datasetListIsLoading).toEqual(false);
        expect(state.datasetListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDataset.initialState);
    });

    it('addDatasetSuccess action should add a dataset', () => {
        const action = datasetActions.addDatasetSuccess({ dataset: DATASET });
        const state = fromDataset.datasetReducer(fromDataset.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(DATASET.name);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.datasetListIsLoading).toEqual(false);
        expect(state.datasetListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDataset.initialState);
    });

    it('editDatasetSuccess action should modify a dataset', () => {
        const initialState = {
            ...fromDataset.initialState,
            ids: [DATASET.name],
            entities: { [DATASET.name]: { ...DATASET, label: 'New label' }}
        };
        const action = datasetActions.editDatasetSuccess({ dataset: DATASET });
        const state = fromDataset.datasetReducer(initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(DATASET.name);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[DATASET.name]).toEqual(DATASET);
        expect(state.datasetListIsLoading).toEqual(false);
        expect(state.datasetListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteDatasetSuccess action should delete an dataset', () => {
        const initialState = {
            ...fromDataset.initialState,
            ids: [DATASET.name],
            entities: { [DATASET.name]: DATASET }
        };
        const action = datasetActions.deleteDatasetSuccess({ dataset: DATASET });
        const state = fromDataset.datasetReducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.datasetListIsLoading).toEqual(false);
        expect(state.datasetListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get datasetListIsLoading', () => {
        const action = {} as Action;
        const state =  fromDataset.datasetReducer(undefined, action);

        expect(fromDataset.selectDatasetListIsLoading(state)).toEqual(false);
    });

    it('should get datasetListIsLoaded', () => {
        const action = {} as Action;
        const state = fromDataset.datasetReducer(undefined, action);

        expect(fromDataset.selectDatasetListIsLoaded(state)).toEqual(false);
    });
});
