/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromMenuFamilyItem from './menu-family-item.reducer';
import * as menuFamilyItemActions from '../actions/menu-family-item.actions';
import { MENU_ITEM_LIST } from 'src/test-data';

describe('[Metamodel][Reducers] MenuFamilyItem reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromMenuFamilyItem.menuFamilyItemReducer(fromMenuFamilyItem.initialState, action);

        expect(state).toBe(fromMenuFamilyItem.initialState);
    });

    it('loadMenuFamilyItemList action should set menuFamilyItemListIsLoading to true', () => {
        const action = menuFamilyItemActions.loadMenuFamilyItemList();
        const state = fromMenuFamilyItem.menuFamilyItemReducer(fromMenuFamilyItem.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.menuFamilyItemListIsLoading).toEqual(true);
        expect(state.menuFamilyItemListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromMenuFamilyItem.initialState);
    });

    it('loadMenuFamilyItemListSuccess action should add menu item list, set menuFamilyItemListIsLoading to false and set menuFamilyItemListIsLoaded to true', () => {
        const action = menuFamilyItemActions.loadMenuFamilyItemListSuccess({ menuFamilyItemList: MENU_ITEM_LIST });
        const state = fromMenuFamilyItem.menuFamilyItemReducer(fromMenuFamilyItem.initialState, action);

        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain(MENU_ITEM_LIST[0].id);
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.menuFamilyItemListIsLoading).toEqual(false);
        expect(state.menuFamilyItemListIsLoaded).toEqual(true);
        expect(state).not.toBe(fromMenuFamilyItem.initialState);
    });

    it('loadMenuFamilyItemListFail action should set menuFamilyItemListIsLoading to false', () => {
        const action = menuFamilyItemActions.loadMenuFamilyItemListFail();
        const state = fromMenuFamilyItem.menuFamilyItemReducer(fromMenuFamilyItem.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.menuFamilyItemListIsLoading).toEqual(false);
        expect(state.menuFamilyItemListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromMenuFamilyItem.initialState);
    });

    it('addMenuFamilyItemSuccess action should add a menu item', () => {
        const action = menuFamilyItemActions.addMenuFamilyItemSuccess({ menuFamilyItem: MENU_ITEM_LIST[0] });
        const state = fromMenuFamilyItem.menuFamilyItemReducer(fromMenuFamilyItem.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(MENU_ITEM_LIST[0].id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.menuFamilyItemListIsLoading).toEqual(false);
        expect(state.menuFamilyItemListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromMenuFamilyItem.initialState);
    });

    it('editMenuFamilyItemSuccess action should modify a menu item', () => {
        const initialState = {
            ...fromMenuFamilyItem.initialState,
            ids: [MENU_ITEM_LIST[0].id],
            entities: { [MENU_ITEM_LIST[0].id]: { ...MENU_ITEM_LIST[0], label: 'New label' }}
        };
        const action = menuFamilyItemActions.editMenuFamilyItemSuccess({ menuFamilyItem: MENU_ITEM_LIST[0] });
        const state = fromMenuFamilyItem.menuFamilyItemReducer(initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(MENU_ITEM_LIST[0].id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[MENU_ITEM_LIST[0].id]).toEqual(MENU_ITEM_LIST[0]);
        expect(state.menuFamilyItemListIsLoading).toEqual(false);
        expect(state.menuFamilyItemListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteMenuFamilyItemSuccess action should delete a menu item', () => {
        const initialState = {
            ...fromMenuFamilyItem.initialState,
            ids: [MENU_ITEM_LIST[0].id],
            entities: { [MENU_ITEM_LIST[0].id]: MENU_ITEM_LIST[0] }
        };
        const action = menuFamilyItemActions.deleteMenuFamilyItemSuccess({ menuFamilyItem: MENU_ITEM_LIST[0] });
        const state = fromMenuFamilyItem.menuFamilyItemReducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.menuFamilyItemListIsLoading).toEqual(false);
        expect(state.menuFamilyItemListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get menuFamilyItemListIsLoading', () => {
        const action = {} as Action;
        const state =  fromMenuFamilyItem.menuFamilyItemReducer(undefined, action);

        expect(fromMenuFamilyItem.selectMenuFamilyItemListIsLoading(state)).toEqual(false);
    });

    it('should get menuFamilyItemListIsLoaded', () => {
        const action = {} as Action;
        const state = fromMenuFamilyItem.menuFamilyItemReducer(undefined, action);

        expect(fromMenuFamilyItem.selectMenuFamilyItemListIsLoaded(state)).toEqual(false);
    });
});
