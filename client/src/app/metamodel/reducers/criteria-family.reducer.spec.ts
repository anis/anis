/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromCriteriaFamily from './criteria-family.reducer';
import * as criteriaFamilyActions from '../actions/criteria-family.actions';
import { CRITERIA_FAMILY, CRITERIA_FAMILY_LIST } from 'src/test-data';

describe('[Metamodel][Reducers] CriteriaFamily reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromCriteriaFamily.criteriaFamilyReducer(fromCriteriaFamily.initialState, action);

        expect(state).toBe(fromCriteriaFamily.initialState);
    });

    it('loadCriteriaFamilyList action should set criteriaFamilyListIsLoading to true', () => {
        const action = criteriaFamilyActions.loadCriteriaFamilyList();
        const state = fromCriteriaFamily.criteriaFamilyReducer(fromCriteriaFamily.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.criteriaFamilyListIsLoading).toEqual(true);
        expect(state.criteriaFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromCriteriaFamily.initialState);
    });

    it('loadCriteriaFamilyListSuccess action should add criteria family list, set criteriaFamilyListIsLoading to false and set criteriaFamilyListIsLoaded to true', () => {
        const action = criteriaFamilyActions.loadCriteriaFamilyListSuccess({ criteriaFamilies: CRITERIA_FAMILY_LIST });
        const state = fromCriteriaFamily.criteriaFamilyReducer(fromCriteriaFamily.initialState, action);

        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain(CRITERIA_FAMILY_LIST[0].id);
        expect(state.ids).toContain(CRITERIA_FAMILY_LIST[1].id);
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.criteriaFamilyListIsLoading).toEqual(false);
        expect(state.criteriaFamilyListIsLoaded).toEqual(true);
        expect(state).not.toBe(fromCriteriaFamily.initialState);
    });

    it('loadCriteriaFamilyListFail action should set criteriaFamilyListIsLoading to false', () => {
        const action = criteriaFamilyActions.loadCriteriaFamilyListFail();
        const state = fromCriteriaFamily.criteriaFamilyReducer(fromCriteriaFamily.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.criteriaFamilyListIsLoading).toEqual(false);
        expect(state.criteriaFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromCriteriaFamily.initialState);
    });

    it('addCriteriaFamilySuccess action should add a criteria family', () => {
        const action = criteriaFamilyActions.addCriteriaFamilySuccess({ criteriaFamily: CRITERIA_FAMILY });
        const state = fromCriteriaFamily.criteriaFamilyReducer(fromCriteriaFamily.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(CRITERIA_FAMILY.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.criteriaFamilyListIsLoading).toEqual(false);
        expect(state.criteriaFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromCriteriaFamily.initialState);
    });

    it('editCriteriaFamilySuccess action should modify a criteria family', () => {
        const initialState = {
            ...fromCriteriaFamily.initialState,
            ids: [CRITERIA_FAMILY.id],
            entities: { [CRITERIA_FAMILY.id]: { ...CRITERIA_FAMILY, label: 'New label' }}
        };
        const action = criteriaFamilyActions.editCriteriaFamilySuccess({ criteriaFamily: CRITERIA_FAMILY });
        const state = fromCriteriaFamily.criteriaFamilyReducer(initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(CRITERIA_FAMILY.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[CRITERIA_FAMILY.id]).toEqual(CRITERIA_FAMILY);
        expect(state.criteriaFamilyListIsLoading).toEqual(false);
        expect(state.criteriaFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteCriteriaFamilySuccess action should delete an criteria family', () => {
        const initialState = {
            ...fromCriteriaFamily.initialState,
            ids: [CRITERIA_FAMILY.id],
            entities: { [CRITERIA_FAMILY.id]: CRITERIA_FAMILY }
        };
        const action = criteriaFamilyActions.deleteCriteriaFamilySuccess({ criteriaFamily: CRITERIA_FAMILY });
        const state = fromCriteriaFamily.criteriaFamilyReducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.criteriaFamilyListIsLoading).toEqual(false);
        expect(state.criteriaFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get criteriaFamilyListIsLoading', () => {
        const action = {} as Action;
        const state =  fromCriteriaFamily.criteriaFamilyReducer(undefined, action);

        expect(fromCriteriaFamily.selectCriteriaFamilyListIsLoading(state)).toEqual(false);
    });

    it('should get criteriaFamilyListIsLoaded', () => {
        const action = {} as Action;
        const state = fromCriteriaFamily.criteriaFamilyReducer(undefined, action);

        expect(fromCriteriaFamily.selectCriteriaFamilyListIsLoaded(state)).toEqual(false);
    });
});
