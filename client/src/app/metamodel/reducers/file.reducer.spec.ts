/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromFile from './file.reducer';
import * as fileActions from '../actions/file.actions';
import { FILE } from 'src/test-data';

describe('[Metamodel][Reducers] File reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromFile.fileReducer(fromFile.initialState, action);

        expect(state).toBe(fromFile.initialState);
    });

    it('loadFileList action should set fileListIsLoading to true', () => {
        const action = fileActions.loadFileList();
        const state = fromFile.fileReducer(fromFile.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.fileListIsLoading).toEqual(true);
        expect(state.fileListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromFile.initialState);
    });

    it('loadFileListSuccess action should add file list, set fileListIsLoading to false and set fileListIsLoaded to true', () => {
        const action = fileActions.loadFileListSuccess({ files: [FILE] });
        const state = fromFile.fileReducer(fromFile.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(FILE.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.fileListIsLoading).toEqual(false);
        expect(state.fileListIsLoaded).toEqual(true);
        expect(state).not.toBe(fromFile.initialState);
    });

    it('loadFileListFail action should set fileListIsLoading to false', () => {
        const action = fileActions.loadFileListFail();
        const state = fromFile.fileReducer(fromFile.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.fileListIsLoading).toEqual(false);
        expect(state.fileListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromFile.initialState);
    });

    it('addFileSuccess action should add a file', () => {
        const action = fileActions.addFileSuccess({ file: FILE });
        const state = fromFile.fileReducer(fromFile.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(FILE.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.fileListIsLoading).toEqual(false);
        expect(state.fileListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromFile.initialState);
    });

    it('editFileSuccess action should modify a file', () => {
        const initialState = {
            ...fromFile.initialState,
            ids: [FILE.id],
            entities: { [FILE.id]: { ...FILE, label: 'New label' }}
        };
        const action = fileActions.editFileSuccess({ file: FILE });
        const state = fromFile.fileReducer(initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(FILE.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[FILE.id]).toEqual(FILE);
        expect(state.fileListIsLoading).toEqual(false);
        expect(state.fileListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteFileSuccess action should delete a file', () => {
        const initialState = {
            ...fromFile.initialState,
            ids: [FILE.id],
            entities: { [FILE.id]: FILE }
        };
        const action = fileActions.deleteFileSuccess({ file: FILE });
        const state = fromFile.fileReducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.fileListIsLoading).toEqual(false);
        expect(state.fileListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get fileListIsLoading', () => {
        const action = {} as Action;
        const state =  fromFile.fileReducer(undefined, action);

        expect(fromFile.selectFileListIsLoading(state)).toEqual(false);
    });

    it('should get fileListIsLoaded', () => {
        const action = {} as Action;
        const state = fromFile.fileReducer(undefined, action);

        expect(fromFile.selectFileListIsLoaded(state)).toEqual(false);
    });
});
