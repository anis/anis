/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';

import { DetailConfig } from '../models';
import * as detailActions from '../actions/detail-config.actions';

export interface State {
    detailConfig: DetailConfig;
    detailConfigIsLoading: boolean;
    detailConfigIsLoaded: boolean;
}

export const initialState: State = {
    detailConfig: null,
    detailConfigIsLoading: false,
    detailConfigIsLoaded: false
};

export const detailConfigReducer = createReducer(
    initialState,
    on(detailActions.loadDetailConfig, (state) => {
        return {
            ...state,
            detailConfig: null,
            detailConfigIsLoading: true,
            detailConfigIsLoaded: false
        }
    }),
    on(detailActions.loadDetailConfigSuccess, (state, { detailConfig }) => {
        return {
            ...state,
            detailConfig,
            detailConfigIsLoading: false,
            detailConfigIsLoaded: true
        }
    }),
    on(detailActions.loadDetailConfigFail, (state) => {
        return {
            ...state,
            detailConfigIsLoading: false
        }
    }),
    on(detailActions.addDetailConfigSuccess, (state, { detailConfig }) => {
        return {
            ...state,
            detailConfig
        }
    }),
    on(detailActions.editDetailConfigSuccess, (state, { detailConfig }) => {
        return {
            ...state,
            detailConfig
        }
    })
);

export const selectDetailConfig = (state: State) => state.detailConfig;
export const selectDetailConfigIsLoading = (state: State) => state.detailConfigIsLoading;
export const selectDetailConfigIsLoaded = (state: State) => state.detailConfigIsLoaded;
