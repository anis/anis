/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { MenuItem } from '../models';
import * as menuItemActions from '../actions/menu-item.actions';

export interface State extends EntityState<MenuItem> {
    menuItemListIsLoading: boolean;
    menuItemListIsLoaded: boolean;
}

export const adapter: EntityAdapter<MenuItem> = createEntityAdapter<MenuItem>({
    selectId: (menuItem: MenuItem) => menuItem.id,
    sortComparer: (a: MenuItem, b: MenuItem) => a.display - b.display
});

export const initialState: State = adapter.getInitialState({
    menuItemListIsLoading: false,
    menuItemListIsLoaded: false
});

export const menuItemReducer = createReducer(
    initialState,
    on(menuItemActions.loadMenuItemList, (state) => {
        return {
            ...state,
            menuItemListIsLoading: true
        }
    }),
    on(menuItemActions.loadMenuItemListSuccess, (state, { menuItems }) => {
        return adapter.setAll(
            menuItems,
            {
                ...state,
                menuItemListIsLoading: false,
                menuItemListIsLoaded: true
            }
        );
    }),
    on(menuItemActions.loadMenuItemListFail, (state) => {
        return {
            ...state,
            menuItemListIsLoading: false
        }
    }),
    on(menuItemActions.addMenuItemSuccess, (state, { menuItem }) => {
        return adapter.addOne(menuItem, state)
    }),
    on(menuItemActions.editMenuItemSuccess, (state, { menuItem }) => {
        return adapter.setOne(menuItem, state)
    }),
    on(menuItemActions.deleteMenuItemSuccess, (state, { menuItem }) => {
        return adapter.removeOne(menuItem.id, state)
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectMenuItemIds = selectIds;
export const selectMenuItemEntities = selectEntities;
export const selectAllMenuItems = selectAll;
export const selectMenuItemTotal = selectTotal;

export const selectMenuItemListIsLoading = (state: State) => state.menuItemListIsLoading;
export const selectMenuItemListIsLoaded = (state: State) => state.menuItemListIsLoaded;
