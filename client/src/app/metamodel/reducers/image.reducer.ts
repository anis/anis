/**
 * This file is part of Anis Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { Image } from '../models';
import * as imageActions from '../actions/image.actions';

export interface State extends EntityState<Image> {
    imageListIsLoading: boolean;
    imageListIsLoaded: boolean;
}

export const adapter: EntityAdapter<Image> = createEntityAdapter<Image>({
    selectId: (image: Image) => image.id,
    sortComparer: (a: Image, b: Image) => a.id - b.id
});

export const initialState: State = adapter.getInitialState({
    imageListIsLoading: false,
    imageListIsLoaded: false
});

export const imageReducer = createReducer(
    initialState,
    on(imageActions.loadImageList, (state) => {
        return {
            ...state,
            imageListIsLoading: true
        }
    }),
    on(imageActions.loadImageListSuccess, (state, { images }) => {
        return adapter.setAll(
            images,
            {
                ...state,
                imageListIsLoading: false,
                imageListIsLoaded: true
            }
        );
    }),
    on(imageActions.loadImageListFail, (state) => {
        return {
            ...state,
            imageListIsLoading: false
        }
    }),
    on(imageActions.addImageSuccess, (state, { image }) => {
        return adapter.addOne(image, state)
    }),
    on(imageActions.editImageSuccess, (state, { image }) => {
        return adapter.setOne(image, state)
    }),
    on(imageActions.deleteImageSuccess, (state, { image }) => {
        return adapter.removeOne(image.id, state)
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectImageIds = selectIds;
export const selectImageEntities = selectEntities;
export const selectAllImages = selectAll;
export const selectImageTotal = selectTotal;

export const selectImageListIsLoading = (state: State) => state.imageListIsLoading;
export const selectImageListIsLoaded = (state: State) => state.imageListIsLoaded;
