/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromDatasetGroup from './dataset-group.reducer';
import * as datasetGroupActions from '../actions/dataset-group.actions';
import { DATASET } from 'src/test-data';

describe('[Metamodel][Reducers] DatasetGroup reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromDatasetGroup.datasetGroupReducer(fromDatasetGroup.initialState, action);

        expect(state).toBe(fromDatasetGroup.initialState);
    });

    it('loadDatasetGroupList action should set datasetGroupListIsLoading to true', () => {
        const action = datasetGroupActions.loadDatasetGroupList();
        const state = fromDatasetGroup.datasetGroupReducer(fromDatasetGroup.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.datasetGroupListIsLoading).toEqual(true);
        expect(state.datasetGroupListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDatasetGroup.initialState);
    });

    it('loadDatasetGroupListSuccess action should add dataset group list, set datasetGroupListIsLoading to false and set datasetGroupListIsLoaded to true', () => {
        const action = datasetGroupActions.loadDatasetGroupListSuccess({ datasetGroups: DATASET.groups });
        const state = fromDatasetGroup.datasetGroupReducer(fromDatasetGroup.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(DATASET.groups[0].role);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.datasetGroupListIsLoading).toEqual(false);
        expect(state.datasetGroupListIsLoaded).toEqual(true);
        expect(state).not.toBe(fromDatasetGroup.initialState);
    });

    it('loadDatasetGroupListFail action should set datasetGroupListIsLoading to false', () => {
        const action = datasetGroupActions.loadDatasetGroupListFail();
        const state = fromDatasetGroup.datasetGroupReducer(fromDatasetGroup.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.datasetGroupListIsLoading).toEqual(false);
        expect(state.datasetGroupListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDatasetGroup.initialState);
    });

    it('addDatasetGroupSuccess action should add a dataset group', () => {
        const action = datasetGroupActions.addDatasetGroupSuccess({ datasetGroup: DATASET.groups[0] });
        const state = fromDatasetGroup.datasetGroupReducer(fromDatasetGroup.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(DATASET.groups[0].role);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.datasetGroupListIsLoading).toEqual(false);
        expect(state.datasetGroupListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDatasetGroup.initialState);
    });

    it('deleteDatasetGroupSuccess action should delete an dataset group', () => {
        const initialState = {
            ...fromDatasetGroup.initialState,
            ids: [DATASET.groups[0].role],
            entities: { [DATASET.groups[0].role]: DATASET.groups[0] }
        };
        const action = datasetGroupActions.deleteDatasetGroupSuccess({ datasetGroup: DATASET.groups[0] });
        const state = fromDatasetGroup.datasetGroupReducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.datasetGroupListIsLoading).toEqual(false);
        expect(state.datasetGroupListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get datasetGroupListIsLoading', () => {
        const action = {} as Action;
        const state =  fromDatasetGroup.datasetGroupReducer(undefined, action);

        expect(fromDatasetGroup.selectDatasetGroupListIsLoading(state)).toEqual(false);
    });

    it('should get datasetGroupListIsLoaded', () => {
        const action = {} as Action;
        const state = fromDatasetGroup.datasetGroupReducer(undefined, action);

        expect(fromDatasetGroup.selectDatasetGroupListIsLoaded(state)).toEqual(false);
    });
});
