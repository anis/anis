/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromDetailConfig from './detail-config.reducer';
import * as detailConfigActions from '../actions/detail-config.actions';
import { DETAIL_CONFIG } from 'src/test-data';

describe('[Metamodel][Reducers] Detail config reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromDetailConfig.detailConfigReducer(fromDetailConfig.initialState, action);

        expect(state).toBe(fromDetailConfig.initialState);
    });

    it('loadDetailConfig action should set detailConfigIsLoading to true', () => {
        const action = detailConfigActions.loadDetailConfig();
        const state = fromDetailConfig.detailConfigReducer(fromDetailConfig.initialState, action);

        expect(state.detailConfig).toBeNull();
        expect(state.detailConfigIsLoading).toEqual(true);
        expect(state.detailConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDetailConfig.initialState);
    });

    it('loadDetailConfigSuccess action should add detail config, set detailConfigIsLoading to false and set detailConfigIsLoaded to true', () => {
        const action = detailConfigActions.loadDetailConfigSuccess({ detailConfig: DETAIL_CONFIG });
        const state = fromDetailConfig.detailConfigReducer(fromDetailConfig.initialState, action);

        expect(state.detailConfig).toEqual(DETAIL_CONFIG);
        expect(state.detailConfigIsLoading).toEqual(false);
        expect(state.detailConfigIsLoaded).toEqual(true);
        expect(state).not.toBe(fromDetailConfig.initialState);
    });

    it('loadDetailConfigFail action should set detailConfigIsLoading to false', () => {
        const action = detailConfigActions.loadDetailConfigFail();
        const state = fromDetailConfig.detailConfigReducer(fromDetailConfig.initialState, action);

        expect(state.detailConfig).toBeNull();
        expect(state.detailConfigIsLoading).toEqual(false);
        expect(state.detailConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDetailConfig.initialState);
    });

    it('addDetailConfigSuccess action should add detail config', () => {
        const action = detailConfigActions.addDetailConfigSuccess({ detailConfig: DETAIL_CONFIG });
        const state = fromDetailConfig.detailConfigReducer(fromDetailConfig.initialState, action);

        expect(state.detailConfig).toEqual(DETAIL_CONFIG);
        expect(state.detailConfigIsLoading).toEqual(false);
        expect(state.detailConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDetailConfig.initialState);
    });

    it('editDetailConfigSuccess action should edit detail config', () => {
        const initialState = {
            ...fromDetailConfig.initialState,
            detailConfig: { ...DETAIL_CONFIG, detail_background_color: 'blue' }
        };
        const action = detailConfigActions.editDetailConfigSuccess({ detailConfig: DETAIL_CONFIG });
        const state = fromDetailConfig.detailConfigReducer(initialState, action);

        expect(state.detailConfig).toEqual(DETAIL_CONFIG);
        expect(state.detailConfigIsLoading).toEqual(false);
        expect(state.detailConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get detailConfig', () => {
        const action = detailConfigActions.loadDetailConfigSuccess({ detailConfig: DETAIL_CONFIG });
        const state = fromDetailConfig.detailConfigReducer(fromDetailConfig.initialState, action);

        expect(fromDetailConfig.selectDetailConfig(state)).toEqual(DETAIL_CONFIG);
    });

    it('should get detailConfigIsLoading', () => {
        const action = {} as Action;
        const state =  fromDetailConfig.detailConfigReducer(undefined, action);

        expect(fromDetailConfig.selectDetailConfigIsLoading(state)).toEqual(false);
    });

    it('should get instanceListIsLoaded', () => {
        const action = {} as Action;
        const state =  fromDetailConfig.detailConfigReducer(undefined, action);

        expect(fromDetailConfig.selectDetailConfigIsLoaded(state)).toEqual(false);
    });
});
