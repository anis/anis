/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromOutputCategory from './output-category.reducer';
import * as outputCategoryActions from '../actions/output-category.actions';
import { OUTPUT_CATEGORY, OUTPUT_CATEGORY_LIST } from 'src/test-data';

describe('[Metamodel][Reducers] OutputCategory reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromOutputCategory.outputCategoryReducer(fromOutputCategory.initialState, action);

        expect(state).toBe(fromOutputCategory.initialState);
    });

    it('loadOutputCategoryList action should set outputCategoryListIsLoading to true', () => {
        const action = outputCategoryActions.loadOutputCategoryList({ outputFamilyId: 1 });
        const state = fromOutputCategory.outputCategoryReducer(fromOutputCategory.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.outputCategoryListIsLoading).toEqual(true);
        expect(state.outputCategoryListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromOutputCategory.initialState);
    });

    it('loadOutputCategoryListSuccess action should add output category list, set outputCategoryListIsLoading to false and set outputCategoryListIsLoaded to true', () => {
        const action = outputCategoryActions.loadOutputCategoryListSuccess({ outputCategories: OUTPUT_CATEGORY_LIST });
        const state = fromOutputCategory.outputCategoryReducer(fromOutputCategory.initialState, action);

        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain(OUTPUT_CATEGORY_LIST[0].id);
        expect(state.ids).toContain(OUTPUT_CATEGORY_LIST[1].id);
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.outputCategoryListIsLoading).toEqual(false);
        expect(state.outputCategoryListIsLoaded).toEqual(true);
        expect(state).not.toBe(fromOutputCategory.initialState);
    });

    it('loadOutputCategoryListFail action should set outputCategoryListIsLoading to false', () => {
        const action = outputCategoryActions.loadOutputCategoryListFail();
        const state = fromOutputCategory.outputCategoryReducer(fromOutputCategory.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.outputCategoryListIsLoading).toEqual(false);
        expect(state.outputCategoryListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromOutputCategory.initialState);
    });

    it('addOutputCategorySuccess action should add a output category', () => {
        const action = outputCategoryActions.addOutputCategorySuccess({ outputCategory: OUTPUT_CATEGORY });
        const state = fromOutputCategory.outputCategoryReducer(fromOutputCategory.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(OUTPUT_CATEGORY.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.outputCategoryListIsLoading).toEqual(false);
        expect(state.outputCategoryListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromOutputCategory.initialState);
    });

    it('editOutputCategorySuccess action should modify a output category', () => {
        const initialState = {
            ...fromOutputCategory.initialState,
            ids: [OUTPUT_CATEGORY.id],
            entities: { [OUTPUT_CATEGORY.id]: { ...OUTPUT_CATEGORY, label: 'New label' }}
        };
        const action = outputCategoryActions.editOutputCategorySuccess({ outputCategory: OUTPUT_CATEGORY });
        const state = fromOutputCategory.outputCategoryReducer(initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(OUTPUT_CATEGORY.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[OUTPUT_CATEGORY.id]).toEqual(OUTPUT_CATEGORY);
        expect(state.outputCategoryListIsLoading).toEqual(false);
        expect(state.outputCategoryListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteOutputCategorySuccess action should delete an output category', () => {
        const initialState = {
            ...fromOutputCategory.initialState,
            ids: [OUTPUT_CATEGORY.id],
            entities: { [OUTPUT_CATEGORY.id]: OUTPUT_CATEGORY }
        };
        const action = outputCategoryActions.deleteOutputCategorySuccess({ outputCategory: OUTPUT_CATEGORY });
        const state = fromOutputCategory.outputCategoryReducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.outputCategoryListIsLoading).toEqual(false);
        expect(state.outputCategoryListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get outputCategoryListIsLoading', () => {
        const action = {} as Action;
        const state =  fromOutputCategory.outputCategoryReducer(undefined, action);

        expect(fromOutputCategory.selectOutputCategoryListIsLoading(state)).toEqual(false);
    });

    it('should get outputCategoryListIsLoaded', () => {
        const action = {} as Action;
        const state = fromOutputCategory.outputCategoryReducer(undefined, action);

        expect(fromOutputCategory.selectOutputCategoryListIsLoaded(state)).toEqual(false);
    });
});
