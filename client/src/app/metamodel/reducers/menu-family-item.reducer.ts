/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { MenuItem } from '../models';
import * as menuFamilyItemActions from '../actions/menu-family-item.actions';

export interface State extends EntityState<MenuItem> {
    menuFamilyItemListIsLoading: boolean;
    menuFamilyItemListIsLoaded: boolean;
}

export const adapter: EntityAdapter<MenuItem> = createEntityAdapter<MenuItem>({
    selectId: (menuFamilyItem: MenuItem) => menuFamilyItem.id,
    sortComparer: (a: MenuItem, b: MenuItem) => a.display - b.display
});

export const initialState: State = adapter.getInitialState({
    menuFamilyItemListIsLoading: false,
    menuFamilyItemListIsLoaded: false
});

export const menuFamilyItemReducer = createReducer(
    initialState,
    on(menuFamilyItemActions.loadMenuFamilyItemList, (state) => {
        return {
            ...state,
            menuFamilyItemListIsLoading: true
        }
    }),
    on(menuFamilyItemActions.loadMenuFamilyItemListSuccess, (state, { menuFamilyItemList }) => {
        return adapter.setAll(
            menuFamilyItemList,
            {
                ...state,
                menuFamilyItemListIsLoading: false,
                menuFamilyItemListIsLoaded: true
            }
        );
    }),
    on(menuFamilyItemActions.loadMenuFamilyItemListFail, (state) => {
        return {
            ...state,
            menuFamilyItemListIsLoading: false
        }
    }),
    on(menuFamilyItemActions.addMenuFamilyItemSuccess, (state, { menuFamilyItem }) => {
        return adapter.addOne(menuFamilyItem, state)
    }),
    on(menuFamilyItemActions.editMenuFamilyItemSuccess, (state, { menuFamilyItem }) => {
        return adapter.setOne(menuFamilyItem, state)
    }),
    on(menuFamilyItemActions.deleteMenuFamilyItemSuccess, (state, { menuFamilyItem }) => {
        return adapter.removeOne(menuFamilyItem.id, state)
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectMenuFamilyItemIds = selectIds;
export const selectMenuFamilyItemEntities = selectEntities;
export const selectAllMenuFamilyItems = selectAll;
export const selectMenuFamilyItemTotal = selectTotal;

export const selectMenuFamilyItemListIsLoading = (state: State) => state.menuFamilyItemListIsLoading;
export const selectMenuFamilyItemListIsLoaded = (state: State) => state.menuFamilyItemListIsLoaded;
