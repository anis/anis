/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromDesignConfig from './design-config.reducer';
import * as designConfigActions from '../actions/design-config.actions';
import { DESIGN_CONFIG } from 'src/test-data';

describe('[Metamodel][Reducers] Design config reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromDesignConfig.designConfigReducer(fromDesignConfig.initialState, action);

        expect(state).toBe(fromDesignConfig.initialState);
    });

    it('loadDesignConfig action should set designConfigIsLoading to true', () => {
        const action = designConfigActions.loadDesignConfig();
        const state = fromDesignConfig.designConfigReducer(fromDesignConfig.initialState, action);

        expect(state.designConfig).toBeNull();
        expect(state.designConfigIsLoading).toEqual(true);
        expect(state.designConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDesignConfig.initialState);
    });

    it('loadDesignConfigSuccess action should add design config, set designConfigIsLoading to false and set designConfigIsLoaded to true', () => {
        const action = designConfigActions.loadDesignConfigSuccess({ designConfig: DESIGN_CONFIG });
        const state = fromDesignConfig.designConfigReducer(fromDesignConfig.initialState, action);

        expect(state.designConfig).toEqual(DESIGN_CONFIG);
        expect(state.designConfigIsLoading).toEqual(false);
        expect(state.designConfigIsLoaded).toEqual(true);
        expect(state).not.toBe(fromDesignConfig.initialState);
    });

    it('loadDesignConfigFail action should set designConfigIsLoading to false', () => {
        const action = designConfigActions.loadDesignConfigFail();
        const state = fromDesignConfig.designConfigReducer(fromDesignConfig.initialState, action);

        expect(state.designConfig).toBeNull();
        expect(state.designConfigIsLoading).toEqual(false);
        expect(state.designConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDesignConfig.initialState);
    });

    it('addDesignConfigSuccess action should add design config', () => {
        const action = designConfigActions.addDesignConfigSuccess({ designConfig: DESIGN_CONFIG });
        const state = fromDesignConfig.designConfigReducer(fromDesignConfig.initialState, action);

        expect(state.designConfig).toEqual(DESIGN_CONFIG);
        expect(state.designConfigIsLoading).toEqual(false);
        expect(state.designConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDesignConfig.initialState);
    });

    it('editDesignConfigSuccess action should edit design config', () => {
        const initialState = {
            ...fromDesignConfig.initialState,
            designConfig: { ...DESIGN_CONFIG, design_background_color: 'blue' }
        };
        const action = designConfigActions.editDesignConfigSuccess({ designConfig: DESIGN_CONFIG });
        const state = fromDesignConfig.designConfigReducer(initialState, action);

        expect(state.designConfig).toEqual(DESIGN_CONFIG);
        expect(state.designConfigIsLoading).toEqual(false);
        expect(state.designConfigIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get designConfig', () => {
        const action = designConfigActions.loadDesignConfigSuccess({ designConfig: DESIGN_CONFIG });
        const state = fromDesignConfig.designConfigReducer(fromDesignConfig.initialState, action);

        expect(fromDesignConfig.selectDesignConfig(state)).toEqual(DESIGN_CONFIG);
    });

    it('should get designConfigIsLoading', () => {
        const action = {} as Action;
        const state =  fromDesignConfig.designConfigReducer(undefined, action);

        expect(fromDesignConfig.selectDesignConfigIsLoading(state)).toEqual(false);
    });

    it('should get instanceListIsLoaded', () => {
        const action = {} as Action;
        const state =  fromDesignConfig.designConfigReducer(undefined, action);

        expect(fromDesignConfig.selectDesignConfigIsLoaded(state)).toEqual(false);
    });
});
