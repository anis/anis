/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { Attribute } from '../models';
import * as attributeActions from '../actions/attribute.actions';

export interface State extends EntityState<Attribute> {
    attributeListIsLoading: boolean;
    attributeListIsLoaded: boolean;
}

export const adapter: EntityAdapter<Attribute> = createEntityAdapter<Attribute>();

export const initialState: State = adapter.getInitialState({
    attributeListIsLoading: false,
    attributeListIsLoaded: false
});

export const attributeReducer = createReducer(
    initialState,
    on(attributeActions.resetAttributeList, () => {
        return {
            ...initialState
        }
    }),
    on(attributeActions.loadAttributeList, (state) => {
        return {
            ...state,
            attributeListIsLoading: true,
            attributeListIsLoaded: false
        }
    }),
    on(attributeActions.loadAttributeListSuccess, (state, { attributes }) => {
        return adapter.setAll(
            attributes,
            {
                ...state,
                attributeListIsLoading: false,
                attributeListIsLoaded: true
            }
        );
    }),
    on(attributeActions.loadAttributeListFail, (state) => {
        return {
            ...state,
            attributeListIsLoading: false
        }
    }),
    on(attributeActions.addAttributeSuccess, (state, { attribute }) => {
        return adapter.addOne(attribute, state)
    }),
    on(attributeActions.editAttributeSuccess, (state, { attribute }) => {
        return adapter.setOne(attribute, state)
    }),
    on(attributeActions.deleteAttributeSuccess, (state, { attribute }) => {
        return adapter.removeOne(attribute.id, state)
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectAttributeIds = selectIds;
export const selectAttributeEntities = selectEntities;
export const selectAllAttributes = selectAll;
export const selectAttributeTotal = selectTotal;

export const selectAttributeListIsLoading = (state: State) => state.attributeListIsLoading;
export const selectAttributeListIsLoaded = (state: State) => state.attributeListIsLoaded;
