/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';

import { DatasetFamily } from '../models';
import * as datasetFamilyActions from '../actions/dataset-family.actions';

export interface State extends EntityState<DatasetFamily> {
    datasetFamilyListIsLoading: boolean;
    datasetFamilyListIsLoaded: boolean;
}

export const adapter: EntityAdapter<DatasetFamily> = createEntityAdapter<DatasetFamily>({
    selectId: (datasetFamily: DatasetFamily) => datasetFamily.id,
    sortComparer: (a: DatasetFamily, b: DatasetFamily) => a.display - b.display
});

export const initialState: State = adapter.getInitialState({
    datasetFamilyListIsLoading: false,
    datasetFamilyListIsLoaded: false
});

export const datasetFamilyReducer = createReducer(
    initialState,
    on(datasetFamilyActions.loadDatasetFamilyList, (state) => {
        return {
            ...state,
            datasetFamilyListIsLoading: true
        }
    }),
    on(datasetFamilyActions.loadDatasetFamilyListSuccess, (state, { datasetFamilies }) => {
        return adapter.setAll(
            datasetFamilies,
            {
                ...state,
                datasetFamilyListIsLoading: false,
                datasetFamilyListIsLoaded: true
            }
        );
    }),
    on(datasetFamilyActions.loadDatasetFamilyListFail, (state) => {
        return {
            ...state,
            datasetFamilyListIsLoading: false
        }
    }),
    on(datasetFamilyActions.addDatasetFamilySuccess, (state, { datasetFamily }) => {
        return adapter.addOne(datasetFamily, state)
    }),
    on(datasetFamilyActions.editDatasetFamilySuccess, (state, { datasetFamily }) => {
        return adapter.setOne(datasetFamily, state)
    }),
    on(datasetFamilyActions.deleteDatasetFamilySuccess, (state, { datasetFamily }) => {
        return adapter.removeOne(datasetFamily.id, state)
    })
);

const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const selectDatasetFamilyIds = selectIds;
export const selectDatasetFamilyEntities = selectEntities;
export const selectAllDatasetFamilies = selectAll;
export const selectDatasetFamilyTotal = selectTotal;

export const selectDatasetFamilyListIsLoading = (state: State) => state.datasetFamilyListIsLoading;
export const selectDatasetFamilyListIsLoaded = (state: State) => state.datasetFamilyListIsLoaded;
