/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromMenuItem from './menu-item.reducer';
import * as menuItemActions from '../actions/menu-item.actions';
import { MENU_ITEM_LIST } from 'src/test-data';

describe('[Metamodel][Reducers] MenuItem reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromMenuItem.menuItemReducer(fromMenuItem.initialState, action);

        expect(state).toBe(fromMenuItem.initialState);
    });

    it('loadMenuItemList action should set menuItemListIsLoading to true', () => {
        const action = menuItemActions.loadMenuItemList();
        const state = fromMenuItem.menuItemReducer(fromMenuItem.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.menuItemListIsLoading).toEqual(true);
        expect(state.menuItemListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromMenuItem.initialState);
    });

    it('loadMenuItemListSuccess action should add menu item list, set menuItemListIsLoading to false and set menuItemListIsLoaded to true', () => {
        const action = menuItemActions.loadMenuItemListSuccess({ menuItems: MENU_ITEM_LIST });
        const state = fromMenuItem.menuItemReducer(fromMenuItem.initialState, action);

        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain(MENU_ITEM_LIST[0].id);
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.menuItemListIsLoading).toEqual(false);
        expect(state.menuItemListIsLoaded).toEqual(true);
        expect(state).not.toBe(fromMenuItem.initialState);
    });

    it('loadMenuItemListFail action should set menuItemListIsLoading to false', () => {
        const action = menuItemActions.loadMenuItemListFail();
        const state = fromMenuItem.menuItemReducer(fromMenuItem.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.menuItemListIsLoading).toEqual(false);
        expect(state.menuItemListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromMenuItem.initialState);
    });

    it('addMenuItemSuccess action should add a menu item', () => {
        const action = menuItemActions.addMenuItemSuccess({ menuItem: MENU_ITEM_LIST[0] });
        const state = fromMenuItem.menuItemReducer(fromMenuItem.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(MENU_ITEM_LIST[0].id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.menuItemListIsLoading).toEqual(false);
        expect(state.menuItemListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromMenuItem.initialState);
    });

    it('editMenuItemSuccess action should modify a menu item', () => {
        const initialState = {
            ...fromMenuItem.initialState,
            ids: [MENU_ITEM_LIST[0].id],
            entities: { [MENU_ITEM_LIST[0].id]: { ...MENU_ITEM_LIST[0], label: 'New label' }}
        };
        const action = menuItemActions.editMenuItemSuccess({ menuItem: MENU_ITEM_LIST[0] });
        const state = fromMenuItem.menuItemReducer(initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(MENU_ITEM_LIST[0].id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[MENU_ITEM_LIST[0].id]).toEqual(MENU_ITEM_LIST[0]);
        expect(state.menuItemListIsLoading).toEqual(false);
        expect(state.menuItemListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteMenuItemSuccess action should delete a menu item', () => {
        const initialState = {
            ...fromMenuItem.initialState,
            ids: [MENU_ITEM_LIST[0].id],
            entities: { [MENU_ITEM_LIST[0].id]: MENU_ITEM_LIST[0] }
        };
        const action = menuItemActions.deleteMenuItemSuccess({ menuItem: MENU_ITEM_LIST[0] });
        const state = fromMenuItem.menuItemReducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.menuItemListIsLoading).toEqual(false);
        expect(state.menuItemListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get menuItemListIsLoading', () => {
        const action = {} as Action;
        const state =  fromMenuItem.menuItemReducer(undefined, action);

        expect(fromMenuItem.selectMenuItemListIsLoading(state)).toEqual(false);
    });

    it('should get menuItemListIsLoaded', () => {
        const action = {} as Action;
        const state = fromMenuItem.menuItemReducer(undefined, action);

        expect(fromMenuItem.selectMenuItemListIsLoaded(state)).toEqual(false);
    });
});
