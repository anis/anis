/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromImage from './image.reducer';
import * as imageActions from '../actions/image.actions';
import { IMAGE } from 'src/test-data';

describe('[Metamodel][Reducers] Image reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromImage.imageReducer(fromImage.initialState, action);

        expect(state).toBe(fromImage.initialState);
    });

    it('loadImageList action should set imageListIsLoading to true', () => {
        const action = imageActions.loadImageList();
        const state = fromImage.imageReducer(fromImage.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.imageListIsLoading).toEqual(true);
        expect(state.imageListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromImage.initialState);
    });

    it('loadImageListSuccess action should add image list, set imageListIsLoading to false and set imageListIsLoaded to true', () => {
        const action = imageActions.loadImageListSuccess({ images: [IMAGE] });
        const state = fromImage.imageReducer(fromImage.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(IMAGE.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.imageListIsLoading).toEqual(false);
        expect(state.imageListIsLoaded).toEqual(true);
        expect(state).not.toBe(fromImage.initialState);
    });

    it('loadImageListFail action should set imageListIsLoading to false', () => {
        const action = imageActions.loadImageListFail();
        const state = fromImage.imageReducer(fromImage.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.imageListIsLoading).toEqual(false);
        expect(state.imageListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromImage.initialState);
    });

    it('addImageSuccess action should add a image', () => {
        const action = imageActions.addImageSuccess({ image: IMAGE });
        const state = fromImage.imageReducer(fromImage.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(IMAGE.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.imageListIsLoading).toEqual(false);
        expect(state.imageListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromImage.initialState);
    });

    it('editImageSuccess action should modify a image', () => {
        const initialState = {
            ...fromImage.initialState,
            ids: [IMAGE.id],
            entities: { [IMAGE.id]: { ...IMAGE, label: 'New label' }}
        };
        const action = imageActions.editImageSuccess({ image: IMAGE });
        const state = fromImage.imageReducer(initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(IMAGE.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[IMAGE.id]).toEqual(IMAGE);
        expect(state.imageListIsLoading).toEqual(false);
        expect(state.imageListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteImageSuccess action should delete a image', () => {
        const initialState = {
            ...fromImage.initialState,
            ids: [IMAGE.id],
            entities: { [IMAGE.id]: IMAGE }
        };
        const action = imageActions.deleteImageSuccess({ image: IMAGE });
        const state = fromImage.imageReducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.imageListIsLoading).toEqual(false);
        expect(state.imageListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get imageListIsLoading', () => {
        const action = {} as Action;
        const state =  fromImage.imageReducer(undefined, action);

        expect(fromImage.selectImageListIsLoading(state)).toEqual(false);
    });

    it('should get imageListIsLoaded', () => {
        const action = {} as Action;
        const state = fromImage.imageReducer(undefined, action);

        expect(fromImage.selectImageListIsLoaded(state)).toEqual(false);
    });
});
