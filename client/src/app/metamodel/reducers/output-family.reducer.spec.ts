/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromOutputFamily from './output-family.reducer';
import * as outputFamilyActions from '../actions/output-family.actions';
import { OUTPUT_FAMILY, OUTPUT_FAMILY_LIST } from 'src/test-data';

describe('[Metamodel][Reducers] OutputFamily reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromOutputFamily.outputFamilyReducer(fromOutputFamily.initialState, action);

        expect(state).toBe(fromOutputFamily.initialState);
    });

    it('loadOutputFamilyList action should set outputFamilyListIsLoading to true', () => {
        const action = outputFamilyActions.loadOutputFamilyList();
        const state = fromOutputFamily.outputFamilyReducer(fromOutputFamily.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.outputFamilyListIsLoading).toEqual(true);
        expect(state.outputFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromOutputFamily.initialState);
    });

    it('loadOutputFamilyListSuccess action should add output family list, set outputFamilyListIsLoading to false and set outputFamilyListIsLoaded to true', () => {
        const action = outputFamilyActions.loadOutputFamilyListSuccess({ outputFamilies: OUTPUT_FAMILY_LIST });
        const state = fromOutputFamily.outputFamilyReducer(fromOutputFamily.initialState, action);

        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain(OUTPUT_FAMILY_LIST[0].id);
        expect(state.ids).toContain(OUTPUT_FAMILY_LIST[1].id);
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.outputFamilyListIsLoading).toEqual(false);
        expect(state.outputFamilyListIsLoaded).toEqual(true);
        expect(state).not.toBe(fromOutputFamily.initialState);
    });

    it('loadOutputFamilyListFail action should set outputFamilyListIsLoading to false', () => {
        const action = outputFamilyActions.loadOutputFamilyListFail();
        const state = fromOutputFamily.outputFamilyReducer(fromOutputFamily.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.outputFamilyListIsLoading).toEqual(false);
        expect(state.outputFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromOutputFamily.initialState);
    });

    it('addOutputFamilySuccess action should add a output family', () => {
        const action = outputFamilyActions.addOutputFamilySuccess({ outputFamily: OUTPUT_FAMILY });
        const state = fromOutputFamily.outputFamilyReducer(fromOutputFamily.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(OUTPUT_FAMILY.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.outputFamilyListIsLoading).toEqual(false);
        expect(state.outputFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromOutputFamily.initialState);
    });

    it('editOutputFamilySuccess action should modify a output family', () => {
        const initialState = {
            ...fromOutputFamily.initialState,
            ids: [OUTPUT_FAMILY.id],
            entities: { [OUTPUT_FAMILY.id]: { ...OUTPUT_FAMILY, label: 'New label' }}
        };
        const action = outputFamilyActions.editOutputFamilySuccess({ outputFamily: OUTPUT_FAMILY });
        const state = fromOutputFamily.outputFamilyReducer(initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(OUTPUT_FAMILY.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[OUTPUT_FAMILY.id]).toEqual(OUTPUT_FAMILY);
        expect(state.outputFamilyListIsLoading).toEqual(false);
        expect(state.outputFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteOutputFamilySuccess action should delete an output family', () => {
        const initialState = {
            ...fromOutputFamily.initialState,
            ids: [OUTPUT_FAMILY.id],
            entities: { [OUTPUT_FAMILY.id]: OUTPUT_FAMILY }
        };
        const action = outputFamilyActions.deleteOutputFamilySuccess({ outputFamily: OUTPUT_FAMILY });
        const state = fromOutputFamily.outputFamilyReducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.outputFamilyListIsLoading).toEqual(false);
        expect(state.outputFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get outputFamilyListIsLoading', () => {
        const action = {} as Action;
        const state =  fromOutputFamily.outputFamilyReducer(undefined, action);

        expect(fromOutputFamily.selectOutputFamilyListIsLoading(state)).toEqual(false);
    });

    it('should get outputFamilyListIsLoaded', () => {
        const action = {} as Action;
        const state = fromOutputFamily.outputFamilyReducer(undefined, action);

        expect(fromOutputFamily.selectOutputFamilyListIsLoaded(state)).toEqual(false);
    });
});
