/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromDatasetFamily from './dataset-family.reducer';
import * as datasetFamilyActions from '../actions/dataset-family.actions';
import { DATASET_FAMILY, DATASET_FAMILY_LIST } from 'src/test-data';

describe('[Metamodel][Reducers] DatasetFamily reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromDatasetFamily.datasetFamilyReducer(fromDatasetFamily.initialState, action);

        expect(state).toBe(fromDatasetFamily.initialState);
    });

    it('loadDatasetFamilyList action should set datasetFamilyListIsLoading to true', () => {
        const action = datasetFamilyActions.loadDatasetFamilyList();
        const state = fromDatasetFamily.datasetFamilyReducer(fromDatasetFamily.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.datasetFamilyListIsLoading).toEqual(true);
        expect(state.datasetFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDatasetFamily.initialState);
    });

    it('loadDatasetFamilyListSuccess action should add dataset family list, set datasetFamilyListIsLoading to false and set datasetFamilyListIsLoaded to true', () => {
        const action = datasetFamilyActions.loadDatasetFamilyListSuccess({ datasetFamilies: DATASET_FAMILY_LIST });
        const state = fromDatasetFamily.datasetFamilyReducer(fromDatasetFamily.initialState, action);

        expect(state.ids.length).toEqual(2);
        expect(state.ids).toContain(DATASET_FAMILY_LIST[0].id);
        expect(state.ids).toContain(DATASET_FAMILY_LIST[1].id);
        expect(Object.keys(state.entities).length).toEqual(2);
        expect(state.datasetFamilyListIsLoading).toEqual(false);
        expect(state.datasetFamilyListIsLoaded).toEqual(true);
        expect(state).not.toBe(fromDatasetFamily.initialState);
    });

    it('loadDatasetFamilyListFail action should set datasetFamilyListIsLoading to false', () => {
        const action = datasetFamilyActions.loadDatasetFamilyListFail();
        const state = fromDatasetFamily.datasetFamilyReducer(fromDatasetFamily.initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.datasetFamilyListIsLoading).toEqual(false);
        expect(state.datasetFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDatasetFamily.initialState);
    });

    it('addDatasetFamilySuccess action should add a dataset family', () => {
        const action = datasetFamilyActions.addDatasetFamilySuccess({ datasetFamily: DATASET_FAMILY });
        const state = fromDatasetFamily.datasetFamilyReducer(fromDatasetFamily.initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(DATASET_FAMILY.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.datasetFamilyListIsLoading).toEqual(false);
        expect(state.datasetFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(fromDatasetFamily.initialState);
    });

    it('editDatasetFamilySuccess action should modify a dataset family', () => {
        const initialState = {
            ...fromDatasetFamily.initialState,
            ids: [DATASET_FAMILY.id],
            entities: { [DATASET_FAMILY.id]: { ...DATASET_FAMILY, label: 'New label' }}
        };
        const action = datasetFamilyActions.editDatasetFamilySuccess({ datasetFamily: DATASET_FAMILY });
        const state = fromDatasetFamily.datasetFamilyReducer(initialState, action);

        expect(state.ids.length).toEqual(1);
        expect(state.ids).toContain(DATASET_FAMILY.id);
        expect(Object.keys(state.entities).length).toEqual(1);
        expect(state.entities[DATASET_FAMILY.id]).toEqual(DATASET_FAMILY);
        expect(state.datasetFamilyListIsLoading).toEqual(false);
        expect(state.datasetFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('deleteDatasetFamilySuccess action should delete an dataset family', () => {
        const initialState = {
            ...fromDatasetFamily.initialState,
            ids: [DATASET_FAMILY.id],
            entities: { [DATASET_FAMILY.id]: DATASET_FAMILY }
        };
        const action = datasetFamilyActions.deleteDatasetFamilySuccess({ datasetFamily: DATASET_FAMILY });
        const state = fromDatasetFamily.datasetFamilyReducer(initialState, action);

        expect(state.ids.length).toEqual(0);
        expect(state.entities).toEqual({ });
        expect(state.datasetFamilyListIsLoading).toEqual(false);
        expect(state.datasetFamilyListIsLoaded).toEqual(false);
        expect(state).not.toBe(initialState);
    });

    it('should get datasetFamilyListIsLoading', () => {
        const action = {} as Action;
        const state =  fromDatasetFamily.datasetFamilyReducer(undefined, action);

        expect(fromDatasetFamily.selectDatasetFamilyListIsLoading(state)).toEqual(false);
    });

    it('should get datasetFamilyListIsLoaded', () => {
        const action = {} as Action;
        const state = fromDatasetFamily.datasetFamilyReducer(undefined, action);

        expect(fromDatasetFamily.selectDatasetFamilyListIsLoaded(state)).toEqual(false);
    });
});
