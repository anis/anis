/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { EffectsMetadata, getEffectsMetadata } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { ToastrService } from 'ngx-toastr';

import { InstanceGroupEffects } from './instance-group.effects';
import { InstanceGroupService } from '../services/instance-group.service';
import * as instanceGroupActions from '../actions/instance-group.actions';
import * as instanceSelector from '../selectors/instance.selector';
import { INSTANCE } from 'src/test-data';

describe('[Metamodel][Effects] InstanceGroupEffects', () => {
    let actions = new Observable();
    let effects: InstanceGroupEffects;
    let metadata: EffectsMetadata<InstanceGroupEffects>;
    let service: InstanceGroupService;
    let toastr: ToastrService;
    let store: MockStore;
    const initialState = { metamodel: {} };
    let mockInstanceSelectorSelectInstanceNameByRoute;
    let router: Router;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                InstanceGroupEffects,
                { provide: InstanceGroupService, useValue: { }},
                { provide: Router, useValue: { navigate: jest.fn() }},
                { provide: ToastrService, useValue: {
                        success: jest.fn(),
                        error: jest.fn()
                    }
                },
                provideMockActions(() => actions),
                provideMockStore({ initialState })
            ]
        }).compileComponents();
        effects = TestBed.inject(InstanceGroupEffects);
        metadata = getEffectsMetadata(effects);
        service = TestBed.inject(InstanceGroupService);
        toastr = TestBed.inject(ToastrService);
        router = TestBed.inject(Router);
        store = TestBed.inject(MockStore);
        mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
            instanceSelector.selectInstanceNameByRoute, ''
        );
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadInstanceGroups$ effect', () => {
        it('should dispatch the loadInstanceGroupListSuccess action on success', () => {
            mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
                instanceSelector.selectInstanceNameByRoute, 'my-instance'
            );

            const action = instanceGroupActions.loadInstanceGroupList();
            const outcome = instanceGroupActions.loadInstanceGroupListSuccess({ instanceGroups: INSTANCE.groups });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: INSTANCE.groups });
            const expected = cold('--b', { b: outcome });
            service.retrieveInstanceGroupList = jest.fn(() => response);

            expect(effects.loadInstanceGroups$).toBeObservable(expected);
            expect(service.retrieveInstanceGroupList).toHaveBeenCalledWith('my-instance');
        });

        it('should dispatch the loadInstanceGroupListFail action on HTTP failure', () => {
            const action = instanceGroupActions.loadInstanceGroupList();
            const error = new Error();
            const outcome = instanceGroupActions.loadInstanceGroupListFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.retrieveInstanceGroupList = jest.fn(() => response);

            expect(effects.loadInstanceGroups$).toBeObservable(expected);
        });
    });

    describe('addInstanceGroup$ effect', () => {
        it('should dispatch the addInstanceGroupSuccess action on success', () => {
            let instanceGroup = INSTANCE.groups[0];
            const action = instanceGroupActions.addInstanceGroup({ instanceGroup });
            const outcome = instanceGroupActions.addInstanceGroupSuccess({ instanceGroup });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: instanceGroup });
            const expected = cold('--b', { b: outcome });
            service.addInstanceGroup = jest.fn(() => response);

            expect(effects.addInstanceGroup$).toBeObservable(expected);
        });

        it('should dispatch the addInstanceGroupFail action on HTTP failure', () => {
            const action = instanceGroupActions.addInstanceGroup({ instanceGroup: INSTANCE.groups[0] });
            const error = new Error();
            const outcome = instanceGroupActions.addInstanceGroupFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.addInstanceGroup = jest.fn(() => response);

            expect(effects.addInstanceGroup$).toBeObservable(expected);
        });
    });

    // describe('addInstanceGroupSuccess$ effect', () => {
    //     it('should display success notification and call router.naviteByUrl', () => {
    //         let spyOnRouterNavigate = jest.spyOn(router, 'navigate');
    //         let spyOnToast = jest.spyOn(toastr, 'success');
    //         mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
    //             instanceSelector.selectInstanceNameByRoute, 'my-instance'
    //         );
    //         let instanceGroup = { ...INSTANCE.groups[0] };
    //         actions = hot('a', { a: instanceGroupActions.addInstanceGroupSuccess({ instanceGroup } ) });
    //         let expected = cold('b', { b: instanceGroupActions.addInstanceGroupSuccess({ instanceGroup }) });
    //         //expect(effects.addInstanceGroupSuccess$).toBeObservable(expected);
    //         expect(spyOnToast).toHaveBeenCalledWith('Instance group successfully added', 'The new instance group was added into the database');
    //         expect(spyOnRouterNavigate).toHaveBeenCalledWith(['/admin/instance/instance-group']);
    //     });
    // });

    describe('addInstanceGroupFail$ effect', () => {
        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = instanceGroupActions.addInstanceGroupFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.addInstanceGroupFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to add instance group',
                'The new instance group could not be added into the database'
            );
        });
    });

    describe('editInstanceGroup$ effect', () => {
        it('should dispatch the editInstanceGroupSuccess action on success', () => {
            const action = instanceGroupActions.editInstanceGroup({ instanceGroup: INSTANCE.groups[0] });
            const outcome = instanceGroupActions.editInstanceGroupSuccess({ instanceGroup: INSTANCE.groups[0] });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: INSTANCE.groups[0] });
            const expected = cold('--b', { b: outcome });
            service.editInstanceGroup = jest.fn(() => response);

            expect(effects.editInstanceGroup$).toBeObservable(expected);
        });

        it('should dispatch the editInstanceGroupFail action on HTTP failure', () => {
            const action = instanceGroupActions.editInstanceGroup({ instanceGroup: INSTANCE.groups[0] });
            const error = new Error();
            const outcome = instanceGroupActions.editInstanceGroupFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.editInstanceGroup = jest.fn(() => response);

            expect(effects.editInstanceGroup$).toBeObservable(expected);
        });
    });

    // describe('editInstanceGroupSuccess$ effect', () => {
    //     it('should display success notification and call router.naviteByUrl', () => {
    //         let spyOnRouterNavigate = jest.spyOn(router, 'navigate');
    //         let spyOnToast = jest.spyOn(toastr, 'success');
    //         mockInstanceSelectorSelectInstanceNameByRoute = store.overrideSelector(
    //             instanceSelector.selectInstanceNameByRoute, 'my-instance'
    //         );
    //         actions = hot('a', { a: instanceGroupActions.editInstanceGroupSuccess({ instanceGroup: { ...INSTANCE.groups[0] } }) });
    //         let expected = cold('b', { b: instanceGroupActions.editInstanceGroupSuccess({ instanceGroup: { ...INSTANCE.groups[0] } }) });
    //         expect(effects.editInstanceGroupSuccess$).toBeObservable(expected);
    //         expect(spyOnToast).toHaveBeenCalledWith('Instance group successfully edited', 'The existing instance group has been edited into the database');
    //         expect(spyOnRouterNavigate).toHaveBeenCalledWith([`/admin/instance/instance-group`]);
    //     });
    // });

    describe('editInstanceGroupFail$ effect', () => {
        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = instanceGroupActions.editInstanceGroupFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.editInstanceGroupFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to edit instance group',
                'The existing instance group could not be edited into the database'
            );
        });
    });

    describe('deleteInstanceGroup$ effect', () => {
        it('should dispatch the deleteInstanceGroupSuccess action on success', () => {
            const action = instanceGroupActions.deleteInstanceGroup({ instanceGroup: INSTANCE.groups[0] });
            const outcome = instanceGroupActions.deleteInstanceGroupSuccess({ instanceGroup: INSTANCE.groups[0] });

            actions = hot('-a', { a: action });
            const response = cold('-a|', { a: INSTANCE.groups[0] });
            const expected = cold('--b', { b: outcome });
            service.deleteInstanceGroup = jest.fn(() => response);

            expect(effects.deleteInstanceGroup$).toBeObservable(expected);
        });

        it('should dispatch the deleteInstanceGroupFail action on HTTP failure', () => {
            const action = instanceGroupActions.deleteInstanceGroup({ instanceGroup: INSTANCE.groups[0] });
            const error = new Error();
            const outcome = instanceGroupActions.deleteInstanceGroupFail();

            actions = hot('-a', { a: action });
            const response = cold('-#|', { }, error);
            const expected = cold('--b', { b: outcome });
            service.deleteInstanceGroup = jest.fn(() => response);

            expect(effects.deleteInstanceGroup$).toBeObservable(expected);
        });
    });

    describe('deleteInstanceGroupSuccess$ effect', () => {
        it('should display a success notification', () => {
            const toastrSpy = jest.spyOn(toastr, 'success');
            const action = instanceGroupActions.deleteInstanceGroupSuccess({ instanceGroup: { ...INSTANCE.groups[0] } });

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteInstanceGroupSuccess$).toBeObservable(expected);
            expect(toastrSpy).toHaveBeenCalledTimes(1);
            expect(toastrSpy).toHaveBeenCalledWith(
                'Instance group successfully deleted',
                'The existing instance group has been deleted'
            );
        });
    });

    describe('deleteDatasetFail$ effect', () => {
        it('should display an error notification', () => {
            const spy = jest.spyOn(toastr, 'error');
            const action = instanceGroupActions.deleteInstanceGroupFail();

            actions = hot('a', { a: action });
            const expected = cold('a', { a: action });

            expect(effects.deleteInstanceGroupFail$).toBeObservable(expected);
            expect(spy).toHaveBeenCalledTimes(1);
            expect(spy).toHaveBeenCalledWith(
                'Failure to delete instance group',
                'The existing instance group could not be deleted from the database'
            );
        });
    });
});
