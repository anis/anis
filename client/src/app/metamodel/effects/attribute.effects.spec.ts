/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Action } from '@ngrx/store';
import { cold } from 'jasmine-marbles';

import * as instanceSelector from '../selectors/instance.selector';
import * as datasetSelector from '../selectors/dataset.selector';
import { AttributeEffects } from './attribute.effects';
import { AttributeService } from '../services/attribute.service';
import { ATTRIBUTE, ATTRIBUTE_LIST } from 'src/test-data';

describe('[Metamodel][Effects] AttributeEffects', () => {
    let effects: AttributeEffects = null;
    let attributeService: AttributeService = null;
    let action$ = new Observable<Action>();
    const initialState = {
        selectors: [
            {
                selector: instanceSelector.selectInstanceNameByRoute,
                value: 'vimuser',
            },
            {
                selector: datasetSelector.selectDatasetNameByRoute,
                value: 'coconut',
            },
        ],
    };
    const toastr = { success: jest.fn(), error: jest.fn() };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                AttributeEffects,
                {
                    provide: AttributeService,
                    useValue: {},
                },
                {
                    provide: ToastrService,
                    useValue: toastr,
                },
                provideMockActions(() => action$),
                provideMockStore(initialState),
            ],
        }).compileComponents();

        effects = TestBed.inject(AttributeEffects);
        attributeService = TestBed.inject(AttributeService);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadAttributes$ effect', () => {
        it('should load attributes properly when payload is right', () => {
            const attr = ATTRIBUTE_LIST;
            attributeService.retrieveAttributeList = () => of(attr);
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Attribute List' },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Load Attribute List Success',
                    attributes: attr,
                },
            });

            expect(effects.loadAttributes$).toBeObservable(expected$);
        });

        it('should not load attributes when payload is wrong', () => {
            attributeService.retrieveAttributeList = () =>
                throwError(() => {
                    const error = new Error('An Error happened');
                    return error;
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Attribute List' },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Load Attribute List Fail',
                },
            });

            expect(effects.loadAttributes$).toBeObservable(expected$);
        });
    });

    describe('addAttributeList$ effect', () => {
        it('should add attributes list properly if payload is right', () => {
            const attrs = ATTRIBUTE_LIST;
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Attribute List',
                    attributeList: attrs,
                },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Attribute List',
                    attributeList: attrs,
                },
            });

            expect(effects.addAttributeList$).toBeObservable(expected$);
        });
    });

    describe('addAttribute$  effect', () => {
        it('should add attributes properly when payload is right', () => {
            const attr = ATTRIBUTE;
            attributeService.addAttribute = () => of(attr);
            action$ = cold('a', {
                a: { type: '[Metamodel] Add Attribute' },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Attribute Success',
                    attribute: attr,
                },
            });

            expect(effects.addAttribute$).toBeObservable(expected$);
        });

        it('should not add attributes when payload is wrong', () => {
            attributeService.addAttribute = () =>
                throwError(() => {
                    const error = new Error('An Error happened');
                    return error;
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Add Attribute' },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Attribute Fail',
                },
            });

            expect(effects.addAttribute$).toBeObservable(expected$);
        });
    });

    describe('addAttributeSuccess$ effect', () => {
        it('should toast a success', () => {
            action$ = of({ type: '[Metamodel] Add Attribute Success' });
            effects.addAttributeSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });
    });

    describe('addAttributeFail$ effect', () => {
        it('should toast a fail', () => {
            action$ = of({ type: '[Metamodel] Add Attribute Fail' });
            effects.addAttributeFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });

    describe('editAttribute$ effect', () => {
        it('should edit attributes properly when payload is right', () => {
            const attr = ATTRIBUTE;
            attributeService.editAttribute = () => of(attr);
            action$ = cold('a', {
                a: { type: '[Metamodel] Edit Attribute' },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Attribute Success',
                    attribute: attr,
                },
            });

            expect(effects.editAttribute$).toBeObservable(expected$);
        });

        it('should not edit attributes when payload is wrong', () => {
            attributeService.editAttribute = () =>
                throwError(() => {
                    const error = new Error('An Error happened');
                    return error;
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Edit Attribute' },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Attribute Fail',
                },
            });

            expect(effects.editAttribute$).toBeObservable(expected$);
        });
    });

    describe('editAttributeFail$ effect', () => {
        it('should toast a fail', () => {
            action$ = of({ type: '[Metamodel] Edit Attribute Fail' });
            effects.editAttributeFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });

    describe('deleteAttribute$ effect', () => {
        it('should delete attribute properly when payload is right', () => {
            const attr = ATTRIBUTE;
            attributeService.deleteAttribute = () => of({ message: 'deleted' });
            action$ = cold('a', {
                a: { type: '[Metamodel] Delete Attribute', attribute: attr },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Delete Attribute Success',
                    attribute: attr,
                },
            });

            expect(effects.deleteAttribute$).toBeObservable(expected$);
        });

        it('should not delete attribute when payload is wrong', () => {
            const attr = ATTRIBUTE;
            attributeService.deleteAttribute = () =>
                throwError(() => {
                    const error = new Error('An Error happened');
                    return error;
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Delete Attribute', attribute: attr },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Delete Attribute Fail',
                },
            });

            expect(effects.deleteAttribute$).toBeObservable(expected$);
        });
    });

    describe('deleteAttributeSuccess$ effect', () => {
        it('should toast a success', () => {
            action$ = of({ type: '[Metamodel] Delete Attribute Success' });
            effects.deleteAttributeSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });
    });

    describe('deleteAttributeFail$ effect', () => {
        it('should toast a fail', () => {
            action$ = of({ type: '[Metamodel] Delete Attribute Fail' });
            effects.deleteAttributeFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });
});
