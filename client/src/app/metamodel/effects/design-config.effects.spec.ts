/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Action } from '@ngrx/store';
import { cold } from 'jasmine-marbles';

import * as instanceSelector from '../selectors/instance.selector';
import { DesignConfigEffects } from './design-config.effects';
import { DesignConfigService } from '../services/design-config.service';
import { DESIGN_CONFIG } from 'src/test-data';

describe('[Metamodel][Effects] DesignConfigEffects', () => {
    let effects: DesignConfigEffects = null;
    let designConfigService: DesignConfigService = null;
    let action$ = new Observable<Action>();
    const initialState = {
        selectors: [
            {
                selector: instanceSelector.selectInstanceNameByRoute,
                value: 'vimuser',
            },
        ],
    };
    const toastr = { success: jest.fn(), error: jest.fn() };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                DesignConfigEffects,
                {
                    provide: DesignConfigService,
                    useValue: {},
                },
                {
                    provide: ToastrService,
                    useValue: toastr,
                },
                provideMockActions(() => action$),
                provideMockStore(initialState),
            ],
        }).compileComponents();

        effects = TestBed.inject(DesignConfigEffects);
        designConfigService = TestBed.inject(DesignConfigService);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should be truthy', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadDesignConfig$ effect', () => {
        it('should load config properly with right payload', () => {
            const design = DESIGN_CONFIG;
            designConfigService.retrieveDesignConfig = () => of(design);
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Design Config' },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Load Design Config Success',
                    designConfig: design,
                },
            });
            expect(effects.loadDesignConfig$).toBeObservable(expected$);
        });

        it('should not load config with wrong payload', () => {
            designConfigService.retrieveDesignConfig = () =>
                throwError(() => {
                    throw new Error('An error');
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Design Config' },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Load Design Config Fail',
                },
            });
            expect(effects.loadDesignConfig$).toBeObservable(expected$);
        });
    });

    describe('addDesignConfig$ toasting', () => {
        it('success', () => {
            action$ = of({ type: '[Metamodel] Add Design Config Success' });
            effects.addDesignConfigSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Add Design Config Fail' });
            effects.addDesignConfigFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });

    describe('addDesignConfig$ effect', () => {
        it('should add config properly with right payload', () => {
            const design = DESIGN_CONFIG;
            designConfigService.addDesignConfig = () => of(design);
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Design Config',
                    designConfig: design,
                },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Design Config Success',
                    designConfig: design,
                },
            });
            expect(effects.addDesignConfig$).toBeObservable(expected$);
        });

        it('should not add config with wrong payload', () => {
            designConfigService.addDesignConfig = () =>
                throwError(() => {
                    throw new Error('An error');
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Add Design Config' },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Design Config Fail',
                },
            });
            expect(effects.addDesignConfig$).toBeObservable(expected$);
        });
    });

    describe('editDesignConfig$ toasting', () => {
        it('success', () => {
            action$ = of({ type: '[Metamodel] Edit Design Config Success' });
            effects.editDesignConfigSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Edit Design Config Fail' });
            effects.editDesignConfigFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });

    describe('editDesignConfig$ effect', () => {
        it('should edit config properly with right payload', () => {
            const design = DESIGN_CONFIG;
            designConfigService.editDesignConfig = () => of(design);
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Design Config',
                    designConfig: design,
                },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Design Config Success',
                    designConfig: design,
                },
            });
            expect(effects.editDesignConfig$).toBeObservable(expected$);
        });

        it('should not edit config with wrong payload', () => {
            designConfigService.editDesignConfig = () =>
                throwError(() => {
                    throw new Error('An error');
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Edit Design Config' },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Design Config Fail',
                },
            });
            expect(effects.editDesignConfig$).toBeObservable(expected$);
        });
    });
});
