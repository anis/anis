/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as outputFamilyActions from '../actions/output-family.actions';
import * as outputCategoryActions from '../actions/output-category.actions';
import { OutputCategoryService } from '../services/output-category.service';
import * as instanceSelector from '../selectors/instance.selector';
import * as datasetSelector from '../selectors/dataset.selector';

@Injectable()
export class OutputCategoryEffects {
    /**
     * Calls action to retrieve output categories list.
     */
    loadOutputCategories$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(outputCategoryActions.loadOutputCategoryList),
            concatLatestFrom(() => [
                this.store.select(instanceSelector.selectInstanceNameByRoute),
                this.store.select(datasetSelector.selectDatasetNameByRoute)
            ]),
            mergeMap(([action, instanceName, datasetName]) => this.outputCategoryService.retrieveOutputCategoryList(instanceName, datasetName, action.outputFamilyId)
                .pipe(
                    map(outputCategories => outputCategoryActions.loadOutputCategoryListSuccess({ outputCategories })),
                    catchError(() => of(outputCategoryActions.loadOutputCategoryListFail()))
                )
            )
        )
    );

    /**
     * Calls action to add an output category.
     */
    addOutputCategory$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(outputCategoryActions.addOutputCategory),
            concatLatestFrom(() => [
                this.store.select(instanceSelector.selectInstanceNameByRoute),
                this.store.select(datasetSelector.selectDatasetNameByRoute)
            ]),
            mergeMap(([action, instanceName, datasetName]) => this.outputCategoryService.addOutputCategory(instanceName, datasetName, action.outputFamilyId, action.outputCategory)
                .pipe(
                    map(outputCategory => outputCategoryActions.addOutputCategorySuccess({ outputCategory })),
                    catchError(() => of(outputCategoryActions.addOutputCategoryFail()))
                )
            )
        )
    );

    /**
     * Displays add output category success notification.
     */
    addOutputCategorySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(outputCategoryActions.addOutputCategorySuccess),
            map(() => {
                this.toastr.success('Output category successfully added', 'The new output category was added into the database');
                return outputFamilyActions.loadOutputFamilyList();
            })
        )
    );

    /**
     * Displays add output category fail notification.
     */
    addOutputCategoryFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(outputCategoryActions.addOutputCategoryFail),
            tap(() => this.toastr.error('Failure to add output category', 'The new output category could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify an output category.
     */
    editOutputCategory$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(outputCategoryActions.editOutputCategory),
            concatLatestFrom(() => [
                this.store.select(instanceSelector.selectInstanceNameByRoute),
                this.store.select(datasetSelector.selectDatasetNameByRoute)
            ]),
            mergeMap(([action, instanceName, datasetName]) => this.outputCategoryService.editOutputCategory(instanceName, datasetName, action.outputFamilyId, action.outputCategory)
                .pipe(
                    map(outputCategory => outputCategoryActions.editOutputCategorySuccess({ outputCategory })),
                    catchError(() => of(outputCategoryActions.editOutputCategoryFail()))
                )
            )
        )
    );

    /**
     * Displays modify output category success notification.
     */
    editOutputCategorySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(outputCategoryActions.editOutputCategorySuccess),
            map(() => {
                this.toastr.success('Output category successfully edited', 'The existing output category has been edited into the database');
                return outputFamilyActions.loadOutputFamilyList();
            })
        )
    );

    /**
     * Displays modify output category fail notification.
     */
    editOutputCategoryFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(outputCategoryActions.editOutputCategoryFail),
            tap(() => this.toastr.error('Failure to edit output category', 'The existing output category could not be edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to remove an output category.
     */
    deleteOutputCategory$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(outputCategoryActions.deleteOutputCategory),
            concatLatestFrom(() => [
                this.store.select(instanceSelector.selectInstanceNameByRoute),
                this.store.select(datasetSelector.selectDatasetNameByRoute)
            ]),
            mergeMap(([action, instanceName, datasetName]) => this.outputCategoryService.deleteOutputCategory(instanceName, datasetName, action.outputFamilyId, action.outputCategory.id)
                .pipe(
                    map(() => outputCategoryActions.deleteOutputCategorySuccess({ outputCategory: action.outputCategory })),
                    catchError(() => of(outputCategoryActions.deleteOutputCategoryFail()))
                )
            )
        )
    );

    /**
     * Displays remove output category success notification.
     */
    deleteOutputCategorySuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(outputCategoryActions.deleteOutputCategorySuccess),
            map(() => {
                this.toastr.success('Output category successfully deleted', 'The existing output category has been deleted');
                return outputFamilyActions.loadOutputFamilyList();
            })
        )
    );

    /**
     * Displays remove output category fail notification.
     */
    deleteOutputCategoryFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(outputCategoryActions.deleteOutputCategoryFail),
            tap(() => this.toastr.error('Failure to delete output category', 'The existing output category could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private outputCategoryService: OutputCategoryService,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
