/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InstanceEffects } from './instance.effects';
import { DesignConfigEffects } from './design-config.effects';
import { InstanceGroupEffects } from './instance-group.effects';
import { DatabaseEffects } from './database.effects';
import { DatasetFamilyEffects } from './dataset-family.effects';
import { MenuItemEffects } from './menu-item.effects';
import { MenuFamilyItemEffects } from './menu-family-item.effects';
import { DatasetEffects } from './dataset.effects';
import { DatasetGroupEffects } from './dataset-group.effects';
import { CriteriaFamilyEffects } from './criteria-family.effects';
import { OutputFamilyEffects } from './output-family.effects';
import { OutputCategoryEffects } from './output-category.effects';
import { AttributeEffects } from './attribute.effects';
import { ImageEffects } from './image.effects';
import { FileEffects } from './file.effects';
import { ConeSearchConfigEffects } from './cone-search-config.effects';
import { DetailConfigEffects } from './detail-config.effects';
import { AliasConfigEffects } from './alias-config.effects';

export const metamodelEffects = [
    InstanceEffects,
    DesignConfigEffects,
    InstanceGroupEffects,
    DatabaseEffects,
    DatasetFamilyEffects,
    MenuItemEffects,
    MenuFamilyItemEffects,
    DatasetEffects,
    DatasetGroupEffects,
    CriteriaFamilyEffects,
    OutputFamilyEffects,
    OutputCategoryEffects,
    AttributeEffects,
    ImageEffects,
    FileEffects,
    ConeSearchConfigEffects,
    DetailConfigEffects,
    AliasConfigEffects
];
