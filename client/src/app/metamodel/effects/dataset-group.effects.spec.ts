/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Action } from '@ngrx/store';
import { cold } from 'jasmine-marbles';

import * as instanceSelector from '../selectors/instance.selector';
import * as datasetSelector from '../selectors/dataset.selector';
import { DatasetGroupEffects } from './dataset-group.effects';
import { DatasetGroupService } from '../services/dataset-group.service';
import { DATASET_GROUP } from 'src/test-data';

describe('[Metamodel][Effects] DatasetGroupEffects', () => {
    let effects: DatasetGroupEffects = null;
    let groupService: DatasetGroupService = null;
    let action$ = new Observable<Action>();
    const initialState = {
        selectors: [
            {
                selector: instanceSelector.selectInstanceNameByRoute,
                value: 'vimuser',
            },
            {
                selector: datasetSelector.selectDatasetNameByRoute,
                value: 'coconut',
            },
        ],
    };
    const toastr = { success: jest.fn(), error: jest.fn() };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                DatasetGroupEffects,
                {
                    provide: DatasetGroupService,
                    useValue: {},
                },
                {
                    provide: ToastrService,
                    useValue: toastr,
                },
                provideMockActions(() => action$),
                provideMockStore(initialState),
            ],
        }).compileComponents();

        effects = TestBed.inject(DatasetGroupEffects);
        groupService = TestBed.inject(DatasetGroupService);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should be truthy', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadDatasetGroups$ effect', () => {
        it('should load properly if payload is right', () => {
            const groups = [DATASET_GROUP, DATASET_GROUP];
            groupService.retrieveDatasetGroupList = () => of(groups);
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Dataset Group List' },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Load Dataset Group List Success',
                    datasetGroups: groups,
                },
            });
            expect(effects.loadDatasetGroups$).toBeObservable(expected);
        });

        it('should not load if payload is wrong', () => {
            groupService.retrieveDatasetGroupList = () =>
                throwError(() => {
                    const error = new Error('An Error happened');
                    throw error;
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Dataset Group List' },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Load Dataset Group List Fail',
                },
            });
            expect(effects.loadDatasetGroups$).toBeObservable(expected);
        });
    });

    describe('addDatasetGroup$ effect', () => {
        it('should add properly if payload is right', () => {
            const group = DATASET_GROUP;
            groupService.addDatasetGroup = () => of(group);
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Dataset Group',
                    datasetGroup: group,
                },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Add Dataset Group Success',
                    datasetGroup: group,
                },
            });
            expect(effects.addDatasetGroup$).toBeObservable(expected);
        });

        it('should not add if payload is wrong', () => {
            const group = DATASET_GROUP;
            groupService.addDatasetGroup = () =>
                throwError(() => {
                    throw new Error('An error');
                });
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Dataset Group',
                    datasetGroup: group,
                },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Add Dataset Group Fail',
                },
            });
            expect(effects.addDatasetGroup$).toBeObservable(expected);
        });
    });

    describe('addDatasetGroup$ toasting', () => {
        it('success', () => {
            action$ = of({ type: '[Metamodel] Add Dataset Group Success' });
            effects.addDatasetGroupSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Add Dataset Group Fail' });
            effects.addDatasetGroupFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });

    describe('editDatasetGroup$ effect', () => {
        it('should edit properly if payload is right', () => {
            const group = DATASET_GROUP;
            groupService.editDatasetGroup = () => of(group);
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Dataset Group',
                    datasetGroup: group,
                },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Edit Dataset Group Success',
                    datasetGroup: group,
                },
            });
            expect(effects.editDatasetGroup$).toBeObservable(expected);
        });

        it('should not edit if payload is wrong', () => {
            const group = DATASET_GROUP;
            groupService.editDatasetGroup = () =>
                throwError(() => {
                    throw new Error('An error');
                });
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Dataset Group',
                    datasetGroup: group,
                },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Edit Dataset Group Fail',
                },
            });
            expect(effects.editDatasetGroup$).toBeObservable(expected);
        });
    });

    describe('editDatasetGroup$ toasting', () => {
        it('success', () => {
            action$ = of({ type: '[Metamodel] Edit Dataset Group Success' });
            effects.editDatasetGroupSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Edit Dataset Group Fail' });
            effects.editDatasetGroupFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });

    describe('deleteDatasetGroup$ effect', () => {
        it('should delete properly if payload is right', () => {
            const group = DATASET_GROUP;
            groupService.deleteDatasetGroup = () => of(group);
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Delete Dataset Group',
                    datasetGroup: group,
                },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Delete Dataset Group Success',
                    datasetGroup: group,
                },
            });
            expect(effects.deleteDatasetGroup$).toBeObservable(expected);
        });

        it('should not delete if payload is wrong', () => {
            const group = DATASET_GROUP;
            groupService.deleteDatasetGroup = () =>
                throwError(() => {
                    throw new Error('An error');
                });
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Delete Dataset Group',
                    datasetGroup: group,
                },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Delete Dataset Group Fail',
                },
            });
            expect(effects.deleteDatasetGroup$).toBeObservable(expected);
        });
    });

    describe('deleteDatasetGroup$ toasting', () => {
        it('success', () => {
            action$ = of({ type: '[Metamodel] Delete Dataset Group Success' });
            effects.deleteDatasetGroupSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Delete Dataset Group Fail' });
            effects.deleteDatasetGroupFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });
});
