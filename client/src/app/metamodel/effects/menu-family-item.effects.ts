/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as menuFamilyItemActions from '../actions/menu-family-item.actions';
import { MenuFamilyItemService } from '../services/menu-family-item.service';
import * as instanceSelector from '../selectors/instance.selector';
import * as menuItemSelector from '../selectors/menu-item.selector';

@Injectable()
export class MenuFamilyItemEffects {
    /**
     * Calls action to retrieve menu family item list.
     */
    loadMenuFamilyItems$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(menuFamilyItemActions.loadMenuFamilyItemList),
            concatLatestFrom(() => [
                this.store.select(instanceSelector.selectInstanceNameByRoute),
                this.store.select(menuItemSelector.selectMenuItemIdByRoute)
            ]),
            mergeMap(([, instanceName, menuItemId]) => this.menuFamilyItemService.retrieveMenuFamilyItemList(instanceName, menuItemId)
                .pipe(
                    map(menuFamilyItemList => menuFamilyItemActions.loadMenuFamilyItemListSuccess({ menuFamilyItemList })),
                    catchError(() => of(menuFamilyItemActions.loadMenuFamilyItemListFail()))
                )
            )
        )
    );

    /**
     * Calls action to add a menu item.
     */
    addMenuFamilyItem$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(menuFamilyItemActions.addMenuFamilyItem),
            concatLatestFrom(() => [
                this.store.select(instanceSelector.selectInstanceNameByRoute),
                this.store.select(menuItemSelector.selectMenuItemByRouteId)
            ]),
            mergeMap(([action, instanceName, menuItem]) => this.menuFamilyItemService.addMenuFamilyItem(instanceName, menuItem.id, action.menuFamilyItem)
                .pipe(
                    map(menuFamilyItem => menuFamilyItemActions.addMenuFamilyItemSuccess({ menuFamilyItem })),
                    catchError(() => of(menuFamilyItemActions.addMenuFamilyItemFail()))
                )
            )
        )
    );

    /**
     * Displays add menu item success notification.
     */
    addMenuFamilyItemSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(menuFamilyItemActions.addMenuFamilyItemSuccess),
            concatLatestFrom(() => [
                this.store.select(instanceSelector.selectInstanceNameByRoute),
                this.store.select(menuItemSelector.selectMenuItemByRouteId)
            ]),
            tap(([, instanceName, menuItem]) => {
                this.router.navigate([`/admin/instance/configure-instance/${instanceName}/menu/menu-family-item-list/${menuItem.id}`]);
                this.toastr.success('Menu item successfully added', 'The new menu item was added into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays add menu item error notification.
     */
    addMenuFamilyItemFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(menuFamilyItemActions.addMenuFamilyItemFail),
            tap(() => this.toastr.error('Failure to add menu item', 'The new  menu item could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify a menu item.
     */
    editMenuFamilyItem$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(menuFamilyItemActions.editMenuFamilyItem),
            concatLatestFrom(() => [
                this.store.select(instanceSelector.selectInstanceNameByRoute),
                this.store.select(menuItemSelector.selectMenuItemByRouteId)
            ]),
            mergeMap(([action, instanceName, menuItem]) => this.menuFamilyItemService.editMenuFamilyItem(instanceName, menuItem.id, action.menuFamilyItem)
                .pipe(
                    map(menuFamilyItem => menuFamilyItemActions.editMenuFamilyItemSuccess({ menuFamilyItem })),
                    catchError(() => of(menuFamilyItemActions.editMenuFamilyItemFail()))
                )
            )
        )
    );

    /**
     * Displays edit menu item success notification.
     */
    editMenuFamilyItemSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(menuFamilyItemActions.editMenuFamilyItemSuccess),
            concatLatestFrom(() => [
                this.store.select(instanceSelector.selectInstanceNameByRoute),
                this.store.select(menuItemSelector.selectMenuItemByRouteId)
            ]),
            tap(() => this.toastr.success('Menu item successfully edited', 'The existing menu item has been edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays edit menu item error notification.
     */
    editMenuFamilyItemFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(menuFamilyItemActions.editMenuFamilyItemFail),
            tap(() => this.toastr.error('Failure to edit menu item', 'The existing menu item could not be edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to remove a menu item.
     */
    deleteMenuFamilyItem$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(menuFamilyItemActions.deleteMenuFamilyItem),
            concatLatestFrom(() => [
                this.store.select(instanceSelector.selectInstanceNameByRoute),
                this.store.select(menuItemSelector.selectMenuItemByRouteId)
            ]),
            mergeMap(([action, instanceName, menuItem]) => this.menuFamilyItemService.deleteMenuFamilyItem(instanceName, menuItem.id, action.menuFamilyItem.id)
                .pipe(
                    map(() => menuFamilyItemActions.deleteMenuFamilyItemSuccess({ menuFamilyItem: action.menuFamilyItem })),
                    catchError(() => of(menuFamilyItemActions.deleteMenuFamilyItemFail()))
                )
            )
        )
    );

    /**
     * Displays delete menu item success notification.
     */
    deleteMenuFamilyItemSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(menuFamilyItemActions.deleteMenuFamilyItemSuccess),
            tap(() => this.toastr.success('Menu item successfully deleted', 'The existing menu item has been deleted'))
        ), { dispatch: false }
    );

    /**
     * Displays delete menu item error notification.
     */
    deleteMenuFamilyItemFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(menuFamilyItemActions.deleteMenuFamilyItemFail),
            tap(() => this.toastr.error('Failure to delete menu item', 'The existing menu item could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private menuFamilyItemService: MenuFamilyItemService,
        private router: Router,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
