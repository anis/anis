/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as designConfigActions from '../actions/design-config.actions';
import { DesignConfigService } from '../services/design-config.service';
import * as instanceSelector from '../selectors/instance.selector';

@Injectable()
export class DesignConfigEffects {
    /**
     * Calls action to retrieve instance design configuration
     */
    loadDesignConfig$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(designConfigActions.loadDesignConfig),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            mergeMap(([, instanceName]) => this.designConfigService.retrieveDesignConfig(instanceName)
                .pipe(
                    map(designConfig => designConfigActions.loadDesignConfigSuccess({ designConfig })),
                    catchError(() => of(designConfigActions.loadDesignConfigFail()))
                )
            )
        )
    );

    /**
     * Calls action to add a design config
     */
    addDesignConfig$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(designConfigActions.addDesignConfig),
            mergeMap(action => this.designConfigService.addDesignConfig(action.instanceName, action.designConfig)
                .pipe(
                    map(designConfig => designConfigActions.addDesignConfigSuccess({ designConfig })),
                    catchError(() => of(designConfigActions.addDesignConfigFail()))
                )
            )
        )
    );

    /**
     * Displays add design configuration success notification.
     */
    addDesignConfigSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(designConfigActions.addDesignConfigSuccess),
            tap(() => this.toastr.success('Design config successfully added', 'The new design config was added into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays add design configuration error notification.
     */
    addDesignConfigFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(designConfigActions.addDesignConfigFail),
            tap(() => this.toastr.error('Failure to add design config', 'The new design config could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify an design configuration
     */
    editDesignConfig$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(designConfigActions.editDesignConfig),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            mergeMap(([action, instanceName]) => this.designConfigService.editDesignConfig(instanceName, action.designConfig)
                .pipe(
                    map(designConfig => designConfigActions.editDesignConfigSuccess({ designConfig })),
                    catchError(() => of(designConfigActions.editDesignConfigFail()))
                )
            )
        )
    );

    /**
     * Displays edit design configuration success notification.
     */
    editDesignConfigSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(designConfigActions.editDesignConfigSuccess),
            tap(() => this.toastr.success('Design configuration successfully edited', 'The existing design configuration has been edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays edit design configuration error notification.
     */
    editDesignConfigFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(designConfigActions.editDesignConfigFail),
            tap(() => this.toastr.error('Failure to edit design configuration', 'The existing design configuration could not be edited into the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private designConfigService: DesignConfigService,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
