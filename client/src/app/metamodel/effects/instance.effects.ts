/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as instanceActions from '../actions/instance.actions';
import * as designConfigActions from '../actions/design-config.actions';
import { InstanceService } from '../services/instance.service';
import { defaultDesignConfig } from '../models';

/**
 * @class
 * @classdesc Instance effects.
 */
@Injectable()
export class InstanceEffects {
    /**
     * Calls action to retrieve instance list.
     */
    loadInstances$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceActions.loadInstanceList),
            mergeMap(() => this.instanceService.retrieveInstanceList()
                .pipe(
                    map(instances => instanceActions.loadInstanceListSuccess({ instances })),
                    catchError(() => of(instanceActions.loadInstanceListFail()))
                )
            )
        )
    );

    /**
     * Calls action to import a full configuraton of instance.
     */
    importInstance$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceActions.importInstance),
            mergeMap(action => this.instanceService.importInstance(action.fullInstance)
                .pipe(
                    map(instance => instanceActions.importInstanceSuccess({ instance })),
                    catchError(() => of(instanceActions.importInstanceFail()))
                )
            )
        )
    );

    /**
     * Displays import instance success notification.
     */
    importInstanceSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceActions.importInstanceSuccess),
            tap(() => {
                this.router.navigate(['/admin/instance/instance-list']);
                this.toastr.success('Instance successfully imported', 'The new import instance was added into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays import instance fail notification.
     */
    importInstanceFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceActions.importInstanceFail),
            tap(() => this.toastr.error('Failure to import instance', 'The new import instance could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to add an instance.
     */
    addInstance$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceActions.addInstance),
            mergeMap(action => this.instanceService.addInstance(action.instance)
                .pipe(
                    map(instance => instanceActions.addInstanceSuccess({ instance })),
                    catchError(() => of(instanceActions.addInstanceFail()))
                )
            )
        )
    );

    /**
     * Displays add instance success notification.
     */
    addInstanceSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceActions.addInstanceSuccess),
            map(action => {
                this.router.navigate(['/admin/instance/instance-list']);
                this.toastr.success('Instance successfully added', 'The new instance was added into the database')
                return designConfigActions.addDesignConfig({ instanceName: action.instance.name, designConfig: defaultDesignConfig });
            })
        )
    );

    /**
     * Displays add instance fail notification.
     */
    addInstanceFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceActions.addInstanceFail),
            tap(() => this.toastr.error('Failure to add instance', 'The new instance could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify an instance.
     */
    editInstance$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceActions.editInstance),
            mergeMap(action => this.instanceService.editInstance(action.instance)
                .pipe(
                    map(instance => instanceActions.editInstanceSuccess({ instance })),
                    catchError(() => of(instanceActions.editInstanceFail()))
                )
            )
        )
    );

    /**
     * Displays edit instance success notification.
     */
    editInstanceSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceActions.editInstanceSuccess),
            tap(() => this.toastr.success('Instance successfully edited', 'The existing instance has been edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays edit instance fail notification.
     */
    editInstanceFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceActions.editInstanceFail),
            tap(() => this.toastr.error('Failure to edit instance', 'The existing instance could not be edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to remove an instance.
     */
    deleteInstance$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceActions.deleteInstance),
            mergeMap(action => this.instanceService.deleteInstance(action.instance.name)
                .pipe(
                    map(() => instanceActions.deleteInstanceSuccess({ instance: action.instance })),
                    catchError(() => of(instanceActions.deleteInstanceFail()))
                )
            )
        )
    );

    /**
     * Displays remove instance success notification.
     */
    deleteInstanceSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceActions.deleteInstanceSuccess),
            tap(() => this.toastr.success('Instance successfully deleted', 'The existing instance has been deleted'))
        ), { dispatch: false }
    );

    /**
     * Displays remove instance fail notification.
     */
    deleteInstanceFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(instanceActions.deleteInstanceFail),
            tap(() => this.toastr.error('Failure to delete instance', 'The existing instance could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private instanceService: InstanceService,
        private router: Router,
        private toastr: ToastrService
    ) {}
}
