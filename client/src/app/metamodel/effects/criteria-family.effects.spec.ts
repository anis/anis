/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Action } from '@ngrx/store';
import { cold } from 'jasmine-marbles';

import * as instanceSelector from '../selectors/instance.selector';
import * as datasetSelector from '../selectors/dataset.selector';
import { CriteriaFamilyEffects } from './criteria-family.effects';
import { CriteriaFamilyService } from '../services/criteria-family.service';
import { CRITERIA_FAMILY, CRITERIA_FAMILY_LIST } from 'src/test-data';

describe('[Metamodel][Effects] CriteriaFamilyEffects', () => {
    let effects: CriteriaFamilyEffects = null;
    let coneSearchConfigService: CriteriaFamilyService = null;
    let action$ = new Observable<Action>();
    const initialState = {
        selectors: [
            {
                selector: instanceSelector.selectInstanceNameByRoute,
                value: 'vimuser',
            },
            {
                selector: datasetSelector.selectDatasetNameByRoute,
                value: 'coconut',
            },
        ],
    };
    const toastr = { success: jest.fn(), error: jest.fn() };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                CriteriaFamilyEffects,
                {
                    provide: CriteriaFamilyService,
                    useValue: {},
                },
                {
                    provide: ToastrService,
                    useValue: toastr,
                },
                provideMockActions(() => action$),
                provideMockStore(initialState),
            ],
        }).compileComponents();

        effects = TestBed.inject(CriteriaFamilyEffects);
        coneSearchConfigService = TestBed.inject(CriteriaFamilyService);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should be truthy', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadCriteriaFamilies$ effect', () => {
        it('should load properly if payload is right', () => {
            const criteria = CRITERIA_FAMILY_LIST;
            coneSearchConfigService.retrieveCriteriaFamilyList = () =>
                of(criteria);
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Criteria Family List' },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Load Criteria Family List Success',
                    criteriaFamilies: criteria,
                },
            });

            expect(effects.loadCriteriaFamilies$).toBeObservable(expected);
        });

        it('should not load properly if payload is wrong', () => {
            coneSearchConfigService.retrieveCriteriaFamilyList = () =>
                throwError(() => {
                    const error = new Error('An error happened!');
                    return error;
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Criteria Family List' },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Load Criteria Family List Fail',
                },
            });

            expect(effects.loadCriteriaFamilies$).toBeObservable(expected);
        });
    });

    describe('addCriteriaFamily$ effect', () => {
        it('should not add properly if payload is wrong', () => {
            coneSearchConfigService.addCriteriaFamily = () =>
                throwError(() => {
                    const error = new Error('An error happened!');
                    return error;
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Add Criteria Family' },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Add Criteria Family Fail',
                },
            });

            expect(effects.addCriteriaFamily$).toBeObservable(expected);
        });

        it('should add properly if payload is right', () => {
            const criteria = CRITERIA_FAMILY;
            coneSearchConfigService.addCriteriaFamily = () => of(criteria);
            action$ = cold('a', {
                a: { type: '[Metamodel] Add Criteria Family' },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Add Criteria Family Success',
                    criteriaFamily: criteria,
                },
            });

            expect(effects.addCriteriaFamily$).toBeObservable(expected);
        });
    });

    describe('addCriteriaFamily$ toasting', () => {
        it('success', () => {
            action$ = of({ type: '[Metamodel] Add Criteria Family Success'});
            effects.addCriteriaFamilySuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Add Criteria Family Fail'});
            effects.addCriteriaFamilyFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        })
    });

    describe('editCriteriaFamily$ effect', () => {
        it('should not edit properly if payload is wrong', () => {
            coneSearchConfigService.editCriteriaFamily = () =>
                throwError(() => {
                    const error = new Error('An error happened!');
                    return error;
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Edit Criteria Family' },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Edit Criteria Family Fail',
                },
            });

            expect(effects.editCriteriaFamily$).toBeObservable(expected);
        });

        it('should edit properly if payload is right', () => {
            const criteria = CRITERIA_FAMILY;
            coneSearchConfigService.editCriteriaFamily = () => of(criteria);
            action$ = cold('a', {
                a: { type: '[Metamodel] Edit Criteria Family' },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Edit Criteria Family Success',
                    criteriaFamily: criteria,
                },
            });

            expect(effects.editCriteriaFamily$).toBeObservable(expected);
        });
    });

    describe('editCriteriaFamily$ toasting', () => {
        it('success', () => {
            action$ = of({ type: '[Metamodel] Edit Criteria Family Success'});
            effects.editCriteriaFamilySuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Edit Criteria Family Fail'});
            effects.editCriteriaFamilyFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        })
    });

    describe('deleteCriteriaFamily$ effect', () => {
        it('should not delete properly if payload is wrong', () => {
            const criteria = CRITERIA_FAMILY;
            coneSearchConfigService.deleteCriteriaFamily = () =>
                throwError(() => {
                    const error = new Error('An error happened!');
                    return error;
                });
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Delete Criteria Family',
                    criteriaFamily: criteria,
                },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Delete Criteria Family Fail',
                },
            });

            expect(effects.deleteCriteriaFamily$).toBeObservable(expected);
        });

        it('should delete properly if payload is right', () => {
            const criteria = CRITERIA_FAMILY;
            coneSearchConfigService.deleteCriteriaFamily = () => of(criteria);
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Delete Criteria Family',
                    criteriaFamily: criteria,
                },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Delete Criteria Family Success',
                    criteriaFamily: criteria,
                },
            });

            expect(effects.deleteCriteriaFamily$).toBeObservable(expected);
        });
    });

    describe('deleteCriteriaFamily$ toasting', () => {
        it('success', () => {
            action$ = of({ type: '[Metamodel] Delete Criteria Family Success'});
            effects.deleteCriteriaFamilySuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Delete Criteria Family Fail'});
            effects.deleteCriteriaFamilyFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        })
    });
});
