/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Action } from '@ngrx/store';
import { cold } from 'jasmine-marbles';

import * as instanceSelector from '../selectors/instance.selector';
import { DatasetEffects } from './dataset.effects';
import { DatasetService } from '../services/dataset.service';
import { DATASET, DATASET_LIST } from 'src/test-data';
import { Router } from '@angular/router';

describe('[Metamodel][Effects] DatasetEffects', () => {
    let effects: DatasetEffects = null;
    let datasetService: DatasetService = null;
    let action$ = new Observable<Action>();
    const initialState = {
        selectors: [
            {
                selector: instanceSelector.selectInstanceNameByRoute,
                value: 'vimuser',
            },
        ],
    };
    const toastr = { success: jest.fn(), error: jest.fn() };
    const router = { navigate: jest.fn() };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                DatasetEffects,
                {
                    provide: DatasetService,
                    useValue: {},
                },
                {
                    provide: Router,
                    useValue: router,
                },
                {
                    provide: ToastrService,
                    useValue: toastr,
                },
                provideMockActions(() => action$),
                provideMockStore(initialState),
            ],
        }).compileComponents();

        effects = TestBed.inject(DatasetEffects);
        datasetService = TestBed.inject(DatasetService);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should be truthy', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadDatasets$ effect', () => {
        it('should load properly with right payload', () => {
            const data = DATASET_LIST;
            datasetService.retrieveDatasetList = () => of(data);
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Dataset List' },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Load Dataset List Success',
                    datasets: data,
                },
            });
            expect(effects.loadDatasets$).toBeObservable(expected$);
        });

        it('should not load properly with wrong payload', () => {
            datasetService.retrieveDatasetList = () =>
                throwError(() => {
                    throw new Error('An error');
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Dataset List' },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Load Dataset List Fail',
                },
            });
            expect(effects.loadDatasets$).toBeObservable(expected$);
        });
    });

    describe('importDataset$ effect', () => {
        it('should import properly with right payload', () => {
            const data = DATASET;
            datasetService.importDataset = () => of(data);
            action$ = cold('a', {
                a: { type: '[Metamodel] Import Dataset', dataset: data },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Import Dataset Success',
                    dataset: data,
                },
            });
            expect(effects.importDataset$).toBeObservable(expected$);
        });

        it('should not import properly with wrong payload', () => {
            const data = DATASET;
            datasetService.importDataset = () =>
                throwError(() => {
                    throw new Error('An error');
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Import Dataset', dataset: data },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Import Dataset Fail',
                },
            });
            expect(effects.importDataset$).toBeObservable(expected$);
        });
    });

    describe('importDataset$ toasting', () => {
        it('success and routing', () => {
            action$ = of({ type: '[Metamodel] Import Dataset Success' });
            effects.importDatasetSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
            expect(router.navigate).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Import Dataset Fail' });
            effects.importDatasetFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });

    describe('addDatasets$ effect', () => {
        it('should add properly with right payload', () => {
            const data = DATASET;
            datasetService.addDataset = () => of(data);
            action$ = cold('a', {
                a: { type: '[Metamodel] Add Dataset', dataset: data },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Dataset Success',
                    dataset: data,
                },
            });
            expect(effects.addDataset$).toBeObservable(expected$);
        });

        it('should not add properly with wrong payload', () => {
            const data = DATASET;
            datasetService.addDataset = () =>
                throwError(() => {
                    throw new Error('An error');
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Add Dataset', dataset: data },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Dataset Fail',
                },
            });
            expect(effects.addDataset$).toBeObservable(expected$);
        });
    });

    describe('addDataset$ toasting', () => {
        it('success', () => {
            action$ = of({ type: '[Metamodel] Add Dataset Success' });
            effects.addDatasetSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Add Dataset Fail' });
            effects.addDatasetFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });

    describe('editDatasets$ effect', () => {
        it('should edit properly with right payload', () => {
            const data = DATASET;
            datasetService.editDataset = () => of(data);
            action$ = cold('a', {
                a: { type: '[Metamodel] Edit Dataset', dataset: data },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Dataset Success',
                    dataset: data,
                },
            });
            expect(effects.editDataset$).toBeObservable(expected$);
        });

        it('should not edit properly with wrong payload', () => {
            const data = DATASET;
            datasetService.editDataset = () =>
                throwError(() => {
                    throw new Error('An error');
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Edit Dataset', dataset: data },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Dataset Fail',
                },
            });
            expect(effects.editDataset$).toBeObservable(expected$);
        });
    });

    describe('editDataset$ toasting', () => {
        it('success', () => {
            action$ = of({ type: '[Metamodel] Edit Dataset Success' });
            effects.editDatasetSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Edit Dataset Fail' });
            effects.editDatasetFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });

    describe('deleteDatasets$ effect', () => {
        it('should delete properly with right payload', () => {
            const data = DATASET;
            datasetService.deleteDataset = () => of(data);
            action$ = cold('a', {
                a: { type: '[Metamodel] Delete Dataset', dataset: data },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Delete Dataset Success',
                    dataset: data,
                },
            });
            expect(effects.deleteDataset$).toBeObservable(expected$);
        });

        it('should not delete properly with wrong payload', () => {
            const data = DATASET;
            datasetService.deleteDataset = () =>
                throwError(() => {
                    throw new Error('An error');
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Delete Dataset', dataset: data },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Delete Dataset Fail',
                },
            });
            expect(effects.deleteDataset$).toBeObservable(expected$);
        });
    });

    describe('deleteDataset$ toasting', () => {
        it('success', () => {
            action$ = of({ type: '[Metamodel] Delete Dataset Success' });
            effects.deleteDatasetSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Delete Dataset Fail' });
            effects.deleteDatasetFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });
});
