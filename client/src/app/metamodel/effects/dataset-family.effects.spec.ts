/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Action } from '@ngrx/store';
import { cold } from 'jasmine-marbles';

import * as instanceSelector from '../selectors/instance.selector';
import { DatasetFamilyEffects } from './dataset-family.effects';
import { DatasetFamilyService } from '../services/dataset-family.service';
import {
    DATASET_FAMILY,
    DATASET_FAMILY_LIST,
} from 'src/test-data';

describe('[Metamodel][Effects] ConeSearchConfigEffects', () => {
    let effects: DatasetFamilyEffects = null;
    let datasetFamilyService: DatasetFamilyService = null;
    let action$ = new Observable<Action>();
    const initialState = {
        selectors: [
            {
                selector: instanceSelector.selectInstanceNameByRoute,
                value: 'vimuser',
            },
        ],
    };
    const toastr = { success: jest.fn(), error: jest.fn() };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                DatasetFamilyEffects,
                {
                    provide: DatasetFamilyService,
                    useValue: {},
                },
                {
                    provide: ToastrService,
                    useValue: toastr,
                },
                provideMockActions(() => action$),
                provideMockStore(initialState),
            ],
        }).compileComponents();

        effects = TestBed.inject(DatasetFamilyEffects);
        datasetFamilyService = TestBed.inject(DatasetFamilyService);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should be truthy', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadDatasetFamilies$ effect', () => {
        it('should load properly if payload is right', () => {
            const datasetFamilies = DATASET_FAMILY_LIST;
            datasetFamilyService.retrieveDatasetFamilyList = () =>
                of(datasetFamilies);
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Dataset Family List' },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Load Dataset Family List Success',
                    datasetFamilies: DATASET_FAMILY_LIST,
                },
            });

            expect(effects.loadDatasetFamilies$).toBeObservable(expected);
        });

        it('should not load properly if payload is wrong', () => {
            datasetFamilyService.retrieveDatasetFamilyList = () =>
                throwError(() => {
                    const error = new Error('An error happened!');
                    return error;
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Dataset Family List' },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Load Dataset Family List Fail',
                },
            });

            expect(effects.loadDatasetFamilies$).toBeObservable(expected);
        });
    });

    describe('addDatasetFamily$ effect', () => {
        it('should not add properly if payload is wrong', () => {
            datasetFamilyService.addDatasetFamily = () =>
                throwError(() => {
                    const error = new Error('An error happened!');
                    return error;
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Add Dataset Family' },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Add Dataset Family Fail',
                },
            });

            expect(effects.addDatasetFamily$).toBeObservable(expected);
        });

        it('should add properly if payload is right', () => {
            const datasetFamily = DATASET_FAMILY;
            datasetFamilyService.addDatasetFamily = () => of(datasetFamily);
            action$ = cold('a', {
                a: { type: '[Metamodel] Add Dataset Family' },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Add Dataset Family Success',
                    datasetFamily: datasetFamily,
                },
            });

            expect(effects.addDatasetFamily$).toBeObservable(expected);
        });
    });

        describe('addDatasetFamily$ toasting', () => {
            it('success', () => {
                action$ = of({ type: '[Metamodel] Add Dataset Family Success' });
                effects.addDatasetFamilySuccess$.subscribe();
                expect(toastr.success).toHaveBeenCalled();
            });

            it('error', () => {
                action$ = of({ type: '[Metamodel] Add Dataset Family Fail' });
                effects.addDatasetFamilyFail$.subscribe();
                expect(toastr.error).toHaveBeenCalled();
            });
        });

    describe('editDatasetFamily$ effect', () => {
        it('should not edit properly if payload is wrong', () => {
            datasetFamilyService.editDatasetFamily = () =>
                throwError(() => {
                    const error = new Error('An error happened!');
                    return error;
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Edit Dataset Family' },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Edit Dataset Family Fail',
                },
            });

            expect(effects.editDatasetFamily$).toBeObservable(expected);
        });

        it('should edit properly if payload is right', () => {
            const datasetFamily = DATASET_FAMILY;
            datasetFamilyService.editDatasetFamily = () => of(datasetFamily);
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Dataset Family',
                    datasetFamily: datasetFamily,
                },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Edit Dataset Family Success',
                    datasetFamily: datasetFamily,
                },
            });

            expect(effects.editDatasetFamily$).toBeObservable(expected);
        });
    });

        describe('editDatasetFamily$ toasting', () => {
            it('success', () => {
                action$ = of({ type: '[Metamodel] Edit Dataset Family Success' });
                effects.editDatasetFamilySuccess$.subscribe();
                expect(toastr.success).toHaveBeenCalled();
            });

            it('error', () => {
                action$ = of({ type: '[Metamodel] Edit Dataset Family Fail' });
                effects.editDatasetFamilyFail$.subscribe();
                expect(toastr.error).toHaveBeenCalled();
            });
        });

    describe('deleteDatasetFamily$ effect', () => {
        it('should not delete properly if payload is wrong', () => {
            const datasetFamily = DATASET_FAMILY;
            datasetFamilyService.deleteDatasetFamily = () =>
                throwError(() => {
                    const error = new Error('An error happened!');
                    return error;
                });
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Delete Dataset Family',
                    datasetFamily: datasetFamily,
                },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Delete Dataset Family Fail',
                },
            });

            expect(effects.deleteDatasetFamily$).toBeObservable(expected);
        });

        it('should delete properly if payload is right', () => {
            const datasetFamily = DATASET_FAMILY;
            datasetFamilyService.deleteDatasetFamily = () =>
                of({ message: 'ok' });
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Delete Dataset Family',
                    datasetFamily: datasetFamily,
                },
            });

            const expected = cold('a', {
                a: {
                    type: '[Metamodel] Delete Dataset Family Success',
                    datasetFamily: datasetFamily,
                },
            });

            expect(effects.deleteDatasetFamily$).toBeObservable(expected);
        });
    });

        describe('deleteDatasetFamily$ toasting', () => {
            it('success', () => {
                action$ = of({
                    type: '[Metamodel] Delete Dataset Family Success',
                });
                effects.deleteDatasetFamilySuccess$.subscribe();
                expect(toastr.success).toHaveBeenCalled();
            });

            it('error', () => {
                action$ = of({ type: '[Metamodel] Delete Dataset Family Fail' });
                effects.deleteDatasetFamilyFail$.subscribe();
                expect(toastr.error).toHaveBeenCalled();
            });
        });
});
