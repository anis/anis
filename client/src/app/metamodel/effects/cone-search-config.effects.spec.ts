/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Action } from '@ngrx/store';
import { cold } from 'jasmine-marbles';

import * as instanceSelector from '../selectors/instance.selector';
import * as datasetSelector from '../selectors/dataset.selector';
import { ConeSearchConfigEffects } from './cone-search-config.effects';
import { ConeSearchConfigService } from '../services/cone-search-config.service';
import { CONE_SEARCH_CONFIG } from 'src/test-data';

describe('[Metamodel][Effects] ConeSearchConfigEffects', () => {
    let effects: ConeSearchConfigEffects = null;
    let coneSearchConfigService: ConeSearchConfigService = null;
    let action$ = new Observable<Action>();
    const initialState = {
        selectors: [
            {
                selector: instanceSelector.selectInstanceNameByRoute,
                value: 'vimuser',
            },
            {
                selector: datasetSelector.selectDatasetNameByRoute,
                value: 'coconut',
            },
        ],
    };
    const toastr = { success: jest.fn(), error: jest.fn() };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ConeSearchConfigEffects,
                {
                    provide: ConeSearchConfigService,
                    useValue: {},
                },
                {
                    provide: ToastrService,
                    useValue: toastr,
                },
                provideMockActions(() => action$),
                provideMockStore(initialState),
            ],
        }).compileComponents();

        effects = TestBed.inject(ConeSearchConfigEffects);
        coneSearchConfigService = TestBed.inject(ConeSearchConfigService);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should be truthy', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadConeSearchsConfig$ effect', () => {
        it('should load config if payload is right', () => {
            const coneSearchConfig = CONE_SEARCH_CONFIG;
            coneSearchConfigService.retrieveConeSearchConfig = () =>
                of(coneSearchConfig);
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Cone Search Config' },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Load Cone Search Config Success',
                    coneSearchConfig: CONE_SEARCH_CONFIG,
                },
            });

            expect(effects.loadConeSearchsConfig$).toBeObservable(expected$);
        });

        it('should not load config if payload is wrong', () => {
            coneSearchConfigService.retrieveConeSearchConfig = () =>
                throwError(() => {
                    const error = new Error('An Error happened');
                    return error;
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Cone Search Config' },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Load Cone Search Config Fail',
                },
            });

            expect(effects.loadConeSearchsConfig$).toBeObservable(expected$);
        });
    });

    describe('addConeSearchConfig$ effect', () => {
        it('should add config if payload is right', () => {
            const coneSearchConfig = CONE_SEARCH_CONFIG;
            coneSearchConfigService.addConeSearchConfig = () =>
                of(coneSearchConfig);
            action$ = cold('a', {
                a: { type: '[Metamodel] Add Cone Search Config' },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Cone Search Config Success',
                    coneSearchConfig: CONE_SEARCH_CONFIG,
                },
            });

            expect(effects.addConeSearchConfig$).toBeObservable(expected$);
        });

        it('should not add config if payload is wrong', () => {
            coneSearchConfigService.addConeSearchConfig = () =>
                throwError(() => {
                    const error = new Error('An Error happened');
                    return error;
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Add Cone Search Config' },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Cone Search Config Fail',
                },
            });

            expect(effects.addConeSearchConfig$).toBeObservable(expected$);
        });
    })

    describe('addConeSearchConfig$ toasting', () => {
        it('success', () => {
            action$ = of({ type: '[Metamodel] Add Cone Search Config Success' });

            effects.addConeSearchConfigSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Add Cone Search Config Fail' });

            effects.addConeSearchConfigFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });

    describe('editConeSearchConfig$ effect', () => {
        it('should edit config if payload is right', () => {
            const coneSearchConfig = CONE_SEARCH_CONFIG;
            coneSearchConfigService.editConeSearchConfig = () =>
                of(coneSearchConfig);
            action$ = cold('a', {
                a: { type: '[Metamodel] Edit Cone Search Config' },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Cone Search Config Success',
                    coneSearchConfig: CONE_SEARCH_CONFIG,
                },
            });

            expect(effects.editConeSearchConfig$).toBeObservable(expected$);
        });

        it('should not edit config if payload is wrong', () => {
            coneSearchConfigService.editConeSearchConfig = () =>
                throwError(() => {
                    const error = new Error('An Error happened');
                    return error;
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Edit Cone Search Config' },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Cone Search Config Fail',
                },
            });

            expect(effects.editConeSearchConfig$).toBeObservable(expected$);
        });
    })

    describe('editConeSearchConfig$ toasting', () => {
        it('success', () => {
            action$ = of({ type: '[Metamodel] Edit Cone Search Config Success' });

            effects.editConeSearchConfigSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Edit Cone Search Config Fail' });

            effects.editConeSearchConfigFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    })
});
