/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Action } from '@ngrx/store';
import { cold } from 'jasmine-marbles';

import * as instanceSelector from '../selectors/instance.selector';
import * as datasetSelector from '../selectors/dataset.selector';
import { AliasConfigEffects } from './alias-config.effects';
import { AliasConfigService } from '../services/alias-config.service';
import { ALIAS_CONFIG } from 'src/test-data';

describe('[Metamodel][Effects] AliasConfigEffects', () => {
    let effects: AliasConfigEffects = null;
    let aliasConfigService: AliasConfigService = null;
    let action$ = new Observable<Action>();
    const initialState = {
        selectors: [
            {
                selector: instanceSelector.selectInstanceNameByRoute,
                value: 'vimuser',
            },
            {
                selector: datasetSelector.selectDatasetNameByRoute,
                value: 'coconut',
            },
        ],
    };

    const toastr = { success: jest.fn(), error: jest.fn() };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                AliasConfigEffects,
                {
                    provide: ToastrService,
                    useValue: toastr,
                },
                {
                    provide: AliasConfigService,
                    useValue: {},
                },
                provideMockActions(() => action$),
                provideMockStore(initialState),
            ],
        }).compileComponents();

        effects = TestBed.inject(AliasConfigEffects);
        aliasConfigService = TestBed.inject(AliasConfigService);
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    describe('$loadAliasConfig effect', () => {
        it('should load config properly if payload is right', () => {
            const alias = ALIAS_CONFIG;
            aliasConfigService.retrieveAliasConfig = () => of(alias);
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Alias Config' },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Load Alias Config Success',
                    aliasConfig: alias,
                },
            });

            expect(effects.loadAliassConfig$).toBeObservable(expected$);
        });

        it('should not load config if payload is wrong', () => {
            aliasConfigService.retrieveAliasConfig = () =>
                throwError(() => {
                    throw new Error('An Error');
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Alias Config' },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Load Alias Config Fail',
                },
            });
            expect(effects.loadAliassConfig$).toBeObservable(expected$);
        });
    });

    describe('addAliasConfig$ effect', () => {
        it('should add a config properly if payload is right', () => {
            const alias = ALIAS_CONFIG;
            aliasConfigService.addAliasConfig = () => of(alias);
            action$ = cold('a', {
                a: { type: '[Metamodel] Add Alias Config', aliasConfig: alias },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Alias Config Success',
                    aliasConfig: alias,
                },
            });

            expect(effects.addAliasConfig$).toBeObservable(expected$);
        });

        it('should not add config if payload is wrong', () => {
            const alias = ALIAS_CONFIG;
            aliasConfigService.addAliasConfig = () =>
                throwError(() => {
                    throw new Error('An Error');
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Add Alias Config', aliasConfig: alias },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Alias Config Fail',
                },
            });
            expect(effects.addAliasConfig$).toBeObservable(expected$);
        });
    });

    describe('editAliasConfig$ effect', () => {
        it('should edit config properly if payload is right', () => {
            const alias = ALIAS_CONFIG;
            aliasConfigService.editAliasConfig = () => of(alias);
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Alias Config',
                    aliasConfig: ALIAS_CONFIG,
                },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Alias Config Success',
                    aliasConfig: ALIAS_CONFIG,
                },
            });

            expect(effects.editAliasConfig$).toBeObservable(expected$);
        });

        it('should not edit config if payload is wrong', () => {
            aliasConfigService.editAliasConfig = () =>
                throwError(() => {
                    throw new Error('An Error');
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Edit Alias Config' },
            });

            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Alias Config Fail',
                },
            });
            expect(effects.editAliasConfig$).toBeObservable(expected$);
        });
    });

    describe('addAliasConfigSuccess$ effect', () => {
        it('should toast success', () => {
            action$ = of({ type: '[Metamodel] Add Alias Config Success' });
            effects.addAliasConfigSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });
    });

    describe('addAliasConfigFail$ effect', () => {
        it('should toast error', () => {
            action$ = of({ type: '[Metamodel] Add Alias Config Fail' });
            effects.addAliasConfigFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });

    describe('editAliasConfigSuccess$ effect', () => {
        it('should toast success', () => {
            action$ = of({ type: '[Metamodel] Edit Alias Config Success' });
            effects.editAliasConfigSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });
    });

    describe('editAliasConfigFail$ effect', () => {
        it('should toast error', () => {
            action$ = of({ type: '[Metamodel] Edit Alias Config Fail' });
            effects.editAliasConfigFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });
});
