/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';

import { provideMockActions } from '@ngrx/effects/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Observable, of, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Action } from '@ngrx/store';
import { cold } from 'jasmine-marbles';

import * as instanceSelector from '../selectors/instance.selector';
import * as datasetSelector from '../selectors/dataset.selector';
import { DetailConfigEffects } from './detail-config.effects';
import { DetailConfigService } from '../services/detail-config.service';
import { DETAIL_CONFIG } from 'src/test-data';

describe('[Metamodel][Effects] DetailConfigEffects', () => {
    let effects: DetailConfigEffects = null;
    let detailConfigService: DetailConfigService = null;
    let action$ = new Observable<Action>();
    const initialState = {
        selectors: [
            {
                selector: instanceSelector.selectInstanceNameByRoute,
                value: 'vimuser',
            },
            {
                selector: datasetSelector.selectDatasetNameByRoute,
                value: 'coconut',
            },
        ],
    };
    const toastr = { success: jest.fn(), error: jest.fn() };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                DetailConfigEffects,
                {
                    provide: DetailConfigService,
                    useValue: {},
                },
                {
                    provide: ToastrService,
                    useValue: toastr,
                },
                provideMockActions(() => action$),
                provideMockStore(initialState),
            ],
        }).compileComponents();

        effects = TestBed.inject(DetailConfigEffects);
        detailConfigService = TestBed.inject(DetailConfigService);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    it('should be truthy', () => {
        expect(effects).toBeTruthy();
    });

    describe('loadDetailsConfig$ effect', () => {
        it('should load properly when payload is right', () => {
            const detail = DETAIL_CONFIG;
            detailConfigService.retrieveDetailConfig = () => of(detail);
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Detail Config' },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Load Detail Config Success',
                    detailConfig: detail,
                },
            });
            expect(effects.loadDetailsConfig$).toBeObservable(expected$);
        });

        it('should not load when payload is wrong', () => {
            detailConfigService.retrieveDetailConfig = () =>
                throwError(() => {
                    throw new Error('An Error');
                });
            action$ = cold('a', {
                a: { type: '[Metamodel] Load Detail Config' },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Load Detail Config Fail',
                },
            });
            expect(effects.loadDetailsConfig$).toBeObservable(expected$);
        });
    });

    describe('addDetailsConfig$ effect', () => {
        it('should add properly when payload is right', () => {
            const detail = DETAIL_CONFIG;
            detailConfigService.addDetailConfig = () => of(detail);
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Detail Config',
                    detailConfig: detail,
                },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Detail Config Success',
                    detailConfig: detail,
                },
            });
            expect(effects.addDetailConfig$).toBeObservable(expected$);
        });

        it('should not add when payload is wrong', () => {
            const detail = DETAIL_CONFIG;
            detailConfigService.addDetailConfig = () =>
                throwError(() => {
                    throw new Error('An Error');
                });
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Detail Config',
                    detailConfig: detail,
                },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Add Detail Config Fail',
                },
            });
            expect(effects.addDetailConfig$).toBeObservable(expected$);
        });
    });

    describe('addDetailConfig$ toasting', () => {
        it('success', () => {
            action$ = of({ type: '[Metamodel] Add Detail Config Success' });
            effects.addDetailConfigSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Add Detail Config Fail' });
            effects.addDetailConfigFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });

    describe('editDetailsConfig$ effect', () => {
        it('should edit properly when payload is right', () => {
            const detail = DETAIL_CONFIG;
            detailConfigService.editDetailConfig = () => of(detail);
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Detail Config',
                    detailConfig: detail,
                },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Detail Config Success',
                    detailConfig: detail,
                },
            });
            expect(effects.editDetailConfig$).toBeObservable(expected$);
        });

        it('should not edit when payload is wrong', () => {
            const detail = DETAIL_CONFIG;
            detailConfigService.editDetailConfig = () =>
                throwError(() => {
                    throw new Error('An Error');
                });
            action$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Detail Config',
                    detailConfig: detail,
                },
            });
            const expected$ = cold('a', {
                a: {
                    type: '[Metamodel] Edit Detail Config Fail',
                },
            });
            expect(effects.editDetailConfig$).toBeObservable(expected$);
        });
    });

    describe('editDetailConfig$ toasting', () => {
        it('success', () => {
            action$ = of({ type: '[Metamodel] Edit Detail Config Success' });
            effects.editDetailConfigSuccess$.subscribe();
            expect(toastr.success).toHaveBeenCalled();
        });

        it('error', () => {
            action$ = of({ type: '[Metamodel] Edit Detail Config Fail' });
            effects.editDetailConfigFail$.subscribe();
            expect(toastr.error).toHaveBeenCalled();
        });
    });

});
