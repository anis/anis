/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { map, tap, mergeMap, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

import * as menuItemActions from '../actions/menu-item.actions';
import { MenuItemService } from '../services/menu-item.service';
import * as instanceSelector from '../selectors/instance.selector';

@Injectable()
export class MenuItemEffects {
    /**
     * Calls action to retrieve menu item list.
     */
    loadMenuItems$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(menuItemActions.loadMenuItemList),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            mergeMap(([, instanceName]) => this.menuItemService.retrieveMenuItemList(instanceName)
                .pipe(
                    map(menuItems => menuItemActions.loadMenuItemListSuccess({ menuItems })),
                    catchError(() => of(menuItemActions.loadMenuItemListFail()))
                )
            )
        )
    );

    /**
     * Calls action to add a menu item.
     */
    addMenuItem$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(menuItemActions.addMenuItem),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            mergeMap(([action, instanceName]) => this.menuItemService.addMenuItem(instanceName, action.menuItem)
                .pipe(
                    map(menuItem => menuItemActions.addMenuItemSuccess({ menuItem })),
                    catchError(() => of(menuItemActions.addMenuItemFail()))
                )
            )
        )
    );

    /**
     * Displays add menu item success notification.
     */
    addMenuItemSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(menuItemActions.addMenuItemSuccess),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            tap(([, instanceName]) => {
                this.router.navigate([`/admin/instance/configure-instance/${instanceName}/menu`]);
                this.toastr.success('Menu item successfully added', 'The new menu item was added into the database')
            })
        ), { dispatch: false }
    );

    /**
     * Displays add menu item error notification.
     */
    addMenuItemFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(menuItemActions.addMenuItemFail),
            tap(() => this.toastr.error('Failure to add menu item', 'The new  menu item could not be added into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to modify a menu item.
     */
    editMenuItem$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(menuItemActions.editMenuItem),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            mergeMap(([action, instanceName]) => this.menuItemService.editMenuItem(instanceName, action.menuItem)
                .pipe(
                    map(menuItem => menuItemActions.editMenuItemSuccess({ menuItem })),
                    catchError(() => of(menuItemActions.editMenuItemFail()))
                )
            )
        )
    );

    /**
     * Displays edit menu item success notification.
     */
    editMenuItemSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(menuItemActions.editMenuItemSuccess),
            tap(() => this.toastr.success('Menu item successfully edited', 'The existing menu item has been edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Displays edit menu item error notification.
     */
    editMenuItemFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(menuItemActions.editMenuItemFail),
            tap(() => this.toastr.error('Failure to edit menu item', 'The existing menu item could not be edited into the database'))
        ), { dispatch: false }
    );

    /**
     * Calls action to remove a menu item.
     */
    deleteMenuItem$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(menuItemActions.deleteMenuItem),
            concatLatestFrom(() => this.store.select(instanceSelector.selectInstanceNameByRoute)),
            mergeMap(([action, instanceName]) => this.menuItemService.deleteMenuItem(instanceName, action.menuItem.id)
                .pipe(
                    map(() => menuItemActions.deleteMenuItemSuccess({ menuItem: action.menuItem })),
                    catchError(() => of(menuItemActions.deleteMenuItemFail()))
                )
            )
        )
    );

    /**
     * Displays delete menu item success notification.
     */
    deleteMenuItemSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(menuItemActions.deleteMenuItemSuccess),
            tap(() => this.toastr.success('Menu item successfully deleted', 'The existing menu item has been deleted'))
        ), { dispatch: false }
    );

    /**
     * Displays delete menu item error notification.
     */
    deleteMenuItemFail$ = createEffect(() =>
        this.actions$.pipe(
            ofType(menuItemActions.deleteMenuItemFail),
            tap(() => this.toastr.error('Failure to delete menu item', 'The existing menu item could not be deleted from the database'))
        ), { dispatch: false }
    );

    constructor(
        private actions$: Actions,
        private menuItemService: MenuItemService,
        private router: Router,
        private toastr: ToastrService,
        private store: Store<{ }>
    ) {}
}
