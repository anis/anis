/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InstanceGroup } from "./instance-group.model";

export interface Instance {
    name: string;
    label: string;
    description: string;
    scientific_manager: string;
    instrument: string;
    wavelength_domain: string;
    data_path: string;
    files_path: string;
    public: boolean;
    portal_logo: string;
    portal_color: string;
    default_redirect: string;
    groups: InstanceGroup[];
    nb_dataset_families: number;
    nb_datasets: number;
}
