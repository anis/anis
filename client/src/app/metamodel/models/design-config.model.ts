/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Logo } from './logo.model';

/**
 * Interface for design config.
 *
 * @interface DesignConfig
 */
export interface DesignConfig {
    design_background_color: string;
    design_text_color: string;
    design_font_family: string;
    design_link_color: string;
    design_link_hover_color: string;
    design_logo: string;
    design_logo_href: string;
    design_favicon: string;
    navbar_background_color: string;
    navbar_border_bottom_color: string;
    navbar_color_href: string;
    navbar_font_family: string;
    navbar_sign_in_btn_color: string;
    navbar_user_btn_color: string;
    footer_background_color: string;
    footer_border_top_color: string;
    footer_text_color: string;
    footer_logos: Logo[];
    family_border_color: string;
    family_header_background_color: string;
    family_title_color: string;
    family_title_bold: boolean;
    family_background_color: string;
    family_text_color: string;
    progress_bar_title: string;
    progress_bar_title_color: string;
    progress_bar_subtitle: string;
    progress_bar_subtitle_color: string;
    progress_bar_step_dataset_title: string;
    progress_bar_step_criteria_title: string;
    progress_bar_step_output_title: string;
    progress_bar_step_result_title: string;
    progress_bar_color: string;
    progress_bar_active_color: string;
    progress_bar_circle_color: string;
    progress_bar_circle_icon_color: string;
    progress_bar_circle_icon_active_color: string;
    progress_bar_text_color: string;
    progress_bar_text_bold: boolean;
    search_next_btn_color: string;
    search_next_btn_hover_color: string;
    search_next_btn_hover_text_color: string;
    search_back_btn_color: string;
    search_back_btn_hover_color: string;
    search_back_btn_hover_text_color: string;
    delete_current_query_btn_enabled: boolean;
    delete_current_query_btn_text: string;
    delete_current_query_btn_color: string;
    delete_current_query_btn_hover_color: string;
    delete_current_query_btn_hover_text_color: string;
    delete_criterion_cross_color: string;
    search_info_background_color: string;
    search_info_text_color: string;
    search_info_help_enabled: boolean;
    dataset_select_btn_color: string;
    dataset_select_btn_hover_color: string;
    dataset_select_btn_hover_text_color: string;
    dataset_selected_icon_color: string;
    search_criterion_background_color: string;
    search_criterion_text_color: string;
    output_columns_selected_color: string;
    output_columns_select_all_btn_color: string;
    output_columns_select_all_btn_hover_color: string;
    output_columns_select_all_btn_hover_text_color: string;
    result_panel_border_size: string;
    result_panel_border_color: string;
    result_panel_title_color: string;
    result_panel_background_color: string;
    result_panel_text_color: string;
    result_download_btn_color: string;
    result_download_btn_hover_color: string;
    result_download_btn_text_color: string;
    result_datatable_actions_btn_color: string;
    result_datatable_actions_btn_hover_color: string;
    result_datatable_actions_btn_text_color: string;
    result_datatable_bordered: boolean;
    result_datatable_bordered_radius: boolean;
    result_datatable_border_color: string;
    result_datatable_header_background_color: string;
    result_datatable_header_text_color: string;
    result_datatable_rows_background_color: string;
    result_datatable_rows_text_color: string;
    result_datatable_sorted_color: string;
    result_datatable_sorted_active_color: string;
    result_datatable_link_color: string;
    result_datatable_link_hover_color: string;
    result_datatable_rows_selected_color: string;
    result_datatable_pagination_link_color: string;
    result_datatable_pagination_active_bck_color: string;
    result_datatable_pagination_active_text_color: string;
    samp_enabled: boolean;
    back_to_portal: boolean;
    user_menu_enabled: boolean;
    search_multiple_all_datasets_selected: boolean;
    search_multiple_progress_bar_title: string;
    search_multiple_progress_bar_subtitle: string;
    search_multiple_progress_bar_step_position: string;
    search_multiple_progress_bar_step_datasets: string;
    search_multiple_progress_bar_step_result: string;
}

export const defaultDesignConfig: DesignConfig = {
    design_background_color: '#FFFFFF',
    design_text_color: '#212529',
    design_font_family: 'Roboto, sans-serif',
    design_link_color: '#007BFF',
    design_link_hover_color: '#0056B3',
    design_logo: '',
    design_logo_href: null,
    design_favicon: '',
    navbar_background_color: '#F8F9FA',
    navbar_border_bottom_color: '#DEE2E6',
    navbar_color_href: '#000000',
    navbar_font_family: 'auto',
    navbar_sign_in_btn_color: '#28A745',
    navbar_user_btn_color: '#7AC29A',
    footer_background_color: '#F8F9FA',
    footer_border_top_color: '#DEE2E6',
    footer_text_color: '#000000',
    footer_logos: [],
    family_border_color: '#DFDFDF',
    family_header_background_color: '#F7F7F7',
    family_title_color: '#007BFF',
    family_title_bold: false,
    family_background_color: '#FFFFFF',
    family_text_color: '#212529',
    progress_bar_title: 'Dataset search',
    progress_bar_title_color: '#000000',
    progress_bar_subtitle: 'Select a dataset, add criteria, select output columns and display the result.',
    progress_bar_subtitle_color: '#6C757D',
    progress_bar_step_dataset_title: 'Dataset selection',
    progress_bar_step_criteria_title: 'Search criteria',
    progress_bar_step_output_title: 'Output columns',
    progress_bar_step_result_title: 'Result table',
    progress_bar_color: '#E9ECEF',
    progress_bar_active_color: '#7AC29A',
    progress_bar_circle_color: '#FFFFFF',
    progress_bar_circle_icon_color: '#CCCCCC',
    progress_bar_circle_icon_active_color: '#FFFFFF',
    progress_bar_text_color: '#91B2BF',
    progress_bar_text_bold: false,
    search_next_btn_color: '#007BFF',
    search_next_btn_hover_color: '#007BFF',
    search_next_btn_hover_text_color: '#FFFFFF',
    search_back_btn_color: '#6C757D',
    search_back_btn_hover_color: '#6C757D',
    search_back_btn_hover_text_color: '#FFFFFF',
    delete_current_query_btn_enabled: false,
    delete_current_query_btn_text: 'Delete the current query',
    delete_current_query_btn_color: '#DC3545',
    delete_current_query_btn_hover_color: '#C82333',
    delete_current_query_btn_hover_text_color: '#FFFFFF',
    delete_criterion_cross_color: '#DC3545',
    search_info_background_color: '#E9ECEF',
    search_info_text_color: '#000000',
    search_info_help_enabled: true,
    dataset_select_btn_color: '#6C757D',
    dataset_select_btn_hover_color: '#6C757D',
    dataset_select_btn_hover_text_color: '#FFFFFF',
    dataset_selected_icon_color: '#28A745',
    search_criterion_background_color: '#7AC29A',
    search_criterion_text_color: '#000000',
    output_columns_selected_color: '#7AC29A',
    output_columns_select_all_btn_color: '#6C757D',
    output_columns_select_all_btn_hover_color: '#6C757D',
    output_columns_select_all_btn_hover_text_color: '#FFFFFF',
    result_panel_border_size: '1px',
    result_panel_border_color: '#DEE2E6',
    result_panel_title_color: '#000000',
    result_panel_background_color: '#FFFFFF',
    result_panel_text_color: '#000000',
    result_download_btn_color: '#007BFF',
    result_download_btn_hover_color: '#0069D9',
    result_download_btn_text_color: '#FFFFFF',
    result_datatable_actions_btn_color: '#007BFF',
    result_datatable_actions_btn_hover_color: '#0069D9',
    result_datatable_actions_btn_text_color: '#FFFFFF',
    result_datatable_bordered: true,
    result_datatable_bordered_radius: false,
    result_datatable_border_color: '#DEE2E6',
    result_datatable_header_background_color: '#FFFFFF',
    result_datatable_header_text_color: '#000000',
    result_datatable_rows_background_color: '#FFFFFF',
    result_datatable_rows_text_color: '#000000',
    result_datatable_sorted_color: '#C5C5C5',
    result_datatable_sorted_active_color: '#000000',
    result_datatable_link_color: '#007BFF',
    result_datatable_link_hover_color: '#0056B3',
    result_datatable_rows_selected_color: '#7AC29A',
    result_datatable_pagination_link_color: '#007BFF',
    result_datatable_pagination_active_bck_color: '#007BFF',
    result_datatable_pagination_active_text_color: '#007BFF',
    samp_enabled: true,
    back_to_portal: true,
    user_menu_enabled: true,
    search_multiple_all_datasets_selected: false,
    search_multiple_progress_bar_title: 'Search around a position in multiple datasets',
    search_multiple_progress_bar_subtitle: 'Fill RA & DEC position, select datasets and display the result.',
    search_multiple_progress_bar_step_position:'Position',
    search_multiple_progress_bar_step_datasets:'Datasets',
    search_multiple_progress_bar_step_result:'Result'
};
