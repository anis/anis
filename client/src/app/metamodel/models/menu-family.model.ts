/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { MenuItem } from './menu-item.model';

/**
 * Interface for menu family.
 *
 * @interface MenuFamily
 */
export interface MenuFamily extends MenuItem {
    name: string;
    items: MenuItem[];
}
