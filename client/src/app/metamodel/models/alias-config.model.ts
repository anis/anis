/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for alias config.
 *
 * @interface AliasConfig
 */
export interface AliasConfig {
    table_alias: string;
    column_alias: string;
    column_name: string;
    column_alias_long: string;
}
