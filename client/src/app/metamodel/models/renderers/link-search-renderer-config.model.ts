/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { RendererConfig } from './renderer-config.model';

/**
 * Interface for link renderer config.
 *
 * @interface LinkSearchRendererConfig
 * @extends RendererConfig
 */
export interface LinkSearchRendererConfig extends RendererConfig {
    dataset: string;
    attributes: string;
    criteria: string;
    display: string;
    text: string;
    icon: string;
}
