/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for renderer config.
 *
 * @interface RendererConfig
 */
export interface RendererConfig {
    id: 'renderer-config';
}
