/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { RendererConfig } from './renderer-config.model';

/**
 * Interface for badge renderer config.
 *
 * @interface BadgeRendererConfig
 * @extends RendererConfig
 */
export interface BadgeRendererConfig extends RendererConfig {
    pill: boolean;
    color: string;
    block: boolean;
}
