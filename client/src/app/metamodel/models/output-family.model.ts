/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { OutputCategory } from "./output-category.model";

export interface OutputFamily {
    id: number;
    label: string;
    display: number;
    opened: boolean;
    output_categories: OutputCategory[];
}
