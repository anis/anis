/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for menu item.
 *
 * @interface MenuItem
 */
export interface MenuItem {
    id: number;
    label: string;
    icon: string;
    display: number;
    type: string;
}
