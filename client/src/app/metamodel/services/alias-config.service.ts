/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AliasConfig } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class AliasConfigService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves alias configuration for a dataset
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * 
     * @return Observable<AliasConfig>
     */
    retrieveAliasConfig(instanceName: string, datasetName: string): Observable<AliasConfig> {
        return this.anisHttpClientService.get<AliasConfig>(`/instance/${instanceName}/dataset/${datasetName}/alias-config`);
    }

    /**
     * Adds a new alias configuration for the given dataset
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param AliasConfig newAliasConfig - The new alias configuration to be registred
     *
     * @return Observable<AliasConfig>
     */
    addAliasConfig(instanceName: string, datasetName: string, newAliasConfig: AliasConfig): Observable<AliasConfig> {
        return this.anisHttpClientService.post<AliasConfig>(`/instance/${instanceName}/dataset/${datasetName}/alias-config`, newAliasConfig);
    }

    /**
     * Modifies alias configuration
     *
     * @param string instanceName - The name of the instance
     * @param string datasetName - The name of the current dataset
     * @param AliasConfig aliasConfig - The alias cofiguration object to be edited
     *
     * @return Observable<AliasConfig>
     */
    editAliasConfig(instanceName: string, datasetName: string, aliasConfig: AliasConfig): Observable<AliasConfig> {
        return this.anisHttpClientService.put<AliasConfig>(`/instance/${instanceName}/dataset/${datasetName}/alias-config`, aliasConfig);
    }
}
