/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { MenuItem, Instance } from '../models';
import { AppConfigService } from 'src/app/app-config.service';
import { MenuItemService } from './menu-item.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { INSTANCE, WEBPAGE } from 'src/test-data';

describe('[Metamodel][services] MenuItemService', () => {
    let service: MenuItemService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let webpage: MenuItem;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                MenuItemService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(MenuItemService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        webpage = { ...WEBPAGE };
    }));

    it('#retrieveMenuItemList() should request return an Observable<MenuItem[]> object', () => {
        service.retrieveMenuItemList(instance.name).subscribe((res: MenuItem[]) => {
            expect(res).toEqual([webpage]);
        });
        const url = `http://localhost:8080/instance/${instance.name}/menu-item`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: [webpage]});
    });

    it('#addMenuItem() should request return an Observable<MenuItem> object', () => {
        service.addMenuItem(instance.name, webpage).subscribe((res: MenuItem) => {
            expect(res).toEqual(webpage);
        });
        const url = `http://localhost:8080/instance/${instance.name}/menu-item?type=${webpage.type}`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: webpage});
    });

    it('#editMenuItem() should request return an Observable<MenuItem> object', () => {
        service.editMenuItem(instance.name, webpage).subscribe((res: MenuItem) => {
            expect(res).toEqual(webpage);
        });
        const url = `http://localhost:8080/instance/${instance.name}/menu-item/${webpage.id}`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: webpage});
    });

    it('#deleteMenuItem() should request return an Observable<{ message: string; }> object', () => {
        service.deleteMenuItem(instance.name, webpage.id).subscribe((res: { message: string; }) => {
            expect(res).toEqual({message: `Menu item with id ${webpage.id} is removed!`});
        });
        const url = `http://localhost:8080/instance/${instance.name}/menu-item/${webpage.id}`;
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}` });
        mockRequest.flush({status: 200, data: {message: `Menu item with id ${webpage.id} is removed!`}});
    });
});
