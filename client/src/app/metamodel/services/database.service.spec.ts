/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Instance, Database } from '../models';
import { AppConfigService } from 'src/app/app-config.service';
import { DatabaseService } from './database.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { INSTANCE, DATABASE } from 'src/test-data';

describe('[Metamodel][services] DatabaseService', () => {
    let service: DatabaseService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let database: Database;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                DatabaseService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(DatabaseService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        database = { ...DATABASE };
    }));

    it('#retrieveDatabaseList() should request return an Observable<Database[]> object', () => {
        service.retrieveDatabaseList(instance.name).subscribe((res: Database[]) => {
            expect(res).toEqual([database]);
        });
        const url = `http://localhost:8080/instance/${instance.name}/database`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: [database]});
    });

    it('#addDatabase() should request return an Observable<Database> object', () => {
        service.addDatabase(instance.name, database).subscribe((res: Database) => {
            expect(res).toEqual(database);
        });
        const url = `http://localhost:8080/instance/${instance.name}/database`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: database});
    });

    it('#editDatabase() should request return an Observable<Database> object', () => {
        service.editDatabase(instance.name, database).subscribe((res: Database) => {
            expect(res).toEqual(database);
        });
        const url = `http://localhost:8080/instance/${instance.name}/database/${database.id}`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: database});
    });

    it('#deleteDatabase() should request return an Observable<{ message: string; }> object', () => {
        service.deleteDatabase(instance.name, database.id).subscribe((res: { message: string; }) => {
            expect(res).toEqual({message: `Instance-group with role ${database.id} is removed!`});
        });
        const url = `http://localhost:8080/instance/${instance.name}/database/${database.id}`;
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}` });
        mockRequest.flush({status: 200, data: {message: `Database with id ${database.id} is removed!`}});
    });
});
