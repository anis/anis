/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Image } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class ImageService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves the list of all images for a dataset
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * 
     * @return Observable<Image[]>
     */
    retrieveImageList(instanceName: string, datasetName: string): Observable<Image[]> {
        return this.anisHttpClientService.get<Image[]>(`/instance/${instanceName}/dataset/${datasetName}/image`);
    }

    /**
     * Adds a new image
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param Image newImage - The new image to be registred
     *
     * @return Observable<Image>
     */
    addImage(instanceName: string, datasetName: string, newImage: Image): Observable<Image> {
        return this.anisHttpClientService.post<Image>(`/instance/${instanceName}/dataset/${datasetName}/image`, newImage);
    }

    /**
     * Modifies an image
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param Image image - The image object to be edited
     *
     * @return Observable<Image>
     */
    editImage(instanceName: string, datasetName: string, image: Image): Observable<Image> {
        return this.anisHttpClientService.put<Image>(`/instance/${instanceName}/dataset/${datasetName}/image/${image.id}`, image);
    }

    /**
     * Removes an image
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param number imageId - The ID of the image to be deleted
     *
     * @return Observable<object>
     */
    deleteImage(instanceName: string, datasetName: string, imageId: number): Observable<object> {
        return this.anisHttpClientService.delete(`/instance/${instanceName}/dataset/${datasetName}/image/${imageId}`);
    }
}
