/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DetailConfig } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class DetailConfigService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves detail configuration for a dataset
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * 
     * @return Observable<DetailConfig>
     */
    retrieveDetailConfig(instanceName: string, datasetName: string): Observable<DetailConfig> {
        return this.anisHttpClientService.get<DetailConfig>(`/instance/${instanceName}/dataset/${datasetName}/detail-config`);
    }

    /**
     * Adds a new detail configuration for the given dataset
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param DetailConfig newDetailConfig - The new detail configuration to be registred
     *
     * @return Observable<DetailConfig>
     */
    addDetailConfig(instanceName: string, datasetName: string, newDetailConfig: DetailConfig): Observable<DetailConfig> {
        return this.anisHttpClientService.post<DetailConfig>(`/instance/${instanceName}/dataset/${datasetName}/detail-config`, newDetailConfig);
    }

    /**
     * Modifies detail configuration
     *
     * @param string instanceName - The name of the instance
     * @param string datasetName - The name of the current dataset
     * @param DetailConfig detailConfig - The detail cofiguration object to be edited
     *
     * @return Observable<DetailConfig>
     */
    editDetailConfig(instanceName: string, datasetName: string, detailConfig: DetailConfig): Observable<DetailConfig> {
        return this.anisHttpClientService.put<DetailConfig>(`/instance/${instanceName}/dataset/${datasetName}/detail-config`, detailConfig);
    }
}
