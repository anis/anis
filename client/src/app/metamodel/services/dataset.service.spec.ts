/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Instance, Dataset } from '../models';
import { AppConfigService } from 'src/app/app-config.service';
import { DatasetService } from './dataset.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { INSTANCE, DATASET } from 'src/test-data';

describe('[Metamodel][services] DatasetService', () => {
    let service: DatasetService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let dataset: Dataset;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                DatasetService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(DatasetService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        dataset = { ...DATASET };
    }));

    it('#retrieveDatasetList() should request return an Observable<Dataset[]> object', () => {
        service.retrieveDatasetList(instance.name).subscribe((res: Dataset[]) => {
            expect(res).toEqual([dataset]);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: [dataset]});
    });

    it('#importDataset() should request return an Observable<Dataset> object', () => {
        service.importDataset(instance.name, dataset).subscribe((res: Dataset) => {
            expect(res).toEqual(dataset);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/import`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 200, data: dataset});
    });

    it('#addDataset() should request return an Observable<Dataset> object', () => {
        service.addDataset(instance.name, dataset).subscribe((res: Dataset) => {
            expect(res).toEqual(dataset);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: dataset});
    });

    it('#editDataset() should request return an Observable<Dataset> object', () => {
        service.editDataset(instance.name, dataset).subscribe((res: Dataset) => {
            expect(res).toEqual(dataset);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: dataset});
    });

    it('#deleteDataset() should request return an Observable<{ message: string; }> object', () => {
        service.deleteDataset(instance.name, dataset.name).subscribe((res: { message: string; }) => {
            expect(res).toEqual({message: `Dataset with name ${dataset.name} is removed!`});
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}`;
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}` });
        mockRequest.flush({status: 200, data: {message: `Dataset with name ${dataset.name} is removed!`}});
    });
});
