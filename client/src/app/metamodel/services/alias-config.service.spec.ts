/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AppConfigService } from '../../app-config.service';
import { AliasConfigService } from './alias-config.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { Instance, Dataset, AliasConfig } from '../models';
import { INSTANCE, DATASET, ALIAS_CONFIG } from 'src/test-data';

describe('[Metamodel][services] AliasConfigService', () => {
    let service: AliasConfigService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let dataset: Dataset;
    let aliasConfig: AliasConfig;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                AliasConfigService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(AliasConfigService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        instance = { ...INSTANCE };
        dataset = { ...DATASET };
        aliasConfig = { ...ALIAS_CONFIG };
    }));

    it('#retrieveAliasConfig() should request return an Observable<AliasConfig> object', () => {
        service.retrieveAliasConfig(instance.name, dataset.name).subscribe((res: AliasConfig) => {
            expect(res).toEqual(aliasConfig);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/alias-config`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: aliasConfig});
    });

    it('#addAliasConfig() should request return an Observable<AliasConfig> object', () => {
        service.addAliasConfig(instance.name, dataset.name, aliasConfig).subscribe((res: AliasConfig) => {
            expect(res).toEqual(aliasConfig);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/alias-config`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: aliasConfig});
    });

    it('#editAliasConfig() should request return an Observable<AliasConfig> object', () => {
        service.editAliasConfig(instance.name, dataset.name, aliasConfig).subscribe((res: AliasConfig) => {
            expect(res).toEqual(aliasConfig);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/alias-config`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: aliasConfig});
    });
});
