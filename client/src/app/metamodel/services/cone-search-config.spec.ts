/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AppConfigService } from '../../app-config.service';
import { ConeSearchConfigService } from './cone-search-config.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { Instance, Dataset, ConeSearchConfig } from '../models';
import { INSTANCE, DATASET, CONE_SEARCH_CONFIG } from 'src/test-data';

describe('[Metamodel][services] ConeSearchConfigService', () => {
    let service: ConeSearchConfigService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let dataset: Dataset;
    let coneSearchConfig: ConeSearchConfig;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                ConeSearchConfigService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(ConeSearchConfigService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        instance = { ...INSTANCE };
        dataset = { ...DATASET };
        coneSearchConfig = { ...CONE_SEARCH_CONFIG };
    }));

    it('#retrieveConeSearchConfig() should request return an Observable<ConeSearchConfig> object', () => {
        service.retrieveConeSearchConfig(instance.name, dataset.name).subscribe((res: ConeSearchConfig) => {
            expect(res).toEqual(coneSearchConfig);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/cone-search-config`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: coneSearchConfig});
    });

    it('#addConeSearchConfig() should request return an Observable<ConeSearchConfig> object', () => {
        service.addConeSearchConfig(instance.name, dataset.name, coneSearchConfig).subscribe((res: ConeSearchConfig) => {
            expect(res).toEqual(coneSearchConfig);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/cone-search-config`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: coneSearchConfig});
    });

    it('#editConeSearchConfig() should request return an Observable<ConeSearchConfig> object', () => {
        service.editConeSearchConfig(instance.name, dataset.name, coneSearchConfig).subscribe((res: ConeSearchConfig) => {
            expect(res).toEqual(coneSearchConfig);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/cone-search-config`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: coneSearchConfig});
    });
});
