/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Instance, Dataset, Attribute } from '../models';
import { AppConfigService } from 'src/app/app-config.service';
import { AttributeService } from './attribute.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { INSTANCE, DATASET, ATTRIBUTE } from 'src/test-data';

describe('[Metamodel][services] AttributeService', () => {
    let service: AttributeService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let dataset: Dataset;
    let attribute: Attribute;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                AttributeService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(AttributeService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        dataset = { ...DATASET };
        attribute = { ...ATTRIBUTE };
    }));

    it('#retrieveAttributeList() should request return an Observable<Attribute[]> object', () => {
        service.retrieveAttributeList(instance.name, dataset.name).subscribe((res: Attribute[]) => {
            expect(res).toEqual([attribute]);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/attribute`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: [attribute]});
    });

    it('#addAttribute() should request return an Observable<Attribute> object', () => {
        service.addAttribute(instance.name, dataset.name, attribute).subscribe((res: Attribute) => {
            expect(res).toEqual(attribute);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/attribute`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: attribute});
    });

    it('#editAttribute() should request return an Observable<Attribute> object', () => {
        service.editAttribute(instance.name, dataset.name, attribute).subscribe((res: Attribute) => {
            expect(res).toEqual(attribute);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/attribute/${attribute.id}`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: attribute});
    });

    it('#deleteAttribute() should request return an Observable<{ message: string; }> object', () => {
        service.deleteAttribute(instance.name, dataset.name, attribute.id).subscribe((res: { message: string; }) => {
            expect(res).toEqual({message: `Attribute with id ${attribute.id} is removed!`});
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/attribute/${attribute.id}`;
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}` });
        mockRequest.flush({status: 200, data: {message: `Attribute with id ${attribute.id} is removed!`}});
    });
});
