/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Database } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class DatabaseService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves the list of all available databases for an instance
     *
     * @param string instanceName - The name of the current instance
     * 
     * @return Observable<Database[]>
     */
    retrieveDatabaseList(instanceName: string): Observable<Database[]> {
        return this.anisHttpClientService.get<Database[]>(`/instance/${instanceName}/database`);
    }

    /**
     * Adds a new database
     *
     * @param string instanceName - The name of the current instance
     * @param Database newDatabase - The new database to be registred
     *
     * @return Observable<Database>
     */
    addDatabase(instanceName: string, newDatabase: Database): Observable<Database> {
        return this.anisHttpClientService.post<Database>(`/instance/${instanceName}/database`, newDatabase);
    }

    /**
     * Modifies a database
     *
     * @param string instanceName - The name of the current instance
     * @param Database database - The database object to be edited
     *
     * @return Observable<Database>
     */
    editDatabase(instanceName: string, database: Database): Observable<Database> {
        return this.anisHttpClientService.put<Database>(`/instance/${instanceName}/database/${database.id}`, database);
    }

    /**
     * Removes a database
     *
     * @param string instanceName - The name of the current instance
     * @param number databaseId - The id of the database to be deleted
     *
     * @return Observable<object>
     */
    deleteDatabase(instanceName: string, databaseId: number): Observable<object> {
        return this.anisHttpClientService.delete(`/instance/${instanceName}/database/${databaseId}`);
    }
}
