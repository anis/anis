/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AppConfigService } from '../../app-config.service';
import { DesignConfigService } from './design-config.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { DesignConfig, Instance } from '../models';
import { INSTANCE, DESIGN_CONFIG } from 'src/test-data';

describe('[Metamodel][services] DesignConfigService', () => {
    let service: DesignConfigService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let designConfig: DesignConfig;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                DesignConfigService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(DesignConfigService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        instance = { ...INSTANCE };
        designConfig = { ...DESIGN_CONFIG };
    }));

    it('#retrieveDesignConfig() should request return an Observable<DesignConfig> object', () => {
        service.retrieveDesignConfig(instance.name).subscribe((res: DesignConfig) => {
            expect(res).toEqual(designConfig);
        });
        const url = `http://localhost:8080/instance/${instance.name}/design-config`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: designConfig});
    });

    it('#addDesignConfig() should request return an Observable<DesignConfig> object', () => {
        service.addDesignConfig(instance.name, designConfig).subscribe((res: DesignConfig) => {
            expect(res).toEqual(designConfig);
        });
        const url = `http://localhost:8080/instance/${instance.name}/design-config`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: designConfig});
    });

    it('#editDesignConfig() should request return an Observable<DesignConfig> object', () => {
        service.editDesignConfig(instance.name, designConfig).subscribe((res: DesignConfig) => {
            expect(res).toEqual(designConfig);
        });
        const url = `http://localhost:8080/instance/${instance.name}/design-config`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: designConfig});
    });
});
