/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Instance, DatasetGroup, Dataset } from '../models';
import { AppConfigService } from 'src/app/app-config.service';
import { DatasetGroupService } from './dataset-group.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { INSTANCE, DATASET } from 'src/test-data';

describe('[Metamodel][services] DatasetGroupService', () => {
    let service: DatasetGroupService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let dataset: Dataset;
    let datasetGroup: DatasetGroup;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                DatasetGroupService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(DatasetGroupService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        dataset = { ...DATASET };
        datasetGroup = { ...DATASET.groups[0] };
    }));

    it('#retrieveDatasetGroupList() should request return an Observable<DatasetGroup[]> object', () => {
        service.retrieveDatasetGroupList(instance.name, dataset.name).subscribe((res: DatasetGroup[]) => {
            expect(res).toEqual([datasetGroup]);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/group`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: [datasetGroup]});
    });

    it('#addDatasetGroup() should request return an Observable<DatasetGroup> object', () => {
        service.addDatasetGroup(instance.name, dataset.name, datasetGroup).subscribe((res: DatasetGroup) => {
            expect(res).toEqual(datasetGroup);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/group`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: datasetGroup});
    });

    it('#editDatasetGroup() should request return an Observable<DatasetGroup> object', () => {
        service.editDatasetGroup(instance.name, dataset.name, datasetGroup).subscribe((res: DatasetGroup) => {
            expect(res).toEqual(datasetGroup);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/group/${datasetGroup.role}`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: datasetGroup});
    });

    it('#deleteDatasetGroup() should request return an Observable<{ message: string; }> object', () => {
        service.deleteDatasetGroup(instance.name, dataset.name, datasetGroup.role).subscribe((res: { message: string; }) => {
            expect(res).toEqual({message: `Dataset group with role ${datasetGroup.role} is removed!s`});
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/group/${datasetGroup.role}`;
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}` });
        mockRequest.flush({status: 200, data: {message: `Dataset group with role ${datasetGroup.role} is removed!`}});
    });
});
