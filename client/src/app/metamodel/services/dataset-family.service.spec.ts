/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DatasetFamily, Instance } from '../models';
import { AppConfigService } from 'src/app/app-config.service';
import { DatasetFamilyService } from './dataset-family.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { INSTANCE, DATASET_FAMILY } from 'src/test-data';

describe('[Metamodel][services] DatasetFamilyService', () => {
    let service: DatasetFamilyService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let datasetFamily: DatasetFamily;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                DatasetFamilyService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(DatasetFamilyService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        datasetFamily = { ...DATASET_FAMILY };
    }));

    it('#retrieveDatasetFamilyList() should request return an Observable<DatasetFamily[]> object', () => {
        service.retrieveDatasetFamilyList(instance.name).subscribe((res: DatasetFamily[]) => {
            expect(res).toEqual([datasetFamily]);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset-family`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: [datasetFamily]});
    });

    it('#addDatasetFamily() should request return an Observable<DatasetFamily> object', () => {
        service.addDatasetFamily(instance.name, datasetFamily).subscribe((res: DatasetFamily) => {
            expect(res).toEqual(datasetFamily);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset-family`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: datasetFamily});
    });

    it('#editDatasetFamily() should request return an Observable<DatasetFamily> object', () => {
        service.editDatasetFamily(instance.name, datasetFamily).subscribe((res: DatasetFamily) => {
            expect(res).toEqual(datasetFamily);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset-family/${datasetFamily.id}`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: datasetFamily});
    });

    it('#deleteDatasetFamily() should request return an Observable<{ message: string; }> object', () => {
        service.deleteDatasetFamily(instance.name, datasetFamily.id).subscribe((res: { message: string; }) => {
            expect(res).toEqual({message: `Dataset family with id ${datasetFamily.id} is removed!`});
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset-family/${datasetFamily.id}`;
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}` });
        mockRequest.flush({status: 200, data: {message: `Dataset family with id ${datasetFamily.id} is removed!`}});
    });
});
