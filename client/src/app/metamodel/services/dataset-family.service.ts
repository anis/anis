/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DatasetFamily } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class DatasetFamilyService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves the list of all available dataset families for an instance
     *
     * @param string instanceName - The name of the current instance
     * 
     * @return Observable<DatasetFamily[]>
     */
    retrieveDatasetFamilyList(instanceName: string): Observable<DatasetFamily[]> {
        return this.anisHttpClientService.get<DatasetFamily[]>(`/instance/${instanceName}/dataset-family`);
    }

    /**
     * Adds a new dataset family
     *
     * @param string instanceName - The name of the current instance
     * @param DatasetFamily newDatasetFamily - The new dataset family to be registred
     *
     * @return Observable<DatasetFamily>
     */
    addDatasetFamily(instanceName: string, newDatasetFamily: DatasetFamily): Observable<DatasetFamily> {
        return this.anisHttpClientService.post<DatasetFamily>(`/instance/${instanceName}/dataset-family`, newDatasetFamily);
    }

    /**
     * Modifies a dataset family
     *
     * @param string instanceName - The name of the current instance
     * @param DatasetFamily datasetFamily - The dataset family object to be edited
     *
     * @return Observable<DatasetFamily>
     */
    editDatasetFamily(instanceName: string, datasetFamily: DatasetFamily): Observable<DatasetFamily> {
        return this.anisHttpClientService.put<DatasetFamily>(`/instance/${instanceName}/dataset-family/${datasetFamily.id}`, datasetFamily);
    }

    /**
     * Removes a dataset family
     *
     * @param string instanceName - The name of the current instance
     * @param number datasetFamilyId - The id of the dataset family to be deleted
     *
     * @return Observable<object>
     */
    deleteDatasetFamily(instanceName: string, datasetFamilyId: number): Observable<object> {
        return this.anisHttpClientService.delete(`/instance/${instanceName}/dataset-family/${datasetFamilyId}`);
    }
}
