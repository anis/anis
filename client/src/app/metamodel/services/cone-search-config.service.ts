/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ConeSearchConfig } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class ConeSearchConfigService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves cone-search configuration for a dataset
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * 
     * @return Observable<ConeSearchConfig>
     */
    retrieveConeSearchConfig(instanceName: string, datasetName: string): Observable<ConeSearchConfig> {
        return this.anisHttpClientService.get<ConeSearchConfig>(`/instance/${instanceName}/dataset/${datasetName}/cone-search-config`);
    }

    /**
     * Adds a new cone-search configuration for the given dataset
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param ConeSearchConfig newConeSearchConfig - The new cone-search configuration to be registred
     *
     * @return Observable<ConeSearchConfig>
     */
    addConeSearchConfig(instanceName: string, datasetName: string, newConeSearchConfig: ConeSearchConfig): Observable<ConeSearchConfig> {
        return this.anisHttpClientService.post<ConeSearchConfig>(`/instance/${instanceName}/dataset/${datasetName}/cone-search-config`, newConeSearchConfig);
    }

    /**
     * Modifies cone-search configuration
     *
     * @param string instanceName - The name of the instance
     * @param string datasetName - The name of the current dataset
     * @param ConeSearchConfig coneSearchConfig - The cone-search cofiguration object to be edited
     *
     * @return Observable<ConeSearchConfig>
     */
    editConeSearchConfig(instanceName: string, datasetName: string, coneSearchConfig: ConeSearchConfig): Observable<ConeSearchConfig> {
        return this.anisHttpClientService.put<ConeSearchConfig>(`/instance/${instanceName}/dataset/${datasetName}/cone-search-config`, coneSearchConfig);
    }
}
