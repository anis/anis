/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Instance, InstanceGroup } from '../models';
import { AppConfigService } from 'src/app/app-config.service';
import { InstanceGroupService } from './instance-group.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { INSTANCE } from 'src/test-data';

describe('[Metamodel][services] InstanceGroupService', () => {
    let service: InstanceGroupService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let instanceGroup: InstanceGroup;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                InstanceGroupService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(InstanceGroupService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        instanceGroup = { ...INSTANCE.groups[0] };
    }));

    it('#retrieveInstanceGroupList() should request return an Observable<InstanceGroup[]> object', () => {
        service.retrieveInstanceGroupList(instance.name).subscribe((res: InstanceGroup[]) => {
            expect(res).toEqual([instanceGroup]);
        });
        const url = `http://localhost:8080/instance/${instance.name}/group`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: [instanceGroup]});
    });

    it('#addInstanceGroup() should request return an Observable<InstanceGroup> object', () => {
        service.addInstanceGroup(instance.name, instanceGroup).subscribe((res: InstanceGroup) => {
            expect(res).toEqual(instanceGroup);
        });
        const url = `http://localhost:8080/instance/${instance.name}/group`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: instanceGroup});
    });

    it('#editInstanceGroup() should request return an Observable<InstanceGroup> object', () => {
        service.editInstanceGroup(instance.name, instanceGroup).subscribe((res: InstanceGroup) => {
            expect(res).toEqual(instanceGroup);
        });
        const url = `http://localhost:8080/instance/${instance.name}/group/${instanceGroup.role}`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: instanceGroup});
    });

    it('#deleteInstanceGroup() should request return an Observable<{ message: string; }> object', () => {
        service.deleteInstanceGroup(instance.name, instanceGroup.role).subscribe((res: { message: string; }) => {
            expect(res).toEqual({message: `Instance-group with role ${instanceGroup.role} is removed!`});
        });
        const url = `http://localhost:8080/instance/${instance.name}/group/${instanceGroup.role}`;
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}` });
        mockRequest.flush({status: 200, data: {message: `Instance-group with role ${instanceGroup.role} is removed!`}});
    });
});
