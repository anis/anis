/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DesignConfig } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class DesignConfigService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves design configuration for an instance
     *
     * @param string instanceName - The name of the current instance
     * 
     * @return Observable<DesignConfig>
     */
    retrieveDesignConfig(instanceName: string): Observable<DesignConfig> {
        return this.anisHttpClientService.get<DesignConfig>(`/instance/${instanceName}/design-config`);
    }

    /**
     * Adds a new design configuration for the given instance.
     *
     * @param string instanceName - The name of the instance
     * @param DesignConfig newDesignConfig - The new design configuration to be registred
     *
     * @return Observable<DesignConfig>
     */
    addDesignConfig(instanceName: string, newDesignConfig: DesignConfig): Observable<DesignConfig> {
        return this.anisHttpClientService.post<DesignConfig>(`/instance/${instanceName}/design-config`, newDesignConfig);
    }

    /**
     * Modifies design configuration.
     *
     * @param string instanceName - The name of the instance
     * @param DesignConfig designConfig - The design cofiguration object to be edited
     *
     * @return Observable<DesignConfig>
     */
    editDesignConfig(instanceName: string, designConfig: DesignConfig): Observable<DesignConfig> {
        return this.anisHttpClientService.put<DesignConfig>(`/instance/${instanceName}/design-config`, designConfig);
    }
}
