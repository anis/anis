/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { MenuItem } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class MenuFamilyItemService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves the list of all available menu items for a menu family
     *
     * @param string instanceName - The name of the current instance
     * @param number menuFamilyId - The ID of the menu family
     * 
     * @return Observable<MenuItem[]>
     */
    retrieveMenuFamilyItemList(instanceName: string, menuFamilyId: number): Observable<MenuItem[]> {
        return this.anisHttpClientService.get<MenuItem[]>(`/instance/${instanceName}/menu-item/${menuFamilyId}/item`);
    }

    /**
     * Adds a new menu item into a menu family
     *
     * @param string instanceName - The name of the current instance
     * @param number menuFamilyId - The ID of the menu family
     * @param MenuItem newMenuItem - The new menu item to be registred
     *
     * @return Observable<MenuItem>
     */
    addMenuFamilyItem(instanceName: string, menuFamilyId: number, newMenuItem: MenuItem): Observable<MenuItem> {
        return this.anisHttpClientService.post<MenuItem>(`/instance/${instanceName}/menu-item/${menuFamilyId}/item?type=${newMenuItem.type}`, newMenuItem);
    }

    /**
     * Modifies a menu item from a menu family
     *
     * @param string instanceName - The name of the current instance
     * @param number menuFamilyId - The ID of the menu family
     * @param MenuItem menuItem - The menu item object to be edited
     *
     * @return Observable<MenuItem>
     */
    editMenuFamilyItem(instanceName: string, menuFamilyId: number, menuItem: MenuItem): Observable<MenuItem> {
        return this.anisHttpClientService.put<MenuItem>(`/instance/${instanceName}/menu-item/${menuFamilyId}/item/${menuItem.id}`, menuItem);
    }

    /**
     * Removes a menu item from ma menu family
     *
     * @param string instanceName - The name of the current instance
     * @param number menuFamilyId - The ID of the menu family
     * @param number menuItemId - The id of the menu item to be deleted
     *
     * @return Observable<object>
     */
    deleteMenuFamilyItem(instanceName: string, menuFamilyId: number, menuItemId: number): Observable<object> {
        return this.anisHttpClientService.delete(`/instance/${instanceName}/menu-item/${menuFamilyId}/item/${menuItemId}`);
    }
}
