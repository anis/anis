/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Instance, Dataset, OutputFamily, OutputCategory } from '../models';
import { AppConfigService } from 'src/app/app-config.service';
import { OutputCategoryService } from './output-category.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { INSTANCE, DATASET, OUTPUT_FAMILY, OUTPUT_CATEGORY } from 'src/test-data';

describe('[Metamodel][services] OutputCategoryService', () => {
    let service: OutputCategoryService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let dataset: Dataset;
    let outputFamily: OutputFamily;
    let outputCategory: OutputCategory;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                OutputCategoryService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(OutputCategoryService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        dataset = { ...DATASET };
        outputFamily = { ...OUTPUT_FAMILY };
        outputCategory = { ...OUTPUT_CATEGORY };
    }));

    it('#retrieveOutputCategoryList() should request return an Observable<OutputCategory[]> object', () => {
        service.retrieveOutputCategoryList(instance.name, dataset.name, outputFamily.id).subscribe((res: OutputCategory[]) => {
            expect(res).toEqual([outputCategory]);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/output-family/${outputFamily.id}/output-category`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: [outputCategory]});
    });

    it('#addOutputCategory() should request return an Observable<OutputCategory> object', () => {
        service.addOutputCategory(instance.name, dataset.name, outputFamily.id, outputCategory).subscribe((res: OutputCategory) => {
            expect(res).toEqual(outputCategory);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/output-family/${outputFamily.id}/output-category`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: outputCategory});
    });

    it('#editOutputCategory() should request return an Observable<OutputCategory> object', () => {
        service.editOutputCategory(instance.name, dataset.name, outputFamily.id, outputCategory).subscribe((res: OutputCategory) => {
            expect(res).toEqual(outputCategory);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/output-family/${outputFamily.id}/output-category/${outputCategory.id}`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: outputCategory});
    });

    it('#deleteOutputCategory() should request return an Observable<{ message: string; }> object', () => {
        service.deleteOutputCategory(instance.name, dataset.name, outputFamily.id, outputCategory.id).subscribe((res: { message: string; }) => {
            expect(res).toEqual({message: `Output category with id ${outputCategory.id} is removed!`});
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/output-family/${outputFamily.id}/output-category/${outputCategory.id}`;
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}` });
        mockRequest.flush({status: 200, data: {message: `Output category with id ${outputCategory.id} is removed!`}});
    });
});
