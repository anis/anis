/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Instance } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class InstanceService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves the list of all available instances
     * 
     * @returns Observable<Instance[]>
     */
    retrieveInstanceList(): Observable<Instance[]> {
        return this.anisHttpClientService.get<Instance[]>('/instance');
    }

    /**
     * Import a new instance
     * 
     * @param any fullInstance - The full instance configuration to import 
     * @returns Observable<Instance>
     */
    importInstance(fullInstance: any): Observable<Instance> {
        return this.anisHttpClientService.post<Instance>('/instance/import', fullInstance);
    }

    /**
     * Adds a new instance
     * 
     * @param Instance newInstance - The new instance to be registred
     * @returns Observable<Instance>
     */
    addInstance(newInstance: Instance): Observable<Instance> {
        return this.anisHttpClientService.post<Instance>('/instance', newInstance);
    }

    /**
     * Modifies an instance
     * 
     * @param Instance instance - The instance object to be edited
     * @returns Observable<Instance>
     */
    editInstance(instance: Instance): Observable<Instance> {
        return this.anisHttpClientService.put<Instance>(`/instance/${instance.name}`, instance);
    }

    /**
     * Deletes an instance
     * 
     * @param string instanceName - The name of the instance to be deleted
     * @returns Observable
     */
    deleteInstance(instanceName: string) {
        return this.anisHttpClientService.delete(`/instance/${instanceName}`);
    }
}
