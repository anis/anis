/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Instance, Dataset, OutputFamily } from '../models';
import { AppConfigService } from 'src/app/app-config.service';
import { OutputFamilyService } from './output-family.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { INSTANCE, DATASET, OUTPUT_FAMILY } from 'src/test-data';

describe('[Metamodel][services] OutputFamilyService', () => {
    let service: OutputFamilyService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let dataset: Dataset;
    let outputFamily: OutputFamily;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                OutputFamilyService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(OutputFamilyService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        dataset = { ...DATASET };
        outputFamily = { ...OUTPUT_FAMILY };
    }));

    it('#retrieveOutputFamilyList() should request return an Observable<OutputFamily[]> object', () => {
        service.retrieveOutputFamilyList(instance.name, dataset.name).subscribe((res: OutputFamily[]) => {
            expect(res).toEqual([outputFamily]);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/output-family`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: [outputFamily]});
    });

    it('#addOutputFamily() should request return an Observable<OutputFamily> object', () => {
        service.addOutputFamily(instance.name, dataset.name, outputFamily).subscribe((res: OutputFamily) => {
            expect(res).toEqual(outputFamily);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/output-family`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: outputFamily});
    });

    it('#editOutputFamily() should request return an Observable<OutputFamily> object', () => {
        service.editOutputFamily(instance.name, dataset.name, outputFamily).subscribe((res: OutputFamily) => {
            expect(res).toEqual(outputFamily);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/output-family/${outputFamily.id}`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: outputFamily});
    });

    it('#deleteOutputFamily() should request return an Observable<{ message: string; }> object', () => {
        service.deleteOutputFamily(instance.name, dataset.name, outputFamily.id).subscribe((res: { message: string; }) => {
            expect(res).toEqual({message: `Output family with id ${outputFamily.id} is removed!`});
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/output-family/${outputFamily.id}`;
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}` });
        mockRequest.flush({status: 200, data: {message: `Output family with id ${outputFamily.id} is removed!`}});
    });
});
