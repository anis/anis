/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { CriteriaFamily } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class CriteriaFamilyService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves the list of all available criteria families for a dataset
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * 
     * @return Observable<CriteriaFamily[]>
     */
    retrieveCriteriaFamilyList(instanceName: string, datasetName: string): Observable<CriteriaFamily[]> {
        return this.anisHttpClientService.get<CriteriaFamily[]>(`/instance/${instanceName}/dataset/${datasetName}/criteria-family`);
    }

    /**
     * Adds a new criteria family
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param CriteriaFamily newCriteriaFamily - The new criteria family to be registred
     *
     * @return Observable<CriteriaFamily>
     */
    addCriteriaFamily(instanceName: string, datasetName: string, newCriteriaFamily: CriteriaFamily): Observable<CriteriaFamily> {
        return this.anisHttpClientService.post<CriteriaFamily>(`/instance/${instanceName}/dataset/${datasetName}/criteria-family`, newCriteriaFamily);
    }

    /**
     * Modifies a criteria family
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param CriteriaFamily criteriaFamily - The criteria family object to be edited
     *
     * @return Observable<CriteriaFamily>
     */
    editCriteriaFamily(instanceName: string, datasetName: string, criteriaFamily: CriteriaFamily): Observable<CriteriaFamily> {
        return this.anisHttpClientService.put<CriteriaFamily>(`/instance/${instanceName}/dataset/${datasetName}/criteria-family/${criteriaFamily.id}`, criteriaFamily);
    }

    /**
     * Removes a criteria family
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param number criteriaFamilyId - The ID of the criteria family to be deleted
     *
     * @return Observable<object>
     */
    deleteCriteriaFamily(instanceName: string, datasetName: string, criteriaFamilyId: number): Observable<object> {
        return this.anisHttpClientService.delete(`/instance/${instanceName}/dataset/${datasetName}/criteria-family/${criteriaFamilyId}`);
    }
}
