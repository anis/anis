/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { MenuItem, Instance, MenuFamily } from '../models';
import { AppConfigService } from 'src/app/app-config.service';
import { MenuFamilyItemService } from './menu-family-item.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { INSTANCE, MENU_FAMILY, WEBPAGE } from 'src/test-data';

describe('[Metamodel][services] MenuFamilyItemService', () => {
    let service: MenuFamilyItemService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let menuFamily: MenuFamily;
    let webpage: MenuItem;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                MenuFamilyItemService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(MenuFamilyItemService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        menuFamily = { ...MENU_FAMILY };
        webpage = { ...WEBPAGE };
    }));

    it('#retrieveMenuFamilyItemList() should request return an Observable<MenuItem[]> object', () => {
        service.retrieveMenuFamilyItemList(instance.name, menuFamily.id).subscribe((res: MenuItem[]) => {
            expect(res).toEqual([webpage]);
        });
        const url = `http://localhost:8080/instance/${instance.name}/menu-item/${menuFamily.id}/item`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: [webpage]});
    });

    it('#addMenuFamilyItem() should request return an Observable<MenuItem> object', () => {
        service.addMenuFamilyItem(instance.name, menuFamily.id, webpage).subscribe((res: MenuItem) => {
            expect(res).toEqual(webpage);
        });
        const url = `http://localhost:8080/instance/${instance.name}/menu-item/${menuFamily.id}/item?type=${webpage.type}`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: webpage});
    });

    it('#editMenuFamilyItem() should request return an Observable<MenuItem> object', () => {
        service.editMenuFamilyItem(instance.name, menuFamily.id, webpage).subscribe((res: MenuItem) => {
            expect(res).toEqual(webpage);
        });
        const url = `http://localhost:8080/instance/${instance.name}/menu-item/${menuFamily.id}/item/${webpage.id}`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: webpage});
    });

    it('#deleteMenuFamilyItem() should request return an Observable<{ message: string; }> object', () => {
        service.deleteMenuFamilyItem(instance.name, menuFamily.id, webpage.id).subscribe((res: { message: string; }) => {
            expect(res).toEqual({message: `Menu item with id ${webpage.id} is removed!`});
        });
        const url = `http://localhost:8080/instance/${instance.name}/menu-item/${menuFamily.id}/item/${webpage.id}`;
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}` });
        mockRequest.flush({status: 200, data: {message: `Menu item with id ${webpage.id} is removed!`}});
    });
});
