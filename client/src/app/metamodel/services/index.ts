/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { InstanceService } from './instance.service';
import { DesignConfigService } from './design-config.service';
import { InstanceGroupService } from './instance-group.service';
import { DatabaseService } from './database.service';
import { DatasetFamilyService } from './dataset-family.service';
import { MenuItemService } from './menu-item.service';
import { MenuFamilyItemService } from './menu-family-item.service';
import { DatasetService } from './dataset.service';
import { DatasetGroupService } from './dataset-group.service';
import { CriteriaFamilyService } from './criteria-family.service';
import { OutputFamilyService } from './output-family.service';
import { OutputCategoryService } from './output-category.service';
import { AttributeService } from './attribute.service';
import { ImageService } from './image.service';
import { FileService } from './file.service';
import { ConeSearchConfigService } from './cone-search-config.service';
import { DetailConfigService } from './detail-config.service';
import { AliasConfigService } from './alias-config.service';

export const metamodelServices = [
    InstanceService,
    DesignConfigService,
    InstanceGroupService,
    DatabaseService,
    DatasetFamilyService,
    MenuItemService,
    MenuFamilyItemService,
    DatasetService,
    DatasetGroupService,
    CriteriaFamilyService,
    OutputFamilyService,
    OutputCategoryService,
    AttributeService,
    ImageService,
    FileService,
    ConeSearchConfigService,
    DetailConfigService,
    AliasConfigService
];
