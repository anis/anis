/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { OutputCategory } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class OutputCategoryService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves the list of all available output categories for an output family
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param number outputFamilyId - The ID of the current output family
     * 
     * @return Observable<OutputCategory[]>
     */
    retrieveOutputCategoryList(instanceName: string, datasetName: string, outputFamilyId: number): Observable<OutputCategory[]> {
        return this.anisHttpClientService.get<OutputCategory[]>(`/instance/${instanceName}/dataset/${datasetName}/output-family/${outputFamilyId}/output-category`);
    }

    /**
     * Adds a new output category
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param number outputFamilyId - The ID of the current output family
     * @param OutputCategory newOutputCategory - The new output family to be registred
     *
     * @return Observable<OutputCategory>
     */
    addOutputCategory(instanceName: string, datasetName: string, outputFamilyId: number, newOutputCategory: OutputCategory): Observable<OutputCategory> {
        return this.anisHttpClientService.post<OutputCategory>(`/instance/${instanceName}/dataset/${datasetName}/output-family/${outputFamilyId}/output-category`, newOutputCategory);
    }

    /**
     * Modifies a output family
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param number outputFamilyId - The ID of the current output family
     * @param OutputCategory outputCategory - The output category object to be edited
     *
     * @return Observable<OutputCategory>
     */
    editOutputCategory(instanceName: string, datasetName: string, outputFamilyId: number, outputCategory: OutputCategory): Observable<OutputCategory> {
        return this.anisHttpClientService.put<OutputCategory>(`/instance/${instanceName}/dataset/${datasetName}/output-family/${outputFamilyId}/output-category/${outputCategory.id}`, outputCategory);
    }

    /**
     * Removes a output category
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param number outputFamilyId - The ID of the current output family
     * @param number outputCategoryId - The ID of the output category to be deleted
     *
     * @return Observable<object>
     */
    deleteOutputCategory(instanceName: string, datasetName: string, outputFamilyId: number, outputCategoryId: number): Observable<object> {
        return this.anisHttpClientService.delete(`/instance/${instanceName}/dataset/${datasetName}/output-family/${outputFamilyId}/output-category/${outputCategoryId}`);
    }
}
