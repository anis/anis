/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { MenuItem } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class MenuItemService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves the list of all available menu items for an instance
     *
     * @param string instanceName - The name of the current instance
     * 
     * @return Observable<MenuItem[]>
     */
    retrieveMenuItemList(instanceName: string): Observable<MenuItem[]> {
        return this.anisHttpClientService.get<MenuItem[]>(`/instance/${instanceName}/menu-item`);
    }

    /**
     * Adds a new menu item
     *
     * @param string instanceName - The name of the current instance
     * @param MenuItem newMenuItem - The new menu item to be registred
     *
     * @return Observable<MenuItem>
     */
    addMenuItem(instanceName: string, newMenuItem: MenuItem): Observable<MenuItem> {
        return this.anisHttpClientService.post<MenuItem>(`/instance/${instanceName}/menu-item?type=${newMenuItem.type}`, newMenuItem);
    }

    /**
     * Modifies a menu item
     *
     * @param string instanceName - The name of the current instance
     * @param MenuItem menuItem - The menu item object to be edited
     *
     * @return Observable<MenuItem>
     */
    editMenuItem(instanceName: string, menuItem: MenuItem): Observable<MenuItem> {
        return this.anisHttpClientService.put<MenuItem>(`/instance/${instanceName}/menu-item/${menuItem.id}`, menuItem);
    }

    /**
     * Removes a menu item
     *
     * @param string instanceName - The name of the current instance
     * @param number menuItemId - The id of the menu item to be deleted
     *
     * @return Observable<object>
     */
    deleteMenuItem(instanceName: string, menuItemId: number): Observable<object> {
        return this.anisHttpClientService.delete(`/instance/${instanceName}/menu-item/${menuItemId}`);
    }
}
