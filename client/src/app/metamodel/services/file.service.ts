/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { File } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class FileService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves the list of all files for a dataset
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * 
     * @return Observable<File[]>
     */
    retrieveFileList(instanceName: string, datasetName: string): Observable<File[]> {
        return this.anisHttpClientService.get<File[]>(`/instance/${instanceName}/dataset/${datasetName}/file`);
    }

    /**
     * Adds a new file
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param File newFile - The new file to be registred
     *
     * @return Observable<File>
     */
    addFile(instanceName: string, datasetName: string, newFile: File): Observable<File> {
        return this.anisHttpClientService.post<File>(`/instance/${instanceName}/dataset/${datasetName}/file`, newFile);
    }

    /**
     * Modifies a file
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param File file - The file object to be edited
     *
     * @return Observable<File>
     */
    editFile(instanceName: string, datasetName: string, file: File): Observable<File> {
        return this.anisHttpClientService.put<File>(`/instance/${instanceName}/dataset/${datasetName}/file/${file.id}`, file);
    }

    /**
     * Removes a file
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param number fileId - The ID of the file to be deleted
     *
     * @return Observable<object>
     */
    deleteFile(instanceName: string, datasetName: string, fileId: number): Observable<object> {
        return this.anisHttpClientService.delete(`/instance/${instanceName}/dataset/${datasetName}/file/${fileId}`);
    }
}
