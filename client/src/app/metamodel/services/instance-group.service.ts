/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { InstanceGroup } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class InstanceGroupService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves the list of all available groups for an instance
     *
     * @param string instanceName - The name of the current instance
     * 
     * @return Observable<InstanceGroup[]>
     */
    retrieveInstanceGroupList(instanceName: string): Observable<InstanceGroup[]> {
        return this.anisHttpClientService.get<InstanceGroup[]>(`/instance/${instanceName}/group`);
    }

    /**
     * Adds a new instance group
     *
     * @param string instanceName - The name of the current instance
     * @param InstanceGroup newInstanceGroup - The new instance group to be registred
     *
     * @return Observable<InstanceGroup>
     */
    addInstanceGroup(instanceName: string, newInstanceGroup: InstanceGroup): Observable<InstanceGroup> {
        return this.anisHttpClientService.post<InstanceGroup>(`/instance/${instanceName}/group`, newInstanceGroup);
    }

    /**
     * Modifies an instance group
     *
     * @param string instanceName - The name of the current instance
     * @param InstanceGroup instanceGroup - The instance group object to be edited
     *
     * @return Observable<InstanceGroup>
     */
    editInstanceGroup(instanceName: string, instanceGroup: InstanceGroup): Observable<InstanceGroup> {
        return this.anisHttpClientService.put<InstanceGroup>(`/instance/${instanceName}/group/${instanceGroup.role}`, instanceGroup);
    }

    /**
     * Removes an instance group
     *
     * @param string instanceName - The name of the current instance
     * @param string instanceGroupRole - The role of the instance group to be deleted
     *
     * @return Observable<object>
     */
    deleteInstanceGroup(instanceName: string, instanceGroupRole: string): Observable<object> {
        return this.anisHttpClientService.delete(`/instance/${instanceName}/group/${instanceGroupRole}`);
    }
}
