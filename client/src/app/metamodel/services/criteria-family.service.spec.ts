/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Instance, Dataset, CriteriaFamily } from '../models';
import { AppConfigService } from 'src/app/app-config.service';
import { CriteriaFamilyService } from './criteria-family.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { INSTANCE, DATASET, CRITERIA_FAMILY } from 'src/test-data';

describe('[Metamodel][services] CriteriaFamilyService', () => {
    let service: CriteriaFamilyService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let dataset: Dataset;
    let criteriaFamily: CriteriaFamily;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                CriteriaFamilyService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(CriteriaFamilyService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        dataset = { ...DATASET };
        criteriaFamily = { ...CRITERIA_FAMILY };
    }));

    it('#retrieveCriteriaFamilyList() should request return an Observable<CriteriaFamily[]> object', () => {
        service.retrieveCriteriaFamilyList(instance.name, dataset.name).subscribe((res: CriteriaFamily[]) => {
            expect(res).toEqual([criteriaFamily]);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/criteria-family`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: [criteriaFamily]});
    });

    it('#addCriteriaFamily() should request return an Observable<CriteriaFamily> object', () => {
        service.addCriteriaFamily(instance.name, dataset.name, criteriaFamily).subscribe((res: CriteriaFamily) => {
            expect(res).toEqual(criteriaFamily);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/criteria-family`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: criteriaFamily});
    });

    it('#editCriteriaFamily() should request return an Observable<CriteriaFamily> object', () => {
        service.editCriteriaFamily(instance.name, dataset.name, criteriaFamily).subscribe((res: CriteriaFamily) => {
            expect(res).toEqual(criteriaFamily);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/criteria-family/${criteriaFamily.id}`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: criteriaFamily});
    });

    it('#deleteCriteriaFamily() should request return an Observable<{ message: string; }> object', () => {
        service.deleteCriteriaFamily(instance.name, dataset.name, criteriaFamily.id).subscribe((res: { message: string; }) => {
            expect(res).toEqual({message: `Criteria family with id ${criteriaFamily.id} is removed!`});
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/criteria-family/${criteriaFamily.id}`;
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}` });
        mockRequest.flush({status: 200, data: {message: `Criteria family with id ${criteriaFamily.id} is removed!`}});
    });
});
