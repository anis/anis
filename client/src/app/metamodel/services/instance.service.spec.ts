/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AppConfigService } from '../../app-config.service';
import { InstanceService } from './instance.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { Instance } from '../models';
import { INSTANCE } from 'src/test-data';

describe('[Metamodel][services] InstanceService', () => {
    let service: InstanceService;
    let httpController: HttpTestingController;
    let instance: Instance;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                InstanceService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(InstanceService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        instance = { ...INSTANCE };
    }));

    it('#retrieveInstanceList() should request return an Observable<Instance[]> object', () => {
        service.retrieveInstanceList().subscribe((res: Instance[]) => {
            expect(res).toEqual([instance]);
        });
        const url = 'http://localhost:8080/instance';
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}`, });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: [instance]});
    });

    it('#importInstance() should request return an Observable<InstanceGroup> object', () => {
        service.importInstance(instance).subscribe((res: Instance) => {
            expect(res).toEqual(instance);
        });
        const url = 'http://localhost:8080/instance/import';
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 200, data: instance});
    });

    it('#addInstance() should request return an Observable<InstanceGroup> object', () => {
        service.addInstance(instance).subscribe((res: Instance) => {
            expect(res).toEqual(instance);
        });
        const url = 'http://localhost:8080/instance';
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: instance});
    });

    it('#editInstance() should request return an Observable<Instance> object', () => {
        service.editInstance(instance).subscribe((res: Instance) => {
            expect(res).toEqual(instance);
        });
        const url = `http://localhost:8080/instance/${instance.name}`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: instance});
    });

    it('#deleteInstance() should request return an Observable<{ message: string; }> object', () => {
        service.deleteInstance(instance.name).subscribe((res: { message: string; }) => {
            expect(res).toEqual({message: `Instance with name ${instance.name} is removed!`});
        });
        const url = `http://localhost:8080/instance/${instance.name}`;
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}` });

        mockRequest.flush({status: 200, data: {message: `Instance with name ${instance.name} is removed!`}});
    });
});
