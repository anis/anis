/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { DatasetGroup } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class DatasetGroupService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves the list of all available groups for a dataset
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * 
     * @return Observable<DatasetGroup[]>
     */
    retrieveDatasetGroupList(instanceName: string, datasetName: string): Observable<DatasetGroup[]> {
        return this.anisHttpClientService.get<DatasetGroup[]>(`/instance/${instanceName}/dataset/${datasetName}/group`);
    }

    /**
     * Adds a new dataset group
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param DatasetGroup newDatasetGroup - The new dataset group to be registred
     *
     * @return Observable<DatasetGroup>
     */
    addDatasetGroup(instanceName: string, datasetName: string, newDatasetGroup: DatasetGroup): Observable<DatasetGroup> {
        return this.anisHttpClientService.post<DatasetGroup>(`/instance/${instanceName}/dataset/${datasetName}/group`, newDatasetGroup);
    }

    /**
     * Modifies a dataset group
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param DatasetGroup datasetGroup - The dataset group object to be edited
     *
     * @return Observable<DatasetGroup>
     */
    editDatasetGroup(instanceName: string, datasetName: string, datasetGroup: DatasetGroup): Observable<DatasetGroup> {
        return this.anisHttpClientService.put<DatasetGroup>(`/instance/${instanceName}/dataset/${datasetName}/group/${datasetGroup.role}`, datasetGroup);
    }

    /**
     * Removes a dataset group
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param string datasetGroupRole - The role of the dataset group to be deleted
     *
     * @return Observable<object>
     */
    deleteDatasetGroup(instanceName: string, datasetName: string, datasetGroupRole: string): Observable<object> {
        return this.anisHttpClientService.delete(`/instance/${instanceName}/dataset/${datasetName}/group/${datasetGroupRole}`);
    }
}
