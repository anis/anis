/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Instance, Dataset, Image } from '../models';
import { AppConfigService } from 'src/app/app-config.service';
import { ImageService } from './image.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { INSTANCE, DATASET, IMAGE } from 'src/test-data';

describe('[Metamodel][services] ImageService', () => {
    let service: ImageService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let dataset: Dataset;
    let image: Image;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                ImageService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(ImageService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        dataset = { ...DATASET };
        image = { ...IMAGE };
    }));

    it('#retrieveImageList() should request return an Observable<Image[]> object', () => {
        service.retrieveImageList(instance.name, dataset.name).subscribe((res: Image[]) => {
            expect(res).toEqual([image]);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/image`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: [image]});
    });

    it('#addImage() should request return an Observable<Image> object', () => {
        service.addImage(instance.name, dataset.name, image).subscribe((res: Image) => {
            expect(res).toEqual(image);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/image`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: image});
    });

    it('#editImage() should request return an Observable<Image> object', () => {
        service.editImage(instance.name, dataset.name, image).subscribe((res: Image) => {
            expect(res).toEqual(image);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/image/${image.id}`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: image});
    });

    it('#deleteImage() should request return an Observable<{ message: string; }> object', () => {
        service.deleteImage(instance.name, dataset.name, image.id).subscribe((res: { message: string; }) => {
            expect(res).toEqual({message: `Image with id ${image.id} is removed!`});
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/image/${image.id}`;
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}` });
        mockRequest.flush({status: 200, data: {message: `Image with id ${image.id} is removed!`}});
    });
});
