/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Attribute } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class AttributeService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves the list of all available attributes for a dataset
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * 
     * @return Observable<Attribute[]>
     */
    retrieveAttributeList(instanceName: string, datasetName: string): Observable<Attribute[]> {
        return this.anisHttpClientService.get<Attribute[]>(`/instance/${instanceName}/dataset/${datasetName}/attribute`);
    }

    /**
     * Adds a new attribute
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param Attribute newAttribute - The new attribute to be registred
     *
     * @return Observable<Attribute>
     */
    addAttribute(instanceName: string, datasetName: string, newAttribute: Attribute): Observable<Attribute> {
        return this.anisHttpClientService.post<Attribute>(`/instance/${instanceName}/dataset/${datasetName}/attribute`, newAttribute);
    }

    /**
     * Modifies an attribute
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param Attribute attribute - The attribute object to be edited
     *
     * @return Observable<Attribute>
     */
    editAttribute(instanceName: string, datasetName: string, attribute: Attribute): Observable<Attribute> {
        return this.anisHttpClientService.put<Attribute>(`/instance/${instanceName}/dataset/${datasetName}/attribute/${attribute.id}`, attribute);
    }

    /**
     * Removes an attribute
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param number attributeId - The ID of the attribute to be deleted
     *
     * @return Observable<object>
     */
    deleteAttribute(instanceName: string, datasetName: string, attributeId: number): Observable<object> {
        return this.anisHttpClientService.delete(`/instance/${instanceName}/dataset/${datasetName}/attribute/${attributeId}`);
    }
}
