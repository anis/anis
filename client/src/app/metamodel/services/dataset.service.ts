/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Dataset } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class DatasetService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves one dataset with instance name and dataset name
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName  - The name of the dataset to retrieve
     * 
     * @return Observable<Dataset>
     */
    retrieveDataset(instanceName: string, datasetName: string): Observable<Dataset> {
        return this.anisHttpClientService.get<Dataset>(`/instance/${instanceName}/dataset/${datasetName}`);
    }
    
    /**
     * Retrieves the list of all available datasets for an instance
     *
     * @param string instanceName - The name of the current instance
     * 
     * @return Observable<Dataset[]>
     */
    retrieveDatasetList(instanceName: string): Observable<Dataset[]> {
        return this.anisHttpClientService.get<Dataset[]>(`/instance/${instanceName}/dataset`);
    }

    /**
     * Import a new dataset
     * 
     * @param any fullDataset - The full dataset configuration to import 
     * @returns Observable<Dataset>
     */
    importDataset(instanceName: string, fullDataset: any): Observable<Dataset> {
        return this.anisHttpClientService.post<Dataset>(`/instance/${instanceName}/dataset/import`, fullDataset);
    }

    /**
     * Adds a new dataset
     *
     * @param string instanceName - The name of the current instance
     * @param Dataset newDataset - The new dataset to be registred
     *
     * @return Observable<Dataset>
     */
    addDataset(instanceName: string, newDataset: Dataset): Observable<Dataset> {
        return this.anisHttpClientService.post<Dataset>(`/instance/${instanceName}/dataset`, newDataset);
    }

    /**
     * Modifies a dataset
     *
     * @param string instanceName - The name of the current instance
     * @param Dataset dataset - The dataset object to be edited
     *
     * @return Observable<Dataset>
     */
    editDataset(instanceName: string, dataset: Dataset): Observable<Dataset> {
        return this.anisHttpClientService.put<Dataset>(`/instance/${instanceName}/dataset/${dataset.name}`, dataset);
    }

    /**
     * Removes a dataset
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the dataset to be deleted
     *
     * @return Observable<object>
     */
    deleteDataset(instanceName: string, datasetName: string): Observable<object> {
        return this.anisHttpClientService.delete(`/instance/${instanceName}/dataset/${datasetName}`);
    }
}
