/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { OutputFamily } from '../models';
import { AnisHttpClientService } from '../anis-http-client.service';

@Injectable()
export class OutputFamilyService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves the list of all available output families for a dataset
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * 
     * @return Observable<OutputFamily[]>
     */
    retrieveOutputFamilyList(instanceName: string, datasetName: string): Observable<OutputFamily[]> {
        return this.anisHttpClientService.get<OutputFamily[]>(`/instance/${instanceName}/dataset/${datasetName}/output-family`);
    }

    /**
     * Adds a new output family
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param OutputFamily newOutputFamily - The new output family to be registred
     *
     * @return Observable<OutputFamily>
     */
    addOutputFamily(instanceName: string, datasetName: string, newOutputFamily: OutputFamily): Observable<OutputFamily> {
        return this.anisHttpClientService.post<OutputFamily>(`/instance/${instanceName}/dataset/${datasetName}/output-family`, newOutputFamily);
    }

    /**
     * Modifies a output family
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param OutputFamily outputFamily - The output family object to be edited
     *
     * @return Observable<OutputFamily>
     */
    editOutputFamily(instanceName: string, datasetName: string, outputFamily: OutputFamily): Observable<OutputFamily> {
        return this.anisHttpClientService.put<OutputFamily>(`/instance/${instanceName}/dataset/${datasetName}/output-family/${outputFamily.id}`, outputFamily);
    }

    /**
     * Removes a output family
     *
     * @param string instanceName - The name of the current instance
     * @param string datasetName - The name of the current dataset
     * @param number outputFamilyId - The ID of the output family to be deleted
     *
     * @return Observable<object>
     */
    deleteOutputFamily(instanceName: string, datasetName: string, outputFamilyId: number): Observable<object> {
        return this.anisHttpClientService.delete(`/instance/${instanceName}/dataset/${datasetName}/output-family/${outputFamilyId}`);
    }
}
