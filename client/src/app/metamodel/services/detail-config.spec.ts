/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AppConfigService } from '../../app-config.service';
import { DetailConfigService } from './detail-config.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { Instance, Dataset, DetailConfig } from '../models';
import { INSTANCE, DATASET, DETAIL_CONFIG } from 'src/test-data';

describe('[Metamodel][services] DetailConfigService', () => {
    let service: DetailConfigService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let dataset: Dataset;
    let detailConfig: DetailConfig;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                DetailConfigService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(DetailConfigService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);
        instance = { ...INSTANCE };
        dataset = { ...DATASET };
        detailConfig = { ...DETAIL_CONFIG };
    }));

    it('#retrieveDetailConfig() should request return an Observable<DetailConfig> object', () => {
        service.retrieveDetailConfig(instance.name, dataset.name).subscribe((res: DetailConfig) => {
            expect(res).toEqual(detailConfig);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/detail-config`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: detailConfig});
    });

    it('#addDetailConfig() should request return an Observable<DetailConfig> object', () => {
        service.addDetailConfig(instance.name, dataset.name, detailConfig).subscribe((res: DetailConfig) => {
            expect(res).toEqual(detailConfig);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/detail-config`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: detailConfig});
    });

    it('#editDetailConfig() should request return an Observable<DetailConfig> object', () => {
        service.editDetailConfig(instance.name, dataset.name, detailConfig).subscribe((res: DetailConfig) => {
            expect(res).toEqual(detailConfig);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/detail-config`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: detailConfig});
    });
});
