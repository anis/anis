/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import { TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { Instance, Dataset, File } from '../models';
import { AppConfigService } from 'src/app/app-config.service';
import { FileService } from './file.service';
import { AnisHttpClientService } from '../anis-http-client.service';
import { INSTANCE, DATASET, FILE } from 'src/test-data';

describe('[Metamodel][services] FileService', () => {
    let service: FileService;
    let httpController: HttpTestingController;
    let instance: Instance;
    let dataset: Dataset;
    let file: File;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                FileService,
                {
                    provide: AppConfigService, useValue: {
                        apiUrl: 'http://localhost:8080',

                    }
                },
                AnisHttpClientService
            ]
        });
        service = TestBed.inject(FileService);
        TestBed.inject(AppConfigService);
        httpController = TestBed.inject(HttpTestingController);

        instance = { ...INSTANCE };
        dataset = { ...DATASET };
        file = { ...FILE };
    }));

    it('#retrieveFileList() should request return an Observable<File[]> object', () => {
        service.retrieveFileList(instance.name, dataset.name).subscribe((res: File[]) => {
            expect(res).toEqual([file]);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/file`;
        const mockRequest = httpController.expectOne({ method: 'GET', url: `${url}` });
        expect(mockRequest.request.method).toEqual('GET');
        expect(mockRequest.request.responseType).toEqual('json');
        mockRequest.flush({status: 200, data: [file]});
    });

    it('#addFile() should request return an Observable<File> object', () => {
        service.addFile(instance.name, dataset.name, file).subscribe((res: File) => {
            expect(res).toEqual(file);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/file`;
        const mockRequest = httpController.expectOne({ method: 'POST', url: `${url}` });

        mockRequest.flush({status: 201, data: file});
    });

    it('#editFile() should request return an Observable<File> object', () => {
        service.editFile(instance.name, dataset.name, file).subscribe((res: File) => {
            expect(res).toEqual(file);
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/file/${file.id}`;
        const mockRequest = httpController.expectOne({ method: 'PUT', url: `${url}` });
        mockRequest.flush({status: 200, data: file});
    });

    it('#deleteFile() should request return an Observable<{ message: string; }> object', () => {
        service.deleteFile(instance.name, dataset.name, file.id).subscribe((res: { message: string; }) => {
            expect(res).toEqual({message: `File with id ${file.id} is removed!`});
        });
        const url = `http://localhost:8080/instance/${instance.name}/dataset/${dataset.name}/file/${file.id}`;
        const mockRequest = httpController.expectOne({ method: 'DELETE', url: `${url}` });
        mockRequest.flush({status: 200, data: {message: `File with id ${file.id} is removed!`}});
    });
});
