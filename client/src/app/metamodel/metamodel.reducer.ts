/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { combineReducers, createFeatureSelector } from '@ngrx/store';

import { RouterReducerState } from 'src/app/custom-route-serializer';
import * as instance from './reducers/instance.reducer';
import * as designConfig from './reducers/design-config.reducer';
import * as instanceGroup from './reducers/instance-group.reducer';
import * as database from './reducers/database.reducer';
import * as datasetFamily from './reducers/dataset-family.reducer';
import * as menuItem from './reducers/menu-item.reducer';
import * as menuFamilyItem from './reducers/menu-family-item.reducer';
import * as dataset from './reducers/dataset.reducer';
import * as datasetGroup from './reducers/dataset-group.reducer';
import * as criteriaFamily from './reducers/criteria-family.reducer';
import * as outputFamily from './reducers/output-family.reducer';
import * as outputCategory from './reducers/output-category.reducer';
import * as attribute from './reducers/attribute.reducer';
import * as image from './reducers/image.reducer';
import * as file from './reducers/file.reducer';
import * as coneSearchConfig from './reducers/cone-search-config.reducer';
import * as detailConfig from './reducers/detail-config.reducer';
import * as aliasConfig from './reducers/alias-config.reducer';

export interface State {
    instance: instance.State;
    designConfig: designConfig.State;
    instanceGroup: instanceGroup.State;
    database: database.State;
    datasetFamily: datasetFamily.State;
    menuItem: menuItem.State;
    menuFamilyItem: menuFamilyItem.State;
    dataset: dataset.State;
    datasetGroup: datasetGroup.State;
    criteriaFamily: criteriaFamily.State;
    outputFamily: outputFamily.State;
    outputCategory: outputCategory.State;
    attribute: attribute.State;
    image: image.State;
    file: file.State;
    coneSearchConfig: coneSearchConfig.State;
    detailConfig: detailConfig.State;
    aliasConfig: aliasConfig.State;
}

const reducers = {
    instance: instance.instanceReducer,
    designConfig: designConfig.designConfigReducer,
    instanceGroup: instanceGroup.instanceGroupReducer,
    database: database.databaseReducer,
    datasetFamily: datasetFamily.datasetFamilyReducer,
    menuItem: menuItem.menuItemReducer,
    menuFamilyItem: menuFamilyItem.menuFamilyItemReducer,
    dataset: dataset.datasetReducer,
    datasetGroup: datasetGroup.datasetGroupReducer,
    criteriaFamily: criteriaFamily.criteriaFamilyReducer,
    outputFamily: outputFamily.outputFamilyReducer,
    outputCategory: outputCategory.outputCategoryReducer,
    attribute: attribute.attributeReducer,
    image: image.imageReducer,
    file: file.fileReducer,
    coneSearchConfig: coneSearchConfig.coneSearchConfigReducer,
    detailConfig: detailConfig.detailConfigReducer,
    aliasConfig: aliasConfig.aliasConfigReducer
};

export const metamodelReducer = combineReducers(reducers);
export const getMetamodelState = createFeatureSelector<State>('metamodel');
export const selectRouterState = createFeatureSelector<RouterReducerState>('router');
