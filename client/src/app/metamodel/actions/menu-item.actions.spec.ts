/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as menuItemActions from './menu-item.actions';
import { MENU_ITEM_LIST } from 'src/test-data';

describe('[Metamodel][Actions] loadMenuItemList actions', () => {
    it('should create loadMenuItemList action', () => {
        const action = menuItemActions.loadMenuItemList();
        expect(action).toEqual({ type: '[Metamodel] Load Menu Item List' });
    });

    it('should create loadMenuItemListSuccess action', () => {
        const action = menuItemActions.loadMenuItemListSuccess({ menuItems: MENU_ITEM_LIST });
        expect(action).toEqual({
            type: '[Metamodel] Load Menu Item List Success',
            menuItems: MENU_ITEM_LIST
        });
    });

    it('should create loadMenuItemListFail action', () => {
        const action = menuItemActions.loadMenuItemListFail();
        expect(action).toEqual({ type: '[Metamodel] Load Menu Item List Fail' });
    });

    it('should create addMenuItem action', () => {
        const action = menuItemActions.addMenuItem({ menuItem: MENU_ITEM_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Add Menu Item',
            menuItem: MENU_ITEM_LIST[0]
        });
    });

    it('should create addMenuItemSuccess action', () => {
        const action = menuItemActions.addMenuItemSuccess({ menuItem: MENU_ITEM_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Add Menu Item Success',
            menuItem: MENU_ITEM_LIST[0]
        });
    });

    it('should create addMenuItemFail action', () => {
        const action = menuItemActions.addMenuItemFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Menu Item Fail'
        });
    });

    it('should create editMenuItem action', () => {
        const action = menuItemActions.editMenuItem({ menuItem: MENU_ITEM_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Edit Menu Item',
            menuItem: MENU_ITEM_LIST[0]
        });
    });

    it('should create editMenuItemSuccess action', () => {
        const action = menuItemActions.editMenuItemSuccess({ menuItem: MENU_ITEM_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Edit Menu Item Success',
            menuItem: MENU_ITEM_LIST[0]
        });
    });

    it('should create editMenuItemFail action', () => {
        const action = menuItemActions.editMenuItemFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Menu Item Fail'
        });
    });

    it('should create deleteMenuItem action', () => {
        const action = menuItemActions.deleteMenuItem({ menuItem: MENU_ITEM_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Delete Menu Item',
            menuItem: MENU_ITEM_LIST[0]
        });
    });

    it('should create deleteMenuItemSuccess action', () => {
        const action = menuItemActions.deleteMenuItemSuccess({ menuItem: MENU_ITEM_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Delete Menu Item Success',
            menuItem: MENU_ITEM_LIST[0]
        });
    });

    it('should create deleteMenuItemFail action', () => {
        const action = menuItemActions.deleteMenuItemFail();
        expect(action).toEqual({
            type: '[Metamodel] Delete Menu Item Fail'
        });
    });
});
