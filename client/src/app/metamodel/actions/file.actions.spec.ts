/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as fileActions from './file.actions';
import { FILE } from 'src/test-data';

describe('[Metamodel][Actions] loadFileList actions', () => {
    it('should create loadFileList action', () => {
        const action = fileActions.loadFileList();
        expect(action).toEqual({ type: '[Metamodel] Load File List' });
    });

    it('should create loadFileListSuccess action', () => {
        const action = fileActions.loadFileListSuccess({ files: [FILE] });
        expect(action).toEqual({
            type: '[Metamodel] Load File List Success',
            files: [FILE]
        });
    });

    it('should create loadFileListFail action', () => {
        const action = fileActions.loadFileListFail();
        expect(action).toEqual({ type: '[Metamodel] Load File List Fail' });
    });

    it('should create addFile action', () => {
        const action = fileActions.addFile({ file: FILE });
        expect(action).toEqual({
            type: '[Metamodel] Add File',
            file: FILE
        });
    });

    it('should create addFileSuccess action', () => {
        const action = fileActions.addFileSuccess({ file: FILE });
        expect(action).toEqual({
            type: '[Metamodel] Add File Success',
            file: FILE
        });
    });

    it('should create addFileFail action', () => {
        const action = fileActions.addFileFail();
        expect(action).toEqual({
            type: '[Metamodel] Add File Fail'
        });
    });

    it('should create editFile action', () => {
        const action = fileActions.editFile({ file: FILE });
        expect(action).toEqual({
            type: '[Metamodel] Edit File',
            file: FILE
        });
    });

    it('should create editFileSuccess action', () => {
        const action = fileActions.editFileSuccess({ file: FILE });
        expect(action).toEqual({
            type: '[Metamodel] Edit File Success',
            file: FILE
        });
    });

    it('should create editFileFail action', () => {
        const action = fileActions.editFileFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit File Fail'
        });
    });

    it('should create deleteFile action', () => {
        const action = fileActions.deleteFile({ file: FILE });
        expect(action).toEqual({
            type: '[Metamodel] Delete File',
            file: FILE
        });
    });

    it('should create deleteFileSuccess action', () => {
        const action = fileActions.deleteFileSuccess({ file: FILE });
        expect(action).toEqual({
            type: '[Metamodel] Delete File Success',
            file: FILE
        });
    });

    it('should create deleteFileFail action', () => {
        const action = fileActions.deleteFileFail();
        expect(action).toEqual({
            type: '[Metamodel] Delete File Fail'
        });
    });
});
