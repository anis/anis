/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { ConeSearchConfig } from '../models';

export const resetConeSearchConfig = createAction('[Metamodel] Reset Cone Search Config');
export const loadConeSearchConfig = createAction('[Metamodel] Load Cone Search Config');
export const loadConeSearchConfigSuccess = createAction('[Metamodel] Load Cone Search Config Success', props<{ coneSearchConfig: ConeSearchConfig }>());
export const loadConeSearchConfigFail = createAction('[Metamodel] Load Cone Search Config Fail');
export const addConeSearchConfig = createAction('[Metamodel] Add Cone Search Config', props<{ coneSearchConfig: ConeSearchConfig }>());
export const addConeSearchConfigSuccess = createAction('[Metamodel] Add Cone Search Config Success', props<{ coneSearchConfig: ConeSearchConfig }>());
export const addConeSearchConfigFail = createAction('[Metamodel] Add Cone Search Config Fail');
export const editConeSearchConfig = createAction('[Metamodel] Edit Cone Search Config', props<{ coneSearchConfig: ConeSearchConfig }>());
export const editConeSearchConfigSuccess = createAction('[Metamodel] Edit Cone Search Config Success', props<{ coneSearchConfig: ConeSearchConfig }>());
export const editConeSearchConfigFail = createAction('[Metamodel] Edit Cone Search Config Fail');
