/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { DesignConfig } from '../models';

export const loadDesignConfig = createAction('[Metamodel] Load Design Config');
export const loadDesignConfigSuccess = createAction('[Metamodel] Load Design Config Success', props<{ designConfig: DesignConfig }>());
export const loadDesignConfigFail = createAction('[Metamodel] Load Design Config Fail');
export const addDesignConfig = createAction('[Metamodel] Add Design Config', props<{ instanceName: string, designConfig: DesignConfig }>());
export const addDesignConfigSuccess = createAction('[Metamodel] Add Design Config Success', props<{ designConfig: DesignConfig }>());
export const addDesignConfigFail = createAction('[Metamodel] Add Design Config Fail');
export const editDesignConfig = createAction('[Metamodel] Edit Design Config', props<{ designConfig: DesignConfig }>());
export const editDesignConfigSuccess = createAction('[Metamodel] Edit Design Config Success', props<{ designConfig: DesignConfig }>());
export const editDesignConfigFail = createAction('[Metamodel] Edit Design Config Fail');
