/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as criteriaFamilyActions from './criteria-family.actions';
import { CRITERIA_FAMILY_LIST, CRITERIA_FAMILY } from 'src/test-data';

describe('[Metamodel][Actions] loadCriteriaFamilyList actions', () => {
    it('should create loadCriteriaFamilyList action', () => {
        const action = criteriaFamilyActions.loadCriteriaFamilyList();
        expect(action).toEqual({ type: '[Metamodel] Load Criteria Family List' });
    });

    it('should create loadCriteriaFamilyListSuccess action', () => {
        const action = criteriaFamilyActions.loadCriteriaFamilyListSuccess({ criteriaFamilies: CRITERIA_FAMILY_LIST });
        expect(action).toEqual({
            type: '[Metamodel] Load Criteria Family List Success',
            criteriaFamilies: CRITERIA_FAMILY_LIST
        });
    });

    it('should create loadCriteriaFamilyListFail action', () => {
        const action = criteriaFamilyActions.loadCriteriaFamilyListFail();
        expect(action).toEqual({ type: '[Metamodel] Load Criteria Family List Fail' });
    });

    it('should create addCriteriaFamily action', () => {
        const action = criteriaFamilyActions.addCriteriaFamily({ criteriaFamily: CRITERIA_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Add Criteria Family',
            criteriaFamily: CRITERIA_FAMILY
        });
    });

    it('should create addCriteriaFamilySuccess action', () => {
        const action = criteriaFamilyActions.addCriteriaFamilySuccess({ criteriaFamily: CRITERIA_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Add Criteria Family Success',
            criteriaFamily: CRITERIA_FAMILY
        });
    });

    it('should create addCriteriaFamilyFail action', () => {
        const action = criteriaFamilyActions.addCriteriaFamilyFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Criteria Family Fail'
        });
    });

    it('should create editCriteriaFamily action', () => {
        const action = criteriaFamilyActions.editCriteriaFamily({ criteriaFamily: CRITERIA_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Edit Criteria Family',
            criteriaFamily: CRITERIA_FAMILY
        });
    });

    it('should create editCriteriaFamilySuccess action', () => {
        const action = criteriaFamilyActions.editCriteriaFamilySuccess({ criteriaFamily: CRITERIA_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Edit Criteria Family Success',
            criteriaFamily: CRITERIA_FAMILY
        });
    });

    it('should create editCriteriaFamilyFail action', () => {
        const action = criteriaFamilyActions.editCriteriaFamilyFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Criteria Family Fail'
        });
    });

    it('should create deleteCriteriaFamily action', () => {
        const action = criteriaFamilyActions.deleteCriteriaFamily({ criteriaFamily: CRITERIA_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Delete Criteria Family',
            criteriaFamily: CRITERIA_FAMILY
        });
    });

    it('should create deleteCriteriaFamilySuccess action', () => {
        const action = criteriaFamilyActions.deleteCriteriaFamilySuccess({ criteriaFamily: CRITERIA_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Delete Criteria Family Success',
            criteriaFamily: CRITERIA_FAMILY
        });
    });

    it('should create deleteCriteriaFamilyFail action', () => {
        const action = criteriaFamilyActions.deleteCriteriaFamilyFail();
        expect(action).toEqual({
            type: '[Metamodel] Delete Criteria Family Fail'
        });
    });
});
