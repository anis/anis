/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { Instance } from '../models';

export const loadInstanceList = createAction('[Metamodel] Load Instance List');
export const loadInstanceListSuccess = createAction('[Metamodel] Load Instance List Success', props<{ instances: Instance[] }>());
export const loadInstanceListFail = createAction('[Metamodel] Load Instance List Fail');
export const importInstance = createAction('[Metamodel] Import Instance', props<{ fullInstance: any }>());
export const importInstanceSuccess = createAction('[Metamodel] Import Instance Success', props<{ instance: Instance }>());
export const importInstanceFail = createAction('[Metamodel] Import Instance Fail');
export const addInstance = createAction('[Metamodel] Add Instance', props<{ instance: Instance }>());
export const addInstanceSuccess = createAction('[Metamodel] Add Instance Success', props<{ instance: Instance }>());
export const addInstanceFail = createAction('[Metamodel] Add Instance Fail');
export const editInstance = createAction('[Metamodel] Edit Instance', props<{ instance: Instance }>());
export const editInstanceSuccess = createAction('[Metamodel] Edit Instance Success', props<{ instance: Instance }>());
export const editInstanceFail = createAction('[Metamodel] Edit Instance Fail');
export const deleteInstance = createAction('[Metamodel] Delete Instance', props<{ instance: Instance }>());
export const deleteInstanceSuccess = createAction('[Metamodel] Delete Instance Success', props<{ instance: Instance }>());
export const deleteInstanceFail = createAction('[Metamodel] Delete Instance Fail');
