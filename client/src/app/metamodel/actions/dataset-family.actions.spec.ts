/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as datasetFamilyActions from './dataset-family.actions';
import { DATASET_FAMILY_LIST, DATASET_FAMILY } from 'src/test-data';

describe('[Metamodel][Actions] loadDatasetFamilyList actions', () => {
    it('should create loadDatasetFamilyList action', () => {
        const action = datasetFamilyActions.loadDatasetFamilyList();
        expect(action).toEqual({ type: '[Metamodel] Load Dataset Family List' });
    });

    it('should create loadDatasetFamilyListSuccess action', () => {
        const action = datasetFamilyActions.loadDatasetFamilyListSuccess({ datasetFamilies: DATASET_FAMILY_LIST });
        expect(action).toEqual({
            type: '[Metamodel] Load Dataset Family List Success',
            datasetFamilies: DATASET_FAMILY_LIST
        });
    });

    it('should create loadDatasetFamilyListFail action', () => {
        const action = datasetFamilyActions.loadDatasetFamilyListFail();
        expect(action).toEqual({ type: '[Metamodel] Load Dataset Family List Fail' });
    });

    it('should create addDatasetFamily action', () => {
        const action = datasetFamilyActions.addDatasetFamily({ datasetFamily: DATASET_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Add Dataset Family',
            datasetFamily: DATASET_FAMILY
        });
    });

    it('should create addDatasetFamilySuccess action', () => {
        const action = datasetFamilyActions.addDatasetFamilySuccess({ datasetFamily: DATASET_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Add Dataset Family Success',
            datasetFamily: DATASET_FAMILY
        });
    });

    it('should create addDatasetFamilyFail action', () => {
        const action = datasetFamilyActions.addDatasetFamilyFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Dataset Family Fail'
        });
    });

    it('should create editDatasetFamily action', () => {
        const action = datasetFamilyActions.editDatasetFamily({ datasetFamily: DATASET_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Edit Dataset Family',
            datasetFamily: DATASET_FAMILY
        });
    });

    it('should create editDatasetFamilySuccess action', () => {
        const action = datasetFamilyActions.editDatasetFamilySuccess({ datasetFamily: DATASET_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Edit Dataset Family Success',
            datasetFamily: DATASET_FAMILY
        });
    });

    it('should create editDatasetFamilyFail action', () => {
        const action = datasetFamilyActions.editDatasetFamilyFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Dataset Family Fail'
        });
    });

    it('should create deleteDatasetFamily action', () => {
        const action = datasetFamilyActions.deleteDatasetFamily({ datasetFamily: DATASET_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Delete Dataset Family',
            datasetFamily: DATASET_FAMILY
        });
    });

    it('should create deleteDatasetFamilySuccess action', () => {
        const action = datasetFamilyActions.deleteDatasetFamilySuccess({ datasetFamily: DATASET_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Delete Dataset Family Success',
            datasetFamily: DATASET_FAMILY
        });
    });

    it('should create deleteDatasetFamilyFail action', () => {
        const action = datasetFamilyActions.deleteDatasetFamilyFail();
        expect(action).toEqual({
            type: '[Metamodel] Delete Dataset Family Fail'
        });
    });
});
