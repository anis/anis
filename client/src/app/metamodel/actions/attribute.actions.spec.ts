/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as attributeActions from './attribute.actions';
import { ATTRIBUTE_LIST, ATTRIBUTE } from 'src/test-data';

describe('[Metamodel][Actions] loadAttributeList actions', () => {
    it('should create loadAttributeList action', () => {
        const action = attributeActions.loadAttributeList();
        expect(action).toEqual({ type: '[Metamodel] Load Attribute List' });
    });

    it('should create loadAttributeListSuccess action', () => {
        const action = attributeActions.loadAttributeListSuccess({ attributes: ATTRIBUTE_LIST });
        expect(action).toEqual({
            type: '[Metamodel] Load Attribute List Success',
            attributes: ATTRIBUTE_LIST
        });
    });

    it('should create loadAttributeListFail action', () => {
        const action = attributeActions.loadAttributeListFail();
        expect(action).toEqual({ type: '[Metamodel] Load Attribute List Fail' });
    });

    it('should create addAttribute action', () => {
        const action = attributeActions.addAttribute({ attribute: ATTRIBUTE });
        expect(action).toEqual({
            type: '[Metamodel] Add Attribute',
            attribute: ATTRIBUTE
        });
    });

    it('should create addAttributeSuccess action', () => {
        const action = attributeActions.addAttributeSuccess({ attribute: ATTRIBUTE });
        expect(action).toEqual({
            type: '[Metamodel] Add Attribute Success',
            attribute: ATTRIBUTE
        });
    });

    it('should create addAttributeFail action', () => {
        const action = attributeActions.addAttributeFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Attribute Fail'
        });
    });

    it('should create editAttribute action', () => {
        const action = attributeActions.editAttribute({ attribute: ATTRIBUTE });
        expect(action).toEqual({
            type: '[Metamodel] Edit Attribute',
            attribute: ATTRIBUTE
        });
    });

    it('should create editAttributeSuccess action', () => {
        const action = attributeActions.editAttributeSuccess({ attribute: ATTRIBUTE });
        expect(action).toEqual({
            type: '[Metamodel] Edit Attribute Success',
            attribute: ATTRIBUTE
        });
    });

    it('should create editAttributeFail action', () => {
        const action = attributeActions.editAttributeFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Attribute Fail'
        });
    });

    it('should create deleteAttribute action', () => {
        const action = attributeActions.deleteAttribute({ attribute: ATTRIBUTE });
        expect(action).toEqual({
            type: '[Metamodel] Delete Attribute',
            attribute: ATTRIBUTE
        });
    });

    it('should create deleteAttributeSuccess action', () => {
        const action = attributeActions.deleteAttributeSuccess({ attribute: ATTRIBUTE });
        expect(action).toEqual({
            type: '[Metamodel] Delete Attribute Success',
            attribute: ATTRIBUTE
        });
    });

    it('should create deleteAttributeFail action', () => {
        const action = attributeActions.deleteAttributeFail();
        expect(action).toEqual({
            type: '[Metamodel] Delete Attribute Fail'
        });
    });
});
