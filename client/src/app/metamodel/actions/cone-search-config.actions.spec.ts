/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as coneSearchConfigActions from './cone-search-config.actions';
import { CONE_SEARCH_CONFIG } from 'src/test-data';

describe('[Metamodel][Actions] Design config actions', () => {
    it('should create loadConeSearchConfig action', () => {
        const action = coneSearchConfigActions.loadConeSearchConfig();
        expect(action).toEqual({ type: '[Metamodel] Load Cone Search Config' });
    });

    it('should create loadConeSearchConfigSuccess action', () => {
        const action = coneSearchConfigActions.loadConeSearchConfigSuccess({ coneSearchConfig: CONE_SEARCH_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Load Cone Search Config Success',
            coneSearchConfig: CONE_SEARCH_CONFIG
        });
    });

    it('should create loadConeSearchConfigFail action', () => {
        const action = coneSearchConfigActions.loadConeSearchConfigFail();
        expect(action).toEqual({ type: '[Metamodel] Load Cone Search Config Fail' });
    });

    it('should create addConeSearchConfig action', () => {
        const action = coneSearchConfigActions.addConeSearchConfig({ coneSearchConfig: CONE_SEARCH_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Add Cone Search Config',
            coneSearchConfig: CONE_SEARCH_CONFIG
        });
    });

    it('should create addConeSearchConfigSuccess action', () => {
        const action = coneSearchConfigActions.addConeSearchConfigSuccess({ coneSearchConfig: CONE_SEARCH_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Add Cone Search Config Success',
            coneSearchConfig: CONE_SEARCH_CONFIG
        });
    });

    it('should create addConeSearchConfigFail action', () => {
        const action = coneSearchConfigActions.addConeSearchConfigFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Cone Search Config Fail'
        });
    });

    it('should create editConeSearchConfig action', () => {
        const action = coneSearchConfigActions.editConeSearchConfig({ coneSearchConfig: CONE_SEARCH_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Edit Cone Search Config',
            coneSearchConfig: CONE_SEARCH_CONFIG
        });
    });

    it('should create editConeSearchConfigSuccess action', () => {
        const action = coneSearchConfigActions.editConeSearchConfigSuccess({ coneSearchConfig: CONE_SEARCH_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Edit Cone Search Config Success',
            coneSearchConfig: CONE_SEARCH_CONFIG
        });
    });

    it('should create editConeSearchConfigFail action', () => {
        const action = coneSearchConfigActions.editConeSearchConfigFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Cone Search Config Fail'
        });
    });
});
