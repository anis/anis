/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as detailConfigActions from './detail-config.actions';
import { DETAIL_CONFIG } from 'src/test-data';

describe('[Metamodel][Actions] Design config actions', () => {
    it('should create loadDetailConfig action', () => {
        const action = detailConfigActions.loadDetailConfig();
        expect(action).toEqual({ type: '[Metamodel] Load Detail Config' });
    });

    it('should create loadDetailConfigSuccess action', () => {
        const action = detailConfigActions.loadDetailConfigSuccess({ detailConfig: DETAIL_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Load Detail Config Success',
            detailConfig: DETAIL_CONFIG
        });
    });

    it('should create loadDetailConfigFail action', () => {
        const action = detailConfigActions.loadDetailConfigFail();
        expect(action).toEqual({ type: '[Metamodel] Load Detail Config Fail' });
    });

    it('should create addDetailConfig action', () => {
        const action = detailConfigActions.addDetailConfig({ detailConfig: DETAIL_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Add Detail Config',
            detailConfig: DETAIL_CONFIG
        });
    });

    it('should create addDetailConfigSuccess action', () => {
        const action = detailConfigActions.addDetailConfigSuccess({ detailConfig: DETAIL_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Add Detail Config Success',
            detailConfig: DETAIL_CONFIG
        });
    });

    it('should create addDetailConfigFail action', () => {
        const action = detailConfigActions.addDetailConfigFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Detail Config Fail'
        });
    });

    it('should create editDetailConfig action', () => {
        const action = detailConfigActions.editDetailConfig({ detailConfig: DETAIL_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Edit Detail Config',
            detailConfig: DETAIL_CONFIG
        });
    });

    it('should create editDetailConfigSuccess action', () => {
        const action = detailConfigActions.editDetailConfigSuccess({ detailConfig: DETAIL_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Edit Detail Config Success',
            detailConfig: DETAIL_CONFIG
        });
    });

    it('should create editDetailConfigFail action', () => {
        const action = detailConfigActions.editDetailConfigFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Detail Config Fail'
        });
    });
});
