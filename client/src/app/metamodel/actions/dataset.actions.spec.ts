/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as datasetActions from './dataset.actions';
import { DATASET_LIST, DATASET } from 'src/test-data';

describe('[Metamodel][Actions] loadDatasetList actions', () => {
    it('should create loadDatasetList action', () => {
        const action = datasetActions.loadDatasetList();
        expect(action).toEqual({ type: '[Metamodel] Load Dataset List' });
    });

    it('should create loadDatasetListSuccess action', () => {
        const action = datasetActions.loadDatasetListSuccess({ datasets: DATASET_LIST });
        expect(action).toEqual({
            type: '[Metamodel] Load Dataset List Success',
            datasets: DATASET_LIST
        });
    });

    it('should create loadDatasetListFail action', () => {
        const action = datasetActions.loadDatasetListFail();
        expect(action).toEqual({ type: '[Metamodel] Load Dataset List Fail' });
    });

    it('should create importDataset action', () => {
        const action = datasetActions.importDataset({ fullDataset: DATASET });
        expect(action).toEqual({
            type: '[Metamodel] Import Dataset',
            fullDataset: DATASET
        });
    });

    it('should create importDatasetSuccess action', () => {
        const action = datasetActions.importDatasetSuccess({ dataset: DATASET });
        expect(action).toEqual({
            type: '[Metamodel] Import Dataset Success',
            dataset: DATASET
        });
    });

    it('should create importDatasetFail action', () => {
        const action = datasetActions.importDatasetFail();
        expect(action).toEqual({
            type: '[Metamodel] Import Dataset Fail'
        });
    });

    it('should create addDataset action', () => {
        const action = datasetActions.addDataset({ dataset: DATASET });
        expect(action).toEqual({
            type: '[Metamodel] Add Dataset',
            dataset: DATASET
        });
    });

    it('should create addDatasetSuccess action', () => {
        const action = datasetActions.addDatasetSuccess({ dataset: DATASET });
        expect(action).toEqual({
            type: '[Metamodel] Add Dataset Success',
            dataset: DATASET
        });
    });

    it('should create addDatasetFail action', () => {
        const action = datasetActions.addDatasetFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Dataset Fail'
        });
    });

    it('should create editDataset action', () => {
        const action = datasetActions.editDataset({ dataset: DATASET });
        expect(action).toEqual({
            type: '[Metamodel] Edit Dataset',
            dataset: DATASET
        });
    });

    it('should create editDatasetSuccess action', () => {
        const action = datasetActions.editDatasetSuccess({ dataset: DATASET });
        expect(action).toEqual({
            type: '[Metamodel] Edit Dataset Success',
            dataset: DATASET
        });
    });

    it('should create editDatasetFail action', () => {
        const action = datasetActions.editDatasetFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Dataset Fail'
        });
    });

    it('should create deleteDataset action', () => {
        const action = datasetActions.deleteDataset({ dataset: DATASET });
        expect(action).toEqual({
            type: '[Metamodel] Delete Dataset',
            dataset: DATASET
        });
    });

    it('should create deleteDatasetSuccess action', () => {
        const action = datasetActions.deleteDatasetSuccess({ dataset: DATASET });
        expect(action).toEqual({
            type: '[Metamodel] Delete Dataset Success',
            dataset: DATASET
        });
    });

    it('should create deleteDatasetFail action', () => {
        const action = datasetActions.deleteDatasetFail();
        expect(action).toEqual({
            type: '[Metamodel] Delete Dataset Fail'
        });
    });
});
