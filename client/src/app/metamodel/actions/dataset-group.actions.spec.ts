/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as datasetGroupActions from './dataset-group.actions';
import { DATASET } from 'src/test-data';

describe('[Metamodel][Actions] loadDatasetGroupList actions', () => {
    it('should create loadDatasetGroupList action', () => {
        const action = datasetGroupActions.loadDatasetGroupList();
        expect(action).toEqual({ type: '[Metamodel] Load Dataset Group List' });
    });

    it('should create loadDatasetGroupListSuccess action', () => {
        const action = datasetGroupActions.loadDatasetGroupListSuccess({ datasetGroups: DATASET.groups });
        expect(action).toEqual({
            type: '[Metamodel] Load Dataset Group List Success',
            datasetGroups: DATASET.groups
        });
    });

    it('should create loadDatasetGroupListFail action', () => {
        const action = datasetGroupActions.loadDatasetGroupListFail();
        expect(action).toEqual({ type: '[Metamodel] Load Dataset Group List Fail' });
    });

    it('should create addDatasetGroup action', () => {
        const action = datasetGroupActions.addDatasetGroup({ datasetGroup: DATASET.groups[0] });
        expect(action).toEqual({
            type: '[Metamodel] Add Dataset Group',
            datasetGroup: DATASET.groups[0]
        });
    });

    it('should create addDatasetGroupSuccess action', () => {
        const action = datasetGroupActions.addDatasetGroupSuccess({ datasetGroup: DATASET.groups[0] });
        expect(action).toEqual({
            type: '[Metamodel] Add Dataset Group Success',
            datasetGroup: DATASET.groups[0]
        });
    });

    it('should create addDatasetGroupFail action', () => {
        const action = datasetGroupActions.addDatasetGroupFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Dataset Group Fail'
        });
    });

    it('should create editDatasetGroup action', () => {
        const action = datasetGroupActions.editDatasetGroup({ datasetGroup: DATASET.groups[0] });
        expect(action).toEqual({
            type: '[Metamodel] Edit Dataset Group',
            datasetGroup: DATASET.groups[0]
        });
    });

    it('should create editDatasetGroupSuccess action', () => {
        const action = datasetGroupActions.editDatasetGroupSuccess({ datasetGroup: DATASET.groups[0] });
        expect(action).toEqual({
            type: '[Metamodel] Edit Dataset Group Success',
            datasetGroup: DATASET.groups[0]
        });
    });

    it('should create editDatasetGroupFail action', () => {
        const action = datasetGroupActions.editDatasetGroupFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Dataset Group Fail'
        });
    });

    it('should create deleteDatasetGroup action', () => {
        const action = datasetGroupActions.deleteDatasetGroup({ datasetGroup: DATASET.groups[0] });
        expect(action).toEqual({
            type: '[Metamodel] Delete Dataset Group',
            datasetGroup: DATASET.groups[0]
        });
    });

    it('should create deleteDatasetGroupSuccess action', () => {
        const action = datasetGroupActions.deleteDatasetGroupSuccess({ datasetGroup: DATASET.groups[0] });
        expect(action).toEqual({
            type: '[Metamodel] Delete Dataset Group Success',
            datasetGroup: DATASET.groups[0]
        });
    });

    it('should create deleteDatasetGroupFail action', () => {
        const action = datasetGroupActions.deleteDatasetGroupFail();
        expect(action).toEqual({
            type: '[Metamodel] Delete Dataset Group Fail'
        });
    });
});
