/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as menuFamilyItemActions from './menu-family-item.actions';
import { MENU_FAMILY, MENU_ITEM_LIST } from 'src/test-data';

describe('[Metamodel][Actions] loadMenuFamilyItemList actions', () => {
    it('should create loadMenuItemList action', () => {
        const action = menuFamilyItemActions.loadMenuFamilyItemList();
        expect(action).toEqual({
            type: '[Metamodel] Load Menu Family Item List'
        });
    });

    it('should create loadMenuFamilyItemListSuccess action', () => {
        const action = menuFamilyItemActions.loadMenuFamilyItemListSuccess({ menuFamilyItemList: MENU_FAMILY.items });
        expect(action).toEqual({
            type: '[Metamodel] Load Menu Family Item List Success',
            menuFamilyItemList: MENU_FAMILY.items
        });
    });

    it('should create loadMenuFamilyItemListFail action', () => {
        const action = menuFamilyItemActions.loadMenuFamilyItemListFail();
        expect(action).toEqual({ type: '[Metamodel] Load Menu Family Item List Fail' });
    });

    it('should create addMenuFamilyItem action', () => {
        const action = menuFamilyItemActions.addMenuFamilyItem({ menuFamilyItem: MENU_ITEM_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Add Menu Family Item',
            menuFamilyItem: MENU_ITEM_LIST[0]
        });
    });

    it('should create addMenuFamilyItemSuccess action', () => {
        const action = menuFamilyItemActions.addMenuFamilyItemSuccess({ menuFamilyItem: MENU_ITEM_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Add Menu Family Item Success',
            menuFamilyItem: MENU_ITEM_LIST[0]
        });
    });

    it('should create addMenuFamilyItemFail action', () => {
        const action = menuFamilyItemActions.addMenuFamilyItemFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Menu Family Item Fail'
        });
    });

    it('should create editMenuFamilyItem action', () => {
        const action = menuFamilyItemActions.editMenuFamilyItem({ menuFamilyItem: MENU_ITEM_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Edit Menu Family Item',
            menuFamilyItem: MENU_ITEM_LIST[0]
        });
    });

    it('should create editMenuFamilyItemSuccess action', () => {
        const action = menuFamilyItemActions.editMenuFamilyItemSuccess({ menuFamilyItem: MENU_ITEM_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Edit Menu Family Item Success',
            menuFamilyItem: MENU_ITEM_LIST[0]
        });
    });

    it('should create editMenuFamilyItemFail action', () => {
        const action = menuFamilyItemActions.editMenuFamilyItemFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Menu Family Item Fail'
        });
    });

    it('should create deleteMenuFamilyItem action', () => {
        const action = menuFamilyItemActions.deleteMenuFamilyItem({ menuFamilyItem: MENU_ITEM_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Delete Menu Family Item',
            menuFamilyItem: MENU_ITEM_LIST[0]
        });
    });

    it('should create deleteMenuFamilyItemSuccess action', () => {
        const action = menuFamilyItemActions.deleteMenuFamilyItemSuccess({ menuFamilyItem: MENU_ITEM_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Delete Menu Family Item Success',
            menuFamilyItem: MENU_ITEM_LIST[0]
        });
    });

    it('should create deleteMenuFamilyItemFail action', () => {
        const action = menuFamilyItemActions.deleteMenuFamilyItemFail();
        expect(action).toEqual({
            type: '[Metamodel] Delete Menu Family Item Fail'
        });
    });
});
