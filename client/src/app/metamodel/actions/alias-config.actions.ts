/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { AliasConfig } from '../models';

export const resetAliasConfig = createAction('[Metamodel] Reset Alias Config');
export const loadAliasConfig = createAction('[Metamodel] Load Alias Config');
export const loadAliasConfigSuccess = createAction('[Metamodel] Load Alias Config Success', props<{ aliasConfig: AliasConfig }>());
export const loadAliasConfigFail = createAction('[Metamodel] Load Alias Config Fail');
export const addAliasConfig = createAction('[Metamodel] Add Alias Config', props<{ aliasConfig: AliasConfig }>());
export const addAliasConfigSuccess = createAction('[Metamodel] Add Alias Config Success', props<{ aliasConfig: AliasConfig }>());
export const addAliasConfigFail = createAction('[Metamodel] Add Alias Config Fail');
export const editAliasConfig = createAction('[Metamodel] Edit Alias Config', props<{ aliasConfig: AliasConfig }>());
export const editAliasConfigSuccess = createAction('[Metamodel] Edit Alias Config Success', props<{ aliasConfig: AliasConfig }>());
export const editAliasConfigFail = createAction('[Metamodel] Edit Alias Config Fail');
