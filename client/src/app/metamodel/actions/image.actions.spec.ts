/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as imageActions from './image.actions';
import { IMAGE } from 'src/test-data';

describe('[Metamodel][Actions] loadImageList actions', () => {
    it('should create loadImageList action', () => {
        const action = imageActions.loadImageList();
        expect(action).toEqual({ type: '[Metamodel] Load Image List' });
    });

    it('should create loadImageListSuccess action', () => {
        const action = imageActions.loadImageListSuccess({ images: [IMAGE] });
        expect(action).toEqual({
            type: '[Metamodel] Load Image List Success',
            images: [IMAGE]
        });
    });

    it('should create loadImageListFail action', () => {
        const action = imageActions.loadImageListFail();
        expect(action).toEqual({ type: '[Metamodel] Load Image List Fail' });
    });

    it('should create addImage action', () => {
        const action = imageActions.addImage({ image: IMAGE });
        expect(action).toEqual({
            type: '[Metamodel] Add Image',
            image: IMAGE
        });
    });

    it('should create addImageSuccess action', () => {
        const action = imageActions.addImageSuccess({ image: IMAGE });
        expect(action).toEqual({
            type: '[Metamodel] Add Image Success',
            image: IMAGE
        });
    });

    it('should create addImageFail action', () => {
        const action = imageActions.addImageFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Image Fail'
        });
    });

    it('should create editImage action', () => {
        const action = imageActions.editImage({ image: IMAGE });
        expect(action).toEqual({
            type: '[Metamodel] Edit Image',
            image: IMAGE
        });
    });

    it('should create editImageSuccess action', () => {
        const action = imageActions.editImageSuccess({ image: IMAGE });
        expect(action).toEqual({
            type: '[Metamodel] Edit Image Success',
            image: IMAGE
        });
    });

    it('should create editImageFail action', () => {
        const action = imageActions.editImageFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Image Fail'
        });
    });

    it('should create deleteImage action', () => {
        const action = imageActions.deleteImage({ image: IMAGE });
        expect(action).toEqual({
            type: '[Metamodel] Delete Image',
            image: IMAGE
        });
    });

    it('should create deleteImageSuccess action', () => {
        const action = imageActions.deleteImageSuccess({ image: IMAGE });
        expect(action).toEqual({
            type: '[Metamodel] Delete Image Success',
            image: IMAGE
        });
    });

    it('should create deleteImageFail action', () => {
        const action = imageActions.deleteImageFail();
        expect(action).toEqual({
            type: '[Metamodel] Delete Image Fail'
        });
    });
});
