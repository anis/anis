/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as outputFamilyActions from './output-family.actions';
import { OUTPUT_FAMILY_LIST, OUTPUT_FAMILY } from 'src/test-data';

describe('[Metamodel][Actions] loadOutputFamilyList actions', () => {
    it('should create loadOutputFamilyList action', () => {
        const action = outputFamilyActions.loadOutputFamilyList();
        expect(action).toEqual({ type: '[Metamodel] Load Output Family List' });
    });

    it('should create loadOutputFamilyListSuccess action', () => {
        const action = outputFamilyActions.loadOutputFamilyListSuccess({ outputFamilies: OUTPUT_FAMILY_LIST });
        expect(action).toEqual({
            type: '[Metamodel] Load Output Family List Success',
            outputFamilies: OUTPUT_FAMILY_LIST
        });
    });

    it('should create loadOutputFamilyListFail action', () => {
        const action = outputFamilyActions.loadOutputFamilyListFail();
        expect(action).toEqual({ type: '[Metamodel] Load Output Family List Fail' });
    });

    it('should create addOutputFamily action', () => {
        const action = outputFamilyActions.addOutputFamily({ outputFamily: OUTPUT_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Add Output Family',
            outputFamily: OUTPUT_FAMILY
        });
    });

    it('should create addOutputFamilySuccess action', () => {
        const action = outputFamilyActions.addOutputFamilySuccess({ outputFamily: OUTPUT_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Add Output Family Success',
            outputFamily: OUTPUT_FAMILY
        });
    });

    it('should create addOutputFamilyFail action', () => {
        const action = outputFamilyActions.addOutputFamilyFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Output Family Fail'
        });
    });

    it('should create editOutputFamily action', () => {
        const action = outputFamilyActions.editOutputFamily({ outputFamily: OUTPUT_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Edit Output Family',
            outputFamily: OUTPUT_FAMILY
        });
    });

    it('should create editOutputFamilySuccess action', () => {
        const action = outputFamilyActions.editOutputFamilySuccess({ outputFamily: OUTPUT_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Edit Output Family Success',
            outputFamily: OUTPUT_FAMILY
        });
    });

    it('should create editOutputFamilyFail action', () => {
        const action = outputFamilyActions.editOutputFamilyFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Output Family Fail'
        });
    });

    it('should create deleteOutputFamily action', () => {
        const action = outputFamilyActions.deleteOutputFamily({ outputFamily: OUTPUT_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Delete Output Family',
            outputFamily: OUTPUT_FAMILY
        });
    });

    it('should create deleteOutputFamilySuccess action', () => {
        const action = outputFamilyActions.deleteOutputFamilySuccess({ outputFamily: OUTPUT_FAMILY });
        expect(action).toEqual({
            type: '[Metamodel] Delete Output Family Success',
            outputFamily: OUTPUT_FAMILY
        });
    });

    it('should create deleteOutputFamilyFail action', () => {
        const action = outputFamilyActions.deleteOutputFamilyFail();
        expect(action).toEqual({
            type: '[Metamodel] Delete Output Family Fail'
        });
    });
});
