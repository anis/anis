/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { InstanceGroup } from '../models';

export const loadInstanceGroupList = createAction('[Metamodel] Load Instance Group List');
export const loadInstanceGroupListSuccess = createAction('[Metamodel] Load Instance Group List Success', props<{ instanceGroups: InstanceGroup[] }>());
export const loadInstanceGroupListFail = createAction('[Metamodel] Load Instance Group List Fail');
export const addInstanceGroup = createAction('[Metamodel] Add Instance Group', props<{ instanceGroup: InstanceGroup }>());
export const addInstanceGroupSuccess = createAction('[Metamodel] Add Instance Group Success', props<{ instanceGroup: InstanceGroup }>());
export const addInstanceGroupFail = createAction('[Metamodel] Add Instance Group Fail');
export const editInstanceGroup = createAction('[Metamodel] Edit Instance Group', props<{ instanceGroup: InstanceGroup }>());
export const editInstanceGroupSuccess = createAction('[Metamodel] Edit Instance Group Success', props<{ instanceGroup: InstanceGroup }>());
export const editInstanceGroupFail = createAction('[Metamodel] Edit Instance Group Fail');
export const deleteInstanceGroup = createAction('[Metamodel] Delete Instance Group', props<{ instanceGroup: InstanceGroup }>());
export const deleteInstanceGroupSuccess = createAction('[Metamodel] Delete Instance Group Success', props<{ instanceGroup: InstanceGroup }>());
export const deleteInstanceGroupFail = createAction('[Metamodel] Delete Instance Group Fail');
