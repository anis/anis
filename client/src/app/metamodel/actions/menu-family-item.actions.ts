/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { MenuItem } from '../models';

export const loadMenuFamilyItemList = createAction('[Metamodel] Load Menu Family Item List');
export const loadMenuFamilyItemListSuccess = createAction('[Metamodel] Load Menu Family Item List Success', props<{ menuFamilyItemList: MenuItem[] }>());
export const loadMenuFamilyItemListFail = createAction('[Metamodel] Load Menu Family Item List Fail');
export const addMenuFamilyItem = createAction('[Metamodel] Add Menu Family Item', props<{ menuFamilyItem: MenuItem }>());
export const addMenuFamilyItemSuccess = createAction('[Metamodel] Add Menu Family Item Success', props<{ menuFamilyItem: MenuItem }>());
export const addMenuFamilyItemFail = createAction('[Metamodel] Add Menu Family Item Fail');
export const editMenuFamilyItem = createAction('[Metamodel] Edit Menu Family Item', props<{ menuFamilyItem: MenuItem }>());
export const editMenuFamilyItemSuccess = createAction('[Metamodel] Edit Menu Family Item Success', props<{ menuFamilyItem: MenuItem }>());
export const editMenuFamilyItemFail = createAction('[Metamodel] Edit Menu Family Item Fail');
export const deleteMenuFamilyItem = createAction('[Metamodel] Delete Menu Family Item', props<{ menuFamilyItem: MenuItem }>());
export const deleteMenuFamilyItemSuccess = createAction('[Metamodel] Delete Menu Family Item Success', props<{ menuFamilyItem: MenuItem }>());
export const deleteMenuFamilyItemFail = createAction('[Metamodel] Delete Menu Family Item Fail');
