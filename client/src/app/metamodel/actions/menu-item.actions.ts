/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';
 
import { MenuItem } from '../models';

export const loadMenuItemList = createAction('[Metamodel] Load Menu Item List');
export const loadMenuItemListSuccess = createAction('[Metamodel] Load Menu Item List Success', props<{ menuItems: MenuItem[] }>());
export const loadMenuItemListFail = createAction('[Metamodel] Load Menu Item List Fail');
export const addMenuItem = createAction('[Metamodel] Add Menu Item', props<{ menuItem: MenuItem }>());
export const addMenuItemSuccess = createAction('[Metamodel] Add Menu Item Success', props<{ menuItem: MenuItem }>());
export const addMenuItemFail = createAction('[Metamodel] Add Menu Item Fail');
export const editMenuItem = createAction('[Metamodel] Edit Menu Item', props<{ menuItem: MenuItem }>());
export const editMenuItemSuccess = createAction('[Metamodel] Edit Menu Item Success', props<{ menuItem: MenuItem }>());
export const editMenuItemFail = createAction('[Metamodel] Edit Menu Item Fail');
export const deleteMenuItem = createAction('[Metamodel] Delete Menu Item', props<{ menuItem: MenuItem }>());
export const deleteMenuItemSuccess = createAction('[Metamodel] Delete Menu Item Success', props<{ menuItem: MenuItem }>());
export const deleteMenuItemFail = createAction('[Metamodel] Delete Menu Item Fail');
