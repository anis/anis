/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as designConfigActions from './design-config.actions';
import { DESIGN_CONFIG } from 'src/test-data';

describe('[Metamodel][Actions] Design config actions', () => {
    it('should create loadDesignConfig action', () => {
        const action = designConfigActions.loadDesignConfig();
        expect(action).toEqual({ type: '[Metamodel] Load Design Config' });
    });

    it('should create loadDesignConfigSuccess action', () => {
        const action = designConfigActions.loadDesignConfigSuccess({ designConfig: DESIGN_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Load Design Config Success',
            designConfig: DESIGN_CONFIG
        });
    });

    it('should create loadDesignConfigFail action', () => {
        const action = designConfigActions.loadDesignConfigFail();
        expect(action).toEqual({ type: '[Metamodel] Load Design Config Fail' });
    });

    it('should create addDesignConfig action', () => {
        const action = designConfigActions.addDesignConfig({ instanceName: 'default', designConfig: DESIGN_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Add Design Config',
            instanceName: 'default',
            designConfig: DESIGN_CONFIG
        });
    });

    it('should create addDesignConfigSuccess action', () => {
        const action = designConfigActions.addDesignConfigSuccess({ designConfig: DESIGN_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Add Design Config Success',
            designConfig: DESIGN_CONFIG
        });
    });

    it('should create addDesignConfigFail action', () => {
        const action = designConfigActions.addDesignConfigFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Design Config Fail'
        });
    });

    it('should create editDesignConfig action', () => {
        const action = designConfigActions.editDesignConfig({ designConfig: DESIGN_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Edit Design Config',
            designConfig: DESIGN_CONFIG
        });
    });

    it('should create editDesignConfigSuccess action', () => {
        const action = designConfigActions.editDesignConfigSuccess({ designConfig: DESIGN_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Edit Design Config Success',
            designConfig: DESIGN_CONFIG
        });
    });

    it('should create editDesignConfigFail action', () => {
        const action = designConfigActions.editDesignConfigFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Design Config Fail'
        });
    });
});
