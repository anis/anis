/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as aliasConfigActions from './alias-config.actions';
import { ALIAS_CONFIG } from 'src/test-data';

describe('[Metamodel][Actions] Design config actions', () => {
    it('should create loadAliasConfig action', () => {
        const action = aliasConfigActions.loadAliasConfig();
        expect(action).toEqual({ type: '[Metamodel] Load Alias Config' });
    });

    it('should create loadAliasConfigSuccess action', () => {
        const action = aliasConfigActions.loadAliasConfigSuccess({ aliasConfig: ALIAS_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Load Alias Config Success',
            aliasConfig: ALIAS_CONFIG
        });
    });

    it('should create loadAliasConfigFail action', () => {
        const action = aliasConfigActions.loadAliasConfigFail();
        expect(action).toEqual({ type: '[Metamodel] Load Alias Config Fail' });
    });

    it('should create addAliasConfig action', () => {
        const action = aliasConfigActions.addAliasConfig({ aliasConfig: ALIAS_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Add Alias Config',
            aliasConfig: ALIAS_CONFIG
        });
    });

    it('should create addAliasConfigSuccess action', () => {
        const action = aliasConfigActions.addAliasConfigSuccess({ aliasConfig: ALIAS_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Add Alias Config Success',
            aliasConfig: ALIAS_CONFIG
        });
    });

    it('should create addAliasConfigFail action', () => {
        const action = aliasConfigActions.addAliasConfigFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Alias Config Fail'
        });
    });

    it('should create editAliasConfig action', () => {
        const action = aliasConfigActions.editAliasConfig({ aliasConfig: ALIAS_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Edit Alias Config',
            aliasConfig: ALIAS_CONFIG
        });
    });

    it('should create editAliasConfigSuccess action', () => {
        const action = aliasConfigActions.editAliasConfigSuccess({ aliasConfig: ALIAS_CONFIG });
        expect(action).toEqual({
            type: '[Metamodel] Edit Alias Config Success',
            aliasConfig: ALIAS_CONFIG
        });
    });

    it('should create editAliasConfigFail action', () => {
        const action = aliasConfigActions.editAliasConfigFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Alias Config Fail'
        });
    });
});
