/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as instanceActions from './instance.actions';
import { INSTANCE_LIST } from 'src/test-data';

describe('[Metamodel][Actions] Instance actions', () => {
    it('should create loadInstanceList action', () => {
        const action = instanceActions.loadInstanceList();
        expect(action).toEqual({ type: '[Metamodel] Load Instance List' });
    });

    it('should create loadInstanceListSuccess action', () => {
        const action = instanceActions.loadInstanceListSuccess({ instances: INSTANCE_LIST });
        expect(action).toEqual({
            type: '[Metamodel] Load Instance List Success',
            instances: INSTANCE_LIST
        });
    });

    it('should create loadInstanceListFail action', () => {
        const action = instanceActions.loadInstanceListFail();
        expect(action).toEqual({ type: '[Metamodel] Load Instance List Fail' });
    });

    it('should create importInstance action', () => {
        const action = instanceActions.importInstance({ fullInstance: INSTANCE_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Import Instance',
            fullInstance: INSTANCE_LIST[0]
        });
    });

    it('should create importInstanceSuccess action', () => {
        const action = instanceActions.importInstanceSuccess({ instance: INSTANCE_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Import Instance Success',
            instance: INSTANCE_LIST[0]
        });
    });

    it('should create importInstanceFail action', () => {
        const action = instanceActions.importInstanceFail();
        expect(action).toEqual({
            type: '[Metamodel] Import Instance Fail'
        });
    });

    it('should create addInstance action', () => {
        const action = instanceActions.addInstance({ instance: INSTANCE_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Add Instance',
            instance: INSTANCE_LIST[0]
        });
    });

    it('should create addInstanceSuccess action', () => {
        const action = instanceActions.addInstanceSuccess({ instance: INSTANCE_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Add Instance Success',
            instance: INSTANCE_LIST[0]
        });
    });

    it('should create addInstanceFail action', () => {
        const action = instanceActions.addInstanceFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Instance Fail'
        });
    });

    it('should create editInstance action', () => {
        const action = instanceActions.editInstance({ instance: INSTANCE_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Edit Instance',
            instance: INSTANCE_LIST[0]
        });
    });

    it('should create editInstanceSuccess action', () => {
        const action = instanceActions.editInstanceSuccess({ instance: INSTANCE_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Edit Instance Success',
            instance: INSTANCE_LIST[0]
        });
    });

    it('should create editInstanceFail action', () => {
        const action = instanceActions.editInstanceFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Instance Fail'
        });
    });

    it('should create deleteInstance action', () => {
        const action = instanceActions.deleteInstance({ instance: INSTANCE_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Delete Instance',
            instance: INSTANCE_LIST[0]
        });
    });

    it('should create deleteInstanceSuccess action', () => {
        const action = instanceActions.deleteInstanceSuccess({ instance: INSTANCE_LIST[0] });
        expect(action).toEqual({
            type: '[Metamodel] Delete Instance Success',
            instance: INSTANCE_LIST[0]
        });
    });

    it('should create deleteInstanceFail action', () => {
        const action = instanceActions.deleteInstanceFail();
        expect(action).toEqual({
            type: '[Metamodel] Delete Instance Fail'
        });
    });
});
