/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as instanceGroupActions from './instance-group.actions';
import { INSTANCE_LIST } from 'src/test-data';

describe('[Metamodel][Actions] loadInstanceGroupList actions', () => {
    it('should create loadInstanceGroupList action', () => {
        const action = instanceGroupActions.loadInstanceGroupList();
        expect(action).toEqual({ type: '[Metamodel] Load Instance Group List' });
    });

    it('should create loadInstanceGroupListSuccess action', () => {
        const action = instanceGroupActions.loadInstanceGroupListSuccess({ instanceGroups: INSTANCE_LIST[0].groups });
        expect(action).toEqual({
            type: '[Metamodel] Load Instance Group List Success',
            instanceGroups: INSTANCE_LIST[0].groups
        });
    });

    it('should create loadInstanceGroupListFail action', () => {
        const action = instanceGroupActions.loadInstanceGroupListFail();
        expect(action).toEqual({ type: '[Metamodel] Load Instance Group List Fail' });
    });

    it('should create addInstanceGroup action', () => {
        const action = instanceGroupActions.addInstanceGroup({ instanceGroup: INSTANCE_LIST[0].groups[0] });
        expect(action).toEqual({
            type: '[Metamodel] Add Instance Group',
            instanceGroup: INSTANCE_LIST[0].groups[0]
        });
    });

    it('should create addInstanceGroupSuccess action', () => {
        const action = instanceGroupActions.addInstanceGroupSuccess({ instanceGroup: INSTANCE_LIST[0].groups[0] });
        expect(action).toEqual({
            type: '[Metamodel] Add Instance Group Success',
            instanceGroup: INSTANCE_LIST[0].groups[0]
        });
    });

    it('should create addInstanceGroupFail action', () => {
        const action = instanceGroupActions.addInstanceGroupFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Instance Group Fail'
        });
    });

    it('should create editInstanceGroup action', () => {
        const action = instanceGroupActions.editInstanceGroup({ instanceGroup: INSTANCE_LIST[0].groups[0] });
        expect(action).toEqual({
            type: '[Metamodel] Edit Instance Group',
            instanceGroup: INSTANCE_LIST[0].groups[0]
        });
    });

    it('should create editInstanceGroupSuccess action', () => {
        const action = instanceGroupActions.editInstanceGroupSuccess({ instanceGroup: INSTANCE_LIST[0].groups[0] });
        expect(action).toEqual({
            type: '[Metamodel] Edit Instance Group Success',
            instanceGroup: INSTANCE_LIST[0].groups[0]
        });
    });

    it('should create editInstanceGroupFail action', () => {
        const action = instanceGroupActions.editInstanceGroupFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Instance Group Fail'
        });
    });

    it('should create deleteInstanceGroup action', () => {
        const action = instanceGroupActions.deleteInstanceGroup({ instanceGroup: INSTANCE_LIST[0].groups[0] });
        expect(action).toEqual({
            type: '[Metamodel] Delete Instance Group',
            instanceGroup: INSTANCE_LIST[0].groups[0]
        });
    });

    it('should create deleteInstanceGroupSuccess action', () => {
        const action = instanceGroupActions.deleteInstanceGroupSuccess({ instanceGroup: INSTANCE_LIST[0].groups[0] });
        expect(action).toEqual({
            type: '[Metamodel] Delete Instance Group Success',
            instanceGroup: INSTANCE_LIST[0].groups[0]
        });
    });

    it('should create deleteInstanceGroupFail action', () => {
        const action = instanceGroupActions.deleteInstanceGroupFail();
        expect(action).toEqual({
            type: '[Metamodel] Delete Instance Group Fail'
        });
    });
});
