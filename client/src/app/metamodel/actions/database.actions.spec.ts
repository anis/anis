/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as databaseActions from './database.actions';
import { DATABASE_LIST, DATABASE } from 'src/test-data';

describe('[Metamodel][Actions] loadDatabaseList actions', () => {
    it('should create loadDatabaseList action', () => {
        const action = databaseActions.loadDatabaseList();
        expect(action).toEqual({ type: '[Metamodel] Load Database List' });
    });

    it('should create loadDatabaseListSuccess action', () => {
        const action = databaseActions.loadDatabaseListSuccess({ databases: DATABASE_LIST });
        expect(action).toEqual({
            type: '[Metamodel] Load Database List Success',
            databases: DATABASE_LIST
        });
    });

    it('should create loadDatabaseListFail action', () => {
        const action = databaseActions.loadDatabaseListFail();
        expect(action).toEqual({ type: '[Metamodel] Load Database List Fail' });
    });

    it('should create addDatabase action', () => {
        const action = databaseActions.addDatabase({ database: DATABASE });
        expect(action).toEqual({
            type: '[Metamodel] Add Database',
            database: DATABASE
        });
    });

    it('should create addDatabaseSuccess action', () => {
        const action = databaseActions.addDatabaseSuccess({ database: DATABASE });
        expect(action).toEqual({
            type: '[Metamodel] Add Database Success',
            database: DATABASE
        });
    });

    it('should create addDatabaseFail action', () => {
        const action = databaseActions.addDatabaseFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Database Fail'
        });
    });

    it('should create editDatabase action', () => {
        const action = databaseActions.editDatabase({ database: DATABASE });
        expect(action).toEqual({
            type: '[Metamodel] Edit Database',
            database: DATABASE
        });
    });

    it('should create editDatabaseSuccess action', () => {
        const action = databaseActions.editDatabaseSuccess({ database: DATABASE });
        expect(action).toEqual({
            type: '[Metamodel] Edit Database Success',
            database: DATABASE
        });
    });

    it('should create editDatabaseFail action', () => {
        const action = databaseActions.editDatabaseFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Database Fail'
        });
    });

    it('should create deleteDatabase action', () => {
        const action = databaseActions.deleteDatabase({ database: DATABASE });
        expect(action).toEqual({
            type: '[Metamodel] Delete Database',
            database: DATABASE
        });
    });

    it('should create deleteDatabaseSuccess action', () => {
        const action = databaseActions.deleteDatabaseSuccess({ database: DATABASE });
        expect(action).toEqual({
            type: '[Metamodel] Delete Database Success',
            database: DATABASE
        });
    });

    it('should create deleteDatabaseFail action', () => {
        const action = databaseActions.deleteDatabaseFail();
        expect(action).toEqual({
            type: '[Metamodel] Delete Database Fail'
        });
    });
});
