/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as outputCategoryActions from './output-category.actions';
import { OUTPUT_CATEGORY_LIST, OUTPUT_CATEGORY } from 'src/test-data';

describe('[Metamodel][Actions] loadOutputCategoryList actions', () => {
    it('should create loadOutputCategoryList action', () => {
        const action = outputCategoryActions.loadOutputCategoryList({ outputFamilyId: 1 });
        expect(action).toEqual({
            type: '[Metamodel] Load Output Category List',
            outputFamilyId: 1
        });
    });

    it('should create loadOutputCategoryListSuccess action', () => {
        const action = outputCategoryActions.loadOutputCategoryListSuccess({ outputCategories: OUTPUT_CATEGORY_LIST });
        expect(action).toEqual({
            type: '[Metamodel] Load Output Category List Success',
            outputCategories: OUTPUT_CATEGORY_LIST
        });
    });

    it('should create loadOutputCategoryListFail action', () => {
        const action = outputCategoryActions.loadOutputCategoryListFail();
        expect(action).toEqual({ type: '[Metamodel] Load Output Category List Fail' });
    });

    it('should create addOutputCategory action', () => {
        const action = outputCategoryActions.addOutputCategory({ outputFamilyId: 1, outputCategory: OUTPUT_CATEGORY });
        expect(action).toEqual({
            type: '[Metamodel] Add Output Category',
            outputFamilyId: 1,
            outputCategory: OUTPUT_CATEGORY
        });
    });

    it('should create addOutputCategorySuccess action', () => {
        const action = outputCategoryActions.addOutputCategorySuccess({ outputCategory: OUTPUT_CATEGORY });
        expect(action).toEqual({
            type: '[Metamodel] Add Output Category Success',
            outputCategory: OUTPUT_CATEGORY
        });
    });

    it('should create addOutputCategoryFail action', () => {
        const action = outputCategoryActions.addOutputCategoryFail();
        expect(action).toEqual({
            type: '[Metamodel] Add Output Category Fail'
        });
    });

    it('should create editOutputCategory action', () => {
        const action = outputCategoryActions.editOutputCategory({ outputFamilyId: 1, outputCategory: OUTPUT_CATEGORY });
        expect(action).toEqual({
            type: '[Metamodel] Edit Output Category',
            outputFamilyId: 1,
            outputCategory: OUTPUT_CATEGORY
        });
    });

    it('should create editOutputCategorySuccess action', () => {
        const action = outputCategoryActions.editOutputCategorySuccess({ outputCategory: OUTPUT_CATEGORY });
        expect(action).toEqual({
            type: '[Metamodel] Edit Output Category Success',
            outputCategory: OUTPUT_CATEGORY
        });
    });

    it('should create editOutputCategoryFail action', () => {
        const action = outputCategoryActions.editOutputCategoryFail();
        expect(action).toEqual({
            type: '[Metamodel] Edit Output Category Fail'
        });
    });

    it('should create deleteOutputCategory action', () => {
        const action = outputCategoryActions.deleteOutputCategory({ outputFamilyId: 1, outputCategory: OUTPUT_CATEGORY });
        expect(action).toEqual({
            type: '[Metamodel] Delete Output Category',
            outputFamilyId: 1,
            outputCategory: OUTPUT_CATEGORY
        });
    });

    it('should create deleteOutputCategorySuccess action', () => {
        const action = outputCategoryActions.deleteOutputCategorySuccess({ outputCategory: OUTPUT_CATEGORY });
        expect(action).toEqual({
            type: '[Metamodel] Delete Output Category Success',
            outputCategory: OUTPUT_CATEGORY
        });
    });

    it('should create deleteOutputCategoryFail action', () => {
        const action = outputCategoryActions.deleteOutputCategoryFail();
        expect(action).toEqual({
            type: '[Metamodel] Delete Output Category Fail'
        });
    });
});
