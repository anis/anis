/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AppConfigService } from 'src/app/app-config.service';

@Injectable()
export class AnisHttpClientService {
    constructor(private http: HttpClient, private config: AppConfigService) { }

    public get<T>(route: string): Observable<T> {
        return this.selectData(this.http.get<{ status: number; data: T; }>(`${this.config.apiUrl}${route}`));
    }

    public post<T>(route: string, newRecord: T): Observable<T> {
        return this.selectData(this.http.post<{ status: number; data: T; }>(`${this.config.apiUrl}${route}`, newRecord));
    }

    public put<T>(route: string, editedRecord: T): Observable<T> {
        return this.selectData(this.http.put<{ status: number; data: T; }>(`${this.config.apiUrl}${route}`, editedRecord));
    }

    public delete(route: string): Observable<{ message: string }> {
        return this.selectData(this.http.delete<{ status: number; data: { message: string } }>(`${this.config.apiUrl}${route}`));
    }

    private selectData<T>(response$: Observable<{ status: number; data: T; }>): Observable<T> {
        return response$.pipe(
            map(response => response.data)
        );
    }
}
