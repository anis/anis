/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as outputCategorySelector from './output-category.selector';
import * as fromOutputCategory from '../reducers/output-category.reducer';
import { OUTPUT_CATEGORY } from 'src/test-data';
import { OutputCategory } from '../models';

describe('[Metamodel][Selectors] OutputCategory selector', () => {
    let state: any;
    let outputCategory: OutputCategory;

    beforeEach(() => {
        outputCategory = { ...OUTPUT_CATEGORY };
        state = {
            metamodel: {
                outputCategory: {
                    ...fromOutputCategory.initialState,
                    outputCategoryListIsLoaded: true,
                    ids: [outputCategory.id],
                    entities: { [outputCategory.id]: outputCategory }
                },
            }
        };
    });

    it('should get outputCategory state', () => {
        expect(outputCategorySelector.selectOutputCategoryState(state)).toEqual(state.metamodel.outputCategory);
    });

    it('should get outputCategory IDs', () => {
        expect(outputCategorySelector.selectOutputCategoryIds(state).length).toEqual(1);
    });

    it('should get outputCategory entities', () => {
        expect(outputCategorySelector.selectOutputCategoryEntities(state)).toEqual({ [outputCategory.id]: outputCategory });
    });

    it('should get all outputCategorys', () => {
        expect(outputCategorySelector.selectAllOutputCategories(state).length).toEqual(1);
    });

    it('should get outputCategory count', () => {
        expect(outputCategorySelector.selectOutputCategoryTotal(state)).toEqual(1);
    });

    it('should get outputCategoryListIsLoading', () => {
        expect(outputCategorySelector.selectOutputCategoryListIsLoading(state)).toBe(false);
    });

    it('should get outputCategoryListIsLoaded', () => {
        expect(outputCategorySelector.selectOutputCategoryListIsLoaded(state)).toBe(true);
    });
});
