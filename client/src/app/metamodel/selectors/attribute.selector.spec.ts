/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as attributeSelector from './attribute.selector';
import * as fromAttribute from '../reducers/attribute.reducer';
import { ATTRIBUTE } from 'src/test-data';
import { Attribute } from '../models';

describe('[Metamodel][Selectors] Attribute selector', () => {
    let state: any;
    let attribute: Attribute;

    beforeEach(() => {
        attribute = { ...ATTRIBUTE };
        state = {
            router: { state: { params: { id: attribute.id }}},
            metamodel: {
                attribute: {
                    ...fromAttribute.initialState,
                    attributeListIsLoaded: true,
                    ids: [attribute.id],
                    entities: { [attribute.id]: attribute }
                },
            }
        };
    });

    it('should get attribute state', () => {
        expect(attributeSelector.selectAttributeState(state)).toEqual(state.metamodel.attribute);
    });

    it('should get attribute IDs', () => {
        expect(attributeSelector.selectAttributeIds(state).length).toEqual(1);
    });

    it('should get attribute entities', () => {
        expect(attributeSelector.selectAttributeEntities(state)).toEqual({ [attribute.id]: attribute });
    });

    it('should get all attributes', () => {
        expect(attributeSelector.selectAllAttributes(state).length).toEqual(1);
    });

    it('should get attribute count', () => {
        expect(attributeSelector.selectAttributeTotal(state)).toEqual(1);
    });

    it('should get attributeListIsLoading', () => {
        expect(attributeSelector.selectAttributeListIsLoading(state)).toBe(false);
    });

    it('should get attributeListIsLoaded', () => {
        expect(attributeSelector.selectAttributeListIsLoaded(state)).toBe(true);
    });
});
