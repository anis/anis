/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromMenuItem from '../reducers/menu-item.reducer';
import { MenuFamily, Webpage, Url } from '../models';

export const selectMenuItemState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.menuItem
);

export const selectMenuItemIds = createSelector(
    selectMenuItemState,
    fromMenuItem.selectMenuItemIds
);

export const selectMenuItemEntities = createSelector(
    selectMenuItemState,
    fromMenuItem.selectMenuItemEntities
);

export const selectAllMenuItems = createSelector(
    selectMenuItemState,
    fromMenuItem.selectAllMenuItems
);

export const selectMenuItemTotal = createSelector(
    selectMenuItemState,
    fromMenuItem.selectMenuItemTotal
);

export const selectMenuItemListIsLoading = createSelector(
    selectMenuItemState,
    fromMenuItem.selectMenuItemListIsLoading
);

export const selectMenuItemListIsLoaded = createSelector(
    selectMenuItemState,
    fromMenuItem.selectMenuItemListIsLoaded
);

export const selectMenuItemByRouteId = createSelector(
    selectMenuItemEntities,
    reducer.selectRouterState,
    (entities, router) => entities[router.state.params['id']]
);

export const selectMenuItemIdByRoute = createSelector(
    reducer.selectRouterState,
    router => router.state.params['id'] as number
);

export const selectWebpageByRouteId = createSelector(
    selectMenuItemByRouteId,
    (menuItem) => menuItem as Webpage
);

export const selectMenuFamilyByRouteId = createSelector(
    selectMenuItemByRouteId,
    (menuItem) => menuItem as MenuFamily
);

export const selectMenuFamilyByName = createSelector(
    selectAllMenuItems,
    reducer.selectRouterState,
    (menuItemList, router) => menuItemList
        .filter(menuItem => menuItem.type === 'menu_family')
        .find(menuItem => (menuItem as MenuFamily).name === router.state.params['fname']) as MenuFamily
);

export const selectUrlByRouteId = createSelector(
    selectMenuItemByRouteId,
    (menuItem) => menuItem as Url
);

export const selectWebpageByRouteName = createSelector(
    selectAllMenuItems,
    reducer.selectRouterState,
    (menuItemList, router) =>  menuItemList.find(menuItem => (menuItem as Webpage).name === router.state.params['name']) as Webpage
);

export const selectWebpageFromFamilyByRouteName = createSelector(
    selectMenuFamilyByName,
    reducer.selectRouterState,
    (menuFamily, router) => menuFamily ? menuFamily.items
        .find(menuItem => (menuItem as Webpage).name === router.state.params['name']) as Webpage
        : null
);

export const selectNextMenuItemId = createSelector(
    selectAllMenuItems,
    (menuItemList) => Math.max(0, ...menuItemList.map(menuItem => menuItem.id)) + 1
);
