/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as designConfigSelector from './design-config.selector';
import * as fromDesignConfig from '../reducers/design-config.reducer';
import { DESIGN_CONFIG } from 'src/test-data';
import { DesignConfig } from '../models';

describe('[Metamodel][Selectors] Design config selector', () => {
    let state: any;
    let designConfig: DesignConfig;

    beforeEach(() => {
        designConfig = { ...DESIGN_CONFIG };
        state = {
            metamodel: {
                designConfig: {
                    ...fromDesignConfig.initialState,
                    designConfig,
                    designConfigIsLoaded: true
                }
            }
        };
    });

    it('should get design config state', () => {
        expect(designConfigSelector.selectDesignConfigState(state)).toEqual(state.metamodel.designConfig);
    });

    it('should get design config', () => {
        expect(designConfigSelector.selectDesignConfig(state)).toEqual(designConfig);
    });

    it('should get design config is loading', () => {
        expect(designConfigSelector.selectDesignConfigIsLoading(state)).toEqual(false);
    });

    it('should get design config is loaded', () => {
        expect(designConfigSelector.selectDesignConfigIsLoaded(state)).toEqual(true);
    });
});
