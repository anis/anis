/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as detailConfigSelector from './detail-config.selector';
import * as fromDetailConfig from '../reducers/detail-config.reducer';
import { DETAIL_CONFIG } from 'src/test-data';
import { DetailConfig } from '../models';

describe('[Metamodel][Selectors] Detail config selector', () => {
    let state: any;
    let detailConfig: DetailConfig;

    beforeEach(() => {
        detailConfig = { ...DETAIL_CONFIG };
        state = {
            metamodel: {
                detailConfig: {
                    ...fromDetailConfig.initialState,
                    detailConfig,
                    detailConfigIsLoaded: true
                }
            }
        };
    });

    it('should get detail config state', () => {
        expect(detailConfigSelector.selectDetailConfigState(state)).toEqual(state.metamodel.detailConfig);
    });

    it('should get detail config', () => {
        expect(detailConfigSelector.selectDetailConfig(state)).toEqual(detailConfig);
    });

    it('should get detail config is loading', () => {
        expect(detailConfigSelector.selectDetailConfigIsLoading(state)).toEqual(false);
    });

    it('should get detail config is loaded', () => {
        expect(detailConfigSelector.selectDetailConfigIsLoaded(state)).toEqual(true);
    });
});
