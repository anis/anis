/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as menuFamilyItemSelector from './menu-family-item.selector';
import * as fromMenuFamilyItem from '../reducers/menu-family-item.reducer';
import { MENU_ITEM_LIST } from 'src/test-data';
import { MenuItem } from '../models';

describe('[Metamodel][Selectors] MenuFamilyItem selector', () => {
    let state: any;
    let menuFamilyItem: MenuItem;

    beforeEach(() => {
        menuFamilyItem = { ...MENU_ITEM_LIST[0] };
        state = {
            router: { state: { params: { fid: menuFamilyItem.id }}},
            metamodel: {
                menuFamilyItem: {
                    ...fromMenuFamilyItem.initialState,
                    menuFamilyItemListIsLoaded: true,
                    ids: [menuFamilyItem.id],
                    entities: { [menuFamilyItem.id]: menuFamilyItem }
                },
            }
        };
    });

    it('should get menuFamilyItem state', () => {
        expect(menuFamilyItemSelector.selectMenuFamilyItemState(state)).toEqual(state.metamodel.menuFamilyItem);
    });

    it('should get menuFamilyItem IDs', () => {
        expect(menuFamilyItemSelector.selectMenuFamilyItemIds(state).length).toEqual(1);
    });

    it('should get menuFamilyItem entities', () => {
        expect(menuFamilyItemSelector.selectMenuFamilyItemEntities(state)).toEqual({ [menuFamilyItem.id]: menuFamilyItem });
    });

    it('should get all menuFamilyItems', () => {
        expect(menuFamilyItemSelector.selectAllMenuFamilyItems(state).length).toEqual(1);
    });

    it('should get menuFamilyItem count', () => {
        expect(menuFamilyItemSelector.selectMenuFamilyItemTotal(state)).toEqual(1);
    });

    it('should get menuFamilyItemListIsLoading', () => {
        expect(menuFamilyItemSelector.selectMenuFamilyItemListIsLoading(state)).toBe(false);
    });

    it('should get menuFamilyItemListIsLoaded', () => {
        expect(menuFamilyItemSelector.selectMenuFamilyItemListIsLoaded(state)).toBe(true);
    });

    it('should get menuFamilyItem by route', () => {
        expect(menuFamilyItemSelector.selectMenuFamilyItemByRouteId(state)).toEqual(menuFamilyItem);
    });
});
