/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromCriteriaFamily from '../reducers/criteria-family.reducer';

export const selectCriteriaFamilyState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.criteriaFamily
);

export const selectCriteriaFamilyIds = createSelector(
    selectCriteriaFamilyState,
    fromCriteriaFamily.selectCriteriaFamilyIds
);

export const selectCriteriaFamilyEntities = createSelector(
    selectCriteriaFamilyState,
    fromCriteriaFamily.selectCriteriaFamilyEntities
);

export const selectAllCriteriaFamilies = createSelector(
    selectCriteriaFamilyState,
    fromCriteriaFamily.selectAllCriteriaFamilies
);

export const selectCriteriaFamilyTotal = createSelector(
    selectCriteriaFamilyState,
    fromCriteriaFamily.selectCriteriaFamilyTotal
);

export const selectCriteriaFamilyListIsLoading = createSelector(
    selectCriteriaFamilyState,
    fromCriteriaFamily.selectCriteriaFamilyListIsLoading
);

export const selectCriteriaFamilyListIsLoaded = createSelector(
    selectCriteriaFamilyState,
    fromCriteriaFamily.selectCriteriaFamilyListIsLoaded
);

export const selectNextCriteriaFamilyId = createSelector(
    selectAllCriteriaFamilies,
    (criteriaFamilies) => Math.max(0, ...criteriaFamilies.map(criteriaFamily => criteriaFamily.id)) + 1
);
