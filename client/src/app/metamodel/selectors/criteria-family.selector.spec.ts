/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as criteriaFamilySelector from './criteria-family.selector';
import * as fromCriteriaFamily from '../reducers/criteria-family.reducer';
import { CRITERIA_FAMILY } from 'src/test-data';
import { CriteriaFamily } from '../models';

describe('[Metamodel][Selectors] CriteriaFamily selector', () => {
    let state: any;
    let criteriaFamily: CriteriaFamily;

    beforeEach(() => {
        criteriaFamily = { ...CRITERIA_FAMILY };
        state = {
            metamodel: {
                criteriaFamily: {
                    ...fromCriteriaFamily.initialState,
                    criteriaFamilyListIsLoaded: true,
                    ids: [criteriaFamily.id],
                    entities: { [criteriaFamily.id]: criteriaFamily }
                },
            }
        };
    });

    it('should get criteriaFamily state', () => {
        expect(criteriaFamilySelector.selectCriteriaFamilyState(state)).toEqual(state.metamodel.criteriaFamily);
    });

    it('should get criteriaFamily IDs', () => {
        expect(criteriaFamilySelector.selectCriteriaFamilyIds(state).length).toEqual(1);
    });

    it('should get criteriaFamily entities', () => {
        expect(criteriaFamilySelector.selectCriteriaFamilyEntities(state)).toEqual({ [criteriaFamily.id]: criteriaFamily });
    });

    it('should get all criteriaFamilys', () => {
        expect(criteriaFamilySelector.selectAllCriteriaFamilies(state).length).toEqual(1);
    });

    it('should get criteriaFamily count', () => {
        expect(criteriaFamilySelector.selectCriteriaFamilyTotal(state)).toEqual(1);
    });

    it('should get criteriaFamilyListIsLoading', () => {
        expect(criteriaFamilySelector.selectCriteriaFamilyListIsLoading(state)).toBe(false);
    });

    it('should get criteriaFamilyListIsLoaded', () => {
        expect(criteriaFamilySelector.selectCriteriaFamilyListIsLoaded(state)).toBe(true);
    });
});
