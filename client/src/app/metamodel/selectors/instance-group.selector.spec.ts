/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as instanceGroupSelector from './instance-group.selector';
import * as fromInstanceGroup from '../reducers/instance-group.reducer';
import { INSTANCE } from 'src/test-data';
import { InstanceGroup } from '../models';

describe('[Metamodel][Selectors] Instance group selector', () => {
    let state: any;
    let instanceGroup: InstanceGroup;

    beforeEach(() => {
        instanceGroup = { ...INSTANCE.groups[0] };
        state = {
            metamodel: {
                instanceGroup: {
                    ...fromInstanceGroup.initialState,
                    instanceGroupListIsLoaded: true,
                    ids: [instanceGroup.role],
                    entities: { [instanceGroup.role]: instanceGroup }
                },
            }
        };
    });

    it('should get instance group state', () => {
        expect(instanceGroupSelector.selectInstanceGroupState(state)).toEqual(state.metamodel.instanceGroup);
    });

    it('should get instance group IDs', () => {
        expect(instanceGroupSelector.selectInstanceGroupIds(state).length).toEqual(1);
    });

    it('should get instance group entities', () => {
        expect(instanceGroupSelector.selectInstanceGroupEntities(state)).toEqual({ [instanceGroup.role]: instanceGroup });
    });

    it('should get all instance groups', () => {
        expect(instanceGroupSelector.selectAllInstanceGroups(state).length).toEqual(1);
    });

    it('should get instance group count', () => {
        expect(instanceGroupSelector.selectInstanceGroupTotal(state)).toEqual(1);
    });

    it('should get instanceGroupListIsLoading', () => {
        expect(instanceGroupSelector.selectInstanceGroupListIsLoading(state)).toBe(false);
    });

    it('should get instanceGroupListIsLoaded', () => {
        expect(instanceGroupSelector.selectInstanceGroupListIsLoaded(state)).toBe(true);
    });
});
