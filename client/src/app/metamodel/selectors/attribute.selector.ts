/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromAttribute from '../reducers/attribute.reducer';

export const selectAttributeState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.attribute
);

export const selectAttributeIds = createSelector(
    selectAttributeState,
    fromAttribute.selectAttributeIds
);

export const selectAttributeEntities = createSelector(
    selectAttributeState,
    fromAttribute.selectAttributeEntities
);

export const selectAllAttributes = createSelector(
    selectAttributeState,
    fromAttribute.selectAllAttributes
);

export const selectAttributeTotal = createSelector(
    selectAttributeState,
    fromAttribute.selectAttributeTotal
);

export const selectAttributeListIsLoading = createSelector(
    selectAttributeState,
    fromAttribute.selectAttributeListIsLoading
);

export const selectAttributeListIsLoaded = createSelector(
    selectAttributeState,
    fromAttribute.selectAttributeListIsLoaded
);

export const selectAllAttributesOrderById = createSelector(
    selectAllAttributes,
    (attributeList) => attributeList.sort((a, b) => a.id - b.id)
);

export const selectAllAttributesOrderByDetailDisplay = createSelector(
    selectAllAttributes,
    (attributeList) => attributeList.sort((a, b) => a.detail_display - b.detail_display)
);
