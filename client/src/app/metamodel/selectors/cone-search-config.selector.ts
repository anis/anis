/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromConeSearchConfig from '../reducers/cone-search-config.reducer';

export const selectConeSearchConfigState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.coneSearchConfig
);

export const selectConeSearchConfig = createSelector(
    selectConeSearchConfigState,
    fromConeSearchConfig.selectConeSearchConfig
);

export const selectConeSearchConfigIsLoading = createSelector(
    selectConeSearchConfigState,
    fromConeSearchConfig.selectConeSearchConfigIsLoading
);

export const selectConeSearchConfigIsLoaded = createSelector(
    selectConeSearchConfigState,
    fromConeSearchConfig.selectConeSearchConfigIsLoaded
);
