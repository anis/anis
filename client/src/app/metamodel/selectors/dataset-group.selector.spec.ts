/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as datasetGroupSelector from './dataset-group.selector';
import * as fromDatasetGroup from '../reducers/dataset-group.reducer';
import { DATASET } from 'src/test-data';
import { DatasetGroup } from '../models';

describe('[Metamodel][Selectors] Dataset group selector', () => {
    let state: any;
    let datasetGroup: DatasetGroup;

    beforeEach(() => {
        datasetGroup = { ...DATASET.groups[0] };
        state = {
            metamodel: {
                datasetGroup: {
                    ...fromDatasetGroup.initialState,
                    datasetGroupListIsLoaded: true,
                    ids: [datasetGroup.role],
                    entities: { [datasetGroup.role]: datasetGroup }
                },
            }
        };
    });

    it('should get dataset group state', () => {
        expect(datasetGroupSelector.selectDatasetGroupState(state)).toEqual(state.metamodel.datasetGroup);
    });

    it('should get dataset group IDs', () => {
        expect(datasetGroupSelector.selectDatasetGroupIds(state).length).toEqual(1);
    });

    it('should get dataset group entities', () => {
        expect(datasetGroupSelector.selectDatasetGroupEntities(state)).toEqual({ [datasetGroup.role]: datasetGroup });
    });

    it('should get all dataset groups', () => {
        expect(datasetGroupSelector.selectAllDatasetGroups(state).length).toEqual(1);
    });

    it('should get dataset group count', () => {
        expect(datasetGroupSelector.selectDatasetGroupTotal(state)).toEqual(1);
    });

    it('should get datasetGroupListIsLoading', () => {
        expect(datasetGroupSelector.selectDatasetGroupListIsLoading(state)).toBe(false);
    });

    it('should get datasetGroupListIsLoaded', () => {
        expect(datasetGroupSelector.selectDatasetGroupListIsLoaded(state)).toBe(true);
    });
});
