/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as databaseSelector from './database.selector';
import * as fromDatabase from '../reducers/database.reducer';
import { DATABASE } from 'src/test-data';
import { Database } from '../models';

describe('[Metamodel][Selectors] Database selector', () => {
    let state: any;
    let database: Database;

    beforeEach(() => {
        database = { ...DATABASE };
        state = {
            router: { state: { params: { id: database.id }}},
            metamodel: {
                database: {
                    ...fromDatabase.initialState,
                    databaseListIsLoaded: true,
                    ids: [database.id],
                    entities: { [database.id]: database }
                },
            }
        };
    });

    it('should get database state', () => {
        expect(databaseSelector.selectDatabaseState(state)).toEqual(state.metamodel.database);
    });

    it('should get database IDs', () => {
        expect(databaseSelector.selectDatabaseIds(state).length).toEqual(1);
    });

    it('should get database entities', () => {
        expect(databaseSelector.selectDatabaseEntities(state)).toEqual({ [database.id]: database });
    });

    it('should get all databases', () => {
        expect(databaseSelector.selectAllDatabases(state).length).toEqual(1);
    });

    it('should get database count', () => {
        expect(databaseSelector.selectDatabaseTotal(state)).toEqual(1);
    });

    it('should get databaseListIsLoading', () => {
        expect(databaseSelector.selectDatabaseListIsLoading(state)).toBe(false);
    });

    it('should get databaseListIsLoaded', () => {
        expect(databaseSelector.selectDatabaseListIsLoaded(state)).toBe(true);
    });

    it('should get database by route', () => {
        expect(databaseSelector.selectDatabaseByRouteId(state)).toEqual(database);
    });
});
