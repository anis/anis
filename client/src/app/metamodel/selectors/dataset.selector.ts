/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromDataset from '../reducers/dataset.reducer';

export const selectDatasetState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.dataset
);

export const selectDatasetIds = createSelector(
    selectDatasetState,
    fromDataset.selectDatasetIds
);

export const selectDatasetEntities = createSelector(
    selectDatasetState,
    fromDataset.selectDatasetEntities
);

export const selectAllDatasets = createSelector(
    selectDatasetState,
    fromDataset.selectAllDatasets
);

export const selectDatasetTotal = createSelector(
    selectDatasetState,
    fromDataset.selectDatasetTotal
);

export const selectDatasetListIsLoading = createSelector(
    selectDatasetState,
    fromDataset.selectDatasetListIsLoading
);

export const selectDatasetListIsLoaded = createSelector(
    selectDatasetState,
    fromDataset.selectDatasetListIsLoaded
);

export const selectDatasetByRouteName = createSelector(
    selectDatasetEntities,
    reducer.selectRouterState,
    (entities, router) => entities[router.state.params['dname']]
);

export const selectDatasetNameByRoute = createSelector(
    reducer.selectRouterState,
    router => router.state.params['dname'] as string
);

export const selectAllConeSearchDatasets = createSelector(
    selectAllDatasets,
    datasetList => datasetList.filter(dataset => dataset.cone_search_enabled)
);
