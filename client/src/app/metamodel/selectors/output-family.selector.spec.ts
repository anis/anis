/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as outputFamilySelector from './output-family.selector';
import * as fromOutputFamily from '../reducers/output-family.reducer';
import { OUTPUT_FAMILY } from 'src/test-data';
import { OutputFamily } from '../models';

describe('[Metamodel][Selectors] OutputFamily selector', () => {
    let state: any;
    let outputFamily: OutputFamily;

    beforeEach(() => {
        outputFamily = { ...OUTPUT_FAMILY };
        state = {
            metamodel: {
                outputFamily: {
                    ...fromOutputFamily.initialState,
                    outputFamilyListIsLoaded: true,
                    ids: [outputFamily.id],
                    entities: { [outputFamily.id]: outputFamily }
                },
            }
        };
    });

    it('should get outputFamily state', () => {
        expect(outputFamilySelector.selectOutputFamilyState(state)).toEqual(state.metamodel.outputFamily);
    });

    it('should get outputFamily IDs', () => {
        expect(outputFamilySelector.selectOutputFamilyIds(state).length).toEqual(1);
    });

    it('should get outputFamily entities', () => {
        expect(outputFamilySelector.selectOutputFamilyEntities(state)).toEqual({ [outputFamily.id]: outputFamily });
    });

    it('should get all outputFamilys', () => {
        expect(outputFamilySelector.selectAllOutputFamilies(state).length).toEqual(1);
    });

    it('should get outputFamily count', () => {
        expect(outputFamilySelector.selectOutputFamilyTotal(state)).toEqual(1);
    });

    it('should get outputFamilyListIsLoading', () => {
        expect(outputFamilySelector.selectOutputFamilyListIsLoading(state)).toBe(false);
    });

    it('should get outputFamilyListIsLoaded', () => {
        expect(outputFamilySelector.selectOutputFamilyListIsLoaded(state)).toBe(true);
    });
});
