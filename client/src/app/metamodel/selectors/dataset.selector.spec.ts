/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as datasetSelector from './dataset.selector';
import * as fromDataset from '../reducers/dataset.reducer';
import { DATASET } from 'src/test-data';
import { Dataset } from '../models';

describe('[Metamodel][Selectors] Dataset selector', () => {
    let state: any;
    let dataset: Dataset;

    beforeEach(() => {
        dataset = { ...DATASET };
        state = {
            router: { state: { params: { dname: dataset.name }}},
            metamodel: {
                dataset: {
                    ...fromDataset.initialState,
                    datasetListIsLoaded: true,
                    ids: [dataset.name],
                    entities: { [dataset.name]: dataset }
                },
            }
        };
    });

    it('should get dataset state', () => {
        expect(datasetSelector.selectDatasetState(state)).toEqual(state.metamodel.dataset);
    });

    it('should get dataset IDs', () => {
        expect(datasetSelector.selectDatasetIds(state).length).toEqual(1);
    });

    it('should get dataset entities', () => {
        expect(datasetSelector.selectDatasetEntities(state)).toEqual({ [dataset.name]: dataset });
    });

    it('should get all datasets', () => {
        expect(datasetSelector.selectAllDatasets(state).length).toEqual(1);
    });

    it('should get dataset count', () => {
        expect(datasetSelector.selectDatasetTotal(state)).toEqual(1);
    });

    it('should get datasetListIsLoading', () => {
        expect(datasetSelector.selectDatasetListIsLoading(state)).toBe(false);
    });

    it('should get datasetListIsLoaded', () => {
        expect(datasetSelector.selectDatasetListIsLoaded(state)).toBe(true);
    });

    it('should get dataset by route', () => {
        expect(datasetSelector.selectDatasetByRouteName(state)).toEqual(dataset);
    });

    it('should get dataset name by route', () => {
        expect(datasetSelector.selectDatasetNameByRoute(state)).toEqual(dataset.name);
    });
});
