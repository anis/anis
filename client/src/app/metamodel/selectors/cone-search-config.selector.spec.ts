/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as coneSearchConfigSelector from './cone-search-config.selector';
import * as fromConeSearchConfig from '../reducers/cone-search-config.reducer';
import { CONE_SEARCH_CONFIG } from 'src/test-data';
import { ConeSearchConfig } from '../models';

describe('[Metamodel][Selectors] ConeSearch config selector', () => {
    let state: any;
    let coneSearchConfig: ConeSearchConfig;

    beforeEach(() => {
        coneSearchConfig = { ...CONE_SEARCH_CONFIG };
        state = {
            metamodel: {
                coneSearchConfig: {
                    ...fromConeSearchConfig.initialState,
                    coneSearchConfig,
                    coneSearchConfigIsLoaded: true
                }
            }
        };
    });

    it('should get coneSearch config state', () => {
        expect(coneSearchConfigSelector.selectConeSearchConfigState(state)).toEqual(state.metamodel.coneSearchConfig);
    });

    it('should get coneSearch config', () => {
        expect(coneSearchConfigSelector.selectConeSearchConfig(state)).toEqual(coneSearchConfig);
    });

    it('should get coneSearch config is loading', () => {
        expect(coneSearchConfigSelector.selectConeSearchConfigIsLoading(state)).toEqual(false);
    });

    it('should get coneSearch config is loaded', () => {
        expect(coneSearchConfigSelector.selectConeSearchConfigIsLoaded(state)).toEqual(true);
    });
});
