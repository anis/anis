/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as datasetFamilySelector from './dataset-family.selector';
import * as fromDatasetFamily from '../reducers/dataset-family.reducer';
import { DATASET_FAMILY } from 'src/test-data';
import { DatasetFamily } from '../models';

describe('[Metamodel][Selectors] DatasetFamily group selector', () => {
    let state: any;
    let datasetFamily: DatasetFamily;

    beforeEach(() => {
        datasetFamily = { ...DATASET_FAMILY };
        state = {
            metamodel: {
                datasetFamily: {
                    ...fromDatasetFamily.initialState,
                    datasetFamilyListIsLoaded: true,
                    ids: [datasetFamily.id],
                    entities: { [datasetFamily.id]: datasetFamily }
                },
            }
        };
    });

    it('should get dataset family state', () => {
        expect(datasetFamilySelector.selectDatasetFamilyState(state)).toEqual(state.metamodel.datasetFamily);
    });

    it('should get dataset family IDs', () => {
        expect(datasetFamilySelector.selectDatasetFamilyIds(state).length).toEqual(1);
    });

    it('should get dataset family entities', () => {
        expect(datasetFamilySelector.selectDatasetFamilyEntities(state)).toEqual({ [datasetFamily.id]: datasetFamily });
    });

    it('should get all dataset family', () => {
        expect(datasetFamilySelector.selectAllDatasetFamilies(state).length).toEqual(1);
    });

    it('should get datasetFamily count', () => {
        expect(datasetFamilySelector.selectDatasetFamilyTotal(state)).toEqual(1);
    });

    it('should get datasetFamilyListIsLoading', () => {
        expect(datasetFamilySelector.selectDatasetFamilyListIsLoading(state)).toBe(false);
    });

    it('should get datasetFamilyListIsLoaded', () => {
        expect(datasetFamilySelector.selectDatasetFamilyListIsLoaded(state)).toBe(true);
    });
});
