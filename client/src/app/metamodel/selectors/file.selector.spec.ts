/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as fileSelector from './file.selector';
import * as fromFile from '../reducers/file.reducer';
import { FILE } from 'src/test-data';
import { File } from '../models';

describe('[Metamodel][Selectors] File selector', () => {
    let state: any;
    let file: File;

    beforeEach(() => {
        file = { ...FILE };
        state = {
            router: { state: { params: { id: file.id }}},
            metamodel: {
                file: {
                    ...fromFile.initialState,
                    fileListIsLoaded: true,
                    ids: [file.id],
                    entities: { [file.id]: file }
                },
            }
        };
    });

    it('should get file state', () => {
        expect(fileSelector.selectFileState(state)).toEqual(state.metamodel.file);
    });

    it('should get file IDs', () => {
        expect(fileSelector.selectFileIds(state).length).toEqual(1);
    });

    it('should get file entities', () => {
        expect(fileSelector.selectFileEntities(state)).toEqual({ [file.id]: file });
    });

    it('should get all files', () => {
        expect(fileSelector.selectAllFiles(state).length).toEqual(1);
    });

    it('should get file count', () => {
        expect(fileSelector.selectFileTotal(state)).toEqual(1);
    });

    it('should get fileListIsLoading', () => {
        expect(fileSelector.selectFileListIsLoading(state)).toBe(false);
    });

    it('should get fileListIsLoaded', () => {
        expect(fileSelector.selectFileListIsLoaded(state)).toBe(true);
    });

    it('should get file by route', () => {
        expect(fileSelector.selectFileByRouteId(state)).toEqual(file);
    });
});
