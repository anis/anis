/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as menuItemSelector from './menu-item.selector';
import * as fromMenuItem from '../reducers/menu-item.reducer';
import { MENU_ITEM_LIST } from 'src/test-data';
import { MenuItem } from '../models';

describe('[Metamodel][Selectors] MenuItem selector', () => {
    let state: any;
    let menuItem: MenuItem;

    beforeEach(() => {
        menuItem = { ...MENU_ITEM_LIST[0] };
        state = {
            router: { state: { params: { id: menuItem.id }}},
            metamodel: {
                menuItem: {
                    ...fromMenuItem.initialState,
                    menuItemListIsLoaded: true,
                    ids: [menuItem.id],
                    entities: { [menuItem.id]: menuItem }
                },
            }
        };
    });

    it('should get menuItem state', () => {
        expect(menuItemSelector.selectMenuItemState(state)).toEqual(state.metamodel.menuItem);
    });

    it('should get menuItem IDs', () => {
        expect(menuItemSelector.selectMenuItemIds(state).length).toEqual(1);
    });

    it('should get menuItem entities', () => {
        expect(menuItemSelector.selectMenuItemEntities(state)).toEqual({ [menuItem.id]: menuItem });
    });

    it('should get all menuItems', () => {
        expect(menuItemSelector.selectAllMenuItems(state).length).toEqual(1);
    });

    it('should get menuItem count', () => {
        expect(menuItemSelector.selectMenuItemTotal(state)).toEqual(1);
    });

    it('should get menuItemListIsLoading', () => {
        expect(menuItemSelector.selectMenuItemListIsLoading(state)).toBe(false);
    });

    it('should get menuItemListIsLoaded', () => {
        expect(menuItemSelector.selectMenuItemListIsLoaded(state)).toBe(true);
    });

    it('should get menuItem by route', () => {
        expect(menuItemSelector.selectMenuItemByRouteId(state)).toEqual(menuItem);
    });
});
