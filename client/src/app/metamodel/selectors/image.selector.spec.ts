/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as imageSelector from './image.selector';
import * as fromImage from '../reducers/image.reducer';
import { IMAGE } from 'src/test-data';
import { Image } from '../models';

describe('[Metamodel][Selectors] Image selector', () => {
    let state: any;
    let image: Image;

    beforeEach(() => {
        image = { ...IMAGE };
        state = {
            router: { state: { params: { id: image.id }}},
            metamodel: {
                image: {
                    ...fromImage.initialState,
                    imageListIsLoaded: true,
                    ids: [image.id],
                    entities: { [image.id]: image }
                },
            }
        };
    });

    it('should get image state', () => {
        expect(imageSelector.selectImageState(state)).toEqual(state.metamodel.image);
    });

    it('should get image IDs', () => {
        expect(imageSelector.selectImageIds(state).length).toEqual(1);
    });

    it('should get image entities', () => {
        expect(imageSelector.selectImageEntities(state)).toEqual({ [image.id]: image });
    });

    it('should get all images', () => {
        expect(imageSelector.selectAllImages(state).length).toEqual(1);
    });

    it('should get image count', () => {
        expect(imageSelector.selectImageTotal(state)).toEqual(1);
    });

    it('should get imageListIsLoading', () => {
        expect(imageSelector.selectImageListIsLoading(state)).toBe(false);
    });

    it('should get imageListIsLoaded', () => {
        expect(imageSelector.selectImageListIsLoaded(state)).toBe(true);
    });

    it('should get image by route', () => {
        expect(imageSelector.selectImageByRouteId(state)).toEqual(image);
    });
});
