/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as aliasConfigSelector from './alias-config.selector';
import * as fromAliasConfig from '../reducers/alias-config.reducer';
import { ALIAS_CONFIG } from 'src/test-data';
import { AliasConfig } from '../models';

describe('[Metamodel][Selectors] Alias config selector', () => {
    let state: any;
    let aliasConfig: AliasConfig;

    beforeEach(() => {
        aliasConfig = { ...ALIAS_CONFIG };
        state = {
            metamodel: {
                aliasConfig: {
                    ...fromAliasConfig.initialState,
                    aliasConfig,
                    aliasConfigIsLoaded: true
                }
            }
        };
    });

    it('should get alias config state', () => {
        expect(aliasConfigSelector.selectAliasConfigState(state)).toEqual(state.metamodel.aliasConfig);
    });

    it('should get alias config', () => {
        expect(aliasConfigSelector.selectAliasConfig(state)).toEqual(aliasConfig);
    });

    it('should get alias config is loading', () => {
        expect(aliasConfigSelector.selectAliasConfigIsLoading(state)).toEqual(false);
    });

    it('should get alias config is loaded', () => {
        expect(aliasConfigSelector.selectAliasConfigIsLoaded(state)).toEqual(true);
    });
});
