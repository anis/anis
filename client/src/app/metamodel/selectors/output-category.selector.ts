/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromOutputCategory from '../reducers/output-category.reducer';

export const selectOutputCategoryState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.outputCategory
);

export const selectOutputCategoryIds = createSelector(
    selectOutputCategoryState,
    fromOutputCategory.selectOutputCategoryIds
);

export const selectOutputCategoryEntities = createSelector(
    selectOutputCategoryState,
    fromOutputCategory.selectOutputCategoryEntities
);

export const selectAllOutputCategories = createSelector(
    selectOutputCategoryState,
    fromOutputCategory.selectAllOutputCategories
);

export const selectOutputCategoryTotal = createSelector(
    selectOutputCategoryState,
    fromOutputCategory.selectOutputCategoryTotal
);

export const selectOutputCategoryListIsLoading = createSelector(
    selectOutputCategoryState,
    fromOutputCategory.selectOutputCategoryListIsLoading
);

export const selectOutputCategoryListIsLoaded = createSelector(
    selectOutputCategoryState,
    fromOutputCategory.selectOutputCategoryListIsLoaded
);

export const selectNextOutputCategoryId = createSelector(
    selectAllOutputCategories,
    (outputCategories) => Math.max(0, ...outputCategories.map(outputCategory => outputCategory.id)) + 1
);
