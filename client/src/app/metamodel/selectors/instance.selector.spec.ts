/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as instanceSelector from './instance.selector';
import * as fromInstance from '../reducers/instance.reducer';
import { INSTANCE } from 'src/test-data';
import { Instance } from '../models';

describe('[Metamodel][Selectors] Instance selector', () => {
    let state: any;
    let instance: Instance;

    beforeEach(() => {
        instance = { ...INSTANCE };
        state = {
            router: { state: { params: { iname: instance.name }}},
            metamodel: {
                instance: {
                    ...fromInstance.initialState,
                    instanceListIsLoaded: true,
                    ids: [instance.name],
                    entities: { [instance.name]: instance }
                },
            }
        };
    });

    it('should get instance state', () => {
        expect(instanceSelector.selectInstanceState(state)).toEqual(state.metamodel.instance);
    });

    it('should get instance IDs', () => {
        expect(instanceSelector.selectInstanceIds(state).length).toEqual(1);
    });

    it('should get instance entities', () => {
        expect(instanceSelector.selectInstanceEntities(state)).toEqual({ [instance.name]: instance });
    });

    it('should get all instances', () => {
        expect(instanceSelector.selectAllInstances(state).length).toEqual(1);
    });

    it('should get instance count', () => {
        expect(instanceSelector.selectInstanceTotal(state)).toEqual(1);
    });

    it('should get instanceListIsLoading', () => {
        expect(instanceSelector.selectInstanceListIsLoading(state)).toBe(false);
    });

    it('should get instanceListIsLoaded', () => {
        expect(instanceSelector.selectInstanceListIsLoaded(state)).toBe(true);
    });

    it('should get instance by route', () => {
        expect(instanceSelector.selectInstanceByRouteName(state)).toEqual(instance);
    });

    it('should get instance name by route', () => {
        expect(instanceSelector.selectInstanceNameByRoute(state)).toEqual(instance.name);
    });
});
