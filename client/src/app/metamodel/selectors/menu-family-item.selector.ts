/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromMenuFamilyItem from '../reducers/menu-family-item.reducer';
import { MenuFamily, Webpage, Url } from '../models';

export const selectMenuFamilyItemState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.menuFamilyItem
);

export const selectMenuFamilyItemIds = createSelector(
    selectMenuFamilyItemState,
    fromMenuFamilyItem.selectMenuFamilyItemIds
);

export const selectMenuFamilyItemEntities = createSelector(
    selectMenuFamilyItemState,
    fromMenuFamilyItem.selectMenuFamilyItemEntities
);

export const selectAllMenuFamilyItems = createSelector(
    selectMenuFamilyItemState,
    fromMenuFamilyItem.selectAllMenuFamilyItems
);

export const selectMenuFamilyItemTotal = createSelector(
    selectMenuFamilyItemState,
    fromMenuFamilyItem.selectMenuFamilyItemTotal
);

export const selectMenuFamilyItemListIsLoading = createSelector(
    selectMenuFamilyItemState,
    fromMenuFamilyItem.selectMenuFamilyItemListIsLoading
);

export const selectMenuFamilyItemListIsLoaded = createSelector(
    selectMenuFamilyItemState,
    fromMenuFamilyItem.selectMenuFamilyItemListIsLoaded
);

export const selectMenuFamilyItemByRouteId = createSelector(
    selectMenuFamilyItemEntities,
    reducer.selectRouterState,
    (entities, router) => entities[router.state.params['fid']]
);

export const selectWebpageByRouteId = createSelector(
    selectMenuFamilyItemByRouteId,
    (menuFamilyItem) => menuFamilyItem as Webpage
);

export const selectMenuFamilyByRouteId = createSelector(
    selectMenuFamilyItemByRouteId,
    (menuFamilyItem) => menuFamilyItem as MenuFamily
);

export const selectUrlByRouteId = createSelector(
    selectMenuFamilyItemByRouteId,
    (menuFamilyItem) => menuFamilyItem as Url
);

export const selectWebpageByRouteName = createSelector(
    selectAllMenuFamilyItems,
    reducer.selectRouterState,
    (menuFamilyItemList, router) =>  menuFamilyItemList.find(menuFamilyItem => (menuFamilyItem as Webpage).name === router.state.params['name']) as Webpage
);

export const selectNextMenuFamilyItemId = createSelector(
    selectAllMenuFamilyItems,
    (menuFamilyItemList) => Math.max(0, ...menuFamilyItemList.map(menuFamilyItem => menuFamilyItem.id)) + 1
);
