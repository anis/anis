/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromInstanceGroup from '../reducers/instance-group.reducer';

export const selectInstanceGroupState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.instanceGroup
);

export const selectInstanceGroupIds = createSelector(
    selectInstanceGroupState,
    fromInstanceGroup.selectInstanceGroupIds
);

export const selectInstanceGroupEntities = createSelector(
    selectInstanceGroupState,
    fromInstanceGroup.selectInstanceGroupEntities
);

export const selectAllInstanceGroups = createSelector(
    selectInstanceGroupState,
    fromInstanceGroup.selectAllInstanceGroups
);

export const selectInstanceGroupTotal = createSelector(
    selectInstanceGroupState,
    fromInstanceGroup.selectInstanceGroupTotal
);

export const selectInstanceGroupListIsLoading = createSelector(
    selectInstanceGroupState,
    fromInstanceGroup.selectInstanceGroupListIsLoading
);

export const selectInstanceGroupListIsLoaded = createSelector(
    selectInstanceGroupState,
    fromInstanceGroup.selectInstanceGroupListIsLoaded
);

export const selectInstanceGroupByRouteId = createSelector(
    selectInstanceGroupEntities,
    reducer.selectRouterState,
    (entities, router) => entities[router.state.params['role']]
);
