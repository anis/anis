/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../metamodel.reducer';
import * as fromDesignConfig from '../reducers/design-config.reducer';

export const selectDesignConfigState = createSelector(
    reducer.getMetamodelState,
    (state: reducer.State) => state.designConfig
);

export const selectDesignConfig = createSelector(
    selectDesignConfigState,
    fromDesignConfig.selectDesignConfig
);

export const selectDesignConfigIsLoading = createSelector(
    selectDesignConfigState,
    fromDesignConfig.selectDesignConfigIsLoading
);

export const selectDesignConfigIsLoaded = createSelector(
    selectDesignConfigState,
    fromDesignConfig.selectDesignConfigIsLoaded
);
