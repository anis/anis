/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';

import { KeycloakAngularModule } from 'keycloak-angular';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '../shared/shared.module';
import { userReducer } from './user.reducer';
import { userEffects } from './store/effects';
import { userServices} from './store/services';

@NgModule({
    imports: [
        SharedModule,
        KeycloakAngularModule,
        StoreModule.forFeature('user', userReducer),
        EffectsModule.forFeature(userEffects)
    ],
    providers: [
        userServices
    ]
})
export class UserModule { }
