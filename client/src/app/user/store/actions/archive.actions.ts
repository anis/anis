/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createAction, props } from '@ngrx/store';

import { Archive } from '../models';

export const loadArchiveList = createAction('[User] Load Archive List');
export const loadArchiveListSuccess = createAction('[User] Load Archive List Success', props<{ archives: Archive[] }>());
export const loadArchiveListFail = createAction('[User] Load Archive List Fail');
