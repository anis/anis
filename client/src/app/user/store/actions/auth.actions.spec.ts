/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as authActions from './auth.actions';
import { USER_PROFILE } from 'src/test-data';

describe('[Auth] Auth actions', () => {
    it('should create login action', () => {
        const action = authActions.login({ redirectUri: '/instance/default' });
        expect(action).toEqual({ type: '[Auth] Login', redirectUri: '/instance/default' })
    });
    
    it('should create logout action', () => {
        const action = authActions.logout();
        expect(action).toEqual({ type: '[Auth] Logout' })
    });

    it('should create authSuccess action', () => {
        const action = authActions.authSuccess();
        expect(action).toEqual({ type: '[Auth] Auth Success' })
    });
    
    it('should create loadUserProfileSuccess action', () => {
        const action = authActions.loadUserProfileSuccess({ userProfile: USER_PROFILE });
        expect(action).toEqual({ type: '[Auth] Load User Profile Success', userProfile: USER_PROFILE });
    });

    it('should create loadUserRolesSuccess action', () => {
        const action = authActions.loadUserRolesSuccess({ userRoles: ['ROLE1', 'ROLE2'] });
        expect(action).toEqual({ type: '[Auth] Load User Roles Success', userRoles: ['ROLE1', 'ROLE2'] });
    });

    it('should create openEditProfile action', () => {
        const action = authActions.openEditProfile();
        expect(action).toEqual({ type: '[Auth] Edit Profile' });
    });
});
