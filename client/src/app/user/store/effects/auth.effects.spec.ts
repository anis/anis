/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable, of } from 'rxjs';
import { cold, hot } from 'jasmine-marbles';
import { KeycloakService } from 'keycloak-angular';

import * as authActions from '../actions/auth.actions';
import { AuthEffects } from './auth.effects';
import { AppConfigService } from 'src/app/app-config.service';

describe('[Auth] Auth effects', () => {
    let actions = new Observable();
    let effects: AuthEffects;
    let keycloakService: KeycloakService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                AuthEffects,
                AppConfigService,
                { provide: Router, useValue: { navigate: jest.fn() } },
                KeycloakService,
                provideMockActions(() => actions),
            ],
        }).compileComponents();
        effects = TestBed.inject(AuthEffects);
        TestBed.inject(AppConfigService);
        keycloakService = TestBed.inject(KeycloakService);
        TestBed.inject(Router);
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });

    it('login$ should call keycloak.login()', () => {
        const action = authActions.login({ redirectUri: 'test' });
        const spy = jest
            .spyOn(keycloakService, 'login')
            .mockImplementation(async () => {});
        actions = hot('a', { a: action });
        const expected = cold('a', { a: action });
        expect(effects.login$).toBeObservable(expected);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('logout$ should call keycloak.logout()', () => {
        const action = authActions.logout();
        const spy = jest
            .spyOn(keycloakService, 'logout')
            .mockImplementation(async () => {});
        actions = hot('a', { a: action });
        const expected = cold('a', { a: action });
        expect(effects.logout$).toBeObservable(expected);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('authSuccess$ should call keycloak.loadUserProfile()', () => {
        keycloakService.loadUserProfile = jest
            .fn()
            .mockImplementation(() => of({}));
        keycloakService.getUserRoles = jest
            .fn()
            .mockImplementation(() => ['test']);
        const action = authActions.authSuccess();
        actions = hot('-a', { a: action });
        const expected = cold('-(bc)', {
            b: authActions.loadUserProfileSuccess({ userProfile: {} }),
            c: authActions.loadUserRolesSuccess({
                userRoles: keycloakService.getUserRoles(),
            }),
        });
        expect(effects.authSuccess$).toBeObservable(expected);
    });

    it('openEditProfile$ should call window.open()', () => {
        const action = authActions.openEditProfile();
        const spy = jest.spyOn(window, 'open').mockImplementation(() => null);
        actions = hot('a', { a: action });
        const expected = cold('a', { a: action });
        expect(effects.openEditProfile$).toBeObservable(expected);
        expect(spy).toHaveBeenCalledTimes(1);
    });
});
