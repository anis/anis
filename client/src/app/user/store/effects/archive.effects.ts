/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';

import * as archiveActions from '../actions/archive.actions';
import { ArchiveService } from '../services/archive.service';

@Injectable()
export class ArchiveEffects {
    /**
     * Calls action to retrieve archive list.
     */
    loadArchives$ = createEffect((): any =>
        this.actions$.pipe(
            ofType(archiveActions.loadArchiveList),
            mergeMap(() => this.archiveService.retrieveArchives()
                .pipe(
                    map(archives => archiveActions.loadArchiveListSuccess({ archives })),
                    catchError(() => of(archiveActions.loadArchiveListFail()))
                )
            )
        )
    );
    
    constructor(
        private actions$: Actions,
        private archiveService: ArchiveService,
    ) {}
}
