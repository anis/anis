/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

export interface Archive {
    id: number;
    name: string;
    created: string;
    status: string;
    file_size: number;
    email: string;
    instance_name: string;
    dataset_name: string;
}
