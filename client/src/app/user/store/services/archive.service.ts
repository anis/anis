/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Archive } from '../models';
import { AnisHttpClientService } from 'src/app/metamodel/anis-http-client.service';

@Injectable()
export class ArchiveService {
    /**
     * @param AnisHttpClientService anisHttpClientService - Service used to call ANIS Server
     */
    constructor(private anisHttpClientService: AnisHttpClientService) { }

    /**
     * Retrieves archives for the current user
     *
     * @return Observable<Archive[]>
     */
    retrieveArchives(): Observable<Archive[]> {
        return this.anisHttpClientService.get<Archive[]>(`/archive/archive-list-by-user`);
    }
}
