/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as authSelector from './auth.selector';
import * as fromAuth from '../reducers/auth.reducer';
import { UserProfile } from '../models/user-profile.model';
import { USER_PROFILE } from 'src/test-data';

describe('[Auth] Auth selector', () => {
    let state: any;
    let userProfile: UserProfile;
    let userRoles: string[];

    beforeEach(() => {
        userProfile = USER_PROFILE;
        userRoles = ['ROLE1', 'ROLE2'];
        state = {
            user: {
                auth: {
                    ...fromAuth.initialState,
                    isAuthenticated: true,
                    userProfile,
                    userRoles
                }
            }
        };
    });

    it('should get auth state', () => {
        expect(authSelector.selectAuthState(state)).toEqual(state.user.auth);
    });

    it('should get is authenticated', () => {
        expect(authSelector.selectIsAuthenticated(state)).toBe(true);
    });

    it('should get auth user profile', () => {
        expect(authSelector.selectUserProfile(state)).toEqual(userProfile);
    });

    it('should get auth user roles', () => {
        expect(authSelector.selectUserRoles(state)).toEqual(userRoles);
    });
});
