/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../../user.reducer';
import * as fromArchive from '../reducers/archive.reducer';

export const selectArchiveState = createSelector(
    reducer.getUserState,
    (state: reducer.State) => state.archive
);

export const selectArchives = createSelector(
    selectArchiveState,
    fromArchive.selectArchives
);

export const selectArchiveListIsLoading = createSelector(
    selectArchiveState,
    fromArchive.selectArchiveListIsLoading
);

export const selectArchiveListIsLoaded = createSelector(
    selectArchiveState,
    fromArchive.selectArchiveListIsLoaded
);

export const selectInstanceNameByRoute = createSelector(
    reducer.selectRouterState,
    router => router.state.params['iname'] as string
);

export const selectArchiveListByInstanceName = createSelector(
    selectArchives,
    selectInstanceNameByRoute,
    (archives, instanceName) => archives.filter(archive => archive.instance_name === instanceName)
);
