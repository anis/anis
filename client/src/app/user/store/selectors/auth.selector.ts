/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createSelector } from '@ngrx/store';

import * as reducer from '../../user.reducer';
import * as fromAuth from '../reducers/auth.reducer';

export const selectAuthState = createSelector(
    reducer.getUserState,
    (state: reducer.State) => state.auth
);

export const selectIsAuthenticated = createSelector(
    selectAuthState,
    fromAuth.selectIsAuthenticated
);

export const selectUserProfile = createSelector(
    selectAuthState,
    fromAuth.selectUserProfile
);

export const selectUserRoles = createSelector(
    selectAuthState,
    fromAuth.selectUserRoles
);
