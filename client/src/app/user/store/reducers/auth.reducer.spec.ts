/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromAuth from './auth.reducer';
import * as authActions from '../actions/auth.actions';
import { USER_PROFILE } from 'src/test-data';

describe('[Auth] Auth reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromAuth.authReducer(fromAuth.initialState, action);

        expect(state).toBe(fromAuth.initialState);
    });

    it('authSuccess action should set isAuthenticated to true', () => {
        const action = authActions.authSuccess();
        const state = fromAuth.authReducer(fromAuth.initialState, action);

        expect(state.isAuthenticated).toBe(true);
        expect(state).not.toBe(fromAuth.initialState);
    });

    it('loadUserProfileSuccess action should set userProfile to new value', () => {
        const action = authActions.loadUserProfileSuccess({ userProfile: USER_PROFILE });
        const state = fromAuth.authReducer(fromAuth.initialState, action);

        expect(state.userProfile).toEqual(USER_PROFILE);
        expect(state).not.toBe(fromAuth.initialState);
    });

    it('loadUserRolesSuccess action should set userRoles to new value', () => {
        const action = authActions.loadUserRolesSuccess({userRoles: ['ROLE1', 'ROLE2']});
        const state = fromAuth.authReducer(fromAuth.initialState, action);

        expect(state.userRoles).toEqual(['ROLE1', 'ROLE2']);
        expect(state).not.toBe(fromAuth.initialState);
    });

    it('should get isAuthenticated', () => {
        const action = {} as Action;
        const state = fromAuth.authReducer(undefined, action);

        expect(fromAuth.selectIsAuthenticated(state)).toEqual(false);
    });

    it('should get userProfile', () => {
        const action = authActions.loadUserProfileSuccess({ userProfile: USER_PROFILE });
        const state = fromAuth.authReducer(fromAuth.initialState, action);

        expect(fromAuth.selectUserProfile(state)).toEqual(USER_PROFILE);
    });

    it('should getuserRoles', () => {
        const action = authActions.loadUserRolesSuccess({userRoles: ['ROLE1', 'ROLE2']});
        const state = fromAuth.authReducer(fromAuth.initialState, action);

        expect(fromAuth.selectUserRoles(state)).toEqual(['ROLE1', 'ROLE2']);
    });
});
