/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { createReducer, on } from '@ngrx/store';

import * as archiveActions from '../actions/archive.actions';
import { Archive } from '../models/archive.model';

export interface State {
    archives: Archive[];
    archiveListIsLoading: boolean;
    archiveListIsLoaded: boolean;
}

export const initialState: State = {
    archives: [],
    archiveListIsLoading: false,
    archiveListIsLoaded: false
}

export const archiveReducer = createReducer(
    initialState,
    on(archiveActions.loadArchiveList, state => ({
        ...state,
        archives: [],
        archiveListIsLoading: true,
        archiveListIsLoaded: false
    })),
    on(archiveActions.loadArchiveListSuccess, (state, { archives }) => ({
        ...state,
        archives,
        archiveListIsLoading: false,
        archiveListIsLoaded: true
    })),
    on(archiveActions.loadArchiveListFail, (state) => ({
        ...state,
        archiveListIsLoading: false,
        archiveListIsLoaded: false
    }))
);

export const selectArchives = (state: State) => state.archives;
export const selectArchiveListIsLoading = (state: State) => state.archiveListIsLoading;
export const selectArchiveListIsLoaded = (state: State) => state.archiveListIsLoaded;
