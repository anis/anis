/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';

import { KeycloakEventType, KeycloakService } from 'keycloak-angular';

import { AppConfigService } from '../app-config.service';
import { initializeKeycloak } from './init.keycloak';

class MockKeycloakService extends KeycloakService {
    constructor() {
        super();
    }
}

class MockAppConfigService extends AppConfigService {
    constructor() {
        super();
    }
}

describe('[Auth] initializeKeycloak', () => {
    let store: MockStore;
    const keycloakService: MockKeycloakService = new MockKeycloakService();
    const appConfigService: MockAppConfigService = new MockAppConfigService();

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                provideMockStore({}),
                { provide: keycloakService, useValue: {} },
                { provide: AppConfigService, useValue: { ...appConfigService, } }
            ]
        })
        store = TestBed.inject(MockStore);
    });

    it('should return Promise.resolve(true)', (done) => {
        appConfigService.authenticationEnabled = false;
        const result = initializeKeycloak(keycloakService, store, appConfigService);
        expect(result).toEqual(Promise.resolve(true));
        done();
    });
    
    it('should dispatch logout action and return keycloak.init', async () => {
        appConfigService.authenticationEnabled = true;
        const KeycloakEventTy = 'KeycloakEventType'
        Object.defineProperty(keycloakService, KeycloakEventTy, { value: { type: KeycloakEventType.OnAuthLogout } });
        const value = {
            config: {},
            initOptions: {},
            loadUserProfileAtStartUp: true,
            bearerExcludedUrls: ['test']
        }
        keycloakService.init = jest.fn().mockImplementation(() => Promise.resolve(value));
        const result = initializeKeycloak(keycloakService, store, appConfigService);
        expect(result).toEqual(Promise.resolve(value));
    });
});
