/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { UserModule } from "./user.module"

describe('[User] UserModule', () => {
    it('should test user module', () => {
        expect(UserModule.name).toBe('UserModule');
    });
});
