/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { combineReducers, createFeatureSelector } from '@ngrx/store';

import { RouterReducerState } from 'src/app/custom-route-serializer';
import * as auth from './store/reducers/auth.reducer';
import * as archive from './store/reducers/archive.reducer';

export interface State {
    auth: auth.State;
    archive: archive.State;
}

const reducers = {
    auth: auth.authReducer,
    archive: archive.archiveReducer
};

export const userReducer = combineReducers(reducers);
export const getUserState = createFeatureSelector<State>('user');
export const selectRouterState = createFeatureSelector<RouterReducerState>('router');
