/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { CoreModule } from './core.module';

describe('[Core] CoreModule', () => {
    it('Test core module', () => {
        expect(CoreModule.name).toEqual('CoreModule');
    });
});
