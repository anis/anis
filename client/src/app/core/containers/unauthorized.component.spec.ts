/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { TestBed, waitForAsync, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { provideMockStore } from '@ngrx/store/testing';
import { StyleService } from 'src/app/shared/services/style.service';

import { UnauthorizedComponent } from './unauthorized.component';

describe('[Core] UnauthorizedComponent', () => {
    let component: UnauthorizedComponent;
    let fixture: ComponentFixture<UnauthorizedComponent>;

    const style = {
        setStyles: jest.fn(),
    };

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [UnauthorizedComponent],
            providers: [
                provideMockStore({}),
                { provide: StyleService, useValue: style },
            ],
        }).compileComponents();
        fixture = TestBed.createComponent(UnauthorizedComponent);
        component = fixture.componentInstance;
    }));

    it('should create the component', () => {
        expect(component).toBeDefined();
    });

    it('should call onInit properly', () => {
        component.favIcon = { href: 'coucou' } as HTMLLinkElement;
        component.body = {
            style: { backgroundColor: 'black' },
        } as HTMLBodyElement;

        component.ngOnInit();

        expect(component.favIcon.href).toEqual('favicon.ico');
        expect(component.body.style.backgroundColor).toEqual('white');
        expect(style.setStyles).toHaveBeenCalled();
    });

    it('should call properly login', () => {
        const spyStore = jest.spyOn((component as any).store, 'dispatch');

        component.login();

        expect(spyStore).toHaveBeenCalled();
    });
});
