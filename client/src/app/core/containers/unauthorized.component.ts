/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { StyleService } from 'src/app/shared/services/style.service';

import * as authActions from 'src/app/user/store/actions/auth.actions';
import * as authSelector from 'src/app/user/store/selectors/auth.selector';

@Component({
    selector: 'app-unauthorized',
    templateUrl: 'unauthorized.component.html',
})
export class UnauthorizedComponent implements OnInit {
    public isAuthenticated: Observable<boolean>;
    public favIcon: HTMLLinkElement = document.querySelector('#favicon');
    public body: HTMLBodyElement = document.querySelector('body');
    public goBack: string = null;

    constructor(
        private store: Store<any>,
        private style: StyleService,
    ) {
        this.isAuthenticated = this.store.select(
            authSelector.selectIsAuthenticated,
        );
        this.goBack = sessionStorage.getItem('go_back');
    }

    /**
     * Resets the css style values for the unauthorized page
     */
    ngOnInit(): void {
        this.favIcon.href = 'favicon.ico';
        this.body.style.backgroundColor = 'white';
        this.style.setStyles('.footer', {
            'background-color': '#F8F9FA',
            'border-top': 'none',
            color: 'black',
        });
    }

    /**
     * Redirects the user to the login page
     */
    login(): void {
        let redirectUri = sessionStorage.getItem('redirect_uri');
        if (!redirectUri) {
            redirectUri = '/';
        }
        this.store.dispatch(authActions.login({ redirectUri }));
    }
}
