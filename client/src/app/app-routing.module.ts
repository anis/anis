/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotFoundPageComponent } from './core/containers/not-found-page.component';
import { UnauthorizedComponent } from './core/containers/unauthorized.component';

const routes: Routes = [
    { path: 'portal', loadChildren: () => import('./portal/portal.module').then(m => m.PortalModule) },
    { path: 'admin', loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule) },
    { path: 'unauthorized', component: UnauthorizedComponent, title: 'ANIS - Unauthorized page' },
    { path: 'not-found', component: NotFoundPageComponent, title: 'ANIS - 404 not found' },
    { path: '', loadChildren: () => import('./instance/instance.module').then(m => m.InstanceModule) }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        scrollPositionRestoration: 'enabled'
    })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
