/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import { Action } from '@ngrx/store';

import * as fromSamp from './samp.reducer';
import * as sampActions from './samp.actions';

describe('[Samp] Samp reducer', () => {
    it('unknown action should return the default state', () => {
        const action = { type: 'Unknown' };
        const state = fromSamp.sampReducer(fromSamp.initialState, action);

        expect(state).toBe(fromSamp.initialState);
    });

    it('registerSuccess action should set registered to true', () => {
        const action = sampActions.registerSuccess();
        const state = fromSamp.sampReducer(fromSamp.initialState, action);

        expect(state.registered).toBeTruthy();
        expect(state).not.toBe(fromSamp.initialState);
    });

    it('unregister action should set registered to false', () => {
        const initialState = {
            ...fromSamp.initialState,
            registered: true
        };
        const action = sampActions.unregister();
        const state = fromSamp.sampReducer(initialState, action);

        expect(state.registered).toBeFalsy();
        expect(state).not.toBe(initialState);
    });

    it('should get registered', () => {
        const action = {} as Action;
        const state =  fromSamp.sampReducer(undefined, action);

        expect(fromSamp.selectRegistered(state)).toBeFalsy();
    });
});
