/**
 * This file is part of ANIS Client.
 *
 * @copyright Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

import * as sampActions from './samp.actions';

describe('[Samp] Samp actions', () => {
    it('should create register action', () => {
        const action = sampActions.register();
        expect(action).toEqual({ type: '[Samp] Register' });
    });

    it('should create registerSuccess action', () => {
        const action = sampActions.registerSuccess();
        expect(action).toEqual({ type: '[Samp] Register Success' });
    });

    it('should create registerFail action', () => {
        const action = sampActions.registerFail();
        expect(action).toEqual({ type: '[Samp] Register Fail' });
    });

    it('should create unregister action', () => {
        const action = sampActions.unregister();
        expect(action).toEqual({ type: '[Samp] Unregister' });
    });

    it('should create broadcastVotable action', () => {
        const action = sampActions.broadcastVotable({ 
            url: 'http://localhost:8080/instance/default/dataset/dataset1/file-explorer/files/file.fits'
        });
        expect(action).toEqual({
            type: '[Samp] Broadcast Votable',
            url: 'http://localhost:8080/instance/default/dataset/dataset1/file-explorer/files/file.fits'
        });
    });

    it('should create broadcastVotable action', () => {
        const action = sampActions.broadcastImage({ 
            url: 'http://localhost:8080/instance/default/dataset/dataset1/file-explorer/files/image.fits'
        });
        expect(action).toEqual({
            type: '[Samp] Broadcast Image',
            url: 'http://localhost:8080/instance/default/dataset/dataset1/file-explorer/files/image.fits'
        });
    });
});
