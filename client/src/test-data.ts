import { Column, FileInfo, FitsImageLimits } from './app/admin/store/models';
import { UserProfile } from './app/user/store/models/user-profile.model';
import { Alias, ConeSearch, FieldCriterion } from './app/instance/store/models';
import { Archive } from './app/instance/store/models/archive.model';
import {
    Instance,
    DesignConfig,
    Database,
    DatasetFamily,
    Dataset,
    Attribute,
    CriteriaFamily,
    OutputCategory,
    OutputFamily,
    Webpage,
    MenuItem,
    Image,
    File,
    ConeSearchConfig,
    DetailConfig,
    AliasConfig,
    Url,
    MenuFamily,
    DatasetGroup,
} from './app/metamodel/models';

export const INSTANCE: Instance = {
    name: 'my-instance',
    label: 'My Instance',
    description: 'My Instance description',
    scientific_manager: 'M. Dupont',
    instrument: 'Multiple',
    wavelength_domain: 'Visible',
    data_path: 'data/path',
    files_path: 'files',
    public: true,
    portal_logo: 'logo.png',
    portal_color: 'green',
    groups: [{ role: 'TEST' }],
    nb_dataset_families: 2,
    nb_datasets: 4,
    default_redirect: '/home'
};

export const INSTANCE_LIST: Instance[] = [
    {
        name: 'my-other-instance',
        label: 'My Other Instance',
        description: 'My Other Instance description',
        scientific_manager: 'M. Dupont',
        instrument: 'Multiple',
        wavelength_domain: 'Visible',
        data_path: 'data/path',
        files_path: 'files',
        public: true,
        portal_logo: '',
        portal_color: 'green',
        groups: [],
        nb_dataset_families: 1,
        nb_datasets: 2,
        default_redirect: '/home'
    },
    INSTANCE,
];

export const DESIGN_CONFIG: DesignConfig = {
    design_background_color: 'darker green',
    design_text_color: '#212529',
    design_font_family: 'Roboto, sans-serif',
    design_link_color: '#007BFF',
    design_link_hover_color: '#0056B3',
    design_logo: 'path/to/logo',
    design_logo_href: null,
    design_favicon: 'path/to/favicon',
    navbar_background_color: '#F8F9FA',
    navbar_border_bottom_color: '#DEE2E6',
    navbar_color_href: '#000000',
    navbar_font_family: 'Roboto, sans-serif',
    navbar_sign_in_btn_color: '#28A745',
    navbar_user_btn_color: '#7AC29A',
    footer_background_color: '#F8F9FA',
    footer_border_top_color: '#DEE2E6',
    footer_text_color: '#000000',
    footer_logos: [
        {
            href: 'http://lam.fr',
            title: "Laboratoire d'Astrophysique de Marseille",
            file: '/logo_lam_s.png',
            display: 20,
        },
        {
            href: 'http://www.univ-amu.fr',
            title: 'Aix*Marseille Université',
            file: '/logo_amu_s.png',
            display: 30,
        },
        {
            href: 'http://anis.lam.fr',
            title: 'AstroNomical Information System',
            file: '/cesam_anis40.png',
            display: 50,
        },
        {
            href: 'http://cesam.lam.fr',
            title: 'Centre de données Astrophysique de Marseille',
            file: '/logo_cesam_s.png',
            display: 10,
        },
        {
            href: 'http://www.insu.cnrs.fr',
            title: "Institut National des Sciences de l'Univers",
            file: '/logo_insu_s.png',
            display: 40,
        },
    ],
    family_border_color: '#DFDFDF',
    family_header_background_color: '#F7F7F7',
    family_title_color: '#007BFF',
    family_title_bold: false,
    family_background_color: '#FFFFFF',
    family_text_color: '#212529',
    progress_bar_title: 'Dataset search',
    progress_bar_title_color: '#000000',
    progress_bar_subtitle:
        'Select a dataset, add criteria, select output columns and display the result.',
    progress_bar_subtitle_color: '#6C757D',
    progress_bar_step_dataset_title: 'Dataset selection',
    progress_bar_step_criteria_title: 'Search criteria',
    progress_bar_step_output_title: 'Output columns',
    progress_bar_step_result_title: 'Result table',
    progress_bar_color: '#E9ECEF',
    progress_bar_active_color: '#7AC29A',
    progress_bar_circle_color: '#FFFFFF',
    progress_bar_circle_icon_color: '#CCCCCC',
    progress_bar_circle_icon_active_color: '#FFFFFF',
    progress_bar_text_color: '#91B2BF',
    progress_bar_text_bold: false,
    search_next_btn_color: '#007BFF',
    search_next_btn_hover_color: '#007BFF',
    search_next_btn_hover_text_color: '#FFFFFF',
    search_back_btn_color: '#6C757D',
    search_back_btn_hover_color: '#6C757D',
    search_back_btn_hover_text_color: '#FFFFFF',
    delete_current_query_btn_enabled: false,
    delete_current_query_btn_text: 'Delete the current query',
    delete_current_query_btn_color: '#DC3545',
    delete_current_query_btn_hover_color: '#C82333',
    delete_current_query_btn_hover_text_color: '#FFFFFF',
    delete_criterion_cross_color: '#DC3545',
    search_info_background_color: '#E9ECEF',
    search_info_text_color: '#000000',
    search_info_help_enabled: true,
    dataset_select_btn_color: '#6C757D',
    dataset_select_btn_hover_color: '#6C757D',
    dataset_select_btn_hover_text_color: '#FFFFFF',
    dataset_selected_icon_color: '#28A745',
    search_criterion_background_color: '#7AC29A',
    search_criterion_text_color: '#000000',
    output_columns_selected_color: '#7AC29A',
    output_columns_select_all_btn_color: '#6C757D',
    output_columns_select_all_btn_hover_color: '#6C757D',
    output_columns_select_all_btn_hover_text_color: '#FFFFFF',
    result_panel_border_size: '1px',
    result_panel_border_color: '#DEE2E6',
    result_panel_title_color: '#000000',
    result_panel_background_color: '#FFFFFF',
    result_panel_text_color: '#000000',
    result_download_btn_color: '#007BFF',
    result_download_btn_hover_color: '#0069D9',
    result_download_btn_text_color: '#FFFFFF',
    result_datatable_actions_btn_color: '#007BFF',
    result_datatable_actions_btn_hover_color: '#0069D9',
    result_datatable_actions_btn_text_color: '#FFFFFF',
    result_datatable_bordered: true,
    result_datatable_bordered_radius: false,
    result_datatable_border_color: '#DEE2E6',
    result_datatable_header_background_color: '#FFFFFF',
    result_datatable_header_text_color: '#000000',
    result_datatable_rows_background_color: '#FFFFFF',
    result_datatable_rows_text_color: '#000000',
    result_datatable_sorted_color: '#C5C5C5',
    result_datatable_sorted_active_color: '#000000',
    result_datatable_link_color: '#007BFF',
    result_datatable_link_hover_color: '#0056B3',
    result_datatable_rows_selected_color: '#7AC29A',
    result_datatable_pagination_link_color: '#7AC29A',
    result_datatable_pagination_active_bck_color: '#7AC29A',
    result_datatable_pagination_active_text_color: '#FFFFFF',
    samp_enabled: true,
    back_to_portal: true,
    user_menu_enabled: true,
    search_multiple_all_datasets_selected: false,
    search_multiple_progress_bar_title:
        'Search around a position in multiple datasets',
    search_multiple_progress_bar_subtitle:
        'Fill RA & DEC position, select datasets and display the result.',
    search_multiple_progress_bar_step_position: 'Position',
    search_multiple_progress_bar_step_datasets: 'Datasets',
    search_multiple_progress_bar_step_result: 'Result',
};

export const DATABASE: Database = {
    id: 1,
    label: 'database one',
    dbname: 'database-one',
    dbtype: 'type',
    dbhost: 'host',
    dbport: 1234,
    dblogin: 'login',
    dbpassword: 'pwd',
    dbdsn_file: '/mnt/dbpassword'
};

export const DATABASE_LIST: Database[] = [
    DATABASE,
    {
        id: 2,
        label: 'database two',
        dbname: 'database-two',
        dbtype: 'type',
        dbhost: 'host',
        dbport: 1234,
        dblogin: 'login',
        dbpassword: 'pwd',
        dbdsn_file: '/mnt/dbpassword'
    },
];

export const TABLE_LIST: string[] = ['table1', 'table2', 'view1'];

export const COLUMN_LIST: Column[] = [
    {
        name: 'column1',
        type: 'integer',
    },
    {
        name: 'column2',
        type: 'string',
    },
];

export const FILE_INFO_LIST: FileInfo[] = [
    {
        name: 'file1.fits',
        size: 12345,
        mimetype: 'fits',
        type: 'file',
    },
    {
        name: 'file2.fits',
        size: 54321,
        mimetype: 'fits',
        type: 'file',
    },
];

export const FITS_IMAGE_LIMITS: FitsImageLimits = {
    ra_min: 5,
    ra_max: 10,
    dec_min: 5,
    dec_max: 10,
};

export const USER_PROFILE: UserProfile = {
    createdTimestamp: 10,
    email: 'test@test.com',
    emailVerified: true,
    enabled: true,
    firstName: 'test',
    id: '',
    lastName: 'test',
    totp: false,
    username: 'test',
};

export const DATASET_FAMILY: DatasetFamily = {
    id: 1,
    label: 'My first dataset family',
    display: 1,
    opened: true,
};

export const DATASET_FAMILY_LIST: DatasetFamily[] = [
    { id: 2, label: 'My second dataset family', display: 2, opened: false },
    DATASET_FAMILY,
];

export const DATASET_GROUP: DatasetGroup = {
    role: 'my-group',
};

export const WEBPAGE: Webpage = {
    id: 1,
    label: 'Home',
    icon: 'fas fa-home',
    display: 10,
    name: 'home',
    title: 'Home',
    content: '<p>TEST</p>',
    style_sheet: '.lead {\n    color: #b60707;\n}',
    type: 'webpage',
};

export const URL: Url = {
    id: 1,
    label: 'Search by criteria',
    icon: 'fas fa-search-by-criteria',
    display: 10,
    type_url: 'internal',
    href: '/search',
    type: 'url',
};

export const MENU_FAMILY: MenuFamily = {
    id: 2,
    name: 'search',
    label: 'Search',
    icon: 'fas fa-search',
    display: 20,
    type: 'menu_family',
    items: [URL],
};

export const MENU_ITEM_LIST: MenuItem[] = [WEBPAGE, MENU_FAMILY];

export const DATASET: Dataset = {
    name: 'my-dataset',
    table_ref: 'table',
    primary_key: 1,
    default_order_by: 1,
    default_order_by_direction: 'a',
    label: 'my dataset',
    description: 'This is my dataset',
    display: 1,
    data_path: 'path',
    public: true,
    download_json: true,
    download_csv: true,
    download_ascii: true,
    download_vo: true,
    download_fits: true,
    server_link_enabled: true,
    datatable_enabled: true,
    datatable_selectable_rows: true,
    id_database: 1,
    id_dataset_family: 1,
    full_data_path: '/data/path',
    cone_search_enabled: true,
    groups: [{ role: 'DTEST' }],
};

export const DATASET_LIST: Dataset[] = [
    DATASET,
    {
        name: 'another-dataset',
        table_ref: 'table',
        primary_key: 1,
        default_order_by: 1,
        default_order_by_direction: 'a',
        label: 'amother dataset',
        description: 'This is another dataset',
        display: 2,
        data_path: 'path',
        public: true,
        download_json: true,
        download_csv: true,
        download_ascii: true,
        download_vo: true,
        server_link_enabled: true,
        datatable_enabled: true,
        datatable_selectable_rows: true,
        id_database: 1,
        id_dataset_family: 1,
        download_fits: true,
        full_data_path: '/data/path',
        cone_search_enabled: false,
        groups: [],
    },
];

export const CONE_SEARCH: ConeSearch = {
    ra: 1,
    dec: 2,
    radius: 3,
};

export const CONE_SEARCH_CONFIG: ConeSearchConfig = {
    enabled: true,
    opened: false,
    column_ra: 3,
    column_dec: 4,
    resolver_enabled: true,
    default_ra: null,
    default_dec: null,
    default_radius: 2.0,
    default_ra_dec_unit: 'degree',
    plot_enabled: true,
};

export const DETAIL_CONFIG: DetailConfig = {
    content: '<p>TEST</p>',
    style_sheet: '.foo { color: blue; }',
};

export const ALIAS_CONFIG: AliasConfig = {
    table_alias: 'aliases',
    column_alias: 'alias',
    column_name: 'name',
    column_alias_long: 'alias_long',
};

export const ALIAS: Alias = {
    alias: 'alias',
    name: 'alias',
    alias_long: 'long_alias',
};

export const CRITERION: FieldCriterion = {
    id: 3,
    type: 'criterion',
    value: 'hello',
    operator: 'eq',
};

export const ATTRIBUTE: Attribute = {
    id: 1,
    name: 'name_one',
    label: 'label_one',
    form_label: 'form_label_one',
    datatable_label: null,
    description: 'description_one',
    type: 'integer',
    search_type: 'field',
    operator: '=',
    dynamic_operator: false,
    null_operators_enabled: false,
    min: null,
    max: null,
    placeholder_min: null,
    placeholder_max: null,
    criteria_display: 1,
    output_display: 1,
    selected: true,
    renderer: null,
    renderer_config: null,
    order_by: true,
    archive: false,
    detail_display: 1,
    detail_renderer: null,
    detail_renderer_config: null,
    options: [
        { label: 'Three', value: 'three', display: 3 },
        { label: 'One', value: 'one', display: 1 },
        { label: 'Two', value: 'two', display: 2 },
    ],
    vo_utype: null,
    vo_ucd: null,
    vo_unit: null,
    vo_description: null,
    vo_datatype: null,
    vo_size: null,
    id_criteria_family: null,
    id_output_category: '1_2',
    id_detail_output_category: '1_2',
};

export const ATTRIBUTE_LIST: Attribute[] = [
    ATTRIBUTE,
    {
        id: 2,
        name: 'name_two',
        label: 'label_two',
        form_label: 'form_label_two',
        datatable_label: null,
        description: 'description_two',
        type: 'integer',
        search_type: 'field',
        operator: '=',
        dynamic_operator: false,
        null_operators_enabled: false,
        min: null,
        max: null,
        placeholder_min: null,
        placeholder_max: null,
        criteria_display: 2,
        output_display: 2,
        selected: true,
        renderer: null,
        renderer_config: null,
        order_by: true,
        archive: false,
        detail_display: 2,
        detail_renderer: null,
        detail_renderer_config: null,
        options: [
            { label: 'Three', value: 'three', display: 3 },
            { label: 'One', value: 'one', display: 1 },
            { label: 'Two', value: 'two', display: 2 },
        ],
        vo_utype: null,
        vo_ucd: null,
        vo_unit: null,
        vo_description: null,
        vo_datatype: null,
        vo_size: null,
        id_criteria_family: 1,
        id_output_category: '1_1',
        id_detail_output_category: '1_1',
    },
    {
        id: 3,
        name: 'name_three',
        label: 'label_three',
        form_label: 'form_label_three',
        datatable_label: null,
        description: 'description_three',
        type: 'integer',
        search_type: 'field',
        operator: '=',
        dynamic_operator: false,
        null_operators_enabled: false,
        min: null,
        max: null,
        placeholder_min: null,
        placeholder_max: null,
        criteria_display: 3,
        output_display: 3,
        selected: true,
        renderer: null,
        renderer_config: null,
        order_by: true,
        archive: false,
        detail_display: 3,
        detail_renderer: null,
        detail_renderer_config: null,
        options: [
            { label: 'Three', value: 'three', display: 3 },
            { label: 'One', value: 'one', display: 1 },
            { label: 'Two', value: 'two', display: 2 },
        ],
        vo_utype: null,
        vo_ucd: null,
        vo_unit: null,
        vo_description: null,
        vo_datatype: null,
        vo_size: null,
        id_criteria_family: null,
        id_output_category: null,
        id_detail_output_category: null,
    },
    {
        id: 4,
        name: 'name_four',
        label: 'label_four',
        form_label: 'form_label_four',
        datatable_label: null,
        description: 'description_four',
        type: 'integer',
        search_type: 'field',
        operator: '=',
        dynamic_operator: false,
        null_operators_enabled: false,
        min: null,
        max: null,
        placeholder_min: null,
        placeholder_max: null,
        criteria_display: 4,
        output_display: 4,
        selected: true,
        renderer: null,
        renderer_config: null,
        order_by: true,
        archive: true,
        detail_display: 4,
        detail_renderer: null,
        detail_renderer_config: null,
        options: [
            { label: 'Three', value: 'three', display: 3 },
            { label: 'One', value: 'one', display: 1 },
            { label: 'Two', value: 'two', display: 2 },
        ],
        vo_utype: null,
        vo_ucd: null,
        vo_unit: null,
        vo_description: null,
        vo_datatype: null,
        vo_size: null,
        id_criteria_family: null,
        id_output_category: '1_1',
        id_detail_output_category: '1_1',
    },
];

export const CRITERIA_FAMILY: CriteriaFamily = {
    id: 1,
    label: 'myCriteriaFamily',
    display: 1,
    opened: true,
};

export const CRITERIA_FAMILY_LIST: CriteriaFamily[] = [
    CRITERIA_FAMILY,
    {
        id: 2,
        label: 'anotherCriteriaFamily',
        display: 1,
        opened: true,
    },
];

export const OUTPUT_CATEGORY: OutputCategory = {
    id: 1,
    label: 'Output category',
    display: 10,
};

export const OUTPUT_CATEGORY_LIST: OutputCategory[] = [
    OUTPUT_CATEGORY,
    {
        id: 2,
        label: 'Another output category',
        display: 20,
    },
];

export const OUTPUT_FAMILY: OutputFamily = {
    id: 1,
    label: 'Output family One',
    display: 10,
    opened: false,
    output_categories: OUTPUT_CATEGORY_LIST,
};

export const OUTPUT_FAMILY_LIST: OutputFamily[] = [
    OUTPUT_FAMILY,
    {
        id: 2,
        label: 'Output family Two',
        display: 20,
        opened: false,
        output_categories: [],
    },
];

export const IMAGE: Image = {
    id: 1,
    label: 'Mag i',
    file_path: '/IMAGES/CFHTLS_D-85_i_022559-042940_T0007_MEDIAN.fits',
    hdu_number: null,
    file_size: 1498320000,
    ra_min: 35.994643451078,
    ra_max: 36.99765934121,
    dec_min: -3.9943006310031,
    dec_max: -4.9941936740893,
    stretch: 'linear',
    pmin: 0.2,
    pmax: 99,
};

export const FILE: File = {
    id: 1,
    label: 'My file',
    file_path: '/FILE/my_file.txt',
    file_size: 1498320000,
    type: 'txt',
};

export const RESULT: any = [
    {
        id: 418,
        ra: 148.9005,
        dec: 69.067,
    },
    {
        id: 419,
        ra: 322.5,
        dec: 12.167,
    },
    {
        id: 420,
        ra: 322.5,
        dec: 12.167,
    },
];

export const ARCHIVE: Archive = {
    id: 12,
    name: 'archive_default_observations_2023-09-22T13:59:28.zip',
    created: '2023-09-22T15:08:59',
    status: 'crearted'
}
