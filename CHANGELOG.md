# Changelog

All notable changes to this project will be documented in this file.

## [3.16.0] - yyyy-mm-dd

 - #115 - Add hdu_number parameter for image
 - #116 - Add a new detail dynamic component to cut a fits image stored directly in the record (app-display-fits-cut-from-record)

## [3.15.1] - 2024-11-21

 - #114 - Fixed bug: SAMP Broadcast of a file does not work if the dataset is private

## [3.15.0] - 2024-11-12

 - #97 - Fixed bug: Loading problem when the user changes cone-search image in the results page
 - #100 - Returns a clear exception when an operator that is not defined is used as part of the search in a JSON attribute
 - #101 - Added management of the hdu number parameter in calls to FITS images services
 - #102 - The search info help buttons use the same text from the progress bar
 - #103 - Fixed bug: in the admin part, in the attributes part, when the user deletes the contents of the datatable_label property, set the value to null
 - #104 - Addition of a separator option in the dynamic component app-display-value-by-attribute to perform a line break
 - #105 - Addition of a dynamic component to interpret the html present in the value of an attribute (app-display-value-html)
 - #106 - Addition of a dynamic component to interpret coordinates stored in a value (app-display-value-geobox)
 - #107 - Addition of a dynamic component to interpret links stored in a value  (app-display-value-multi-links)
 - #108 - Add a renderer to display a value in the form of a badge
 - #109 - Add a mailto renderer
 - #110 - Add a new detail page dynamic component named cedop-category
 - #111 - Add a new detail page dynamic component named cedop-status
 - #112 - Allow the user to broadcast the files available in the renderer download (datatable)
 - #113 - Display a message if no data is available for the dynamic component app-datatable-other-dataset

## [3.14.5] - 2024-09-18

 - #98 - Modification of the ergonomics of app-datatable-one-liner

## [3.14.4] - 2024-09-03

 - #96 - Fixed bug: The tree structure is not preserved when the zip file is created by tasks

## [3.14.3] - 2024-08-29

 - #95 - Adding the possibility to make NULL/NOT NULL criterion operators visible or not

## [3.14.2] - 2024-08-22

 - #93 - Back to 128M memory (anis_server)
 - #94 - Fixed archive creation bug when parameter MAILER_ENABLED=0

## [3.14.1] - 2024-08-21

 - #90 - Limit memory footprint zip creation (anis_tasks)
 - #91 - Set memory_limit PHP parameter to 32M to limit memory footprint (anis_server)
 - #92 - Fixed bug: Problem when a user searches in a JSON with a numerical value. To fix the problem, a string must be used systematically

## [3.14.0] - 2024-07-18

 - #23 - Add renderer to format numbers
 - #32 - Automatic clean-up of out-of-date archives stored in the archive directory
 - #35 - Saving the datatable configuration (result page)
 - #40 - When a user is logged in and creates an archive, send them an email when the archive is complete.
 - #57 - Fixed bug diva+: Problem switching between searches
 - #80 - Sort svom keywords in alphabetical order
 - #81 - Add a renderer to display the value in scientific format
 - #82 - Improving the design of the portal page
 - #84 - Add the description of each keyword svom to the ng-select list
 - #85 - Add a renderer to create a link between the datatable and a search on another dataset
 - #86 - Improvements to the unauthorized page
 - #87 - Fixed bug: problem generating URLs for fits cut images
 - #88 - Fixed some errors for logs in test client.
 - #89 - Add a dynamic component for displaying the result of another dataset

## [3.13.0] - 2024-06-17

 - #79 - Improved search using JSON (SVOM JSON KWs)

## [3.12.0] - 2024-05-29

 - #78 - Database connection DSN stored in files (instead of just the password)

## [3.11.0] - 2024-04-15

 - #63 - When a user leaves the results page, the system must delete the datatable rows selection
 - #64 - In the "Request informationts" box (result page), replace objects found by rows found
 - #65 - Add a button to delete the current query on the dataset selection page
 - #66 - In the result part, when user selects rows, the "Downloads" section changes to indicate the number of rows selected, and the buttons allow only the selected results to be downloaded
 - #67 - Bouton refresh dataset (optionnel / configurable)
 - #68 - Be able to delete a criterion directly from the search criteria accordion (only on criteria step)
 - #71 - Implement checksum in TUS client
 - #72 - Possibility of passing the password for a business database via a file (see entity database)
 - #73 - Fixed bug: Delete the null value from the array of possible values for an attribute (admin criteria generate values)
 - #74 - Handle Creation With Upload extension of the tus protocol
 - #76 - Possibility of passing the password for metamodel database via a file (see docker-compose.yml)
 - #77 - Upgrade to php 8.2 for the anis server part

## [3.10.0] - 2024-02-14

 - #50 - Fixed bug: The endpoints services were not protected when a project and/or a dataset is private
 - #51 - Fixed bug: When a dataset is private and the datatable is configured with an image renderer to display a PNG file, the server returns a 401 error
 - #52 - Fixed bug: Display problem when an image is not found (infinite displaying of loading spinner, no indication that the image is not found)
 - #53 - Fixed bug: If the database does not contain a view and the administrator wants to display the list of columns available for a table using the new attribute button, then the server returns an error
 - #54 - To be able to display N2CLS images, the services module has been modified to add the wells parameter to FITSFigure.
 - #55 - Fixed bug: In the database results, when the ID = 0 the link to the detail page is not displayed
 - #56 - Adds dynamic component DisplayFileLinkPrivateDatasetComponent to handle, in a webpage, the dataset file link when dataset is private
 - #58 - Adds dowload and broadcast buttons for cropped images in the detail page
 - #60 - Adds dynamic component to manage file uploads via the TUS protocol (TUS client)
 - #61 - Fixed bug: In the link renderer configuration, the admin can now use $ws_value to retrieve the value without spaces

## [3.9.1] - 2024-01-10

 - #48 - On the detail page of an object with a spectrum, the spectra component cannot be displayed if there are NaN values (x or Flux).
 - #49 - The el_cosmos dataset in ASPIC is not displayed on the detail page (error 500). A read strategy needs to be added to the services to be able to read it.

## [3.9.0] - 2023-12-18

 - #33 - When a user is logged in, they have access to the list of archives already downloaded.
 - #38 - The administrator can now change the title of each column in the results datatable (datatable_label).
 - #39 - If the ID column of a table is not selected as output column, the table rows are selected all at once when selecting just one.
 - #41 - When a cell in the database table is empty (NULL value) ANIS displays a 0 (zero) when it is a column with numbers, it should be empty.
 - #42 - At the root, the server API now responds with the version of ANIS used and makes a connection test to the metamodel database (SVOM).
 - #43 - In the alias list search, users can now copy and paste a list into a textarea and launch the search on this list. Each object found is then added to the search selectbox (Diva+).
 - #44 - The dataset-sample dynamic component can now be displayed with the renders configured for the dataset (IRiS).
 - #46 - Dataset attributes descriptions added to the detail page.
 - #47 - If the user adds style to a webpage with an @media then the @media is not taken into account in the processing as it is prefixed by the webpage class. The webpage style must not prefix an @xxx instruction.
