"""Test utils module"""

from unittest.mock import MagicMock, patch

import pytest

from anis_tasks import utils


class TestUtils:
    """Test all utils functions."""

    def test_check_config(self, monkeypatch):
        """check_config shall return ConfigKeyNotFound exception if ENV
        variables are not set.

        check_config shall not return ConfigKeyNotFound exception if ENV
        variables are set.
        """
        monkeypatch.setenv("DATA_PATH", value="yo")
        monkeypatch.setenv("ARCHIVE_FOLDER", value="yo")
        monkeypatch.setenv("SERVER_URL", value="yo")
        monkeypatch.setenv("RMQ_HOST", value="yo")
        monkeypatch.setenv("RMQ_PORT", value="yo")
        monkeypatch.setenv("RMQ_USER", value="yo")
        monkeypatch.setenv("RMQ_PASSWORD", value="yo")

        try:
            utils.check_config()
        except utils.ConfigKeyNotFound:
            pytest.fail(
                "ConfigKeyNotFound was raised while ENV variables have been set"
            )

        monkeypatch.delenv("DATA_PATH", raising=False)
        with pytest.raises(utils.ConfigKeyNotFound):
            utils.check_config()

    def test_one_liner_getter(self, monkeypatch):
        monkeypatch.setenv("DATA_PATH", value="yo")
        monkeypatch.setenv("ARCHIVE_FOLDER", value="yo")
        monkeypatch.setenv("SERVER_URL", value="yo")
        monkeypatch.setenv("RMQ_HOST", value="yo")
        monkeypatch.setenv("RMQ_PORT", value="yo")
        monkeypatch.setenv("RMQ_USER", value="yo")
        monkeypatch.setenv("RMQ_PASSWORD", value="yo")

        assert utils.get_data_path() is not None
        assert utils.get_archive_folder() is not None
        assert utils.get_server_url() is not None
        assert utils.get_rmq_host() is not None
        assert utils.get_rmq_port() is not None
        assert utils.get_rmq_user() is not None
        assert utils.get_rmq_password() is not None

    @patch("anis_tasks.utils.requests")
    def test_get_dataset(self, mock_requests, monkeypatch):
        monkeypatch.setenv("SERVER_URL", "http://here.fr")
        r = MagicMock()
        mock_requests.get.return_value = r

        r.status_code = 200
        r.json.return_value = {"data": "mydata"}
        res = utils.get_dataset("myinstance", "mydataset")
        mock_requests.get.assert_called_with(
            "http://here.fr/instance/myinstance/dataset/mydataset"
        )
        assert res == "mydata"

        r.status_code = 404
        with pytest.raises(utils.DatasetNotFound):
            _ = utils.get_dataset("myinstance", "mydataset")

        r.status_code = 500
        r.json.return_value = {"message": "coucou"}
        with pytest.raises(utils.AnisServerError):
            _ = utils.get_dataset("myinstance", "mydataset")

    @patch("anis_tasks.utils.requests")
    def test_get_attributes(self, mock_requests, monkeypatch):
        monkeypatch.setenv("SERVER_URL", "http://here.fr")
        r = MagicMock()
        mock_requests.get.return_value = r

        r.status_code = 200
        r.json.return_value = {"data": "mydata"}
        res = utils.get_attributes("myinstance", "mydataset")
        mock_requests.get.assert_called_with(
            "http://here.fr/instance/myinstance/dataset/mydataset/attribute"
        )
        assert res == "mydata"

        r.status_code = 404
        with pytest.raises(utils.DatasetNotFound):
            _ = utils.get_attributes("myinstance", "mydataset")

        r.status_code = 500
        r.json.return_value = {"message": "coucou"}
        with pytest.raises(utils.AnisServerError):
            _ = utils.get_attributes("myinstance", "mydataset")

    @patch("anis_tasks.utils.requests")
    def test_search_data(self, mock_requests, monkeypatch):
        monkeypatch.setenv("SERVER_URL", "http://here.fr")
        r = MagicMock()
        mock_requests.get.return_value = r

        r.status_code = 200
        r.json.return_value = {"data": "mydata"}
        res = utils.search_data("myinstance", "mydataset", "coucou:coconut", "token")
        mock_requests.get.assert_called_with(
            "http://here.fr/search/myinstance/mydataset?coucou:coconut",
            headers={"Authorization": "token"},
        )
        assert res == r

        r.status_code = 404
        with pytest.raises(utils.DatasetNotFound):
            _ = utils.search_data("myinstance", "mydataset", "coucou:coconut", "token")

        r.status_code = 500
        r.json.return_value = {"message": "coucou"}
        with pytest.raises(utils.AnisServerError):
            _ = utils.search_data("myinstance", "mydataset", "coucou:coconut", "token")

    @patch("anis_tasks.utils.requests")
    def test_mark_archive_as_finished(self, mock_requests, monkeypatch):
        monkeypatch.setenv("SERVER_URL", "http://here.fr")
        r = MagicMock()
        mock_requests.get.return_value = r

        r.status_code = 200
        r.json.return_value = {"data": "mydata"}
        res = utils.mark_archive_as_finished(3, "id")
        mock_requests.get.assert_called_with(
            "http://here.fr/archive/mark-archive-as-finished/3?uniq_id=id",
        )
        assert res == "mydata"

        r.status_code = 404
        with pytest.raises(utils.ArchiveNotFound):
            _ = utils.mark_archive_as_finished(3, "id")

        r.status_code = 500
        r.json.return_value = {"message": "coucou"}
        with pytest.raises(utils.AnisServerError):
            _ = utils.mark_archive_as_finished(3, "id")

    @patch("anis_tasks.utils.requests")
    def test_mark_archive_as_in_progress(self, mock_requests, monkeypatch):
        monkeypatch.setenv("SERVER_URL", "http://here.fr")
        r = MagicMock()
        mock_requests.get.return_value = r

        r.status_code = 200
        r.json.return_value = {"data": "mydata"}
        res = utils.mark_archive_as_in_progress(3, "id")
        mock_requests.get.assert_called_with(
            "http://here.fr/archive/mark-archive-as-in-progress/3?uniq_id=id",
        )
        assert res == "mydata"

        r.status_code = 404
        with pytest.raises(utils.ArchiveNotFound):
            _ = utils.mark_archive_as_in_progress(3, "id")

        r.status_code = 500
        r.json.return_value = {"message": "coucou"}
        with pytest.raises(utils.AnisServerError):
            _ = utils.mark_archive_as_in_progress(3, "id")
