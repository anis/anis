"""Test __init__ module"""

from os import environ
from unittest.mock import MagicMock, patch

import anis_tasks as tasks


class TestAnisTasks:
    @patch("anis_tasks.pika")
    @patch("anis_tasks.logging")
    def test_run_fail(self, mock_logging, mock_pika, monkeypatch):
        if "SERVER_URL" in environ.keys():
            monkeypatch.delenv("SERVER_URL")
        tasks.run()
        mock_logging.error.assert_called()
        assert mock_logging.error.call_count == 2
        mock_pika.PlainCredentials.assert_not_called()

    @patch("anis_tasks.logging")
    @patch("anis_tasks.pika")
    def test_run(self, mock_pika, mock_logging, monkeypatch):
        monkeypatch.setenv("DATA_PATH", value="yo")
        monkeypatch.setenv("ARCHIVE_FOLDER", value="yo")
        monkeypatch.setenv("SERVER_URL", value="yo")
        monkeypatch.setenv("RMQ_HOST", value="yo")
        monkeypatch.setenv("RMQ_PORT", value="yo")
        monkeypatch.setenv("RMQ_USER", value="yo")
        monkeypatch.setenv("RMQ_PASSWORD", value="yo")

        mock_pika.PlainCredentials.return_value = "credentials"
        connection = MagicMock()
        mock_pika.BlockingConnection.return_value = connection
        channel = MagicMock()
        connection.channel.return_value = channel

        tasks.run()

        channel.queue_declare.assert_called()
        channel.basic_consume.assert_called()
        mock_logging.info.assert_called_once()
        channel.start_consuming.assert_called()

    @patch("anis_tasks.logging")
    @patch("anis_tasks.run")
    def test_exec(self, mock_run, mock_logging):
        mock_run.return_value = None
        mock_logging.basicConfig.return_value = None
        tasks.exec()
        mock_run.assert_called_once()
        mock_logging.basicConfig.assert_called_once()
