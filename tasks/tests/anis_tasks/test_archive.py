"""Test archive module"""

import json
from unittest.mock import MagicMock, patch

from requests import Response

from anis_tasks import archive
from anis_tasks.archive import ZipFile, os


class TestArchive:
    def test_get_attributes_selected_archive_files(self):
        attributes = [
            {"id": 4, "archive": "zip"},
            {"id": 2, "archive": "zip"},
            {"id": 9, "archive": "zip"},
        ]

        attr = archive.get_attributes_selected_archive_files(attributes, "4;2")
        assert attr == [
            {"id": 4, "archive": "zip"},
            {"id": 2, "archive": "zip"},
        ]

    @patch.object(Response, "json")
    @patch.object(os, "rename")
    @patch.object(os, "makedirs")
    @patch.object(os, "path")
    @patch("anis_tasks.archive.ZipFile")
    @patch("anis_tasks.archive.utils")
    @patch("anis_tasks.archive.logging")
    def test_archive_handler(
        self,
        mock_logging,
        mock_utils,
        mock_zip,
        mock_path,
        mock_makedirs,
        mock_rename,
        mock_response,
    ):
        mock_utils.get_attributes.return_value = [
            {"id": 4, "archive": "zip", "label": "data"},
            {"id": 2, "archive": "zip", "label": "data"},
            {"id": 9, "archive": "zip", "label": "data"},
        ]

        body = {
            "archive_id": "myarchive",
            "uniq_id": 12,
            "instance_name": "myinstance",
            "dataset_name": "mydataset",
            "param_a": "4;2",
            "query": "coucou:coconut",
            "token": "mytoken",
            "user_email": "me@lam.fr",
            "user_email": "me@lam.fr",
        }
        archive.archive_handler(None, None, None, json.dumps(body))
        mock_logging.info.assert_called()
        assert mock_logging.info.call_count == 3
