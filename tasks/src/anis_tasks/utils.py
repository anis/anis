# Standard library imports
import os

# Third party imports
import requests


def check_config():
    """
    Check Config variables
    """
    check_keys = {
        "DATA_PATH",
        "ARCHIVE_FOLDER",
        "SERVER_URL",
        "RMQ_HOST",
        "RMQ_PORT",
        "RMQ_USER",
        "RMQ_PASSWORD",
    }
    for value in check_keys:
        if value not in os.environ.keys():
            raise ConfigKeyNotFound(value)


def get_data_path():
    """
    Get ANIS data path
    """
    return os.environ["DATA_PATH"]


def get_archive_folder():
    """
    Get ANIS archive folder path
    """
    return os.environ["ARCHIVE_FOLDER"]


def get_server_url():
    """
    Get ANIS server URL
    """
    return os.environ["SERVER_URL"]


def get_rmq_host():
    """
    Get RabbitMQ hostname
    """
    return os.environ["RMQ_HOST"]


def get_rmq_port():
    """
    Get RabbitMQ port
    """
    return os.environ["RMQ_PORT"]


def get_rmq_user():
    """
    Get RabbitMQ user
    """
    return os.environ["RMQ_USER"]


def get_rmq_password():
    """
    Get RabbitMQ password
    """
    return os.environ["RMQ_PASSWORD"]


def get_dataset(iname, dname):
    server_url = os.environ["SERVER_URL"]

    r = requests.get(server_url + "/instance/" + iname + "/dataset/" + dname)

    if r.status_code == 404:
        raise DatasetNotFound(dname)
    if r.status_code == 500:
        raise AnisServerError(r.json()["message"])

    return r.json()["data"]


def get_attributes(iname, dname):
    server_url = os.environ["SERVER_URL"]

    r = requests.get(
        server_url + "/instance/" + iname + "/dataset/" + dname + "/attribute"
    )

    if r.status_code == 404:
        raise DatasetNotFound(dname)
    if r.status_code == 500:
        raise AnisServerError(r.json()["message"])

    return r.json()["data"]


def search_data(iname, dname, query, token):
    server_url = os.environ["SERVER_URL"]

    headers = {}

    if token:
        headers = {"Authorization": token}

    r = requests.get(
        server_url + "/search/" + iname + "/" + dname + "?" + query, headers=headers
    )

    if r.status_code == 404:
        raise DatasetNotFound(dname)
    if r.status_code == 500:
        raise AnisServerError(r.json()["message"])

    return r


def mark_archive_as_in_progress(archive_id, uniq_id):
    server_url = os.environ["SERVER_URL"]

    r = requests.get(
        server_url
        + "/archive/mark-archive-as-in-progress/"
        + str(archive_id)
        + "?uniq_id="
        + uniq_id
    )

    if r.status_code == 404:
        raise ArchiveNotFound(archive_id)
    if r.status_code == 500:
        raise AnisServerError(r.json()["message"])

    return r.json()["data"]


def mark_archive_as_finished(archive_id, uniq_id):
    server_url = os.environ["SERVER_URL"]

    r = requests.get(
        server_url
        + "/archive/mark-archive-as-finished/"
        + str(archive_id)
        + "?uniq_id="
        + uniq_id
    )

    if r.status_code == 404:
        raise ArchiveNotFound(archive_id)
    if r.status_code == 500:
        raise AnisServerError(r.json()["message"])

    return r.json()["data"]


def mark_archive_as_deleted(archive_id, uniq_id):
    server_url = os.environ["SERVER_URL"]

    r = requests.get(
        server_url
        + "/archive/mark-archive-as-deleted/"
        + str(archive_id)
        + "?uniq_id="
        + uniq_id
    )

    if r.status_code == 404:
        raise ArchiveNotFound(archive_id)
    if r.status_code == 500:
        raise AnisServerError(r.json()["message"])

    return r.json()["data"]


class ConfigKeyNotFound(Exception):
    """
    Config Key Not Found
    """

    def __init__(self, value: str):
        super().__init__(self)
        self.value = value

    def __str__(self):
        return f"{self.value} was not found in the environment variables"


class DatasetNotFound(Exception):
    def __init__(self, dname):
        super().__init__(self)
        self.dname = dname

    def __str__(self):
        return f"Dataset {self.dname} was not found"


class ArchiveNotFound(Exception):
    def __init__(self, archive_id):
        super().__init__(self)
        self.archive_id = archive_id

    def __str__(self):
        return f"Archive with id {self.archive_id} was not found"


class AnisServerError(Exception):
    def __init__(self, message):
        super().__init__(self, message)
        self.message = message

    def __str__(self):
        return f"Anis-server error: {self.message}"
