# Standard library imports
import json
import logging
import os
from zipfile import ZipFile, ZipInfo, ZIP_DEFLATED

# Local application imports
from anis_tasks import utils


def archive_handler(ch, method, properties, body):
    logging.info(f"Processing a new archive message: {body}")

    # Decode JSON
    message = json.loads(body)

    # Mark archive as in progress
    utils.mark_archive_as_in_progress(message["archive_id"], message["uniq_id"])

    # Retrieve metadata information
    dataset = utils.get_dataset(message["instance_name"], message["dataset_name"])
    attributes = utils.get_attributes(message["instance_name"], message["dataset_name"])
    attributes_selected = get_attributes_selected_archive_files(
        attributes, message["param_a"]
    )

    # Retrieve data
    data = utils.search_data(
        message["instance_name"],
        message["dataset_name"],
        message["query"],
        message["token"],
    )

    # Create a ZipFile object
    data_path = utils.get_data_path()
    archive_folder = utils.get_archive_folder()

    if message["user_email"] is not None:
        user_directory = message["user_email"]
    else:
        user_directory = "TEMP"

    zip_folder_path = os.path.join(
        data_path,
        archive_folder,
        user_directory,
        message["instance_name"],
        message["dataset_name"],
    )

    if not os.path.exists(zip_folder_path):
        os.makedirs(zip_folder_path)
    zip_path = zip_folder_path + "/" + str(message["archive_id"]) + ".zip"
    logging.info(f"Create file: {zip_path}")
    zipf = ZipFile(zip_path + ".tmp", "w", ZIP_DEFLATED)

    # Search files
    for row in data.json():
        for attribute in attributes_selected:
            attribute_label = attribute["label"]
            file_path = (
                utils.get_data_path()
                + dataset["full_data_path"]
                + "/"
                + str(row[attribute_label])
            )
            if os.path.exists(file_path) and os.path.isfile(file_path) and file_path:
                # Adds file to the zip archive (streaming)
                with open(file_path, 'rb') as f:
                    zipf.writestr(row[attribute_label], f.read())

    # Close the Zip File
    zipf.close()

    # Rename the tmp zip file with the correct archive name
    os.rename(zip_path + ".tmp", zip_path)

    # Mark archive as finished
    utils.mark_archive_as_finished(message["archive_id"], message["uniq_id"])

    logging.info("Zip created: " + zip_path)


def get_attributes_selected_archive_files(attributes, param_a):
    attributes_selected = []
    ids = param_a.split(";")
    for attribute in attributes:
        if str(attribute["id"]) in ids and attribute["archive"]:
            attributes_selected.append(attribute)

    return attributes_selected
