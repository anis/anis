# Standard library imports
import logging
import sys

# Third party imports
import pika
from pika.exceptions import AMQPConnectionError
from retry import retry

# Local application imports
from anis_tasks import archive, clean_archives, utils


@retry(AMQPConnectionError, delay=5, jitter=(1, 3))
def run() -> None:
    try:
        utils.check_config()
    except utils.ConfigKeyNotFound as e:
        logging.error("Config error")
        logging.error(e)
        return

    # Connect to the rabbitMQ server
    credentials = pika.PlainCredentials(utils.get_rmq_user(), utils.get_rmq_password())
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(
            host=utils.get_rmq_host(),
            port=utils.get_rmq_port(),
            credentials=credentials,
        )
    )
    channel = connection.channel()

    # Add archive task handler
    channel.queue_declare(queue="archive")
    channel.basic_consume(
        queue="archive", on_message_callback=archive.archive_handler, auto_ack=True
    )

    # Add clean archives task handler
    channel.queue_declare(queue="clean_archives")
    channel.basic_consume(
        queue="clean_archives", on_message_callback=clean_archives.clean_archives_handler, auto_ack=True
    )

    # Start
    logging.info("ANIS tasks started")
    channel.start_consuming()


def exec() -> None:
    logging.basicConfig(
        stream=sys.stderr,
        level=logging.INFO,
        format="[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s",
    )
    run()
