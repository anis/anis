# Standard library imports
import json
import logging
import os
from zipfile import ZipFile

# Local application imports
from anis_tasks import utils


def clean_archives_handler(ch, method, properties, body):
    logging.info(f"Processing clean archives message: {body}")

    # Decode JSON
    message = json.loads(body)

    # Loop on archives to delete
    for archive in message:
        # Remove file
        os.remove(archive['file_path'])

        # Mark archive as finished
        utils.mark_archive_as_deleted(archive["archive_id"], archive["uniq_id"])

    logging.info(f"Processing clean archives completed")
