"""Module to mock requests.get for the tests."""

import pytest

import requests
from os import path

import anis_services.utils as utils


@pytest.fixture(autouse=True)
def no_requests(monkeypatch):
    """Remove requests.sessions.Session.request for all tests."""
    monkeypatch.delattr("requests.sessions.Session.request")


@pytest.fixture()
def bad_token(monkeypatch):
    def check_token(
        iname: str = None, dname: str = None, request: requests.Request = None
    ):
        raise utils.AnisServicesError(msg="not ok", status_code=401)

    monkeypatch.setattr(utils, "verify_token", check_token)


@pytest.fixture()
def good_token(monkeypatch):
    def check_token(
        iname: str = None, dname: str = None, request: requests.Request = None
    ):
        return "ok"

    monkeypatch.setattr(utils, "verify_token", check_token)


@pytest.fixture()
def os_path_exists_mocker(monkeypatch):
    def mocked_exists(filename: str):
        return True

    monkeypatch.setattr(path, "exists", mocked_exists)
