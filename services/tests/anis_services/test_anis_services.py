"""Test __init__ for anis_services.

The aim of this module is to test the app creation.
"""

from unittest.mock import MagicMock

import pytest

from anis_services import create_app, utils


@pytest.fixture()
def app_fixture(monkeypatch):
    monkeypatch.setenv("DATA_PATH", "/data")
    monkeypatch.setenv("SERVER_URL", "http://server")
    app = create_app()
    app.config.update(
        {
            "TESTING": True,
        }
    )

    yield app


@pytest.fixture()
def del_env_variables(monkeypatch):
    monkeypatch.delenv("DATA_PATH")
    monkeypatch.delenv("SERVER_URL")


@pytest.fixture()
def client(app_fixture):
    return app_fixture.test_client()


class TestApp:
    def test_create_app(self, app_fixture):
        blueprints = app_fixture.iter_blueprints()
        for blueprint in blueprints:
            assert blueprint

    def test_request_home(self, client):
        response = client.get("/")
        assert response.json == {"message": "it works!"}


class TestUtilsCall:
    def test_config_check(self, monkeypatch):
        """Test the raising of an error if ENV variables does not exist.

        This test shall fail locally but not with the container up. Indeed, ENV
        variables are defined for the `CI` and in the `docker-compose.yml`.
        """
        mock_config = MagicMock()
        monkeypatch.setattr(utils, "check_config", mock_config)
        mock_config.side_effect = utils.ConfigKeyNotFound("KEY")
        with pytest.raises(utils.ConfigKeyNotFound):
            _ = create_app()
