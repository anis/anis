"""To test anis_service.fits_service.fits_service module."""

import pytest
from typing import Optional, Union

from astropy.io import fits
from astropy.utils.data import get_pkg_data_filename
from astropy.nddata.utils import NoOverlapError

from anis_services import create_app
from anis_services import utils
from anis_services.fits_service import cut


@pytest.fixture()
def app(monkeypatch):
    monkeypatch.setenv("DATA_PATH", "/data")
    monkeypatch.setenv("SERVER_URL", "http://server")
    app = create_app()
    app.config.update(
        {
            "TESTING": True,
        }
    )

    return app


@pytest.fixture()
def get_absolute_filename_mocker(monkeypatch):
    def mocked_get_absolute_filename(
        iname: str = None, dname: str = None, filename: str = None
    ):
        image_file = get_pkg_data_filename("tutorials/FITS-images/HorseHead.fits")
        return {"message": image_file}, 200

    monkeypatch.setattr(utils, "get_absolute_filename", mocked_get_absolute_filename)


@pytest.fixture()
def throw_no_overlap(monkeypatch):
    def throw_overlap(hdu: fits.ImageHDU, ra: float, dec: float, radius: float):
        raise NoOverlapError

    return monkeypatch.setattr(cut, "fits_cut_hdu", throw_overlap)


@pytest.fixture()
def fits_cut_raise_mocker(monkeypatch):
    def fits_cut(
        hdul: fits.HDUList = None,
        hdu_number: Optional[Union[str, float]] = None,
        ra: float = None,
        dec: float = None,
        radius: float = None,
    ):
        raise cut.FitsCutError(msg="not ok")

    monkeypatch.setattr(cut, "fits_cut", fits_cut)


class TestFitsService:
    def test_get_fits_image_limits_error(self, app, bad_token):
        with app.test_client() as c:
            response = c.get("/fits/get-fits-image-limits/default/observations?")
            assert response.status == "400 BAD REQUEST"
            assert response.json == {"message": "Parameter filename is mandatory"}

            response = c.get(
                "/fits/get-fits-image-limits/default/observations?filename=coconut"
            )
            assert response.status == "401 UNAUTHORIZED"

    def test_get_fits_image_limits_success(
        self, app, good_token, get_absolute_filename_mocker
    ):
        with app.test_client() as c:
            response = c.get(
                "/fits/get-fits-image-limits/default/observations?filename=coconut&stretch=linear&pmin=0.23&pmax=0.22&axes=false"
            )
            assert response

    def test_fits_to_png_error(self, app, bad_token):
        with app.test_client() as c:
            response = c.get("/fits/fits-to-png/default/observations?")
            assert response.status == "400 BAD REQUEST"
            assert response.json == {"message": "Parameter filename is mandatory"}

            response = c.get(
                "/fits/fits-to-png/default/observations?filename=coconut&stretch=linear&pmin=0.23&pmax=0.22&axes=false"
            )
            assert response.status == "401 UNAUTHORIZED"

    def test_get_fits_to_png_success(
        self, app, good_token, get_absolute_filename_mocker
    ):
        with app.test_client() as c:
            response = c.get(
                "/fits/fits-to-png/default/observations?filename=coconut&stretch=linear&pmin=0.23&pmax=0.22&axes=false"
            )
            assert response

    def test_fits_cut_error(
        self, app, good_token, fits_cut_raise_mocker, get_absolute_filename_mocker
    ):
        with app.test_client() as c:
            response = c.get("/fits/fits-cut/default/observations?")
            assert response.status == "400 BAD REQUEST"
            assert response.json == {"message": "Parameter ra is mandatory"}

            response = c.get(
                "/fits/fits-cut/default/observations?filename=coconut&ra=0.12&dec=0.23&radius=0.22"
            )
            assert response.status == "400 BAD REQUEST"
            assert response.json == {"message": "not ok"}

    def test_fits_cut_success(self, app, good_token, get_absolute_filename_mocker):
        with app.test_client() as c:
            response = c.get(
                "/fits/fits-cut/default/observations?filename=coconut&ra=85.21&dec=-2.45&radius=4"
            )
            assert response

            response = c.get(
                "/fits/fits-cut/default/observations?filename=coconut&ra=85.21&dec=-2.45&radius=4&hdu_number=300"
            )
            assert response.status == "400 BAD REQUEST"
            assert response.json == {
                "message": "HDU header does not exist in the FITS to cut"
            }

    def test_fits_cut_to_png_error(self, app, good_token, get_absolute_filename_mocker):
        with app.test_client() as c:
            response = c.get("/fits/fits-cut-to-png/default/observations?")
            assert response.status == "400 BAD REQUEST"
            assert response.json == {"message": "Parameter filename is mandatory"}

    def test_get_fits_cut_to_png_success(
        self, app, good_token, get_absolute_filename_mocker
    ):
        with app.test_client() as c:
            response = c.get(
                "/fits/fits-cut-to-png/default/observations?filename=coconut&ra=85.21&dec=-2.45&radius=4&stretch=linear&pmin=0.23&pmax=0.22&axes=false&hdu_number=300"
            )
            assert response.status == "400 BAD REQUEST"
            assert response.json == {
                "message": "HDU header does not exist in the FITS to cut"
            }

    def test_get_fits_cut_to_png_no_overlap(
        self, app, good_token, get_absolute_filename_mocker, throw_no_overlap
    ):
        with app.test_client() as c:
            response = c.get(
                "/fits/fits-cut-to-png/default/observations?filename=coconut&ra=85.21&dec=-2.45&radius=4&stretch=linear&pmin=0.23&pmax=0.22&axes=false&hdu_number=0"
            )
            assert response.status == "200 OK"
