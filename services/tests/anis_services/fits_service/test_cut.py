"""Module to test cut.py in fits_service."""

import warnings

import pytest
from astropy.io import fits
from astropy.utils.data import get_pkg_data_filename

from anis_services.fits_service import cut


@pytest.fixture(scope="class")
def file():
    image_file = get_pkg_data_filename("tutorials/FITS-images/HorseHead.fits")
    return image_file


class TestCut:
    def test_get_image_limit(self, file):
        res = cut.get_image_limits(file, 0)
        assert res

    def test_fits_cut(self, file):
        hdul = fits.open(file)
        res = cut.fits_cut(hdul, 0, 85.21, -2.45, 4)
        assert res

        with pytest.raises(cut.FitsCutError) as exc_info:
            res = cut.fits_cut(hdul, 300, 85.21, -2.45, 4)
        str(exc_info.value) == "HDU header does not exist in the FITS to cut"

        hdul.close()
