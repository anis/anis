"""Module to test the module fits_service.spectra_service."""

import pytest

from anis_services import create_app
from anis_services import utils
import anis_services.fits_service.spectra as spectra


@pytest.fixture()
def app(monkeypatch):
    monkeypatch.setenv("DATA_PATH", "/data")
    monkeypatch.setenv("SERVER_URL", "http://server")
    app = create_app()
    app.config.update(
        {
            "TESTING": True,
        }
    )

    return app


@pytest.fixture()
def get_filename_mocker(monkeypatch):
    def get_absolute_filename(
        iname: str = None, dname: str = None, filename: str = None
    ):
        message = {"message": "yo"}
        return message, 200

    monkeypatch.setattr(utils, "get_absolute_filename", get_absolute_filename)


@pytest.fixture()
def spectra_to_csv_mocker(monkeypatch):
    def spectra_to_csv(absolute_filename: str = None, filename: str = None):
        return "42,42"

    monkeypatch.setattr(spectra, "spectra_to_csv", spectra_to_csv)


class TestSpectraService:
    def test_spectra_to_csv_error(self, app, bad_token):
        with app.test_client() as c:
            response = c.get("spectra/spectra-to-csv/default/observations?")
            assert response.status == "400 BAD REQUEST"
            assert response.json == {"message": "Parameter filename is mandatory"}

            response = c.get("spectra/spectra-to-csv/default/observations?filename=yo")
            assert response.status == "401 UNAUTHORIZED"

    def test_spectra_to_csv_success(
        self, app, good_token, get_filename_mocker, spectra_to_csv_mocker
    ):
        with app.test_client() as c:
            response = c.get("spectra/spectra-to-csv/default/observations?filename=yo")
            assert response.status == "200 OK"
