"""Module to test fits_service.plot service."""

import pytest

from astropy.io.fits import open
from astropy.utils.data import get_pkg_data_filename

from anis_services.fits_service import plot


@pytest.fixture(scope="class")
def hdu():
    filename = get_pkg_data_filename("galactic_center/gc_msx_e.fits")
    hdu = open(filename)[0]
    return hdu


class TestPlot:
    def test_plot(self, hdu):
        result = plot.create_figure(hdu, "linear", 0.05, 0.95, "true")
        assert result

        result = plot.create_figure(hdu, "linear", 0.05, 0.95, "bla")
        assert result

        result = plot.create_figure(hdu, "linear", 0.05, 0.95, "bla", "viridis")
        assert result
