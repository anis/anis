"""Module to test the module fits_service.spectra."""

import pytest

from astropy.utils.data import get_pkg_data_filename
from anis_services.fits_service import spectra


@pytest.fixture(scope="class")
def absolute_filename():
    image_file = get_pkg_data_filename("tutorials/FITS-images/HorseHead.fits")
    return image_file


class TestSpectra:
    def test_spectra_to_csv(self, absolute_filename):
        res = spectra.spectra_to_csv(absolute_filename, "HorseHead.fits")
        assert res
