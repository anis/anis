"""Module to test the different strategies for spectra."""

import numpy as np
import pytest
from astropy.io import fits
from astropy.io.fits import BinTableHDU, Card, HDUList, Header
from astropy.table import Table
from astropy.utils.data import get_pkg_data_filename

import anis_services.fits_service.spectra_strategies as strategies


@pytest.fixture()
def hdul():
    filename = get_pkg_data_filename("galactic_center/gc_msx_e.fits")
    hdul = fits.open(filename)
    return hdul


class TestSpectraStrategies:
    def test_default_strategy(self, hdul):
        default_strategy = strategies.DefaultStrategy()
        assert default_strategy
        assert default_strategy.algorithm(hdul)

    def test_el_cosmos_strategy(self):
        data = np.array([[2, 3], [3, 4]])
        table = Table(data, names=("wavelength", "flux"))
        hdu = BinTableHDU(table)
        hdul = HDUList([hdu, hdu])
        el_cosmos = strategies.ElCosmosStrategy()
        assert el_cosmos.algorithm(hdul)

    def test_espresso_strategy(self):
        values = np.arange(2 * 4).reshape(2, 4)
        hdu = fits.ImageHDU(values)
        hdul = HDUList([hdu, hdu])
        esp = strategies.EspressoStrategy()
        assert esp.algorithm(hdul)

    def test_sixdf_strategy(self):
        values = np.arange(2 * 4).reshape(2, 4)
        header = Header(
            cards=[
                Card("CRPIX1", value=1),
                Card("CRVAL1", value=1),
                Card("CDELT1", value=1),
            ]
        )
        hdu = fits.ImageHDU(values, header=header)
        hdul = HDUList([hdu for _ in range(8)])
        six = strategies.SixdFStrategy()
        assert six.algorithm(hdul)

    def test_gamadr2lt_strategy(self):
        values = np.arange(2 * 4)
        header = Header(
            cards=[
                Card("CRPIX", value=1),
                Card("CRVAL", value=1),
                Card("CDELT", value=1),
            ]
        )
        hdu = fits.ImageHDU(values, header=header)
        hdul = HDUList([hdu])
        gam = strategies.GamaDR2LTStrategy()
        assert gam.algorithm(hdul)

    def test_gamadr2aat_strategy(self):
        values = np.arange(2 * 4).reshape(2, 4)
        header = Header(
            cards=[
                Card("CRPIX1", value=1),
                Card("CRVAL1", value=1),
                Card("CD1_1", value=1),
            ]
        )
        hdu = fits.ImageHDU(values, header=header)
        hdul = HDUList([hdu])
        gam = strategies.GamaDR2AATStrategy()
        assert gam.algorithm(hdul)

    def test_zcosmosbrightdr3_strategy(self):
        values = np.arange(2 * 4 * 4).reshape(2, 4, 4)
        header = Header(
            cards=[
                Card("NELEM", value=1),
            ]
        )
        hdu = fits.ImageHDU(values, header=header)
        hdul = HDUList([hdu, hdu])
        zc = strategies.ZCosmosBrightDr3Strategy()
        assert zc.algorithm(hdul)
