"""Module to test the FindStrategy."""

import pytest
from astropy.utils.data import get_pkg_data_filename
from astropy.io import fits

from anis_services.fits_service.spectra_strategies.find_stratgy import FindStrategy


@pytest.fixture()
def hdul():
    filename = get_pkg_data_filename('galactic_center/gc_msx_e.fits')
    hdul = fits.open(filename)
    return hdul


class TestFindStrategy:
    def test_find_strategy(self, hdul, mocker):
        find_strategy = FindStrategy()
        mock_spectra = mocker.spy(find_strategy, 'algorithm')
        find_strategy.algorithm(hdul)
        assert mock_spectra.call_count == 1
