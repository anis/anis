"""Test utils module of anis_services."""

import pytest
import requests

from anis_services import utils


@pytest.fixture()
def get_data(monkeypatch):
    """Server responds correct data."""

    def get(uri):
        r = requests.Response()
        r.status_code = 200
        r._content = b'{"data": "ok"}'
        return r

    monkeypatch.setattr(requests, "get", get)


@pytest.fixture()
def get_error(monkeypatch):
    """Server crashes."""

    def get(uri):
        r = requests.Response()
        r.status_code = 500
        r._content = b'{"message": "berk"}'
        return r

    monkeypatch.setattr(requests, "get", get)


@pytest.fixture()
def good_dataset(monkeypatch):
    def get_dataset(iname: str = None, dname: str = None):
        dataset = {"full_data_path": "yo"}
        return dataset

    monkeypatch.setattr(utils, "get_dataset", get_dataset)


class TestUtils:
    """Test all utils functions."""

    def test_check_config(self, monkeypatch):
        """check_config shall return ConfigKeyNotFound exception if ENV
        variables are not set.

        check_config shall not return ConfigKeyNotFound exception if ENV
        variables are set.
        """
        monkeypatch.setenv("DATA_PATH", "/data")
        monkeypatch.setenv("SERVER_URL", "http://server")
        try:
            utils.check_config()
        except utils.ConfigKeyNotFound:
            pytest.fail(
                "ConfigKeyNotFound was raised while ENV variables have been set"
            )

        monkeypatch.delenv("DATA_PATH", raising=False)
        monkeypatch.delenv("SERVER_URL", raising=False)
        with pytest.raises(utils.ConfigKeyNotFound):
            utils.check_config()

    def test_get_dataset_success(self, monkeypatch, get_data):
        """get_dataset should not return any error and get data for defined
        instances and datasets.
        """
        monkeypatch.setenv("DATA_PATH", "/data")
        monkeypatch.setenv("SERVER_URL", "http://server")
        try:
            _ = utils.get_dataset(iname="default", dname="observations")
        except utils.AnisServerError:
            pytest.fail(
                "An AnisServerError error was raised while connecting to the database."
            )

    def test_get_dataset_error(self, monkeypatch, get_error):
        """ """
        monkeypatch.setenv("DATA_PATH", "/data")
        monkeypatch.setenv("SERVER_URL", "http://server")

        with pytest.raises(utils.AnisServerError):
            _ = utils.get_dataset(iname="default", dname="bleh")

    def test_get_data_success(self, monkeypatch, get_data):
        """get_dataset should not return any error and get data for defined
        instances and datasets.
        """
        monkeypatch.setenv("DATA_PATH", "/data")
        monkeypatch.setenv("SERVER_URL", "http://server")
        try:
            _ = utils.get_data(
                iname="default", dname="observations", query_string="a=1&c=1::eq::418"
            )
        except utils.DatasetNotFound:
            pytest.fail(
                "An DatasetNotFound error was raised while connecting to the database."
            )
        except utils.AnisServerError:
            pytest.fail(
                "An AnisServerError error was raised while connecting to the database."
            )

    def test_get_data_error(self, monkeypatch, get_error):
        """get_dataset should return a connection Error (DatasetNotFound) for
        undefined instances and datasets.
        """
        monkeypatch.setenv("SERVER_URL", "server")
        with pytest.raises(utils.AnisServerError):
            _ = utils.get_data(
                iname="default",
                dname="observations",
                query_string="a=1&c=1::eq::418",
            )

    def test_exceptions(self):
        anis_error = utils.AnisServerError("coconut")
        expr = str(anis_error)
        assert expr == "Anis-server error: coconut"

        config_key = utils.ConfigKeyNotFound("DATA_PATH")
        expr = str(config_key)
        assert expr == "DATA_PATH was not found in the environment variables"

    def test_get_file_path(self, monkeypatch, os_path_exists_mocker, good_dataset):
        monkeypatch.setenv("DATA_PATH", "./data")
        res = utils.get_file_path("default", "observations", "coucou")
        assert res

        with pytest.raises(utils.FileForbidden):
            _ = utils.get_file_path("default", "observations", "../coucou")

    def test_get_absolute_filename(
        self, monkeypatch, os_path_exists_mocker, good_dataset
    ):
        monkeypatch.setenv("DATA_PATH", "./data")
        res, status_code = utils.get_absolute_filename(
            "default", "observations", "coucou"
        )
        assert status_code == 200

        res, status_code = utils.get_absolute_filename(
            "default", "observations", "../coucou"
        )
        assert status_code == 403
