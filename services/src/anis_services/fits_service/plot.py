# This file is part of ANIS Client.
#
# @copyright Laboratoire d'Astrophysique de Marseille / CNRS
#
# For the full copyright and license information, please view the LICENSE file
# that was distributed with this source code.

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import patches
from aplpy import FITSFigure

matplotlib.use("Agg")


def create_figure(hdu, stretch, pmin, pmax, axes, theme=None):
    fig = plt.figure()
    try:
        image = FITSFigure(hdu, figure=fig)
    except Exception:
        image = FITSFigure(hdu, figure=fig, convention='wells')
    image.show_grayscale(stretch=stretch, pmin=pmin, pmax=pmax)
    if theme is not None:
        image.set_theme(theme)
    if axes == "false":
        image.axis_labels.hide()
        image.tick_labels.hide()
        image.ticks.hide()
    return fig


def create_no_overlap_figure():
    fig = plt.figure()
    left, width = 0.15, 0.7
    bottom, height = 0.25, 0.5
    right = left + width
    top = bottom + height
    ax = fig.add_axes([0, 0, 1, 1])

    p = patches.Rectangle(
        (left, bottom), width, height, fill=False, transform=ax.transAxes, clip_on=False
    )
    ax.add_patch(p)

    ax.text(
        0.5 * (left + right),
        0.5 * (bottom + top),
        "No Overlap",
        horizontalalignment="center",
        verticalalignment="center",
        fontsize=50,
        color="red",
        transform=ax.transAxes,
    )

    ax.set_axis_off()
    return fig
