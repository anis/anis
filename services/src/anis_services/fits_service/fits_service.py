# This file is part of ANIS Client.
#
# @copyright Laboratoire d'Astrophysique de Marseille / CNRS
#
# For the full copyright and license information, please view the LICENSE file
# that was distributed with this source code.

import io
import json
from logging import getLogger

from flask import Blueprint, request, Response
from astropy.io import fits
from astropy.nddata.utils import NoOverlapError
from astropy.table import Table
from werkzeug.datastructures import MultiDict

from .. import utils
from . import cut
from . import plot

dev = getLogger("dev")

FITS_CUT_MANDATORY_PARAMETERS = ["ra", "dec", "radius"]
FITS_TO_PNG_MANDATORY_PARAMETERS = ["filename", "stretch", "pmin", "pmax", "axes"]
FITS_CUT_TO_PNG_MANDATORY_PARAMETERS = [
    "filename",
    "ra",
    "dec",
    "radius",
    "stretch",
    "pmin",
    "pmax",
    "axes",
]
READ_FITS_PARAMETERS = ["filename", "x_column", "y_column", "hdu"]


class RequestMandatoryError(Exception):
    """A class stating that a parameter is not present

    :class:`Exception` is the parent class
    :param str msg: The message stating the missing parameter
    """

    def __init__(self, msg: str):
        self.msg = msg
        super().__init__(self.msg)

    def __str__(self):
        return f"{self.msg}"


def check_mandatory_params(params: MultiDict[str, str], mandatory_params: list[str]):
    """Check if mandatory parameters are all present in request arguments

    :param params: The parameters from which the presence is checked
    :param mandatory_params: All the necessary parameters for the request
    :raises RequestMandatoryError: The error thrown stating the missing parameter
    """
    for param in mandatory_params:
        if param not in params:
            raise RequestMandatoryError(f"Parameter {param} is mandatory")


fits_service = Blueprint("fits", __name__, url_prefix="/fits")


@fits_service.route("/get-fits-image-limits/<iname>/<dname>")
def get_fits_image_limits(iname: str, dname: str) -> dict[str, float]:
    """Route returning a JSON stating the limit of :class:`fits.ImageHDU`

    :param iname: The instance name.
    :type iname: str
    :param dname: The dataset name.
    :type dname: str
    :return: A `dict` transform internally into a JSON by `flask` or a JSON
        containing the message and the status code.
    :rtype: dict[str, float]
    """
    filename = request.args.get("filename")
    if filename is None:
        return {"message": "Parameter filename is mandatory"}, 400

    try:
        utils.verify_token(iname, dname, request)
    except utils.AnisServicesError as e:
        return e.msg, e.err_status()

    hdu_number = request.args.get("hdu_number", type=int)
    if hdu_number is None:
        hdu_number = 0

    msg, status_code = utils.get_absolute_filename(iname, dname, filename)
    if status_code != 200:
        return msg, status_code

    absolute_file_name = msg["message"]
    return cut.get_image_limits(absolute_file_name, hdu_number)


@fits_service.route("/fits-cut/<iname>/<dname>")
def fits_cut(iname, dname) -> Response:
    """Route executing a cut on FITS containing a :class:`fits.ImageHDU`

    The request posess mandatory arguments, the more principal being `filename`
    If the request argument `hdu_number` is `None`, defaults to 0. The latter
    can evaluate to `*`, so that the cut is made on all headers.

    :param iname: The instance name.
    :type iname: str
    :param dname: The dataset name.
    :type dname: str
    :return: The response of the request if the cut is executed properly.
    :rtype: :class:`Response`
    """
    try:
        check_mandatory_params(request.args, FITS_CUT_MANDATORY_PARAMETERS)
    except RequestMandatoryError as e:
        return {"message": str(e)}, 400

    try:
        utils.verify_token(iname, dname, request)
    except utils.AnisServicesError as e:
        return e.msg, e.err_status()

    hdu_number = request.args.get("hdu_number")
    if hdu_number is None:
        hdu_number = 0

    filename = request.args.get("filename")
    ra = request.args.get("ra", type=float)
    dec = request.args.get("dec", type=float)
    radius = request.args.get("radius", type=float)

    msg, status_code = utils.get_absolute_filename(iname, dname, filename)
    if status_code != 200:
        return msg, status_code

    absolute_file_name = msg["message"]
    try:
        hdul = fits.open(absolute_file_name)
        hdul = cut.fits_cut(hdul, hdu_number, ra, dec, radius)
    except cut.FitsCutError as e:
        hdul.close()
        return {"message": str(e)}, 400

    output = io.BytesIO()
    hdul.writeto(output, overwrite=True)
    hdul.close()
    return Response(output.getvalue(), mimetype="image/fits")


@fits_service.route("/fits-cut-to-png/<iname>/<dname>")
def fits_cut_to_png(iname: str, dname: str) -> Response:
    """Route executing a cut on FITS containing a :class:`fits.ImageHDU` and
    returning the PNG image.

    The request posess mandatory arguments, the more principal being `filename`
    If the request argument `hdu_number` is `None`, defaults to 0.

    :param iname: The instance name.
    :type iname: str
    :param dname: The dataset name.
    :type dname: str
    :return: The response of the request containing the PNG image.
    :rtype: :class:`Response`
    """
    try:
        check_mandatory_params(request.args, FITS_CUT_TO_PNG_MANDATORY_PARAMETERS)
    except RequestMandatoryError as e:
        return {"message": str(e)}, 400

    try:
        utils.verify_token(iname, dname, request)
    except utils.AnisServicesError as e:
        return e.msg, e.err_status()

    hdu_number = request.args.get("hdu_number", default=None, type=int)
    if hdu_number is None:
        hdu_number = 0
    if hdu_number == "*":
        return {"message": "* in hdu_number is forbidden for this route"}, 400

    filename = request.args.get("filename")
    axes = request.args.get("axes", default=None)
    ra = request.args.get("ra", default=None, type=float)
    dec = request.args.get("dec", default=None, type=float)
    radius = request.args.get("radius", default=None, type=float)
    stretch = request.args.get("stretch", default=None)
    pmin = request.args.get("pmin", default=None, type=float)
    pmax = request.args.get("pmax", default=None, type=float)
    theme = request.args.get("theme", default=None)

    msg, status_code = utils.get_absolute_filename(iname, dname, filename)
    if status_code != 200:
        return msg, status_code

    absolute_file_name = msg["message"]
    try:
        hdul = fits.open(absolute_file_name)
        hdul = cut.fits_cut(hdul, hdu_number, ra, dec, radius)
    except cut.FitsCutError as e:
        hdul.close()
        return {"message": str(e)}, 400
    except NoOverlapError:
        hdul.close()
        fig = plot.create_no_overlap_figure()
        output = io.BytesIO()
        fig.savefig(output, format="png", bbox_inches="tight", pad_inches=0)
        return Response(output.getvalue(), mimetype="image/png")

    hdu = hdul[hdu_number]
    fig = plot.create_figure(hdu, stretch, pmin, pmax, axes, theme)

    output = io.BytesIO()
    if axes == "true":
        fig.savefig(output, format="png")
    else:
        fig.savefig(output, format="png", bbox_inches="tight", pad_inches=0)

    hdul.close()
    return Response(output.getvalue(), mimetype="image/png")


@fits_service.route("/fits-to-png/<iname>/<dname>")
def fits_to_png(iname, dname):
    """Route returning the PNG image of a :class:`fits.ImageHDU`.

    The request posess mandatory arguments, the more principal being `filename`
    If the request argument `hdu_number` is `None`, defaults to 0.

    :param iname: The instance name.
    :type iname: str
    :param dname: The dataset name.
    :type dname: str
    :return: The response of the request containing the PNG image.
    :rtype: :class:`Response`
    """
    try:
        check_mandatory_params(request.args, FITS_TO_PNG_MANDATORY_PARAMETERS)
    except RequestMandatoryError as e:
        return {"message": str(e)}, 400

    try:
        utils.verify_token(iname, dname, request)
    except utils.AnisServicesError as e:
        return e.msg, e.err_status()

    hdu_number = request.args.get("hdu_number", default=None, type=int)
    if hdu_number is None:
        hdu_number = 0
    if hdu_number == "*":
        return {"message": "* in hdu_number is forbidden for this route"}, 400

    filename = request.args.get("filename", default=None)
    stretch = request.args.get("stretch", default=None)
    pmin = request.args.get("pmin", default=None, type=float)
    pmax = request.args.get("pmax", default=None, type=float)
    axes = request.args.get("axes", default=None)
    theme = request.args.get("theme", default=None)

    msg, status_code = utils.get_absolute_filename(iname, dname, filename)
    if status_code != 200:
        return msg, status_code

    absolute_file_name = msg["message"]
    with fits.open(absolute_file_name) as hdul:
        hdu = hdul[hdu_number]
        fig = plot.create_figure(hdu, stretch, pmin, pmax, axes, theme)

    output = io.BytesIO()
    fig.savefig(output, format="png")

    return Response(output.getvalue(), mimetype="image/png")


@fits_service.route("/download-fits/<iname>/<dname>")
def download_fits(iname: str, dname: str) -> Response:
    """Route returning the the FITS file of a :class:`fits.ImageHDU`.

    :param iname: The instance name.
    :type iname: str
    :param dname: The dataset name.
    :type dname: str
    :return: The response of the request containing the FITS file.
    :rtype: :class:`Response`
    """
    try:
        data_from_server = utils.get_data(iname, dname, request.query_string.decode())
    except utils.AnisServicesError as e:
        return e.msg, e.err_status()

    colomn_names = tuple(data_from_server[0])
    column_values_dictionary = dict()

    for name in colomn_names:
        column_values_dictionary[name] = list()
        for data in data_from_server:
            column_values_dictionary[name].fitsend(data[name])

    output = io.BytesIO()
    t = Table(
        [list(item) for item in list(column_values_dictionary.values())],
        names=colomn_names,
    )
    t.write(output, format="fits")
    return Response(output.getvalue(), mimetype="fitslication/fits")


@fits_service.route("/read-fits/<iname>/<dname>")
def read_fits(iname, dname):
    """Route returning the the FITS file of a :class:`fits.ImageHDU` under JSON.

    :param iname: The instance name.
    :type iname: str
    :param dname: The dataset name.
    :type dname: str
    :return: The response of the request containing the FITS file under JSON.
    :rtype: :class:`Response`
    """
    try:
        check_mandatory_params(request.args, READ_FITS_PARAMETERS)
    except RequestMandatoryError as e:
        return {"message": str(e)}, 400

    try:
        utils.verify_token(iname, dname, request)
    except utils.AnisServicesError as e:
        return e.msg, e.err_status()

    filename = request.args.get("filename")
    x_column = request.args.get("xcolumn")
    y_column = request.args.get("ycolumn")
    hdu = request.args.get("hdu", type=int)

    msg, status_code = utils.get_absolute_filename(iname, dname, filename)
    if status_code != 200:
        return msg, status_code

    absolute_file_name = msg["message"]
    with fits.open(absolute_file_name) as hdulist:
        data = hdulist[hdu].data
        result = {
            "x": [float(el) for el in list(data[x_column])],
            "y": [float(el) for el in list(data[y_column])],
        }

    return Response(json.dumps(result))
