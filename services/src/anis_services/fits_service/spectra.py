# This file is part of ANIS Client.
#
# @copyright Laboratoire d'Astrophysique de Marseille / CNRS
#
# For the full copyright and license information, please view the LICENSE file
# that was distributed with this source code.

from astropy.io import fits

from .spectra_strategies import StrategySolver
from .spectra_strategies import DefaultStrategy
from .spectra_strategies import SixdFStrategy
from .spectra_strategies import GamaDR2AATStrategy
from .spectra_strategies import GamaDR2LTStrategy
from .spectra_strategies import ZCosmosBrightDr3Strategy
from .spectra_strategies import EspressoStrategy
from .spectra_strategies import ElCosmosStrategy

STRATEGY_PER_FILENAME = {
    "6dF": SixdFStrategy(),
    "AAT": GamaDR2AATStrategy(),
    "LT": GamaDR2LTStrategy(),
    "zCOSMOS_BRIGHT_DR3": ZCosmosBrightDr3Strategy(),
    "ESPRE": EspressoStrategy(),
    "sed_": ElCosmosStrategy()
}


def spectra_to_csv(absolute_filename: str, filename: str) -> str:
    """Transform data of a FITS file into a CSV file.

    :param absolute_filename: The absolute filename (full directory) file name to open.
    :type absolute_filename: str
    :param filename: The filename itself (used to discriminate strategies)
    :type filename: str
    :return: a CSV text
    :rtype: str
    """
    solver = StrategySolver(DefaultStrategy())

    for key in STRATEGY_PER_FILENAME.keys():
        if key in filename:
            solver = StrategySolver(STRATEGY_PER_FILENAME[key])
            break

    with fits.open(absolute_filename) as hdul:
        csv = solver.execute(hdul)

    return csv
