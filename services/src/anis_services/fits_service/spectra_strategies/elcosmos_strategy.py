from .find_stratgy import FindStrategy


class ElCosmosStrategy(FindStrategy):
    def algorithm(self, hdulist):
        wave = hdulist[1].data['wavelength']
        flux = hdulist[1].data['flux']

        csv = "x,Flux\n" + "\n".join(
            [f"{x:.2f},{y:e}" for x, y in zip(wave, flux)]
        )

        return csv
