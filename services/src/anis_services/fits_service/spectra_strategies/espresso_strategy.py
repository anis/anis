from .find_stratgy import FindStrategy


class EspressoStrategy(FindStrategy):
    def algorithm(self, hdulist):
        header = hdulist[1].header

        nbpix = header["NAXIS2"]
        tbdata = hdulist[1].data

        csv = "x,Flux\n"
        i = 0
        while i < nbpix:
            csv += "%.2f,%e" % (tbdata[i][0], tbdata[i][2])
            csv += "\n"
            i = i + 100

        return csv
