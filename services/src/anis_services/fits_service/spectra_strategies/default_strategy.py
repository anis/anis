from .find_stratgy import FindStrategy


class DefaultStrategy(FindStrategy):
    def algorithm(self, hdulist):
        header = hdulist[0].header

        naxis = header["NAXIS"]
        nbpix = header["NAXIS1"]

        crpix = 1
        cdelt = 1
        crval = 1

        if "CRPIX1" in header:
            crpix = header["CRPIX1"]
        elif "CRPIX" in header:
            crpix = header["CRPIX"]

        if "CRVAL1" in header:
            crval = header["CRVAL1"]
        elif "CRVAL" in header:
            crval = header["CRVAL"]

        if "CDELT1" in header:
            cdelt = header["CDELT1"]
        elif "CDELT" in header:
            cdelt = header["CDELT"]
        elif "CD1_1" in header:
            cdelt = header["CD1_1"]

        if naxis == 1:
            tbdata = hdulist[0].data
        else:
            tbdata = hdulist[0].data[0]

        csv = "x,Flux\n"
        i = 0
        while i < nbpix:
            csv += "%.2f,%e" % ((((i - crpix + 1) * cdelt) + crval), tbdata[i])
            csv += "\n"
            i = i + 1

        return csv
