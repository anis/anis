import math

from .find_stratgy import FindStrategy


class GamaDR2LTStrategy(FindStrategy):
    def algorithm(self, hdulist):
        header = hdulist[0].header
        tbdata = hdulist[0].data

        nbpix = header["NAXIS1"]
        crpix = header["CRPIX"]
        crval = header["CRVAL"]
        cdelt = header["CDELT"]

        csv = "x,Flux\n"
        i = 0
        while i < nbpix:
            csv += "%.2f,%e" % (
                (((i - crpix + 1) * cdelt) + crval),
                math.pow(10, -18) * tbdata[i],
            )
            csv += "\n"
            i = i + 1

        return csv
