import math

from .find_stratgy import FindStrategy


class SixdFStrategy(FindStrategy):
    def algorithm(self, hdulist):
        header = hdulist[7].header
        tbdata = hdulist[7].data[0]

        nbpix = header["NAXIS1"]
        crpix = header["CRPIX1"]
        crval = header["CRVAL1"]
        cdelt = header["CDELT1"]

        csv = "x,Flux\n"
        i = 0
        while i < nbpix:
            csv += "%.2f,%e" % (
                (((i - crpix + 1) * cdelt) + crval),
                math.pow(10, -18) * tbdata[i],
            )
            csv += "\n"
            i = i + 1

        return csv
