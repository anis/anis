class StrategySolver:
    def __init__(self, strategy):
        self.strategy = strategy

    def execute(self, hdulist):
        return self.strategy.algorithm(hdulist)
