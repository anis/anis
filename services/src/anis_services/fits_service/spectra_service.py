# This file is part of ANIS Client.
#
# @copyright Laboratoire d'Astrophysique de Marseille / CNRS
#
# For the full copyright and license information, please view the LICENSE file
# that was distributed with this source code.


from flask import Blueprint, Response, make_response, request

from anis_services import utils
from . import spectra

spectra_service = Blueprint("spectra", __name__, url_prefix="/spectra")


@spectra_service.route("/spectra-to-csv/<iname>/<dname>")
def spectra_to_csv(iname: str, dname: str) -> Response:
    """Define the route spectra/spectra-to-csv/

    A query to this route shall return a response containing a csv string
    generated from the specified FITS file.

    If the request errors or is not allowed, a message with the status code is sent.

    :param iname: The name of the instance in which the request to the route is made
    :type iname: str
    :param dname: The name of the dataset containing the destination for the FITS file
    :type dname: str
    :return: The response with the CSV of the spectra
    :rtype: `flask.Response`
    """
    filename = request.args.get("filename", default=None)
    if filename is None:
        return {"message": "Parameter filename is mandatory"}, 400

    try:
        utils.verify_token(iname, dname, request)
    except utils.AnisServicesError as e:
        return e.msg, e.err_status()

    msg, status_code = utils.get_absolute_filename(iname, dname, filename)
    if status_code != 200:
        return msg, status_code

    absolute_filename = msg["message"]
    csv = spectra.spectra_to_csv(absolute_filename, filename)

    return make_response(csv)
