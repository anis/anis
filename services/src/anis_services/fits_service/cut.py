"""Module with all cut utilities for FITS files."""

# This file is part of ANIS Client.
#
# @copyright Laboratoire d'Astrophysique de Marseille / CNRS
#
# For the full copyright and license information, please view the LICENSE file
# that was distributed with this source code.

from typing import Optional, Union

from astropy.io import fits
from astropy.nddata import Cutout2D
from astropy.nddata.utils import NoOverlapError
from astropy.wcs import WCS
from astropy.coordinates import SkyCoord
from astropy import units as u


class FitsCutError(Exception):
    """Error thrown when fits cut error occur.

    :class:`Exception` is the parent class.
    :param str msg: The message stating the error.
    """

    def __init__(self, msg):
        super().__init__(msg)
        self.msg = msg

    def __str__(self):
        return f"{self.msg}"


def fits_cut(
    hdul: fits.HDUList,
    hdu_number: Optional[Union[str, float]],
    ra: float,
    dec: float,
    radius: float,
) -> fits.HDUList:
    """Make the specified ra-dec cut on the header(s) specified.

    if `hdu_number` is `*`, all headers in `hdul` are cut, else, if it is a
    specfic `int`, only the specified header `hdul[hdu_number]` is updated.

    :param fits.HDUList hdul: The list of header :class:`fits.ImageHDU` passed.
    :param hdu_number: Specify the header to cut.
        Can be `*` or a specfic `int` to cut a specific fits header.
    :type hdu_number: Optional[str, int]
    :param float ra: The maximal radial ascension at which the cut occur.
    :param float dec: The maximal declination at which the cut occur.
    :param float radius: The maximal radius beyond which the cut occur.
    :raises FitsCutError: An error occur if the header wanted is not part of
        the header list.
    :raises NoOverlapError: An error occur if the cut specified does not overlap
        the original fits file. Due to internal call of :func:`fits_cut_hdu`
    :return: The updated :class:`HDUList`
    :rtype: :class:`fits.HDUList`
    """
    if hdu_number == "*":
        for hdu in hdul:
            cutout = fits_cut_hdu(hdu, ra, dec, radius)
            hdu.data = cutout.data
            hdu.header.update(cutout.wcs.to_header())
    else:
        try:
            hdu = hdul[int(hdu_number)]
        except IndexError:
            raise FitsCutError("HDU header does not exist in the FITS to cut")
        cutout = fits_cut_hdu(hdu, ra, dec, radius)
        hdu.data = cutout.data
        hdu.header.update(cutout.wcs.to_header())

    return hdul


def fits_cut_hdu(hdu: fits.ImageHDU, ra: float, dec: float, radius: float) -> WCS:
    """Make the fits cut effectively on the ImageHDU.

    :param hdu: The :class:`fits.HDUList` element to cut.
    :type hdu: :class:`fits.ImageHDU`
    :param float ra: The maximal radial ascension at which the cut occur.
    :param float dec: The maximal declination at which the cut occur.
    :param float radius: The maximal radius beyond which the cut occur.
    :raises NoOverlapError: An error occur if the cut specified does not overlap
        the original fits file.
    :return: The cutout data :class:`WCS`
    :rtype: :class:`WCS`
    """
    wcs = WCS(hdu.header)
    position = SkyCoord(ra, dec, unit="deg")
    size = u.Quantity(radius * 2, u.si.arcsec)

    try:
        cutout = Cutout2D(hdu.data, position=position, size=size, wcs=wcs)
    except NoOverlapError as e:
        raise e

    return cutout


def get_image_limits(filename: str, hdu_number: int) -> dict[str, float]:
    """Get the limit of the specified image :class:`fits.ImageHDU` in the `hdu_number` header

    :param filename: The name of the fits file to read from
    :param hdu_number: The header to look for to get the image
    :return: The limits in RA-DEC units (`pixel_to_world`) in a dict with the
    following keys

        - ra_min
        - ra_max
        - dec_min
        - dec_max

    """
    with fits.open(filename) as hdul:
        wcs = WCS(hdul[hdu_number].header)
        pmin = wcs.pixel_to_world(wcs.pixel_shape[0], wcs.pixel_shape[1])
        pmax = wcs.pixel_to_world(0, 0)

    return {
        "ra_min": pmin.ra.degree,
        "ra_max": pmax.ra.degree,
        "dec_min": pmin.dec.degree,
        "dec_max": pmax.dec.degree,
    }
