"""Module containing all utilities for anis-services."""

# This file is part of ANIS Client.
#
# @copyright Laboratoire d'Astrophysique de Marseille / CNRS
#
# For the full copyright and license information, please view the LICENSE file
# that was distributed with this source code.

import os
from logging import getLogger

import requests

dev = getLogger("dev")


def check_config():
    """Check if environments variables to run anis-services are defined.

    :raises ConfigKeyNotFound: Error stating the missing config key.
    """
    check_keys = {"DATA_PATH", "SERVER_URL"}
    for value in check_keys:
        if value not in os.environ.keys():
            raise ConfigKeyNotFound(value)


def verify_token(iname: str, dname: str, request: requests.Request) -> None:
    """Check if the request to the service is allowed or correct.

    This function calls the server internally (the service is then a proxy) to
    verify the request integrity. If the request is not allowed, or if the
    dataset/instance does not exist, an error is raised. This function shall be
    called for all routes.

    :param iname: The name of the instance.
    :type iname: str
    :param dname: The name of the dataset.
    :type dname: str
    :param token: The token passed to the request (may be None).
    :type token: str
    :raises AnisServicesError: Error if the request is not allowed or not
    correct.
    """
    server_url = os.environ["SERVER_URL"]

    # check token in headers
    token = request.headers.get("Authorization", type=str)
    if token is None:
        # if no token in headers, check token in url
        key = request.args.get("Authorization", default=None)
        if key is not None:
            token = "Bearer " + key

    r = requests.get(
        f"{server_url}/instance/{iname}/dataset/{dname}/verify-token",
        headers={"Authorization": token},
    )

    # Check both the existence of the dataset AND of the instance
    # If status_code == 401 => not authorized
    if r.status_code != 200:
        raise AnisServicesError(r.json(), r.status_code)

    return


def get_dataset(iname: str, dname: str):
    """Get the ANIS dataset configugation for a dataset in a instance

    :param iname: The name of the instance.
    :type iname: str
    :param dname: The name of the dataset.
    :type dname: str
    :raises DatasetNotFound: Error stating that the dataset is not found.
    :raises AnisServerError: Error stating an internal failure from the server.
    :return: The json encoded content of the response.
    """
    server_url = os.environ["SERVER_URL"]

    r = requests.get(f"{server_url}/instance/{iname}/dataset/{dname}")

    if r.status_code == 500:
        raise AnisServerError(r.json()["message"])

    return r.json()["data"]


def get_data(iname: str, dname: str, query_string: str):
    """Get the data with a specific query for a given instance on a given dataset.

    :param str iname: The name of an instance.
    :param str dname: The name of the dataset.
    :param str query_string: The query itself.
    :raises DatasetNotFound: Error stating the dataset is not found.
    :raises AnisServerError: Error stating an internal failure from the server.
    :raises AnisServicesError: Error stating an internal failure from the service.
    :return: The json encoded content of the response.
    """
    server_url = os.environ["SERVER_URL"]
    r = requests.get(f"{server_url}/search/{iname}/{dname}?{query_string}")

    if r.status_code == 404:
        raise DatasetNotFound("")
    if r.status_code == 500:
        raise AnisServerError(r.json()["message"])
    if r.status_code != 200:
        raise AnisServicesError(r.json(), r.status_code)

    return r.json()


def get_file_path(iname: str, dname: str, filename: str) -> str:
    """Get the full file path of a file stored in the dataset for a given filename in the given instance.

    :param str iname: The name of the instance.
    :param str dname: The name of the dataset.
    :param str filename: The file name.
    :raises FileForbidden: Error stating that the file is forbidden.
    :raises FileNotFound: Error stating that the file is not found.
    :raises DatasetNotFound: Error stating the dataset is not found.
    :raises AnisServerError: Error stating an internal failure from the server.
    :return: The file path.
    :rtype: str
    """
    data_path = os.environ["DATA_PATH"]
    try:
        dataset = get_dataset(iname, dname)
    except AnisServicesError as e:
        return {"message": str(e)}, e.err_status()

    dataset_data_path = dataset["full_data_path"]
    file_path = data_path + dataset_data_path + "/" + filename

    if ".." in filename:
        raise FileForbidden(filename)

    if not os.path.exists(file_path):
        raise FileNotFound(filename)

    return file_path


def get_absolute_filename(
    iname: str, dname: str, filename: str
) -> tuple[dict[str, str], int]:
    """Get the absolute file name of the given filename in the dataset provided for the given instance

    :param iname: The instance name.
    :param dname: The dataset name.
    :param filename: The file name.
    :return: The tuple with a message stating the error or the absolute
        filename itself with the status code. The tuple contains a `dict` with the
        single "message" key and the status code of the response.
    """
    try:
        absolute_file_name = get_file_path(iname, dname, filename)
    except AnisServicesError as e:
        return {"message": str(e)}, e.err_status()

    return {"message": absolute_file_name}, 200


class AnisServicesError(Exception):
    """Base class for Errors thrown by the anis-services server.

    :param msg: The message thrown by the error.
    :param status_code: The status code of the error.
    """

    def __init__(self, msg: str, status_code: int):
        """Define the constructor."""
        self.msg = msg
        self._status_code = status_code
        super().__init__(self.msg)

    def __str__(self) -> str:
        """Define the representation."""
        return self.msg

    def err_status(self) -> int:
        """Return the status code of the error.

        :return: The status code of the error
        :rtype: int
        """
        return self._status_code


class DatasetNotFound(AnisServicesError):
    """Error thrown when no Dataset is found for a query.

    :class:`AnisServicesError` is the parent class.
    :param dname: The name of the not-found dataset.
    """

    def __init__(self, dname: str):
        self.dname = dname
        self._status_code = 404
        self.msg = f"Dataset {self.dname} was not found"
        super().__init__(self.msg, self._status_code)


class AnisServerError(AnisServicesError):
    """Class representing anis-services internal error.

    :class:`AnisServicesError` is the parent class.
    :param str message: The message stating the internal error.
    """

    def __init__(self, message: str):
        self.msg = f"Anis-server error: {message}"
        self._status_code = 500
        super().__init__(self.msg, self._status_code)


class FileForbidden(AnisServicesError):
    """Error thrown when the file is forbidden.

    :class:`AnisServicesError` is the parent class.
    :param str filename: The forbidden file name.
    """

    def __init__(self, filename: str):
        self.filename = filename
        self._status_code = 403
        self.msg = f"File {self.filename} => It is forbidden to use '..'"
        super().__init__(self.msg, self._status_code)


class FileNotFound(AnisServicesError):
    """Error thrown when the file is not found.

    :class:`AnisServicesError` is the parent class.
    :param str filename: The file name not found.
    """

    def __init__(self, filename: str):
        self.filename = filename
        self._status_code = 404
        self.msg = f"File {self.filename} was not found"
        super().__init__(self.msg, self._status_code)


class ConfigKeyNotFound(AnisServicesError):
    """Error thrown when a config environment variable is not found.

    :class:`AnisServicesError` is the parent class.
    :param str value: The environment variable not defined.
    """

    def __init__(self, value):
        self.value = value
        self._status_code = 500
        self.msg = f"{self.value} was not found in the environment variables"
        super().__init__(self.msg, self._status_code)
