"""Module instantiating the Flask server defining anis services."""

# This file is part of ANIS Client.
#
# @copyright Laboratoire d'Astrophysique de Marseille / CNRS
#
# For the full copyright and license information, please view the LICENSE file
# that was distributed with this source code.

from logging.config import dictConfig

from flask import Flask
from flask_cors import CORS

from . import utils

dictConfig(
    {
        "version": 1,
        "formatters": {
            "extra": {
                "format": "[%(asctime)s] %(levelname)s | %(module)s >> %(message)s ",
            },
        },
        "handlers": {
            "extra": {
                "class": "logging.StreamHandler",
                "stream": "ext://flask.logging.wsgi_errors_stream",
                "formatter": "extra",
            }
        },
        "loggers": {
            "dev": {
                "level": "DEBUG",
                "handlers": ["extra"],
                "propagate": False,
            },
            "info": {
                "level": "INFO",
                "handlers": ["extra"],
                "propagate": False,
            },
        },
    }
)


def create_app() -> Flask:
    """Create the app serving all services.

    :return: The app
    :rtype: `flask.Flask.app`
    """
    utils.check_config()

    app = Flask(__name__)
    CORS(app)

    @app.route("/")
    def index():
        return {"message": "it works!"}

    from .fits_service import fits_service, spectra_service

    app.register_blueprint(fits_service)
    app.register_blueprint(spectra_service)

    return app
