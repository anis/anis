UID := 1000
GID := 1000

list:
	@echo ""
	@echo "Useful targets:"
	@echo ""
	@echo "  rebuild          > Rebuild all images and start containers for dev only"
	@echo "  start            > Start containers"
	@echo "  restart          > Restart containers"
	@echo "  stop             > Stop and kill running containers"
	@echo "  status           > Display stack containers status"
	@echo "  logs             > Display containers logs"
	@echo "  install_client   > Install client dependencies"
	@echo "  shell_client     > Shell into angular client container"
	@echo "  build_client     > Generate the angular client dist application (html, css, js)"
	@echo "  test_client      > Run the angular client unit tests and generate code coverage report"
	@echo "  test_client-live > Run the angular client unit tests on every file change"
	@echo "  lint_client      > Run angular eslint for the client"
	@echo "  install_server   > Install server dependencies"
	@echo "  shell_server     > Shell into php server container"
	@echo "  test_server      > Starts the server php unit tests"
	@echo "  phpcs            > Run php code sniffer test suite"
	@echo "  install_services > install services dependencies (virtualenv)"
	@echo "  shell_services   > Shell into python services"
	@echo "  test_services    > Starts the services unit tests"
	@echo "  install_tasks    > install tasks dependencies (virtualenv)"
	@echo "  shell_tasks      > Shell into python tasks"
	@echo "  test_tasks       > Starts the tasks unit tests"
	@echo "  create-db        > Create a database for dev only (need token_enabled=0)"
	@echo "  remove-pgdata    > Remove the anis-next database"
	@echo "  remove-matodata  > Remove the anis-next matomo volumes"
	@echo ""

rebuild:
	@docker compose up --build -d

start:
	@docker compose up -d

restart: stop start

stop:
	@docker compose kill
	@docker compose rm -v --force

status:
	@docker compose ps

logs:
	@docker compose logs -f -t

install_client:
	@docker run --init -it --rm --user $(UID):$(GID) \
	-v $(CURDIR)/client:/project \
	-w /project node:18-slim yarn install

shell_client:
	@docker compose exec client bash

build_client:
	@docker compose exec client ng build

test_client:
	@docker compose exec client npx jest --coverage --collectCoverageFrom='src/**/*.ts' --ci -w=2

test_client_live:
	@docker compose exec client npx jest --watchAll --coverage

lint_client:
	@docker compose exec client npm run lint

install_server:
	@docker run --init -it --rm \
	-v $(CURDIR)/server:/app --user $(UID):$(GID) \
	composer --ignore-platform-reqs install

shell_server:
	@docker compose exec server bash

test_server:
	@docker compose exec server bash ./run_tests.sh

phpcs:
	@docker compose exec server ./vendor/bin/phpcs

install_services:
	@docker compose exec services pip install -r requirements.txt

shell_services:
	@docker compose exec services bash

test_services:
	@docker compose exec services pytest -v --cov=/project/src/anis_services/ \
		--cov-report html:report --cov-report term

install_tasks:
	@docker compose exec tasks pip install -r requirements.txt

shell_tasks:
	@docker compose exec tasks bash

test_tasks:
	@docker compose exec tasks pytest -v --cov=/project/src/anis_tasks/ \
		--cov-report html:report --cov-report term

create-db:
	@docker compose exec keycloak kc.sh import --file /tmp/keycloak-anis-realm.json
	@docker compose exec server sh /mnt/init-keycloak.sh
	@docker compose exec server sh /mnt/create-db.sh

remove-pgdata:
	@docker volume rm anis_pgdata

remove-matodata:
	@docker volume rm anis_mariadata
	@docker volume rm anis_matomo
