<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification;

use App\Specification\Validator\NoRecordExists;

final class Image extends AbstractSpecification
{
    public function getSpecification(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getString(),
            'file_path' => $this->getString(),
            'hdu_number' => $this->getPositiveDigits(false),
            'file_size' => $this->getPositiveDigits(),
            'ra_min' => $this->getRa(),
            'ra_max' => $this->getRa(),
            'dec_min' => $this->getDec(),
            'dec_max' => $this->getDec(),
            'stretch' => $this->getString(),
            'pmin' => $this->getP(),
            'pmax' => $this->getP()
        ];
    }

    public function getId(): array
    {
        $spec = $this->getPositiveDigits();
        if ($this->checkNoRecordExists) {
            $spec['validators'][] = [
                'name' => NoRecordExists::class,
                'options' => [
                    'em' => $this->em,
                    'entityClassName' => 'App\Entity\Image',
                    'pkeyType' => NoRecordExists::CONCATENATE_PKEY,
                    'columnName' => 'id',
                    'extraId' => [ $this->args['name'], $this->args['dname'] ]
                ]
            ];
        }

        return $spec;
    }

    private function getP()
    {
        return [
            'required' => true,
            'filters' => [
                ['name' => 'ToFloat']
            ],
            'validators' => [
                [
                    'name' => 'Between',
                    'options' => [
                        'min' => 0,
                        'max' => 100
                    ]
                ]
            ]
        ];
    }
}
