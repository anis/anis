<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification;

use App\Specification\Validator\NoRecordExists;

final class Database extends AbstractSpecification
{
    public function getSpecification(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getString(),
            'dbname' => $this->getString(false),
            'dbtype' => $this->getString(false),
            'dbhost' => $this->getString(false),
            'dbport' => $this->getDbport(),
            'dblogin' => $this->getString(false),
            'dbpassword' => $this->getString(false),
            'dbdsn_file' => $this->getString(false)
        ];
    }

    public function getId(): array
    {
        $spec = $this->getPositiveDigits();
        if ($this->checkNoRecordExists) {
            $spec['validators'][] = [
                'name' => NoRecordExists::class,
                'options' => [
                    'em' => $this->em,
                    'entityClassName' => 'App\Entity\Database',
                    'pkeyType' => NoRecordExists::COMPOSITE_PKEY,
                    'columnName' => 'id',
                    'extraId' => [ 'instance' => $this->args['name'] ]
                ]
            ];
        }

        return $spec;
    }

    private function getDbport()
    {
        return [
            'required' => false,
            'validators' => [
                ['name' => 'Digits'],
                [
                    'name' => 'Between',
                    'options' => [
                        'min' => 0,
                        'max' => 65535
                    ]
                ]
            ]
        ];
    }
}
