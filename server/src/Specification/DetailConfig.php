<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification;

final class DetailConfig extends AbstractSpecification
{
    public function getSpecification(): array
    {
        return [
            'content' => $this->getContent(),
            'style_sheet' => $this->getStyleSheet()
        ];
    }

    protected function getContent(): array
    {
        return [
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim']
            ]
        ];
    }

    protected function getStyleSheet(): array
    {
        return [
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim']
            ]
        ];
    }
}
