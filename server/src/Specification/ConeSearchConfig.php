<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification;

final class ConeSearchConfig extends AbstractSpecification
{
    public function getSpecification(): array
    {
        return [
            'enabled' => $this->getRequiredBoolean(),
            'opened' => $this->getRequiredBoolean(),
            'column_ra' => $this->getPositiveDigits(),
            'column_dec' => $this->getPositiveDigits(),
            'resolver_enabled' => $this->getRequiredBoolean(),
            'default_ra' => $this->getRa(false),
            'default_dec' => $this->getDec(false),
            'default_radius' => $this->getRadius(),
            'default_ra_dec_unit' => $this->getString(),
            'plot_enabled' => $this->getRequiredBoolean()
        ];
    }

    private function getRadius()
    {
        return [
            'required' => false,
            'filters' => [
                ['name' => 'ToFloat']
            ],
            'validators' => [
                [
                    'name' => 'Between',
                    'options' => [
                        'min' => 0,
                        'max' => 150
                    ]
                ]
            ]
        ];
    }
}
