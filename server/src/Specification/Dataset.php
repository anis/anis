<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification;

use App\Specification\Validator\NoRecordExists;
use App\Specification\Validator\RecordExists;

final class Dataset extends AbstractSpecification
{
    public function getSpecification(): array
    {
        return [
            'name' => $this->getName(),
            'table_ref' => $this->getString(),
            'primary_key' => $this->getPositiveDigits(),
            'default_order_by' => $this->getPositiveDigits(),
            'default_order_by_direction' => $this->getString(),
            'label' => $this->getString(),
            'description' => $this->getString(),
            'display' => $this->getPositiveDigits(),
            'data_path' => $this->getString(false),
            'public' => $this->getRequiredBoolean(),
            'download_json' => $this->getRequiredBoolean(),
            'download_csv' => $this->getRequiredBoolean(),
            'download_ascii' => $this->getRequiredBoolean(),
            'download_vo' => $this->getRequiredBoolean(),
            'download_fits' => $this->getRequiredBoolean(),
            'server_link_enabled' => $this->getRequiredBoolean(),
            'datatable_enabled' => $this->getRequiredBoolean(),
            'datatable_selectable_rows' => $this->getRequiredBoolean(),
            'id_database' => $this->getIdDatabase(),
            'id_dataset_family' => $this->getIdDatasetFamily()
        ];
    }

    public function getName(): array
    {
        $spec = $this->getString();
        if ($this->checkNoRecordExists) {
            $spec['validators'][] = [
                'name' => NoRecordExists::class,
                'options' => [
                    'em' => $this->em,
                    'entityClassName' => 'App\Entity\Dataset',
                    'pkeyType' => NoRecordExists::COMPOSITE_PKEY,
                    'columnName' => 'name',
                    'extraId' => [ 'instance' => $this->args['name'] ]
                ]
            ];
        }

        return $spec;
    }

    public function getIdDatabase(): array
    {
        $spec = $this->getPositiveDigits();
        if ($this->checkRecordExists) {
            $spec['validators'][] = [
                'name' => RecordExists::class,
                'options' => [
                    'em' => $this->em,
                    'entityClassName' => 'App\Entity\Database',
                    'pkeyType' => RecordExists::COMPOSITE_PKEY,
                    'columnName' => 'id',
                    'extraId' => [ 'instance' => $this->args['name'] ]
                ]
            ];
        }

        return $spec;
    }

    public function getIdDatasetFamily(): array
    {
        $spec = $this->getPositiveDigits();
        if ($this->checkRecordExists) {
            $spec['validators'][] = [
                'name' => RecordExists::class,
                'options' => [
                    'em' => $this->em,
                    'entityClassName' => 'App\Entity\DatasetFamily',
                    'pkeyType' => RecordExists::COMPOSITE_PKEY,
                    'columnName' => 'id',
                    'extraId' => [ 'instance' => $this->args['name'] ]
                ]
            ];
        }

        return $spec;
    }
}
