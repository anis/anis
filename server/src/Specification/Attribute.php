<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification;

use Laminas\InputFilter\Factory as LaminasInputFilterFactory;
use App\Specification\Validator\NoRecordExists;
use App\Specification\Validator\RecordExists;
use App\Specification\Validator\Collection;

final class Attribute extends AbstractSpecification
{
    public function getSpecification(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getString(),
            'label' => $this->getString(),
            'form_label' => $this->getString(),
            'datatable_label' => $this->getString(false),
            'description' => $this->getString(false),
            'type' => $this->getString(),
            'search_type' => $this->getString(false),
            'operator' => $this->getString(false),
            'dynamic_operator' => $this->getRequiredBoolean(),
            'null_operators_enabled' => $this->getRequiredBoolean(),
            'min' => $this->getString(false),
            'max' => $this->getString(false),
            'options' => $this->getOptions(),
            'placeholder_min' => $this->getString(false),
            'placeholder_max' => $this->getString(false),
            'criteria_display' => $this->getPositiveDigits(false),
            'output_display' => $this->getPositiveDigits(false),
            'selected' => $this->getRequiredBoolean(),
            'renderer' => $this->getString(false),
            'renderer_config' => $this->getRendererConfig(),
            'order_by' => $this->getRequiredBoolean(),
            'archive' => $this->getRequiredBoolean(),
            'detail_display' => $this->getPositiveDigits(false),
            'detail_renderer' => $this->getString(false),
            'detail_renderer_config' => $this->getRendererConfig(),
            'vo_utype' => $this->getString(false),
            'vo_ucd' => $this->getString(false),
            'vo_unit' => $this->getString(false),
            'vo_description' => $this->getString(false),
            'vo_datatype' => $this->getString(false),
            'vo_size' => $this->getString(false),
            'id_criteria_family' => $this->getIdCriteriaFamily(),
            'id_output_category' => $this->getIdOutputCategory(),
            'id_detail_output_category' => $this->getIdOutputCategory()
        ];
    }

    private function getId(): array
    {
        $spec = $this->getPositiveDigits();
        if ($this->checkNoRecordExists) {
            $spec['validators'][] = [
                'name' => NoRecordExists::class,
                'options' => [
                    'em' => $this->em,
                    'entityClassName' => 'App\Entity\Attribute',
                    'pkeyType' => NoRecordExists::CONCATENATE_PKEY,
                    'columnName' => 'id',
                    'extraId' => [ $this->args['name'], $this->args['dname'] ]
                ]
            ];
        }

        return $spec;
    }

    private function getOptions(): array
    {
        return [
            'required' => false,
            'validators' => [
                [ 'name' => 'IsCountable' ],
                [
                    'name' => Collection::class,
                    'options' => [
                        'iff' => new LaminasInputFilterFactory(),
                        'validator' => [
                            'label' => $this->getString(),
                            'value' => $this->getString(),
                            'display' => $this->getPositiveDigits()
                        ]
                    ]
                ]
            ]
        ];
    }

    private function getRendererConfig(): array
    {
        return [
            'required' => false,
            'validators' => [
                [ 'name' => 'IsCountable' ]
            ]
        ];
    }

    private function getIdCriteriaFamily(): array
    {
        $spec = $this->getPositiveDigits(false);
        if ($this->checkRecordExists) {
            $spec['validators'][] = [
                'name' => RecordExists::class,
                'options' => [
                    'em' => $this->em,
                    'entityClassName' => 'App\Entity\CriteriaFamily',
                    'pkeyType' => RecordExists::CONCATENATE_PKEY,
                    'columnName' => 'id',
                    'extraId' => [ $this->args['name'], $this->args['dname'] ]
                ]
            ];
        }

        return $spec;
    }

    private function getIdOutputCategory(): array
    {
        $spec = $this->getString(false);
        if ($this->checkRecordExists) {
            $spec['validators'][] = [
                'name' => RecordExists::class,
                'options' => [
                    'em' => $this->em,
                    'entityClassName' => 'App\Entity\OutputCategory',
                    'pkeyType' => RecordExists::CONCATENATE_PKEY,
                    'columnName' => 'id',
                    'extraId' => [ $this->args['name'], $this->args['dname'] ]
                ]
            ];
        }

        return $spec;
    }
}
