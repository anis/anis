<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification\Factory;

use Laminas\InputFilter\Factory as LaminasInputFilterFactory;
use Laminas\InputFilter\InputFilterInterface;
use Doctrine\ORM\EntityManagerInterface;

class InputFilterFactory implements IInputFilterFactory
{
    protected LaminasInputFilterFactory $inputFilterFactory;
    protected InputFilterInterface $inputFilter;

    public function __construct(LaminasInputFilterFactory $inputFilterFactory)
    {
        $this->inputFilterFactory = $inputFilterFactory;
    }

    public function isValid(
        string $specName,
        EntityManagerInterface $em,
        array $args,
        array $data,
        bool $checkNoRecordExists,
        bool $checkRecordExists
    ): bool {
        $className = ucfirst(lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $specName)))));
        $specClassName = '\App\Specification\\' . $className;
        if (!class_exists($specClassName)) {
            throw new InputFilterFactoryException('Specification class not found');
        }

        $this->inputFilter = $this->inputFilterFactory->createInputFilter(
            (new $specClassName($em, $args, $checkNoRecordExists, $checkRecordExists))->getSpecification()
        );
        $this->inputFilter->setData($data);
        return $this->inputFilter->isValid();
    }

    public function getInputFilter(): InputFilterInterface
    {
        return $this->inputFilter;
    }
}
