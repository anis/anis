<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification;

use Laminas\InputFilter\Factory as LaminasInputFilterFactory;
use App\Specification\Validator\Collection;

final class DesignConfig extends AbstractSpecification
{
    public function getSpecification(): array
    {
        return [
            'design_background_color' => $this->getHtmlColor(false),
            'design_text_color' => $this->getHtmlColor(),
            'design_font_family' => $this->getString(),
            'design_link_color' => $this->getHtmlColor(),
            'design_link_hover_color' => $this->getHtmlColor(),
            'design_logo' => $this->getString(false),
            'design_logo_href' => $this->getString(false),
            'design_favicon' => $this->getString(false),
            'navbar_background_color' => $this->getHtmlColor(),
            'navbar_border_bottom_color' => $this->getHtmlColor(),
            'navbar_color_href' => $this->getHtmlColor(),
            'navbar_font_family' => $this->getString(),
            'navbar_sign_in_btn_color' => $this->getHtmlColor(),
            'navbar_user_btn_color' => $this->getHtmlColor(),
            'footer_background_color' => $this->getHtmlColor(),
            'footer_border_top_color' => $this->getHtmlColor(),
            'footer_text_color' => $this->getHtmlColor(),
            'footer_logos' => $this->getFooterLogos(),
            'family_border_color' => $this->getHtmlColor(),
            'family_header_background_color' => $this->getHtmlColor(),
            'family_title_color' => $this->getHtmlColor(),
            'family_title_bold' => $this->getRequiredBoolean(),
            'family_background_color' => $this->getHtmlColor(),
            'family_text_color' => $this->getHtmlColor(),
            'progress_bar_title' => $this->getString(),
            'progress_bar_title_color' => $this->getHtmlColor(),
            'progress_bar_subtitle' => $this->getString(),
            'progress_bar_subtitle_color' => $this->getHtmlColor(),
            'progress_bar_step_dataset_title' => $this->getString(),
            'progress_bar_step_criteria_title' => $this->getString(),
            'progress_bar_step_output_title' => $this->getString(),
            'progress_bar_step_result_title' => $this->getString(),
            'progress_bar_color' => $this->getHtmlColor(),
            'progress_bar_active_color' => $this->getHtmlColor(),
            'progress_bar_circle_color' => $this->getHtmlColor(),
            'progress_bar_circle_icon_color' => $this->getHtmlColor(),
            'progress_bar_circle_icon_active_color' => $this->getHtmlColor(),
            'progress_bar_text_color' => $this->getHtmlColor(),
            'progress_bar_text_bold' => $this->getRequiredBoolean(),
            'search_next_btn_color' => $this->getHtmlColor(),
            'search_next_btn_hover_color' => $this->getHtmlColor(),
            'search_next_btn_hover_text_color' => $this->getHtmlColor(),
            'search_back_btn_color' => $this->getHtmlColor(),
            'search_back_btn_hover_color' => $this->getHtmlColor(),
            'search_back_btn_hover_text_color' => $this->getHtmlColor(),
            'delete_current_query_btn_enabled' => $this->getRequiredBoolean(),
            'delete_current_query_btn_text' => $this->getString(),
            'delete_current_query_btn_color' => $this->getHtmlColor(),
            'delete_current_query_btn_hover_color' => $this->getHtmlColor(),
            'delete_current_query_btn_hover_text_color' => $this->getHtmlColor(),
            'delete_criterion_cross_color' => $this->getHtmlColor(),
            'search_info_background_color' => $this->getHtmlColor(),
            'search_info_text_color' => $this->getHtmlColor(),
            'search_info_help_enabled' => $this->getRequiredBoolean(),
            'dataset_select_btn_color' => $this->getHtmlColor(),
            'dataset_select_btn_hover_color' => $this->getHtmlColor(),
            'dataset_select_btn_hover_text_color' => $this->getHtmlColor(),
            'dataset_selected_icon_color' => $this->getHtmlColor(),
            'search_criterion_background_color' => $this->getHtmlColor(),
            'search_criterion_text_color' => $this->getHtmlColor(),
            'output_columns_selected_color' => $this->getHtmlColor(),
            'output_columns_select_all_btn_color' => $this->getHtmlColor(),
            'output_columns_select_all_btn_hover_color' => $this->getHtmlColor(),
            'output_columns_select_all_btn_hover_text_color' => $this->getHtmlColor(),
            'result_panel_border_size' => $this->getString(),
            'result_panel_border_color' => $this->getHtmlColor(),
            'result_panel_title_color' => $this->getHtmlColor(),
            'result_panel_background_color' => $this->getHtmlColor(),
            'result_panel_text_color' => $this->getHtmlColor(),
            'result_download_btn_color' => $this->getHtmlColor(),
            'result_download_btn_hover_color' => $this->getHtmlColor(),
            'result_download_btn_text_color' => $this->getHtmlColor(),
            'result_datatable_actions_btn_color' => $this->getHtmlColor(),
            'result_datatable_actions_btn_hover_color' => $this->getHtmlColor(),
            'result_datatable_actions_btn_text_color' => $this->getHtmlColor(),
            'result_datatable_bordered' => $this->getRequiredBoolean(),
            'result_datatable_bordered_radius' => $this->getRequiredBoolean(),
            'result_datatable_border_color' => $this->getHtmlColor(),
            'result_datatable_header_background_color' => $this->getHtmlColor(),
            'result_datatable_header_text_color' => $this->getHtmlColor(),
            'result_datatable_rows_background_color' => $this->getHtmlColor(),
            'result_datatable_rows_text_color' => $this->getHtmlColor(),
            'result_datatable_sorted_color' => $this->getHtmlColor(),
            'result_datatable_sorted_active_color' => $this->getHtmlColor(),
            'result_datatable_link_color' => $this->getHtmlColor(),
            'result_datatable_link_hover_color' => $this->getHtmlColor(),
            'result_datatable_rows_selected_color' => $this->getHtmlColor(),
            'result_datatable_pagination_link_color' => $this->getHtmlColor(),
            'result_datatable_pagination_active_bck_color' => $this->getHtmlColor(),
            'result_datatable_pagination_active_text_color' => $this->getHtmlColor(),
            'samp_enabled' => $this->getRequiredBoolean(),
            'back_to_portal' => $this->getRequiredBoolean(),
            'user_menu_enabled' => $this->getRequiredBoolean(),
            'search_multiple_all_datasets_selected' => $this->getRequiredBoolean(),
            'search_multiple_progress_bar_title' => $this->getString(),
            'search_multiple_progress_bar_subtitle' => $this->getString(),
            'search_multiple_progress_bar_step_position' => $this->getString(),
            'search_multiple_progress_bar_step_datasets' => $this->getString(),
            'search_multiple_progress_bar_step_result' => $this->getString()
        ];
    }

    private function getFooterLogos(): array
    {
        return [
            'required' => false,
            'validators' => [
                [ 'name' => 'IsCountable' ],
                [
                    'name' => Collection::class,
                    'options' => [
                        'iff' => new LaminasInputFilterFactory(),
                        'validator' => [
                            'href' => $this->getString(),
                            'title' => $this->getString(),
                            'file' => $this->getString(),
                            'display' => $this->getPositiveDigits()
                        ]
                    ]
                ]
            ]
        ];
    }
}
