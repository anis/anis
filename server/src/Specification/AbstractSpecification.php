<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification;

use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractSpecification implements ISpecification
{
    protected EntityManagerInterface $em;
    protected array $args;
    protected bool $checkNoRecordExists;
    protected bool $checkRecordExists;

    public function __construct(
        EntityManagerInterface $em,
        array $args,
        bool $checkNoRecordExists,
        bool $checkRecordExists
    ) {
        $this->em = $em;
        $this->args = $args;
        $this->checkNoRecordExists = $checkNoRecordExists;
        $this->checkRecordExists = $checkRecordExists;
    }

    protected function getString(bool $required = true): array
    {
        return [
            'required' => $required,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [
                    'name' => 'StringLength',
                    'options' => [
                        'min' => 1,
                        'max' => 255
                    ]
                ]
            ]
        ];
    }

    protected function getPositiveDigits(bool $required = true)
    {
        return [
            'required' => $required,
            'validators' => [
                ['name' => 'Digits'],
                [
                    'name' => 'GreaterThan',
                    'options' => [
                        'min' => 0
                    ]
                ]
            ]
        ];
    }

    protected function getRequiredBoolean(): array
    {
        return [
            'required' => true,
            'allow_empty' => true,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StringToLower'],
                [
                    'name' => 'Boolean',
                    'options' => [
                        'casting' => false,
                        'type' => [
                            'all',
                        ],
                        'translations' => [
                            'yes' => true,
                            'no' => false,
                            'on' => true,
                            'off' => false,
                        ]
                    ]
                ]
            ],
            'validators' => [
                [
                    'name' => 'Callback',
                    'options' => [
                        'callback' => function ($v) {
                            return is_bool($v);
                        },
                        'messages' => [
                            'callbackValue' => 'Input must be a boolean value'
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function getHtmlColor(bool $required = true): array
    {
        return [
            'required' => $required,
            'filters' => [
                ['name' => 'StringTrim'],
                ['name' => 'StripTags'],
            ],
            'validators' => [
                [
                    'name' => 'Regex',
                    'options' => [
                        'pattern' => '/^#(?:[0-9a-fA-F]{3}){1,2}$/',
                        'messages' => [
                            'regexNotMatch' => 'Input must be a html color (example: #FFF or #FFFFFF)'
                        ]
                    ]
                ]
            ]
        ];
    }

    protected function getRa(bool $required = true)
    {
        return [
            'required' => $required,
            'filters' => [
                ['name' => 'ToFloat']
            ],
            'validators' => [
                [
                    'name' => 'Between',
                    'options' => [
                        'min' => 0,
                        'max' => 360
                    ]
                ]
            ]
        ];
    }

    protected function getDec(bool $required = true)
    {
        return [
            'required' => $required,
            'filters' => [
                ['name' => 'ToFloat']
            ],
            'validators' => [
                [
                    'name' => 'Between',
                    'options' => [
                        'min' => -90,
                        'max' => 90
                    ]
                ]
            ]
        ];
    }
}
