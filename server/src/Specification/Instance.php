<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification;

use App\Specification\Validator\NoRecordExists;

final class Instance extends AbstractSpecification
{
    public function getSpecification(): array
    {
        return [
            'name' => $this->getName(),
            'label' => $this->getString(),
            'description' => $this->getString(),
            'scientific_manager' => $this->getString(),
            'instrument' => $this->getString(),
            'wavelength_domain' => $this->getString(),
            'display' => $this->getPositiveDigits(),
            'data_path' => $this->getString(false),
            'files_path' => $this->getString(false),
            'public' => $this->getRequiredBoolean(),
            'portal_logo' => $this->getString(false),
            'portal_color' => $this->getHtmlColor(false),
            'default_redirect' => $this->getString(false)
        ];
    }

    private function getName(): array
    {
        $spec = $this->getString();
        if ($this->checkNoRecordExists) {
            $spec['validators'][] = [
                'name' => NoRecordExists::class,
                'options' => [
                    'em' => $this->em,
                    'entityClassName' => 'App\Entity\Instance',
                    'pkeyType' => NoRecordExists::SIMPLE_PKEY,
                    'columnName' => 'name'
                ]
            ];
        }

        return $spec;
    }
}
