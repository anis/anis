<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification;

use App\Specification\Validator\NoRecordExists;
use App\Specification\Validator\RecordExists;

abstract class MenuItem extends AbstractSpecification
{
    public function getSpecification(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getString(),
            'icon' => $this->getString(false),
            'display' => $this->getPositiveDigits(),
            'id_menu_family' => $this->getIdMenuFamily()
        ];
    }

    public function getId(): array
    {
        if (array_key_exists('fid', $this->args)) {
            $extraId = [ $this->args['name'], $this->args['fid'] ];
        } else {
            $extraId = [ $this->args['name'] ];
        }

        $spec = $this->getPositiveDigits();
        if ($this->checkNoRecordExists) {
            $spec['validators'][] = [
                'name' => NoRecordExists::class,
                'options' => [
                    'em' => $this->em,
                    'entityClassName' => 'App\Entity\MenuItem',
                    'pkeyType' => NoRecordExists::CONCATENATE_PKEY,
                    'columnName' => 'id',
                    'extraId' => $extraId
                ]
            ];
        }

        return $spec;
    }

    private function getIdMenuFamily(): array
    {
        $spec = $this->getPositiveDigits(false);
        if ($this->checkRecordExists) {
            $spec['validators'][] = [
                'name' => RecordExists::class,
                'options' => [
                    'em' => $this->em,
                    'entityClassName' => 'App\Entity\MenuFamily',
                    'pkeyType' => RecordExists::CONCATENATE_PKEY,
                    'columnName' => 'id',
                    'extraId' => [ $this->args['name'] ]
                ]
            ];
        }

        return $spec;
    }
}
