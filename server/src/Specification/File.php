<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification;

use App\Specification\Validator\NoRecordExists;

final class File extends AbstractSpecification
{
    public function getSpecification(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getString(),
            'file_path' => $this->getString(),
            'file_size' => $this->getPositiveDigits(),
            'type' => $this->getString()
        ];
    }

    public function getId(): array
    {
        $spec = $this->getPositiveDigits();
        if ($this->checkNoRecordExists) {
            $spec['validators'][] = [
                'name' => NoRecordExists::class,
                'options' => [
                    'em' => $this->em,
                    'entityClassName' => 'App\Entity\File',
                    'pkeyType' => NoRecordExists::CONCATENATE_PKEY,
                    'columnName' => 'id',
                    'extraId' => [ $this->args['name'], $this->args['dname'] ]
                ]
            ];
        }

        return $spec;
    }
}
