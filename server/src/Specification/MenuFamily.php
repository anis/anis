<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification;

final class MenuFamily extends MenuItem
{
    public function getSpecification(): array
    {
        return array_merge(parent::getSpecification(), [
            'name' => $this->getString()
        ]);
    }
}
