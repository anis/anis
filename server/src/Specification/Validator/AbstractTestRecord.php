<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification\Validator;

use Laminas\Validator\AbstractValidator;
use Laminas\Validator\Exception\InvalidArgumentException;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractTestRecord extends AbstractValidator
{
    public const SIMPLE_PKEY = 'simplePkey';
    public const COMPOSITE_PKEY = 'compositePkey';
    public const CONCATENATE_PKEY = 'concatenatePkey';

    public const ERROR_RECORD_FOUND = 'recordFound';
    public const ERROR_RECORD_NOT_FOUND = 'recordNotFound';

    protected $messageTemplates = [
        self::ERROR_RECORD_FOUND => 'A record matching the input was found',
        self::ERROR_RECORD_NOT_FOUND => 'A record matching the input was not found'
    ];

    protected EntityManagerInterface $em;
    protected string $entityClassName;
    protected string $pkeyType;
    protected string $columnName;
    protected ?array $extraId = null;

    public function __construct(array $options = null)
    {
        parent::__construct($options);

        if (!array_key_exists('em', $options)) {
            throw new InvalidArgumentException('Entity manager option (em) missing!');
        }

        if (!($options['em'] instanceof EntityManagerInterface)) {
            throw new InvalidArgumentException('Option em must be of type Doctrine\ORM\EntityManagerInterface');
        }

        if (!array_key_exists('entityClassName', $options)) {
            throw new InvalidArgumentException('Entity class name option (entityClassName) missing!');
        }

        if (!class_exists($options['entityClassName'])) {
            throw new InvalidArgumentException('Option entityClassName must be the name of a valid class!');
        }

        if (!array_key_exists('pkeyType', $options)) {
            throw new InvalidArgumentException('Primary key type (pkeyType) missing!');
        }

        if (!array_key_exists('columnName', $options)) {
            throw new InvalidArgumentException('Column name option (columnName) missing!');
        }

        if ($options['pkeyType'] !== self::SIMPLE_PKEY && !array_key_exists('extraId', $options)) {
            throw new InvalidArgumentException('Extra id option (extraId) missing!');
        }

        $this->em = $options['em'];
        $this->entityClassName = $options['entityClassName'];
        $this->pkeyType = $options['pkeyType'];
        $this->columnName = $options['columnName'];

        if (array_key_exists('extraId', $options)) {
            $this->extraId = $options['extraId'];
        }
    }

    protected function getObject($value): ?object
    {
        switch ($this->pkeyType) {
            case self::SIMPLE_PKEY:
                $id = [$this->columnName => $value];
                break;
            case self::COMPOSITE_PKEY:
                $id = [
                    $this->columnName => $value,
                    ...$this->extraId
                ];
                break;
            case self::CONCATENATE_PKEY:
                $id = [$this->columnName => implode('_', [...$this->extraId, $value])];
                break;
            default:
                throw new InvalidArgumentException('Primary key type must be simple, composite or concatenate!');
        }

        return $this->em->find($this->entityClassName, $id);
    }
}
