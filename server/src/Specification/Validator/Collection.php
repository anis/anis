<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification\Validator;

use Laminas\Validator\AbstractValidator;
use Laminas\InputFilter\Factory as LaminasInputFilterFactory;
use Laminas\Validator\Exception\InvalidArgumentException;

class Collection extends AbstractValidator
{
    protected LaminasInputFilterFactory $iff;
    protected array $messages;

    protected $options = [
        'validator' => null
    ];

    public function __construct(array $options = null)
    {
        parent::__construct($options);

        if (!array_key_exists('iff', $options)) {
            throw new InvalidArgumentException('Input filter factory option (iff) missing!');
        }

        if (!($options['iff'] instanceof LaminasInputFilterFactory)) {
            throw new InvalidArgumentException('Option em must be of type Laminas\InputFilter\Factory');
        }

        $this->iff = $options['iff'];
    }

    public function isValid($array)
    {
        $this->messages = [];
        $isvalid = true;
        foreach ($array as $index => $item) {
            $inputFilter = $this->iff->createInputFilter($this->getOption('validator'));
            $inputFilter->setData($item);
            $isvalid = $isvalid && $inputFilter->isValid($item);
            foreach ($inputFilter->getMessages() as $field => $errors) {
                foreach ($errors as $key => $string) {
                    $this->messages[$index][$field][$key] = $string;
                }
            }
        }
        return $isvalid;
    }

    public function getMessages()
    {
        return $this->messages;
    }
}
