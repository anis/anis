<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification\Validator;

final class RecordExists extends AbstractTestRecord
{
    public function isValid($value): bool
    {
        $valid = true;
        $this->setValue($value);

        if (!$this->getObject($value)) {
            $valid = false;
            $this->error(self::ERROR_RECORD_NOT_FOUND);
        }

        return $valid;
    }
}
