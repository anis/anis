<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Specification;

final class AliasConfig extends AbstractSpecification
{
    public function getSpecification(): array
    {
        return [
            'table_alias' => $this->getString(),
            'column_alias' => $this->getString(),
            'column_name' => $this->getString(),
            'column_alias_long' => $this->getString()
        ];
    }
}
