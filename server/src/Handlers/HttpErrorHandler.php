<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Handlers;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpException;
use Slim\Exception\HttpForbiddenException;
use Slim\Exception\HttpMethodNotAllowedException;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpNotImplementedException;
use Slim\Exception\HttpUnauthorizedException;
use Slim\Handlers\ErrorHandler as SlimErrorHandler;
use App\Error\ActionError;
use App\Error\FormActionError;
use App\Response\ResponsePayload;
use App\Exception\InvalidFormException;
use Throwable;

class HttpErrorHandler extends SlimErrorHandler
{
    /**
     * @inheritdoc
     */
    protected function respond(): Response
    {
        $exception = $this->exception;

        if ($exception instanceof InvalidFormException) {
            $statusCode = $exception->getCode();
            $error = (new FormActionError(
                ActionError::BAD_REQUEST,
                $exception->getMessage()
            ))->setErrors($exception->getErrors());
        } else {
            $statusCode = 500;
            $error = new ActionError(
                ActionError::SERVER_ERROR,
                'An internal error has occurred while processing your request.'
            );

            if ($exception instanceof HttpException) {
                $statusCode = $exception->getCode();
                $error->setDescription($exception->getMessage());

                if ($exception instanceof HttpNotFoundException) {
                    $error->setType(ActionError::RESOURCE_NOT_FOUND);
                } elseif ($exception instanceof HttpMethodNotAllowedException) {
                    $error->setType(ActionError::NOT_ALLOWED);
                } elseif ($exception instanceof HttpUnauthorizedException) {
                    $error->setType(ActionError::UNAUTHENTICATED);
                } elseif ($exception instanceof HttpForbiddenException) {
                    $error->setType(ActionError::INSUFFICIENT_PRIVILEGES);
                } elseif ($exception instanceof HttpBadRequestException) {
                    $error->setType(ActionError::BAD_REQUEST);
                } elseif ($exception instanceof HttpNotImplementedException) {
                    $error->setType(ActionError::NOT_IMPLEMENTED);
                }
            }

            if (
                !($exception instanceof HttpException)
                && $exception instanceof Throwable
                && $this->displayErrorDetails
            ) {
                $error->setDescription($exception->getMessage());
            }
        }

        $payload = new ResponsePayload($statusCode, null, $error);
        $json = json_encode($payload);

        $response = $this->responseFactory->createResponse($statusCode);
        $response->getBody()->write($json);

        return $response;
    }
}
