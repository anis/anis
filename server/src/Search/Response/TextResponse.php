<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Response;

use Psr\Http\Message\ResponseInterface;
use Doctrine\DBAL\Result;
use App\Search\Query\AnisQueryBuilder;

/**
 * Class to build the text search response (csv, ascii)
 */
class TextResponse implements IResponse
{
    private string $delimiter;
    private string $contentType;

    /**
     * @param string $delimiter   The string delimiter between each column
     * @param string $contentType The Mime Type of the result
     */
    public function __construct(string $delimiter, string $contentType)
    {
        $this->delimiter = $delimiter;
        $this->contentType = $contentType;
    }

    /**
     * @return ResponseInterface
     * @param  ResponseInterface $response PSR-7   This object represents the HTTP response
     * @param  AnisQueryBuilder  $anisQueryBuilder Object used to wrap the Doctrine DBAL Query Builder
     */
    public function getResponse(ResponseInterface $response, AnisQueryBuilder $anisQueryBuilder): ResponseInterface
    {
        $stmt = $anisQueryBuilder->getDoctrineQueryBuilder()->executeQuery();
        $attributes = $anisQueryBuilder->getAttributesSelected();
        $payload = $this->transformArrayToCsv($stmt, $attributes);
        $response->getBody()->write($payload);
        return $response->withHeader('Content-Type', $this->contentType);
    }

    /**
     * Transform each array row to text format
     *
     * @return string
     * @param Result      $stmt       The doctrine statement of the query request
     * @param Attribute[] $attributes The selected attributes for the request
     */
    private function transformArrayToCsv(Result $stmt, array $attributes): string
    {
        $attributesLabel = array_map(function ($attribute) {
            return $attribute->getLabel();
        }, $attributes);
        $csv = implode($this->delimiter, $attributesLabel) . PHP_EOL;
        while ($row = $stmt->fetchAssociative()) {
            $csv .= implode($this->delimiter, $row) . PHP_EOL;
        }
        return $csv;
    }
}
