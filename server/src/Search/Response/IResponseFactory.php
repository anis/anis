<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Response;

/**
 * Interface that represents the factory of search responses
 */
interface IResponseFactory
{
    /**
     * Method that allows the creation of a response according to a format (example: json, csv, ascii, votable)
     *
     * @return IResponse
     * @param string $format json, csv, ascii, votable
     */
    public function create(string $format): IResponse;
}
