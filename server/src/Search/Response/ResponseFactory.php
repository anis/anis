<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Response;

/**
 * Interface that represents the factory of search responses
 */
class ResponseFactory implements IResponseFactory
{
    /**
     * Method that allows the creation of a response according to a format (example: json, csv, ascii, votable)
     *
     * @return IResponse
     * @param string $format json, csv, ascii, votable
     */
    public function create(string $format): IResponse
    {
        switch ($format) {
            case 'json':
                $searchResponse = new JsonResponse();
                break;
            case 'csv':
                $searchResponse = new TextResponse(',', 'text/csv');
                break;
            case 'ascii':
                $searchResponse = new TextResponse(' ', 'text/plain');
                break;
            case 'votable':
                $searchResponse = new VotableResponse();
                break;
            default:
                throw SearchResponseException::typeOfFormatDoesNotExist();
        }
        return $searchResponse;
    }
}
