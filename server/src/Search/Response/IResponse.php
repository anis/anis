<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Response;

use Psr\Http\Message\ResponseInterface;
use App\Search\Query\AnisQueryBuilder;

/**
 * Interface implemented by search response formatting classes
 */
interface IResponse
{
    public function getResponse(ResponseInterface $response, AnisQueryBuilder $anisQueryBuilder): ResponseInterface;
}
