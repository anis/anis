<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Response;

use Psr\Http\Message\ResponseInterface;
use Doctrine\DBAL\Result;
use App\Search\Query\AnisQueryBuilder;

/**
 * Class to build the json search response
 */
class JsonResponse implements IResponse
{
    /**
     * @return ResponseInterface
     * @param  ResponseInterface $response PSR-7   This object represents the HTTP response
     * @param  AnisQueryBuilder  $anisQueryBuilder Object used to wrap the Doctrine DBAL Query Builder
     */
    public function getResponse(ResponseInterface $response, AnisQueryBuilder $anisQueryBuilder): ResponseInterface
    {
        $stmt = $anisQueryBuilder->getDoctrineQueryBuilder()->executeQuery();
        $attributes = $anisQueryBuilder->getAttributesSelected();
        $payload = json_encode($this->processesTypes($stmt, $attributes), JSON_UNESCAPED_SLASHES);
        $response->getBody()->write($payload);
        return $response;
    }

    /**
     * Process types like float
     * Decode each nsted json result and returns array results
     *
     * @return array
     * @param Result      $stmt       The doctrine statement of the query request
     * @param Attribute[] $attributes The selected attributes for the request
     */
    private function processesTypes(Result $stmt, array $attributes): array
    {
        $rows = [];
        while ($row = $stmt->fetchAssociative()) {
            foreach ($attributes as $attribute) {
                $value = $row[$attribute->getLabel()];
                if ($attribute->getType() === 'json' && !is_null($value)) {
                    $row[$attribute->getLabel()] = json_decode($value, true);
                }
                if ($attribute->getType() === 'float' && !is_null($value)) {
                    $row[$attribute->getLabel()] = floatval($value);
                }
                if ($attribute->getType() === 'float' && $value === 'NaN') {
                    $row[$attribute->getLabel()] = null;
                }
            }
            $rows[] = $row;
        }
        return $rows;
    }
}
