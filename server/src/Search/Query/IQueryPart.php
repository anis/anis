<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query;

use App\Entity\Dataset;

/**
 * Interface used by Anis Query Part (select, from, where, order...)
 */
interface IQueryPart
{
    /**
     * Add a part for the request (select, from, where, order...)
     *
     * @param AnisQueryBuilder $anisQueryBuilder Represents the query being built
     * @param Dataset          $dataset          Represents the requested dataset
     * @param string[]         $queryParams      The query params of the url (after ?)
     */
    public function __invoke(AnisQueryBuilder $anisQueryBuilder, Dataset $dataset, array $queryParams): void;
}
