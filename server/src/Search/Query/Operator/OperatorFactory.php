<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query\Operator;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;

/**
 * Represents the operators factory.
 * Main program calls the factory with a good type (eq, bw, in...) and factory
 * returns the operator representation
 */
class OperatorFactory implements IOperatorFactory
{
    /**
     * Make the right operator and send it back
     * Parameters represent values ​​sent by the user
     *
     * @param string            $type
     * @param ExpressionBuilder $expr
     * @param string            $column
     * @param string            $columnType
     * @param string[]          $parameters
     *
     * @throws OperatorException Could not create operator
     * @return IOperator
     */
    public function create(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): IOperator {
        switch ($type) {
            case 'bw':
                $operator = $this->createBetween($type, $expr, $column, $columnType, $parameters);
                break;

            case 'eq':
                $operator = $this->createEqual($type, $expr, $column, $columnType, $parameters);
                break;

            case 'neq':
                $operator = $this->createNotEqual($type, $expr, $column, $columnType, $parameters);
                break;

            case 'gt':
                $operator = $this->createGreaterThan($type, $expr, $column, $columnType, $parameters);
                break;

            case 'gte':
                $operator = $this->createGreaterThanEqual($type, $expr, $column, $columnType, $parameters);
                break;

            case 'lt':
                $operator = $this->createLessThan($type, $expr, $column, $columnType, $parameters);
                break;

            case 'lte':
                $operator = $this->createLessThanEqual($type, $expr, $column, $columnType, $parameters);
                break;

            case 'lk':
                $operator = $this->createLike($type, $expr, $column, $columnType, $parameters);
                break;

            case 'nlk':
                $operator = $this->createNotLike($type, $expr, $column, $columnType, $parameters);
                break;

            case 'in':
                $operator = $this->createIn($type, $expr, $column, $columnType, $parameters);
                break;

            case 'nin':
                $operator = $this->createNotIn($type, $expr, $column, $columnType, $parameters);
                break;

            case 'nl':
                $operator = $this->createOperatorNull($type, $expr, $column, $columnType, $parameters);
                break;

            case 'nnl':
                $operator = $this->createOperatorNotNull($type, $expr, $column, $columnType, $parameters);
                break;

            case 'js':
                $operator = $this->createJsonPostgres($type, $expr, $column, $columnType, $parameters);
                break;

            default:
                throw OperatorException::unknownOperator($type);
        }
        return $operator;
    }

    private function createBetween(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): Between {
        if (count($parameters) != 2) {
            throw OperatorException::operatorBadNumberOfParameters($type, 2);
        }
        return new Between($expr, $column, $columnType, $parameters[0], $parameters[1]);
    }

    private function createEqual(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): Equal {
        if (count($parameters) != 1) {
            throw OperatorException::operatorBadNumberOfParameters($type, 1);
        }
        return new Equal($expr, $column, $columnType, $parameters[0]);
    }

    private function createNotEqual(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): NotEqual {
        if (count($parameters) != 1) {
            throw OperatorException::operatorBadNumberOfParameters($type, 1);
        }
        return new NotEqual($expr, $column, $columnType, $parameters[0]);
    }

    private function createGreaterThan(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): GreaterThan {
        if (count($parameters) != 1) {
            throw OperatorException::operatorBadNumberOfParameters($type, 1);
        }
        return new GreaterThan($expr, $column, $columnType, $parameters[0]);
    }

    private function createGreaterThanEqual(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): GreaterThanEqual {
        if (count($parameters) != 1) {
            throw OperatorException::operatorBadNumberOfParameters($type, 1);
        }
        return new GreaterThanEqual($expr, $column, $columnType, $parameters[0]);
    }

    private function createLessThan(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): LessThan {
        if (count($parameters) != 1) {
            throw OperatorException::operatorBadNumberOfParameters($type, 1);
        }
        return new LessThan($expr, $column, $columnType, $parameters[0]);
    }

    private function createLessThanEqual(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): LessThanEqual {
        if (count($parameters) != 1) {
            throw OperatorException::operatorBadNumberOfParameters($type, 1);
        }
        return new LessThanEqual($expr, $column, $columnType, $parameters[0]);
    }

    private function createLike(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): Like {
        if (count($parameters) != 1) {
            throw OperatorException::operatorBadNumberOfParameters($type, 1);
        }
        return new Like($expr, $column, $columnType, $parameters[0]);
    }

    private function createNotLike(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): NotLike {
        if (count($parameters) != 1) {
            throw OperatorException::operatorBadNumberOfParameters($type, 1);
        }
        return new NotLike($expr, $column, $columnType, $parameters[0]);
    }

    private function createIn(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): In {
        if (count($parameters) < 1) {
            throw OperatorException::inBadNumberOfParameters($type);
        }
        return new In($expr, $column, $columnType, $parameters);
    }

    private function createNotIn(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): NotIn {
        if (count($parameters) < 1) {
            throw OperatorException::inBadNumberOfParameters($type);
        }
        return new NotIn($expr, $column, $columnType, $parameters);
    }

    private function createOperatorNull(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): OperatorNull {
        if (count($parameters) != 0) {
            throw OperatorException::operatorBadNumberOfParameters($type, 0);
        }
        return new OperatorNull($expr, $column, $columnType);
    }

    private function createOperatorNotNull(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): OperatorNotNull {
        if (count($parameters) != 0) {
            throw OperatorException::operatorBadNumberOfParameters($type, 0);
        }
        return new OperatorNotNull($expr, $column, $columnType);
    }

    private function createJsonPostgres(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): JsonPostgres {
        if ($columnType !== 'json') {
            throw OperatorException::operatorJsonNeedColumnTypeJson();
        }
        if (count($parameters) != 3) {
            throw OperatorException::operatorBadNumberOfParameters($type, 3);
        }
        return new JsonPostgres($expr, $column, $columnType, $parameters[0], $parameters[1], $parameters[2]);
    }
}
