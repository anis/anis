<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query\Operator;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;

/**
 * Operator that represents a sql in of a where clause
 */
class In extends Operator
{
    /**
     * Values of this criterion
     *
     * @var string[]
     */
    private array $values;

    /**
     * Create the class before call getExpression method to execute this operator
     *
     * @param ExpressionBuilder $expr
     * @param string            $column
     * @param string            $columnType
     * @param string[]          $values
     */
    public function __construct(ExpressionBuilder $expr, string $column, string $columnType, array $values)
    {
        parent::__construct($expr, $column, $columnType);
        foreach ($values as $value) {
            $this->verifyTypeCompatibility($value);
        }
        $this->values = $values;
    }

    /**
     * This method returns the in expression for this criterion
     *
     * @return string
     */
    public function getExpression(): string
    {
        return $this->expr->in($this->column, array_map([$this, 'getSqlValue'], $this->values));
    }
}
