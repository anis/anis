<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query\Operator;

/**
 * Operator that represents a null of a where clause
 */
class OperatorNull extends Operator
{
    /**
     * This method returns the null expression for this criterion
     *
     * @return string
     */
    public function getExpression(): string
    {
        return $this->expr->isNull($this->column);
    }
}
