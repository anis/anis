<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query\Operator;

/**
 * Interface that represents an operator.
 * An operator represents a where clause for a query.
 * Example: column1 = value
 */
interface IOperator
{
    /**
     * Method that returns the operator in sql format
     *
     * @return string
     */
    public function getExpression(): string;
}
