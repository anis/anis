<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query\Operator;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;

/**
 * Operator that represents a json postgres where clause
 */
class JsonPostgres extends Operator
{
    /**
     * Path inside the json to the criterion
     */
    private array $path;

    /**
     * Operator fot this json postgres criterion
     */
    private string $operator;

    /**
     * Value of this criterion
     */
    private string $value;

    /**
     * Create the class before call getExpression method to execute this operator
     *
     * @param ExpressionBuilder $expr
     * @param string            $column
     * @param string            $columnType
     * @param string            $path
     * @param string            $operator
     * @param string            $value
     */
    public function __construct(
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        string $path,
        string $operator,
        string $value
    ) {
        parent::__construct($expr, $column, $columnType);
        $this->path = explode(',', $path);
        $this->operator = $operator;
        $this->value = $value;
    }

    /**
     * This method returns the json postgres expression for this criterion
     *
     * @return string
     */
    public function getExpression(): string
    {
        switch ($this->operator) {
            case 'eq':
                $expr = $this->expr->eq($this->getColumn(), $this->getValue());
                break;
            case 'neq':
                $expr = $this->expr->neq($this->getColumn(), $this->getValue());
                break;
            case 'gt':
                $expr = $this->expr->gt($this->getColumn(), $this->getValue());
                break;
            case 'gte':
                $expr = $this->expr->gte($this->getColumn(), $this->getValue());
                break;
            case 'lt':
                $expr = $this->expr->lt($this->getColumn(), $this->getValue());
                break;
            case 'lte':
                $expr = $this->expr->lte($this->getColumn(), $this->getValue());
                break;
            default:
                throw OperatorException::jsonPostgresOperatorNotDefined($this->operator);
        }
        return $expr;
    }

    private function getColumn(): string
    {
        $column = $this->column;
        foreach ($this->path as $value) {
            if ($value === end($this->path)) {
                $column .= '->>' . $this->getSqlValue($value);
            } else {
                $column .= '->' . $this->getSqlValue($value);
            }
        }

        return $column;
    }

    private function getValue(): string
    {
        return $this->getSqlValue($this->value);
    }
}
