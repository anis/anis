<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query\Operator;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;

/**
 * Interface that represents the factory of operators
 */
interface IOperatorFactory
{
    /**
     * Method that allows the creation of an operator according to a type (example: eq, bw, in...)
     *
     * @param string            $type
     * @param ExpressionBuilder $expr
     * @param string            $column
     * @param string            $columnType
     * @param string[]          $parameters
     *
     * @return IOperator
     */
    public function create(
        string $type,
        ExpressionBuilder $expr,
        string $column,
        string $columnType,
        array $parameters
    ): IOperator;
}
