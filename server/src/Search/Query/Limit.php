<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search\Query;

use App\Entity\Dataset;

/**
 * Represents the Anis Limit Query Part
 */
class Limit extends AbstractQueryPart
{
    /**
     * Add limit clause to the request
     *
     * @param AnisQueryBuilder $anisQueryBuilder Represents the query being built
     * @param Dataset          $dataset          Represents the requested dataset
     * @param string[]         $queryParams      The query params of the url (after ?)
     */
    public function __invoke(AnisQueryBuilder $anisQueryBuilder, Dataset $dataset, array $queryParams): void
    {
        if (array_key_exists('p', $queryParams)) {
            $p = explode(':', $queryParams['p']);
            if (count($p) != 2) {
                throw SearchQueryException::badNumberOfParamsForLimit();
            }
            $limit = $p[0];
            $offset = ($p[1] - 1) * $limit;
            $anisQueryBuilder->getDoctrineQueryBuilder()
                ->setFirstResult($offset)
                ->setMaxResults($limit);
        }
    }
}
