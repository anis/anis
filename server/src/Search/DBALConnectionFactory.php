<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Search;

use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Tools\DsnParser;
use App\Entity\Database;

/**
 * Factory used to create a Doctrine DBAL connection to a business database
 */
class DBALConnectionFactory
{
    /**
     * This method create a connection to a business database.
     * It retrieves connection information from the Database object.
     *
     * Database object stores information about a business database in
     * the metamodel
     *
     * @return Connection
     * @param Database $database This object contains the database connection parameters
     */
    public function create(Database $database): Connection
    {
        if ($database->getDsnFile()) {
            $connectionParams = $this->getConnecionParamsByDsn($database->getDsnFile());
        } else {
            $connectionParams = $this->getConnecionParams($database);
        }

        $config = new \Doctrine\DBAL\Configuration();
        $connection = DriverManager::getConnection($connectionParams, $config);

        // Permet de faire fonctionner Doctrine avec les fonctions de pgsphere
        $connection->getDatabasePlatform()->registerDoctrineTypeMapping('spoly', 'string');
        $connection->getDatabasePlatform()->registerDoctrineTypeMapping('spoint', 'string');

        return $connection;
    }

    /**
     * @return array
     * @param Database $database Database object containing connection parameters
     */
    private function getConnecionParams(Database $database): array
    {
        return [
            'dbname' => $database->getDbName(),
            'user' => $database->getLogin(),
            'password' => $database->getPassword(),
            'host' => $database->getHost(),
            'port' => $database->getPort(),
            'driver' => $database->getType(),
        ];
    }

    /**
     * Returns the connection parameters retrieved from the DSN stored in a file
     *
     * @return array
     * @param string $dsnFile Path of the file containing the DSN
     */
    private function getConnecionParamsByDsn(string $dsnFile): array
    {
        if (!file_exists($dsnFile)) {
            throw new ConnectionException(
                'The dsn file for the database does not exist (' . $dsnFile . ')'
            );
        }

        $dsn = file_get_contents($dsnFile);
        $dsnParser = new DsnParser(['postgres' => 'pdo_pgsql']);
        $connectionParams = $dsnParser->parse($dsn);
        return $connectionParams;
    }
}
