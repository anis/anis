<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

#[Entity, Table(name: 'archive')]
class Archive implements \JsonSerializable
{
    #[Id, Column(type: 'integer'), GeneratedValue]
    private int $id;

    #[Column(type: 'string', nullable: false)]
    private string $name;

    #[Column(type: 'datetime', nullable: false)]
    private \DateTime $created;

    #[Column(type: 'string', enumType: ArchiveStatus::class, nullable: false)]
    private ArchiveStatus $status;

    #[Column(type: 'string', name: 'file_size', nullable: true)]
    private ?string $fileSize = null;

    #[Column(type: 'string', nullable: true)]
    private ?string $email = null;

    #[Column(type: 'string', name: 'uniq_id', nullable: false)]
    private string $uniqId;

    #[ManyToOne(targetEntity: Dataset::class)]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'instance_name', nullable: false, onDelete: 'CASCADE')]
    #[JoinColumn(name: 'dataset_name', referencedColumnName: 'name', nullable: false, onDelete: 'CASCADE')]
    private Dataset $dataset;

    public function __construct(
        string $name,
        \DateTime $created,
        ArchiveStatus $status,
        string $uniqId,
        Dataset $dataset
    ) {
        $this->name = $name;
        $this->created = $created;
        $this->status = $status;
        $this->uniqId = $uniqId;
        $this->dataset = $dataset;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function getStatus(): ArchiveStatus
    {
        return $this->status;
    }

    public function setStatus(ArchiveStatus $status): void
    {
        $this->status = $status;
    }

    public function getFileSize(): ?string
    {
        return $this->fileSize;
    }

    public function setFileSize(string $fileSize): void
    {
        $this->fileSize = $fileSize;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getUniqId(): string
    {
        return $this->uniqId;
    }

    public function getDataset(): Dataset
    {
        return $this->dataset;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'created' => $this->getCreated()->format('Y-m-d\TH:i:s'),
            'status' => $this->getStatus(),
            'file_size' => $this->getFileSize(),
            'email' => $this->getEmail(),
            'instance_name' => $this->getDataset()->getInstance()->getName(),
            'dataset_name' => $this->getDataset()->getName()
        ];
    }
}
