<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

#[Entity, Table(name: 'instance_group')]
class InstanceGroup implements \JsonSerializable
{
    #[Id]
    #[ManyToOne(targetEntity: Instance::class, inversedBy: 'groups')]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'name', nullable: false, onDelete: 'CASCADE')]
    private Instance $instance;

    #[Id, Column(type: 'string')]
    private string $role;

    public function __construct(Instance $instance, string $role)
    {
        $this->instance = $instance;
        $this->role = $role;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function getInstance(): Instance
    {
        return $this->instance;
    }

    public function jsonSerialize(): array
    {
        return [
            'role' => $this->getRole()
        ];
    }
}
