<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

#[Entity, Table(name: 'file')]
class File implements \JsonSerializable
{
    #[Id, Column(type: 'string')]
    private string $id;

    #[Column(type: 'string', nullable: false)]
    private string $label;

    #[Column(name: 'file_path', type: 'string', nullable: false)]
    private string $filePath;

    #[Column(name: 'file_size', type: 'bigint', nullable: false)]
    private int $fileSize;

    #[Column(type: 'string', nullable: false)]
    private string $type;

    #[ManyToOne(targetEntity: Dataset::class, inversedBy: 'files')]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'instance_name', nullable: false, onDelete: 'CASCADE')]
    #[JoinColumn(name: 'dataset_name', referencedColumnName: 'name', nullable: false, onDelete: 'CASCADE')]
    private Dataset $dataset;

    public function __construct(Dataset $dataset, int $id)
    {
        $this->dataset = $dataset;
        $this->id = $this->dataset->getInstance()->getName() . '_' . $this->dataset->getName() . '_' . $id;
    }

    public function getId(): int
    {
        return intval(substr(
            $this->id,
            strlen($this->dataset->getInstance()->getName() . '_' . $this->dataset->getName()) + 1
        ));
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }

    public function setFilePath(string $filePath): void
    {
        $this->filePath = $filePath;
    }

    public function getFileSize(): int
    {
        return $this->fileSize;
    }

    public function setFileSize(int $fileSize): void
    {
        $this->fileSize = $fileSize;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getDataset(): Dataset
    {
        return $this->dataset;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'file_path' => $this->getFilePath(),
            'file_size' => $this->getFileSize(),
            'type' => $this->getType()
        ];
    }
}
