<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

#[Entity, Table(name: 'attribute')]
class Attribute implements \JsonSerializable
{
    #[Id, Column(type: 'string')]
    private string $id;

    #[ManyToOne(targetEntity: Dataset::class, inversedBy: 'attributes')]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'instance_name', nullable: false, onDelete: 'CASCADE')]
    #[JoinColumn(name: 'dataset_name', referencedColumnName: 'name', nullable: false, onDelete: 'CASCADE')]
    private Dataset $dataset;

    #[Column(type: 'string', nullable: false)]
    private string $name;

    #[Column(type: 'string', nullable: false)]
    private string $label;

    #[Column(name: 'form_label', type: 'string', nullable: false)]
    private string $formLabel;

    #[Column(name: 'datatable_label', type: 'string', nullable: true)]
    private ?string $datatableLabel;

    #[Column(type: 'string', nullable: true)]
    private ?string $description = null;

    #[Column(type: 'string', nullable: false)]
    private string $type;

    #[Column(name: 'search_type', type: 'string', nullable: true)]
    private ?string $searchType = null;

    #[Column(type: 'string', nullable: true)]
    private ?string $operator = null;

    #[Column(type: 'boolean', nullable: false)]
    private bool $dynamicOperator;

    #[Column(name: 'null_operators_enabled', type: 'boolean', nullable: false, options: ['default' => false])]
    private bool $nullOperatorsEnabled;

    #[Column(type: 'string', nullable: true)]
    private ?string $min = null;

    #[Column(type: 'string', nullable: true)]
    private ?string $max = null;

    #[Column(type: 'json', nullable: true)]
    private ?array $options = null;

    #[Column(name: 'placeholder_min', type: 'string', nullable: true)]
    private ?string $placeholderMin = null;

    #[Column(name: 'placeholder_max', type: 'string', nullable: true)]
    private ?string $placeholderMax = null;

    #[Column(name: 'criteria_display', type: 'integer', nullable: true)]
    private ?int $criteriaDisplay = null;

    #[Column(name: 'output_display', type: 'integer', nullable: true)]
    private ?int $outputDisplay = null;

    #[Column(type: 'boolean', nullable: false)]
    private bool $selected;

    #[Column(type: 'string', nullable: true)]
    private ?string $renderer = null;

    #[Column(name: 'renderer_config', type: 'json', nullable: true)]
    private ?array $rendererConfig = null;

    #[Column(name: 'order_by', type: 'boolean', nullable: false)]
    private bool $orderBy;

    #[Column(type: 'boolean', nullable: false)]
    private bool $archive;

    #[Column(name: 'detail_display', type: 'integer', nullable: true)]
    private ?int $detailDisplay;

    #[Column(name: 'detail_renderer', type: 'string', nullable: true)]
    private ?string $detailRenderer = null;

    #[Column(name: 'detail_renderer_config', type: 'json', nullable: true)]
    private ?array $detailRendererConfig = null;

    #[Column(name: 'vo_utype', type: 'string', nullable: true)]
    private ?string $voUtype = null;

    #[Column(name: 'vo_ucd', type: 'string', nullable: true)]
    private ?string $voUcd = null;

    #[Column(name: 'vo_unit', type: 'string', nullable: true)]
    private ?string $voUnit = null;

    #[Column(name: 'vo_description', type: 'string', nullable: true)]
    private ?string $voDescription = null;

    #[Column(name: 'vo_datatype', type: 'string', nullable: true)]
    private ?string $voDatatype = null;

    #[Column(name: 'vo_size', type: 'string', nullable: true)]
    private ?string $voSize = null;

    #[ManyToOne(targetEntity: CriteriaFamily::class)]
    #[JoinColumn(name: 'criteria_family', referencedColumnName: 'id', nullable: true, onDelete: SET_NULL)]
    private ?CriteriaFamily $criteriaFamily;

    #[ManyToOne(targetEntity: OutputCategory::class)]
    #[JoinColumn(name: 'output_category', referencedColumnName: 'id', nullable: true, onDelete: SET_NULL)]
    private ?OutputCategory $outputCategory;

    #[ManyToOne(targetEntity: OutputCategory::class)]
    #[JoinColumn(name: 'detail_output_category', referencedColumnName: 'id', nullable: true, onDelete: SET_NULL)]
    private ?OutputCategory $detailOutputCategory;

    public function __construct(Dataset $dataset, int $id)
    {
        $this->dataset = $dataset;
        $this->id = $this->dataset->getInstance()->getName() . '_' . $this->dataset->getName() . '_' . $id;
    }

    public function getId(): int
    {
        return intval(substr(
            $this->id,
            strlen($this->dataset->getInstance()->getName() . '_' . $this->dataset->getName()) + 1
        ));
    }

    public function getDataset(): Dataset
    {
        return $this->dataset;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getFormLabel(): string
    {
        return $this->formLabel;
    }

    public function setFormLabel(string $formLabel): void
    {
        $this->formLabel = $formLabel;
    }

    public function getDatatableLabel(): ?string
    {
        return $this->datatableLabel;
    }

    public function setDatatableLabel(?string $datatableLabel): void
    {
        $this->datatableLabel = $datatableLabel;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getSearchType(): ?string
    {
        return $this->searchType;
    }

    public function setSearchType(?string $searchType): void
    {
        $this->searchType = $searchType;
    }

    public function getOperator(): ?string
    {
        return $this->operator;
    }

    public function setOperator(?string $operator): void
    {
        $this->operator = $operator;
    }

    public function getDynamicOperator(): bool
    {
        return $this->dynamicOperator;
    }

    public function setDynamicOperator(bool $dynamicOperator): void
    {
        $this->dynamicOperator = $dynamicOperator;
    }

    public function getNullOperatorsEnabled(): bool
    {
        return $this->nullOperatorsEnabled;
    }

    public function setNullOperatorsEnabled($nullOperatorsEnabled): void
    {
        $this->nullOperatorsEnabled = $nullOperatorsEnabled;
    }

    public function getMin(): ?string
    {
        return $this->min;
    }

    public function setMin(?string $min): void
    {
        $this->min = $min;
    }

    public function getMax(): ?string
    {
        return $this->max;
    }

    public function setMax(?string $max): void
    {
        $this->max = $max;
    }

    public function getOptions(): ?array
    {
        return $this->options;
    }

    public function setOptions(?array $options): void
    {
        $this->options = $options;
    }

    public function getPlaceholderMin(): ?string
    {
        return $this->placeholderMin;
    }

    public function setPlaceholderMin(?string $placeholderMin): void
    {
        $this->placeholderMin = $placeholderMin;
    }

    public function getPlaceholderMax(): ?string
    {
        return $this->placeholderMax;
    }

    public function setPlaceholderMax(?string $placeholderMax): void
    {
        $this->placeholderMax = $placeholderMax;
    }

    public function getCriteriaDisplay(): ?int
    {
        return $this->criteriaDisplay;
    }

    public function setCriteriaDisplay(?int $criteriaDisplay): void
    {
        $this->criteriaDisplay = $criteriaDisplay;
    }

    public function getOutputDisplay(): ?int
    {
        return $this->outputDisplay;
    }

    public function setOutputDisplay(?int $outputDisplay): void
    {
        $this->outputDisplay = $outputDisplay;
    }

    public function getSelected(): bool
    {
        return $this->selected;
    }

    public function setSelected(bool $selected): void
    {
        $this->selected = $selected;
    }

    public function getRenderer(): ?string
    {
        return $this->renderer;
    }

    public function setRenderer(?string $renderer): void
    {
        $this->renderer = $renderer;
    }

    public function getRendererConfig(): ?array
    {
        return $this->rendererConfig;
    }

    public function setRendererConfig(?array $rendererConfig): void
    {
        $this->rendererConfig = $rendererConfig;
    }

    public function getOrderBy(): bool
    {
        return $this->orderBy;
    }

    public function setOrderBy(bool $orderBy): void
    {
        $this->orderBy = $orderBy;
    }

    public function getArchive(): bool
    {
        return $this->archive;
    }

    public function setArchive(bool $archive): void
    {
        $this->archive = $archive;
    }

    public function getDetailDisplay(): ?int
    {
        return $this->detailDisplay;
    }

    public function setDetailDisplay(?int $detailDisplay): void
    {
        $this->detailDisplay = $detailDisplay;
    }

    public function getDetailRenderer(): ?string
    {
        return $this->detailRenderer;
    }

    public function setDetailRenderer(?string $detailRenderer): void
    {
        $this->detailRenderer = $detailRenderer;
    }

    public function getDetailRendererConfig(): ?array
    {
        return $this->detailRendererConfig;
    }

    public function setDetailRendererConfig(?array $detailRendererConfig): void
    {
        $this->detailRendererConfig = $detailRendererConfig;
    }

    public function getVoUtype(): ?string
    {
        return $this->voUtype;
    }

    public function setVoUtype(?string $voUtype): void
    {
        $this->voUtype = $voUtype;
    }

    public function getVoUcd(): ?string
    {
        return $this->voUcd;
    }

    public function setVoUcd(?string $voUcd): void
    {
        $this->voUcd = $voUcd;
    }

    public function getVoUnit(): ?string
    {
        return $this->voUnit;
    }

    public function setVoUnit(?string $voUnit): void
    {
        $this->voUnit = $voUnit;
    }

    public function getVoDescription(): ?string
    {
        return $this->voDescription;
    }

    public function setVoDescription(?string $voDescription): void
    {
        $this->voDescription = $voDescription;
    }

    public function getVoDatatype(): ?string
    {
        return $this->voDatatype;
    }

    public function setVoDatatype(?string $voDatatype): void
    {
        $this->voDatatype = $voDatatype;
    }

    public function getVoSize(): ?string
    {
        return $this->voSize;
    }

    public function setVoSize(?string $voSize): void
    {
        $this->voSize = $voSize;
    }

    public function getCriteriaFamily(): ?CriteriaFamily
    {
        return $this->criteriaFamily;
    }

    public function setCriteriaFamily(?CriteriaFamily $criteriaFamily): void
    {
        $this->criteriaFamily = $criteriaFamily;
    }

    public function getOutputCategory(): ?OutputCategory
    {
        return $this->outputCategory;
    }

    public function setOutputCategory(?OutputCategory $outputCategory): void
    {
        $this->outputCategory = $outputCategory;
    }

    public function getDetailOutputCategory(): ?OutputCategory
    {
        return $this->detailOutputCategory;
    }

    public function setDetailOutputCategory(?OutputCategory $detailOutputCategory): void
    {
        $this->detailOutputCategory = $detailOutputCategory;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'form_label' => $this->getFormLabel(),
            'datatable_label' => $this->getDatatableLabel(),
            'description' => $this->getDescription(),
            'type' => $this->getType(),
            'search_type' => $this->getSearchType(),
            'operator' => $this->getOperator(),
            'dynamic_operator' => $this->getDynamicOperator(),
            'null_operators_enabled' => $this->getNullOperatorsEnabled(),
            'min' => $this->getMin(),
            'max' => $this->getMax(),
            'options' => $this->getOptions(),
            'placeholder_min' => $this->getPlaceholderMin(),
            'placeholder_max' => $this->getPlaceholderMax(),
            'criteria_display' => $this->getCriteriaDisplay(),
            'output_display' => $this->getOutputDisplay(),
            'selected' => $this->getSelected(),
            'renderer' => $this->getRenderer(),
            'renderer_config' => $this->getRendererConfig(),
            'order_by' => $this->getOrderBy(),
            'archive' => $this->getArchive(),
            'detail_display' => $this->getDetailDisplay(),
            'detail_renderer' => $this->getDetailRenderer(),
            'detail_renderer_config' => $this->getDetailRendererConfig(),
            'vo_utype' => $this->getVoUtype(),
            'vo_ucd' => $this->getVoUcd(),
            'vo_unit' => $this->getVoUnit(),
            'vo_description' => $this->getVoDescription(),
            'vo_datatype' => $this->getVoDatatype(),
            'vo_size' => $this->getVoSize(),
            'id_criteria_family' => is_null($this->getCriteriaFamily()) ? null : $this->getCriteriaFamily()->getId(),
            'id_output_category' => is_null($this->getOutputCategory())
                ? null
                : $this->getOutputCategory()->getOutputFamily()->getId() . '_' . $this->getOutputCategory()->getId(),
            'id_detail_output_category' => is_null($this->getDetailOutputCategory())
                ? null
                : $this->getDetailOutputCategory()->getOutputFamily()->getId()
                    . '_' . $this->getDetailOutputCategory()->getId()
        ];
    }
}
