<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Column;

#[Entity]
class Url extends MenuItem
{
    #[Column(name: 'type_url', type: 'string', nullable: false)]
    private string $typeUrl;

    #[Column(type: 'string', nullable: false)]
    private string $href;

    public function getTypeUrl(): string
    {
        return $this->typeUrl;
    }

    public function setTypeUrl(string $typeUrl): void
    {
        $this->typeUrl = $typeUrl;
    }

    public function getHref(): string
    {
        return $this->href;
    }

    public function setHref(string $href): void
    {
        $this->href = $href;
    }

    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), [
            'type_url' => $this->getTypeUrl(),
            'href' => $this->getHref(),
            'type' => 'url'
        ]);
    }
}
