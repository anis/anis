<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;

#[Entity]
class Webpage extends MenuItem
{
    #[Column(type: 'string', nullable: false)]
    private string $name;

    #[Column(type: 'string', nullable: false)]
    private string $title;

    #[Column(type: 'text', nullable: false)]
    private string $content;

    #[Column(name:'style_sheet', type: 'text', nullable: true)]
    private ?string $styleSheet = null;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content)
    {
        $this->content = $content;
    }

    public function getStyleSheet(): ?string
    {
        return $this->styleSheet;
    }

    public function setStyleSheet(?string $styleSheet): void
    {
        $this->styleSheet = $styleSheet;
    }

    public function jsonSerialize(): array
    {
        return array_merge(parent::jsonSerialize(), [
            'name' => $this->getName(),
            'title' => $this->getTitle(),
            'content' => $this->getContent(),
            'style_sheet' => $this->getStyleSheet(),
            'type' => 'webpage'
        ]);
    }
}
