<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

#[Entity, Table(name: 'image')]
class Image implements \JsonSerializable
{
    #[Id, Column(type: 'string')]
    private string $id;

    #[Column(type: 'string', nullable: false)]
    private string $label;

    #[Column(name: 'file_path', type: 'string', nullable: false)]
    private string $filePath;

    #[Column(name: 'hdu_number', type: 'integer', nullable: true)]
    private ?int $hduNumber;

    #[Column(name: 'file_size', type: 'bigint', nullable: false)]
    private int $fileSize;

    #[Column(name: 'ra_min', type: 'float', nullable: false)]
    private float $raMin;

    #[Column(name: 'ra_max', type: 'float', nullable: false)]
    private float $raMax;

    #[Column(name: 'dec_min', type: 'float', nullable: false)]
    private float $decMin;

    #[Column(name: 'dec_max', type: 'float', nullable: false)]
    private float $decMax;

    #[Column(type: 'string', nullable: false)]
    private string $stretch;

    #[Column(type: 'float', nullable: false)]
    private float $pmin;

    #[Column(type: 'float', nullable: false)]
    private float $pmax;

    #[ManyToOne(targetEntity: Dataset::class, inversedBy: 'images')]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'instance_name', nullable: false, onDelete: 'CASCADE')]
    #[JoinColumn(name: 'dataset_name', referencedColumnName: 'name', nullable: false, onDelete: 'CASCADE')]
    private Dataset $dataset;

    public function __construct(Dataset $dataset, int $id)
    {
        $this->dataset = $dataset;
        $this->id = $this->dataset->getInstance()->getName() . '_' . $this->dataset->getName() . '_' . $id;
    }

    public function getId(): int
    {
        return intval(substr(
            $this->id,
            strlen($this->dataset->getInstance()->getName() . '_' . $this->dataset->getName()) + 1
        ));
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }

    public function setFilePath(string $filePath): void
    {
        $this->filePath = $filePath;
    }

    public function getHduNumber(): ?int
    {
        return $this->hduNumber;
    }

    public function setHduNumber(?int $hduNumber): void
    {
        $this->hduNumber = $hduNumber;
    }

    public function getFileSize(): int
    {
        return $this->fileSize;
    }

    public function setFileSize(int $fileSize): void
    {
        $this->fileSize = $fileSize;
    }

    public function getRaMin(): float
    {
        return $this->raMin;
    }

    public function setRaMin(float $raMin): void
    {
        $this->raMin = $raMin;
    }

    public function getRaMax(): float
    {
        return $this->raMax;
    }

    public function setRaMax(float $raMax): void
    {
        $this->raMax = $raMax;
    }

    public function getDecMin(): float
    {
        return $this->decMin;
    }

    public function setDecMin(float $decMin): void
    {
        $this->decMin = $decMin;
    }

    public function getDecMax(): float
    {
        return $this->decMax;
    }

    public function setDecMax(float $decMax): void
    {
        $this->decMax = $decMax;
    }

    public function getStretch(): string
    {
        return $this->stretch;
    }

    public function setStretch(string $stretch): void
    {
        $this->stretch = $stretch;
    }

    public function getPmin(): float
    {
        return $this->pmin;
    }

    public function setPmin(float $pmin): void
    {
        $this->pmin = $pmin;
    }

    public function getPmax(): float
    {
        return $this->pmax;
    }

    public function setPmax($pmax)
    {
        $this->pmax = $pmax;
    }

    public function getDataset(): Dataset
    {
        return $this->dataset;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'file_path' => $this->getFilePath(),
            'hdu_number' => $this->getHduNumber(),
            'file_size' => $this->getFileSize(),
            'ra_min' => $this->getRaMin(),
            'ra_max' => $this->getRaMax(),
            'dec_min' => $this->getDecMin(),
            'dec_max' => $this->getDecMax(),
            'stretch' => $this->getStretch(),
            'pmin' => $this->getPmin(),
            'pmax' => $this->getPmax()
        ];
    }
}
