<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\Common\Collections\Collection;

#[Entity, Table(name: 'output_family')]
class OutputFamily implements \JsonSerializable
{
    #[Id, Column(type: 'string')]
    private string $id;

    #[Column(type: 'string', nullable: false)]
    private string $label;

    #[Column(type: 'integer', nullable: false)]
    private int $display;

    #[Column(type: 'boolean', nullable: false)]
    private bool $opened;

    #[ManyToOne(targetEntity: Dataset::class, inversedBy: 'outputFamilies')]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'instance_name', nullable: false, onDelete: 'CASCADE')]
    #[JoinColumn(name: 'dataset_name', referencedColumnName: 'name', nullable: false, onDelete: 'CASCADE')]
    private Dataset $dataset;

    #[OneToMany(targetEntity: OutputCategory::class, mappedBy: 'outputFamily')]
    private ?Collection $outputCategories = null;

    public function __construct(Dataset $dataset, int $id)
    {
        $this->dataset = $dataset;
        $this->id = $this->dataset->getInstance()->getName() . '_' . $this->dataset->getName() . '_' . $id;
    }

    public function getPrimaryKey(): string
    {
        return $this->id;
    }

    public function getId(): int
    {
        return intval(substr(
            $this->id,
            strlen($this->dataset->getInstance()->getName() . '_' . $this->dataset->getName()) + 1
        ));
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getDisplay(): int
    {
        return $this->display;
    }

    public function setDisplay(int $display): void
    {
        $this->display = $display;
    }

    public function getOpened(): bool
    {
        return $this->opened;
    }

    public function setOpened(bool $opened): void
    {
        $this->opened = $opened;
    }

    public function getDataset(): Dataset
    {
        return $this->dataset;
    }

    public function getOutputCategories(): ?Collection
    {
        return $this->outputCategories;
    }

    public function jsonSerialize(): array
    {
        $outputCategories = [];
        if (!is_null($this->getOutputCategories())) {
            $outputCategories = array_map(
                fn(OutputCategory $outputCategory) => $outputCategory->jsonSerialize(),
                $this->getOutputCategories()->toArray()
            );
        }

        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'display' => $this->getDisplay(),
            'opened' => $this->getOpened(),
            'output_categories' => $outputCategories
        ];
    }
}
