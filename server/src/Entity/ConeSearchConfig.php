<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\JoinColumn;

#[Entity, Table(name: 'cone_search_config')]
class ConeSearchConfig implements \JsonSerializable
{
    #[Id, Column(type: 'string')]
    private string $id;

    #[OneToOne(targetEntity: Dataset::class, inversedBy: 'coneSearchConfig')]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'instance_name', nullable: false, onDelete: 'CASCADE')]
    #[JoinColumn(name: 'dataset_name', referencedColumnName: 'name', nullable: false, onDelete: 'CASCADE')]
    private Dataset $datasetConeSearchConfig;

    #[Column(type: 'boolean', nullable: false)]
    private bool $enabled;

    #[Column(type: 'boolean', nullable: false)]
    private bool $opened;

    #[Column(name: 'column_ra', type: 'integer', nullable: false)]
    private int $columnRa;

    #[Column(name: 'column_dec', type: 'integer', nullable: false)]
    private int $columnDec;

    #[Column(name: 'resolver_enabled', type: 'boolean', nullable: false)]
    private bool $resolverEnabled;

    #[Column(name: 'default_ra', type: 'float', nullable: true)]
    private ?float $defaultRa = null;

    #[Column(name: 'default_dec', type: 'float', nullable: true)]
    private ?float $defaultDec = null;

    #[Column(name: 'default_radius', type: 'float', nullable: true)]
    private ?float $defaultRadius = null;

    #[Column(name: 'default_ra_dec_unit', type: 'string', nullable: false)]
    private string $defaultRaDecUnit;

    #[Column(name: 'plot_enabled', type: 'boolean', nullable: false)]
    private bool $plotEnabled;

    public function __construct(Dataset $datasetConeSearchConfig)
    {
        $this->id = $datasetConeSearchConfig->getInstance()->getName() . '_' . $datasetConeSearchConfig->getName();
        $this->datasetConeSearchConfig = $datasetConeSearchConfig;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    public function getOpened(): bool
    {
        return $this->opened;
    }

    public function setOpened(bool $opened): void
    {
        $this->opened = $opened;
    }

    public function getColumnRa(): int
    {
        return $this->columnRa;
    }

    public function setColumnRa(int $columnRa): void
    {
        $this->columnRa = $columnRa;
    }

    public function getColumnDec(): int
    {
        return $this->columnDec;
    }

    public function setColumnDec(int $columnDec): void
    {
        $this->columnDec = $columnDec;
    }

    public function getResolverEnabled(): bool
    {
        return $this->resolverEnabled;
    }

    public function setResolverEnabled(bool $resolverEnabled): void
    {
        $this->resolverEnabled = $resolverEnabled;
    }

    public function getDefaultRa(): ?float
    {
        return $this->defaultRa;
    }

    public function setDefaultRa(?float $defaultRa): void
    {
        $this->defaultRa = $defaultRa;
    }

    public function getDefaultDec(): ?float
    {
        return $this->defaultDec;
    }

    public function setDefaultDec(?float $defaultDec): void
    {
        $this->defaultDec = $defaultDec;
    }

    public function getDefaultRadius(): ?float
    {
        return $this->defaultRadius;
    }

    public function setDefaultRadius(?float $defaultRadius): void
    {
        $this->defaultRadius = $defaultRadius;
    }

    public function getDefaultRaDecUnit(): string
    {
        return $this->defaultRaDecUnit;
    }

    public function setDefaultRaDecUnit(string $defaultRaDecUnit): void
    {
        $this->defaultRaDecUnit = $defaultRaDecUnit;
    }

    public function getPlotEnabled(): bool
    {
        return $this->plotEnabled;
    }

    public function setPlotEnabled(bool $plotEnabled): void
    {
        $this->plotEnabled = $plotEnabled;
    }

    public function getDataset()
    {
        return $this->datasetConeSearchConfig;
    }

    public function jsonSerialize(): array
    {
        return [
            'enabled' => $this->getEnabled(),
            'opened' => $this->getOpened(),
            'column_ra' => $this->getColumnRa(),
            'column_dec' => $this->getColumnDec(),
            'resolver_enabled' => $this->getResolverEnabled(),
            'default_ra' => $this->getDefaultRa(),
            'default_dec' => $this->getDefaultDec(),
            'default_radius' => $this->getDefaultRadius(),
            'default_ra_dec_unit' => $this->getDefaultRaDecUnit(),
            'plot_enabled' => $this->getPlotEnabled()
        ];
    }
}
