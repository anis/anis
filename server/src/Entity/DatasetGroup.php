<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

#[Entity, Table(name: 'dataset_group')]
class DatasetGroup implements \JsonSerializable
{
    #[Id, Column(type: 'string', nullable: false)]
    private string $role;

    #[ManyToOne(targetEntity: Dataset::class, inversedBy: 'groups')]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'instance_name', nullable: false, onDelete: 'CASCADE')]
    #[JoinColumn(name: 'dataset_name', referencedColumnName: 'name', nullable: false, onDelete: 'CASCADE')]
    private Dataset $dataset;

    public function __construct(Dataset $dataset, string $role)
    {
        $this->dataset = $dataset;
        $this->role = $this->dataset->getInstance()->getName() . '_' . $this->dataset->getName() . '_' . $role;
    }

    public function getRole(): string
    {
        return substr(
            $this->role,
            strlen($this->dataset->getInstance()->getName() . '_' . $this->dataset->getName()) + 1
        );
    }

    public function getDataset(): Dataset
    {
        return $this->dataset;
    }

    public function jsonSerialize(): array
    {
        return [
            'role' => $this->getRole()
        ];
    }
}
