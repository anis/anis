<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

#[Entity, Table(name: 'database')]
class Database implements \JsonSerializable
{
    #[Id]
    #[ManyToOne(targetEntity: Instance::class, inversedBy: 'databases')]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'name', nullable: false, onDelete: 'CASCADE')]
    private Instance $instance;

    #[Id, Column(type: 'integer')]
    private int $id;

    #[Column(type: 'string', nullable: false)]
    private string $label;

    #[Column(type: 'string', nullable: true)]
    private ?string $dbname = null;

    #[Column(type: 'string', nullable: true)]
    private ?string $type = null;

    #[Column(type: 'string', nullable: true)]
    private ?string $host = null;

    #[Column(type: 'integer', nullable: true)]
    private ?int $port = null;

    #[Column(type: 'string', nullable: true)]
    private ?string $login = null;

    #[Column(type: 'string', nullable: true)]
    private ?string $password = null;

    #[Column(name: 'dsn_file', type: 'string', nullable: true)]
    private ?string $dsnFile = null;

    public function __construct(Instance $instance, int $id)
    {
        $this->instance = $instance;
        $this->id = $id;
    }

    public function getInstance(): Instance
    {
        return $this->instance;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getDbName(): ?string
    {
        return $this->dbname;
    }

    public function setDbName(?string $dbname): void
    {
        $this->dbname = $dbname;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    public function getHost(): ?string
    {
        return $this->host;
    }

    public function setHost(?string $host): void
    {
        $this->host = $host;
    }

    public function getPort(): ?int
    {
        return $this->port;
    }

    public function setPort(?int $port): void
    {
        $this->port = $port;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(?string $login): void
    {
        $this->login = $login;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    public function getDsnFile(): ?string
    {
        return $this->dsnFile;
    }

    public function setDsnFile(?string $dsnFile): void
    {
        $this->dsnFile = $dsnFile;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'dbname' => $this->getDbName(),
            'dbtype' => $this->getType(),
            'dbhost' => $this->getHost(),
            'dbport' => $this->getPort(),
            'dblogin' => $this->getLogin(),
            'dbpassword' => $this->getPassword(),
            'dbdsn_file' => $this->getDsnFile()
        ];
    }
}
