<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\JoinColumn;

#[Entity, Table(name: 'design_config')]
class DesignConfig implements \JsonSerializable
{
    #[Id]
    #[OneToOne(targetEntity: Instance::class, inversedBy: 'designConfig')]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'name', nullable: false, onDelete: 'CASCADE')]
    private Instance $instance;

    #[Column(name:'design_background_color', type: 'string', nullable: true)]
    private ?string $designBackgroundColor = null;

    #[Column(name:'design_text_color', type: 'string', nullable: false)]
    private string $designTextColor;

    #[Column(name:'design_font_family', type: 'string', nullable: false)]
    private string $designFontFamily;

    #[Column(name:'design_link_color', type: 'string', nullable: false)]
    private string $designLinkColor;

    #[Column(name:'design_link_hover_color', type: 'string', nullable: false)]
    private string $designLinkHoverColor;

    #[Column(name:'design_logo', type: 'string', nullable: true)]
    private ?string $designLogo = null;

    #[Column(name:'design_logo_href', type: 'string', nullable: true)]
    private ?string $designLogoHref = null;

    #[Column(name:'design_favicon', type: 'string', nullable: true)]
    private ?string $designFavicon = null;

    #[Column(name:'navbar_background_color', type: 'string', nullable: false)]
    private string $navbarBackgroundColor;

    #[Column(name:'navbar_border_bottom_color', type: 'string', nullable: false)]
    private string $navbarBorderBottomColor;

    #[Column(name:'navbar_color_href', type: 'string', nullable: false)]
    private string $navbarColorHref;

    #[Column(name:'navbar_font_family', type: 'string', nullable: false)]
    private string $navbarFontFamily;

    #[Column(name:'navbar_sign_in_btn_color', type: 'string', nullable: false)]
    private string $navbarSignInBtnColor;

    #[Column(name:'navbar_user_btn_color', type: 'string', nullable: false)]
    private string $navbarUserBtnColor;

    #[Column(name:'footer_background_color', type: 'string', nullable: false)]
    private string $footerBackgroundColor;

    #[Column(name:'footer_border_top_color', type: 'string', nullable: false)]
    private string $footerBorderTopColor;

    #[Column(name:'footer_text_color', type: 'string', nullable: false)]
    private string $footerTextColor;

    #[Column(name:'footer_logos', type: 'json', nullable: false)]
    private array $footerLogos;

    #[Column(name:'family_border_color', type: 'string', nullable: false)]
    private string $familyBorderColor;

    #[Column(name:'family_header_background_color', type: 'string', nullable: false)]
    private string $familyHeaderBackgroundColor;

    #[Column(name:'family_title_color', type: 'string', nullable: false)]
    private string $familyTitleColor;

    #[Column(name:'family_title_bold', type: 'boolean', nullable: false)]
    private bool $familyTitleBold;

    #[Column(name:'family_background_color', type: 'string', nullable: false)]
    private string $familyBackgroundColor;

    #[Column(name:'family_text_color', type: 'string', nullable: false)]
    private string $familyTextColor;

    #[Column(name:'progress_bar_title', type: 'string', nullable: false)]
    private string $progressBarTitle;

    #[Column(name:'progress_bar_title_color', type: 'string', nullable: false)]
    private string $progressBarTitleColor;

    #[Column(name:'progress_bar_subtitle', type: 'string', nullable: false)]
    private string $progressBarSubtitle;

    #[Column(name:'progress_bar_subtitle_color', type: 'string', nullable: false)]
    private string $progressBarSubtitleColor;

    #[Column(name:'progress_bar_step_dataset_title', type: 'string', nullable: false)]
    private string $progressBarStepDatasetTitle;

    #[Column(name:'progress_bar_step_criteria_title', type: 'string', nullable: false)]
    private string $progressBarStepCriteriaTitle;

    #[Column(name:'progress_bar_step_output_title', type: 'string', nullable: false)]
    private string $progressBarStepOutputTitle;

    #[Column(name:'progress_bar_step_result_title', type: 'string', nullable: false)]
    private string $progressBarStepResultTitle;

    #[Column(name:'progress_bar_color', type: 'string', nullable: false)]
    private string $progressBarColor;

    #[Column(name:'progress_bar_active_color', type: 'string', nullable: false)]
    private string $progressBarActiveColor;

    #[Column(name:'progress_bar_circle_color', type: 'string', nullable: false)]
    private string $progressBarCircleColor;

    #[Column(name:'progress_bar_circle_icon_color', type: 'string', nullable: false)]
    private string $progressBarCircleIconColor;

    #[Column(name:'progress_bar_circle_icon_active_color', type: 'string', nullable: false)]
    private string $progressBarCircleIconActiveColor;

    #[Column(name:'progress_bar_text_color', type: 'string', nullable: false)]
    private string $progressBarTextColor;

    #[Column(name:'progress_bar_text_bold', type: 'boolean', nullable: false)]
    private bool $progressBarTextBold;

    #[Column(name:'search_next_btn_color', type: 'string', nullable: false)]
    private string $searchNextBtnColor;

    #[Column(name:'search_next_btn_hover_color', type: 'string', nullable: false)]
    private string $searchNextBtnHoverColor;

    #[Column(name:'search_next_btn_hover_text_color', type: 'string', nullable: false)]
    private string $searchNextBtnHoverTextColor;

    #[Column(name:'search_back_btn_color', type: 'string', nullable: false)]
    private string $searchBackBtnColor;

    #[Column(name:'search_back_btn_hover_color', type: 'string', nullable: false)]
    private string $searchBackBtnHoverColor;

    #[Column(name:'search_back_btn_hover_text_color', type: 'string', nullable: false)]
    private string $searchBackBtnHoverTextColor;

    #[Column(name:'delete_current_query_btn_enabled', type: 'boolean', nullable: false, options: ['default' => false])]
    private bool $deleteCurrentQueryBtnEnabled;

    #[Column(
        name:'delete_current_query_btn_text',
        type: 'string',
        nullable: false,
        options: ['default' => 'Delete the current query']
    )]
    private string $deleteCurrentQueryBtnText;

    #[Column(name:'delete_current_query_btn_color', type: 'string', nullable: false, options: ['default' => '#DC3545'])]
    private string $deleteCurrentQueryBtnColor;

    #[Column(
        name:'delete_current_query_btn_hover_color',
        type: 'string',
        nullable: false,
        options: ['default' => '#C82333']
    )]
    private string $deleteCurrentQueryBtnHoverColor;

    #[Column(
        name:'delete_current_query_btn_hover_text_color',
        type: 'string',
        nullable: false,
        options: ['default' => '#FFFFFF']
    )]
    private string $deleteCurrentQueryBtnHoverTextColor;

    #[Column(name:'delete_criterion_cross_color', type: 'string', nullable: false, options: ['default' => '#DC3545'])]
    private string $deleteCriterionCrossColor;

    #[Column(name:'search_info_background_color', type: 'string', nullable: false)]
    private string $searchInfoBackgroundColor;

    #[Column(name:'search_info_text_color', type: 'string', nullable: false)]
    private string $searchInfoTextColor;

    #[Column(name:'search_info_help_enabled', type: 'boolean', nullable: false)]
    private bool $searchInfoHelpEnabled;

    #[Column(name:'dataset_select_btn_color', type: 'string', nullable: false)]
    private string $datasetSelectBtnColor;

    #[Column(name:'dataset_select_btn_hover_color', type: 'string', nullable: false)]
    private string $datasetSelectBtnHoverColor;

    #[Column(name:'dataset_select_btn_hover_text_color', type: 'string', nullable: false)]
    private string $datasetSelectBtnHoverTextColor;

    #[Column(name:'dataset_selected_icon_color', type: 'string', nullable: false)]
    private string $datasetSelectedIconColor;

    #[Column(name:'search_criterion_background_color', type: 'string', nullable: false)]
    private string $searchCriterionBackgroundColor;

    #[Column(name:'search_criterion_text_color', type: 'string', nullable: false)]
    private string $searchCriterionTextColor;

    #[Column(name:'output_columns_selected_color', type: 'string', nullable: false)]
    private string $outputColumnsSelectedColor;

    #[Column(name:'output_columns_select_all_btn_color', type: 'string', nullable: false)]
    private string $outputColumnsSelectAllBtnColor;

    #[Column(name:'output_columns_select_all_btn_hover_color', type: 'string', nullable: false)]
    private string $outputColumnsSelectAllBtnHoverColor;

    #[Column(name:'output_columns_select_all_btn_hover_text_color', type: 'string', nullable: false)]
    private string $outputColumnsSelectAllBtnHoverTextColor;

    #[Column(name:'result_panel_border_size', type: 'string', nullable: false)]
    private string $resultPanelBorderSize;

    #[Column(name:'result_panel_border_color', type: 'string', nullable: false)]
    private string $resultPanelBorderColor;

    #[Column(name:'result_panel_title_color', type: 'string', nullable: false)]
    private string $resultPanelTitleColor;

    #[Column(name:'result_panel_background_color', type: 'string', nullable: false)]
    private string $resultPanelBackgroundColor;

    #[Column(name:'result_panel_text_color', type: 'string', nullable: false)]
    private string $resultPanelTextColor;

    #[Column(name:'result_download_btn_color', type: 'string', nullable: false)]
    private string $resultDownloadBtnColor;

    #[Column(name:'result_download_btn_hover_color', type: 'string', nullable: false)]
    private string $resultDownloadBtnHoverColor;

    #[Column(name:'result_download_btn_text_color', type: 'string', nullable: false)]
    private string $resultDownloadBtnTextColor;

    #[Column(name:'result_datatable_actions_btn_color', type: 'string', nullable: false)]
    private string $resultDatatableActionsBtnColor;

    #[Column(name:'result_datatable_actions_btn_hover_color', type: 'string', nullable: false)]
    private string $resultDatatableActionsBtnHoverColor;

    #[Column(name:'result_datatable_actions_btn_text_color', type: 'string', nullable: false)]
    private string $resultDatatableActionsBtnTextColor;

    #[Column(name:'resultDatatableBordered', type: 'boolean', nullable: false)]
    private bool $resultDatatableBordered;

    #[Column(name:'result_datatable_bordered_radius', type: 'boolean', nullable: false)]
    private bool $resultDatatableBorderedRadius;

    #[Column(name:'result_datatable_border_color', type: 'string', nullable: false)]
    private string $resultDatatableBorderColor;

    #[Column(name:'result_datatable_header_background_color', type: 'string', nullable: false)]
    private string $resultDatatableHeaderBackgroundColor;

    #[Column(name:'result_datatable_header_text_color', type: 'string', nullable: false)]
    private string $resultDatatableHeaderTextColor;

    #[Column(name:'result_datatable_rows_background_color', type: 'string', nullable: false)]
    private string $resultDatatableRowsBackgroundColor;

    #[Column(name:'result_datatable_rows_text_color', type: 'string', nullable: false)]
    private string $resultDatatableRowsTextColor;

    #[Column(name:'result_datatable_sorted_color', type: 'string', nullable: false)]
    private string $resultDatatableSortedColor;

    #[Column(name:'result_datatable_sorted_active_color', type: 'string', nullable: false)]
    private string $resultDatatableSortedActiveColor;

    #[Column(name:'result_datatable_link_color', type: 'string', nullable: false)]
    private string $resultDatatableLinkColor;

    #[Column(name:'result_datatable_link_hover_color', type: 'string', nullable: false)]
    private string $resultDatatableLinkHoverColor;

    #[Column(name:'result_datatable_rows_selected_color', type: 'string', nullable: false)]
    private string $resultDatatableRowsSelectedColor;

    #[Column(name:'result_datatable_pagination_link_color', type: 'string', nullable: false)]
    private string $resultDatatablePaginationLinkColor;

    #[Column(name:'result_datatable_pagination_active_bck_color', type: 'string', nullable: false)]
    private string $resultDatatablePaginationActiveBckColor;

    #[Column(name:'result_datatable_pagination_active_text_color', type: 'string', nullable: false)]
    private string $resultDatatablePaginationActiveTextColor;

    #[Column(name:'samp_enabled', type: 'boolean', nullable: false)]
    private bool $sampEnabled;

    #[Column(name:'back_to_portal', type: 'boolean', nullable: false)]
    private bool $backToPortal;

    #[Column(name:'user_menu_enabled', type: 'boolean', nullable: false)]
    private bool $userMenuEnabled;

    #[Column(name:'search_multiple_all_datasets_selected', type: 'boolean', nullable: false)]
    private bool $searchMultipleAllDatasetsSelected;

    #[Column(name:'search_multiple_progress_bar_title', type: 'string', nullable: false)]
    private string $searchMultipleProgressBarTitle;

    #[Column(name:'search_multiple_progress_bar_subtitle', type: 'string', nullable: false)]
    private string $searchMultipleProgressBarSubtitle;

    #[Column(name:'search_multiple_progress_bar_step_position', type: 'string', nullable: false)]
    private string $searchMultipleProgressBarStepPosition;

    #[Column(name:'search_multiple_progress_bar_step_datasets', type: 'string', nullable: false)]
    private string $searchMultipleProgressBarStepDatasets;

    #[Column(name:'search_multiple_progress_bar_step_result', type: 'string', nullable: false)]
    private string $searchMultipleProgressBarStepResult;

    public function __construct(Instance $instance)
    {
        $this->instance = $instance;
    }

    public function getDesignBackgroundColor(): ?string
    {
        return $this->designBackgroundColor;
    }

    public function setDesignBackgroundColor(?string $designBackgroundColor): void
    {
        $this->designBackgroundColor = $designBackgroundColor;
    }

    public function getDesignTextColor(): string
    {
        return $this->designTextColor;
    }

    public function setDesignTextColor(string $designTextColor): void
    {
        $this->designTextColor = $designTextColor;
    }

    public function getDesignFontFamily(): string
    {
        return $this->designFontFamily;
    }

    public function setDesignFontFamily(string $designFontFamily): void
    {
        $this->designFontFamily = $designFontFamily;
    }

    public function getDesignLinkColor(): string
    {
        return $this->designLinkColor;
    }

    public function setDesignLinkColor(string $designLinkColor): void
    {
        $this->designLinkColor = $designLinkColor;
    }

    public function getDesignLinkHoverColor(): string
    {
        return $this->designLinkHoverColor;
    }

    public function setDesignLinkHoverColor(string $designLinkHoverColor): void
    {
        $this->designLinkHoverColor = $designLinkHoverColor;
    }

    public function getDesignLogo(): ?string
    {
        return $this->designLogo;
    }

    public function setDesignLogo(?string $designLogo): void
    {
        $this->designLogo = $designLogo;
    }

    public function getDesignLogoHref(): ?string
    {
        return $this->designLogoHref;
    }

    public function setDesignLogoHref(?string $designLogoHref): void
    {
        $this->designLogoHref = $designLogoHref;
    }

    public function getDesignFavicon(): ?string
    {
        return $this->designFavicon;
    }

    public function setDesignFavicon(?string $designFavicon): void
    {
        $this->designFavicon = $designFavicon;
    }

    public function getNavbarBackgroundColor(): string
    {
        return $this->navbarBackgroundColor;
    }

    public function setNavbarBackgroundColor(string $navbarBackgroundColor): void
    {
        $this->navbarBackgroundColor = $navbarBackgroundColor;
    }

    public function getNavbarBorderBottomColor(): string
    {
        return $this->navbarBorderBottomColor;
    }

    public function setNavbarBorderBottomColor(string $navbarBorderBottomColor): void
    {
        $this->navbarBorderBottomColor = $navbarBorderBottomColor;
    }

    public function getNavbarColorHref(): string
    {
        return $this->navbarColorHref;
    }

    public function setNavbarColorHref(string $navbarColorHref): void
    {
        $this->navbarColorHref = $navbarColorHref;
    }

    public function getNavbarFontFamily(): string
    {
        return $this->navbarFontFamily;
    }

    public function setNavbarFontFamily(string $navbarFontFamily): void
    {
        $this->navbarFontFamily = $navbarFontFamily;
    }

    public function getNavbarSignInBtnColor(): string
    {
        return $this->navbarSignInBtnColor;
    }

    public function setNavbarSignInBtnColor(string $navbarSignInBtnColor): void
    {
        $this->navbarSignInBtnColor = $navbarSignInBtnColor;
    }

    public function getNavbarUserBtnColor(): string
    {
        return $this->navbarUserBtnColor;
    }

    public function setNavbarUserBtnColor(string $navbarUserBtnColor): void
    {
        $this->navbarUserBtnColor = $navbarUserBtnColor;
    }

    public function getFooterBackgroundColor(): string
    {
        return $this->footerBackgroundColor;
    }

    public function setFooterBackgroundColor(string $footerBackgroundColor): void
    {
        $this->footerBackgroundColor = $footerBackgroundColor;
    }

    public function getFooterBorderTopColor(): string
    {
        return $this->footerBorderTopColor;
    }

    public function setFooterBorderTopColor(string $footerBorderTopColor): void
    {
        $this->footerBorderTopColor = $footerBorderTopColor;
    }

    public function getFooterTextColor(): string
    {
        return $this->footerTextColor;
    }

    public function setFooterTextColor(string $footerTextColor): void
    {
        $this->footerTextColor = $footerTextColor;
    }

    public function getFooterLogos(): array
    {
        return $this->footerLogos;
    }

    public function setFooterLogos(array $footerLogos): void
    {
        $this->footerLogos = $footerLogos;
    }

    public function getFamilyBorderColor(): string
    {
        return $this->familyBorderColor;
    }

    public function setFamilyBorderColor(string $familyBorderColor): void
    {
        $this->familyBorderColor = $familyBorderColor;
    }

    public function getFamilyHeaderBackgroundColor(): string
    {
        return $this->familyHeaderBackgroundColor;
    }

    public function setFamilyHeaderBackgroundColor(string $familyHeaderBackgroundColor): void
    {
        $this->familyHeaderBackgroundColor = $familyHeaderBackgroundColor;
    }

    public function getFamilyTitleColor(): string
    {
        return $this->familyTitleColor;
    }

    public function setFamilyTitleColor(string $familyTitleColor): void
    {
        $this->familyTitleColor = $familyTitleColor;
    }

    public function getFamilyTitleBold(): bool
    {
        return $this->familyTitleBold;
    }

    public function setFamilyTitleBold(bool $familyTitleBold): void
    {
        $this->familyTitleBold = $familyTitleBold;
    }

    public function getFamilyBackgroundColor(): string
    {
        return $this->familyBackgroundColor;
    }

    public function setFamilyBackgroundColor(string $familyBackgroundColor): void
    {
        $this->familyBackgroundColor = $familyBackgroundColor;
    }

    public function getFamilyTextColor(): string
    {
        return $this->familyTextColor;
    }

    public function setFamilyTextColor(string $familyTextColor): void
    {
        $this->familyTextColor = $familyTextColor;
    }

    public function getProgressBarTitle(): string
    {
        return $this->progressBarTitle;
    }

    public function setProgressBarTitle(string $progressBarTitle): void
    {
        $this->progressBarTitle = $progressBarTitle;
    }

    public function getProgressBarTitleColor(): string
    {
        return $this->progressBarTitleColor;
    }

    public function setProgressBarTitleColor(string $progressBarTitleColor): void
    {
        $this->progressBarTitleColor = $progressBarTitleColor;
    }

    public function getProgressBarSubtitle(): string
    {
        return $this->progressBarSubtitle;
    }

    public function setProgressBarSubtitle(string $progressBarSubtitle): void
    {
        $this->progressBarSubtitle = $progressBarSubtitle;
    }

    public function getProgressBarSubtitleColor(): string
    {
        return $this->progressBarSubtitleColor;
    }

    public function setProgressBarSubtitleColor(string $progressBarSubtitleColor): void
    {
        $this->progressBarSubtitleColor = $progressBarSubtitleColor;
    }

    public function getProgressBarStepDatasetTitle(): string
    {
        return $this->progressBarStepDatasetTitle;
    }

    public function setProgressBarStepDatasetTitle(string $progressBarStepDatasetTitle): void
    {
        $this->progressBarStepDatasetTitle = $progressBarStepDatasetTitle;
    }

    public function getProgressBarStepCriteriaTitle(): string
    {
        return $this->progressBarStepCriteriaTitle;
    }

    public function setProgressBarStepCriteriaTitle(string $progressBarStepCriteriaTitle): void
    {
        $this->progressBarStepCriteriaTitle = $progressBarStepCriteriaTitle;
    }

    public function getProgressBarStepOutputTitle(): string
    {
        return $this->progressBarStepOutputTitle;
    }

    public function setProgressBarStepOutputTitle(string $progressBarStepOutputTitle): void
    {
        $this->progressBarStepOutputTitle = $progressBarStepOutputTitle;
    }

    public function getProgressBarStepResultTitle(): string
    {
        return $this->progressBarStepResultTitle;
    }

    public function setProgressBarStepResultTitle(string $progressBarStepResultTitle): void
    {
        $this->progressBarStepResultTitle = $progressBarStepResultTitle;
    }

    public function getProgressBarColor(): string
    {
        return $this->progressBarColor;
    }

    public function setProgressBarColor(string $progressBarColor): void
    {
        $this->progressBarColor = $progressBarColor;
    }

    public function getProgressBarActiveColor(): string
    {
        return $this->progressBarActiveColor;
    }

    public function setProgressBarActiveColor(string $progressBarActiveColor): void
    {
        $this->progressBarActiveColor = $progressBarActiveColor;
    }

    public function getProgressBarCircleColor(): string
    {
        return $this->progressBarCircleColor;
    }

    public function setProgressBarCircleColor(string $progressBarCircleColor): void
    {
        $this->progressBarCircleColor = $progressBarCircleColor;
    }

    public function getProgressBarCircleIconColor(): string
    {
        return $this->progressBarCircleIconColor;
    }

    public function setProgressBarCircleIconColor(string $progressBarCircleIconColor): void
    {
        $this->progressBarCircleIconColor = $progressBarCircleIconColor;
    }

    public function getProgressBarCircleIconActiveColor(): string
    {
        return $this->progressBarCircleIconActiveColor;
    }

    public function setProgressBarCircleIconActiveColor(string $progressBarCircleIconActiveColor): void
    {
        $this->progressBarCircleIconActiveColor = $progressBarCircleIconActiveColor;
    }

    public function getProgressBarTextColor(): string
    {
        return $this->progressBarTextColor;
    }

    public function setProgressBarTextColor(string $progressBarTextColor): void
    {
        $this->progressBarTextColor = $progressBarTextColor;
    }

    public function getProgressBarTextBold(): bool
    {
        return $this->progressBarTextBold;
    }

    public function setProgressBarTextBold(bool $progressBarTextBold): void
    {
        $this->progressBarTextBold = $progressBarTextBold;
    }

    public function getSearchNextBtnColor(): string
    {
        return $this->searchNextBtnColor;
    }

    public function setSearchNextBtnColor(string $searchNextBtnColor): void
    {
        $this->searchNextBtnColor = $searchNextBtnColor;
    }

    public function getSearchNextBtnHoverColor(): string
    {
        return $this->searchNextBtnHoverColor;
    }

    public function setSearchNextBtnHoverColor(string $searchNextBtnHoverColor): void
    {
        $this->searchNextBtnHoverColor = $searchNextBtnHoverColor;
    }

    public function getSearchNextBtnHoverTextColor(): string
    {
        return $this->searchNextBtnHoverTextColor;
    }

    public function setSearchNextBtnHoverTextColor(string $searchNextBtnHoverTextColor): void
    {
        $this->searchNextBtnHoverTextColor = $searchNextBtnHoverTextColor;
    }

    public function getSearchBackBtnColor(): string
    {
        return $this->searchBackBtnColor;
    }

    public function setSearchBackBtnColor(string $searchBackBtnColor): void
    {
        $this->searchBackBtnColor = $searchBackBtnColor;
    }

    public function getSearchBackBtnHoverColor(): string
    {
        return $this->searchBackBtnHoverColor;
    }

    public function setSearchBackBtnHoverColor(string $searchBackBtnHoverColor): void
    {
        $this->searchBackBtnHoverColor = $searchBackBtnHoverColor;
    }

    public function getSearchBackBtnHoverTextColor(): string
    {
        return $this->searchBackBtnHoverTextColor;
    }

    public function setSearchBackBtnHoverTextColor(string $searchBackBtnHoverTextColor): void
    {
        $this->searchBackBtnHoverTextColor = $searchBackBtnHoverTextColor;
    }

    public function getDeleteCurrentQueryBtnEnabled(): bool
    {
        return $this->deleteCurrentQueryBtnEnabled;
    }

    public function setDeleteCurrentQueryBtnEnabled(bool $deleteCurrentQueryBtnEnabled): void
    {
        $this->deleteCurrentQueryBtnEnabled = $deleteCurrentQueryBtnEnabled;
    }

    public function getDeleteCurrentQueryBtnText(): string
    {
        return $this->deleteCurrentQueryBtnText;
    }

    public function setDeleteCurrentQueryBtnText(string $deleteCurrentQueryBtnText): void
    {
        $this->deleteCurrentQueryBtnText = $deleteCurrentQueryBtnText;
    }

    public function getDeleteCurrentQueryBtnColor(): string
    {
        return $this->deleteCurrentQueryBtnColor;
    }

    public function setDeleteCurrentQueryBtnColor(string $deleteCurrentQueryBtnColor): void
    {
        $this->deleteCurrentQueryBtnColor = $deleteCurrentQueryBtnColor;
    }

    public function getDeleteCurrentQueryBtnHoverColor(): string
    {
        return $this->deleteCurrentQueryBtnHoverColor;
    }

    public function setDeleteCurrentQueryBtnHoverColor(string $deleteCurrentQueryBtnHoverColor): void
    {
        $this->deleteCurrentQueryBtnHoverColor = $deleteCurrentQueryBtnHoverColor;
    }

    public function getDeleteCurrentQueryBtnHoverTextColor(): string
    {
        return $this->deleteCurrentQueryBtnHoverTextColor;
    }

    public function setDeleteCurrentQueryBtnHoverTextColor(string $deleteCurrentQueryBtnHoverTextColor): void
    {
        $this->deleteCurrentQueryBtnHoverTextColor = $deleteCurrentQueryBtnHoverTextColor;
    }

    public function getDeleteCriterionCrossColor(): string
    {
        return $this->deleteCriterionCrossColor;
    }

    public function setDeleteCriterionCrossColor(string $deleteCriterionCrossColor): void
    {
        $this->deleteCriterionCrossColor = $deleteCriterionCrossColor;
    }

    public function getSearchInfoBackgroundColor(): string
    {
        return $this->searchInfoBackgroundColor;
    }

    public function setSearchInfoBackgroundColor(string $searchInfoBackgroundColor): void
    {
        $this->searchInfoBackgroundColor = $searchInfoBackgroundColor;
    }

    public function getSearchInfoTextColor(): string
    {
        return $this->searchInfoTextColor;
    }

    public function setSearchInfoTextColor(string $searchInfoTextColor): void
    {
        $this->searchInfoTextColor = $searchInfoTextColor;
    }

    public function getSearchInfoHelpEnabled(): bool
    {
        return $this->searchInfoHelpEnabled;
    }

    public function setSearchInfoHelpEnabled(bool $searchInfoHelpEnabled): void
    {
        $this->searchInfoHelpEnabled = $searchInfoHelpEnabled;
    }

    public function getDatasetSelectBtnColor(): string
    {
        return $this->datasetSelectBtnColor;
    }

    public function setDatasetSelectBtnColor(string $datasetSelectBtnColor): void
    {
        $this->datasetSelectBtnColor = $datasetSelectBtnColor;
    }

    public function getDatasetSelectBtnHoverColor(): string
    {
        return $this->datasetSelectBtnHoverColor;
    }

    public function setDatasetSelectBtnHoverColor(string $datasetSelectBtnHoverColor): void
    {
        $this->datasetSelectBtnHoverColor = $datasetSelectBtnHoverColor;
    }

    public function getDatasetSelectBtnHoverTextColor(): string
    {
        return $this->datasetSelectBtnHoverTextColor;
    }

    public function setDatasetSelectBtnHoverTextColor(string $datasetSelectBtnHoverTextColor): void
    {
        $this->datasetSelectBtnHoverTextColor = $datasetSelectBtnHoverTextColor;
    }

    public function getDatasetSelectedIconColor(): string
    {
        return $this->datasetSelectedIconColor;
    }

    public function setDatasetSelectedIconColor(string $datasetSelectedIconColor): void
    {
        $this->datasetSelectedIconColor = $datasetSelectedIconColor;
    }

    public function getSearchCriterionBackgroundColor(): string
    {
        return $this->searchCriterionBackgroundColor;
    }

    public function setSearchCriterionBackgroundColor(string $searchCriterionBackgroundColor): void
    {
        $this->searchCriterionBackgroundColor = $searchCriterionBackgroundColor;
    }

    public function getSearchCriterionTextColor(): string
    {
        return $this->searchCriterionTextColor;
    }

    public function setSearchCriterionTextColor(string $searchCriterionTextColor): void
    {
        $this->searchCriterionTextColor = $searchCriterionTextColor;
    }

    public function getOutputColumnsSelectedColor(): string
    {
        return $this->outputColumnsSelectedColor;
    }

    public function setOutputColumnsSelectedColor(string $outputColumnsSelectedColor): void
    {
        $this->outputColumnsSelectedColor = $outputColumnsSelectedColor;
    }

    public function getOutputColumnsSelectAllBtnColor(): string
    {
        return $this->outputColumnsSelectAllBtnColor;
    }

    public function setOutputColumnsSelectAllBtnColor(string $outputColumnsSelectAllBtnColor): void
    {
        $this->outputColumnsSelectAllBtnColor = $outputColumnsSelectAllBtnColor;
    }

    public function getOutputColumnsSelectAllBtnHoverColor(): string
    {
        return $this->outputColumnsSelectAllBtnHoverColor;
    }

    public function setOutputColumnsSelectAllBtnHoverColor(string $outputColumnsSelectAllBtnHoverColor): void
    {
        $this->outputColumnsSelectAllBtnHoverColor = $outputColumnsSelectAllBtnHoverColor;
    }

    public function getOutputColumnsSelectAllBtnHoverTextColor(): string
    {
        return $this->outputColumnsSelectAllBtnHoverTextColor;
    }

    public function setOutputColumnsSelectAllBtnHoverTextColor(string $outputColumnsSelectAllBtnHoverTextColor): void
    {
        $this->outputColumnsSelectAllBtnHoverTextColor = $outputColumnsSelectAllBtnHoverTextColor;
    }

    public function getResultPanelBorderSize(): string
    {
        return $this->resultPanelBorderSize;
    }

    public function setResultPanelBorderSize(string $resultPanelBorderSize): void
    {
        $this->resultPanelBorderSize = $resultPanelBorderSize;
    }

    public function getResultPanelBorderColor(): string
    {
        return $this->resultPanelBorderColor;
    }

    public function setResultPanelBorderColor(string $resultPanelBorderColor): void
    {
        $this->resultPanelBorderColor = $resultPanelBorderColor;
    }

    public function getResultPanelTitleColor(): string
    {
        return $this->resultPanelTitleColor;
    }

    public function setResultPanelTitleColor(string $resultPanelTitleColor): void
    {
        $this->resultPanelTitleColor = $resultPanelTitleColor;
    }

    public function getResultPanelBackgroundColor(): string
    {
        return $this->resultPanelBackgroundColor;
    }

    public function setResultPanelBackgroundColor(string $resultPanelBackgroundColor): void
    {
        $this->resultPanelBackgroundColor = $resultPanelBackgroundColor;
    }

    public function getResultPanelTextColor(): string
    {
        return $this->resultPanelTextColor;
    }

    public function setResultPanelTextColor(string $resultPanelTextColor): void
    {
        $this->resultPanelTextColor = $resultPanelTextColor;
    }

    public function getResultDownloadBtnColor(): string
    {
        return $this->resultDownloadBtnColor;
    }

    public function setResultDownloadBtnColor(string $resultDownloadBtnColor): void
    {
        $this->resultDownloadBtnColor = $resultDownloadBtnColor;
    }

    public function getResultDownloadBtnHoverColor(): string
    {
        return $this->resultDownloadBtnHoverColor;
    }

    public function setResultDownloadBtnHoverColor(string $resultDownloadBtnHoverColor): void
    {
        $this->resultDownloadBtnHoverColor = $resultDownloadBtnHoverColor;
    }

    public function getResultDownloadBtnTextColor(): string
    {
        return $this->resultDownloadBtnTextColor;
    }

    public function setResultDownloadBtnTextColor(string $resultDownloadBtnTextColor): void
    {
        $this->resultDownloadBtnTextColor = $resultDownloadBtnTextColor;
    }

    public function getResultDatatableActionsBtnColor(): string
    {
        return $this->resultDatatableActionsBtnColor;
    }

    public function setResultDatatableActionsBtnColor(string $resultDatatableActionsBtnColor): void
    {
        $this->resultDatatableActionsBtnColor = $resultDatatableActionsBtnColor;
    }

    public function getResultDatatableActionsBtnHoverColor(): string
    {
        return $this->resultDatatableActionsBtnHoverColor;
    }

    public function setResultDatatableActionsBtnHoverColor(string $resultDatatableActionsBtnHoverColor): void
    {
        $this->resultDatatableActionsBtnHoverColor = $resultDatatableActionsBtnHoverColor;
    }

    public function getResultDatatableActionsBtnTextColor(): string
    {
        return $this->resultDatatableActionsBtnTextColor;
    }

    public function setResultDatatableActionsBtnTextColor(string $resultDatatableActionsBtnTextColor): void
    {
        $this->resultDatatableActionsBtnTextColor = $resultDatatableActionsBtnTextColor;
    }

    public function getResultDatatableBordered(): bool
    {
        return $this->resultDatatableBordered;
    }

    public function setResultDatatableBordered(bool $resultDatatableBordered): void
    {
        $this->resultDatatableBordered = $resultDatatableBordered;
    }

    public function getResultDatatableBorderedRadius(): bool
    {
        return $this->resultDatatableBorderedRadius;
    }

    public function setResultDatatableBorderedRadius(bool $resultDatatableBorderedRadius): void
    {
        $this->resultDatatableBorderedRadius = $resultDatatableBorderedRadius;
    }

    public function getResultDatatableBorderColor(): string
    {
        return $this->resultDatatableBorderColor;
    }

    public function setResultDatatableBorderColor(string $resultDatatableBorderColor): void
    {
        $this->resultDatatableBorderColor = $resultDatatableBorderColor;
    }

    public function getResultDatatableHeaderBackgroundColor(): string
    {
        return $this->resultDatatableHeaderBackgroundColor;
    }

    public function setResultDatatableHeaderBackgroundColor(string $resultDatatableHeaderBackgroundColor): void
    {
        $this->resultDatatableHeaderBackgroundColor = $resultDatatableHeaderBackgroundColor;
    }

    public function getResultDatatableHeaderTextColor(): string
    {
        return $this->resultDatatableHeaderTextColor;
    }

    public function setResultDatatableHeaderTextColor(string $resultDatatableHeaderTextColor): void
    {
        $this->resultDatatableHeaderTextColor = $resultDatatableHeaderTextColor;
    }

    public function getResultDatatableRowsBackgroundColor(): string
    {
        return $this->resultDatatableRowsBackgroundColor;
    }

    public function setResultDatatableRowsBackgroundColor(string $resultDatatableRowsBackgroundColor): void
    {
        $this->resultDatatableRowsBackgroundColor = $resultDatatableRowsBackgroundColor;
    }

    public function getResultDatatableRowsTextColor(): string
    {
        return $this->resultDatatableRowsTextColor;
    }

    public function setResultDatatableRowsTextColor(string $resultDatatableRowsTextColor): void
    {
        $this->resultDatatableRowsTextColor = $resultDatatableRowsTextColor;
    }

    public function getResultDatatableSortedColor(): string
    {
        return $this->resultDatatableSortedColor;
    }

    public function setResultDatatableSortedColor(string $resultDatatableSortedColor): void
    {
        $this->resultDatatableSortedColor = $resultDatatableSortedColor;
    }

    public function getResultDatatableSortedActiveColor(): string
    {
        return $this->resultDatatableSortedActiveColor;
    }

    public function setResultDatatableSortedActiveColor(string $resultDatatableSortedActiveColor): void
    {
        $this->resultDatatableSortedActiveColor = $resultDatatableSortedActiveColor;
    }

    public function getResultDatatableLinkColor(): string
    {
        return $this->resultDatatableLinkColor;
    }

    public function setResultDatatableLinkColor(string $resultDatatableLinkColor): void
    {
        $this->resultDatatableLinkColor = $resultDatatableLinkColor;
    }

    public function getResultDatatableLinkHoverColor(): string
    {
        return $this->resultDatatableLinkHoverColor;
    }

    public function setResultDatatableLinkHoverColor(string $resultDatatableLinkHoverColor): void
    {
        $this->resultDatatableLinkHoverColor = $resultDatatableLinkHoverColor;
    }

    public function getResultDatatableRowsSelectedColor(): string
    {
        return $this->resultDatatableRowsSelectedColor;
    }

    public function setResultDatatableRowsSelectedColor(string $resultDatatableRowsSelectedColor): void
    {
        $this->resultDatatableRowsSelectedColor = $resultDatatableRowsSelectedColor;
    }

    public function getResultDatatablePaginationLinkColor(): string
    {
        return $this->resultDatatablePaginationLinkColor;
    }

    public function setResultDatatablePaginationLinkColor(string $resultDatatablePaginationLinkColor): void
    {
        $this->resultDatatablePaginationLinkColor = $resultDatatablePaginationLinkColor;
    }

    public function getResultDatatablePaginationActiveBckColor(): string
    {
        return $this->resultDatatablePaginationActiveBckColor;
    }

    public function setResultDatatablePaginationActiveBckColor(string $resultDatatablePaginationActiveBckColor): void
    {
        $this->resultDatatablePaginationActiveBckColor = $resultDatatablePaginationActiveBckColor;
    }

    public function getResultDatatablePaginationActiveTextColor(): string
    {
        return $this->resultDatatablePaginationActiveTextColor;
    }

    public function setResultDatatablePaginationActiveTextColor(string $resultDatatablePaginationActiveTextColor): void
    {
        $this->resultDatatablePaginationActiveTextColor = $resultDatatablePaginationActiveTextColor;
    }

    public function getSampEnabled(): bool
    {
        return $this->sampEnabled;
    }

    public function setSampEnabled(bool $sampEnabled): void
    {
        $this->sampEnabled = $sampEnabled;
    }

    public function getBackToPortal(): bool
    {
        return $this->backToPortal;
    }

    public function setBackToPortal(bool $backToPortal): void
    {
        $this->backToPortal = $backToPortal;
    }

    public function getUserMenuEnabled(): bool
    {
        return $this->userMenuEnabled;
    }

    public function setUserMenuEnabled(bool $userMenuEnabled): void
    {
        $this->userMenuEnabled = $userMenuEnabled;
    }

    public function getSearchMultipleAllDatasetsSelected(): bool
    {
        return $this->searchMultipleAllDatasetsSelected;
    }

    public function setSearchMultipleAllDatasetsSelected(bool $searchMultipleAllDatasetsSelected): void
    {
        $this->searchMultipleAllDatasetsSelected = $searchMultipleAllDatasetsSelected;
    }

    public function getSearchMultipleProgressBarTitle(): string
    {
        return $this->searchMultipleProgressBarTitle;
    }

    public function setSearchMultipleProgressBarTitle(string $searchMultipleProgressBarTitle): void
    {
        $this->searchMultipleProgressBarTitle = $searchMultipleProgressBarTitle;
    }

    public function getSearchMultipleProgressBarSubtitle(): string
    {
        return $this->searchMultipleProgressBarSubtitle;
    }

    public function setSearchMultipleProgressBarSubtitle(string $searchMultipleProgressBarSubtitle): void
    {
        $this->searchMultipleProgressBarSubtitle = $searchMultipleProgressBarSubtitle;
    }

    public function getSearchMultipleProgressBarStepPosition(): string
    {
        return $this->searchMultipleProgressBarStepPosition;
    }

    public function setSearchMultipleProgressBarStepPosition(string $searchMultipleProgressBarStepPosition): void
    {
        $this->searchMultipleProgressBarStepPosition = $searchMultipleProgressBarStepPosition;
    }

    public function getSearchMultipleProgressBarStepDatasets(): string
    {
        return $this->searchMultipleProgressBarStepDatasets;
    }

    public function setSearchMultipleProgressBarStepDatasets(string $searchMultipleProgressBarStepDatasets): void
    {
        $this->searchMultipleProgressBarStepDatasets = $searchMultipleProgressBarStepDatasets;
    }

    public function getSearchMultipleProgressBarStepResult(): string
    {
        return $this->searchMultipleProgressBarStepResult;
    }

    public function setSearchMultipleProgressBarStepResult(string $searchMultipleProgressBarStepResult): void
    {
        $this->searchMultipleProgressBarStepResult = $searchMultipleProgressBarStepResult;
    }

    public function getInstance()
    {
        return $this->instance;
    }

    public function jsonSerialize(): array
    {
        return [
            'design_background_color' => $this->getDesignBackgroundColor(),
            'design_text_color' => $this->getDesignTextColor(),
            'design_font_family' => $this->getDesignFontFamily(),
            'design_link_color' => $this->getDesignLinkColor(),
            'design_link_hover_color' => $this->getDesignLinkHoverColor(),
            'design_logo' => $this->getDesignLogo(),
            'design_logo_href' => $this->getDesignLogoHref(),
            'design_favicon' => $this->getDesignFavicon(),
            'navbar_background_color' => $this->getNavbarBackgroundColor(),
            'navbar_border_bottom_color' => $this->getNavbarBorderBottomColor(),
            'navbar_color_href' => $this->getNavbarColorHref(),
            'navbar_font_family' => $this->getNavbarFontFamily(),
            'navbar_sign_in_btn_color' => $this->getNavbarSignInBtnColor(),
            'navbar_user_btn_color' => $this->getNavbarUserBtnColor(),
            'footer_background_color' => $this->getFooterBackgroundColor(),
            'footer_border_top_color' => $this->getFooterBorderTopColor(),
            'footer_text_color' => $this->getFooterTextColor(),
            'footer_logos' => $this->getFooterLogos(),
            'family_border_color' => $this->getFamilyBorderColor(),
            'family_header_background_color' => $this->getFamilyHeaderBackgroundColor(),
            'family_title_color' => $this->getFamilyTitleColor(),
            'family_title_bold' => $this->getFamilyTitleBold(),
            'family_background_color' => $this->getFamilyBackgroundColor(),
            'family_text_color' => $this->getFamilyTextColor(),
            'progress_bar_title' => $this->getProgressBarTitle(),
            'progress_bar_title_color' => $this->getProgressBarTitleColor(),
            'progress_bar_subtitle' => $this->getProgressBarSubtitle(),
            'progress_bar_subtitle_color' => $this->getProgressBarSubtitleColor(),
            'progress_bar_step_dataset_title' => $this->getProgressBarStepDatasetTitle(),
            'progress_bar_step_criteria_title' => $this->getProgressBarStepCriteriaTitle(),
            'progress_bar_step_output_title' => $this->getProgressBarStepOutputTitle(),
            'progress_bar_step_result_title' => $this->getProgressBarStepResultTitle(),
            'progress_bar_color' => $this->getProgressBarColor(),
            'progress_bar_active_color' => $this->getProgressBarActiveColor(),
            'progress_bar_circle_color' => $this->getProgressBarCircleColor(),
            'progress_bar_circle_icon_color' => $this->getProgressBarCircleIconColor(),
            'progress_bar_circle_icon_active_color' => $this->getProgressBarCircleIconActiveColor(),
            'progress_bar_text_color' => $this->getProgressBarTextColor(),
            'progress_bar_text_bold' => $this->getProgressBarTextBold(),
            'search_next_btn_color' => $this->getSearchNextBtnColor(),
            'search_next_btn_hover_color' => $this->getSearchNextBtnHoverColor(),
            'search_next_btn_hover_text_color' => $this->getSearchNextBtnHoverTextColor(),
            'search_back_btn_color' => $this->getSearchBackBtnColor(),
            'search_back_btn_hover_color' => $this->getSearchBackBtnHoverColor(),
            'search_back_btn_hover_text_color' => $this->getSearchBackBtnHoverTextColor(),
            'delete_current_query_btn_enabled' => $this->getDeleteCurrentQueryBtnEnabled(),
            'delete_current_query_btn_text' => $this->getDeleteCurrentQueryBtnText(),
            'delete_current_query_btn_color' => $this->getDeleteCurrentQueryBtnColor(),
            'delete_current_query_btn_hover_color' => $this->getDeleteCurrentQueryBtnHoverColor(),
            'delete_current_query_btn_hover_text_color' => $this->getDeleteCurrentQueryBtnHoverTextColor(),
            'delete_criterion_cross_color' => $this->getDeleteCriterionCrossColor(),
            'search_info_background_color' => $this->getSearchInfoBackgroundColor(),
            'search_info_text_color' => $this->getSearchInfoTextColor(),
            'search_info_help_enabled' => $this->getSearchInfoHelpEnabled(),
            'dataset_select_btn_color' => $this->getDatasetSelectBtnColor(),
            'dataset_select_btn_hover_color' => $this->getDatasetSelectBtnHoverColor(),
            'dataset_select_btn_hover_text_color' => $this->getDatasetSelectBtnHoverTextColor(),
            'dataset_selected_icon_color' => $this->getDatasetSelectedIconColor(),
            'search_criterion_background_color' => $this->getSearchCriterionBackgroundColor(),
            'search_criterion_text_color' => $this->getSearchCriterionTextColor(),
            'output_columns_selected_color' => $this->getOutputColumnsSelectedColor(),
            'output_columns_select_all_btn_color' => $this->getOutputColumnsSelectAllBtnColor(),
            'output_columns_select_all_btn_hover_color' => $this->getOutputColumnsSelectAllBtnHoverColor(),
            'output_columns_select_all_btn_hover_text_color' => $this->getOutputColumnsSelectAllBtnHoverTextColor(),
            'result_panel_border_size' => $this->getResultPanelBorderSize(),
            'result_panel_border_color' => $this->getResultPanelBorderColor(),
            'result_panel_title_color' => $this->getResultPanelTitleColor(),
            'result_panel_background_color' => $this->getResultPanelBackgroundColor(),
            'result_panel_text_color' => $this->getResultPanelTextColor(),
            'result_download_btn_color' => $this->getResultDownloadBtnColor(),
            'result_download_btn_hover_color' => $this->getResultDownloadBtnHoverColor(),
            'result_download_btn_text_color' => $this->getResultDownloadBtnTextColor(),
            'result_datatable_actions_btn_color' => $this->getResultDatatableActionsBtnColor(),
            'result_datatable_actions_btn_hover_color' => $this->getResultDatatableActionsBtnHoverColor(),
            'result_datatable_actions_btn_text_color' => $this->getResultDatatableActionsBtnTextColor(),
            'result_datatable_bordered' => $this->getResultDatatableBordered(),
            'result_datatable_bordered_radius' => $this->getResultDatatableBorderedRadius(),
            'result_datatable_border_color' => $this->getResultDatatableBorderColor(),
            'result_datatable_header_background_color' => $this->getResultDatatableHeaderBackgroundColor(),
            'result_datatable_header_text_color' => $this->getResultDatatableHeaderTextColor(),
            'result_datatable_rows_background_color' => $this->getResultDatatableRowsBackgroundColor(),
            'result_datatable_rows_text_color' => $this->getResultDatatableRowsTextColor(),
            'result_datatable_sorted_color' => $this->getResultDatatableSortedColor(),
            'result_datatable_sorted_active_color' => $this->getResultDatatableSortedActiveColor(),
            'result_datatable_link_color' => $this->getResultDatatableLinkColor(),
            'result_datatable_link_hover_color' => $this->getResultDatatableLinkHoverColor(),
            'result_datatable_rows_selected_color' => $this->getResultDatatableRowsSelectedColor(),
            'result_datatable_pagination_link_color' => $this->getResultDatatablePaginationLinkColor(),
            'result_datatable_pagination_active_bck_color' => $this->getResultDatatablePaginationActiveBckColor(),
            'result_datatable_pagination_active_text_color' => $this->getResultDatatablePaginationActiveTextColor(),
            'samp_enabled' => $this->getSampEnabled(),
            'back_to_portal' => $this->getBackToPortal(),
            'user_menu_enabled' => $this->getUserMenuEnabled(),
            'search_multiple_all_datasets_selected' => $this->getSearchMultipleAllDatasetsSelected(),
            'search_multiple_progress_bar_title' => $this->getSearchMultipleProgressBarTitle(),
            'search_multiple_progress_bar_subtitle' => $this->getSearchMultipleProgressBarSubtitle(),
            'search_multiple_progress_bar_step_position' => $this->getSearchMultipleProgressBarStepPosition(),
            'search_multiple_progress_bar_step_datasets' => $this->getSearchMultipleProgressBarStepDatasets(),
            'search_multiple_progress_bar_step_result' => $this->getSearchMultipleProgressBarStepResult()
        ];
    }
}
