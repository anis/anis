<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use JsonSerializable;

#[Entity, Table(name: 'menu_item')]
#[InheritanceType('JOINED')]
#[DiscriminatorColumn(name: 'type', type: 'string')]
#[DiscriminatorMap(['menu_family' => MenuFamily::class, 'webpage' => Webpage::class, 'url' => Url::class])]
abstract class MenuItem implements JsonSerializable
{
    #[Id, Column(type: 'string')]
    protected string $id;

    #[Column(type: 'string', nullable: false)]
    protected string $label;

    #[Column(type: 'string', nullable: true)]
    protected ?string $icon;

    #[Column(type: 'integer', nullable: false)]
    protected int $display;

    #[ManyToOne(targetEntity: MenuFamily::class, inversedBy: 'menuItems')]
    #[JoinColumn(name: 'menu_family', referencedColumnName: 'id', nullable: true, onDelete: 'CASCADE')]
    protected ?MenuFamily $menuFamily = null;

    #[ManyToOne(targetEntity: Instance::class, inversedBy: 'menuItems')]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'name', nullable: true, onDelete: 'CASCADE')]
    protected ?Instance $instance = null;

    public function __construct(Instance $instance, int $id, ?MenuFamily $menuFamily)
    {
        if (is_null($menuFamily)) {
            $this->instance = $instance;
            $this->id = $this->instance->getName() . '_' . $id;
        } else {
            $this->menuFamily = $menuFamily;
            $this->id = $this->menuFamily->getInstance()->getName() . '_' . $this->menuFamily->getId() . '_' . $id;
        }
    }

    public function getId(): int
    {
        if ($this->instance) {
            $prefixId = $this->getInstance()->getName();
        } else {
            $prefixId = $this->getMenuFamily()->getInstance()->getName() . '_' . $this->getMenuFamily()->getId();
        }

        return intval(substr(
            $this->id,
            strlen($prefixId) + 1
        ));
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function setIcon(?string $icon): void
    {
        $this->icon = $icon;
    }

    public function getDisplay(): int
    {
        return $this->display;
    }

    public function setDisplay(int $display): void
    {
        $this->display = $display;
    }

    public function getMenuFamily(): ?MenuFamily
    {
        return $this->menuFamily;
    }

    public function getInstance(): Instance
    {
        return $this->instance;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'icon' => $this->getIcon(),
            'display' => $this->getDisplay()
        ];
    }
}
