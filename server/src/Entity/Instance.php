<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\Common\Collections\Collection;

#[Entity, Table(name: 'instance')]
class Instance implements \JsonSerializable
{
    #[Id, Column(type: 'string')]
    private string $name;

    #[Column(type: 'string', nullable: false)]
    private string $label;

    #[Column(type: 'string', nullable: false)]
    private string $description;

    #[Column(name: 'scientific_manager', type: 'string', nullable: false)]
    private string $scientificManager;

    #[Column(type: 'string', nullable: false)]
    private string $instrument;

    #[Column(name:'wavelength_domain', type: 'string', nullable: false)]
    private string $wavelengthDomain;

    #[Column(type: 'integer', nullable: false)]
    private int $display;

    #[Column(name:'data_path', type: 'string', nullable: true)]
    private ?string $dataPath = null;

    #[Column(name:'files_path', type: 'string', nullable: true)]
    private ?string $filesPath = null;

    #[Column(name:'public', type: 'boolean', nullable: false)]
    private bool $public;

    #[Column(name:'portal_logo', type: 'string', nullable: true)]
    private ?string $portalLogo = null;

    #[Column(name:'portal_color', type: 'string', nullable: true)]
    private ?string $portalColor = null;

    #[Column(name:'default_redirect', type: 'string', nullable: true)]
    private ?string $defaultRedirect = null;

    #[OneToOne(targetEntity: DesignConfig::class, mappedBy: 'instance')]
    private ?DesignConfig $designConfig = null;

    #[OneToMany(targetEntity: DatasetFamily::class, mappedBy: 'instance')]
    private ?Collection $datasetFamilies = null;

    #[OneToMany(targetEntity: Dataset::class, mappedBy: 'instance')]
    private ?Collection $datasets = null;

    #[OneToMany(targetEntity: Database::class, mappedBy: 'instance')]
    private ?Collection $databases = null;

    #[OneToMany(targetEntity: InstanceGroup::class, mappedBy: 'instance')]
    private ?Collection $groups = null;

    #[OneToMany(targetEntity: MenuItem::class, mappedBy: 'instance')]
    private ?Collection $menuItems = null;

    public function __construct(string $name, string $label)
    {
        $this->name = $name;
        $this->label = $label;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getScientificManager(): string
    {
        return $this->scientificManager;
    }

    public function setScientificManager(string $scientificManager): void
    {
        $this->scientificManager = $scientificManager;
    }

    public function getInstrument(): string
    {
        return $this->instrument;
    }

    public function setInstrument(string $instrument): void
    {
        $this->instrument = $instrument;
    }

    public function getWavelengthDomain(): string
    {
        return $this->wavelengthDomain;
    }

    public function setWavelengthDomain(string $wavelengthDomain): void
    {
        $this->wavelengthDomain = $wavelengthDomain;
    }

    public function getDisplay(): int
    {
        return $this->display;
    }

    public function setDisplay(int $display): void
    {
        $this->display = $display;
    }

    public function getDataPath(): ?string
    {
        return $this->dataPath;
    }

    public function setDataPath(?string $dataPath): void
    {
        $this->dataPath = $dataPath;
    }

    public function getFilesPath(): ?string
    {
        return $this->filesPath;
    }

    public function setFilesPath(?string $filesPath): void
    {
        $this->filesPath = $filesPath;
    }

    public function getPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): void
    {
        $this->public = $public;
    }

    public function getPortalLogo(): ?string
    {
        return $this->portalLogo;
    }

    public function setPortalLogo(?string $portalLogo): void
    {
        $this->portalLogo = $portalLogo;
    }

    public function getPortalColor(): ?string
    {
        return $this->portalColor;
    }

    public function setPortalColor(?string $portalColor): void
    {
        $this->portalColor = $portalColor;
    }

    public function getDefaultRedirect(): ?string
    {
        return $this->defaultRedirect;
    }

    public function setDefaultRedirect(?string $defaultRedirect): void
    {
        $this->defaultRedirect = $defaultRedirect;
    }

    public function getDesignConfig(): ?DesignConfig
    {
        return $this->designConfig;
    }

    public function getDatasetFamilies(): ?Collection
    {
        return $this->datasetFamilies;
    }

    public function getDatasets(): ?Collection
    {
        return $this->datasets;
    }

    public function getDatabases(): ?Collection
    {
        return $this->databases;
    }

    public function getGroups(): ?Collection
    {
        return $this->groups;
    }

    public function getMenuItems(): ?Collection
    {
        return $this->menuItems;
    }

    public function jsonSerialize(): array
    {
        $groups = [];
        if (!is_null($this->getGroups())) {
            $groups = array_map(
                fn(InstanceGroup $group) => $group->jsonSerialize(),
                $this->getGroups()->toArray()
            );
        }

        $nbDatasetFamilies = 0;
        if (!is_null($this->getDatasetFamilies())) {
            $nbDatasetFamilies = count($this->getDatasetFamilies());
        }

        $nbDatasets = 0;
        if (!is_null($this->getDatasets())) {
            $nbDatasets = count($this->getDatasets());
        }

        return [
            'name' => $this->getName(),
            'label' => $this->getLabel(),
            'description' => $this->getDescription(),
            'scientific_manager' => $this->getScientificManager(),
            'instrument' => $this->getInstrument(),
            'wavelength_domain' => $this->getWavelengthDomain(),
            'display' => $this->getDisplay(),
            'data_path' => $this->getDataPath(),
            'files_path' => $this->getFilesPath(),
            'public' => $this->getPublic(),
            'portal_logo' => $this->getPortalLogo(),
            'portal_color' => $this->getPortalColor(),
            'default_redirect' => $this->getDefaultRedirect(),
            'groups' => $groups,
            'nb_dataset_families' => $nbDatasetFamilies,
            'nb_datasets' => $nbDatasets
        ];
    }
}
