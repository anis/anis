<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

enum ArchiveStatus: string
{
    case Created = 'created';
    case InProgress = 'in_progress';
    case Finished = 'finished';
    case Deleted = 'deleted';
}
