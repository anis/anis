<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

#[Entity, Table(name: 'dataset_family')]
class DatasetFamily implements \JsonSerializable
{
    #[Id]
    #[ManyToOne(targetEntity: Instance::class, inversedBy: 'datasetFamilies')]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'name', nullable: false, onDelete: 'CASCADE')]
    private Instance $instance;

    #[Id, Column(type: 'integer')]
    private int $id;

    #[Column(type: 'string', nullable: false)]
    private string $label;

    #[Column(type: 'integer', nullable: false)]
    private int $display;

    #[Column(type: 'boolean', nullable: false)]
    private bool $opened;

    public function __construct(Instance $instance, int $id)
    {
        $this->instance = $instance;
        $this->id = $id;
    }

    public function getInstance(): Instance
    {
        return $this->instance;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label)
    {
        $this->label = $label;
    }

    public function getDisplay(): int
    {
        return $this->display;
    }

    public function setDisplay(int $display): void
    {
        $this->display = $display;
    }

    public function getOpened(): bool
    {
        return $this->opened;
    }

    public function setOpened(bool $opened): void
    {
        $this->opened = $opened;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'display' => $this->getDisplay(),
            'opened' => $this->getOpened()
        ];
    }
}
