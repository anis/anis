<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\Common\Collections\Collection;

#[Entity, Table(name: 'menu_family')]
class MenuFamily extends MenuItem
{
    #[Column(type: 'string', nullable: false)]
    private string $name;

    #[OneToMany(targetEntity: MenuItem::class, mappedBy: 'menuFamily')]
    private ?Collection $menuItems = null;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getMenuItems(): ?Collection
    {
        return $this->menuItems;
    }

    public function jsonSerialize(): array
    {
        $items = [];
        if (!is_null($this->getMenuItems())) {
            $items = array_map(
                fn(MenuItem $menuItem) => $menuItem->jsonSerialize(),
                $this->getMenuItems()->toArray()
            );
        }

        return array_merge(parent::jsonSerialize(), [
            'name' => $this->getName(),
            'type' => 'menu_family',
            'items' => $items
        ]);
    }
}
