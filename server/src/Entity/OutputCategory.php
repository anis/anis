<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

#[Entity, Table(name: 'output_category')]
class OutputCategory implements \JsonSerializable
{
    #[Id, Column(type: 'string')]
    private string $id;

    #[Column(type: 'string', nullable: false)]
    private string $label;

    #[Column(type: 'integer', nullable: false)]
    private int $display;

    #[ManyToOne(targetEntity: OutputFamily::class, inversedBy: 'outputCategories')]
    #[JoinColumn(name: 'output_family', referencedColumnName: 'id', nullable: false, onDelete: 'CASCADE')]
    private OutputFamily $outputFamily;

    public function __construct(OutputFamily $outputFamily, int $id)
    {
        $this->outputFamily = $outputFamily;
        $this->id = $this->outputFamily->getPrimaryKey() . '_' . $id;
    }

    public function getId(): int
    {
        return intval(substr($this->id, strlen($this->outputFamily->getPrimaryKey()) + 1));
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getDisplay(): int
    {
        return $this->display;
    }

    public function setDisplay(int $display): void
    {
        $this->display = $display;
    }

    public function getOutputFamily(): OutputFamily
    {
        return $this->outputFamily;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'display' => $this->getDisplay()
        ];
    }
}
