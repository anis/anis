<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\JoinColumn;

#[Entity, Table(name: 'alias_config')]
class AliasConfig implements \JsonSerializable
{
    #[Id, Column(type: 'string')]
    private string $id;

    #[OneToOne(targetEntity: Dataset::class, inversedBy: 'aliasConfig')]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'instance_name', nullable: false, onDelete: 'CASCADE')]
    #[JoinColumn(name: 'dataset_name', referencedColumnName: 'name', nullable: false, onDelete: 'CASCADE')]
    private Dataset $datasetAliasConfig;

    #[Column(name: 'table_alias', type: 'string', nullable: false)]
    private string $tableAlias;

    #[Column(name: 'column_alias', type: 'string', nullable: false)]
    private string $columnAlias;

    #[Column(name: 'column_name', type: 'string', nullable: false)]
    private string $columnName;

    #[Column(name: 'column_alias_long', type: 'string', nullable: false)]
    private string $columnAliasLong;

    public function __construct(Dataset $datasetAliasConfig)
    {
        $this->id = $datasetAliasConfig->getInstance()->getName() . '_' . $datasetAliasConfig->getName();
        $this->datasetAliasConfig = $datasetAliasConfig;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTableAlias(): string
    {
        return $this->tableAlias;
    }

    public function setTableAlias(string $tableAlias): void
    {
        $this->tableAlias = $tableAlias;
    }

    public function getColumnAlias(): string
    {
        return $this->columnAlias;
    }

    public function setColumnAlias(string $columnAlias): void
    {
        $this->columnAlias = $columnAlias;
    }

    public function getColumnName(): string
    {
        return $this->columnName;
    }

    public function setColumnName(string $columnName): void
    {
        $this->columnName = $columnName;
    }

    public function getColumnAliasLong(): string
    {
        return $this->columnAliasLong;
    }

    public function setColumnAliasLong(string $columnAliasLong): void
    {
        $this->columnAliasLong = $columnAliasLong;
    }

    public function getDataset()
    {
        return $this->datasetAliasConfig;
    }

    public function jsonSerialize(): array
    {
        return [
            'table_alias' => $this->getTableAlias(),
            'column_alias' => $this->getColumnAlias(),
            'column_name' => $this->getColumnName(),
            'column_alias_long' => $this->getColumnAliasLong()
        ];
    }
}
