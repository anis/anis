<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\JoinColumn;

#[Entity, Table(name: 'detail_config')]
class DetailConfig implements \JsonSerializable
{
    #[Id, Column(type: 'string')]
    private string $id;

    #[OneToOne(targetEntity: Dataset::class, inversedBy: 'detailConfig')]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'instance_name', nullable: false, onDelete: 'CASCADE')]
    #[JoinColumn(name: 'dataset_name', referencedColumnName: 'name', nullable: false, onDelete: 'CASCADE')]
    private Dataset $datasetDetailConfig;

    #[Column(type: 'text', nullable: false)]
    private string $content;

    #[Column(name: 'style_sheet', type: 'text', nullable: true)]
    private ?string $styleSheet = null;

    public function __construct(Dataset $datasetDetailConfig)
    {
        $this->id = $datasetDetailConfig->getInstance()->getName() . '_' . $datasetDetailConfig->getName();
        $this->datasetDetailConfig = $datasetDetailConfig;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getStyleSheet(): ?string
    {
        return $this->styleSheet;
    }

    public function setStyleSheet(?string $styleSheet): void
    {
        $this->styleSheet = $styleSheet;
    }

    public function getDataset()
    {
        return $this->datasetDetailConfig;
    }

    public function jsonSerialize(): array
    {
        return [
            'content' => $this->getContent(),
            'style_sheet' => $this->getStyleSheet()
        ];
    }
}
