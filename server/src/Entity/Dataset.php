<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\Common\Collections\Collection;

#[Entity, Table(name: 'dataset')]
class Dataset implements \JsonSerializable
{
    #[Id]
    #[ManyToOne(targetEntity: Instance::class, inversedBy: 'datasets')]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'name', nullable: false, onDelete: 'CASCADE')]
    private Instance $instance;

    #[Id, Column(type: 'string')]
    private string $name;

    #[Column(name: 'table_ref', type: 'string', nullable: false)]
    private string $tableRef;

    #[Column(name: 'primary_key', type: 'integer', nullable: false)]
    private int $primaryKey;

    #[Column(name: 'default_order_by', type: 'integer', nullable: false)]
    private int $defaultOrderBy;

    #[Column(name: 'default_order_by_direction', type: 'string', nullable: false)]
    private string $defaultOrderByDirection;

    #[Column(type: 'string', nullable: false)]
    private string $label;

    #[Column(type: 'string', nullable: false)]
    private string $description;

    #[Column(type: 'integer', nullable: false)]
    private int $display;

    #[Column(name: 'data_path', type: 'string', nullable: true)]
    private ?string $dataPath = null;

    #[Column(type: 'boolean', nullable: false)]
    private bool $public;

    #[Column(name: 'download_json', type: 'boolean', nullable: false)]
    private bool $downloadJson;

    #[Column(name: 'download_csv', type: 'boolean', nullable: false)]
    private bool $downloadCsv;

    #[Column(name: 'download_ascii', type: 'boolean', nullable: false)]
    private bool $downloadAscii;

    #[Column(name: 'download_vo', type: 'boolean', nullable: false)]
    private bool $downloadVo;

    #[Column(name: 'download_fits', type: 'boolean', nullable: false)]
    private bool $downloadFits;

    #[Column(name: 'server_link_enabled', type: 'boolean', nullable: false)]
    private bool $serverLinkEnabled;

    #[Column(name: 'datatable_enabled', type: 'boolean', nullable: false)]
    private bool $datatableEnabled;

    #[Column(name: 'datatable_selectable_rows', type: 'boolean', nullable: false)]
    private bool $datatableSelectableRows;

    #[ManyToOne(targetEntity: Database::class)]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'instance_name', nullable: false, onDelete: 'CASCADE')]
    #[JoinColumn(name: 'id_database', referencedColumnName: 'id', nullable: false, onDelete: 'CASCADE')]
    private Database $database;

    #[ManyToOne(targetEntity: DatasetFamily::class)]
    #[JoinColumn(name: 'instance_name', referencedColumnName: 'instance_name', nullable: false, onDelete: 'CASCADE')]
    #[JoinColumn(name: 'id_dataset_family', referencedColumnName: 'id', nullable: false, onDelete: 'CASCADE')]
    private DatasetFamily $datasetFamily;

    #[OneToMany(targetEntity: Attribute::class, mappedBy: 'dataset')]
    private ?Collection $attributes = null;

    #[OneToMany(targetEntity: File::class, mappedBy: 'dataset')]
    private ?Collection $files = null;

    #[OneToMany(targetEntity: Image::class, mappedBy: 'dataset')]
    private ?Collection $images = null;

    #[OneToMany(targetEntity: CriteriaFamily::class, mappedBy: 'dataset')]
    private ?Collection $criteriaFamilies = null;

    #[OneToMany(targetEntity: OutputFamily::class, mappedBy: 'dataset')]
    private ?Collection $outputFamilies = null;

    #[OneToMany(targetEntity: DatasetGroup::class, mappedBy: 'dataset')]
    private ?Collection $groups = null;

    #[OneToOne(targetEntity: ConeSearchConfig::class, mappedBy: 'datasetConeSearchConfig')]
    private ?ConeSearchConfig $coneSearchConfig = null;

    #[OneToOne(targetEntity: AliasConfig::class, mappedBy: 'datasetAliasConfig')]
    private ?AliasConfig $aliasConfig = null;

    #[OneToOne(targetEntity: DetailConfig::class, mappedBy: 'datasetDetailConfig')]
    private ?DetailConfig $detailConfig = null;

    public function __construct(Instance $instance, string $name)
    {
        $this->instance = $instance;
        $this->name = $name;
    }

    public function getInstance(): Instance
    {
        return $this->instance;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getTableRef(): string
    {
        return $this->tableRef;
    }

    public function setTableRef(string $tableRef): void
    {
        $this->tableRef = $tableRef;
    }

    public function getPrimaryKey(): int
    {
        return $this->primaryKey;
    }

    public function setPrimaryKey(int $primaryKey): void
    {
        $this->primaryKey = $primaryKey;
    }

    public function getDefaultOrderBy(): int
    {
        return $this->defaultOrderBy;
    }

    public function setDefaultOrderBy(int $defaultOrderBy): void
    {
        $this->defaultOrderBy = $defaultOrderBy;
    }

    public function getDefaultOrderByDirection(): string
    {
        return $this->defaultOrderByDirection;
    }

    public function setDefaultOrderByDirection(string $defaultOrderByDirection): void
    {
        $this->defaultOrderByDirection = $defaultOrderByDirection;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getDisplay(): int
    {
        return $this->display;
    }

    public function setDisplay(int $display): void
    {
        $this->display = $display;
    }

    public function getDataPath(): ?string
    {
        return $this->dataPath;
    }

    public function setDataPath(?string $dataPath): void
    {
        $this->dataPath = $dataPath;
    }

    public function getPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): void
    {
        $this->public = $public;
    }

    public function getDownloadJson(): bool
    {
        return $this->downloadJson;
    }

    public function setDownloadJson(bool $downloadJson): void
    {
        $this->downloadJson = $downloadJson;
    }

    public function getDownloadCsv(): bool
    {
        return $this->downloadCsv;
    }

    public function setDownloadCsv(bool $downloadCsv): void
    {
        $this->downloadCsv = $downloadCsv;
    }

    public function getDownloadAscii(): bool
    {
        return $this->downloadAscii;
    }

    public function setDownloadAscii(bool $downloadAscii): void
    {
        $this->downloadAscii = $downloadAscii;
    }

    public function getDownloadVo(): bool
    {
        return $this->downloadVo;
    }

    public function setDownloadVo(bool $downloadVo): void
    {
        $this->downloadVo = $downloadVo;
    }

    public function getDownloadFits(): bool
    {
        return $this->downloadFits;
    }

    public function setDownloadFits(bool $downloadFits): void
    {
        $this->downloadFits = $downloadFits;
    }

    public function getServerLinkEnabled(): bool
    {
        return $this->serverLinkEnabled;
    }

    public function setServerLinkEnabled(bool $serverLinkEnabled): void
    {
        $this->serverLinkEnabled = $serverLinkEnabled;
    }

    public function getDatatableEnabled(): bool
    {
        return $this->datatableEnabled;
    }

    public function setDatatableEnabled(bool $datatableEnabled): void
    {
        $this->datatableEnabled = $datatableEnabled;
    }

    public function getDatatableSelectableRows(): bool
    {
        return $this->datatableSelectableRows;
    }

    public function setDatatableSelectableRows(bool $datatableSelectableRows): void
    {
        $this->datatableSelectableRows = $datatableSelectableRows;
    }

    public function getDatabase(): Database
    {
        return $this->database;
    }

    public function setDatabase(Database $database): void
    {
        $this->database = $database;
    }

    public function getDatasetFamily(): DatasetFamily
    {
        return $this->datasetFamily;
    }

    public function setDatasetFamily(DatasetFamily $datasetFamily): void
    {
        $this->datasetFamily = $datasetFamily;
    }

    public function getFullDataPath(): string
    {
        return $this->getDatasetFamily()->getInstance()->getDataPath() . $this->getDataPath();
    }

    public function getConeSearchEnabled(): bool
    {
        $enabled = false;
        if (!is_null($this->getConeSearchConfig())) {
            $enabled = $this->getConeSearchConfig()->getEnabled();
        }
        return $enabled;
    }

    public function getAttributes(): ?Collection
    {
        return $this->attributes;
    }

    public function getFiles(): ?Collection
    {
        return $this->files;
    }

    public function getImages(): ?Collection
    {
        return $this->images;
    }

    public function getCriteriaFamilies(): ?Collection
    {
        return $this->criteriaFamilies;
    }

    public function getOutputFamilies(): ?Collection
    {
        return $this->outputFamilies;
    }

    public function getGroups(): ?Collection
    {
        return $this->groups;
    }

    public function getConeSearchConfig(): ?ConeSearchConfig
    {
        return $this->coneSearchConfig;
    }

    public function getAliasConfig(): ?AliasConfig
    {
        return $this->aliasConfig;
    }

    public function getDetailConfig(): ?DetailConfig
    {
        return $this->detailConfig;
    }

    public function jsonSerialize(): array
    {
        $groups = [];
        if (!is_null($this->getGroups())) {
            $groups = array_map(
                fn(DatasetGroup $group) => $group->jsonSerialize(),
                $this->getGroups()->toArray()
            );
        }

        return [
            'name' => $this->getName(),
            'table_ref' => $this->getTableRef(),
            'primary_key' => $this->getPrimaryKey(),
            'default_order_by' => $this->getDefaultOrderBy(),
            'default_order_by_direction' => $this->getDefaultOrderByDirection(),
            'label' => $this->getLabel(),
            'description' => $this->getDescription(),
            'display' => $this->getDisplay(),
            'data_path' => $this->getDataPath(),
            'public' => $this->getPublic(),
            'download_json' => $this->getDownloadJson(),
            'download_csv' => $this->getDownloadCsv(),
            'download_ascii' => $this->getDownloadAscii(),
            'download_vo' => $this->getDownloadVo(),
            'download_fits' => $this->getDownloadFits(),
            'server_link_enabled' => $this->getServerLinkEnabled(),
            'datatable_enabled' => $this->getDatatableEnabled(),
            'datatable_selectable_rows' => $this->getDatatableSelectableRows(),
            'id_database' => $this->getDatabase()->getId(),
            'id_dataset_family' => $this->getDatasetFamily()->getId(),
            'full_data_path' => $this->getFullDataPath(),
            'cone_search_enabled' => $this->getConeSearchEnabled(),
            'groups' => $groups
        ];
    }
}
