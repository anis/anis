<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Exception;

use Slim\Exception\HttpBadRequestException;

class InvalidFormException extends HttpBadRequestException
{
    protected array $errors;

    public function setErrors(array $errors): InvalidFormException
    {
        $this->errors = $errors;
        return $this;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}
