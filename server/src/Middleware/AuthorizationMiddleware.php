<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Middleware;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Server\MiddlewareInterface;
use Slim\Exception\HttpInternalServerErrorException;
use Slim\Exception\HttpUnauthorizedException;
use Firebase\JWT\JWT;
use Firebase\JWT\JWK;

/**
 * Middleware to handle Authorization request header (JWT)
 *
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Middleware
 */
final class AuthorizationMiddleware implements MiddlewareInterface
{
    /**
     * Auth system is enabled or not
     */
    private bool $tokenEnabled;

    /**
     * Url for jwt key sets
     */
    private string $jwksUrl;

    /**
     * Create the classe before call process to execute this middleware
     *
     * @param bool $tokenEnabled Auth system is enabled or not
     * @param string $jwksUrl Url for jwt key sets
     */
    public function __construct(bool $tokenEnabled, string $jwksUrl)
    {
        $this->tokenEnabled = $tokenEnabled;
        $this->jwksUrl = $jwksUrl;
    }

    /**
     * Try to validating and verifing the signature of the json web token
     * if Authorization header is present
     *
     * @param  ServerRequestInterface   $request  PSR-7 This object represents the HTTP request
     * @param  RequestHandlerInterface  $handler  PSR-15 request handler
     *
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (
            $request->getMethod() === OPTIONS
            || (!$request->hasHeader('Authorization') && !array_key_exists('token', $request->getQueryParams()))
            || !$this->tokenEnabled
        ) {
            return $handler->handle($request);
        }

        // Get token string from Authorizarion header
        if ($request->hasHeader('Authorization')) {
            $bearer = $request->getHeader('Authorization')[0];
        } else {
            $bearer = 'Bearer ' . $request->getQueryParams()['token'];
        }

        $data = explode(' ', $bearer);
        if ($data[0] !== 'Bearer') {
            throw new HttpUnauthorizedException(
                $request,
                'Authorization must contain a string with the following format -> Bearer JWT'
            );
        }

        $keySet = $this->getJwkKeySet();

        try {
            $token = JWT::decode($data[1], $keySet);
        } catch (\LogicException $e) {
            // errors having to do with environmental setup or malformed JWT Keys
            throw new HttpInternalServerErrorException(
                $request,
                $e->getMessage()
            );
        } catch (\UnexpectedValueException $e) {
            // errors having to do with JWT signature and claims
            throw new HttpUnauthorizedException(
                $request,
                $e->getMessage()
            );
        }

        return $handler->handle($request->withAttribute('token', $token));
    }

    /**
     * Returns public key information from the jwksUrl URL
     * If the information has already been loaded, then the data is retrieved from the cache
     *
     * @return array
     */
    private function getJwkKeySet(): array
    {
        if (!($jwks = apcu_fetch('jwks'))) {
            $jwks = file_get_contents($this->jwksUrl);
            apcu_store('jwks', $jwks);
        }

        $keys = JWK::parseKeySet(json_decode($jwks, true));
        return $keys;
    }
}
