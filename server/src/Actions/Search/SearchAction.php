<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Search;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Settings\SettingsInterface;
use App\Specification\Factory\IInputFilterFactory;
use App\Search\DBALConnectionFactory;
use App\Search\Query\AnisQueryBuilder;
use App\Search\Response\ResponseFactory;
use App\Actions\Instance\Dataset\DatasetTrait;
use App\Actions\AuthorizationTrait;
use App\Search\SearchException;
use App\Actions\Action;

/**
 * @author François Agneray <francois.agneray@lam.fr>
 * @package App\Action
 */
final class SearchAction extends Action
{
    use DatasetTrait;
    use AuthorizationTrait;

    private DBALConnectionFactory $dcf;

    private AnisQueryBuilder $anisQueryBuilder;

    private ResponseFactory $responseFactory;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $em,
        SettingsInterface $settings,
        IInputFilterFactory $inputFilterFactory,
        DBALConnectionFactory $dcf,
        AnisQueryBuilder $anisQueryBuilder,
        ResponseFactory $responseFactory
    ) {
        $this->dcf = $dcf;
        $this->anisQueryBuilder = $anisQueryBuilder;
        $this->responseFactory = $responseFactory;
        parent::__construct($logger, $em, $settings, $inputFilterFactory);
    }

    /**
     * `GET` This action returns the results of ANIS search
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        // Search the correct dataset with primary key
        $dataset = $this->getDataset();
        $instance = $dataset->getInstance();

        // Retrieve token settings
        $tokenSettings = $this->settings->get('token');

        // Retrieve token enabled
        $tokenEnabled = $tokenSettings['enabled'];

        // If instance is private and authorization enabled
        if (!$instance->getPublic() && $tokenEnabled) {
            $this->verifyInstanceAuthorization(
                $this->request,
                $this->resolveArg('name'),
                explode(',', $tokenSettings['admin_roles'])
            );
        }

        // If dataset is private and authorization enabled
        if (!$dataset->getPublic() && $tokenEnabled) {
            $this->verifyDatasetAuthorization(
                $this->request,
                $this->resolveArg('name'),
                $this->resolveArg('dname'),
                explode(',', $tokenSettings['admin_roles'])
            );
        }

        $queryParams = $this->request->getQueryParams();

        // The parameter "a" is mandatory
        if (!array_key_exists('a', $queryParams)) {
            throw new HttpBadRequestException(
                $this->request,
                'Param a is required for this request'
            );
        }

        try {
            // Configure the Anis Query Builder
            $connection = $this->dcf->create($dataset->getDatabase());
            $this->anisQueryBuilder->setDoctrineQueryBuilder($connection->createQueryBuilder());
            $this->anisQueryBuilder->setDatasetSelected($dataset);

            // Build the SQL request
            $this->anisQueryBuilder->build($queryParams);

            // Log SQL request for debug
            $this->logger->debug($this->anisQueryBuilder->getDoctrineQueryBuilder()->getSQL());

            // The parameter f is not mandatory and represents the output format
            // By default the format is JSON
            $format = (array_key_exists('f', $queryParams)) ? $queryParams['f'] : 'json';
            return $this->responseFactory->create($format)->getResponse($this->response, $this->anisQueryBuilder);
        } catch (SearchException $e) {
            throw new HttpBadRequestException(
                $this->request,
                $e->getMessage()
            );
        }
    }
}
