<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Search;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Connection;
use Slim\Exception\HttpBadRequestException;
use App\Settings\SettingsInterface;
use App\Specification\Factory\IInputFilterFactory;
use App\Search\DBALConnectionFactory;
use App\Actions\Instance\Dataset\DatasetTrait;
use App\Actions\AuthorizationTrait;
use App\Actions\Action;
use App\Entity\AliasConfig;

final class SearchAliasAction extends Action
{
    use DatasetTrait;
    use AuthorizationTrait;

    private DBALConnectionFactory $dcf;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $em,
        SettingsInterface $settings,
        IInputFilterFactory $inputFilterFactory,
        DBALConnectionFactory $dcf
    ) {
        $this->dcf = $dcf;
        parent::__construct($logger, $em, $settings, $inputFilterFactory);
    }

    /**
     * `GET` This action returns the results of ANIS alias search
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        // Search the correct dataset with primary key
        $dataset = $this->getDataset();
        $instance = $dataset->getInstance();

        // Retrieve token settings
        $tokenSettings = $this->settings->get('token');

        // Retrieve token enabled
        $tokenEnabled = $tokenSettings['enabled'];

        // If instance is private and authorization enabled
        if (!$instance->getPublic() && $tokenEnabled) {
            $this->verifyInstanceAuthorization(
                $this->request,
                $this->resolveArg('name'),
                explode(',', $tokenSettings['admin_roles'])
            );
        }

        // If dataset is private and authorization enabled
        if (!$dataset->getPublic() && $tokenEnabled) {
            $this->verifyDatasetAuthorization(
                $this->request,
                $this->resolveArg('name'),
                $this->resolveArg('dname'),
                explode(',', $tokenSettings['admin_roles'])
            );
        }

        if (is_null($dataset->getAliasConfig())) {
            throw new HttpBadRequestException(
                $this->request,
                'Alias configuration for the dataaset' . $this->resolveArg('name') . ' is not found'
            );
        }

        $connection = $this->dcf->create($dataset->getDatabase());
        $payload = $this->getAliases($connection, $dataset->getAliasConfig(), $this->resolveArg('alias'));

        return $this->respond($payload);
    }

    private function getAliases(Connection $connection, AliasConfig $aliasConfig, string $alias): array
    {
        $queryBuilder = $connection->createQueryBuilder();
        $queryBuilder
            ->select(
                'aliases.' . $aliasConfig->getColumnAlias() . ' as alias',
                'aliases.' . $aliasConfig->getColumnName() . ' as name',
                'aliases.' . $aliasConfig->getColumnAliasLong() . ' as alias_long'
            )
            ->from($aliasConfig->getTableAlias(), 'aliases')
            ->where($aliasConfig->getColumnAlias() . ' LIKE ?')
            ->setParameter(0, '%' . $alias . '%');
        return $queryBuilder->executeQuery()->fetchAllAssociative();
    }
}
