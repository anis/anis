<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Archive;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpForbiddenException;
use App\Actions\Action;
use App\Actions\Archive\ArchiveTrait;

final class ArchiveListByUserAction extends Action
{
    use ArchiveTrait;

    /**
     * `GET` Returns the list of archive files found for one user
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        // Retrieve token settings
        $tokenSettings = $this->settings->get('token');

        // Retrieve token enabled
        $tokenEnabled = $tokenSettings['enabled'];

        // Verify that user is already connected
        if (!$tokenEnabled) {
            throw new HttpForbiddenException(
                $this->request,
                'The user must be logged in to use this action'
            );
        }

        // Retrieve user email
        $token = $this->request->getAttribute('token');
        $email = $token->email;

        // Retrieve all archives for the current user
        $archives = $this->em->getRepository('App\Entity\Archive')->findBy(
            ['email' => $email],
            ['created' => 'DESC']
        );

        return $this->respond($archives);
    }
}
