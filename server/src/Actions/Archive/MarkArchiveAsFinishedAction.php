<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Archive;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\Criteria;
use PhpAmqpLib\Connection\AbstractConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use App\Settings\SettingsInterface;
use App\Specification\Factory\IInputFilterFactory;
use App\Actions\Action;
use App\Entity\Archive;
use App\Entity\ArchiveStatus;
use App\Actions\Archive\ArchiveTrait;
use App\Entity\Dataset;
use App\Entity\Instance;

final class MarkArchiveAsFinishedAction extends Action
{
    use ArchiveTrait;

    /**
     * Contains RabbitMQ connection socket
     */
    private AbstractConnection $rmq;

    /**
     * Contains controller to send email
     */
    private ?MailerInterface $mailer;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $em,
        SettingsInterface $settings,
        IInputFilterFactory $inputFilterFactory,
        AbstractConnection $rmq,
        ?MailerInterface $mailer
    ) {
        $this->rmq = $rmq;
        $this->mailer = $mailer;
        parent::__construct($logger, $em, $settings, $inputFilterFactory);
    }

    /**
     * `GET` This action mark an archive as finished
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        $queryParams = $this->request->getQueryParams();
        // The parameter "uniq_id" is mandatory
        if (!array_key_exists('uniq_id', $queryParams)) {
            throw new HttpBadRequestException(
                $this->request,
                'Param uniq_id is required for this request'
            );
        }

        // Search the correct archive with primary key
        $archive = $this->getArchive();

        if ($queryParams['uniq_id'] !== $archive->getUniqId()) {
            throw new HttpBadRequestException(
                $this->request,
                'Param uniq_id is is not correct for this archive'
            );
        }

        $filePath = $this->getFilePath($archive);
        $archive->setStatus(ArchiveStatus::Finished);
        $archive->setFileSize(strval(filesize($filePath)));
        $this->em->flush();

        // Send an email to warn the user if the user was connected
        if ($this->settings->get('mailer')['enabled'] && $archive->getEmail()) {
            $dataset = $archive->getDataset();
            $this->sendEmailToTheUser(
                $archive->getEmail(),
                $dataset->getInstance(),
                $dataset,
                $archive->getId()
            );
        }

        // Run clean archives process
        $this->cleanArchives();

        return $this->respond($archive);
    }

    private function getFilePath(Archive $archive): string
    {
        $dataPath = $this->settings->get('data_path');
        $archiveFolder = $this->settings->get('archive_folder');
        $archiveId = $archive->getId();

        $dataset = $archive->getDataset();
        $instance = $dataset->getInstance();

        // Construct file path
        if ($archive->getEmail()) {
            $userDirectory = $archive->getEmail();
        } else {
            $userDirectory = 'TEMP';
        }

        return $dataPath . $archiveFolder . '/' . $userDirectory . '/' . $instance->getName()
            . '/' . $dataset->getName() . '/' . $archiveId . '.zip';
    }

    private function sendEmailToTheUser(
        string $email,
        Instance $instance,
        Dataset $dataset,
        int $archiveId
    ): void {
        $email = (new Email())
            ->from('data@lam.fr')
            ->to($email)
            ->subject('[' . $instance->getLabel() . '] Your archive files is now available')
            ->text(
                'Dear user,' . PHP_EOL .
                PHP_EOL .
                'You have requested the generation of an archive files for the dataset ' . $dataset->getLabel() . '.' .
                PHP_EOL .
                'We are pleased to inform you that the archive is now available ' .
                'for download from the web interface in the archive list section with the ID : ' . $archiveId . '.' .
                PHP_EOL .
                PHP_EOL .
                'Go to archive list webpage to download your archive : ' .
                $this->getArchiveListUrl($instance->getName()) .
                PHP_EOL .
                'Please note that we only guarantee the availability of the archive for ' .
                $this->settings->get('archive_retention_period') . '.' .
                PHP_EOL .
                PHP_EOL .
                'Sincerely'
            );

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            $this->logger->error('Email could not be sent. Error: ' . $e->getMessage());
        }
    }

    private function getArchiveListUrl(string $instanceName): string
    {
        return $this->settings->get('site_url')
            . $this->settings->get('base_href')
            . $instanceName
            . '/user/archive-list';
    }

    private function cleanArchives(): void
    {
        // Select all archives that have exceeded the maximum time limit
        $queryBuilder = $this->em->getRepository('App\Entity\Archive')
            ->createQueryBuilder('archive');

        $archiveRetentionPeriod = $this->settings->get('archive_retention_period');
        $maxTime = new \DateTime($archiveRetentionPeriod, new \DateTimeZone('UTC'));
        $criteria = Criteria::create()
            ->where(Criteria::expr()->neq('status', 'deleted'))
            ->andWhere(Criteria::expr()->lte('created', $maxTime))
            ->orderBy(['created' => Criteria::DESC]);

        $queryBuilder->addCriteria($criteria);
        $archives = $queryBuilder->getQuery()->getResult();

        if (count($archives) > 0) {
            $filesToDelete = [];
            foreach ($archives as $archive) {
                $filePath = $this->getFilePath($archive);
                if (file_exists($filePath)) {
                    // The file exists, add it to the list of files to delete
                    $filesToDelete[] = [
                        'archive_id' => $archive->getId(),
                        'uniq_id' => $archive->getUniqId(),
                        'file_path' => $this->getFilePath($archive)
                    ];
                } else {
                    // The file no longer exists update with status finished
                    $archive->setStatus(ArchiveStatus::Deleted);
                }
            }

            // Update the database for completed archives
            $this->em->flush();

            // If there are files to delete, call clean archives
            if (count($filesToDelete) > 0) {
                // Publish message in the rmq archive queue
                $channel = $this->rmq->channel();
                $channel->queue_declare('clean_archives', false, false, false, false);
                $content = json_encode($filesToDelete);

                $msg = new AMQPMessage($content);
                $channel->basic_publish($msg, '', 'clean_archives');

                // Close rmq connection
                $connection = $channel->getConnection();
                $channel->close();
                $connection->close();
            }
        }
    }
}
