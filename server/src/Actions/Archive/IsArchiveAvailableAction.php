<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Archive;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\AuthorizationTrait;
use App\Actions\Action;
use App\Entity\ArchiveStatus;
use App\Actions\Archive\ArchiveTrait;

final class IsArchiveAvailableAction extends Action
{
    use AuthorizationTrait;
    use ArchiveTrait;

    /**
     * `GET` This action indicates if the archive file is found
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        // Search the correct archive with primary key
        $archive = $this->getArchive();

        // Search the correct instance and dataset with primary keys
        $dataset = $archive->getDataset();
        $instance = $dataset->getInstance();

        // Retrieve token settings
        $tokenSettings = $this->settings->get('token');

        // Retrieve token enabled
        $tokenEnabled = $tokenSettings['enabled'];

        // If instance is private and authorization enabled
        if (!$instance->getPublic() && $tokenEnabled) {
            $this->verifyInstanceAuthorization(
                $this->request,
                $instance->getName(),
                explode(',', $tokenSettings['admin_roles'])
            );
        }

        // If dataset is private and authorization enabled
        if (!$dataset->getPublic() && $tokenEnabled) {
            $this->verifyDatasetAuthorization(
                $this->request,
                $instance->getName(),
                $dataset->getName(),
                explode(',', $tokenSettings['admin_roles'])
            );
        }

        $isAvailable = false;
        if ($archive->getStatus() === ArchiveStatus::Finished) {
            $isAvailable = true;
        }

        return $this->respond([
            'archive_is_available' => $isAvailable
        ]);
    }
}
