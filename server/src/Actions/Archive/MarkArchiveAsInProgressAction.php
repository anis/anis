<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Archive;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use App\Actions\Action;
use App\Entity\ArchiveStatus;
use App\Actions\Archive\ArchiveTrait;

final class MarkArchiveAsInProgressAction extends Action
{
    use ArchiveTrait;

    /**
     * `GET` This action mark an archive as pending
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        $queryParams = $this->request->getQueryParams();
        // The parameter "uniq_id" is mandatory
        if (!array_key_exists('uniq_id', $queryParams)) {
            throw new HttpBadRequestException(
                $this->request,
                'Param uniq_id is required for this request'
            );
        }

        // Search the correct archive with primary key
        $archive = $this->getArchive();

        if ($queryParams['uniq_id'] !== $archive->getUniqId()) {
            throw new HttpBadRequestException(
                $this->request,
                'Param uniq_id is is not correct for this archive'
            );
        }

        $archive->setStatus(ArchiveStatus::InProgress);
        $this->em->flush();

        return $this->respond($archive);
    }
}
