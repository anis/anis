<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Archive;

use Slim\Exception\HttpNotFoundException;
use App\Entity\Archive;

trait ArchiveTrait
{
    /**
     * Returns the archive with the id sent in the URL
     *
     * @return Archive
     * @throws HttpNotFoundException
     */
    private function getArchive(): Archive
    {
        $archive = $this->em->find(Archive::class, $this->resolveArg('id'));
        if (!is_null($archive)) {
            return $archive;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Archive with id ' . $this->resolveArg('id') . ' is not found'
        );
    }
}
