<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Archive;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManagerInterface;
use PhpAmqpLib\Connection\AbstractConnection;
use PhpAmqpLib\Message\AMQPMessage;
use App\Settings\SettingsInterface;
use App\Specification\Factory\IInputFilterFactory;
use App\Actions\Instance\Dataset\DatasetTrait;
use App\Actions\AuthorizationTrait;
use App\Actions\Action;
use App\Entity\Dataset;
use App\Entity\Archive;
use App\Entity\ArchiveStatus;

final class StartTaskCreateArchiveAction extends Action
{
    use DatasetTrait;
    use AuthorizationTrait;

    /**
     * Contains RabbitMQ connection socket
     */
    private AbstractConnection $rmq;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $em,
        SettingsInterface $settings,
        IInputFilterFactory $inputFilterFactory,
        AbstractConnection $rmq
    ) {
        $this->rmq = $rmq;
        parent::__construct($logger, $em, $settings, $inputFilterFactory);
    }

    /**
     * `GET` Starts an asynchronous task, through rabbitmq, to build an archive
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        // Search the correct instance and dataset with primary keys
        $dataset = $this->getDataset();
        $instance = $dataset->getInstance();

        // Retrieve token settings
        $tokenSettings = $this->settings->get('token');

        // Retrieve token enabled
        $tokenEnabled = $tokenSettings['enabled'];

        // If instance is private and authorization enabled
        if (!$instance->getPublic() && $tokenEnabled) {
            $this->verifyInstanceAuthorization(
                $this->request,
                $this->resolveArg('name'),
                explode(',', $tokenSettings['admin_roles'])
            );
        }

        // If dataset is private and authorization enabled
        if (!$dataset->getPublic() && $tokenEnabled) {
            $this->verifyDatasetAuthorization(
                $this->request,
                $this->resolveArg('name'),
                $this->resolveArg('dname'),
                explode(',', $tokenSettings['admin_roles'])
            );
        }

        $queryParams = $this->request->getQueryParams();

        // The parameter "a" is mandatory
        if (!array_key_exists('a', $queryParams)) {
            throw new HttpBadRequestException(
                $this->request,
                'Param a is required for this request'
            );
        }

        // Archive creation date
        $archiveCreationDate = new \DateTime('now', new \DateTimeZone('UTC'));

        // Create the name of the future archive
        $archiveName = 'archive_' . $instance->getName() . '_' . $dataset->getName()
            . '_' . $archiveCreationDate->format('Y-m-d\TH:i:s') . '.zip';

        // Saves the archive to the database
        $archive = $this->postArchive($archiveName, $archiveCreationDate, $dataset);

        // If user is connected add email to archive
        $userEmail = $this->getUserEmail();
        if ($userEmail) {
            $archive->setEmail($userEmail);
            $this->em->flush();
        }

        // Publish message in the rmq archive queue
        $channel = $this->rmq->channel();
        $channel->queue_declare('archive', false, false, false, false);
        $content = json_encode([
            'archive_id' => $archive->getId(),
            'instance_name' => $instance->getName(),
            'dataset_name' => $dataset->getName(),
            'query' => $this->request->getUri()->getQuery(),
            'param_a' => $queryParams['a'],
            'token' => $this->getToken(),
            'user_email' => $userEmail,
            'uniq_id' => $archive->getUniqId()
        ]);
        $msg = new AMQPMessage($content);
        $channel->basic_publish($msg, '', 'archive');

        // Close rmq connection
        $connection = $channel->getConnection();
        $channel->close();
        $connection->close();

        // Just returns the future archive name
        return $this->respond($archive);
    }

    private function getToken(): ?string
    {
        $bearer = null;

        if ($this->request->hasHeader('Authorization')) {
            $bearer = $this->request->getHeader('Authorization')[0];
        } elseif (array_key_exists('token', $this->request->getQueryParams())) {
            $bearer = 'Bearer ' . $this->request->getQueryParams()['token'];
        }

        return $bearer;
    }

    /**
     * Add a new archive into the metamodel
     *
     * @return Archive
     * @param array $data Contains the values ​​of the new archive
     */
    private function postArchive(string $name, \Datetime $created, Dataset $dataset): Archive
    {
        $archive = new Archive(
            $name,
            $created,
            ArchiveStatus::Created,
            uniqid(),
            $dataset
        );

        $this->em->persist($archive);
        $this->em->flush();

        return $archive;
    }

    /**
     * Get user email if user is connected
     *
     * @return string User email
     */
    private function getUserEmail(): ?string
    {
        $token = $this->request->getAttribute('token');
        if ($token) {
            return $token->email;
        } else {
            return null;
        }
    }
}
