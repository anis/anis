<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Archive;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Psr7\Factory\StreamFactory;
use App\Actions\AuthorizationTrait;
use App\Actions\Action;
use App\Actions\Archive\ArchiveTrait;
use App\Entity\ArchiveStatus;

final class DownloadArchiveAction extends Action
{
    use ArchiveTrait;
    use AuthorizationTrait;

    /**
     * `GET` Returns the archive file found
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        // Search the correct archive with primary key
        $archive = $this->getArchive();

        // Search the correct instance and dataset with primary keys
        $dataset = $archive->getDataset();
        $instance = $dataset->getInstance();

        // Retrieve token settings
        $tokenSettings = $this->settings->get('token');

        // Retrieve token enabled
        $tokenEnabled = $tokenSettings['enabled'];

        // If instance is private and authorization enabled
        if (!$instance->getPublic() && $tokenEnabled) {
            $this->verifyInstanceAuthorization(
                $this->request,
                $instance->getName(),
                explode(',', $tokenSettings['admin_roles'])
            );
        }

        // If dataset is private and authorization enabled
        if (!$dataset->getPublic() && $tokenEnabled) {
            $this->verifyDatasetAuthorization(
                $this->request,
                $instance->getName(),
                $dataset->getName(),
                explode(',', $tokenSettings['admin_roles'])
            );
        }

        $dataPath = $this->settings->get('data_path');
        $archiveFolder = $this->settings->get('archive_folder');
        $archiveId = $this->resolveArg('id');

        if ($archive->getEmail()) {
            $userDirectory = $archive->getEmail();
        } else {
            $userDirectory = 'TEMP';
        }

        // Search the file
        $filePath = $dataPath . $archiveFolder . '/' . $userDirectory . '/' . $instance->getName()
            . '/' . $dataset->getName() . '/' . $archiveId . '.zip';

        // If the file not found 404
        if ($archive->getStatus() !== ArchiveStatus::Finished) {
            throw new HttpNotFoundException(
                $this->request,
                'Archive file with name ' . $archiveId . '.zip is not found'
            );
        }

        // If the file found so stream it
        $streamFactory = new StreamFactory();
        $stream = $streamFactory->createStreamFromFile($filePath, 'r');

        return $this->response->withBody($stream)
            ->withHeader('Content-Type', mime_content_type($filePath))
            ->withHeader('Content-Length', filesize($filePath));
    }
}
