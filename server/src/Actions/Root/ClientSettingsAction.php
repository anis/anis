<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Root;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;

final class ClientSettingsAction extends Action
{
    /**
     * `GET` This action returns the ANIS settings needed to start the web client
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        return $this->respond([
            'servicesUrl' => $this->settings->get('services_url'),
            'baseHref' =>  $this->settings->get('base_href'),
            'authenticationEnabled' => $this->settings->get('token')['enabled'],
            'mailerEnabled' => $this->settings->get('mailer')['enabled'],
            'ssoAuthUrl' => $this->settings->get('sso')['auth_url'],
            'ssoRealm' => $this->settings->get('sso')['realm'],
            'ssoClientId' => $this->settings->get('sso')['client_id'],
            'adminRoles' => $this->settings->get('token')['admin_roles'],
            'matomoEnabled' => $this->settings->get('matomo')['enabled'],
            'matomoSiteId' => $this->settings->get('matomo')['site_id'],
            'matomoTrackerUrl' => $this->settings->get('matomo')['tracker_url']
        ]);
    }
}
