<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Root;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;

final class RootAction extends Action
{
    /**
     * `GET` This action indicates that the service is responding
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }
        $this->em->getConnection()->connect();

        return $this->respond([
            'message' => 'it works!',
            'version' => '3.16.0'
        ]);
    }
}
