<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Actions\FileExplorerTrait;
use App\Actions\AuthorizationTrait;

final class InstanceFileExplorerAction extends Action
{
    use InstanceTrait;
    use FileExplorerTrait;
    use AuthorizationTrait;

    /**
     * `GET` Returns the list of files if path is a directory or stream file
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        // Search the correct instance with primary key
        $instance = $this->getInstance();

        // Retrieve token settings
        $tokenSettings = $this->settings->get('token');

        // Retrieve token enabled
        $tokenEnabled = $tokenSettings['enabled'];

        // If instance is private and authorization enabled
        if (!$instance->getPublic() && $tokenEnabled) {
            $this->verifyInstanceAuthorization(
                $this->request,
                $this->resolveArg('name'),
                explode(',', $tokenSettings['admin_roles'])
            );
        }

        $instanceRootPath = $this->settings->get('data_path') . $instance->getDataPath() . $instance->getFilesPath();
        return $this->explorePath($instanceRootPath);
    }
}
