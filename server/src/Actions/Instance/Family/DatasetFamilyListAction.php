<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Family;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Actions\Instance\InstanceTrait;
use App\Entity\DatasetFamily;
use App\Entity\Instance;

final class DatasetFamilyListAction extends Action
{
    use InstanceTrait;
    use DatasetFamilyTrait;

    /**
     * `GET`  Returns a list of all dataset family for a given instance
     * `POST` Add a new dataset family to a given instance
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        // Search the correct instance with primary key
        $instance = $this->getInstance();

        if ($this->request->getMethod() === GET) {
            $payload = $this->em->getRepository(DatasetFamily::class)->findBy(['instance' => $instance]);
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $data = $this->getValidatedData('dataset_family');
            $payload = $this->postDatasetFamily($data, $instance);
            $status = 201;
        }

        return $this->respond($payload, $status);
    }

    /**
     * Add a new dataset family for an instance
     *
     * @return DatasetFamily
     * @param array    $data Contains the values ​​of the new dataset family sent by the user
     * @param Instance $instance The instance for adding the dataset family
     */
    private function postDatasetFamily(array $data, Instance $instance): DatasetFamily
    {
        $datasetFamily = new DatasetFamily($instance, $data['id']);
        $this->hydrateDatasetFamily($datasetFamily, $data);

        $this->em->persist($datasetFamily);
        $this->em->flush();

        return $datasetFamily;
    }
}
