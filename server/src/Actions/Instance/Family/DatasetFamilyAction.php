<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Family;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Actions\Instance\InstanceTrait;
use App\Entity\DatasetFamily;

final class DatasetFamilyAction extends Action
{
    use InstanceTrait;
    use DatasetFamilyTrait;

    /**
     * `GET` Returns the dataset family found
     * `PUT` Full update the dataset family and returns the new version
     * `DELETE` Delete the dataset family found and return a confirmation message
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct dataset family with primary key
        $datasetFamily = $this->getDatasetFamily();

        if ($this->request->getMethod() === GET) {
            $payload = $datasetFamily;
        }

        if ($this->request->getMethod() === PUT) {
            $data = $this->getValidatedData('dataset_family');
            $this->editDatasetFamily($datasetFamily, $data);
            $payload = $datasetFamily;
        }

        if ($this->request->getMethod() === DELETE) {
            $id = $datasetFamily->getId();
            $this->em->remove($datasetFamily);
            $this->em->flush();
            $payload = ['message' => 'Dataset family with id ' . $id . ' is removed!'];
        }

        return $this->respond($payload);
    }

    /**
     * Update dataset family object with setters
     *
     * @param DatasetFamily $datasetFamily The dataset family to update
     * @param array         $data Contains the new values ​​of the dataset family sent by the user
     */
    private function editDatasetFamily(DatasetFamily $datasetFamily, array $data): void
    {
        $this->hydrateDatasetFamily($datasetFamily, $data);
        $this->em->flush();
    }
}
