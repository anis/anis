<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Family;

use Slim\Exception\HttpNotFoundException;
use App\Entity\DatasetFamily;

trait DatasetFamilyTrait
{
    /**
     * Returns the dataset family with the id sent in the URL
     *
     * @return DatasetFamily
     * @throws HttpNotFoundException
     */
    private function getDatasetFamily(): DatasetFamily
    {
        $datasetFamily = $this->em->find(DatasetFamily::class, [
            'instance' => $this->resolveArg('name'),
            'id' => $this->resolveArg('id')
        ]);
        if (!is_null($datasetFamily)) {
            return $datasetFamily;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Dataset family with id ' . $this->resolveArg('id') . ' is not found'
        );
    }

    /**
     * Set the dataset family object with the values sent by the user
     *
     * @param DatasetFamily $datasetFamily The dataset family object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateDatasetFamily(DatasetFamily $datasetFamily, array $data): void
    {
        $datasetFamily->setLabel($data['label']);
        $datasetFamily->setDisplay($data['display']);
        $datasetFamily->setOpened($data['opened']);
    }
}
