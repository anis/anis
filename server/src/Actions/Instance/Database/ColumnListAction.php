<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Database;

use Psr\Http\Message\ResponseInterface as Response;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\DBAL\Connection;
use App\Settings\SettingsInterface;
use App\Specification\Factory\IInputFilterFactory;
use App\Actions\Action;
use App\Actions\Instance\Database\DatabaseTrait;
use App\Search\DBALConnectionFactory;

final class ColumnListAction extends Action
{
    use DatabaseTrait;

    private array $doctrineTypeMapping = [
        'bigint'           => 'bigint',
        'bigserial'        => 'bigint',
        'bool'             => 'boolean',
        'boolean'          => 'boolean',
        'bpchar'           => 'string',
        'bytea'            => 'blob',
        'char'             => 'string',
        'date'             => 'date',
        'datetime'         => 'datetime',
        'decimal'          => 'decimal',
        'double'           => 'float',
        'double precision' => 'float',
        'float'            => 'float',
        'float4'           => 'float',
        'float8'           => 'float',
        'inet'             => 'string',
        'int'              => 'integer',
        'int2'             => 'smallint',
        'int4'             => 'integer',
        'int8'             => 'bigint',
        'integer'          => 'integer',
        'interval'         => 'string',
        'json'             => 'json',
        'jsonb'            => 'json',
        'money'            => 'decimal',
        'numeric'          => 'decimal',
        'serial'           => 'integer',
        'serial4'          => 'integer',
        'serial8'          => 'bigint',
        'real'             => 'float',
        'smallint'         => 'smallint',
        'text'             => 'text',
        'time'             => 'time',
        'timestamp'        => 'datetime',
        'timestamptz'      => 'datetimetz',
        'timetz'           => 'time',
        'tsvector'         => 'text',
        'uuid'             => 'guid',
        'varchar'          => 'string',
        'year'             => 'date',
        '_varchar'         => 'string',
    ];

    private $dcf;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $em,
        SettingsInterface $settings,
        IInputFilterFactory $inputFilterFactory,
        DBALConnectionFactory $dcf
    ) {
        $this->dcf = $dcf;
        parent::__construct($logger, $em, $settings, $inputFilterFactory);
    }

    /**
     * `GET` Returns a list of all columns available in a table from a database
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        $database = $this->getDatabase();
        $connection = $this->dcf->create($database);
        $tableName = $this->resolveArg('tname');

        $strategy = 'default';

        if (
            $connection->getDatabasePlatform()->getName() === 'postgresql'
            && $this->isPgView($connection, $tableName)
        ) {
            $strategy = 'pg_view';
        }

        if ($strategy === 'default') {
            $sm = $connection->createSchemaManager();
            $payload = $this->getColumns($sm, $this->resolveArg('tname'));
        } elseif ($strategy === 'pg_view') {
            $payload = $this->getPgViewColumns($connection, $this->resolveArg('tname'));
        }

        return $this->respond($payload);
    }

    /**
     * @return array
     * @param AbstractSchemaManager $sm Doctrine schema manager for the selected database
     * @param string $tableName The name of the table from which to retrieve the columns
     */
    private function getColumns(AbstractSchemaManager $sm, string $tableName): array
    {
        $columns = [];
        foreach ($sm->listTableColumns($tableName) as $column) {
            $columns[] = [
                'name' => $column->getName(),
                'type' => $column->getType()->getName()
            ];
        }
        return $columns;
    }

    /**
     * @return bool
     * @param Connection $connection DBAL connection to the business database
     * @param string $tableName The name to test to know if it is a view
     */
    private function isPgView(Connection $connection, string $tableName): bool
    {
        $isView = false;
        $result = $connection->fetchAllAssociative($this->getPgListViewsSQL());
        $i = 0;
        $count = count($result);
        if ($count > 0) {
            do {
                $isView = $result[$i]['viewname'] === $tableName;
                $i++;
            } while ($i < $count && !$isView);
        }

        return $isView;
    }

    /**
     * @return array
     * @param Connection $connection DBAL connection to the business database
     * @param string $tableName The name of the view from which to retrieve the columns
     */
    private function getPgViewColumns(Connection $connection, string $viewName): array
    {
        $columns = [];
        $sql = 'SELECT a.attname, t.typname
            FROM pg_class c
                INNER JOIN pg_attribute a ON a.attrelid = c.oid
                INNER JOIN pg_type t ON t.oid = a.atttypid
            WHERE c.relkind = \'v\'
            AND c.relname = \'' . $viewName . '\';';

        $result = $connection->fetchAllAssociative($sql);

        foreach ($result as $row) {
            $columns[] = [
                'name' => $row['attname'],
                'type' => $this->doctrineTypeMapping[$row['typname']]
            ];
        }

        return $columns;
    }
}
