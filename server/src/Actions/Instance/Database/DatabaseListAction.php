<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Database;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Actions\Instance\InstanceTrait;
use App\Entity\Database;
use App\Entity\Instance;

final class DatabaseListAction extends Action
{
    use InstanceTrait;
    use DatabaseTrait;

    /**
     * `GET`  Returns a list of all databases listed in the metamodel database
     * `POST` Add a new database
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        // Search the correct instance with primary key
        $instance = $this->getInstance();

        if ($this->request->getMethod() === GET) {
            $payload = $this->em->getRepository(Database::class)->findBy(
                ['instance' => $instance],
                ['id' => 'ASC']
            );
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $data = $this->getValidatedData('database');
            $payload = $this->postDatabase($data, $instance);
            $status = 201;
        }

        return $this->respond($payload, $status);
    }

    /**
     * Add a new database into the metamodel
     *
     * @return Database The new database created
     * @param array $data Contains the values ​​of the new database sent by the user
     * @param Instance $instance Contains the instance where to add the new dataset
     */
    private function postDatabase(array $data, Instance $instance): Database
    {
        $database = new Database($instance, $data['id']);
        $this->hydrateDatabase($database, $data);
        $this->em->persist($database);
        $this->em->flush();

        return $database;
    }
}
