<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Database;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Entity\Database;

final class DatabaseAction extends Action
{
    use DatabaseTrait;

    /**
     * `GET` Returns the database found
     * `PUT` Full update the database and returns the new version
     * `DELETE` Delete the database found and return a confirmation message
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct database with primary key
        $database = $this->getDatabase();

        if ($this->request->getMethod() === GET) {
            $payload = $database;
        }

        if ($this->request->getMethod() === PUT) {
            $data = $this->getValidatedData('database');
            $this->editDatabase($database, $data);
            $payload = $database;
        }

        if ($this->request->getMethod() === DELETE) {
            $id = $database->getId();
            $this->em->remove($database);
            $this->em->flush();
            $payload = ['message' => 'Database with id ' . $id . ' is removed!'];
        }

        return $this->respond($payload);
    }

    /**
     * Update database object with setters
     *
     * @param Database $database   The database to update
     * @param array    $data Contains the new values ​​of the database sent by the user
     */
    private function editDatabase(Database $database, array $data): void
    {
        $this->hydrateDatabase($database, $data);
        $this->em->flush();
    }
}
