<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Database;

use Psr\Http\Message\ResponseInterface as Response;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Connection;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Settings\SettingsInterface;
use App\Specification\Factory\IInputFilterFactory;
use App\Actions\Action;
use App\Search\DBALConnectionFactory;

final class TableListAction extends Action
{
    use DatabaseTrait;

    private DBALConnectionFactory $dcf;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $em,
        SettingsInterface $settings,
        IInputFilterFactory $inputFilterFactory,
        DBALConnectionFactory $dcf
    ) {
        $this->dcf = $dcf;
        parent::__construct($logger, $em, $settings, $inputFilterFactory);
    }

    /**
     * `GET` Returns a list of all tables and views available in the database
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        // Search the correct database with primary key
        $database = $this->getDatabase();

        $connection = $this->dcf->create($database);
        $sm = $connection->createSchemaManager();
        $payload = [...$this->getTables($sm), ...$this->getViews($sm, $connection)];

        return $this->respond($payload);
    }

    /**
     * @param AbstractSchemaManager $sm Doctrine schema manager for the selected database
     *
     * @return string[]
     */
    private function getTables(AbstractSchemaManager $sm): array
    {
        $tables = [];
        foreach ($sm->listTables() as $table) {
            $tables[] = $table->getName();
        }
        return $tables;
    }

    /**
     * @param AbstractSchemaManager $sm Doctrine schema manager for the selected database
     *
     * @return string[]
     */
    private function getViews(AbstractSchemaManager $sm, Connection $connection): array
    {
        $views = [];
        if ($connection->getDatabasePlatform()->getName() === 'postgresql') {
            $result = $connection->fetchAllAssociative($this->getPgListViewsSQL());
            foreach ($result as $row) {
                $views[] = $row['viewname'];
            }
        } else {
            foreach ($sm->listViews() as $view) {
                $views[] = $view->getName();
            }
        }
        return $views;
    }
}
