<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Database;

use Slim\Exception\HttpNotFoundException;
use App\Entity\Database;

trait DatabaseTrait
{
    /**
     * Returns the database with the ID sent in the URL
     *
     * @return Database
     * @throws HttpNotFoundException
     */
    private function getDatabase(): Database
    {
        $database = $this->em->find(Database::class, [
            'instance' => $this->resolveArg('name'),
            'id' => $this->resolveArg('id')
        ]);
        if (!is_null($database)) {
            return $database;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Database with id ' . $this->resolveArg('id') . ' is not found'
        );
    }

    /**
     * Set the database object with the values sent by the user
     *
     * @param Database $database The database object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateDatabase(Database $database, array $data): void
    {
        $database->setLabel($data['label']);
        $database->setDbName($data['dbname']);
        $database->setType($data['dbtype']);
        $database->setHost($data['dbhost']);
        $database->setPort($data['dbport']);
        $database->setLogin($data['dblogin']);
        $database->setPassword($data['dbpassword']);
        $database->setDsnFile($data['dbdsn_file']);
    }

    /**
     * Returns the Postgres SQL query to list the available views
     *
     * @return string
     */
    private function getPgListViewsSQL(): string
    {
        return 'SELECT quote_ident(table_name) AS viewname,
                       table_schema AS schemaname,
                       view_definition AS definition
                FROM   information_schema.views
                WHERE  table_schema NOT IN (\'pg_catalog\',\'information_schema\')';
    }
}
