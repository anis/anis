<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Group;

use App\Entity\DatasetGroup;

trait DatasetGroupTrait
{
    /**
     * Set the dataset group object with the values sent by the user
     *
     * @param DatasetGroup $datasetGroup The dataset group object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateDatasetGroup(DatasetGroup $datasetGroup, array $data): void
    {
    }
}
