<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Group;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use App\Actions\Action;
use App\Entity\DatasetGroup;

final class DatasetGroupAction extends Action
{
    use DatasetGroupTrait;

    /**
     * `GET` Returns the dataset group found
     * `PUT` Full update the dataset group and returns the new version
     * `DELETE` Delete the dataset group found and return a confirmation message
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct group with primary key
        $datasetGroup = $this->getDatasetGroup();

        if ($this->request->getMethod() === GET) {
            $payload = $datasetGroup;
        }

        if ($this->request->getMethod() === PUT) {
            $data = $this->getValidatedData('dataset_group');
            $this->editDatasetGroup($datasetGroup, $data);
            $payload = $datasetGroup;
        }

        if ($this->request->getMethod() === DELETE) {
            $role = $datasetGroup->getRole();
            $this->em->remove($datasetGroup);
            $this->em->flush();
            $payload = ['message' => 'Dataset group with role ' . $role . ' is removed!'];
        }

        return $this->respond($payload);
    }

    /**
     * Returns the dataset group with the role sent in the URL
     *
     * @return DatasetGroup
     * @throws HttpNotFoundException
     */
    private function getDatasetGroup(): DatasetGroup
    {
        $role = $this->resolveArg('name') . '_' . $this->resolveArg('dname') . '_' . $this->resolveArg('role');

        $datasetGroup = $this->em->find(DatasetGroup::class, $role);
        if (!is_null($datasetGroup)) {
            return $datasetGroup;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Dataset group with role ' . $this->resolveArg('role') . ' is not found'
        );
    }

    /**
     * Update dataset group object with setters
     *
     * @param DatasetGroup $datasetGroup  The dataset group to update
     * @param array        $data Contains the new values ​​of the dataset group sent by the user
     */
    private function editDatasetGroup(DatasetGroup $datasetGroup, array $data): void
    {
        $this->hydrateDatasetGroup($datasetGroup, $data);
        $this->em->flush();
    }
}
