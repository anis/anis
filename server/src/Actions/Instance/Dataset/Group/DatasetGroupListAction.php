<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Group;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Actions\Instance\Dataset\DatasetTrait;
use App\Entity\DatasetGroup;
use App\Entity\Dataset;

final class DatasetGroupListAction extends Action
{
    use DatasetTrait;
    use DatasetGroupTrait;

    /**
     * `GET`  Returns a list of all dataset groups listed in the metamodel database
     * `POST` Add a new dataset group
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        // Search the correct dataset with primary key
        $dataset = $this->getDataset();

        if ($this->request->getMethod() === GET) {
            $payload = $this->em->getRepository(DatasetGroup::class)->findBy(
                ['dataset' => $dataset]
            );

            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $data = $this->getValidatedData('dataset_group');
            $payload = $this->postDatasetGroup($data, $dataset);
            $status = 201;
        }

        return $this->respond($payload, $status);
    }

    /**
     * Add a new dataset group for an instance into the metamodel
     *
     * @return DatasetGroup The newly created group
     * @param array    $data Contains the values ​​of the new group sent by the user
     * @param Dataset $dataset Dataset in which the group will be added
     */
    private function postDatasetGroup(array $data, Dataset $dataset): DatasetGroup
    {
        $datasetGroup = new DatasetGroup($dataset, $data['role']);
        $this->hydrateDatasetGroup($datasetGroup, $data);

        $this->em->persist($datasetGroup);
        $this->em->flush();

        return $datasetGroup;
    }
}
