<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Actions\FileExplorerTrait;
use App\Actions\AuthorizationTrait;

final class DatasetFileExplorerAction extends Action
{
    use DatasetTrait;
    use FileExplorerTrait;
    use AuthorizationTrait;

    /**
     * `GET` Returns the list of files if path is a directory or stream file
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        // Search the correct dataset with primary key
        $dataset = $this->getDataset();
        $instance = $dataset->getDatasetFamily()->getInstance();

        // Retrieve token settings
        $tokenSettings = $this->settings->get('token');

        // Retrieve token enabled
        $tokenEnabled = $tokenSettings['enabled'];

        // If instance is private and authorization enabled
        if (!$instance->getPublic() && $tokenEnabled) {
            $this->verifyInstanceAuthorization(
                $this->request,
                $this->resolveArg('name'),
                explode(',', $tokenSettings['admin_roles'])
            );
        }

        // If dataset is private and authorization enabled
        if (!$dataset->getPublic() && $tokenEnabled) {
            $this->verifyDatasetAuthorization(
                $this->request,
                $this->resolveArg('name'),
                $this->resolveArg('dname'),
                explode(',', $tokenSettings['admin_roles'])
            );
        }

        $datasetRootPath = $this->settings->get('data_path') . $instance->getDataPath() . $dataset->getDataPath();
        return $this->explorePath($datasetRootPath);
    }
}
