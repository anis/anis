<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Output;

use Slim\Exception\HttpNotFoundException;
use App\Entity\OutputFamily;

trait OutputFamilyTrait
{
    /**
     * Returns the output family with the id sent in the URL
     *
     * @return OutputFamily
     * @throws HttpNotFoundException
     */
    private function getOutputFamily(): OutputFamily
    {
        $id = $this->resolveArg('name') . '_' . $this->resolveArg('dname') . '_' . $this->resolveArg('id');

        $outputFamily = $this->em->find(OutputFamily::class, $id);
        if (!is_null($outputFamily)) {
            return $outputFamily;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Output family with id ' . $this->resolveArg('id') . ' is not found'
        );
    }

    /**
     * Set the output family object with the values sent by the user
     *
     * @param OutputFamily $outputFamily The output family object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateOutputFamily(OutputFamily $outputFamily, array $data): void
    {
        $outputFamily->setLabel($data['label']);
        $outputFamily->setDisplay($data['display']);
        $outputFamily->setOpened($data['opened']);
    }
}
