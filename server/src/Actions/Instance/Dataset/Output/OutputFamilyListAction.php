<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Output;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Actions\Instance\Dataset\DatasetTrait;
use App\Entity\Dataset;
use App\Entity\OutputFamily;

final class OutputFamilyListAction extends Action
{
    use DatasetTrait;
    use OutputFamilyTrait;

    /**
     * `GET`  Returns a list of all output family for a given dataset
     * `POST` Add a new dataset output family to a given dataset
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        // Search the dataset by name (primary key)
        $dataset = $this->getDataset();

        if ($this->request->getMethod() === GET) {
            $payload = $this->em->getRepository(OutputFamily::class)->findBy(
                ['dataset' => $dataset],
                ['id' => 'ASC']
            );
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $data = $this->getValidatedData('output_family');
            $payload = $this->postOutputFamily($data, $dataset);
            $status = 201;
        }

        return $this->respond($payload, $status);
    }

    /**
     * @return OutputFamily
     * @param array $data Contains the values ​​of the new output family sent by the user
     * @param Dataset $dataset Dataset for adding the family
     */
    private function postOutputFamily(array $data, Dataset $dataset): OutputFamily
    {
        $outputFamily = new OutputFamily($dataset, $data['id']);
        $this->hydrateOutputFamily($outputFamily, $data);

        $this->em->persist($outputFamily);
        $this->em->flush();

        return $outputFamily;
    }
}
