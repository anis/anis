<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Output;

use App\Entity\OutputCategory;

trait OutputCategoryTrait
{
    /**
     * Set the output family object with the values sent by the user
     *
     * @param OutputCategory $outputCategory The output category object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateOutputCategory(
        OutputCategory $outputCategory,
        array $data
    ): void {
        $outputCategory->setLabel($data['label']);
        $outputCategory->setDisplay($data['display']);
    }
}
