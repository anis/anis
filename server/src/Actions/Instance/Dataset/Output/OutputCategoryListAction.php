<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Output;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Entity\OutputFamily;
use App\Entity\OutputCategory;

final class OutputCategoryListAction extends Action
{
    use OutputFamilyTrait;
    use OutputCategoryTrait;

    /**
     * `GET`  Returns a list of all output categories listed in the metamodel database
     * `POST` Add a new output category
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        // Search the output family by id (primary key)
        $outputFamily = $this->getOutputFamily();

        if ($this->request->getMethod() === GET) {
            $payload = $this->em->getRepository(OutputCategory::class)->findBy(
                ['outputFamily' => $outputFamily],
                ['id' => 'ASC']
            );
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $data = $this->getValidatedData('output_category');
            $payload = $this->postOutputCategory($data, $outputFamily);
            $status = 201;
        }

        return $this->respond($payload, $status);
    }

    /**
     * @return OutputCategory
     * @param array $data Contains the values ​​of the new output category sent by the user
     * @param OutputFamily $outputFamily  Output family for adding the output category
     */
    private function postOutputCategory(array $data, OutputFamily $outputFamily): OutputCategory
    {
        $outputCategory = new OutputCategory($outputFamily, $data['id']);
        $this->hydrateOutputCategory($outputCategory, $data);

        $this->em->persist($outputCategory);
        $this->em->flush();

        return $outputCategory;
    }
}
