<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Output;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use App\Actions\Action;
use App\Entity\OutputCategory;

final class OutputCategoryAction extends Action
{
    use OutputCategoryTrait;

    /**
     * `GET` Returns the output category found
     * `PUT` Full update the output category and returns the new version
     * `DELETE` Delete the output category found and return a confirmation message
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        $outputCategory = $this->getOutputCategory();

        if ($this->request->getMethod() === GET) {
            $payload = $outputCategory;
        }

        if ($this->request->getMethod() === PUT) {
            $data = $this->getValidatedData('output_category');
            $this->editOutputCategory($outputCategory, $data);
            $payload = $outputCategory;
        }

        if ($this->request->getMethod() === DELETE) {
            $id = $outputCategory->getId();
            $this->em->remove($outputCategory);
            $this->em->flush();
            $payload = ['message' => 'Output category with id ' . $id . ' is removed!'];
        }

        return $this->respond($payload);
    }

    /**
     * Returns the output category with the id sent in the URL
     *
     * @return OutputCategory
     * @throws HttpNotFoundException
     */
    private function getOutputCategory(): OutputCategory
    {
        $id = $this->resolveArg('name')
            . '_' . $this->resolveArg('dname')
            . '_' . $this->resolveArg('id')
            . '_' . $this->resolveArg('ocid');

        $outputCategory = $this->em->find(OutputCategory::class, $id);
        if (!is_null($outputCategory)) {
            return $outputCategory;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Output category with id ' . $this->resolveArg('id') . ' is not found'
        );
    }

    /**
     * Update output category object with setters
     *
     * @param OutputCategory $outputCategory The output category to update
     * @param array $data Contains the new values ​​of the output category sent by the user
     */
    private function editOutputCategory(
        OutputCategory $outputCategory,
        array $data
    ): void {
        $this->hydrateOutputCategory($outputCategory, $data);
        $this->em->flush();
    }
}
