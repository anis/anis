<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Output;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Entity\OutputFamily;

final class OutputFamilyAction extends Action
{
    use OutputFamilyTrait;

    /**
     * `GET` Returns the output family found
     * `PUT` Full update the output family and returns the new version
     * `DELETE` Delete the output family found and return a confirmation message
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        $outputFamily = $this->getOutputFamily();

        if ($this->request->getMethod() === GET) {
            $payload = $outputFamily;
        }

        if ($this->request->getMethod() === PUT) {
            $data = $this->getValidatedData('output_family');
            $this->editOutputFamily($outputFamily, $data);
            $payload = $outputFamily;
        }

        if ($this->request->getMethod() === DELETE) {
            $id = $outputFamily->getId();
            $this->em->remove($outputFamily);
            $this->em->flush();
            $payload = ['message' => 'Output family with id ' . $id . ' is removed!'];
        }

        return $this->respond($payload);
    }

    /**
     * Update output family object with setters
     *
     * @param OutputFamily $outputFamily The output family to update
     * @param array $data Contains the new values ​​of the output family sent by the user
     */
    private function editOutputFamily(OutputFamily $outputFamily, array $data): void
    {
        $this->hydrateOutputFamily($outputFamily, $data);
        $this->em->flush();
    }
}
