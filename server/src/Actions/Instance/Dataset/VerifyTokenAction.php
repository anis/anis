<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Instance\Dataset\DatasetTrait;
use App\Actions\AuthorizationTrait;
use App\Actions\Action;

final class VerifyTokenAction extends Action
{
    use DatasetTrait;
    use AuthorizationTrait;

    /**
     * `GET` This action check the authorization on dataset/instance access
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        // Search the correct dataset with primary key
        $dataset = $this->getDataset();
        $instance = $dataset->getInstance();

        // Retrieve token settings
        $tokenSettings = $this->settings->get('token');

        // Retrieve token enabled
        $tokenEnabled = $tokenSettings['enabled'];

        // If instance is private and authorization enabled
        if (!$instance->getPublic() && $tokenEnabled) {
            $this->verifyInstanceAuthorization(
                $this->request,
                $this->resolveArg('name'),
                explode(',', $tokenSettings['admin_roles'])
            );
        }

        // If dataset is private and authorization enabled
        if (!$dataset->getPublic() && $tokenEnabled) {
            $this->verifyDatasetAuthorization(
                $this->request,
                $this->resolveArg('name'),
                $this->resolveArg('dname'),
                explode(',', $tokenSettings['admin_roles'])
            );
        }

        return $this->respond([
            'message' => 'It is authorized',
        ]);
    }
}
