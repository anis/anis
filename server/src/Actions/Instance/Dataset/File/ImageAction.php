<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\File;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use App\Actions\Action;
use App\Entity\Image;

final class ImageAction extends Action
{
    use ImageTrait;

    /**
     * `GET` Returns the image found
     * `PUT` Full update the image and returns the new version
     * `DELETE` Delete the image found and return a confirmation message
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct image with primary key
        $image = $this->getImage();

        if ($this->request->getMethod() === GET) {
            $payload = $image;
        }

        if ($this->request->getMethod() === PUT) {
            $data = $this->getValidatedData('file');
            $this->editImage($image, $data);
            $payload = $image;
        }

        if ($this->request->getMethod() === DELETE) {
            $id = $image->getId();
            $this->em->remove($image);
            $this->em->flush();
            $payload = ['message' => 'Image with id ' . $id . ' is removed!'];
        }

        return $this->respond($payload);
    }

    /**
     * Returns the image with the id sent in the URL
     *
     * @return Image
     * @throws HttpNotFoundException
     */
    private function getImage(): Image
    {
        $id = $this->resolveArg('name') . '_' . $this->resolveArg('dname') . '_' . $this->resolveArg('id');

        $image = $this->em->find(Image::class, $id);
        if (!is_null($image)) {
            return $image;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Image with id ' . $this->resolveArg('id') . ' is not found'
        );
    }

    /**
     * Update image object with setters
     *
     * @param Image $image The image to update
     * @param array $data Contains the new values ​​of the image sent by the user
     */
    private function editImage(Image $image, array $data): void
    {
        $this->hydrateImage($image, $data);
        $this->em->flush();
    }
}
