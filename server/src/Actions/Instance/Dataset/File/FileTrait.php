<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\File;

use App\Entity\File;

trait FileTrait
{
    /**
     * Set the file object with the values sent by the user
     *
     * @param File $file The file object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateFile(File $file, array $data): void
    {
        $file->setLabel($data['label']);
        $file->setFilePath($data['file_path']);
        $file->setFileSize($data['file_size']);
        $file->setType($data['type']);
    }
}
