<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\File;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use App\Actions\Action;
use App\Entity\File;

final class FileAction extends Action
{
    use FileTrait;

    /**
     * `GET` Returns the file found
     * `PUT` Full update the file and returns the new version
     * `DELETE` Delete the file found and return a confirmation message
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct file with primary key
        $file = $this->getFile();

        if ($this->request->getMethod() === GET) {
            $payload = $file;
        }

        if ($this->request->getMethod() === PUT) {
            $data = $this->getValidatedData('file');
            $this->editFile($file, $data);
            $payload = $file;
        }

        if ($this->request->getMethod() === DELETE) {
            $id = $file->getId();
            $this->em->remove($file);
            $this->em->flush();
            $payload = ['message' => 'File with id ' . $id . ' is removed!'];
        }

        return $this->respond($payload);
    }

    /**
     * Returns the file with the id sent in the URL
     *
     * @return File
     * @throws HttpNotFoundException
     */
    private function getFile(): File
    {
        $id = $this->resolveArg('name') . '_' . $this->resolveArg('dname') . '_' . $this->resolveArg('id');

        $file = $this->em->find(File::class, $id);
        if (!is_null($file)) {
            return $file;
        }

        throw new HttpNotFoundException(
            $this->request,
            'File with id ' . $this->resolveArg('id') . ' is not found'
        );
    }

    /**
     * Update file object with setters
     *
     * @param File $file The file to update
     * @param array $data Contains the new values ​​of the file sent by the user
     */
    private function editFile(File $file, array $data): void
    {
        $this->hydrateFile($file, $data);
        $this->em->flush();
    }
}
