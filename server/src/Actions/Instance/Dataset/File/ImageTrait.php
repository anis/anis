<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\File;

use App\Entity\Image;

trait ImageTrait
{
    /**
     * Set the image object with the values sent by the user
     *
     * @param Image $image The image object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateImage(Image $image, array $data): void
    {
        $image->setLabel($data['label']);
        $image->setFilePath($data['file_path']);
        $image->setHduNumber($data['hdu_number']);
        $image->setFileSize($data['file_size']);
        $image->setRaMin($data['ra_min']);
        $image->setRaMax($data['ra_max']);
        $image->setDecMin($data['dec_min']);
        $image->setDecMax($data['dec_max']);
        $image->setStretch($data['stretch']);
        $image->setPmin($data['pmin']);
        $image->setPmax($data['pmax']);
    }
}
