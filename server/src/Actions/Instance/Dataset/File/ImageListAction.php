<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\File;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Actions\Instance\Dataset\DatasetTrait;
use App\Entity\Dataset;
use App\Entity\Image;

final class ImageListAction extends Action
{
    use DatasetTrait;
    use ImageTrait;

    /**
     * `GET`  Returns a list of all files for a given dataset
     * `POST` Add a new file to a given dataset
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        $dataset = $this->getDataset();

        if ($this->request->getMethod() === GET) {
            $payload = $this->em->getRepository(Image::class)->findBy(
                ['dataset' => $dataset],
                ['id' => 'ASC']
            );
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $data = $this->getValidatedData('image');
            $payload = $this->postImage($data, $dataset);
            $status = 201;
        }

        return $this->respond($payload, $status);
    }

    /**
     * @return Image
     * @param array $data Contains the values ​​of the new image sent by the user
     * @param Dataset $dataset Dataset for adding the image
     */
    private function postImage(array $data, Dataset $dataset): Image
    {
        $image = new Image($dataset, $data['id']);
        $this->hydrateImage($image, $data);

        $this->em->persist($image);
        $this->em->flush();

        return $image;
    }
}
