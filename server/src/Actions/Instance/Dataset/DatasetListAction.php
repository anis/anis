<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Actions\Instance\InstanceTrait;
use App\Entity\Instance;
use App\Entity\DatasetFamily;
use App\Entity\Database;
use App\Entity\Dataset;

final class DatasetListAction extends Action
{
    use InstanceTrait;
    use DatasetTrait;

    /**
     * `GET`  Returns a list of all datasets for a given dataset family
     * `POST` Add a new dataset
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        // Search the correct instance with primary key
        $instance = $this->getInstance();

        if ($this->request->getMethod() === GET) {
            $payload = $this->em->getRepository(Dataset::class)->findBy(
                ['instance' => $instance]
            );
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $data = $this->getValidatedData('dataset');
            $database = $this->em->find(Database::class, [
                'instance' => $instance,
                'id' => $data['id_database']
            ]);
            $datasetFamily = $this->em->find(DatasetFamily::class, [
                'instance' => $instance,
                'id' => $data['id_dataset_family']
            ]);
            $payload = $this->postDataset($data, $instance, $database, $datasetFamily);
            $status = 201;
        }

        return $this->respond($payload, $status);
    }

    /**
     * Add a new dataset into the metamodel
     *
     * @return Dataset
     * @param array $parsedBody Contains the values ​​of the new dataset sent by the user
     * @param Instance $instance Contains the instance where to add the new dataset
     * @param Database $database Contains the database doctrine object
     * @param DatasetFamily $datasetFamily Contains the dataset family doctrine object
     */
    private function postDataset(
        array $data,
        Instance $instance,
        Database $database,
        DatasetFamily $datasetFamily
    ): Dataset {
        $dataset = new Dataset($instance, $data['name']);
        $this->hydrateDataset($dataset, $data);
        $dataset->setDatabase($database);
        $dataset->setDatasetFamily($datasetFamily);

        $this->em->persist($dataset);
        $this->em->flush();

        return $dataset;
    }
}
