<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Attribute;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Settings\SettingsInterface;
use App\Specification\Factory\IInputFilterFactory;
use App\Search\DBALConnectionFactory;
use App\Actions\Action;

final class AttributeDistinctAction extends Action
{
    use AttributeTrait;

    private $dcf;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $em,
        SettingsInterface $settings,
        IInputFilterFactory $inputFilterFactory,
        DBALConnectionFactory $dcf
    ) {
        $this->dcf = $dcf;
        parent::__construct($logger, $em, $settings, $inputFilterFactory);
    }

    /**
     * `GET` Returns a list of distinct values found for this attribute into the business database
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        $attribute = $this->getAttribute();

        if ($this->request->getMethod() === GET) {
            // Create query builder with from clause using dataset information
            // With select, group by and order by clauses using attribute information
            $dataset = $attribute->getDataset();
            $column = $dataset->getTableRef() . '.' . $attribute->getName();
            $connection = $this->dcf->create($dataset->getDatabase());
            $queryBuilder = $connection->createQueryBuilder();
            $queryBuilder->select($column . ' as ' . $attribute->getLabel());
            $queryBuilder->from($dataset->getTableRef());
            $queryBuilder->groupBy($column);
            $queryBuilder->orderBy($column);

            // Execute query and returns response
            $stmt = $queryBuilder->executeQuery();

            $payload = array_filter($stmt->fetchFirstColumn(), function ($value) {
                return !is_null($value);
            });
        }

        return $this->respond($payload);
    }
}
