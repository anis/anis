<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Attribute;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Actions\Instance\Dataset\DatasetTrait;
use App\Entity\Dataset;
use App\Entity\CriteriaFamily;
use App\Entity\OutputCategory;
use App\Entity\Attribute;

final class AttributeListAction extends Action
{
    use DatasetTrait;
    use AttributeTrait;

    /**
     * `GET`  Returns a list of all attributes of a dataset listed in the metamodel database
     * `POST` Add a new attribute for a dataset
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        $dataset = $this->getDataset();

        if ($this->request->getMethod() === GET) {
            $payload = $this->em->getRepository(Attribute::class)->findBy(
                ['dataset' => $dataset],
                ['id' => 'ASC']
            );
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $data = $this->getValidatedData('attribute');
            $criteriaFamily = $this->getCriteriaFamily(
                $this->resolveArg('name'),
                $this->resolveArg('dname'),
                $data['id_criteria_family']
            );
            $outputCategory = $this->getOutputCategory(
                $this->resolveArg('name'),
                $this->resolveArg('dname'),
                $data['id_output_category']
            );
            $detailOutputCategory = $this->getOutputCategory(
                $this->resolveArg('name'),
                $this->resolveArg('dname'),
                $data['id_detail_output_category']
            );
            $payload = $this->postAttribute(
                $data,
                $dataset,
                $criteriaFamily,
                $outputCategory,
                $detailOutputCategory
            );
            $status = 201;
        }

        return $this->respond($payload, $status);
    }

    /**
     * Add a new attribute into the metamodel
     *
     * @return Attribute
     * @param array $data Contains the values ​​of the new attribute sent by the user
     * @param Dataset $dataset The attribute's dataset
     * @param CriteriaFamily|null $criteriaFamily
     * @param OutputCategory|null $outputCategory
     * @param OutputCategory|null $detailOutputCategory
     */
    private function postAttribute(
        array $data,
        Dataset $dataset,
        ?CriteriaFamily $criteriaFamily,
        ?OutputCategory $outputCategory,
        ?OutputCategory $detailOutputCategory
    ): Attribute {
        $attribute = new Attribute($dataset, $data['id']);
        $this->hydrateAttribute($attribute, $data, $criteriaFamily, $outputCategory, $detailOutputCategory);

        $this->em->persist($attribute);
        $this->em->flush();

        return $attribute;
    }
}
