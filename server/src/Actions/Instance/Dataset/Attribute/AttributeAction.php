<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Attribute;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Entity\CriteriaFamily;
use App\Entity\OutputCategory;
use App\Entity\Attribute;

final class AttributeAction extends Action
{
    use AttributeTrait;

    /**
     * `GET` Returns the attribute found
     * `PUT` Full update the attribute and returns the new version
     * `DELETE` Delete the attribute found and return a confirmation message
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        $attribute = $this->getAttribute();

        if ($this->request->getMethod() === GET) {
            $payload = $attribute;
        }

        if ($this->request->getMethod() === PUT) {
            $data = $this->getValidatedData('attribute');
            $criteriaFamily = $this->getCriteriaFamily(
                $this->resolveArg('name'),
                $this->resolveArg('dname'),
                $data['id_criteria_family']
            );
            $outputCategory = $this->getOutputCategory(
                $this->resolveArg('name'),
                $this->resolveArg('dname'),
                $data['id_output_category']
            );
            $detailOutputCategory = $this->getOutputCategory(
                $this->resolveArg('name'),
                $this->resolveArg('dname'),
                $data['id_detail_output_category']
            );
            $this->editAttribute(
                $attribute,
                $data,
                $criteriaFamily,
                $outputCategory,
                $detailOutputCategory
            );
            $payload = $attribute;
        }

        if ($this->request->getMethod() === DELETE) {
            $id = $attribute->getId();
            $this->em->remove($attribute);
            $this->em->flush();
            $payload = ['message' => 'Attribute with id ' . $id . ' is removed!'];
        }

        return $this->respond($payload);
    }

    /**
     * Update attribute object with setters
     *
     * @param Attribute $attribute The attribute to update
     * @param array $data Contains the new values ​​of the attribute sent by the user
     * @param CriteriaFamily|null $criteriaFamily
     * @param OutputCategory|null $outputCategory
     * @param OutputCategory|null $detailOutputCategory
     */
    private function editAttribute(
        Attribute $attribute,
        array $data,
        ?CriteriaFamily $criteriaFamily,
        ?OutputCategory $outputCategory,
        ?OutputCategory $detailOutputCategory
    ): void {
        $this->hydrateAttribute($attribute, $data, $criteriaFamily, $outputCategory, $detailOutputCategory);
        $this->em->flush();
    }
}
