<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Attribute;

use Slim\Exception\HttpNotFoundException;
use App\Entity\CriteriaFamily;
use App\Entity\OutputCategory;
use App\Entity\Attribute;

trait AttributeTrait
{
    /**
     * Returns the attribute with the id sent in the URL
     *
     * @return Attribute
     * @throws HttpNotFoundException
     */
    private function getAttribute(): Attribute
    {
        $id = $this->resolveArg('name') . '_' . $this->resolveArg('dname') . '_' . $this->resolveArg('id');

        $attribute = $this->em->find(Attribute::class, $id);
        if (!is_null($attribute)) {
            return $attribute;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Attribute with dataset name '
                . $this->resolveArg('dname')
                . ' and attribute id ' . $this->resolveArg('id') . ' is not found'
        );
    }

    /**
     * Returns the criteria family with the id sent in the form (id_criteria_family)
     *
     * @return CriteriaFamily|null
     * @param string $instanceName
     * @param string $datasetName
     * @param int|null $idCriteriaFamily
     */
    private function getCriteriaFamily(
        string $instanceName,
        string $datasetName,
        ?int $idCriteriaFamily
    ): ?CriteriaFamily {
        if (is_null($idCriteriaFamily)) {
            $criteriaFamily = null;
        } else {
            $criteriaFamily = $this->em->find(
                CriteriaFamily::class,
                $instanceName . '_' . $datasetName . '_' . $idCriteriaFamily
            );
        }
        return $criteriaFamily;
    }

    /**
     * Returns the output category with the id sent in the form (id_output_family + id_output_category)
     *
     * @return OutputCategory|null
     * @param string $instanceName
     * @param string $datasetName
     * @param string|null $idOutputCategory
     */
    private function getOutputCategory(
        string $instanceName,
        string $datasetName,
        ?string $idOutputCategory
    ): ?OutputCategory {
        if (is_null($idOutputCategory)) {
            $outputCategory = null;
        } else {
            $outputCategory = $this->em->find(
                OutputCategory::class,
                $instanceName
                    . '_' . $datasetName
                    . '_' . $idOutputCategory
            );
        }
        return $outputCategory;
    }

    /**
     * Set the attribute object with the values sent by the user
     *
     * @param Attribute $attribute The attribute object to be modified
     * @param array $data Data sent by the user
     * @param CriteriaFamily|null $criteriaFamily
     * @param OutputCategory|null $outputCategory
     * @param OutputCategory|null $detailOutputCategory
     */
    private function hydrateAttribute(
        Attribute $attribute,
        array $data,
        ?CriteriaFamily $criteriaFamily,
        ?OutputCategory $outputCategory,
        ?OutputCategory $detailOutputCategory
    ): void {
        $attribute->setName($data['name']);
        $attribute->setLabel($data['label']);
        $attribute->setFormLabel($data['form_label']);
        $attribute->setDatatableLabel($data['datatable_label']);
        $attribute->setDescription($data['description']);
        $attribute->setType($data['type']);
        $attribute->setSearchType($data['search_type']);
        $attribute->setOperator($data['operator']);
        $attribute->setDynamicOperator($data['dynamic_operator']);
        $attribute->setNullOperatorsEnabled($data['null_operators_enabled']);
        $attribute->setMin($data['min']);
        $attribute->setMax($data['max']);
        $attribute->setOptions($data['options']);
        $attribute->setPlaceholderMin($data['placeholder_min']);
        $attribute->setPlaceholderMax($data['placeholder_max']);
        $attribute->setCriteriaDisplay($data['criteria_display']);
        $attribute->setOutputDisplay($data['output_display']);
        $attribute->setSelected($data['selected']);
        $attribute->setRenderer($data['renderer']);
        $attribute->setRendererConfig($data['renderer_config']);
        $attribute->setOrderBy($data['order_by']);
        $attribute->setArchive($data['archive']);
        $attribute->setDetailDisplay($data['detail_display']);
        $attribute->setDetailRenderer($data['detail_renderer']);
        $attribute->setDetailRendererConfig($data['detail_renderer_config']);
        $attribute->setVoUtype($data['vo_utype']);
        $attribute->setVoUcd($data['vo_ucd']);
        $attribute->setVoUnit($data['vo_unit']);
        $attribute->setVoDescription($data['vo_description']);
        $attribute->setVoDatatype($data['vo_datatype']);
        $attribute->setVoSize($data['vo_size']);
        $attribute->setCriteriaFamily($criteriaFamily);
        $attribute->setOutputCategory($outputCategory);
        $attribute->setDetailOutputCategory($detailOutputCategory);
    }
}
