<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Settings\SettingsInterface;
use App\Specification\Factory\IInputFilterFactory;
use App\Actions\Action;
use App\IO\Dataset\Export\IExportDataset;

final class DatasetExportAction extends Action
{
    use DatasetTrait;

    private IExportDataset $exportDataset;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $em,
        SettingsInterface $settings,
        IInputFilterFactory $inputFilterFactory,
        IExportDataset $exportDataset
    ) {
        $this->exportDataset = $exportDataset;
        parent::__construct($logger, $em, $settings, $inputFilterFactory);
    }

    /**
     * `GET` Returns the JSON exported dataset
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        $dataset = $this->getDataset();

        if ($this->request->getMethod() === GET) {
            $payload = $this->exportDataset->export($dataset);
        }

        $this->response->getBody()->write(json_encode($payload));
        return $this->response;
    }
}
