<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Criteria;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use App\Actions\Action;
use App\Entity\CriteriaFamily;

final class CriteriaFamilyAction extends Action
{
    use CriteriaFamilyTrait;

    /**
     * `GET` Returns the criteria family found
     * `PUT` Full update the criteria family and returns the new version
     * `DELETE` Delete the criteria family found and return a confirmation message
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        $criteriaFamily = $this->getCriteriaFamily();

        if ($this->request->getMethod() === GET) {
            $payload = $criteriaFamily;
        }

        if ($this->request->getMethod() === PUT) {
            $data = $this->getValidatedData('criteria_family');
            $this->editCriteriaFamily($criteriaFamily, $data);
            $payload = $criteriaFamily;
        }

        if ($this->request->getMethod() === DELETE) {
            $id = $criteriaFamily->getId();
            $this->em->remove($criteriaFamily);
            $this->em->flush();
            $payload = ['message' => 'Criteria family with id ' . $id . ' is removed!'];
        }

        return $this->respond($payload);
    }

    /**
     * Returns the criteria family with the id sent in the URL
     *
     * @return CriteriaFamily
     * @throws HttpNotFoundException
     */
    private function getCriteriaFamily(): CriteriaFamily
    {
        $id = $this->resolveArg('name') . '_' . $this->resolveArg('dname') . '_' . $this->resolveArg('id');

        $criteriaFamily = $this->em->find(CriteriaFamily::class, $id);
        if (!is_null($criteriaFamily)) {
            return $criteriaFamily;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Criteria family with id ' . $this->resolveArg('id') . ' is not found'
        );
    }

    /**
     * Update criteria family object with setters
     *
     * @param CriteriaFamily $criteriaFamily The criteria family to update
     * @param array $data Contains the new values ​​of the criteria family sent by the user
     */
    private function editCriteriaFamily(CriteriaFamily $criteriaFamily, array $data): void
    {
        $this->hydrateCriteriaFamily($criteriaFamily, $data);
        $this->em->flush();
    }
}
