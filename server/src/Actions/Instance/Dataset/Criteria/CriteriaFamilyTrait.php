<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Criteria;

use App\Entity\CriteriaFamily;

trait CriteriaFamilyTrait
{
    /**
     * Set the criteria family object with the values sent by the user
     *
     * @param CriteriaFamily $criteriaFamily The criteria family object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateCriteriaFamily(CriteriaFamily $criteriaFamily, array $data): void
    {
        $criteriaFamily->setLabel($data['label']);
        $criteriaFamily->setDisplay($data['display']);
        $criteriaFamily->setOpened($data['opened']);
    }
}
