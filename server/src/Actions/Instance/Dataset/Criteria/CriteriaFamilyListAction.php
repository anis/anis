<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Criteria;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Actions\Instance\Dataset\DatasetTrait;
use App\Entity\CriteriaFamily;
use App\Entity\Dataset;

final class CriteriaFamilyListAction extends Action
{
    use DatasetTrait;
    use CriteriaFamilyTrait;

    /**
     * `GET`  Returns a list of all criteria family for a given dataset
     * `POST` Add a new dataset criteria family to a given dataset
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        // Search the dataset by name (primary key)
        $dataset = $this->getDataset();

        if ($this->request->getMethod() === GET) {
            $payload = $this->em->getRepository(CriteriaFamily::class)->findBy(
                ['dataset' => $dataset],
                ['id' => 'ASC']
            );
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $data = $this->getValidatedData('criteria_family');
            $payload = $this->postCriteriaFamily($data, $dataset);
            $status = 201;
        }

        return $this->respond($payload, $status);
    }

    /**
     * Add a new criteria family into the metamodel
     *
     * @return CriteriaFamily
     * @param array   $data Contains the values ​​of the new criteria family sent by the user
     * @param Dataset $dataset The dataset for adding the family
     */
    private function postCriteriaFamily(array $data, Dataset $dataset): CriteriaFamily
    {
        $criteriaFamily = new CriteriaFamily($dataset, $data['id']);
        $this->hydrateCriteriaFamily($criteriaFamily, $data);

        $this->em->persist($criteriaFamily);
        $this->em->flush();

        return $criteriaFamily;
    }
}
