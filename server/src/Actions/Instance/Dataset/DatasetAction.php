<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Entity\DatasetFamily;
use App\Entity\Database;
use App\Entity\Dataset;

final class DatasetAction extends Action
{
    use DatasetTrait;

    /**
     * `GET` Returns the dataset found
     * `PUT` Full update the dataset and returns the new version
     * `DELETE` Delete the dataset found and return a confirmation message
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct dataset with primary key
        $dataset = $this->getDataset();

        if ($this->request->getMethod() === GET) {
            $payload = $dataset;
        }

        if ($this->request->getMethod() === PUT) {
            $data = $this->getValidatedData('dataset');
            $database = $this->em->find(Database::class, [
                'instance' => $this->resolveArg('name'),
                'id' => $data['id_database']
            ]);
            $datasetFamily = $this->em->find(DatasetFamily::class, [
                'instance' => $this->resolveArg('name'),
                'id' => $data['id_dataset_family']
            ]);

            $this->editDataset($dataset, $data, $database, $datasetFamily);
            $payload = $dataset;
        }

        if ($this->request->getMethod() === DELETE) {
            $name = $dataset->getName();
            $this->em->remove($dataset);
            $this->em->flush();
            $payload = ['message' => 'Dataset with name ' . $name . ' is removed!'];
        }

        return $this->respond($payload);
    }

    /**
     * Update dataset object with setters
     *
     * @param Dataset       $dataset The dataset to update
     * @param string[]      $parsedBody Contains the new values ​​of the dataset sent by the user
     * @param DatasetFamily $datasetFamily Contains the dataset family doctrine object
     */
    private function editDataset(Dataset $dataset, array $data, Database $database, DatasetFamily $datasetFamily): void
    {
        $this->hydrateDataset($dataset, $data);
        $dataset->setDatabase($database);
        $dataset->setDatasetFamily($datasetFamily);

        $this->em->flush();
    }
}
