<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Config;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use App\Actions\Action;
use App\Actions\Instance\Dataset\DatasetTrait;
use App\Entity\Dataset;
use App\Entity\DetailConfig;

final class DetailConfigAction extends Action
{
    use DatasetTrait;
    use DatasetConfigTrait;

    /**
     * `GET` Returns the detail configuration found
     * `POST` Add a new detail configuration and returns this
     * `PUT` Full update the detail configuration and returns the new version
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS');
        }

        if ($this->request->getMethod() === GET) {
            $dataset = $this->getDataset();
            $payload = $this->em->find(
                DetailConfig::class,
                $this->resolveArg('name') . '_' . $dataset->getName()
            );
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $this->checkNoRecordExists();
            $data = $this->getValidatedData('detail_config');
            $dataset = $this->getDataset();
            $payload = $this->postDetailConfig($data, $dataset);
            $status = 201;
        }

        if ($this->request->getMethod() === PUT) {
            $data = $this->getValidatedData('detail_config');
            $detailConfig = $this->getDetailConfig();
            $this->editDetailConfig($detailConfig, $data);
            $payload = $detailConfig;
            $status = 200;
        }

        return $this->respond($payload, $status);
    }

    /**
     * Returns the detail config from dataset object
     *
     * @return DetailConfig
     */
    private function getDetailConfig(): ?DetailConfig
    {
        // Search the correct detail configuration with primary key
        $detailConfig = $this->em->find(
            DetailConfig::class,
            $this->resolveArg('name') . '_' . $this->resolveArg('dname')
        );
        if (!is_null($detailConfig)) {
            return $detailConfig;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Detail config for the dataset ' . $this->resolveArg('dname') . ' is not found'
        );
    }

    /**
     * Check if detail config is already created
     */
    private function checkNoRecordExists(): void
    {
        // Search the correct alias configuration with primary key
        $detailConfig = $this->em->find(
            DetailConfig::class,
            $this->resolveArg('name') . '_' . $this->resolveArg('dname')
        );
        if ($detailConfig) {
            throw new HttpBadRequestException(
                $this->request,
                'Detail config for the dataset ' . $this->resolveArg('dname') . ' is already exists'
            );
        }
    }

    /**
     * @return DetailConfig
     * @param array $data Contains the values ​​of the new detail configuration sent by the user
     * @param Dataset $dataset Dataset for adding the detail configuration
     */
    private function postDetailConfig(array $data, Dataset $dataset): DetailConfig
    {
        $detailConfig = new DetailConfig($dataset);
        $this->hydrateDetailConfig($detailConfig, $data);

        $this->em->persist($detailConfig);
        $this->em->flush();

        return $detailConfig;
    }

    /**
     * Update detail configuration object with setters
     *
     * @param DetailConfig $detailConfig The detail configuration to update
     * @param array $data Contains the new values ​​of the detail sent by the user
     */
    private function editDetailConfig(DetailConfig $detailConfig, array $data): void
    {
        $this->hydrateDetailConfig($detailConfig, $data);
        $this->em->flush();
    }
}
