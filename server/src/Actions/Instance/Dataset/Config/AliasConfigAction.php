<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Config;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use App\Actions\Action;
use App\Actions\Instance\Dataset\DatasetTrait;
use App\Entity\Dataset;
use App\Entity\AliasConfig;

final class AliasConfigAction extends Action
{
    use DatasetTrait;
    use DatasetConfigTrait;

    /**
     * `GET` Returns the alias configuration found
     * `POST` Add a new alias configuration and returns this
     * `PUT` Full update the alias configuration and returns the new version
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS');
        }

        if ($this->request->getMethod() === GET) {
            $dataset = $this->getDataset();
            $payload = $this->em->find(
                AliasConfig::class,
                $this->resolveArg('name') . '_' . $dataset->getName()
            );
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $this->checkNoRecordExists();
            $data = $this->getValidatedData('alias_config');
            $dataset = $this->getDataset();
            $payload = $this->postAliasConfig($data, $dataset);
            $status = 201;
        }

        if ($this->request->getMethod() === PUT) {
            $data = $this->getValidatedData('alias_config');
            $aliasConfig = $this->getAliasConfig();
            $this->editAliasConfig($aliasConfig, $data);
            $payload = $aliasConfig;
            $status = 200;
        }

        return $this->respond($payload, $status);
    }

    /**
     * Returns the alias config from dataset object
     *
     * @return AliasConfig
     */
    private function getAliasConfig(): ?AliasConfig
    {
        // Search the correct alias configuration with primary key
        $aliasConfig = $this->em->find(
            AliasConfig::class,
            $this->resolveArg('name') . '_' . $this->resolveArg('dname')
        );
        if (!is_null($aliasConfig)) {
            return $aliasConfig;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Alias config for the dataset ' . $this->resolveArg('dname') . ' is not found'
        );
    }

    /**
     * Check if alias config is already created
     */
    private function checkNoRecordExists(): void
    {
        // Search the correct alias configuration with primary key
        $aliasConfig = $this->em->find(
            AliasConfig::class,
            $this->resolveArg('name') . '_' . $this->resolveArg('dname')
        );
        if ($aliasConfig) {
            throw new HttpBadRequestException(
                $this->request,
                'Alias config for the dataset ' . $this->resolveArg('dname') . ' is already exists'
            );
        }
    }

    /**
     * @return AliasConfig
     * @param array $data Contains the values ​​of the new alias configuration sent by the user
     * @param Dataset $dataset Dataset for adding the alias configuration
     */
    private function postAliasConfig(array $data, Dataset $dataset): AliasConfig
    {
        $aliasConfig = new AliasConfig($dataset);
        $this->hydrateAliasConfig($aliasConfig, $data);

        $this->em->persist($aliasConfig);
        $this->em->flush();

        return $aliasConfig;
    }

    /**
     * Update alias configuration object with setters
     *
     * @param AliasConfig $aliasConfig The alias configuration to update
     * @param array $data Contains the new values ​​of the alias config sent by the user
     */
    private function editAliasConfig(AliasConfig $aliasConfig, array $data): void
    {
        $this->hydrateAliasConfig($aliasConfig, $data);
        $this->em->flush();
    }
}
