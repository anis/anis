<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Config;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use App\Actions\Action;
use App\Actions\Instance\Dataset\DatasetTrait;
use App\Entity\Dataset;
use App\Entity\ConeSearchConfig;

final class ConeSearchConfigAction extends Action
{
    use DatasetTrait;
    use DatasetConfigTrait;

    /**
     * `GET` Returns the cone-search configuration found
     * `POST` Add a new cone-search configuration and returns this
     * `PUT` Full update the cone-search configuration and returns the new version
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS');
        }

        if ($this->request->getMethod() === GET) {
            $dataset = $this->getDataset();
            $payload = $this->em->find(
                ConeSearchConfig::class,
                $this->resolveArg('name') . '_' . $dataset->getName()
            );
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $this->checkNoRecordExists();
            $data = $this->getValidatedData('cone_search_config');
            $dataset = $this->getDataset();
            $payload = $this->postConeSearchConfig($data, $dataset);
            $status = 201;
        }

        if ($this->request->getMethod() === PUT) {
            $data = $this->getValidatedData('cone_search_config');
            $coneSearchConfig = $this->getConeSearchConfig();
            $this->editConeSearchConfig($coneSearchConfig, $data);
            $payload = $coneSearchConfig;
            $status = 200;
        }

        return $this->respond($payload, $status);
    }

    /**
     * Returns the cone search config from dataset object
     *
     * @return ConeSearchConfig
     */
    private function getConeSearchConfig(): ?ConeSearchConfig
    {
        // Search the correct cone-search configuration with primary key
        $coneSearchConfig = $this->em->find(
            ConeSearchConfig::class,
            $this->resolveArg('name') . '_' . $this->resolveArg('dname')
        );
        if (!is_null($coneSearchConfig)) {
            return $coneSearchConfig;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Cone-search config for the dataset ' . $this->resolveArg('dname') . ' is not found'
        );
    }

    /**
     * Check if cone search config is already created
     */
    private function checkNoRecordExists(): void
    {
        // Search the correct alias configuration with primary key
        $coneSearchConfig = $this->em->find(
            ConeSearchConfig::class,
            $this->resolveArg('name') . '_' . $this->resolveArg('dname')
        );
        if ($coneSearchConfig) {
            throw new HttpBadRequestException(
                $this->request,
                'Cone-search config for the dataset ' . $this->resolveArg('dname') . ' is already exists'
            );
        }
    }

    /**
     * @return ConeSearchConfig
     * @param array $data Contains the values ​​of the new cone-search configuration sent by the user
     * @param Dataset $dataset Dataset for adding the cone-search configuration
     */
    private function postConeSearchConfig(array $data, Dataset $dataset): ConeSearchConfig
    {
        $coneSearchConfig = new ConeSearchConfig($dataset);
        $this->hydrateConeSearchConfig($coneSearchConfig, $data);

        $this->em->persist($coneSearchConfig);
        $this->em->flush();

        return $coneSearchConfig;
    }

    /**
     * Update cone-search configuration object with setters
     *
     * @param ConeSearchConfig $coneSearchConfig The cone-search configuration to update
     * @param array $data Contains the new values ​​of the cone-search sent by the user
     */
    private function editConeSearchConfig(coneSearchConfig $coneSearchConfig, array $data): void
    {
        $this->hydrateConeSearchConfig($coneSearchConfig, $data);
        $this->em->flush();
    }
}
