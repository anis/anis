<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset\Config;

use App\Entity\ConeSearchConfig;
use App\Entity\DetailConfig;
use App\Entity\AliasConfig;

trait DatasetConfigTrait
{
    /**
     * Set the cone search config object with the values sent by the user
     *
     * @param ConeSearchConfig $coneSearchConfig The cone search config object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateConeSearchConfig(ConeSearchConfig $coneSearchConfig, array $data): void
    {
        $coneSearchConfig->setEnabled($data['enabled']);
        $coneSearchConfig->setOpened($data['opened']);
        $coneSearchConfig->setColumnRa($data['column_ra']);
        $coneSearchConfig->setColumnDec($data['column_dec']);
        $coneSearchConfig->setResolverEnabled($data['resolver_enabled']);
        $coneSearchConfig->setDefaultRa($data['default_ra']);
        $coneSearchConfig->setDefaultDec($data['default_dec']);
        $coneSearchConfig->setDefaultRadius($data['default_radius']);
        $coneSearchConfig->setDefaultRaDecUnit($data['default_ra_dec_unit']);
        $coneSearchConfig->setPlotEnabled($data['plot_enabled']);
    }

    /**
     * Set the detail config object with the values sent by the user
     *
     * @param DetailConfig $detailConfig The detail config object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateDetailConfig(DetailConfig $detailConfig, array $data): void
    {
        $detailConfig->setContent($data['content']);
        $detailConfig->setStyleSheet($data['style_sheet']);
    }

    /**
     * Set the alias config object with the values sent by the user
     *
     * @param AliasConfig $aliasConfig The alias config object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateAliasConfig(AliasConfig $aliasConfig, array $data): void
    {
        $aliasConfig->setTableAlias($data['table_alias']);
        $aliasConfig->setColumnAlias($data['column_alias']);
        $aliasConfig->setColumnName($data['column_name']);
        $aliasConfig->setColumnAliasLong($data['column_alias_long']);
    }
}
