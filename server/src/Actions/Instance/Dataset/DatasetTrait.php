<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset;

use Slim\Exception\HttpNotFoundException;
use App\Entity\Dataset;

trait DatasetTrait
{
    /**
     * Set the dataset object with the values sent by the user
     *
     * @param Dataset $dataset The dataset object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateDataset(Dataset $dataset, array $data): void
    {
        $dataset->setTableRef($data['table_ref']);
        $dataset->setPrimaryKey($data['primary_key']);
        $dataset->setDefaultOrderBy($data['default_order_by']);
        $dataset->setDefaultOrderByDirection($data['default_order_by_direction']);
        $dataset->setLabel($data['label']);
        $dataset->setDescription($data['description']);
        $dataset->setDisplay($data['display']);
        $dataset->setDataPath($data['data_path']);
        $dataset->setPublic($data['public']);
        $dataset->setDownloadJson($data['download_json']);
        $dataset->setDownloadCsv($data['download_csv']);
        $dataset->setDownloadAscii($data['download_ascii']);
        $dataset->setDownloadVo($data['download_vo']);
        $dataset->setDownloadFits($data['download_fits']);
        $dataset->setServerLinkEnabled($data['server_link_enabled']);
        $dataset->setDatatableEnabled($data['datatable_enabled']);
        $dataset->setDatatableSelectableRows($data['datatable_selectable_rows']);
    }

    /**
     * Returns the dataset with the name sent in the URL
     *
     * @return Dataset
     * @throws HttpNotFoundException
     */
    private function getDataset(): Dataset
    {
        $dataset = $this->em->find(Dataset::class, [
            'instance' => $this->resolveArg('name'),
            'name' => $this->resolveArg('dname')
        ]);
        if (!is_null($dataset)) {
            return $dataset;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Dataset with name ' . $this->resolveArg('dname') . ' is not found'
        );
    }
}
