<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Dataset;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Settings\SettingsInterface;
use App\Specification\Factory\IInputFilterFactory;
use App\Actions\Action;
use App\Actions\Instance\InstanceTrait;
use App\IO\Dataset\Import\ICheckImportDataset;
use App\IO\Dataset\Import\IImportDataset;
use App\Exception\InvalidFormException;

final class DatasetImportAction extends Action
{
    use InstanceTrait;

    private ICheckImportDataset $checkImportDataset;
    private IImportDataset $importDataset;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $em,
        SettingsInterface $settings,
        IInputFilterFactory $inputFilterFactory,
        ICheckImportDataset $checkImportDataset,
        IImportDataset $importDataset
    ) {
        $this->checkImportDataset = $checkImportDataset;
        $this->importDataset = $importDataset;
        parent::__construct($logger, $em, $settings, $inputFilterFactory);
    }

    /**
     * `POST` Import and save a dataset
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'POST, OPTIONS');
        }

        if (!$this->checkImportDataset->check($this->getFormData(), $this->resolveArg('name'))) {
            throw (new InvalidFormException($this->request, 'Errors in the form parameters'))
                ->setErrors($this->checkImportDataset->getErrorsMessages());
        }

        $payload = $this->importDataset->import($this->getFormData(), $this->getInstance());

        return $this->respond($payload, 201);
    }
}
