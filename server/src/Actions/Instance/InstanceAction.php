<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Entity\Instance;

final class InstanceAction extends Action
{
    use InstanceTrait;

    /**
     * `GET` Returns the instance found
     * `PUT` Full update the instance and returns the new version
     * `DELETE` Delete the instance found and return a confirmation message
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct instance with primary key
        $instance = $this->getInstance();

        if ($this->request->getMethod() === GET) {
            $payload = $instance;
        }

        if ($this->request->getMethod() === PUT) {
            $data = $this->getValidatedData('instance');
            $this->editInstance($instance, $data);
            $payload = $instance;
        }

        if ($this->request->getMethod() === DELETE) {
            $name = $instance->getName();
            $this->em->remove($instance);
            $this->em->flush();
            $payload = ['message' => 'Instance with name ' . $name . ' is removed!'];
        }

        return $this->respond($payload);
    }

    /**
     * Update instance object with setters
     *
     * @param Instance $instance The instance to update
     * @param array $data Contains the new values ​​of the instance sent by the user
     */
    private function editInstance(Instance $instance, array $data): void
    {
        $instance->setLabel($data['label']);
        $this->hydrateInstance($instance, $data);

        $this->em->flush();
    }
}
