<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Group;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use App\Actions\Action;
use App\Entity\InstanceGroup;

final class InstanceGroupAction extends Action
{
    use InstanceGroupTrait;

    /**
     * `GET` Returns the instance group found
     * `PUT` Full update the instance group and returns the new version
     * `DELETE` Delete the instance group found and return a confirmation message
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct instance-group with primary key
        $instanceGroup = $this->getInstanceGroup();

        if ($this->request->getMethod() === GET) {
            $payload = $instanceGroup;
        }

        if ($this->request->getMethod() === PUT) {
            $data = $this->getValidatedData('instance_group');
            $this->editInstanceGroup($instanceGroup, $data);
            $payload = $instanceGroup;
        }

        if ($this->request->getMethod() === DELETE) {
            $role = $instanceGroup->getRole();
            $this->em->remove($instanceGroup);
            $this->em->flush();
            $payload = ['message' => 'Instance-group with role ' . $role . ' is removed!'];
        }

        return $this->respond($payload);
    }

    /**
     * Returns the instance-group with the role sent in the URL
     *
     * @return InstanceGroup
     * @throws HttpNotFoundException
     */
    private function getInstanceGroup(): InstanceGroup
    {
        $instanceGroup = $this->em->find(InstanceGroup::class, [
            'instance' => $this->resolveArg('name'),
            'role' => $this->resolveArg('role')
        ]);
        if (!is_null($instanceGroup)) {
            return $instanceGroup;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Instance-group with role ' . $this->resolveArg('role') . ' is not found'
        );
    }

    /**
     * Update instance group object with setters
     *
     * @param InstanceGroup $instanceGroup The instance-group to update
     * @param array $data Contains the new values ​​of the instance-group sent by the user
     */
    private function editInstanceGroup(InstanceGroup $instanceGroup, array $data): void
    {
        $this->hydrateInstanceGroup($instanceGroup, $data);
        $this->em->flush();
    }
}
