<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Group;

use App\Entity\InstanceGroup;

trait InstanceGroupTrait
{
    /**
     * Set the instance group object with the values sent by the user
     *
     * @param InstanceGroup $instanceGroup The instance group object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateInstanceGroup(InstanceGroup $instanceGroup, array $data): void
    {
    }
}
