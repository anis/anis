<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Group;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Actions\Instance\InstanceTrait;
use App\Entity\InstanceGroup;
use App\Entity\Instance;

final class InstanceGroupListAction extends Action
{
    use InstanceTrait;
    use InstanceGroupTrait;

    /**
     * `GET`  Returns a list of all instance groups listed in the metamodel database
     * `POST` Add a new instance group
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        $instance = $this->getInstance();

        if ($this->request->getMethod() === GET) {
            $payload = $this->em->getRepository('App\Entity\InstanceGroup')->findBy(['instance' => $instance]);
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $data = $this->getValidatedData('instance_group');
            $payload = $this->postInstanceGroup($data, $instance);
            $status = 201;
        }

        return $this->respond($payload, $status);
    }

    /**
     * Add a new instance group into the metamodel
     *
     * @return InstanceGroup The newly created instance group
     * @param array $data Contains the values ​​of the new instance-group sent by the user
     * @param Instance $instance The instance for adding the instance group
     */
    private function postInstanceGroup(array $data, Instance $instance): InstanceGroup
    {
        $instanceGroup = new InstanceGroup($instance, $data['role']);
        $this->hydrateInstanceGroup($instanceGroup, $data);

        $this->em->persist($instanceGroup);
        $this->em->flush();

        return $instanceGroup;
    }
}
