<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance;

use Slim\Exception\HttpNotFoundException;
use App\Entity\Instance;

trait InstanceTrait
{
    /**
     * Returns the instance with the name sent in the URL
     *
     * @return Instance
     * @throws HttpNotFoundException
     */
    private function getInstance(): Instance
    {
        $instance = $this->em->find(Instance::class, $this->resolveArg('name'));
        if (!is_null($instance)) {
            return $instance;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Instance with name ' . $this->resolveArg('name') . ' is not found'
        );
    }

    /**
     * Set the instance object with the values sent by the user
     *
     * @param Instance $instance The instance object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateInstance(Instance $instance, array $data): void
    {
        $instance->setDescription($data['description']);
        $instance->setScientificManager($data['scientific_manager']);
        $instance->setInstrument($data['instrument']);
        $instance->setWavelengthDomain($data['wavelength_domain']);
        $instance->setDisplay($data['display']);
        $instance->setDataPath($data['data_path']);
        $instance->setFilesPath($data['files_path']);
        $instance->setPublic($data['public']);
        $instance->setPortalLogo($data['portal_logo']);
        $instance->setPortalColor($data['portal_color']);
        $instance->setDefaultRedirect($data['default_redirect']);
    }
}
