<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Entity\Instance;

final class InstanceListAction extends Action
{
    use InstanceTrait;

    /**
     * `GET`  Returns a list of all instances listed in the metamodel
     * `POST` Add a new instance
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        if ($this->request->getMethod() === GET) {
            $payload = $this->em->getRepository(Instance::class)->findAll();
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $data = $this->getValidatedData('instance');
            $payload = $this->postInstance($data);
            $status = 201;
        }

        return $this->respond($payload, $status);
    }

    /**
     * Add a new instance into the metamodel
     *
     * @return Instance
     * @param array $data Contains the values ​​of the new instance sent by the user
     */
    private function postInstance(array $data): Instance
    {
        $instance = new Instance($data['name'], $data['label']);
        $this->hydrateInstance($instance, $data);

        $this->em->persist($instance);
        $this->em->flush();

        return $instance;
    }
}
