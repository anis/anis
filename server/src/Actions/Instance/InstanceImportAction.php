<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Settings\SettingsInterface;
use App\Specification\Factory\IInputFilterFactory;
use App\Actions\Action;
use App\Actions\Instance\InstanceTrait;
use App\IO\Instance\Import\ICheckImportInstance;
use App\IO\Instance\Import\IImportInstance;
use App\Exception\InvalidFormException;

final class InstanceImportAction extends Action
{
    use InstanceTrait;

    private ICheckImportInstance $checkImportInstance;
    private IImportInstance $importInstance;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $em,
        SettingsInterface $settings,
        IInputFilterFactory $inputFilterFactory,
        ICheckImportInstance $checkImportInstance,
        IImportInstance $importInstance
    ) {
        $this->checkImportInstance = $checkImportInstance;
        $this->importInstance = $importInstance;
        parent::__construct($logger, $em, $settings, $inputFilterFactory);
    }

    /**
     * `POST` Import and save an instance
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'POST, OPTIONS');
        }

        if (!$this->checkImportInstance->check($this->getFormData())) {
            throw (new InvalidFormException($this->request, 'Errors in the form parameters'))
                ->setErrors($this->checkImportInstance->getErrorsMessages());
        }

        $payload = $this->importInstance->import($this->getFormData());
        return $this->respond($payload, 201);
    }
}
