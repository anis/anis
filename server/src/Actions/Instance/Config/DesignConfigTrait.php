<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Config;

use App\Entity\DesignConfig;

trait DesignConfigTrait
{
    /**
     * Set the design config object with the values sent by the user
     *
     * @param DesignConfig $designConfig The design config object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateDesignConfig(DesignConfig $designConfig, array $data): void
    {
        $designConfig->setDesignBackgroundColor($data['design_background_color']);
        $designConfig->setDesignTextColor($data['design_text_color']);
        $designConfig->setDesignFontFamily($data['design_font_family']);
        $designConfig->setDesignLinkColor($data['design_link_color']);
        $designConfig->setDesignLinkHoverColor($data['design_link_hover_color']);
        $designConfig->setDesignLogo($data['design_logo']);
        $designConfig->setDesignLogoHref($data['design_logo_href']);
        $designConfig->setDesignFavicon($data['design_favicon']);
        $designConfig->setNavbarBackgroundColor($data['navbar_background_color']);
        $designConfig->setNavbarBorderBottomColor($data['navbar_border_bottom_color']);
        $designConfig->setNavbarColorHref($data['navbar_color_href']);
        $designConfig->setNavbarFontFamily($data['navbar_font_family']);
        $designConfig->setNavbarSignInBtnColor($data['navbar_sign_in_btn_color']);
        $designConfig->setNavbarUserBtnColor($data['navbar_user_btn_color']);
        $designConfig->setFooterBackgroundColor($data['footer_background_color']);
        $designConfig->setFooterBorderTopColor($data['footer_border_top_color']);
        $designConfig->setFooterTextColor($data['footer_text_color']);
        $designConfig->setFooterLogos($data['footer_logos']);
        $designConfig->setFamilyBorderColor($data['family_border_color']);
        $designConfig->setFamilyHeaderBackgroundColor($data['family_header_background_color']);
        $designConfig->setFamilyTitleColor($data['family_title_color']);
        $designConfig->setFamilyTitleBold($data['family_title_bold']);
        $designConfig->setFamilyBackgroundColor($data['family_background_color']);
        $designConfig->setFamilyTextColor($data['family_text_color']);
        $designConfig->setProgressBarTitle($data['progress_bar_title']);
        $designConfig->setProgressBarTitleColor($data['progress_bar_title_color']);
        $designConfig->setProgressBarSubtitle($data['progress_bar_subtitle']);
        $designConfig->setProgressBarSubtitleColor($data['progress_bar_subtitle_color']);
        $designConfig->setProgressBarStepDatasetTitle($data['progress_bar_step_dataset_title']);
        $designConfig->setProgressBarStepCriteriaTitle($data['progress_bar_step_criteria_title']);
        $designConfig->setProgressBarStepOutputTitle($data['progress_bar_step_output_title']);
        $designConfig->setProgressBarStepResultTitle($data['progress_bar_step_result_title']);
        $designConfig->setProgressBarColor($data['progress_bar_color']);
        $designConfig->setProgressBarActiveColor($data['progress_bar_active_color']);
        $designConfig->setProgressBarCircleColor($data['progress_bar_circle_color']);
        $designConfig->setProgressBarCircleIconColor($data['progress_bar_circle_icon_color']);
        $designConfig->setProgressBarCircleIconActiveColor($data['progress_bar_circle_icon_active_color']);
        $designConfig->setProgressBarTextColor($data['progress_bar_text_color']);
        $designConfig->setProgressBarTextBold($data['progress_bar_text_bold']);
        $designConfig->setSearchNextBtnColor($data['search_next_btn_color']);
        $designConfig->setSearchNextBtnHoverColor($data['search_next_btn_hover_color']);
        $designConfig->setSearchNextBtnHoverTextColor($data['search_next_btn_hover_text_color']);
        $designConfig->setSearchBackBtnColor($data['search_back_btn_color']);
        $designConfig->setSearchBackBtnHoverColor($data['search_back_btn_hover_color']);
        $designConfig->setSearchBackBtnHoverTextColor($data['search_back_btn_hover_text_color']);
        $designConfig->setDeleteCurrentQueryBtnEnabled($data['delete_current_query_btn_enabled']);
        $designConfig->setDeleteCurrentQueryBtnText($data['delete_current_query_btn_text']);
        $designConfig->setDeleteCurrentQueryBtnColor($data['delete_current_query_btn_color']);
        $designConfig->setDeleteCurrentQueryBtnHoverColor($data['delete_current_query_btn_hover_color']);
        $designConfig->setDeleteCurrentQueryBtnHoverTextColor($data['delete_current_query_btn_hover_text_color']);
        $designConfig->setDeleteCriterionCrossColor($data['delete_criterion_cross_color']);
        $designConfig->setSearchInfoBackgroundColor($data['search_info_background_color']);
        $designConfig->setSearchInfoTextColor($data['search_info_text_color']);
        $designConfig->setSearchInfoHelpEnabled($data['search_info_help_enabled']);
        $designConfig->setDatasetSelectBtnColor($data['dataset_select_btn_color']);
        $designConfig->setDatasetSelectBtnHoverColor($data['dataset_select_btn_hover_color']);
        $designConfig->setDatasetSelectBtnHoverTextColor($data['dataset_select_btn_hover_text_color']);
        $designConfig->setDatasetSelectedIconColor($data['dataset_selected_icon_color']);
        $designConfig->setSearchCriterionBackgroundColor($data['search_criterion_background_color']);
        $designConfig->setSearchCriterionTextColor($data['search_criterion_text_color']);
        $designConfig->setOutputColumnsSelectedColor($data['output_columns_selected_color']);
        $designConfig->setOutputColumnsSelectAllBtnColor($data['output_columns_select_all_btn_color']);
        $designConfig->setOutputColumnsSelectAllBtnHoverColor($data['output_columns_select_all_btn_hover_color']);
        $designConfig->setOutputColumnsSelectAllBtnHoverTextColor($data[
            'output_columns_select_all_btn_hover_text_color'
        ]);
        $designConfig->setResultPanelBorderSize($data['result_panel_border_size']);
        $designConfig->setResultPanelBorderColor($data['result_panel_border_color']);
        $designConfig->setResultPanelTitleColor($data['result_panel_title_color']);
        $designConfig->setResultPanelBackgroundColor($data['result_panel_background_color']);
        $designConfig->setResultPanelTextColor($data['result_panel_text_color']);
        $designConfig->setResultDownloadBtnColor($data['result_download_btn_color']);
        $designConfig->setResultDownloadBtnHoverColor($data['result_download_btn_hover_color']);
        $designConfig->setResultDownloadBtnTextColor($data['result_download_btn_text_color']);
        $designConfig->setResultDatatableActionsBtnColor($data['result_datatable_actions_btn_color']);
        $designConfig->setResultDatatableActionsBtnHoverColor($data['result_datatable_actions_btn_hover_color']);
        $designConfig->setResultDatatableActionsBtnTextColor($data['result_datatable_actions_btn_text_color']);
        $designConfig->setResultDatatableBordered($data['result_datatable_bordered']);
        $designConfig->setResultDatatableBorderedRadius($data['result_datatable_bordered_radius']);
        $designConfig->setResultDatatableBorderColor($data['result_datatable_border_color']);
        $designConfig->setResultDatatableHeaderBackgroundColor($data['result_datatable_header_background_color']);
        $designConfig->setResultDatatableHeaderTextColor($data['result_datatable_header_text_color']);
        $designConfig->setResultDatatableRowsBackgroundColor($data['result_datatable_rows_background_color']);
        $designConfig->setResultDatatableRowsTextColor($data['result_datatable_rows_text_color']);
        $designConfig->setResultDatatableSortedColor($data['result_datatable_sorted_color']);
        $designConfig->setResultDatatableSortedActiveColor($data['result_datatable_sorted_active_color']);
        $designConfig->setResultDatatableLinkColor($data['result_datatable_link_color']);
        $designConfig->setResultDatatableLinkHoverColor($data['result_datatable_link_hover_color']);
        $designConfig->setResultDatatableRowsSelectedColor($data['result_datatable_rows_selected_color']);
        $designConfig->setResultDatatablePaginationLinkColor($data['result_datatable_pagination_link_color']);
        $designConfig->setResultDatatablePaginationActiveBckColor($data[
            'result_datatable_pagination_active_bck_color'
        ]);
        $designConfig->setResultDatatablePaginationActiveTextColor($data[
            'result_datatable_pagination_active_text_color'
        ]);
        $designConfig->setSampEnabled($data['samp_enabled']);
        $designConfig->setBackToPortal($data['back_to_portal']);
        $designConfig->setUserMenuEnabled($data['user_menu_enabled']);
        $designConfig->setSearchMultipleAllDatasetsSelected($data['search_multiple_all_datasets_selected']);
        $designConfig->setSearchMultipleProgressBarTitle($data['search_multiple_progress_bar_title']);
        $designConfig->setSearchMultipleProgressBarSubtitle($data['search_multiple_progress_bar_subtitle']);
        $designConfig->setSearchMultipleProgressBarStepPosition($data['search_multiple_progress_bar_step_position']);
        $designConfig->setSearchMultipleProgressBarStepDatasets($data['search_multiple_progress_bar_step_datasets']);
        $designConfig->setSearchMultipleProgressBarStepResult($data['search_multiple_progress_bar_step_result']);
    }
}
