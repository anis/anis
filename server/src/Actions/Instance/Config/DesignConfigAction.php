<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Config;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use App\Actions\Action;
use App\Actions\Instance\InstanceTrait;
use App\Entity\Instance;
use App\Entity\DesignConfig;

final class DesignConfigAction extends Action
{
    use InstanceTrait;
    use DesignConfigTrait;

    /**
     * `GET` Returns the cone-search configuration found
     * `POST` Add a new cone-search configuration and returns this
     * `PUT` Full update the cone-search configuration and returns the new version
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS');
        }

        if ($this->request->getMethod() === GET) {
            $payload = $this->getDesignConfig();
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $this->checkNoRecordExists();
            $data = $this->getValidatedData('design_config');
            $instance = $this->getInstance();
            $payload = $this->postDesignConfig($data, $instance);
            $status = 201;
        }

        if ($this->request->getMethod() === PUT) {
            $designConfig = $this->getDesignConfig();
            $data = $this->getValidatedData('design_config');
            $this->editDesignConfig($designConfig, $data);
            $payload = $designConfig;
            $status = 200;
        }

        return $this->respond($payload, $status);
    }

    /**
     * Returns the instance design config
     *
     * @return DesignConfig
     */
    private function getDesignConfig(): ?DesignConfig
    {
        // Search the correct instance design config with primary key
        $designConfig = $this->em->find(
            DesignConfig::class,
            $this->resolveArg('name')
        );
        if (!is_null($designConfig)) {
            return $designConfig;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Design config for instance ' . $this->resolveArg('name') . ' is not found'
        );
    }

    /**
     * Check if design config is already created
     */
    private function checkNoRecordExists(): void
    {
        // Search the correct instance design config with primary key
        $designConfig = $this->em->find(
            DesignConfig::class,
            $this->resolveArg('name')
        );
        if ($designConfig) {
            throw new HttpBadRequestException(
                $this->request,
                'Design config for instance ' . $this->resolveArg('name') . ' is already exists'
            );
        }
    }

    /**
     * @return DesignConfig
     * @param array $data Contains the values ​​of the new design configuration sent by the user
     * @param Instance $instance Instance for adding the design configuration
     */
    private function postDesignConfig(array $data, Instance $instance): DesignConfig
    {
        $designConfig = new DesignConfig($instance);
        $this->hydrateDesignConfig($designConfig, $data);

        $this->em->persist($designConfig);
        $this->em->flush();

        return $designConfig;
    }

    /**
     * Update design configuration object with setters
     *
     * @param DesignConfig $designConfig The design configuration to update
     * @param array $data Contains the new values ​​of the design configuration sent by the user
     */
    private function editDesignConfig(DesignConfig $designConfig, array $data): void
    {
        $this->hydrateDesignConfig($designConfig, $data);
        $this->em->flush();
    }
}
