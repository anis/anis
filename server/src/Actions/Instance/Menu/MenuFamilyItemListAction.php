<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Menu;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use App\Actions\Instance\InstanceTrait;
use App\Entity\MenuFamily;
use App\Entity\MenuItem;

class MenuFamilyItemListAction extends MenuItemListAction
{
    use InstanceTrait;
    use MenuItemTrait;

    /**
     * `GET`  Returns a list of all menu items listed for a family
     * `POST` Add a new family menu item
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        $instance = $this->getInstance();
        $menuFamily = $this->getMenuFamily();

        if ($this->request->getMethod() === GET) {
            $payload = $this->em->getRepository(MenuItem::class)->findBy(['menuFamily' => $menuFamily]);
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $menuItemType = $this->getMenuItemType(['webpage', 'url']);
            $data = $this->getValidatedData($menuItemType);
            $payload = $this->postMenuItem($menuItemType, $data, $instance, $menuFamily);
            $status = 201;
        }

        return $this->respond($payload, $status);
    }

    /**
     * Returns the menu family with the id sent in the form
     *
     * @return MenuFamily|null
     * @param string $instanceName
     * @param int|null $idMenuFamily
     */
    private function getMenuFamily(): MenuFamily
    {
        $menuFamily = $this->em->find(
            'App\Entity\MenuFamily',
            $this->resolveArg('name') . '_' . $this->resolveArg('fid')
        );
        if (!is_null($menuFamily)) {
            return $menuFamily;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Menu family with id ' . $this->resolveArg('fid') . ' is not found'
        );
    }
}
