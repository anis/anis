<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Menu;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use App\Actions\Action;
use App\Actions\Instance\InstanceTrait;
use App\Entity\Instance;
use App\Entity\MenuItem;
use App\Entity\MenuFamily;

class MenuItemListAction extends Action
{
    use InstanceTrait;
    use MenuItemTrait;

    /**
     * `GET`  Returns a list of all menu items listed in the metamodel
     * `POST` Add a new instance menu item
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        }

        $instance = $this->getInstance();

        if ($this->request->getMethod() === GET) {
            $payload = $this->em->getRepository(MenuItem::class)->findBy(['instance' => $instance]);
            $status = 200;
        }

        if ($this->request->getMethod() === POST) {
            $menuItemType = $this->getMenuItemType(['menu_family', 'webpage', 'url']);
            $data = $this->getValidatedData($menuItemType);
            $payload = $this->postMenuItem($menuItemType, $data, $instance);
            $status = 201;
        }

        return $this->respond($payload, $status);
    }

    protected function getMenuItemType(array $authorizedType): string
    {
        if (!array_key_exists('type', $this->request->getQueryParams())) {
            throw new HttpBadRequestException(
                $this->request,
                'Query param type is mandatory for this request'
            );
        }
        $params = $this->request->getQueryParams()['type'];
        if (!in_array($params, $authorizedType)) {
            throw new HttpBadRequestException(
                $this->request,
                'Query param type must be equal to menu_family, webpage or url'
            );
        }
        return $params;
    }

    /**
     * Add a new menu item into the metamodel
     *
     * @return MenuItem
     * @param string $menuItemType webpage, menu_family or url
     * @param array $data Contains the values ​​of the new menu item sent by the user
     * @param Instance $instance
     * @param MenuFamily|null $menuFamilyLinked
     */
    protected function postMenuItem(
        string $menuItemType,
        array $data,
        Instance $instance,
        ?MenuFamily $menuFamilyLinked = null
    ): MenuItem {
        $class = ucfirst(lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $menuItemType)))));
        $fullClass = '\App\Entity\\' . $class;
        $hydrateFn = 'hydrate' . $class;
        $menuItem = new $fullClass($instance, $data['id'], $menuFamilyLinked);
        $this->$hydrateFn($menuItem, $data);

        $this->em->persist($menuItem);
        $this->em->flush();

        return $menuItem;
    }
}
