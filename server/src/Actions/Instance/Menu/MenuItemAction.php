<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Menu;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use App\Actions\Action;
use App\Entity\MenuItem;

class MenuItemAction extends Action
{
    use MenuItemTrait;

    /**
     * `GET`  Returns a list of all menu items listed in the metamodel
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, OPTIONS');
        }

        // Search the correct menu item with primary key
        $menuItem = $this->getMenuItem();

        if ($this->request->getMethod() === GET) {
            $payload = $menuItem;
        }

        if ($this->request->getMethod() === PUT) {
            $class = $this->getMenuItemClass($menuItem);
            $menuItemType = strtolower(preg_replace('/\B([A-Z])/', '_$1', $class));
            $data = $this->getValidatedData($menuItemType);
            $this->editMenuItem($class, $menuItem, $data);
            $payload = $menuItem;
        }

        if ($this->request->getMethod() === DELETE) {
            $id = $menuItem->getId();
            $this->em->remove($menuItem);
            $this->em->flush();
            $payload = ['message' => 'Menu item with id ' . $id . ' is removed!'];
        }

        return $this->respond($payload);
    }

    protected function getMenuItemClass(MenuItem $menuItem): string
    {
        $temp = explode('\\', get_class($menuItem));
        return array_pop($temp);
    }

    /**
     * Returns the menu item with the id sent in the URL
     *
     * @return MenuItem
     * @throws HttpNotFoundException
     */
    protected function getMenuItem(): MenuItem
    {
        $menuItem = $this->em->find(
            'App\Entity\MenuItem',
            $this->getMenuItemId()
        );
        if (!is_null($menuItem)) {
            return $menuItem;
        }

        throw new HttpNotFoundException(
            $this->request,
            'Menu item with id ' . $this->resolveArg('id') . ' is not found'
        );
    }

    protected function getMenuItemId(): string
    {
        return $this->resolveArg('name') . '_' . $this->resolveArg('id');
    }

    protected function editMenuItem(string $class, MenuItem $menuItem, array $data): void
    {
        $hydrateFn = 'hydrate' . $class;
        $this->$hydrateFn($menuItem, $data);

        $this->em->flush();
    }
}
