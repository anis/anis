<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Instance\Menu;

use App\Entity\MenuItem;
use App\Entity\MenuFamily;
use App\Entity\Webpage;
use App\Entity\Url;

trait MenuItemTrait
{
    /**
     * Set the menu item object with the values sent by the user
     *
     * @param MenuItem $menuItem The menu item object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateMenuItem(MenuItem $menuItem, array $data): void
    {
        $menuItem->setLabel($data['label']);
        $menuItem->setIcon($data['icon']);
        $menuItem->setDisplay($data['display']);
    }

    /**
     * Set the menu family object with the values sent by the user
     *
     * @param MenuFamily $menuFamily The menu family object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateMenuFamily(MenuFamily $menuFamily, array $data): void
    {
        $this->hydrateMenuItem($menuFamily, $data);
        $menuFamily->setName($data['name']);
    }

    /**
     * Set the webpage object with the values sent by the user
     *
     * @param Webpage $webpage The webpage object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateWebpage(Webpage $webpage, array $data): void
    {
        $this->hydrateMenuItem($webpage, $data);
        $webpage->setName($data['name']);
        $webpage->setTitle($data['title']);
        $webpage->setContent($data['content']);
        $webpage->setStyleSheet($data['style_sheet']);
    }

    /**
     * Set the url object with the values sent by the user
     *
     * @param Url $url The url object to be modified
     * @param array $data Data sent by the user
     */
    private function hydrateUrl(Url $url, array $data): void
    {
        $this->hydrateMenuItem($url, $data);
        $url->setTypeUrl($data['type_url']);
        $url->setHref($data['href']);
    }
}
