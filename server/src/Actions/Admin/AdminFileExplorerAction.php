<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions\Admin;

use Psr\Http\Message\ResponseInterface as Response;
use App\Actions\Action;
use App\Actions\FileExplorerTrait;

final class AdminFileExplorerAction extends Action
{
    use FileExplorerTrait;

    /**
     * `GET` Returns the list of files if path is a directory or stream file
     *
     * @return Response
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() === OPTIONS) {
            return $this->response->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
        }

        $anisRootPath = $this->settings->get('data_path');
        return $this->explorePath($anisRootPath);
    }
}
