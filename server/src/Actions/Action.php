<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;
use Doctrine\ORM\EntityManagerInterface;
use App\Settings\SettingsInterface;
use App\Specification\Factory\IInputFilterFactory;
use App\Exception\InvalidFormException;
use App\Response\ResponsePayload;
use RuntimeException;

abstract class Action
{
    protected LoggerInterface $logger;

    protected Request $request;

    protected Response $response;

    protected array $args;

    protected EntityManagerInterface $em;

    protected SettingsInterface $settings;

    protected IInputFilterFactory $inputFilterFactory;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $em,
        SettingsInterface $settings,
        IInputFilterFactory $inputFilterFactory
    ) {
        $this->logger = $logger;
        $this->em = $em;
        $this->settings = $settings;
        $this->inputFilterFactory = $inputFilterFactory;
    }

    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;
        return $this->action();
    }

    abstract protected function action(): Response;

    /**
     * @return array|object
     */
    protected function getFormData()
    {
        if (is_null($this->request->getParsedBody())) {
            return [];
        } else {
            return $this->request->getParsedBody();
        }
    }

    /**
     * @return array  The validated data
     * @param  string $specName The specification name
     * @throws RuntimeException
     * @throws InvalidFormException
     */
    protected function getValidatedData(string $specName): array
    {
        // If the method is different from PUT, check that the record does not already exist
        if ($this->request->getMethod() === PUT) {
            $checkNoRecordExists = false;
        } else {
            $checkNoRecordExists = true;
        }

        $isValid = $this->inputFilterFactory->isValid(
            $specName,
            $this->em,
            $this->args,
            $this->getFormData(),
            $checkNoRecordExists,
            true
        );
        if ($isValid) {
            return $this->inputFilterFactory->getInputFilter()->getValues();
        }

        throw (new InvalidFormException($this->request, 'Errors in the form parameters'))
            ->setErrors($this->inputFilterFactory->getInputFilter()->getMessages());
    }

    /**
     * Returns the argument present in the URL if it exists
     *
     * @return mixed
     * @throws HttpBadRequestException
     */
    protected function resolveArg(string $name): mixed
    {
        if (!isset($this->args[$name])) {
            throw new HttpBadRequestException($this->request, "Could not resolve argument `{$name}`.");
        }

        return $this->args[$name];
    }

    /**
     * Returns the HTTP response to the client
     *
     * @return Response
     * @param array|object|null $data Data sent to the client
     * @param int $statusCode  HTTP status code
     */
    protected function respond($data = null, int $statusCode = 200): Response
    {
        $payload = new ResponsePayload($statusCode, $data);

        $json = json_encode($payload);
        $this->response->getBody()->write($json);

        return $this->response->withStatus($payload->getStatusCode());
    }
}
