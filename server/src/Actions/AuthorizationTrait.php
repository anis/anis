<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions;

use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Exception\HttpUnauthorizedException;
use Slim\Exception\HttpForbiddenException;

trait AuthorizationTrait
{
    /**
     * @param Request $request PSR-7 This object represents the HTTP request
     * @param string $datasetName
     * @param array $adminRoles
     */
    protected function verifyDatasetAuthorization(
        Request $request,
        string $instanceName,
        string $datasetName,
        array $adminRoles
    ) {
        $token = $request->getAttribute('token');
        if (!$token) {
            // The user is not connected (401)
            throw new HttpUnauthorizedException($request);
        }
        $roles = $token->realm_access->roles;
        if (!$this->isAdmin($adminRoles, $roles)) {
            $qb = $this->em->createQueryBuilder();
            $qb->select('dg.role')
                ->from('App\Entity\Instance', 'i')
                ->innerJoin('i.datasets', 'd')
                ->innerJoin('d.groups', 'dg')
                ->where($qb->expr()->eq('i.name', ':iname'))
                ->andWhere($qb->expr()->eq('d.name', ':dname'))
                ->andWhere($qb->expr()->in(
                    'dg.role',
                    array_map(fn ($role) => $instanceName . '_' . $datasetName . '_' . $role, $roles)
                ))
                ->setParameters([
                    'iname' => $instanceName,
                    'dname' => $datasetName
                ]);

            $r = $qb->getQuery()->getResult();
            if (count($r) < 1) {
                throw new HttpForbiddenException(
                    $request,
                    'You do not have the permission to access the dataset : ' . $datasetName
                );
            }
        }
    }

    /**
     * @param Request $request PSR-7 This object represents the HTTP request
     * @param string $instanceName
     * @param array $adminRoles
     */
    protected function verifyInstanceAuthorization(
        Request $request,
        string $instanceName,
        array $adminRoles
    ) {
        $token = $request->getAttribute('token');
        if (!$token) {
            // The user is not connected (401)
            throw new HttpUnauthorizedException($request);
        }
        $roles = $token->realm_access->roles;
        if (!$this->isAdmin($adminRoles, $roles)) {
            $qb = $this->em->createQueryBuilder();
            $qb->select('ig.role')
                ->from('App\Entity\InstanceGroup', 'ig')
                ->where($qb->expr()->in('ig.role', $roles))
                ->andWhere($qb->expr()->eq('ig.instance', ':iname'));
            $qb->setParameter('iname', $instanceName);
            $r = $qb->getQuery()->getResult();
            if (count($r) < 1) {
                throw new HttpForbiddenException(
                    $request,
                    'You do not have the permission to access the instance : ' . $instanceName
                );
            }
        }
    }

    private function isAdmin(array $adminRoles, array $roles): bool
    {
        $admin = false;
        for ($i = 0; $i < count($adminRoles); $i++) {
            $admin = in_array($adminRoles[$i], $roles);
            if ($admin) {
                break;
            }
        }
        return $admin;
    }
}
