<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Actions;

use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpNotFoundException;
use Slim\Psr7\Factory\StreamFactory;

trait FileExplorerTrait
{
    private function explorePath(string $startPath): Response
    {
        // Retrieve the requested path
        $path = $this->getPath($startPath);

        // Throw exception if path does not exist
        $this->testPath($path);

        if (is_file($path)) {
            // If the path exists and if is a file so stream it
            return $this->streamFile($path);
        } else {
            // If the path exists but is a directory so return list files
            return $this->listItemsInADirectory($path);
        }
    }

    /**
     * Determines the path to the file or to the directory
     *
     * @param  string $startPath The start of the path (ANIS admin, instance or dataset)
     * @return string The full path
     */
    private function getPath(string $startPath): string
    {
        if (array_key_exists('fpath', $this->args)) {
            $path = $startPath . $this->args['fpath'];
        } else {
            $path = $startPath;
        }
        return $path;
    }

    /**
     * If the path does not exist, an error is returned
     *
     * @param string $path The full path
     */
    private function testPath(string $path): void
    {
        if (!file_exists($path)) {
            throw new HttpNotFoundException(
                $this->request,
                'The requested path is not found'
            );
        }
    }

    /**
     * Creates a stream for the file and returns it
     *
     * @param string $path The full file path
     */
    private function streamFile(string $path): Response
    {
        $streamFactory = new StreamFactory();
        $stream = $streamFactory->createStreamFromFile($path, 'r');

        return $this->response->withBody($stream)
            ->withHeader('Content-Type', mime_content_type($path))
            ->withHeader('Content-Length', filesize($path));
    }

    /**
     * Lists the items in a directory and returns it
     *
     * @param string $path The full directory path
     */
    private function listItemsInADirectory(string $path): Response
    {
        $files = [];

        foreach (scandir($path) as $file) {
            $files[] = $this->getFileInfo($path, $file);
        }

        return $this->respond($files);
    }

    /**
     * Returns an array with informations about the file
     *
     * @param string $path The full directory path
     * @param string $file The file to be scanned
     *
     * @return array Informations about the file
     */
    private function getFileInfo(string $path, $file): array
    {
        $type = filetype($path . DIRECTORY_SEPARATOR . $file);
        if ($type === 'link') {
            $targetFile = readlink($path . DIRECTORY_SEPARATOR . $file);
            if ($targetFile[0] !== DIRECTORY_SEPARATOR) {
                $targetFile = $path . DIRECTORY_SEPARATOR . $targetFile;
            }
            if (file_exists($targetFile)) {
                $size = filesize($targetFile);
                $type = filetype($targetFile);
                $mimetype = mime_content_type($targetFile);
            } else {
                $size = 0;
                $type = 'broken_link';
                $mimetype = 'unknown';
            }
        } else {
            $size = filesize($path . DIRECTORY_SEPARATOR . $file);
            $mimetype = mime_content_type($path . DIRECTORY_SEPARATOR . $file);
        }

        return [
            'name' => $file,
            'size' => $size,
            'type' => $type,
            'mimetype' => $mimetype
        ];
    }
}
