<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\IO;

use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractImport
{
    protected EntityManagerInterface $em;
    protected bool $flush;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->flush = true;
    }

    public function setFlush(bool $flush): AbstractImport
    {
        $this->flush = $flush;
        return $this;
    }
}
