<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\IO;

use Doctrine\ORM\EntityManagerInterface;
use App\Specification\Factory\IInputFilterFactory;

abstract class AbstractCheckImport
{
    protected EntityManagerInterface $em;
    protected IInputFilterFactory $inputFilterFactory;
    protected array $errorsMessages;
    protected bool $valid;

    public function __construct(EntityManagerInterface $em, IInputFilterFactory $inputFilterFactory)
    {
        $this->em = $em;
        $this->inputFilterFactory = $inputFilterFactory;
    }

    /**
     * Returns error messages
     *
     * @return array
     */
    public function getErrorsMessages(): array
    {
        return $this->errorsMessages;
    }

    protected function checkSpecification(
        string $specName,
        array $data,
        $args = [],
        $checkNoRecordExists = false,
        $checkRecordExists = false
    ): void {
        if (
            !$this->inputFilterFactory->isValid(
                $specName,
                $this->em,
                $args,
                $data,
                $checkNoRecordExists,
                $checkRecordExists
            )
        ) {
            array_push(
                $this->errorsMessages,
                [$specName => $this->inputFilterFactory->getInputFilter()->getMessages()]
            );
            $this->valid = false;
        }
    }
}
