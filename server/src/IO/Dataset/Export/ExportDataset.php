<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\IO\Dataset\Export;

use App\Entity\Dataset;
use App\Entity\File;
use App\Entity\Image;
use App\Entity\CriteriaFamily;
use App\Entity\OutputFamily;
use App\Entity\Attribute;
use App\Entity\DatasetGroup;

final class ExportDataset implements IExportDataset
{
    /**
     * The Dataset that user wants to export
     */
    private Dataset $dataset;

    /**
     * Call this function to returns full dataset information (array)
     *
     * @return array Full information about the dataset
     */
    public function export(Dataset $dataset): array
    {
        $this->dataset = $dataset;
        $exportDataset = $this->dataset->jsonSerialize();
        $exportDataset['cone_search_config'] = $this->addConeSearchConfig();
        $exportDataset['detail_config'] = $this->addDetailConfig();
        $exportDataset['alias_config'] = $this->addAliasConfig();
        $exportDataset['groups'] = $this->addGroups();
        $exportDataset['files'] = $this->addFiles();
        $exportDataset['images'] = $this->addImages();
        $exportDataset['criteria_families'] = $this->addCriteriaFamilies();
        $exportDataset['output_families'] = $this->addOutputFamilies();
        $exportDataset['attributes'] = $this->addAttributes();
        return $exportDataset;
    }

    private function addConeSearchConfig(): ?array
    {
        if (!is_null($this->dataset->getConeSearchConfig())) {
            return $this->dataset->getConeSearchConfig()->jsonSerialize();
        } else {
            return null;
        }
    }

    private function addDetailConfig(): ?array
    {
        if (!is_null($this->dataset->getDetailConfig())) {
            return $this->dataset->getDetailConfig()->jsonSerialize();
        } else {
            return null;
        }
    }

    private function addAliasConfig(): ?array
    {
        if (!is_null($this->dataset->getAliasConfig())) {
            return $this->dataset->getAliasConfig()->jsonSerialize();
        } else {
            return null;
        }
    }

    private function addGroups(): ?array
    {
        if (!is_null($this->dataset->getGroups())) {
            return array_map(
                fn(DatasetGroup $group) => $group->jsonSerialize(),
                $this->dataset->getGroups()->toArray()
            );
        } else {
            return null;
        }
    }

    private function addFiles(): ?array
    {
        if (!is_null($this->dataset->getFiles())) {
            return array_map(fn(File $file) => $file->jsonSerialize(), $this->dataset->getFiles()->toArray());
        } else {
            return null;
        }
    }

    private function addImages(): ?array
    {
        if (!is_null($this->dataset->getImages())) {
            return array_map(fn(Image $image) => $image->jsonSerialize(), $this->dataset->getImages()->toArray());
        } else {
            return null;
        }
    }

    private function addCriteriaFamilies(): ?array
    {
        if (!is_null($this->dataset->getCriteriaFamilies())) {
            return array_map(
                fn(CriteriaFamily $criteriaFamily) => $criteriaFamily->jsonSerialize(),
                $this->dataset->getCriteriaFamilies()->toArray()
            );
        } else {
            return null;
        }
    }

    private function addOutputFamilies(): ?array
    {
        if (!is_null($this->dataset->getOutputFamilies())) {
            return array_map(
                fn(OutputFamily $outputFamily) => $outputFamily->jsonSerialize(),
                $this->dataset->getOutputFamilies()->toArray()
            );
        } else {
            return null;
        }
    }

    private function addAttributes(): ?array
    {
        if (!is_null($this->dataset->getOutputFamilies())) {
            return array_map(
                fn(Attribute $attribute) => $attribute->jsonSerialize(),
                $this->dataset->getAttributes()->toArray()
            );
        } else {
            return null;
        }
    }
}
