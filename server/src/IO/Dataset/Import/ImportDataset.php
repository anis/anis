<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\IO\Dataset\Import;

use App\IO\AbstractImport;
use App\Actions\Instance\Dataset\DatasetTrait;
use App\Actions\Instance\Dataset\Config\DatasetConfigTrait;
use App\Actions\Instance\Dataset\File\FileTrait;
use App\Actions\Instance\Dataset\File\ImageTrait;
use App\Actions\Instance\Dataset\Criteria\CriteriaFamilyTrait;
use App\Actions\Instance\Dataset\Output\OutputFamilyTrait;
use App\Actions\Instance\Dataset\Output\OutputCategoryTrait;
use App\Actions\Instance\Dataset\Attribute\AttributeTrait;
use App\Actions\Instance\Dataset\Group\DatasetGroupTrait;
use App\Entity\Instance;
use App\Entity\Dataset;
use App\Entity\ConeSearchConfig;
use App\Entity\DetailConfig;
use App\Entity\AliasConfig;
use App\Entity\File;
use App\Entity\Image;
use App\Entity\CriteriaFamily;
use App\Entity\OutputFamily;
use App\Entity\OutputCategory;
use App\Entity\Attribute;
use App\Entity\DatasetGroup;

class ImportDataset extends AbstractImport implements IImportDataset
{
    use DatasetTrait;
    use DatasetConfigTrait;
    use FileTrait;
    use ImageTrait;
    use CriteriaFamilyTrait;
    use OutputFamilyTrait;
    use OutputCategoryTrait;
    use AttributeTrait;
    use DatasetGroupTrait;

    public function import(array $data, Instance $instance): Dataset
    {
        $dataset = $this->importDataset($data, $instance);
        $this->importConeSearchConfig($data['cone_search_config'], $dataset);
        $this->importDetailConfig($data['detail_config'], $dataset);
        $this->importAliasConfig($data['alias_config'], $dataset);
        $this->importGroups($data['groups'], $dataset);
        $this->importFiles($data['files'], $dataset);
        $this->importImages($data['images'], $dataset);
        $this->importCriteriaFamilies($data['criteria_families'], $dataset);
        $this->importOutputFamilies($data['output_families'], $dataset);
        $this->importAttributes($data['attributes'], $instance, $dataset);
        if ($this->flush) {
            $this->em->flush();
        }
        return $dataset;
    }

    private function importDataset(array $data, Instance $instance): Dataset
    {
        $database = $this->em->find('App\Entity\Database', [
            'instance' => $instance,
            'id' => $data['id_database']
        ]);
        $datasetFamily = $this->em->find('App\Entity\DatasetFamily', [
            'instance' => $instance,
            'id' => $data['id_dataset_family']
        ]);

        $dataset = new Dataset($instance, $data['name']);
        $this->hydrateDataset($dataset, $data);
        $dataset->setDatabase($database);
        $dataset->setDatasetFamily($datasetFamily);

        $this->em->persist($dataset);

        return $dataset;
    }

    private function importConeSearchConfig(?array $data, Dataset $dataset): void
    {
        if (!is_null($data)) {
            $coneSearchConfig = new ConeSearchConfig($dataset);
            $this->hydrateConeSearchConfig($coneSearchConfig, $data);

            $this->em->persist($coneSearchConfig);
        }
    }

    private function importDetailConfig(?array $data, Dataset $dataset): void
    {
        if (!is_null($data)) {
            $detailConfig = new DetailConfig($dataset);
            $this->hydrateDetailConfig($detailConfig, $data);

            $this->em->persist($detailConfig);
        }
    }

    private function importAliasConfig(?array $data, Dataset $dataset): void
    {
        if (!is_null($data)) {
            $aliasConfig = new AliasConfig($dataset);
            $this->hydrateAliasConfig($aliasConfig, $data);

            $this->em->persist($aliasConfig);
        }
    }

    private function importGroups(?array $data, Dataset $dataset): void
    {
        if (!is_null($data)) {
            foreach ($data as $dataGroup) {
                $datasetGroup = new DatasetGroup($dataset, $dataGroup['role']);
                $this->hydrateDatasetGroup($datasetGroup, $dataGroup);

                $this->em->persist($datasetGroup);
            }
        }
    }

    private function importFiles(?array $data, Dataset $dataset): void
    {
        if (!is_null($data)) {
            foreach ($data as $dataFile) {
                $file = new File($dataset, $dataFile['id']);
                $this->hydrateFile($file, $dataFile);

                $this->em->persist($file);
            }
        }
    }

    private function importImages(?array $data, Dataset $dataset): void
    {
        if (!is_null($data)) {
            foreach ($data as $dataImage) {
                $image = new Image($dataset, $dataImage['id']);
                $this->hydrateImage($image, $dataImage);

                $this->em->persist($image);
            }
        }
    }

    private function importCriteriaFamilies(?array $data, Dataset $dataset): void
    {
        if (!is_null($data)) {
            foreach ($data as $dataCriteriaFamily) {
                $criteriaFamily = new CriteriaFamily($dataset, $dataCriteriaFamily['id']);
                $this->hydrateCriteriaFamily($criteriaFamily, $dataCriteriaFamily);

                $this->em->persist($criteriaFamily);
            }
        }
    }

    private function importOutputFamilies(?array $data, Dataset $dataset): void
    {
        if (!is_null($data)) {
            foreach ($data as $dataOutputFamily) {
                $outputFamily = new OutputFamily($dataset, $dataOutputFamily['id']);
                $this->hydrateOutputFamily($outputFamily, $dataOutputFamily);

                $this->em->persist($outputFamily);
                $this->importOutputCategories($dataOutputFamily['output_categories'], $outputFamily);
            }
        }
    }

    private function importOutputCategories(?array $data, OutputFamily $outputFamily): void
    {
        if (!is_null($data)) {
            foreach ($data as $dataOutputCategory) {
                $outputCategory = new OutputCategory($outputFamily, $dataOutputCategory['id']);
                $this->hydrateOutputCategory($outputCategory, $dataOutputCategory);

                $this->em->persist($outputCategory);
            }
        }
    }

    private function importAttributes(?array $data, Instance $instance, Dataset $dataset): void
    {
        if (!is_null($data)) {
            foreach ($data as $dataAttribute) {
                $criteriaFamily = $this->getCriteriaFamily(
                    $instance->getName(),
                    $dataset->getName(),
                    $dataAttribute['id_criteria_family']
                );
                $outputCategory = $this->getOutputCategory(
                    $instance->getName(),
                    $dataset->getName(),
                    $dataAttribute['id_output_category']
                );
                $detailOutputCategory = $this->getOutputCategory(
                    $instance->getName(),
                    $dataset->getName(),
                    $dataAttribute['id_detail_output_category']
                );
                $attribute = new Attribute($dataset, $dataAttribute['id']);
                $this->hydrateAttribute(
                    $attribute,
                    $dataAttribute,
                    $criteriaFamily,
                    $outputCategory,
                    $detailOutputCategory
                );

                $this->em->persist($attribute);
            }
        }
    }
}
