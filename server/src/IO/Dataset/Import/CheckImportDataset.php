<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\IO\Dataset\Import;

use App\IO\AbstractCheckImport;

class CheckImportDataset extends AbstractCheckImport implements ICheckImportDataset
{
    public function check(array $data, string $instanceName): bool
    {
        $this->valid = true;
        $this->errorsMessages = [];
        $this->checkDataset($data, $instanceName);
        $this->checkConeSearchConfig($data['cone_search_config']);
        $this->checkDetailConfig($data['detail_config']);
        $this->checkAliasConfig($data['alias_config']);
        $this->checkGroups($data['groups'], $instanceName, $data['name']);
        $this->checkFiles($data['files'], $instanceName, $data['name']);
        $this->checkImages($data['images'], $instanceName, $data['name']);
        $this->checkCriteriaFamilies($data['criteria_families'], $instanceName, $data['name']);
        $this->checkOutputFamilies($data['output_families'], $instanceName, $data['name']);
        $this->checkAttributes($data['attributes'], $instanceName, $data['name']);
        return $this->valid;
    }

    private function checkDataset(array $data, string $instanceName): void
    {
        $this->checkSpecification('dataset', $data, ['name' => $instanceName], true, false);
    }

    private function checkConeSearchConfig(?array $coneSearchConfig): void
    {
        if (!is_null($coneSearchConfig)) {
            $this->checkSpecification('cone_search_config', $coneSearchConfig);
        }
    }

    private function checkDetailConfig(?array $detailConfig): void
    {
        if (!is_null($detailConfig)) {
            $this->checkSpecification('detail_config', $detailConfig);
        }
    }

    private function checkAliasConfig(?array $aliasConfig): void
    {
        if (!is_null($aliasConfig)) {
            $this->checkSpecification('alias_config', $aliasConfig);
        }
    }

    private function checkGroups(?array $groups, string $instanceName, string $datasetName): void
    {
        if (!is_null($groups)) {
            foreach ($groups as $group) {
                $this->checkSpecification('dataset_group', $group, ['name' => $instanceName, 'dname' => $datasetName]);
            }
        }
    }

    private function checkFiles(?array $files, string $instanceName, string $datasetName): void
    {
        if (!is_null($files)) {
            foreach ($files as $file) {
                $this->checkSpecification('file', $file, ['name' => $instanceName, 'dname' => $datasetName]);
            }
        }
    }

    private function checkImages(?array $images, string $instanceName, string $datasetName): void
    {
        if (!is_null($images)) {
            foreach ($images as $image) {
                $this->checkSpecification('image', $image, ['name' => $instanceName, 'dname' => $datasetName]);
            }
        }
    }

    private function checkCriteriaFamilies(?array $criteriaFamilies, string $instanceName, string $datasetName): void
    {
        if (!is_null($criteriaFamilies)) {
            foreach ($criteriaFamilies as $criteriaFamily) {
                $this->checkSpecification(
                    'criteria_family',
                    $criteriaFamily,
                    ['name' => $instanceName, 'dname' => $datasetName]
                );
            }
        }
    }

    private function checkOutputFamilies(?array $outputFamilies, string $instanceName, string $datasetName): void
    {
        if (!is_null($outputFamilies)) {
            foreach ($outputFamilies as $outputFamily) {
                $this->checkOutputCategories(
                    $outputFamily['output_categories'],
                    $instanceName,
                    $datasetName,
                    $outputFamily['id']
                );
                $this->checkSpecification(
                    'output_family',
                    $outputFamily,
                    ['name' => $instanceName, 'dname' => $datasetName]
                );
            }
        }
    }

    private function checkOutputCategories(
        ?array $outputCategories,
        string $instanceName,
        string $datasetName,
        int $outputFamilyId
    ): void {
        if (!is_null($outputCategories)) {
            foreach ($outputCategories as $outputCategory) {
                $this->checkSpecification('output_category', $outputCategory, [
                    'name' => $instanceName,
                    'dname' => $datasetName,
                    'id' => $outputFamilyId
                ]);
            }
        }
    }

    private function checkAttributes(?array $attributes, string $instanceName, string $datasetName): void
    {
        if (!is_null($attributes)) {
            foreach ($attributes as $attribute) {
                $this->checkSpecification('attribute', $attribute, ['name' => $instanceName, 'dname' => $datasetName]);
            }
        }
    }
}
