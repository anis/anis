<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\IO\Instance\Import;

interface ICheckImportInstance
{
    public function check(array $data): bool;
    public function getErrorsMessages(): array;
}
