<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\IO\Instance\Import;

use Doctrine\ORM\EntityManagerInterface;
use App\IO\AbstractImport;
use App\IO\Dataset\Import\IImportDataset;
use App\Actions\Instance\Config\DesignConfigTrait;
use App\Actions\Instance\Database\DatabaseTrait;
use App\Actions\Instance\Family\DatasetFamilyTrait;
use App\Actions\Instance\Group\InstanceGroupTrait;
use App\Actions\Instance\InstanceTrait;
use App\Actions\Instance\Menu\MenuItemTrait;
use App\Entity\Database;
use App\Entity\DatasetFamily;
use App\Entity\Instance;
use App\Entity\DesignConfig;
use App\Entity\InstanceGroup;
use App\Entity\MenuFamily;

class ImportInstance extends AbstractImport implements IImportInstance
{
    use InstanceTrait;
    use DesignConfigTrait;
    use InstanceGroupTrait;
    use DatabaseTrait;
    use DatasetFamilyTrait;
    use MenuItemTrait;

    private IImportDataset $importDataset;

    public function __construct(EntityManagerInterface $em, IImportDataset $importDataset)
    {
        $this->importDataset = $importDataset;
        parent::__construct($em);
    }

    public function import(array $data): Instance
    {
        $instance = $this->importInstance($data);
        $this->importDesignConfig($data['design_config'], $instance);
        $this->importGroups($data['groups'], $instance);
        $this->importDatabases($data['databases'], $instance);
        $this->importDatasetFamilies($data['dataset_families'], $instance);
        $this->importMenuItems($data['menu_items'], $instance);
        $this->importDatasets($data['datasets'], $instance);
        if ($this->flush) {
            $this->em->flush();
        }
        return $instance;
    }

    private function importInstance(array $data): Instance
    {
        $instance = new Instance($data['name'], $data['label']);
        $this->hydrateInstance($instance, $data);

        $this->em->persist($instance);

        return $instance;
    }

    private function importDesignConfig(?array $data, Instance $instance): void
    {
        $designConfig = new DesignConfig($instance);
        $this->hydrateDesignConfig($designConfig, $data);

        $this->em->persist($designConfig);
    }

    private function importGroups(?array $data, Instance $instance): void
    {
        if (!is_null($data)) {
            foreach ($data as $dataGroup) {
                $instanceGroup = new InstanceGroup($instance, $dataGroup['role']);
                $this->hydrateInstanceGroup($instanceGroup, $dataGroup);

                $this->em->persist($instanceGroup);
            }
        }
    }

    private function importDatabases(?array $data, Instance $instance): void
    {
        if (!is_null($data)) {
            foreach ($data as $dataDatabase) {
                $database = new Database($instance, $dataDatabase['id']);
                $this->hydrateDatabase($database, $dataDatabase);

                $this->em->persist($database);
            }
        }
    }

    private function importDatasetFamilies(?array $data, Instance $instance): void
    {
        if (!is_null($data)) {
            foreach ($data as $dataDatasetFamily) {
                $datasetFamily = new DatasetFamily($instance, $dataDatasetFamily['id']);
                $this->hydrateDatasetFamily($datasetFamily, $dataDatasetFamily);

                $this->em->persist($datasetFamily);
            }
        }
    }

    private function importMenuItems(?array $data, Instance $instance, ?MenuFamily $menuFamily = null): void
    {
        if (!is_null($data)) {
            foreach ($data as $dataMenuItem) {
                $class = ucfirst(lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $dataMenuItem['type'])))));
                $fullClass = '\App\Entity\\' . $class;
                $hydrateFn = 'hydrate' . $class;
                $menuItem = new $fullClass($instance, $dataMenuItem['id'], $menuFamily);
                $this->$hydrateFn($menuItem, $dataMenuItem);
                if ($dataMenuItem['type'] === 'menu_family') {
                    $this->importMenuItems($dataMenuItem['items'], $instance, $menuItem);
                }
                $this->em->persist($menuItem);
            }
        }
    }

    private function importDatasets(?array $data, Instance $instance): void
    {
        if (!is_null($data)) {
            foreach ($data as $dataDataset) {
                $this->importDataset->import($dataDataset, $instance);
            }
        }
    }
}
