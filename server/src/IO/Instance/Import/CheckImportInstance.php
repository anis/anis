<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\IO\Instance\Import;

use Doctrine\ORM\EntityManagerInterface;
use App\Specification\Factory\IInputFilterFactory;
use App\IO\AbstractCheckImport;
use App\IO\Dataset\Import\ICheckImportDataset;

class CheckImportInstance extends AbstractCheckImport implements ICheckImportInstance
{
    private ICheckImportDataset $checkImportDataset;

    public function __construct(
        EntityManagerInterface $em,
        IInputFilterFactory $inputFilterFactory,
        ICheckImportDataset $checkImportDataset
    ) {
        $this->checkImportDataset = $checkImportDataset;
        parent::__construct($em, $inputFilterFactory);
    }

    public function check(array $data): bool
    {
        $this->valid = true;
        $this->errorsMessages = [];
        $this->checkInstance($data);
        $this->checkDesignConfig($data['design_config']);
        $this->checkGroups($data['groups'], $data['name']);
        $this->checkDatabases($data['databases'], $data['name']);
        $this->checkDatasetFamilies($data['dataset_families'], $data['name']);
        $this->checkMenuItems($data['menu_items'], $data['name']);
        $this->checkDatasets($data['datasets'], $data['name']);
        return $this->valid;
    }

    private function checkInstance(array $data): void
    {
        $this->checkSpecification('instance', $data, [], true, false);
    }

    private function checkDesignConfig(?array $designConfig): void
    {
        if (!is_null($designConfig)) {
            $this->checkSpecification('design_config', $designConfig);
        }
    }

    private function checkGroups(?array $groups, string $instanceName): void
    {
        if (!is_null($groups)) {
            foreach ($groups as $group) {
                $this->checkSpecification('instance_group', $group, ['name' => $instanceName]);
            }
        }
    }

    private function checkDatabases(?array $databases, string $instanceName): void
    {
        if (!is_null($databases)) {
            foreach ($databases as $database) {
                $this->checkSpecification('database', $database, ['name' => $instanceName]);
            }
        }
    }

    private function checkDatasetFamilies(?array $datasetFamilies, string $instanceName): void
    {
        if (!is_null($datasetFamilies)) {
            foreach ($datasetFamilies as $datasetFamily) {
                $this->checkSpecification('dataset_family', $datasetFamily, ['name' => $instanceName]);
            }
        }
    }

    private function checkMenuItems(?array $menuItems, string $instanceName): void
    {
        if (!is_null($menuItems)) {
            foreach ($menuItems as $menuItem) {
                if ($menuItem['type'] === 'menu_family') {
                    $this->checkMenuItems($menuItem['items'], $instanceName);
                }
                // Check menu type
                if (!in_array($menuItem['type'], ['menu_family', 'webpage', 'url'])) {
                    array_push(
                        $this->errorsMessages,
                        [$menuItem['type'] => 'Menu item param type must be equal to menu_family, webpage or url']
                    );
                    $this->valid = false;
                } else {
                    $this->checkSpecification($menuItem['type'], $menuItem, ['name' => $instanceName]);
                }
            }
        }
    }

    private function checkDatasets(?array $datasets, string $instanceName)
    {
        if (!is_null($datasets)) {
            foreach ($datasets as $dataset) {
                $valid = $this->checkImportDataset->check($dataset, $instanceName);
                if (!$valid) {
                    array_push(
                        $this->errorsMessages,
                        [$dataset['name'] => $this->checkImportDataset->getErrorsMessages()]
                    );
                    $this->valid = false;
                }
            }
        }
    }
}
