<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\IO\Instance\Export;

use App\Entity\Instance;
use App\Entity\Dataset;
use App\Entity\Database;
use App\Entity\DatasetFamily;
use App\Entity\InstanceGroup;
use App\Entity\MenuItem;
use App\IO\Dataset\Export\IExportDataset;

final class ExportInstance implements IExportInstance
{
    /**
     * The Instance that user wants to export
     */
    private Instance $instance;

    private IExportDataset $exportDataset;

    public function __construct(IExportDataset $exportDataset)
    {
        $this->exportDataset = $exportDataset;
    }

    public function export(Instance $instance): array
    {
        $this->instance = $instance;
        $exportInstance = $this->instance->jsonSerialize();
        $exportInstance['design_config'] = $this->addDesignConfig();
        $exportInstance['groups'] = $this->addGroups();
        $exportInstance['databases'] = $this->addDatabases();
        $exportInstance['dataset_families'] = $this->addDatasetFamilies();
        $exportInstance['menu_items'] = $this->addMenuItems();
        $exportInstance['datasets'] = $this->addDatasets();
        return $exportInstance;
    }

    private function addDesignConfig(): ?array
    {
        if (!is_null($this->instance->getDesignConfig())) {
            return $this->instance->getDesignConfig()->jsonSerialize();
        } else {
            return null;
        }
    }

    private function addGroups(): ?array
    {
        if (!is_null($this->instance->getGroups())) {
            return array_map(
                fn(InstanceGroup $group) => $group->jsonSerialize(),
                $this->instance->getGroups()->toArray()
            );
        } else {
            return null;
        }
    }

    private function addDatabases(): ?array
    {
        if (!is_null($this->instance->getDatabases())) {
            return array_map(
                fn(Database $database) => $database->jsonSerialize(),
                $this->instance->getDatabases()->toArray()
            );
        } else {
            return null;
        }
    }

    private function addDatasetFamilies(): ?array
    {
        if (!is_null($this->instance->getDatasetFamilies())) {
            return array_map(
                fn(DatasetFamily $datasetFamily) => $datasetFamily->jsonSerialize(),
                $this->instance->getDatasetFamilies()->toArray()
            );
        } else {
            return null;
        }
    }

    private function addMenuItems(): ?array
    {
        if (!is_null($this->instance->getMenuItems())) {
            return array_map(
                fn(MenuItem $menuItem) => $menuItem->jsonSerialize(),
                $this->instance->getMenuItems()->toArray()
            );
        } else {
            return null;
        }
    }

    private function addDatasets(): ?array
    {
        if (!is_null($this->instance->getDatasets())) {
            return array_map(
                fn(Dataset $dataset) => $this->exportDataset->export($dataset),
                $this->instance->getDatasets()->toArray()
            );
        } else {
            return null;
        }
    }
}
