<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Settings;

use App\Settings\SettingsInterface;
use App\Settings\Settings;
use Tests\TestCase;

final class SettingsTest extends TestCase
{
    private SettingsInterface $settings;

    protected function setUp(): void
    {
        $this->settings = new Settings([
            'foo' => 'bar',
            'too' => 'baz'
        ]);
    }

    public function testGetKey(): void
    {
        $this->assertSame('bar', $this->settings->get('foo'));
    }

    public function testGetAll(): void
    {
        $this->assertSame(
            [
                'foo' => 'bar',
                'too' => 'baz'
            ],
            $this->settings->get()
        );
    }
}
