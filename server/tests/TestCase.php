<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests;

use PHPUnit\Framework\TestCase as PHPUnit_TestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\Persistence\ObjectRepository;
use Slim\Psr7\Factory\ServerRequestFactory;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

class TestCase extends PHPUnit_TestCase
{
    protected function createRequest(string $method, string $uri): Request
    {
        return (new ServerRequestFactory())->createServerRequest($method, $uri, [
            'Content-Type' => 'application/json'
        ]);
    }

    protected function createResponse(): Response
    {
        return (new ResponseFactory())->createResponse();
    }

    protected function getObjectRepositoryMock(): ObjectRepository|MockObject
    {
        return $this->createMock(ObjectRepository::class);
    }
}
