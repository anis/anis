<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Specification;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\ORM\EntityManager;
use App\Specification\Validator\NoRecordExists;
use Tests\TestCase;
use App\Specification\DatasetFamily;

final class DatasetFamilyTest extends TestCase
{
    use SpecificationTrait;

    private array $specification;
    private EntityManager|MockObject $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $datasetFamily = new DatasetFamily($this->entityManager, ['name' => 'default'], true, false);
        $this->specification = $datasetFamily->getSpecification();
    }

    public function testSpecification(): void
    {
        $this->assertSame(
            $this->specification,
            $this->getSpecificationToTest()
        );
    }

    protected function getSpecificationToTest(): array
    {
        $id = $this->getPositiveDigits();
        $id['validators'][] = [
            'name' => NoRecordExists::class,
            'options' => [
                'em' => $this->entityManager,
                'entityClassName' => 'App\Entity\DatasetFamily',
                'pkeyType' => NoRecordExists::COMPOSITE_PKEY,
                'columnName' => 'id',
                'extraId' => [ 'instance' => 'default' ]
            ]
        ];
        $specification = [
            'id' => $id,
            'label' => $this->getString(),
            'display' => $this->getPositiveDigits(),
            'opened' => $this->getRequiredBoolean()
        ];
        $callback = $this->specification['opened']['validators'][0]['options']['callback'];
        $specification['opened']['validators'][0]['options']['callback'] = $callback;

        return $specification;
    }
}
