<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Specification;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\ORM\EntityManager;
use App\Specification\Validator\NoRecordExists;
use Tests\TestCase;
use App\Specification\Instance;

final class InstanceTest extends TestCase
{
    use SpecificationTrait;

    private array $specification;
    private EntityManager|MockObject $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $instance = new Instance($this->entityManager, [], true, false);
        $this->specification = $instance->getSpecification();
    }

    public function testSpecification(): void
    {
        $this->assertSame(
            $this->specification,
            $this->getSpecificationToTest()
        );
    }

    protected function getSpecificationToTest(): array
    {
        $name = $this->getString();
        $name['validators'][] = [
            'name' => NoRecordExists::class,
            'options' => [
                'em' => $this->entityManager,
                'entityClassName' => 'App\Entity\Instance',
                'pkeyType' => NoRecordExists::SIMPLE_PKEY,
                'columnName' => 'name'
            ]
        ];
        $specification = [
            'name' => $name,
            'label' => $this->getString(),
            'description' => $this->getString(),
            'scientific_manager' => $this->getString(),
            'instrument' => $this->getString(),
            'wavelength_domain' => $this->getString(),
            'display' => $this->getPositiveDigits(),
            'data_path' => $this->getString(false),
            'files_path' => $this->getString(false),
            'public' => $this->getRequiredBoolean(),
            'portal_logo' => $this->getString(false),
            'portal_color' => $this->getHtmlColor(false),
            'default_redirect' => $this->getString(false)
        ];
        $callback = $this->specification['public']['validators'][0]['options']['callback'];
        $specification['public']['validators'][0]['options']['callback'] = $callback;

        return $specification;
    }
}
