<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Specification;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\ORM\EntityManager;
use Tests\TestCase;
use App\Specification\ConeSearchConfig;

final class ConeSearchConfigTest extends TestCase
{
    use SpecificationTrait;

    private array $specification;
    private EntityManager|MockObject $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $coneSearchConfig = new ConeSearchConfig($this->entityManager, [], false, false);
        $this->specification = $coneSearchConfig->getSpecification();
    }

    public function testSpecification(): void
    {
        $this->assertSame(
            $this->specification,
            $this->getSpecificationToTest()
        );
    }

    protected function getSpecificationToTest(): array
    {
        $specification = [
            'enabled' => $this->getRequiredBoolean(),
            'opened' => $this->getRequiredBoolean(),
            'column_ra' => $this->getPositiveDigits(),
            'column_dec' => $this->getPositiveDigits(),
            'resolver_enabled' => $this->getRequiredBoolean(),
            'default_ra' => $this->getRa(false),
            'default_dec' => $this->getDec(false),
            'default_radius' => $this->getRadius(),
            'default_ra_dec_unit' => $this->getString(),
            'plot_enabled' => $this->getRequiredBoolean()
        ];
        $callbackEnabled = $this->specification['enabled']['validators'][0]['options']['callback'];
        $callbackOpened = $this->specification['opened']['validators'][0]['options']['callback'];
        $callbackResolverEnabled = $this->specification['resolver_enabled']['validators'][0]['options']['callback'];
        $callbackPlotEnabled = $this->specification['plot_enabled']['validators'][0]['options']['callback'];

        $specification['enabled']['validators'][0]['options']['callback'] = $callbackEnabled;
        $specification['opened']['validators'][0]['options']['callback'] = $callbackOpened;
        $specification['resolver_enabled']['validators'][0]['options']['callback'] = $callbackResolverEnabled;
        $specification['plot_enabled']['validators'][0]['options']['callback'] = $callbackPlotEnabled;

        return $specification;
    }

    protected function getRadius()
    {
        return [
            'required' => false,
            'filters' => [
                ['name' => 'ToFloat']
            ],
            'validators' => [
                [
                    'name' => 'Between',
                    'options' => [
                        'min' => 0,
                        'max' => 150
                    ]
                ]
            ]
        ];
    }
}
