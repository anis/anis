<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Specification;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\ORM\EntityManager;
use App\Specification\Validator\NoRecordExists;
use App\Specification\Validator\RecordExists;
use Tests\TestCase;
use App\Specification\Dataset;

final class DatasetTest extends TestCase
{
    use SpecificationTrait;

    private array $specification;
    private EntityManager|MockObject $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $dataset = new Dataset($this->entityManager, [ 'name' => 'default' ], true, true);
        $this->specification = $dataset->getSpecification();
    }

    public function testSpecification(): void
    {
        $this->assertSame(
            $this->specification,
            $this->getSpecificationToTest()
        );
    }

    protected function getSpecificationToTest(): array
    {
        $name = $this->getString();
        $name['validators'][] = [
            'name' => NoRecordExists::class,
            'options' => [
                'em' => $this->entityManager,
                'entityClassName' => 'App\Entity\Dataset',
                'pkeyType' => NoRecordExists::COMPOSITE_PKEY,
                'columnName' => 'name',
                'extraId' => [ 'instance' => 'default' ]
            ]
        ];
        $specification = [
            'name' => $name,
            'table_ref' => $this->getString(),
            'primary_key' => $this->getPositiveDigits(),
            'default_order_by' => $this->getPositiveDigits(),
            'default_order_by_direction' => $this->getString(),
            'label' => $this->getString(),
            'description' => $this->getString(),
            'display' => $this->getPositiveDigits(),
            'data_path' => $this->getString(false),
            'public' => $this->getRequiredBoolean(),
            'download_json' => $this->getRequiredBoolean(),
            'download_csv' => $this->getRequiredBoolean(),
            'download_ascii' => $this->getRequiredBoolean(),
            'download_vo' => $this->getRequiredBoolean(),
            'download_fits' => $this->getRequiredBoolean(),
            'server_link_enabled' => $this->getRequiredBoolean(),
            'datatable_enabled' => $this->getRequiredBoolean(),
            'datatable_selectable_rows' => $this->getRequiredBoolean(),
            'id_database' => $this->getIdDatabase(),
            'id_dataset_family' => $this->getIdDatasetFamily()
        ];
        $callbackPublic = $this->specification['public']['validators'][0]['options']['callback'];
        $callbackDownloadJson = $this->specification['download_json']['validators'][0]['options']['callback'];
        $callbackDownloadCsv = $this->specification['download_csv']['validators'][0]['options']['callback'];
        $callbackDownloadAscii = $this->specification['download_ascii']['validators'][0]['options']['callback'];
        $callbackDownloadVo = $this->specification['download_vo']['validators'][0]['options']['callback'];
        $callbackDownloadFits = $this->specification['download_fits']['validators'][0]['options']['callback'];
        $callbackSLE = $this->specification['server_link_enabled']['validators'][0]['options']['callback'];
        $callbackDatatableEnabled = $this->specification['datatable_enabled']['validators'][0]['options']['callback'];
        $callbackDSR = $this->specification['datatable_selectable_rows']['validators'][0]['options']['callback'];

        $specification['public']['validators'][0]['options']['callback'] = $callbackPublic;
        $specification['download_json']['validators'][0]['options']['callback'] = $callbackDownloadJson;
        $specification['download_csv']['validators'][0]['options']['callback'] = $callbackDownloadCsv;
        $specification['download_ascii']['validators'][0]['options']['callback'] = $callbackDownloadAscii;
        $specification['download_vo']['validators'][0]['options']['callback'] = $callbackDownloadVo;
        $specification['download_fits']['validators'][0]['options']['callback'] = $callbackDownloadFits;
        $specification['server_link_enabled']['validators'][0]['options']['callback'] = $callbackSLE;
        $specification['datatable_enabled']['validators'][0]['options']['callback'] = $callbackDatatableEnabled;
        $specification['datatable_selectable_rows']['validators'][0]['options']['callback'] = $callbackDSR;

        return $specification;
    }

    private function getIdDatabase(): array
    {
        $spec = $this->getPositiveDigits();
        $spec['validators'][] = [
            'name' => RecordExists::class,
            'options' => [
                'em' => $this->entityManager,
                'entityClassName' => 'App\Entity\Database',
                'pkeyType' => RecordExists::COMPOSITE_PKEY,
                'columnName' => 'id',
                'extraId' => [ 'instance' => 'default' ]
            ]
        ];

        return $spec;
    }

    private function getIdDatasetFamily(): array
    {
        $spec = $this->getPositiveDigits();
        $spec['validators'][] = [
            'name' => RecordExists::class,
            'options' => [
                'em' => $this->entityManager,
                'entityClassName' => 'App\Entity\DatasetFamily',
                'pkeyType' => RecordExists::COMPOSITE_PKEY,
                'columnName' => 'id',
                'extraId' => [ 'instance' => 'default' ]
            ]
        ];

        return $spec;
    }
}
