<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Specification;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\ORM\EntityManager;
use Tests\TestCase;
use App\Specification\AliasConfig;

final class AliasConfigTest extends TestCase
{
    use SpecificationTrait;

    private array $specification;
    private EntityManager|MockObject $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $aliasConfig = new AliasConfig($this->entityManager, [], false, false);
        $this->specification = $aliasConfig->getSpecification();
    }

    public function testSpecification(): void
    {
        $this->assertSame(
            $this->specification,
            $this->getSpecificationToTest()
        );
    }

    protected function getSpecificationToTest(): array
    {
        return [
            'table_alias' => $this->getString(),
            'column_alias' => $this->getString(),
            'column_name' => $this->getString(),
            'column_alias_long' => $this->getString()
        ];
    }
}
