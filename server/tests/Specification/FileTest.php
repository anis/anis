<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Specification;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\ORM\EntityManager;
use App\Specification\Validator\NoRecordExists;
use Tests\TestCase;
use App\Specification\File;

final class FileTest extends TestCase
{
    use SpecificationTrait;

    private array $specification;
    private EntityManager|MockObject $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $file = new File($this->entityManager, ['name' => 'default', 'dname' => 'observations'], true, false);
        $this->specification = $file->getSpecification();
    }

    public function testSpecification(): void
    {
        $this->assertSame(
            $this->specification,
            $this->getSpecificationToTest()
        );
    }

    protected function getSpecificationToTest(): array
    {
        $id = $this->getPositiveDigits();
        $id['validators'][] = [
            'name' => NoRecordExists::class,
            'options' => [
                'em' => $this->entityManager,
                'entityClassName' => 'App\Entity\File',
                'pkeyType' => NoRecordExists::CONCATENATE_PKEY,
                'columnName' => 'id',
                'extraId' => [ 'default', 'observations' ]
            ]
        ];
        $specification = [
            'id' => $id,
            'label' => $this->getString(),
            'file_path' => $this->getString(),
            'file_size' => $this->getPositiveDigits(),
            'type' => $this->getString()
        ];

        return $specification;
    }
}
