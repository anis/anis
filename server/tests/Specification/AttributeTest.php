<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Specification;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\ORM\EntityManager;
use Laminas\InputFilter\Factory as LaminasInputFilterFactory;
use App\Specification\Validator\NoRecordExists;
use App\Specification\Validator\RecordExists;
use App\Specification\Validator\Collection;
use Tests\TestCase;
use App\Specification\Attribute;

final class AttributeTest extends TestCase
{
    use SpecificationTrait;

    private array $specification;
    private EntityManager|MockObject $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $attribute = new Attribute($this->entityManager, ['name' => 'default', 'dname' => 'observations'], true, true);
        $this->specification = $attribute->getSpecification();
    }

    public function testSpecification(): void
    {
        $this->assertSame(
            $this->specification,
            $this->getSpecificationToTest()
        );
    }

    protected function getSpecificationToTest(): array
    {
        $id = $this->getPositiveDigits();
        $id['validators'][] = [
            'name' => NoRecordExists::class,
            'options' => [
                'em' => $this->entityManager,
                'entityClassName' => 'App\Entity\Attribute',
                'pkeyType' => NoRecordExists::CONCATENATE_PKEY,
                'columnName' => 'id',
                'extraId' => [ 'default', 'observations' ]
            ]
        ];
        $specification = [
            'id' => $id,
            'name' => $this->getString(),
            'label' => $this->getString(),
            'form_label' => $this->getString(),
            'datatable_label' => $this->getString(false),
            'description' => $this->getString(false),
            'type' => $this->getString(),
            'search_type' => $this->getString(false),
            'operator' => $this->getString(false),
            'dynamic_operator' => $this->getRequiredBoolean(),
            'null_operators_enabled' => $this->getRequiredBoolean(),
            'min' => $this->getString(false),
            'max' => $this->getString(false),
            'options' => $this->getOptions(),
            'placeholder_min' => $this->getString(false),
            'placeholder_max' => $this->getString(false),
            'criteria_display' => $this->getPositiveDigits(false),
            'output_display' => $this->getPositiveDigits(false),
            'selected' => $this->getRequiredBoolean(),
            'renderer' => $this->getString(false),
            'renderer_config' => $this->getRendererConfig(),
            'order_by' => $this->getRequiredBoolean(),
            'archive' => $this->getRequiredBoolean(),
            'detail_display' => $this->getPositiveDigits(false),
            'detail_renderer' => $this->getString(false),
            'detail_renderer_config' => $this->getRendererConfig(),
            'vo_utype' => $this->getString(false),
            'vo_ucd' => $this->getString(false),
            'vo_unit' => $this->getString(false),
            'vo_description' => $this->getString(false),
            'vo_datatype' => $this->getString(false),
            'vo_size' => $this->getString(false),
            'id_criteria_family' => $this->getIdCriteriaFamily(),
            'id_output_category' => $this->getIdOutputCategory(),
            'id_detail_output_category' => $this->getIdOutputCategory()
        ];
        $callbackDynamicOperator = $this->specification['dynamic_operator']['validators'][0]['options']['callback'];
        $cnoe = $this->specification['null_operators_enabled']['validators'][0]['options']['callback'];
        $callbackSelected = $this->specification['selected']['validators'][0]['options']['callback'];
        $callbackOrderBy = $this->specification['order_by']['validators'][0]['options']['callback'];
        $callbackArchive = $this->specification['archive']['validators'][0]['options']['callback'];

        $specification['dynamic_operator']['validators'][0]['options']['callback'] = $callbackDynamicOperator;
        $specification['null_operators_enabled']['validators'][0]['options']['callback'] = $cnoe;
        $specification['selected']['validators'][0]['options']['callback'] = $callbackSelected;
        $specification['order_by']['validators'][0]['options']['callback'] = $callbackOrderBy;
        $specification['archive']['validators'][0]['options']['callback'] = $callbackArchive;

        $callbackOptions = $this->specification['options']['validators'][1]['options']['iff'];
        $specification['options']['validators'][1]['options']['iff'] = $callbackOptions;

        return $specification;
    }

    private function getOptions(): array
    {
        return [
            'required' => false,
            'validators' => [
                [ 'name' => 'IsCountable' ],
                [
                    'name' => Collection::class,
                    'options' => [
                        'iff' => new LaminasInputFilterFactory(),
                        'validator' => [
                            'label' => $this->getString(),
                            'value' => $this->getString(),
                            'display' => $this->getPositiveDigits()
                        ]
                    ]
                ]
            ]
        ];
    }

    private function getRendererConfig(): array
    {
        return [
            'required' => false,
            'validators' => [
                [ 'name' => 'IsCountable' ]
            ]
        ];
    }

    private function getIdCriteriaFamily(): array
    {
        $id = $this->getPositiveDigits(false);
        $id['validators'][] = [
            'name' => RecordExists::class,
            'options' => [
                'em' => $this->entityManager,
                'entityClassName' => 'App\Entity\CriteriaFamily',
                'pkeyType' => RecordExists::CONCATENATE_PKEY,
                'columnName' => 'id',
                'extraId' => [ 'default', 'observations' ]
            ]
        ];

        return $id;
    }

    private function getIdOutputCategory(): array
    {
        $id = $this->getString(false);
        $id['validators'][] = [
            'name' => RecordExists::class,
            'options' => [
                'em' => $this->entityManager,
                'entityClassName' => 'App\Entity\OutputCategory',
                'pkeyType' => RecordExists::CONCATENATE_PKEY,
                'columnName' => 'id',
                'extraId' => [ 'default', 'observations' ]
            ]
        ];

        return $id;
    }
}
