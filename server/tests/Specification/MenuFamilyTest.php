<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Specification;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\ORM\EntityManager;
use App\Specification\Validator\NoRecordExists;
use App\Specification\Validator\RecordExists;
use Tests\TestCase;
use App\Specification\MenuFamily;

final class MenuFamilyTest extends TestCase
{
    use SpecificationTrait;

    private array $specification;
    private EntityManager|MockObject $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $menuFamily = new MenuFamily($this->entityManager, [ 'name' => 'default' ], true, true);
        $this->specification = $menuFamily->getSpecification();
    }

    public function testSpecification(): void
    {
        $this->assertSame(
            $this->specification,
            $this->getSpecificationToTest()
        );
    }

    protected function getSpecificationToTest(): array
    {
        $id = $this->getPositiveDigits();
        $id['validators'][] = [
            'name' => NoRecordExists::class,
            'options' => [
                'em' => $this->entityManager,
                'entityClassName' => 'App\Entity\MenuItem',
                'pkeyType' => NoRecordExists::CONCATENATE_PKEY,
                'columnName' => 'id',
                'extraId' => [ 'default' ]
            ]
        ];
        return [
            'id' => $id,
            'label' => $this->getString(),
            'icon' => $this->getString(false),
            'display' => $this->getPositiveDigits(),
            'id_menu_family' => $this->getIdMenuFamily(),
            'name' => $this->getString()
        ];
    }

    private function getIdMenuFamily(): array
    {
        $spec = $this->getPositiveDigits(false);
        $spec['validators'][] = [
            'name' => RecordExists::class,
            'options' => [
                'em' => $this->entityManager,
                'entityClassName' => 'App\Entity\MenuFamily',
                'pkeyType' => RecordExists::CONCATENATE_PKEY,
                'columnName' => 'id',
                'extraId' => [ 'default' ]
            ]
        ];

        return $spec;
    }
}
