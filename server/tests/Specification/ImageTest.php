<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Specification;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\ORM\EntityManager;
use App\Specification\Validator\NoRecordExists;
use Tests\TestCase;
use App\Specification\Image;

final class ImageTest extends TestCase
{
    use SpecificationTrait;

    private array $specification;
    private EntityManager|MockObject $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $image = new Image($this->entityManager, ['name' => 'default', 'dname' => 'observations'], true, false);
        $this->specification = $image->getSpecification();
    }

    public function testSpecification(): void
    {
        $this->assertSame(
            $this->specification,
            $this->getSpecificationToTest()
        );
    }

    protected function getSpecificationToTest(): array
    {
        $id = $this->getPositiveDigits();
        $id['validators'][] = [
            'name' => NoRecordExists::class,
            'options' => [
                'em' => $this->entityManager,
                'entityClassName' => 'App\Entity\Image',
                'pkeyType' => NoRecordExists::CONCATENATE_PKEY,
                'columnName' => 'id',
                'extraId' => [ 'default', 'observations' ]
            ]
        ];
        $specification = [
            'id' => $id,
            'label' => $this->getString(),
            'file_path' => $this->getString(),
            'hdu_number' => $this->getPositiveDigits(false),
            'file_size' => $this->getPositiveDigits(),
            'ra_min' => $this->getRa(),
            'ra_max' => $this->getRa(),
            'dec_min' => $this->getDec(),
            'dec_max' => $this->getDec(),
            'stretch' => $this->getString(),
            'pmin' => $this->getP(),
            'pmax' => $this->getP()
        ];

        return $specification;
    }

    private function getP()
    {
        return [
            'required' => true,
            'filters' => [
                ['name' => 'ToFloat']
            ],
            'validators' => [
                [
                    'name' => 'Between',
                    'options' => [
                        'min' => 0,
                        'max' => 100
                    ]
                ]
            ]
        ];
    }
}
