<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Specification;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\ORM\EntityManager;
use App\Specification\Validator\NoRecordExists;
use Tests\TestCase;
use App\Specification\Database;

final class DatabaseTest extends TestCase
{
    use SpecificationTrait;

    private array $specification;
    private EntityManager|MockObject $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $database = new Database($this->entityManager, ['name' => 'default'], true, false);
        $this->specification = $database->getSpecification();
    }

    public function testSpecification(): void
    {
        $this->assertSame(
            $this->specification,
            $this->getSpecificationToTest()
        );
    }

    protected function getSpecificationToTest(): array
    {
        $id = $this->getPositiveDigits();
        $id['validators'][] = [
            'name' => NoRecordExists::class,
            'options' => [
                'em' => $this->entityManager,
                'entityClassName' => 'App\Entity\Database',
                'pkeyType' => NoRecordExists::COMPOSITE_PKEY,
                'columnName' => 'id',
                'extraId' => [ 'instance' => 'default' ]
            ]
        ];

        return [
            'id' => $id,
            'label' => $this->getString(),
            'dbname' => $this->getString(false),
            'dbtype' => $this->getString(false),
            'dbhost' => $this->getString(false),
            'dbport' => $this->getDbport(),
            'dblogin' => $this->getString(false),
            'dbpassword' => $this->getString(false),
            'dbdsn_file' => $this->getString(false)
        ];
    }

    protected function getDbport()
    {
        return [
            'required' => false,
            'validators' => [
                ['name' => 'Digits'],
                [
                    'name' => 'Between',
                    'options' => [
                        'min' => 0,
                        'max' => 65535
                    ]
                ]
            ]
        ];
    }
}
