<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Specification;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\ORM\EntityManager;
use Tests\TestCase;
use App\Specification\DetailConfig;

final class DetailConfigTest extends TestCase
{
    use SpecificationTrait;

    private array $specification;
    private EntityManager|MockObject $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $detailConfig = new DetailConfig($this->entityManager, [], false, false);
        $this->specification = $detailConfig->getSpecification();
    }

    public function testSpecification(): void
    {
        $this->assertSame(
            $this->specification,
            $this->getSpecificationToTest()
        );
    }

    protected function getSpecificationToTest(): array
    {
        return [
            'content' => $this->getContent(),
            'style_sheet' => $this->getStyleSheet()
        ];
    }

    protected function getContent(): array
    {
        return [
            'required' => true,
            'filters' => [
                ['name' => 'StringTrim']
            ]
        ];
    }

    protected function getStyleSheet(): array
    {
        return [
            'required' => false,
            'filters' => [
                ['name' => 'StringTrim']
            ]
        ];
    }
}
