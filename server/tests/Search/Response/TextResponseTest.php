<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Search\Response;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\DBAL\Result;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use Tests\TestCase;
use App\Search\Response\TextResponse;
use App\Search\Query\AnisQueryBuilder;
use App\Entity\Attribute;

final class TextResponseTest extends TestCase
{
    public function testGetResponse(): void
    {
        $stmt = $this->getResultMock();
        $stmt->method('fetchAssociative')->willReturnOnConsecutiveCalls([
            'id' => 1,
            'ra' => 102.5,
            'dec' => 0.1
        ], false);

        $id = $this->getAttributeMock();
        $id->method('getLabel')->willReturn('id');
        $ra = $this->getAttributeMock();
        $ra->method('getLabel')->willReturn('ra');
        $dec = $this->getAttributeMock();
        $dec->method('getLabel')->willReturn('dec');

        $doctrineQueryBuilder = $this->getDoctrineQueryBuilderMock();
        $doctrineQueryBuilder->method('executeQuery')->willReturn($stmt);
        $anisQueryBuilder = $this->getAnisQueryBuilderMock();
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $anisQueryBuilder->method('getAttributesSelected')->willReturn([$id, $ra, $dec]);

        $textResponse = new TextResponse(',', 'text/csv');
        $response = $textResponse->getResponse($this->createResponse(), $anisQueryBuilder);
        $lines = explode(PHP_EOL, (string) $response->getBody());
        $this->assertSame('text/csv', $response->getHeaders()['Content-Type'][0]);
        $this->assertSame('id,ra,dec', $lines[0]);
        $this->assertSame('1,102.5,0.1', $lines[1]);
    }

    private function getResultMock(): Result|MockObject
    {
        return $this->createMock(Result::class);
    }

    private function getAttributeMock(): Attribute|MockObject
    {
        return $this->createMock(Attribute::class);
    }

    private function getDoctrineQueryBuilderMock(): DoctrineQueryBuilder|MockObject
    {
        return $this->createMock(DoctrineQueryBuilder::class);
    }

    private function getAnisQueryBuilderMock(): AnisQueryBuilder|MockObject
    {
        return $this->createMock(AnisQueryBuilder::class);
    }
}
