<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Search\Query;

use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;
use App\Search\Query\Select;
use App\Search\Query\AnisQueryBuilder;
use App\Search\Query\SearchQueryException;
use App\Entity\Dataset;
use App\Entity\Attribute;

final class SelectTest extends TestCase
{
    public function testSelect(): void
    {
        $id = $this->getAttribute();
        $id->method('getId')->willReturn(1);
        $ra = $this->getAttribute();
        $ra->method('getId')->willReturn(2);
        $dec = $this->getAttribute();
        $dec->method('getId')->willReturn(3);
        $datasetSelected = $this->getDataset();
        $datasetSelected->method('getAttributes')->willReturn(new ArrayCollection([$id, $ra, $dec]));
        $doctrineQueryBuilder = $this->getDoctrineQueryBuilder();
        $doctrineQueryBuilder->expects($this->once())->method('select');
        $anisQueryBuilder = $this->getAnisQueryBuilder();
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $queryParams = ['a' => '1;2;3'];
        (new Select())($anisQueryBuilder, $datasetSelected, $queryParams);
    }

    public function testSelectException(): void
    {
        $this->expectException(SearchQueryException::class);
        $id = $this->getAttribute();
        $id->method('getId')->willReturn(1);
        $ra = $this->getAttribute();
        $ra->method('getId')->willReturn(2);
        $dec = $this->getAttribute();
        $dec->method('getId')->willReturn(3);
        $datasetSelected = $this->getDataset();
        $datasetSelected->method('getAttributes')->willReturn(new ArrayCollection([$id, $ra, $dec]));
        $datasetSelected->method('getLabel')->willReturn('Test');
        $anisQueryBuilder = $this->getAnisQueryBuilder();
        $queryParams = ['a' => '1;2;3;4'];
        (new Select())($anisQueryBuilder, $datasetSelected, $queryParams);
    }

    private function getAttribute(): Attribute|MockObject
    {
        return $this->createMock(Attribute::class);
    }

    private function getDataset(): Dataset|MockObject
    {
        return $this->createMock(Dataset::class);
    }

    private function getDoctrineQueryBuilder(): DoctrineQueryBuilder|MockObject
    {
        return $this->createMock(DoctrineQueryBuilder::class);
    }

    private function getAnisQueryBuilder(): AnisQueryBuilder|MockObject
    {
        return $this->createMock(AnisQueryBuilder::class);
    }
}
