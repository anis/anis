<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Search\Query;

use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;
use App\Search\Query\From;
use App\Search\Query\AnisQueryBuilder;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use App\Entity\Dataset;

final class FromTest extends TestCase
{
    public function testFrom(): void
    {
        $doctrineQueryBuilder = $this->getDoctrineQueryBuilder();
        $doctrineQueryBuilder->expects($this->once())->method('from');
        $anisQueryBuilder = $this->getAnisQueryBuilder();
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $datasetSelected = $this->getDataset();
        (new From())($anisQueryBuilder, $datasetSelected, []);
    }

    private function getDataset(): Dataset|MockObject
    {
        return $this->createMock(Dataset::class);
    }

    private function getDoctrineQueryBuilder(): DoctrineQueryBuilder|MockObject
    {
        return $this->createMock(DoctrineQueryBuilder::class);
    }

    private function getAnisQueryBuilder(): AnisQueryBuilder|MockObject
    {
        return $this->createMock(AnisQueryBuilder::class);
    }
}
