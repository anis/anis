<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Search\Query;

use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;
use App\Search\Query\Count;
use App\Search\Query\AnisQueryBuilder;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use App\Entity\Dataset;

final class CountTest extends TestCase
{
    public function testCount(): void
    {
        $doctrineQueryBuilder = $this->getDoctrineQueryBuilder();
        $doctrineQueryBuilder->expects($this->once())->method('select');
        $anisQueryBuilder = $this->getAnisQueryBuilder();
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $datasetSelected = $this->getDataset();
        $queryParams = ['a' => 'count'];
        (new Count())($anisQueryBuilder, $datasetSelected, $queryParams);
    }

    private function getDataset(): Dataset|MockObject
    {
        return $this->createMock(Dataset::class);
    }

    private function getDoctrineQueryBuilder(): DoctrineQueryBuilder|MockObject
    {
        return $this->createMock(DoctrineQueryBuilder::class);
    }

    private function getAnisQueryBuilder(): AnisQueryBuilder|MockObject
    {
        return $this->createMock(AnisQueryBuilder::class);
    }
}
