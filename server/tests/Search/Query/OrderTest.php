<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Search\Query;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use Doctrine\Common\Collections\ArrayCollection;
use Tests\TestCase;
use App\Search\Query\Order;
use App\Search\Query\AnisQueryBuilder;
use App\Entity\Dataset;
use App\Entity\Attribute;
use App\Search\Query\SearchQueryException;

final class OrderTest extends TestCase
{
    public function testOrderAsc(): void
    {
        $attribute = $this->getAttribute();
        $attribute->method('getId')->willReturn(1);
        $datasetSelected = $this->getDataset();
        $datasetSelected->method('getAttributes')->willReturn(new ArrayCollection([$attribute]));
        $doctrineQueryBuilder = $this->getDoctrineQueryBuilder();
        $doctrineQueryBuilder->expects($this->once())->method('orderBy');
        $anisQueryBuilder = $this->getAnisQueryBuilder();
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        (new Order())($anisQueryBuilder, $datasetSelected, ['o' => '1:a']);
    }

    public function testOrderDesc(): void
    {
        $attribute = $this->getAttribute();
        $attribute->method('getId')->willReturn(1);
        $datasetSelected = $this->getDataset();
        $datasetSelected->method('getAttributes')->willReturn(new ArrayCollection([$attribute]));
        $doctrineQueryBuilder = $this->getDoctrineQueryBuilder();
        $doctrineQueryBuilder->expects($this->once())->method('orderBy');
        $anisQueryBuilder = $this->getAnisQueryBuilder();
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        (new Order())($anisQueryBuilder, $datasetSelected, ['o' => '1:d']);
    }

    public function testOrderException(): void
    {
        $this->expectException(SearchQueryException::class);
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $datasetSelected = $this->createMock(Dataset::class);
        (new Order())($anisQueryBuilder, $datasetSelected, ['o' => '1:a:d']);
    }

    public function testOrderTypeException(): void
    {
        $this->expectException(SearchQueryException::class);
        $anisQueryBuilder = $this->createMock(AnisQueryBuilder::class);
        $datasetSelected = $this->createMock(Dataset::class);
        (new Order())($anisQueryBuilder, $datasetSelected, ['o' => '1:b']);
    }

    private function getAttribute(): Attribute|MockObject
    {
        return $this->createMock(Attribute::class);
    }

    private function getDataset(): Dataset|MockObject
    {
        return $this->createMock(Dataset::class);
    }

    private function getDoctrineQueryBuilder(): DoctrineQueryBuilder|MockObject
    {
        return $this->createMock(DoctrineQueryBuilder::class);
    }

    private function getAnisQueryBuilder(): AnisQueryBuilder|MockObject
    {
        return $this->createMock(AnisQueryBuilder::class);
    }
}
