<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Search\Query;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use Tests\TestCase;
use App\Search\Query\AnisQueryBuilder;
use App\Entity\Dataset;
use App\Entity\Attribute;
use App\Search\Query\Select;
use App\Search\Query\From;

final class AnisQueryBuilderTest extends TestCase
{
    private AnisQueryBuilder $anisQueryBuilder;

    protected function setUp(): void
    {
        $this->anisQueryBuilder = new AnisQueryBuilder();
    }

    public function testGetDoctrineQueryBuilder(): void
    {
        $doctrineQueryBuilder = $this->getDoctrineQueryBuilder();
        $this->anisQueryBuilder->setDoctrineQueryBuilder($doctrineQueryBuilder);
        $this->assertSame($doctrineQueryBuilder, $this->anisQueryBuilder->getDoctrineQueryBuilder());
    }

    public function testGetDatasetSelected(): void
    {
        $datasetSelected = $this->getDataset();
        $this->anisQueryBuilder->setDatasetSelected($datasetSelected);
        $this->assertSame($datasetSelected, $this->anisQueryBuilder->getDatasetSelected());
    }

    public function testGetAttributesSelected(): void
    {
        $ra = $this->getAttribute();
        $dec = $this->getAttribute();
        $attributesSelected = [$ra, $dec];
        $this->anisQueryBuilder->setAttributesSelected($attributesSelected);
        $this->assertSame($attributesSelected, $this->anisQueryBuilder->getAttributesSelected());
    }

    public function testBuild(): void
    {
        $select = $this->getSelect();
        $select->expects($this->once())->method('__invoke');
        $from = $this->getFrom();
        $from->expects($this->once())->method('__invoke');
        $this->anisQueryBuilder->setDatasetSelected($this->createMock(Dataset::class));
        $this->anisQueryBuilder->addQueryPart($select);
        $this->anisQueryBuilder->addQueryPart($from);
        $this->anisQueryBuilder->build([]);
    }

    private function getAttribute(): Attribute|MockObject
    {
        return $this->createMock(Attribute::class);
    }

    private function getDataset(): Dataset|MockObject
    {
        return $this->createMock(Dataset::class);
    }

    private function getDoctrineQueryBuilder(): DoctrineQueryBuilder|MockObject
    {
        return $this->createMock(DoctrineQueryBuilder::class);
    }

    private function getSelect(): Select|MockObject
    {
        return $this->createMock(Select::class);
    }

    private function getFrom(): From|MockObject
    {
        return $this->createMock(From::class);
    }
}
