<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Search\Query;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use Tests\TestCase;
use App\Search\Query\Limit;
use App\Search\Query\AnisQueryBuilder;
use App\Entity\Dataset;
use App\Search\Query\SearchQueryException;

final class LimitTest extends TestCase
{
    public function testLimit(): void
    {
        $doctrineQueryBuilder = $this->getDoctrineQueryBuilder();
        $doctrineQueryBuilder->method('setFirstResult')->willReturn($doctrineQueryBuilder);
        $doctrineQueryBuilder->expects($this->once())->method('setFirstResult');
        $doctrineQueryBuilder->expects($this->once())->method('setMaxResults');
        $anisQueryBuilder = $this->getAnisQueryBuilder();
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $datasetSelected = $this->getDataset();
        (new Limit())($anisQueryBuilder, $datasetSelected, ['p' => '1:10']);
    }

    public function testLimitException(): void
    {
        $this->expectException(SearchQueryException::class);
        $anisQueryBuilder = $this->getAnisQueryBuilder();
        $datasetSelected = $this->getDataset();
        (new Limit())($anisQueryBuilder, $datasetSelected, ['p' => '1:10:20']);
    }

    private function getDataset(): Dataset|MockObject
    {
        return $this->createMock(Dataset::class);
    }

    private function getDoctrineQueryBuilder(): DoctrineQueryBuilder|MockObject
    {
        return $this->createMock(DoctrineQueryBuilder::class);
    }

    private function getAnisQueryBuilder(): AnisQueryBuilder|MockObject
    {
        return $this->createMock(AnisQueryBuilder::class);
    }
}
