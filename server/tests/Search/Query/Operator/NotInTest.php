<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Search\Query\Operator;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Tests\TestCase;
use App\Search\Query\Operator\NotIn;

final class NotInTest extends TestCase
{
    private NotIn $operator;

    protected function setUp(): void
    {
        $expr = $this->getExpressionBuilderMock();
        $expr->method('notIn')
            ->willReturn('NOT IN(10, 20, 30)');
        $this->operator = new NotIn($expr, 'test', 'integer', ['10', '20', '30']);
    }

    public function testGetExpression(): void
    {
        $this->assertSame('NOT IN(10, 20, 30)', $this->operator->getExpression());
    }

    private function getExpressionBuilderMock(): ExpressionBuilder|MockObject
    {
        return $this->createMock(ExpressionBuilder::class);
    }
}
