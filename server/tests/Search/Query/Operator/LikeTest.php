<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Search\Query\Operator;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Tests\TestCase;
use App\Search\Query\Operator\Like;

final class LikeTest extends TestCase
{
    private Like $operator;

    protected function setUp(): void
    {
        $expr = $this->getExpressionBuilderMock();
        $expr->method('like')
            ->willReturn('CAST(test AS text) LIKE \'%toto%\'');
        $this->operator = new Like($expr, 'test', 'string', 'toto');
    }

    public function testGetExpression(): void
    {
        $this->assertSame('CAST(test AS text) LIKE \'%toto%\'', $this->operator->getExpression());
    }

    private function getExpressionBuilderMock(): ExpressionBuilder|MockObject
    {
        return $this->createMock(ExpressionBuilder::class);
    }
}
