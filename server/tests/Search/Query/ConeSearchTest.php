<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Search\Query;

use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;
use App\Search\Query\ConeSearch;
use App\Search\Query\AnisQueryBuilder;
use App\Search\Query\SearchQueryException;
use App\Entity\Dataset;
use App\Entity\Attribute;
use App\Entity\ConeSearchConfig;

final class ConeSearchTest extends TestCase
{
    public function testConeSearch(): void
    {
        $id = $this->getAttribute();
        $id->method('getId')->willReturn(1);
        $ra = $this->getAttribute();
        $ra->method('getId')->willReturn(2);
        $dec = $this->getAttribute();
        $dec->method('getId')->willReturn(3);
        $coneSearchConfig = $this->getConeSearchConfig();
        $coneSearchConfig->method('getEnabled')->willReturn(true);
        $coneSearchConfig->method('getColumnRa')->willReturn(2);
        $coneSearchConfig->method('getColumnDec')->willReturn(3);
        $datasetSelected = $this->getDataset();
        $datasetSelected->method('getConeSearchConfig')->willReturn($coneSearchConfig);
        $datasetSelected->method('getAttributes')->willReturn(new ArrayCollection([$id, $ra, $dec]));

        $doctrineQueryBuilder = $this->getDoctrineQueryBuilder();
        $expr = $this->getExpressionBuilder();
        $doctrineQueryBuilder->method('expr')->willReturn($expr);
        $doctrineQueryBuilder->expects($this->once())->method('where');
        $anisQueryBuilder = $this->getAnisQueryBuilder();
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $queryParams = ['cs' => '102.5:0.0:100'];
        (new ConeSearch())($anisQueryBuilder, $datasetSelected, $queryParams);
    }

    public function testConeSearchException(): void
    {
        $this->expectException(SearchQueryException::class);
        $anisQueryBuilder = $this->getAnisQueryBuilder();
        $datasetSelected = $this->getDataset();
        $queryParams = ['cs' => '102.5:0.0:100:10'];
        (new ConeSearch())($anisQueryBuilder, $datasetSelected, $queryParams);
    }

    public function testConeSearchUnavailableException(): void
    {
        $this->expectException(SearchQueryException::class);
        $anisQueryBuilder = $this->getAnisQueryBuilder();
        $coneSearchConfig = $this->getConeSearchConfig();
        $coneSearchConfig->method('getEnabled')->willReturn(false);
        $datasetSelected = $this->getDataset();

        $queryParams = ['cs' => '102.5:0.0:100'];
        (new ConeSearch())($anisQueryBuilder, $datasetSelected, $queryParams);
    }

    public function testConeSearchParamsValues(): void
    {
        $id = $this->getAttribute();
        $id->method('getId')->willReturn(1);
        $ra = $this->getAttribute();
        $ra->method('getId')->willReturn(2);
        $dec = $this->getAttribute();
        $dec->method('getId')->willReturn(3);
        $coneSearchConfig = $this->getConeSearchConfig();
        $coneSearchConfig->method('getEnabled')->willReturn(true);
        $coneSearchConfig->method('getColumnRa')->willReturn(2);
        $coneSearchConfig->method('getColumnDec')->willReturn(3);
        $datasetSelected = $this->getDataset();
        $datasetSelected->method('getConeSearchConfig')->willReturn($coneSearchConfig);
        $datasetSelected->method('getAttributes')->willReturn(new ArrayCollection([$id, $ra, $dec]));

        $doctrineQueryBuilder = $this->getDoctrineQueryBuilder();
        $expr = $this->getExpressionBuilder();
        $doctrineQueryBuilder->method('expr')->willReturn($expr);
        $doctrineQueryBuilder->expects($this->once())->method('where');
        $anisQueryBuilder = $this->getAnisQueryBuilder();
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $queryParams = ['cs' => '102.5:91:0'];
        (new ConeSearch())($anisQueryBuilder, $datasetSelected, $queryParams);
    }

    private function getAttribute(): Attribute|MockObject
    {
        return $this->createMock(Attribute::class);
    }

    private function getDataset(): Dataset|MockObject
    {
        return $this->createMock(Dataset::class);
    }

    private function getConeSearchConfig(): ConeSearchConfig|MockObject
    {
        return $this->createMock(ConeSearchConfig::class);
    }

    private function getDoctrineQueryBuilder(): DoctrineQueryBuilder|MockObject
    {
        return $this->createMock(DoctrineQueryBuilder::class);
    }

    private function getExpressionBuilder(): ExpressionBuilder|MockObject
    {
        return $this->createMock(ExpressionBuilder::class);
    }

    private function getAnisQueryBuilder(): AnisQueryBuilder|MockObject
    {
        return $this->createMock(AnisQueryBuilder::class);
    }
}
