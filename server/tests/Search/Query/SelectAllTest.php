<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Search\Query;

use PHPUnit\Framework\MockObject\MockObject;
use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use Doctrine\Common\Collections\ArrayCollection;
use Tests\TestCase;
use App\Search\Query\SelectAll;
use App\Search\Query\AnisQueryBuilder;
use App\Entity\Dataset;
use App\Entity\Attribute;

final class SelectAllTest extends TestCase
{
    public function testCount(): void
    {
        $id = $this->getAttribute();
        $id->method('getId')->willReturn(1);
        $ra = $this->getAttribute();
        $ra->method('getId')->willReturn(2);
        $dec = $this->getAttribute();
        $dec->method('getId')->willReturn(3);
        $datasetSelected = $this->getDataset();
        $datasetSelected->method('getAttributes')->willReturn(new ArrayCollection([$id, $ra, $dec]));
        $doctrineQueryBuilder = $this->getDoctrineQueryBuilder();
        $doctrineQueryBuilder->expects($this->once())->method('select');
        $anisQueryBuilder = $this->getAnisQueryBuilder();
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $queryParams = ['a' => 'all'];
        (new SelectAll())($anisQueryBuilder, $datasetSelected, $queryParams);
    }

    private function getAttribute(): Attribute|MockObject
    {
        return $this->createMock(Attribute::class);
    }

    private function getDataset(): Dataset|MockObject
    {
        return $this->createMock(Dataset::class);
    }

    private function getDoctrineQueryBuilder(): DoctrineQueryBuilder|MockObject
    {
        return $this->createMock(DoctrineQueryBuilder::class);
    }

    private function getAnisQueryBuilder(): AnisQueryBuilder|MockObject
    {
        return $this->createMock(AnisQueryBuilder::class);
    }
}
