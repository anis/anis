<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Search\Query;

use Doctrine\DBAL\Query\QueryBuilder as DoctrineQueryBuilder;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;
use App\Search\Query\Where;
use App\Search\Query\AnisQueryBuilder;
use App\Search\Query\Operator\IOperatorFactory;
use App\Search\Query\Operator\IOperator;
use App\Entity\Dataset;
use App\Entity\Attribute;

final class WhereTest extends TestCase
{
    public function testWhere(): void
    {
        $operatorFactory = $this->getOperatorFactory();
        $operator = $this->getOperator();
        $operatorFactory->method('create')->willReturn($operator);

        $id = $this->getAttribute();
        $id->method('getId')->willReturn(1);
        $id->method('getType')->willReturn('integer');
        $datasetSelected = $this->getDataset();
        $datasetSelected->method('getAttributes')->willReturn(new ArrayCollection([$id]));

        $doctrineQueryBuilder = $this->getDoctrineQueryBuilder();
        $expr = $this->getExpressionBuilder();
        $doctrineQueryBuilder->method('expr')->willReturn($expr);
        $doctrineQueryBuilder->expects($this->once())->method('andWhere');
        $anisQueryBuilder = $this->getAnisQueryBuilder();
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $queryParams = ['c' => '1::eq::10'];
        (new Where($operatorFactory))($anisQueryBuilder, $datasetSelected, $queryParams);
    }

    public function testWhereWithoutValues(): void
    {
        $operatorFactory = $this->getOperatorFactory();
        $operator = $this->getOperator();
        $operatorFactory->method('create')->willReturn($operator);

        $id = $this->getAttribute();
        $id->method('getId')->willReturn(1);
        $id->method('getType')->willReturn('integer');
        $datasetSelected = $this->getDataset();
        $datasetSelected->method('getAttributes')->willReturn(new ArrayCollection([$id]));

        $doctrineQueryBuilder = $this->getDoctrineQueryBuilder();
        $expr = $this->getExpressionBuilder();
        $doctrineQueryBuilder->method('expr')->willReturn($expr);
        $doctrineQueryBuilder->expects($this->once())->method('andWhere');
        $anisQueryBuilder = $this->getAnisQueryBuilder();
        $anisQueryBuilder->method('getDoctrineQueryBuilder')->willReturn($doctrineQueryBuilder);
        $queryParams = ['c' => '1::nl'];
        (new Where($operatorFactory))($anisQueryBuilder, $datasetSelected, $queryParams);
    }

    private function getOperatorFactory(): IOperatorFactory|MockObject
    {
        return $this->createMock(IOperatorFactory::class);
    }

    private function getOperator(): IOperator|MockObject
    {
        return $this->createMock(IOperator::class);
    }

    private function getAttribute(): Attribute|MockObject
    {
        return $this->createMock(Attribute::class);
    }

    private function getDataset(): Dataset|MockObject
    {
        return $this->createMock(Dataset::class);
    }

    private function getDoctrineQueryBuilder(): DoctrineQueryBuilder|MockObject
    {
        return $this->createMock(DoctrineQueryBuilder::class);
    }

    private function getExpressionBuilder(): ExpressionBuilder|MockObject
    {
        return $this->createMock(ExpressionBuilder::class);
    }

    private function getAnisQueryBuilder(): AnisQueryBuilder|MockObject
    {
        return $this->createMock(AnisQueryBuilder::class);
    }
}
