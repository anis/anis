<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Search;

use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;
use App\Search\DBALConnectionFactory;
use App\Entity\Database;

final class DBALConnectionFactoryTest extends TestCase
{
    private DBALConnectionFactory $connectionFactory;

    protected function setUp(): void
    {
        $this->connectionFactory = new DBALConnectionFactory();
    }

    public function testCreate(): void
    {
        $database = $this->getDatabaseMock();
        $database->method('getDbName')
            ->willReturn('test');
        $database->method('getLogin')
            ->willReturn('user');
        $database->method('getPassword')
            ->willReturn('password');
        $database->method('getHost')
            ->willReturn('db');
        $database->method('getPort')
            ->willReturn(5432);
        $database->method('getType')
            ->willReturn('pdo_sqlite');

        $connection = $this->connectionFactory->create($database);
        $this->assertInstanceOf('Doctrine\DBAL\Connection', $connection);
    }

    private function getDatabaseMock(): Database|MockObject
    {
        return $this->createMock(Database::class);
    }
}
