<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Root;

use Tests\TestCase;
use App\Exception\InvalidFormException;

final class InvalidFormExceptionTest extends TestCase
{
    public function testSetErrors(): void
    {
        $invalidFormException = new InvalidFormException($this->createRequest('OPTIONS', '/'));
        $this->assertSame(
            $invalidFormException->setErrors(['errors' => 'My errors']),
            $invalidFormException
        );
    }

    public function testGetErrors(): void
    {
        $invalidFormException = new InvalidFormException($this->createRequest('OPTIONS', '/'));
        $invalidFormException->setErrors(['errors' => 'My errors']);
        $this->assertSame($invalidFormException->getErrors(), ['errors' => 'My errors']);
    }
}
