<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Family;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Laminas\InputFilter\InputFilterInterface;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Actions\Instance\Family\DatasetFamilyListAction;
use App\Entity\Instance;

final class DatasetFamilyListActionTest extends TestCase
{
    private DatasetFamilyListAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;
    private Instance|MockObject $instance;

    protected function setUp(): void
    {
        $this->instance = $this->createMock(Instance::class);
        $this->entityManager = $this->getEntityManager();
        $this->entityManager->method('find')->willReturn($this->instance);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new DatasetFamilyListAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/dataset-family'),
            $this->createResponse(),
            []
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testGetAllDatasetFamiliesForAnInstance(): void
    {
        $repository = $this->getObjectRepositoryMock();
        $repository->expects($this->once())->method('findBy')->with(
            ['instance' => $this->instance]
        );
        $this->entityManager->method('getRepository')->with('App\Entity\DatasetFamily')->willReturn($repository);

        ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset-family'),
            $this->createResponse(),
            ['name' => 'default']
        );
    }

    public function testAddANewDatasetFamily(): void
    {
        $fields = [
            'id' => 1,
            'label' => 'Default',
            'display' => 10,
            'opened' => true
        ];

        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($fields);
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $this->entityManager->expects($this->once())->method('persist');

        $response = ($this->action)(
            $this->createRequest('POST', '/instance/default/dataset-family')->withParsedBody($fields),
            $this->createResponse(),
            ['name' => 'default']
        );
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }

    public function getEntityManager(): EntityManager|MockObject
    {
        return $this->createMock(EntityManager::class);
    }
}
