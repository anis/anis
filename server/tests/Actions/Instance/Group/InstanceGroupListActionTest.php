<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Group;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Laminas\InputFilter\InputFilterInterface;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Actions\Instance\Group\InstanceGroupListAction;
use App\Entity\Instance;

final class InstanceGroupListActionTest extends TestCase
{
    private InstanceGroupListAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;
    private Instance|MockObject $instance;

    protected function setUp(): void
    {
        $this->instance = $this->createMock(Instance::class);
        $this->entityManager = $this->getEntityManager();
        $this->entityManager->method('find')->willReturn($this->instance);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new InstanceGroupListAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/group'),
            $this->createResponse(),
            []
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testGetAllGroupsForAnInstance(): void
    {
        $repository = $this->getObjectRepositoryMock();
        $repository->expects($this->once())->method('findBy')->with(
            ['instance' => $this->instance]
        );
        $this->entityManager->method('getRepository')->with('App\Entity\InstanceGroup')->willReturn($repository);

        ($this->action)(
            $this->createRequest('GET', '/instance/default/group'),
            $this->createResponse(),
            ['name' => 'default']
        );
    }

    public function testAddANewInstanceGroup(): void
    {
        $fields = [
            'role' => 'TEST'
        ];

        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($fields);
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $this->entityManager->expects($this->once())->method('persist');

        $response = ($this->action)(
            $this->createRequest('POST', '/instance/default/group')->withParsedBody($fields),
            $this->createResponse(),
            ['name' => 'default']
        );
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }

    public function getEntityManager(): EntityManager|MockObject
    {
        return $this->createMock(EntityManager::class);
    }
}
