<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Group;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Laminas\InputFilter\InputFilterInterface;
use Slim\Exception\HttpNotFoundException;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Actions\Instance\Group\InstanceGroupAction;
use App\Entity\InstanceGroup;

final class InstanceGroupActionTest extends TestCase
{
    private InstanceGroupAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new InstanceGroupAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/group/TEST'),
            $this->createResponse(),
            []
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testInstanceGroupIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Instance-group with role TEST is not found');
        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default/group/TEST'),
            $this->createResponse(),
            ['name' => 'default', 'role' => 'TEST']
        );
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAnInstanceGroupByRoleName(): void
    {
        $instanceGroup = $this->getIntanceGroupMock();
        $instanceGroup->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($instanceGroup);
        ($this->action)(
            $this->createRequest('GET', '/instance/default/group/TEST'),
            $this->createResponse(),
            ['name' => 'default', 'role' => 'TEST']
        );
    }

    public function testEditAnInstanceGroup(): void
    {
        $fields = [
            'role' => 'TUTU'
        ];

        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($fields);
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $instanceGroup = $this->getIntanceGroupMock();
        $this->entityManager->method('find')->willReturn($instanceGroup);
        $this->entityManager->expects($this->once())->method('flush');

        ($this->action)(
            $this->createRequest('PUT', '/instance/default/group/TEST')->withParsedBody($fields),
            $this->createResponse(),
            ['name' => 'default', 'role' => 'TEST']
        );
    }

    public function testDeleteAnInstanceGroup(): void
    {
        $instanceGroup = $this->getIntanceGroupMock();
        $instanceGroup->method('getRole')->willReturn('TEST');
        $this->entityManager->method('find')->willReturn($instanceGroup);

        $response = ($this->action)(
            $this->createRequest('DELETE', '/instance/default/group/TEST'),
            $this->createResponse(),
            ['name' => 'default', 'role' => 'TEST']
        );
        $this->assertSame(
            '{"statusCode":200,"data":{"message":"Instance-group with role TEST is removed!"}}',
            (string) $response->getBody()
        );
    }

    private function getIntanceGroupMock(): InstanceGroup|MockObject
    {
        return $this->createMock(InstanceGroup::class);
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }
}
