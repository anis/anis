<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Menu;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Laminas\InputFilter\InputFilterInterface;
use Slim\Exception\HttpNotFoundException;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Actions\Instance\Menu\MenuFamilyItemListAction;
use App\Entity\Instance;
use App\Entity\MenuFamily;

final class MenuFamilyItemListActionTest extends TestCase
{
    private MenuFamilyItemListAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->getEntityManager();
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new MenuFamilyItemListAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/menu-item/1/item'),
            $this->createResponse(),
            []
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testGetAllMenuItemsForAMenuFamilyItem(): void
    {
        $menuFamily = $this->getMenuFamily();
        $this->entityManager->method('find')->willReturn($this->getInstance(), $menuFamily);
        $repository = $this->getObjectRepositoryMock();
        $repository->expects($this->once())->method('findBy')->with(
            ['menuFamily' => $menuFamily]
        );
        $this->entityManager->method('getRepository')->with('App\Entity\MenuItem')->willReturn($repository);

        ($this->action)(
            $this->createRequest('GET', '/instance/default/menu-item/1/item'),
            $this->createResponse(),
            ['name' => 'default', 'fid' => 1]
        );
    }

    public function testMenuFamilyItemIsNotFound(): void
    {
        $this->entityManager->method('find')->willReturn($this->getInstance(), null);
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Menu family with id 1 is not found');
        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default/menu-item/1/item'),
            $this->createResponse(),
            ['name' => 'default', 'fid' => 1]
        );
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testAddANewMenuFamilyItem(): void
    {
        $menuFamily = $this->getMenuFamily();
        $this->entityManager->method('find')->willReturn($this->getInstance(), $menuFamily);
        $fields = [
            'id' => 1,
            'label' => 'Default',
            'icon' => 'fas fa-home',
            'display' => 10,
            'name' => 'webpage',
            'title' => 'webpage',
            'content' => '<h1>My webpage</h1>',
            'style_sheet' => ''
        ];

        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($fields);
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $this->entityManager->expects($this->once())->method('persist');

        $response = ($this->action)(
            $this->createRequest('POST', '/instance/default/menu-item/1/item?type=webpage')->withParsedBody($fields),
            $this->createResponse(),
            ['name' => 'default', 'fid' => 1]
        );
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }

    public function getEntityManager(): EntityManager|MockObject
    {
        return $this->createMock(EntityManager::class);
    }

    public function getInstance(): Instance|MockObject
    {
        return $this->createMock(Instance::class);
    }

    public function getMenuFamily(): MenuFamily|MockObject
    {
        return $this->createMock(MenuFamily::class);
    }
}
