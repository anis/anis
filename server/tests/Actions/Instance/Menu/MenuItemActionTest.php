<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Menu;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Laminas\InputFilter\InputFilterInterface;
use Slim\Exception\HttpNotFoundException;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Actions\Instance\Menu\MenuItemAction;
use App\Entity\MenuItem;

final class MenuItemActionTest extends TestCase
{
    private MenuItemAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new MenuItemAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/menu-item/1'),
            $this->createResponse(),
            []
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testMenuItemIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Menu item with id 1 is not found');
        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default/menu-item/1'),
            $this->createResponse(),
            ['name' => 'default', 'id' => 1]
        );
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAMenuItemById(): void
    {
        $menuItem = $this->getMenuItemMock();
        $menuItem->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($menuItem);
        ($this->action)(
            $this->createRequest('GET', '/instance/default/menu-item/1'),
            $this->createResponse(),
            ['name' => 'default', 'id' => 1]
        );
    }

    public function testDeleteAMenuItem(): void
    {
        $menuItem = $this->getMenuItemMock();
        $menuItem->method('getId')->willReturn(1);
        $this->entityManager->method('find')->willReturn($menuItem);

        $response = ($this->action)(
            $this->createRequest('DELETE', '/instance/default/menu-item/1'),
            $this->createResponse(),
            ['name' => 'default', 'id' => 1]
        );
        $this->assertSame(
            '{"statusCode":200,"data":{"message":"Menu item with id 1 is removed!"}}',
            (string) $response->getBody()
        );
    }

    private function getMenuItemMock(): MenuItem|MockObject
    {
        return $this->createMock(MenuItem::class);
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }
}
