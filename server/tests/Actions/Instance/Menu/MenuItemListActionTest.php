<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Menu;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Laminas\InputFilter\InputFilterInterface;
use Slim\Exception\HttpBadRequestException;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Actions\Instance\Menu\MenuItemListAction;
use App\Entity\Instance;

final class MenuItemListActionTest extends TestCase
{
    private MenuItemListAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;
    private Instance|MockObject $instance;

    protected function setUp(): void
    {
        $this->instance = $this->createMock(Instance::class);
        $this->entityManager = $this->getEntityManager();
        $this->entityManager->method('find')->willReturn($this->instance);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new MenuItemListAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/menu-item'),
            $this->createResponse(),
            []
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testGetAllMenuItemsForAnInstance(): void
    {
        $repository = $this->getObjectRepositoryMock();
        $repository->expects($this->once())->method('findBy')->with(
            ['instance' => $this->instance]
        );
        $this->entityManager->method('getRepository')->with('App\Entity\MenuItem')->willReturn($repository);

        ($this->action)(
            $this->createRequest('GET', '/instance/default/menu-item'),
            $this->createResponse(),
            ['name' => 'default']
        );
    }

    public function testQueryParamTypeIsNotFound(): void
    {
        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Query param type is mandatory for this request');
        $response = ($this->action)(
            $this->createRequest('POST', '/instance/default/menu-item'),
            $this->createResponse(),
            ['name' => 'default']
        );
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testBadQueryParamType(): void
    {
        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Query param type must be equal to menu_family, webpage or url');
        $response = ($this->action)(
            $this->createRequest('POST', '/instance/default/menu-item?type=badtype'),
            $this->createResponse(),
            ['name' => 'default']
        );
        $this->assertEquals(400, (int) $response->getStatusCode());
    }

    public function testAddANewMenuFamily(): void
    {
        $fields = [
            'id' => 1,
            'label' => 'Default',
            'icon' => 'fas fa-home',
            'display' => 10,
            'name' => 'submenu'
        ];

        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($fields);
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $this->entityManager->expects($this->once())->method('persist');

        $response = ($this->action)(
            $this->createRequest('POST', '/instance/default/menu-item?type=menu_family')->withParsedBody($fields),
            $this->createResponse(),
            ['name' => 'default']
        );
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    public function testAddANewWebpage(): void
    {
        $fields = [
            'id' => 1,
            'label' => 'Default',
            'icon' => 'fas fa-home',
            'display' => 10,
            'name' => 'webpage',
            'title' => 'webpage',
            'content' => '<h1>My webpage</h1>',
            'style_sheet' => ''
        ];

        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($fields);
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $this->entityManager->expects($this->once())->method('persist');

        $response = ($this->action)(
            $this->createRequest('POST', '/instance/default/menu-item?type=webpage')->withParsedBody($fields),
            $this->createResponse(),
            ['name' => 'default']
        );
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    public function testAddANewUrl(): void
    {
        $fields = [
            'id' => 1,
            'label' => 'Default',
            'icon' => 'fas fa-home',
            'display' => 10,
            'type_url' => 'internal',
            'href' => '/test'
        ];

        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($fields);
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $this->entityManager->expects($this->once())->method('persist');

        $response = ($this->action)(
            $this->createRequest('POST', '/instance/default/menu-item?type=url')->withParsedBody($fields),
            $this->createResponse(),
            ['name' => 'default']
        );
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }

    public function getEntityManager(): EntityManager|MockObject
    {
        return $this->createMock(EntityManager::class);
    }
}
