<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Slim\Exception\HttpNotFoundException;
use Laminas\InputFilter\InputFilterInterface;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Entity\Dataset;
use App\Entity\Database;
use App\Entity\DatasetFamily;
use App\Actions\Instance\Dataset\DatasetAction;

final class DatasetActionTest extends TestCase
{
    use DatasetTrait;

    private DatasetAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new DatasetAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/dataset/observations'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations']
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testDatasetIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Dataset with name observations is not found');
        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset/observations'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations']
        );
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetADatasetByName(): void
    {
        $dataset = $this->getDatasetMock();
        $dataset->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($dataset);
        ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset/observations'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations']
        );
    }

    public function testEditADataset(): void
    {
        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($this->getDatasetFields());
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $this->entityManager->method('find')->willReturnOnConsecutiveCalls(
            $this->getDatasetMock(),
            $this->getDatabaseMock(),
            $this->getDatasetFamilyMock()
        );
        $this->entityManager->expects($this->once())->method('flush');

        ($this->action)(
            $this->createRequest(
                'PUT',
                '/instance/default/dataset/observations'
            )->withParsedBody($this->getDatasetFields()),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations']
        );
    }

    public function testDeleteADataset(): void
    {
        $dataset = $this->getDatasetMock();
        $dataset->method('getName')->willReturn('observations');
        $this->entityManager->method('find')->willReturn($dataset);

        $response = ($this->action)(
            $this->createRequest('DELETE', '/instance/default/dataset/observations'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations']
        );
        $this->assertSame(
            '{"statusCode":200,"data":{"message":"Dataset with name observations is removed!"}}',
            (string) $response->getBody()
        );
    }

    private function getDatasetMock(): Dataset|MockObject
    {
        return $this->createMock(Dataset::class);
    }

    private function getDatabaseMock(): Database|MockObject
    {
        return $this->createMock(Database::class);
    }

    private function getDatasetFamilyMock(): DatasetFamily|MockObject
    {
        return $this->createMock(DatasetFamily::class);
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }
}
