<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset\File;

trait ImageTrait
{
    private function getImageFields(): array
    {
        return [
            'id' => 1,
            'label' => 'Mag i',
            'file_path' => '/IMAGES/CFHTLS_D-85_i_022559-042940_T0007_MEDIAN.fits',
            'hdu_number' => null,
            'file_size' => 1498320000,
            'ra_min' => 35.994643451078,
            'ra_max' => 36.99765934121,
            'dec_min' => -3.9943006310031,
            'dec_max' => -4.9941936740893,
            'stretch' => 'linear',
            'pmin' => 0.2,
            'pmax' => 99
        ];
    }
}
