<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset\File;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Slim\Exception\HttpNotFoundException;
use Laminas\InputFilter\InputFilterInterface;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Entity\File;
use App\Actions\Instance\Dataset\File\FileAction;

final class FileActionTest extends TestCase
{
    use FileTrait;

    private FileAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new FileAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/dataset/observations/file/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testFileIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('File with id 1 is not found');
        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset/observations/file/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAFileByID(): void
    {
        $file = $this->getFileMock();
        $file->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($file);
        ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset/observations/file/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
    }

    public function testEditAFile(): void
    {
        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($this->getFileFields());
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $file = $this->getFileMock();
        $this->entityManager->method('find')->willReturn($file);
        $this->entityManager->expects($this->once())->method('flush');

        ($this->action)(
            $this->createRequest(
                'PUT',
                '/instance/default/dataset/observations/file/1'
            )->withParsedBody($this->getFileFields()),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
    }

    public function testDeleteAFile(): void
    {
        $file = $this->getFileMock();
        $file->method('getId')->willReturn(1);
        $this->entityManager->method('find')->willReturn($file);

        $response = ($this->action)(
            $this->createRequest('DELETE', '/instance/default/dataset/observations/file/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
        $this->assertSame(
            '{"statusCode":200,"data":{"message":"File with id 1 is removed!"}}',
            (string) $response->getBody()
        );
    }

    private function getFileMock(): File|MockObject
    {
        return $this->createMock(File::class);
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }
}
