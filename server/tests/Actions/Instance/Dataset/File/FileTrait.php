<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset\File;

trait FileTrait
{
    private function getFileFields(): array
    {
        return [
            'id' => 1,
            'label' => 'My file',
            'file_path' => '/FILE/my_file.txt',
            'file_size' => 1498320000,
            'type' => 'txt'
        ];
    }
}
