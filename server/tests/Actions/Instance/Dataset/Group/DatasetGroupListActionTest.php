<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset\Group;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Laminas\InputFilter\InputFilterInterface;
use Tests\TestCase;
use App\Entity\Dataset;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Actions\Instance\Dataset\Group\DatasetGroupListAction;

final class DatasetGroupListActionTest extends TestCase
{
    use DatasetGroupTrait;

    private DatasetGroupListAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new DatasetGroupListAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/{name}/dataset/{dname}/group'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations']
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testGetAllDatasetGroups(): void
    {
        $this->entityManager->method('find')->willReturn($this->getDatasetMock());

        $repository = $this->getObjectRepositoryMock();
        $repository->expects($this->once())->method('findBy');
        $this->entityManager->method('getRepository')->with('App\Entity\DatasetGroup')->willReturn($repository);
        ($this->action)(
            $this->createRequest('GET', '/instance/{name}/dataset/{dname}/group'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations']
        );
    }

    public function testAddANewOutputFamily(): void
    {
        $this->entityManager->method('find')->willReturn($this->getDatasetMock());

        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($this->getDatasetGroupFields());
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $this->entityManager->expects($this->once())->method('persist');

        $response = ($this->action)(
            $this->createRequest(
                'POST',
                '/instance/{name}/dataset/{dname}/group'
            )->withParsedBody($this->getDatasetGroupFields()),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations']
        );
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    private function getDatasetMock(): Dataset|MockObject
    {
        return $this->createMock(Dataset::class);
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }
}
