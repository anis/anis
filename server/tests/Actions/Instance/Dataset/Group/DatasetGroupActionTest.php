<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset\Group;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Slim\Exception\HttpNotFoundException;
use Laminas\InputFilter\InputFilterInterface;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Entity\DatasetGroup;
use App\Actions\Instance\Dataset\Group\DatasetGroupAction;

final class DatasetGroupActionTest extends TestCase
{
    use DatasetGroupTrait;

    private DatasetGroupAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new DatasetGroupAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/dataset/observations/group/my-role'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'role' => 'my-role']
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testDatasetGroupIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Dataset group with role my-role is not found');
        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset/observations/group/my-role'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'role' => 'my-role']
        );
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetADatasetGroupByRole(): void
    {
        $datasetGroup = $this->getDatasetGroupMock();
        $datasetGroup->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($datasetGroup);
        ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset/observations/group/my-role'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'role' => 'my-role']
        );
    }

    public function testEditADatasetGroup(): void
    {
        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($this->getDatasetGroupFields());
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $datasetGroup = $this->getDatasetGroupMock();
        $this->entityManager->method('find')->willReturn($datasetGroup);
        $this->entityManager->expects($this->once())->method('flush');

        ($this->action)(
            $this->createRequest(
                'PUT',
                '/instance/default/dataset/observations/group/my-role'
            )->withParsedBody($this->getDatasetGroupFields()),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'role' => 'my-role']
        );
    }

    public function testDeleteADatasetGroup(): void
    {
        $datasetGroup = $this->getDatasetGroupMock();
        $datasetGroup->method('getRole')->willReturn('my-role');
        $this->entityManager->method('find')->willReturn($datasetGroup);

        $response = ($this->action)(
            $this->createRequest('DELETE', '/instance/default/dataset/observations/group/my-role'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'role' => 'my-role']
        );
        $this->assertSame(
            '{"statusCode":200,"data":{"message":"Dataset group with role my-role is removed!"}}',
            (string) $response->getBody()
        );
    }

    private function getDatasetGroupMock(): DatasetGroup|MockObject
    {
        return $this->createMock(DatasetGroup::class);
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }
}
