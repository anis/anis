<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset\Criteria;

trait CriteriaFamilyTrait
{
    private function getCriteriaFamilyFields(): array
    {
        return [
            'id' => 1,
            'label' => 'myCriteriaFamily',
            'display' => 1,
            'opened' => true
        ];
    }
}
