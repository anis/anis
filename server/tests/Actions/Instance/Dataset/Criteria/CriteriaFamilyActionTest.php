<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset\Criteria;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Slim\Exception\HttpNotFoundException;
use Laminas\InputFilter\InputFilterInterface;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Entity\CriteriaFamily;
use App\Actions\Instance\Dataset\Criteria\CriteriaFamilyAction;

final class CriteriaFamilyActionTest extends TestCase
{
    use CriteriaFamilyTrait;

    private CriteriaFamilyAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new CriteriaFamilyAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/dataset/observations/criteria-family/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testCriteriaFamilyIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Criteria family with id 1 is not found');
        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset/observations/criteria-family/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetACriteriaFamilyByID(): void
    {
        $criteriaFamily = $this->getCriteriaFamilyMock();
        $criteriaFamily->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($criteriaFamily);
        ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset/observations/criteria-family/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
    }

    public function testEditACriteriaFamily(): void
    {
        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($this->getCriteriaFamilyFields());
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $criteriaFamily = $this->getCriteriaFamilyMock();
        $this->entityManager->method('find')->willReturn($criteriaFamily);
        $this->entityManager->expects($this->once())->method('flush');

        ($this->action)(
            $this->createRequest(
                'PUT',
                '/instance/default/dataset/observations/criteria-family/1'
            )->withParsedBody($this->getCriteriaFamilyFields()),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
    }

    public function testDeleteACriteriaFamily(): void
    {
        $criteriaFamily = $this->getCriteriaFamilyMock();
        $criteriaFamily->method('getId')->willReturn(1);
        $this->entityManager->method('find')->willReturn($criteriaFamily);

        $response = ($this->action)(
            $this->createRequest('DELETE', '/instance/default/dataset/observations/criteria-family/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
        $this->assertSame(
            '{"statusCode":200,"data":{"message":"Criteria family with id 1 is removed!"}}',
            (string) $response->getBody()
        );
    }

    private function getCriteriaFamilyMock(): CriteriaFamily|MockObject
    {
        return $this->createMock(CriteriaFamily::class);
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }
}
