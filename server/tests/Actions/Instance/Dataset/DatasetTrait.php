<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset;

trait DatasetTrait
{
    private function getDatasetFields(): array
    {
        return [
            'name' => 'another-dataset',
            'table_ref' => 'table',
            'primary_key' => 1,
            'default_order_by' => 1,
            'default_order_by_direction' => 'a',
            'label' => 'amother dataset',
            'description' => 'This is another dataset',
            'display' => 2,
            'data_path' => 'path',
            'public' => true,
            'download_json' => true,
            'download_csv' => true,
            'download_ascii' => true,
            'download_vo' => true,
            'download_fits' => true,
            'server_link_enabled' => true,
            'datatable_enabled' => true,
            'datatable_selectable_rows' => true,
            'id_database' => 1,
            'id_dataset_family' => 1
        ];
    }
}
