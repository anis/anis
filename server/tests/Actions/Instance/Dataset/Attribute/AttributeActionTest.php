<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset\Attribute;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Slim\Exception\HttpNotFoundException;
use Laminas\InputFilter\InputFilterInterface;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Entity\Attribute;
use App\Actions\Instance\Dataset\Attribute\AttributeAction;

final class AttributeActionTest extends TestCase
{
    use AttributeTrait;

    private AttributeAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new AttributeAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/dataset/observations/attribute/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testAttributeIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Attribute with dataset name observations and attribute id 1 is not found');
        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset/observations/attribute/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAnAttributeByID(): void
    {
        $attribute = $this->getAttributeMock();
        $attribute->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($attribute);
        ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset/observations/attribute/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
    }

    public function testEditAnAttribute(): void
    {
        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($this->getAttributeFields());
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $this->entityManager->method('find')->willReturnOnConsecutiveCalls(
            $this->getAttributeMock(),
            $this->getCriteriaFamilyMock(),
            $this->getOutputCategoryMock(),
            $this->getOutputCategoryMock()
        );

        $this->entityManager->expects($this->once())->method('flush');

        ($this->action)(
            $this->createRequest(
                'PUT',
                '/instance/default/dataset/observations/attribute/1'
            )->withParsedBody($this->getAttributeFields()),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
    }

    public function testDeleteAnAttribute(): void
    {
        $attribute = $this->getAttributeMock();
        $attribute->method('getId')->willReturn(1);
        $this->entityManager->method('find')->willReturn($attribute);

        $response = ($this->action)(
            $this->createRequest('DELETE', '/instance/default/dataset/observations/attribute/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
        $this->assertSame(
            '{"statusCode":200,"data":{"message":"Attribute with id 1 is removed!"}}',
            (string) $response->getBody()
        );
    }

    private function getAttributeMock(): Attribute|MockObject
    {
        return $this->createMock(Attribute::class);
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }
}
