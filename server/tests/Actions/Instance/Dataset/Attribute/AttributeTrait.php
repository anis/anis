<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset\Attribute;

use PHPUnit\Framework\MockObject\MockObject;
use App\Entity\CriteriaFamily;
use App\Entity\OutputCategory;

trait AttributeTrait
{
    private function getAttributeFields(): array
    {
        return [
            'id' => 1,
            'name' => 'name_one',
            'label' => 'label_one',
            'form_label' => 'form_label_one',
            'datatable_label' => null,
            'description' => 'description_one',
            'type' => 'integer',
            'search_type' => 'field',
            'operator' => '=',
            'dynamic_operator' => false,
            'null_operators_enabled' => false,
            'min' => null,
            'max' => null,
            'placeholder_min' => null,
            'placeholder_max' => null,
            'criteria_display' => 1,
            'output_display' => 1,
            'selected' => true,
            'renderer' => null,
            'renderer_config' => null,
            'order_by' => true,
            'archive' => false,
            'detail_display' => 1,
            'detail_renderer' => null,
            'detail_renderer_config' => null,
            'options' => [
                'label' => 'Three', 'value' => 'three', 'display' => 3,
                'label' => 'One', 'value' => 'one', 'display' => 1,
                'label' => 'Two', 'value' => 'two', 'display' => 2,
            ],
            'vo_utype' => null,
            'vo_ucd' => null,
            'vo_unit' => null,
            'vo_description' => null,
            'vo_datatype' => null,
            'vo_size' => null,
            'id_criteria_family' => 1,
            'id_output_category' => '1_1',
            'id_detail_output_category' => '1_1'
        ];
    }

    private function getCriteriaFamilyMock(): CriteriaFamily|MockObject
    {
        return $this->createMock(CriteriaFamily::class);
    }

    private function getOutputCategoryMock(): OutputCategory|MockObject
    {
        return $this->createMock(OutputCategory::class);
    }
}
