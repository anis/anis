<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Laminas\InputFilter\InputFilterInterface;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Entity\Instance;
use App\Entity\Dataset;
use App\Entity\Database;
use App\Entity\DatasetFamily;
use App\Actions\Instance\Dataset\DatasetListAction;

final class DatasetListActionTest extends TestCase
{
    use DatasetTrait;

    private DatasetListAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new DatasetListAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/dataset'),
            $this->createResponse(),
            ['name' => 'default']
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testGetAllInstances(): void
    {
        $this->entityManager->method('find')->willReturn($this->getInstanceMock());

        $repository = $this->getObjectRepositoryMock();
        $repository->expects($this->once())->method('findBy');
        $this->entityManager->method('getRepository')->with(Dataset::class)->willReturn($repository);
        ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset'),
            $this->createResponse(),
            ['name' => 'default']
        );
    }

    public function testAddANewInstance(): void
    {
        $this->entityManager->method('find')->willReturnOnConsecutiveCalls(
            $this->getInstanceMock(),
            $this->getDatabaseMock(),
            $this->getDatasetFamilyMock()
        );

        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($this->getDatasetFields());
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $this->entityManager->expects($this->once())->method('persist');

        $response = ($this->action)(
            $this->createRequest('POST', '/instance/default/dataset')->withParsedBody($this->getDatasetFields()),
            $this->createResponse(),
            ['name' => 'default']
        );
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    private function getInstanceMock(): Instance|MockObject
    {
        return $this->createMock(Instance::class);
    }

    private function getDatabaseMock(): Database|MockObject
    {
        return $this->createMock(Database::class);
    }

    private function getDatasetFamilyMock(): DatasetFamily|MockObject
    {
        return $this->createMock(DatasetFamily::class);
    }

    private function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }
}
