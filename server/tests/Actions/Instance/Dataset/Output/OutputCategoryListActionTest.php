<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset\Output;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Laminas\InputFilter\InputFilterInterface;
use Tests\TestCase;
use App\Entity\OutputFamily;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Actions\Instance\Dataset\Output\OutputCategoryListAction;

final class OutputCategoryListActionTest extends TestCase
{
    use OutputCategoryTrait;

    private OutputCategoryListAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new OutputCategoryListAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/dataset/observations/output-family/1/output-category'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, OPTIONS');
    }

    public function testGetAllOutputCategoriesFamilies(): void
    {
        $this->entityManager->method('find')->willReturn($this->getOutputFamilyMock());

        $repository = $this->getObjectRepositoryMock();
        $repository->expects($this->once())->method('findBy');
        $this->entityManager->method('getRepository')->with('App\Entity\OutputCategory')->willReturn($repository);
        ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset/observations/output-family/1/output-category'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
    }

    public function testAddANewOutputCategory(): void
    {
        $this->entityManager->method('find')->willReturn($this->getOutputFamilyMock());

        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($this->getOutputCategoryFields());
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $this->entityManager->expects($this->once())->method('persist');

        $response = ($this->action)(
            $this->createRequest(
                'POST',
                '/instance/default/dataset/observations/output-family/1/output-category'
            )->withParsedBody($this->getOutputCategoryFields()),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1]
        );
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    private function getOutputFamilyMock(): OutputFamily|MockObject
    {
        return $this->createMock(OutputFamily::class);
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }
}
