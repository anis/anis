<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset\Output;

trait OutputCategoryTrait
{
    private function getOutputCategoryFields(): array
    {
        return [
            'id' => 1,
            'label' => 'Output category',
            'display' => 10
        ];
    }
}
