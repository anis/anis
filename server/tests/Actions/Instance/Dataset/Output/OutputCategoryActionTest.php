<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset\Output;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Slim\Exception\HttpNotFoundException;
use Laminas\InputFilter\InputFilterInterface;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Entity\OutputCategory;
use App\Actions\Instance\Dataset\Output\OutputCategoryAction;

final class OutputCategoryActionTest extends TestCase
{
    use OutputCategoryTrait;

    private OutputCategoryAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new OutputCategoryAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/dataset/observations/output-family/1/output-category/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1, 'ocid' => 1]
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testOutputCategoryIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Output category with id 1 is not found');
        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset/observations/output-family/1/output-category/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1, 'ocid' => 1]
        );
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAnOutputCategoryByID(): void
    {
        $outputFamily = $this->getOutputCategoryMock();
        $outputFamily->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($outputFamily);
        ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset/observations/output-family/1/output-category/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1, 'ocid' => 1]
        );
    }

    public function testEditAnOutputCategory(): void
    {
        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($this->getOutputCategoryFields());
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $outputCategory = $this->getOutputCategoryMock();
        $this->entityManager->method('find')->willReturn($outputCategory);
        $this->entityManager->expects($this->once())->method('flush');

        ($this->action)(
            $this->createRequest(
                'PUT',
                '/instance/default/dataset/observations/output-family/1/output-category/1'
            )->withParsedBody($this->getOutputCategoryFields()),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1, 'ocid' => 1]
        );
    }

    public function testDeleteAnOutputCategory(): void
    {
        $outputCategory = $this->getOutputCategoryMock();
        $outputCategory->method('getId')->willReturn(1);
        $this->entityManager->method('find')->willReturn($outputCategory);

        $response = ($this->action)(
            $this->createRequest('DELETE', '/instance/default/dataset/observations/output-family/1/output-category/1'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations', 'id' => 1, 'ocid' => 1]
        );
        $this->assertSame(
            '{"statusCode":200,"data":{"message":"Output category with id 1 is removed!"}}',
            (string) $response->getBody()
        );
    }

    private function getOutputCategoryMock(): OutputCategory|MockObject
    {
        return $this->createMock(OutputCategory::class);
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }
}
