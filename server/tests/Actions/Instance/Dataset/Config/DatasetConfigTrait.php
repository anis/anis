<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset\Config;

trait DatasetConfigTrait
{
    private function getAliasConfigFields(): array
    {
        return [
            'table_alias' => 'aliases',
            'column_alias' => 'alias',
            'column_name' => 'name',
            'column_alias_long' => 'alias_long'
        ];
    }

    private function getConeSearchConfigFields(): array
    {
        return [
            'enabled' => true,
            'opened' => false,
            'column_ra' => 3,
            'column_dec' => 4,
            'resolver_enabled' => true,
            'default_ra' => null,
            'default_dec' => null,
            'default_radius' => 2.0,
            'default_ra_dec_unit' => 'degree',
            'plot_enabled' => true
        ];
    }

    private function getDetailConfigFields(): array
    {
        return [
            'content' => '<p>TEST</p>',
            'style_sheet' => '.foo { color: blue; }'
        ];
    }
}
