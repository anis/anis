<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Dataset\Config;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Laminas\InputFilter\InputFilterInterface;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Entity\Dataset;
use App\Entity\ConeSearchConfig;
use App\Actions\Instance\Dataset\Config\ConeSearchConfigAction ;

final class ConeSearchConfigActionTest extends TestCase
{
    use DatasetConfigTrait;

    private ConeSearchConfigAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new ConeSearchConfigAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/dataset/observations/cone-search-config'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations']
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, PUT, OPTIONS');
    }

    public function testGetConeSearchConfig(): void
    {
        $dataset = $this->getDatasetMock();
        $dataset->method('getName')->willReturn('observations');
        $coneSearchConfig = $this->getConeSearchConfigMock();
        $this->entityManager
            ->expects($this->exactly(2))
            ->method('find')
            ->willReturnOnConsecutiveCalls($dataset, $coneSearchConfig);
        ($this->action)(
            $this->createRequest('GET', '/instance/default/dataset/observations/cone-search-config'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations']
        );
    }

    public function testAddButConeSearchConfigIsAlreadyExists(): void
    {
        $this->entityManager->method('find')->willReturn($this->getConeSearchConfigMock());
        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('Cone-search config for the dataset observations is already exists');

        ($this->action)(
            $this->createRequest(
                'POST',
                '/instance/default/dataset/observations/cone-search-config'
            )->withParsedBody($this->getConeSearchConfigFields()),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations']
        );
    }

    public function testAddConeSearchConfig(): void
    {
        $this->entityManager->method('find')->willReturnOnConsecutiveCalls(
            null,
            $this->getDatasetMock()
        );

        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($this->getConeSearchConfigFields());
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $this->entityManager->expects($this->once())->method('persist');
        $this->entityManager->expects($this->once())->method('flush');

        $response = ($this->action)(
            $this->createRequest(
                'POST',
                '/instance/default/dataset/observations/cone-search-config'
            )->withParsedBody($this->getConeSearchConfigFields()),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations']
        );
        $this->assertEquals(201, (int) $response->getStatusCode());
    }

    public function testEditButConeSearchConfigIsNotFound(): void
    {
        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($this->getConeSearchConfigFields());
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Cone-search config for the dataset observations is not found');
        $response = ($this->action)(
            $this->createRequest('PUT', '/instance/default/dataset/observations/cone-search-config'),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations']
        );
        $this->assertEquals(200, (int) $response->getStatusCode());
    }

    public function testEditConeSearchConfig(): void
    {
        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($this->getConeSearchConfigFields());
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $coneSearchConfig = $this->getConeSearchConfigMock();
        $this->entityManager->method('find')->willReturn($coneSearchConfig);

        $this->entityManager->expects($this->once())->method('flush');
        ($this->action)(
            $this->createRequest(
                'PUT',
                '/instance/default/dataset/observations/cone-search-config'
            )->withParsedBody($this->getConeSearchConfigFields()),
            $this->createResponse(),
            ['name' => 'default', 'dname' => 'observations']
        );
    }

    private function getDatasetMock(): Dataset|MockObject
    {
        return $this->createMock(Dataset::class);
    }

    private function getConeSearchConfigMock(): ConeSearchConfig|MockObject
    {
        return $this->createMock(ConeSearchConfig::class);
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }
}
