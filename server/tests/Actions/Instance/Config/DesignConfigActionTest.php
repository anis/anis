<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Database;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Laminas\InputFilter\InputFilterInterface;
use Slim\Exception\HttpNotFoundException;
use Slim\Exception\HttpBadRequestException;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Actions\Instance\Config\DesignConfigAction;
use App\Entity\Instance;
use App\Entity\DesignConfig;

final class DesignConfigActionTest extends TestCase
{
    private DesignConfigAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->getEntityManager();
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new DesignConfigAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/design-config'),
            $this->createResponse(),
            []
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, POST, PUT, OPTIONS');
    }

    public function testGetDesignConfig(): void
    {
        $designConfig = $this->getDesignConfigMock();
        $designConfig->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($designConfig);
        ($this->action)(
            $this->createRequest('GET', '/instance/default/design-config'),
            $this->createResponse(),
            ['name' => 'default']
        );
    }

    public function testAddADesignConfig(): void
    {
        $fields = $this->getDesignConfigFields();

        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($fields);
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $this->entityManager
            ->expects($this->exactly(2))
            ->method('find')->willReturnOnConsecutiveCalls(null, $this->getInstance());
        $this->entityManager->expects($this->once())->method('flush');

        ($this->action)(
            $this->createRequest('POST', '/instance/default/design-config')->withParsedBody($fields),
            $this->createResponse(),
            ['name' => 'default']
        );
    }

    public function testAddADesignConfigButARecordAlreadyExists(): void
    {
        $designConfig = $this->getDesignConfigMock();
        $this->entityManager->method('find')->willReturn($designConfig);
        $this->expectException(HttpBadRequestException::class);

        ($this->action)(
            $this->createRequest('POST', '/instance/default/design-config')->withParsedBody([]),
            $this->createResponse(),
            ['name' => 'default']
        );
    }

    public function testEditADesignConfig(): void
    {
        $fields = $this->getDesignConfigFields();

        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($fields);
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $designConfig = $this->getDesignConfigMock();
        $this->entityManager->method('find')->willReturn($designConfig);
        $this->entityManager->expects($this->once())->method('flush');

        ($this->action)(
            $this->createRequest('PUT', '/instance/default/design-config')->withParsedBody($fields),
            $this->createResponse(),
            ['name' => 'default']
        );
    }

    public function testEditADesignConfigButEntityIsNotFound(): void
    {
        $this->entityManager->method('find')->willReturn(null);
        $this->expectException(HttpNotFoundException::class);

        ($this->action)(
            $this->createRequest('PUT', '/instance/default/design-config')->withParsedBody([]),
            $this->createResponse(),
            ['name' => 'default']
        );
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }

    public function getEntityManager(): EntityManager|MockObject
    {
        return $this->createMock(EntityManager::class);
    }

    private function getInstance(): Instance|MockObject
    {
        return $this->createMock(Instance::class);
    }

    private function getDesignConfigMock(): DesignConfig|MockObject
    {
        return $this->createMock(DesignConfig::class);
    }

    private function getDesignConfigFields(): array
    {
        return [
            'design_background_color' => 'darker green',
            'design_text_color' => '#212529',
            'design_font_family' => 'Roboto, sans-serif',
            'design_link_color' => '#007BFF',
            'design_link_hover_color' => '#0056B3',
            'design_logo' => 'path/to/logo',
            'design_logo_href' => null,
            'design_favicon' => 'path/to/favicon',
            'navbar_background_color' => '#F8F9FA',
            'navbar_border_bottom_color' => '#DEE2E6',
            'navbar_color_href' => '#000000',
            'navbar_font_family' => 'Roboto, sans-serif',
            'navbar_sign_in_btn_color' => '#28A745',
            'navbar_user_btn_color' => '#7AC29A',
            'footer_background_color' => '#F8F9FA',
            'footer_border_top_color' => '#DEE2E6',
            'footer_text_color' => '#000000',
            'footer_logos' => [
                [
                    'href' => 'http =>//lam.fr',
                    'title' => 'Laboratoire d\'Astrophysique de Marseille',
                    'file' => '/logo_lam_s.png',
                    'display' => 20
                ]
            ],
            'family_border_color' => '#DFDFDF',
            'family_header_background_color' => '#F7F7F7',
            'family_title_color' => '#007BFF',
            'family_title_bold' => false,
            'family_background_color' => '#FFFFFF',
            'family_text_color' => '#212529',
            'progress_bar_title' => 'Dataset search',
            'progress_bar_title_color' => '#000000',
            'progress_bar_subtitle' => 'Select a dataset, add criteria, select output columns and display the result.',
            'progress_bar_subtitle_color' => '#6C757D',
            'progress_bar_step_dataset_title' => 'Dataset selection',
            'progress_bar_step_criteria_title' => 'Search criteria',
            'progress_bar_step_output_title' => 'Output columns',
            'progress_bar_step_result_title' => 'Result table',
            'progress_bar_color' => '#E9ECEF',
            'progress_bar_active_color' => '#7AC29A',
            'progress_bar_circle_color' => '#FFFFFF',
            'progress_bar_circle_icon_color' => '#CCCCCC',
            'progress_bar_circle_icon_active_color' => '#FFFFFF',
            'progress_bar_text_color' => '#91B2BF',
            'progress_bar_text_bold' => false,
            'search_next_btn_color' => '#007BFF',
            'search_next_btn_hover_color' => '#007BFF',
            'search_next_btn_hover_text_color' => '#FFFFFF',
            'search_back_btn_color' => '#6C757D',
            'search_back_btn_hover_color' => '#6C757D',
            'search_back_btn_hover_text_color' => '#FFFFFF',
            'delete_current_query_btn_enabled' => false,
            'delete_current_query_btn_text' => 'Delete the current query',
            'delete_current_query_btn_color' => '#DC3545',
            'delete_current_query_btn_hover_color' => '#C82333',
            'delete_current_query_btn_hover_text_color' => '#FFFFFF',
            'delete_criterion_cross_color' => '#DC3545',
            'search_info_background_color' => '#E9ECEF',
            'search_info_text_color' => '#000000',
            'search_info_help_enabled' => true,
            'dataset_select_btn_color' => '#6C757D',
            'dataset_select_btn_hover_color' => '#6C757D',
            'dataset_select_btn_hover_text_color' => '#FFFFFF',
            'dataset_selected_icon_color' => '#28A745',
            'search_criterion_background_color' => '#7AC29A',
            'search_criterion_text_color' => '#000000',
            'output_columns_selected_color' => '#7AC29A',
            'output_columns_select_all_btn_color' => '#6C757D',
            'output_columns_select_all_btn_hover_color' => '#6C757D',
            'output_columns_select_all_btn_hover_text_color' => '#FFFFFF',
            'result_panel_border_size' => '1px',
            'result_panel_border_color' => '#DEE2E6',
            'result_panel_title_color' => '#000000',
            'result_panel_background_color' => '#FFFFFF',
            'result_panel_text_color' => '#000000',
            'result_download_btn_color' => '#007BFF',
            'result_download_btn_hover_color' => '#0069D9',
            'result_download_btn_text_color' => '#FFFFFF',
            'result_datatable_actions_btn_color' => '#007BFF',
            'result_datatable_actions_btn_hover_color' => '#0069D9',
            'result_datatable_actions_btn_text_color' => '#FFFFFF',
            'result_datatable_bordered' => true,
            'result_datatable_bordered_radius' => false,
            'result_datatable_border_color' => '#DEE2E6',
            'result_datatable_header_background_color' => '#FFFFFF',
            'result_datatable_header_text_color' => '#000000',
            'result_datatable_rows_background_color' => '#FFFFFF',
            'result_datatable_rows_text_color' => '#000000',
            'result_datatable_sorted_color' => '#C5C5C5',
            'result_datatable_sorted_active_color' => '#000000',
            'result_datatable_link_color' => '#007BFF',
            'result_datatable_link_hover_color' => '#0056B3',
            'result_datatable_rows_selected_color' => '#7AC29A',
            'result_datatable_pagination_link_color' => '#7AC29A',
            'result_datatable_pagination_active_bck_color' => '#7AC29A',
            'result_datatable_pagination_active_text_color' => '#FFFFFF',
            'samp_enabled' => true,
            'back_to_portal' => true,
            'user_menu_enabled' => true,
            'search_multiple_all_datasets_selected' => false,
            'search_multiple_progress_bar_title' => 'Search around a position in multiple datasets',
            'search_multiple_progress_bar_subtitle' =>
                'Fill RA & DEC position, select datasets and display the result.',
            'search_multiple_progress_bar_step_position' => 'Position',
            'search_multiple_progress_bar_step_datasets' => 'Datasets',
            'search_multiple_progress_bar_step_result' => 'Result'
        ];
    }
}
