<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Database;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Schema\Table;
use Doctrine\DBAL\Schema\View;
use Doctrine\ORM\EntityManager;
use Slim\Exception\HttpNotFoundException;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Search\DBALConnectionFactory;
use App\Entity\Database;
use App\Actions\Instance\Database\TableListAction;

final class TableListActionTest extends TestCase
{
    private TableListAction $action;
    private EntityManager|MockObject $entityManager;
    private DBALConnectionFactory|MockObject $connectionFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->connectionFactory = $this->createMock(DBALConnectionFactory::class);
        $this->action = new TableListAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->createMock(IInputFilterFactory::class),
            $this->connectionFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/database/1/table'),
            $this->createResponse(),
            []
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, OPTIONS');
    }

    public function testDatabaseIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Database with id 1 is not found');
        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default/database/1/table'),
            $this->createResponse(),
            ['name' => 'default', 'id' => 1]
        );
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetTablesAndViewsList(): void
    {
        $database = $this->getDatabaseMock();
        $this->entityManager->method('find')->willReturn($database);

        $table = $this->getTableMock();
        $table->method('getName')->willReturn('table1');
        $table2 = $this->getTableMock();
        $table2->method('getName')->willReturn('table2');

        $ap = $this->getAbstractPlatformMock();
        $ap->method('getName')->willReturn('sqlite');

        $sm = $this->getAbstractSchemaManagerMock();
        $sm->method('listTables')->willReturn([$table, $table2]);

        $view = $this->getViewMock();
        $view->method('getName')->willReturn('view1');
        $view2 = $this->getViewMock();
        $view2->method('getName')->willReturn('view2');

        $sm->method('listViews')->willReturn([$view, $view2]);

        $connection = $this->getConnectionMock();
        $connection->method('createSchemaManager')->willReturn($sm);
        $connection->method('getDatabasePlatform')->willReturn($ap);

        $this->connectionFactory->method('create')->willReturn($connection);

        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default/database/1/table'),
            $this->createResponse(),
            ['name' => 'default', 'id' => 1]
        );
        $this->assertSame(
            '{"statusCode":200,"data":["table1","table2","view1","view2"]}',
            (string) $response->getBody()
        );
    }

    public function testGetTablesAndViewsListWhenPlatformIsPostgres(): void
    {
        $database = $this->getDatabaseMock();
        $this->entityManager->method('find')->willReturn($database);

        $table = $this->getTableMock();
        $table->method('getName')->willReturn('table1');
        $table2 = $this->getTableMock();
        $table2->method('getName')->willReturn('table2');

        $ap = $this->getAbstractPlatformMock();
        $ap->method('getName')->willReturn('postgresql');

        $sm = $this->getAbstractSchemaManagerMock();
        $sm->method('listTables')->willReturn([$table, $table2]);

        $connection = $this->getConnectionMock();
        $connection->method('createSchemaManager')->willReturn($sm);
        $connection->method('getDatabasePlatform')->willReturn($ap);
        $connection->method('fetchAllAssociative')->willReturn([
            ['viewname' => 'view1'],
            ['viewname' => 'view2'],
        ]);

        $this->connectionFactory->method('create')->willReturn($connection);

        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default/database/1/table'),
            $this->createResponse(),
            ['name' => 'default', 'id' => 1]
        );
        $this->assertSame(
            '{"statusCode":200,"data":["table1","table2","view1","view2"]}',
            (string) $response->getBody()
        );
    }

    private function getDatabaseMock(): Database|MockObject
    {
        return $this->createMock(Database::class);
    }

    private function getTableMock(): Table|MockObject
    {
        return $this->createMock(Table::class);
    }

    private function getAbstractSchemaManagerMock(): AbstractSchemaManager|MockObject
    {
        return $this->createMock(AbstractSchemaManager::class);
    }

    private function getAbstractPlatformMock(): AbstractPlatform|MockObject
    {
        return $this->createMock(AbstractPlatform::class);
    }

    private function getViewMock(): View|MockObject
    {
        return $this->createMock(View::class);
    }

    private function getConnectionMock(): Connection|MockObject
    {
        return $this->createMock(Connection::class);
    }
}
