<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Database;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManager;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Search\DBALConnectionFactory;
use App\Entity\Database;
use App\Actions\Instance\Database\ColumnListAction;

final class ColumnListActionTest extends TestCase
{
    private ColumnListAction $action;
    private EntityManager|MockObject $entityManager;
    private DBALConnectionFactory|MockObject $connectionFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->connectionFactory = $this->createMock(DBALConnectionFactory::class);
        $this->action = new ColumnListAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->createMock(IInputFilterFactory::class),
            $this->connectionFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/database/1/table/my-table/column'),
            $this->createResponse(),
            []
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, OPTIONS');
    }

    public function testGetTableOrViewColumns(): void
    {
        $database = $this->getDatabaseMock();
        $this->entityManager->method('find')->willReturn($database);

        $type1 = $this->getType();
        $type1->method('getName')->willReturn('integer');
        $column1 = $this->getColumn();
        $column1->method('getName')->willReturn('column1');
        $column1->method('getType')->willReturn($type1);

        $type2 = $this->getType();
        $type2->method('getName')->willReturn('string');
        $column2 = $this->getColumn();
        $column2->method('getName')->willReturn('column2');
        $column2->method('getType')->willReturn($type2);

        $sm = $this->getAbstractSchemaManagerMock();
        $sm->method('listTableColumns')->willReturn([$column1, $column2]);

        $databasePlatform = $this->getDatabasePlatformMock();
        $databasePlatform->method('getName')->willReturn('default');

        $connection = $this->getConnectionMock();
        $connection->method('getDatabasePlatform')->willReturn($databasePlatform);
        $connection->method('createSchemaManager')->willReturn($sm);

        $this->connectionFactory->method('create')->willReturn($connection);

        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default/database/1/table/my-table/column'),
            $this->createResponse(),
            ['name' => 'default', 'id' => 1, 'tname' => 'my-table']
        );
        $this->assertSame(
            '{"statusCode":200,"data":[{"name":"column1","type":"integer"},{"name":"column2","type":"string"}]}',
            (string) $response->getBody()
        );
    }

    private function getDatabaseMock(): Database|MockObject
    {
        return $this->createMock(Database::class);
    }

    private function getConnectionMock(): Connection|MockObject
    {
        return $this->createMock(Connection::class);
    }

    private function getAbstractSchemaManagerMock(): AbstractSchemaManager|MockObject
    {
        return $this->createMock(AbstractSchemaManager::class);
    }

    private function getColumn(): Column|MockObject
    {
        return $this->createMock(Column::class);
    }

    private function getType(): Type|MockObject
    {
        return $this->createMock(Type::class);
    }

    private function getDatabasePlatformMock(): AbstractPlatform|MockObject
    {
        return $this->createMock(AbstractPlatform::class);
    }
}
