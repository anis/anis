<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance\Database;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Laminas\InputFilter\InputFilterInterface;
use Slim\Exception\HttpNotFoundException;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Entity\Database;
use App\Actions\Instance\Database\DatabaseAction;

final class DatabaseActionTest extends TestCase
{
    private DatabaseAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new DatabaseAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/database/1'),
            $this->createResponse(),
            []
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testDatabaseIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Database with id 1 is not found');
        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default/database/1'),
            $this->createResponse(),
            ['name' => 'default', 'id' => 1]
        );
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetADatabaseById(): void
    {
        $database = $this->getDatabaseMock();
        $database->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($database);
        ($this->action)(
            $this->createRequest('GET', '/instance/default/database/1'),
            $this->createResponse(),
            ['name' => 'default', 'id' => 1]
        );
    }

    public function testEditADatabase(): void
    {
        $fields = [
            'id' => 1,
            'label' => 'Label',
            'dbname' => 'test',
            'dbtype' => 'pgsql',
            'dbhost' => 'db',
            'dbport' => 5432,
            'dblogin' => 'test',
            'dbpassword' => 'test',
            'dbdsn_file' => '/mnt/password'
        ];

        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($fields);
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $database = $this->getDatabaseMock();
        $this->entityManager->method('find')->willReturn($database);
        $this->entityManager->expects($this->once())->method('flush');

        ($this->action)(
            $this->createRequest('PUT', '/instance/default/database/1')->withParsedBody($fields),
            $this->createResponse(),
            ['name' => 'default', 'id' => 1]
        );
    }

    public function testDeleteADatabase(): void
    {
        $database = $this->getDatabaseMock();
        $database->method('getId')->willReturn(1);
        $this->entityManager->method('find')->willReturn($database);

        $response = ($this->action)(
            $this->createRequest('DELETE', '/instance/default/database/1'),
            $this->createResponse(),
            ['name' => 'default', 'id' => 1]
        );
        $this->assertSame(
            '{"statusCode":200,"data":{"message":"Database with id 1 is removed!"}}',
            (string) $response->getBody()
        );
    }

    private function getDatabaseMock(): Database|MockObject
    {
        return $this->createMock(Database::class);
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }
}
