<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\IO\Instance\Export\IExportInstance;
use App\Actions\Instance\InstanceExportAction;
use App\Entity\Instance;

final class InstanceExportActionTest extends TestCase
{
    private InstanceExportAction $action;
    private EntityManager|MockObject $entityManager;
    private IExportInstance|MockObject $exportInstance;

    protected function setUp(): void
    {
        $this->exportInstance = $this->createMock(IExportInstance::class);
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new InstanceExportAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->createMock(IInputFilterFactory::class),
            $this->exportInstance
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/export'),
            $this->createResponse(),
            []
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, OPTIONS');
    }

    public function testExportInstance(): void
    {
        $instance = $this->getInstanceMock();
        $this->entityManager->method('find')->willReturn($instance);
        $this->exportInstance->expects($this->once())->method('export')->with($instance)->willReturn([]);

        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default/export'),
            $this->createResponse(),
            ['name' => 'default']
        );
        $this->assertEquals(200, (int) $response->getStatusCode());
    }

    private function getInstanceMock(): Instance|MockObject
    {
        return $this->createMock(Instance::class);
    }
}
