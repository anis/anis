<?php

/*
 * This file is part of Anis Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance;

use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Actions\Instance\InstanceFileExplorerAction;

final class InstanceFileExplorerActionTest extends TestCase
{
    private InstanceFileExplorerAction $action;

    protected function setUp(): void
    {
        $this->action = new InstanceFileExplorerAction(
            $this->createMock(LoggerInterface::class),
            $this->createMock(EntityManager::class),
            $this->createMock(SettingsInterface::class),
            $this->createMock(IInputFilterFactory::class)
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)(
            $this->createRequest('OPTIONS', '/instance/default/file-explorer'),
            $this->createResponse(),
            []
        );
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, OPTIONS');
    }
}
