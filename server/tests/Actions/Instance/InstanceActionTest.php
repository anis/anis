<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Instance;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Slim\Exception\HttpNotFoundException;
use Laminas\InputFilter\InputFilterInterface;
use Tests\TestCase;
use App\Specification\Factory\IInputFilterFactory;
use App\Settings\SettingsInterface;
use App\Entity\Instance;
use App\Actions\Instance\InstanceAction;

final class InstanceActionTest extends TestCase
{
    use InstanceTrait;

    private InstanceAction $action;
    private EntityManager|MockObject $entityManager;
    private IInputFilterFactory|MockObject $inputFilterFactory;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->inputFilterFactory = $this->createMock(IInputFilterFactory::class);
        $this->action = new InstanceAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->inputFilterFactory
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)($this->createRequest('OPTIONS', '/instance/default'), $this->createResponse(), []);
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, PUT, DELETE, OPTIONS');
    }

    public function testInstanceIsNotFound(): void
    {
        $this->expectException(HttpNotFoundException::class);
        $this->expectExceptionMessage('Instance with name default is not found');
        $response = ($this->action)(
            $this->createRequest('GET', '/instance/default'),
            $this->createResponse(),
            ['name' => 'default']
        );
        $this->assertEquals(404, (int) $response->getStatusCode());
    }

    public function testGetAnInstanceByName(): void
    {
        $instance = $this->getInstanceMock();
        $instance->expects($this->once())->method('jsonSerialize');
        $this->entityManager->method('find')->willReturn($instance);
        ($this->action)(
            $this->createRequest('GET', '/instance/default'),
            $this->createResponse(),
            ['name' => 'default']
        );
    }

    public function testEditAnInstance(): void
    {
        $inputFilter = $this->getInputFilter();
        $inputFilter->method('getValues')->willReturn($this->getInstanceFields());
        $this->inputFilterFactory->method('getInputFilter')->willReturn($inputFilter);
        $this->inputFilterFactory->method('isValid')->willReturn(true);

        $instance = $this->getInstanceMock();
        $this->entityManager->method('find')->willReturn($instance);
        $this->entityManager->expects($this->once())->method('flush');

        ($this->action)(
            $this->createRequest('PUT', '/instance/default')->withParsedBody($this->getInstanceFields()),
            $this->createResponse(),
            ['name' => 'default']
        );
    }

    public function testDeleteAnInstance(): void
    {
        $instance = $this->getInstanceMock();
        $instance->method('getName')->willReturn('aspic');
        $this->entityManager->method('find')->willReturn($instance);

        $response = ($this->action)(
            $this->createRequest('DELETE', '/instance/default'),
            $this->createResponse(),
            ['name' => 'aspic']
        );
        $this->assertSame(
            '{"statusCode":200,"data":{"message":"Instance with name aspic is removed!"}}',
            (string) $response->getBody()
        );
    }

    private function getInstanceMock(): Instance|MockObject
    {
        return $this->createMock(Instance::class);
    }

    public function getInputFilter(): InputFilterInterface|MockObject
    {
        return $this->createMock(InputFilterInterface::class);
    }
}
