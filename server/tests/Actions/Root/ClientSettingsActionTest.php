<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Root;

use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Tests\TestCase;
use App\Settings\Settings;
use App\Actions\Root\ClientSettingsAction;
use App\Specification\Factory\IInputFilterFactory;

final class ClientSettingsActionTest extends TestCase
{
    private ClientSettingsAction $action;

    protected function setUp(): void
    {
        $this->action = new ClientSettingsAction(
            $this->createMock(LoggerInterface::class),
            $this->createMock(EntityManager::class),
            new Settings([
                'mailer' => [
                    'enabled' => 1,
                    'dsn' => 'smtp://mailer:25?verify_peer=0'
                ],
                'services_url' => 'http://localhost:5000',
                'base_href' =>  '/',
                'sso' => [
                    'auth_url' => 'http://localhost:8180',
                    'realm' => 'anis',
                    'client_id' => 'anis-client'
                ],
                'token' => [
                    'enabled' => 0,
                    'admin_roles' => 'anis_admin,superuser',
                ],
                'matomo' => [
                    'enabled' => 0,
                    'site_id' => 1,
                    'tracker_url' => 'http://localhost:8888'
                ]
            ]),
            $this->createMock(IInputFilterFactory::class)
        );
    }

    public function testAction(): void
    {
        $response = ($this->action)($this->createRequest('GET', '/client-settings'), $this->createResponse(), []);
        $payload = [
            'statusCode' => 200,
            'data' => [
                'servicesUrl' => 'http://localhost:5000',
                'baseHref' => '/',
                'authenticationEnabled' => 0,
                'mailerEnabled' => 1,
                'ssoAuthUrl' => 'http://localhost:8180',
                'ssoRealm' => 'anis',
                'ssoClientId' => 'anis-client',
                'adminRoles' => 'anis_admin,superuser',
                'matomoEnabled' => 0,
                'matomoSiteId' => 1,
                'matomoTrackerUrl' => 'http://localhost:8888'
            ]
        ];
        $this->assertSame(json_encode($payload), (string) $response->getBody());
    }
}
