<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Tests\Actions\Root;

use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\Connection;
use Tests\TestCase;
use App\Settings\SettingsInterface;
use App\Actions\Root\RootAction;
use App\Specification\Factory\IInputFilterFactory;

final class RootActionTest extends TestCase
{
    private RootAction $action;
    private EntityManager|MockObject $entityManager;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->action = new RootAction(
            $this->createMock(LoggerInterface::class),
            $this->entityManager,
            $this->createMock(SettingsInterface::class),
            $this->createMock(IInputFilterFactory::class)
        );
    }

    public function testOptionsHttpMethod(): void
    {
        $response = ($this->action)($this->createRequest('OPTIONS', '/'), $this->createResponse(), []);
        $this->assertSame($response->getHeaderLine('Access-Control-Allow-Methods'), 'GET, OPTIONS');
    }

    public function testAction(): void
    {
        $connection = $this->createMock(Connection::class);
        $connection->method('connect')->willReturn(true);
        $this->entityManager->method('getConnection')->willReturn($connection);

        $response = ($this->action)($this->createRequest('GET', '/'), $this->createResponse(), []);
        $this->assertSame(
            '{"statusCode":200,"data":{"message":"it works!","version":"3.16.0"}}',
            (string) $response->getBody()
        );
    }
}
