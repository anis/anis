<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

use Psr\Container\ContainerInterface;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        \Psr\Log\LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get(\App\Settings\SettingsInterface::class);

            $loggerSettings = $settings->get('logger');
            $logger = new \Monolog\Logger($loggerSettings['name']);

            $processor = new \Monolog\Processor\UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new \Monolog\Handler\StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
        \Doctrine\ORM\EntityManagerInterface::class => function (ContainerInterface $c) {
            // Determines whether the console doctrine programme is running
            $doctrineScriptRunning = false;
            if (isset($GLOBALS['doctrine_script_running']) && $GLOBALS['doctrine_script_running']) {
                $doctrineScriptRunning = true;
            }

            $settings = $c->get(\App\Settings\SettingsInterface::class);

            if ($settings->get('doctrine')['dev_mode'] && !$doctrineScriptRunning) {
                $logger = $c->get(\Psr\Log\LoggerInterface::class);
            } else {
                $logger = new \Psr\Log\NullLogger();
            }

            if ($settings->get('doctrine')['dev_mode']) {
                $queryCache = new \Symfony\Component\Cache\Adapter\ArrayAdapter();
                $metadataCache = new \Symfony\Component\Cache\Adapter\ArrayAdapter();
            } else {
                $queryCache = new \Symfony\Component\Cache\Adapter\PhpFilesAdapter(
                    directory: $settings->get('doctrine')['cache_dir']
                );
                $metadataCache = new \Symfony\Component\Cache\Adapter\PhpFilesAdapter(
                    directory: $settings->get('doctrine')['cache_dir']
                );
            }

            $config = new \Doctrine\ORM\Configuration;
            $config->setMetadataCache($metadataCache);
            $driverImpl = new \Doctrine\ORM\Mapping\Driver\AttributeDriver(
                $settings->get('doctrine')['metadata_dirs']
            );
            $config->setMetadataDriverImpl($driverImpl);
            $config->setQueryCache($queryCache);
            $config->setProxyDir($settings->get('doctrine')['proxy_dir']);
            $config->setProxyNamespace('App\Proxies');
            $config->setAutoGenerateProxyClasses(false);
            $config->setMiddlewares([new \Doctrine\DBAL\Logging\Middleware($logger)]);

            // Retrieving anis metamodel connection parameters
            $connectionParams = $settings->get('doctrine')['connection'];

            // If the dsn_file parameter exists, the DSN is used to connect to the database
            $dsnFile = $connectionParams['dsn_file'];
            if ($dsnFile) {
                if (!file_exists($dsnFile)) {
                    throw new Exception(
                        'The password file for the anis metamodel does not exist (' . $dsnFile . ')'
                    );
                }
                $dsn = file_get_contents($dsnFile);

                $dsnParser = new Doctrine\DBAL\Tools\DsnParser(['postgres' => 'pdo_pgsql']);
                $connectionParams = $dsnParser->parse($dsn);
            }

            $connection = \Doctrine\DBAL\DriverManager::getConnection(
                $connectionParams, $config
            );
            return new \Doctrine\ORM\EntityManager($connection, $config);
        },
        \PhpAmqpLib\Connection\AbstractConnection::class => function(ContainerInterface $c) {
            $settings = $c->get(\App\Settings\SettingsInterface::class);

            $rmqSettings = $settings->get('rmq');
            return new \PhpAmqpLib\Connection\AMQPStreamConnection(
                $rmqSettings['host'],
                $rmqSettings['port'],
                $rmqSettings['user'],
                $rmqSettings['password']
            );
        },
        \Symfony\Component\Mailer\MailerInterface::class => function(ContainerInterface $c) {
            $settings = $c->get(\App\Settings\SettingsInterface::class);

            if ($settings->get('mailer')['enabled']) {
                $transport = \Symfony\Component\Mailer\Transport::fromDsn($settings->get('mailer')['dsn']);
                return new Symfony\Component\Mailer\Mailer($transport);
            } else {
                return null;
            }
        },
        \App\Search\DBALConnectionFactory::class => function () {
            return new \App\Search\DBALConnectionFactory();
        },
        \App\Search\Query\AnisQueryBuilder::class => function () {
            return (new \App\Search\Query\AnisQueryBuilder())
                ->addQueryPart(new App\Search\Query\From())
                ->addQueryPart(new App\Search\Query\Count())
                ->addQueryPart(new App\Search\Query\SelectAll())
                ->addQueryPart(new App\Search\Query\Select())
                ->addQueryPart(new App\Search\Query\ConeSearch())
                ->addQueryPart(new App\Search\Query\Where(new App\Search\Query\Operator\OperatorFactory()))
                ->addQueryPart(new App\Search\Query\Order())
                ->addQueryPart(new App\Search\Query\Limit());
        },
        \App\Search\Response\ResponseFactory::class  => function () {
            return new \App\Search\Response\ResponseFactory();
        },
        \App\IO\Dataset\Export\IExportDataset::class => function () {
            return new \App\IO\Dataset\Export\ExportDataset();
        },
        \App\IO\Instance\Export\IExportInstance::class => function (ContainerInterface $c) {
            return new \App\IO\Instance\Export\ExportInstance($c->get(\App\IO\Dataset\Export\IExportDataset::class));
        },
        \App\Specification\Factory\IInputFilterFactory::class => function () {
            return new \App\Specification\Factory\InputFilterFactory(new \Laminas\InputFilter\Factory());
        },
        \App\IO\Dataset\Import\ICheckImportDataset::class => function (ContainerInterface $c) {
            return new \App\IO\Dataset\Import\CheckImportDataset(
                $c->get(\Doctrine\ORM\EntityManagerInterface::class),
                $c->get(\App\Specification\Factory\IInputFilterFactory::class)
            );
        },
        \App\IO\Dataset\Import\IImportDataset::class => function (ContainerInterface $c) {
            return new \App\IO\Dataset\Import\ImportDataset($c->get(\Doctrine\ORM\EntityManagerInterface::class));
        },
        \App\IO\Instance\Import\ICheckImportInstance::class => function(ContainerInterface $c) {
            return new \App\IO\Instance\Import\CheckImportInstance(
                $c->get(\Doctrine\ORM\EntityManagerInterface::class),
                $c->get(\App\Specification\Factory\IInputFilterFactory::class),
                $c->get(\App\IO\Dataset\Import\ICheckImportDataset::class)
            );
        },
        \App\IO\Instance\Import\IImportInstance::class => function (ContainerInterface $c) {
            return new \App\IO\Instance\Import\ImportInstance(
                $c->get(\Doctrine\ORM\EntityManagerInterface::class),
                $c->get(\App\IO\Dataset\Import\IImportDataset::class)->setFlush(false)
            );
        }
    ]);
};
