<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

use App\Settings\Settings;
use App\Settings\SettingsInterface;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    // Global Settings Object
    $containerBuilder->addDefinitions([
        SettingsInterface::class => function () {
            return new Settings([
                // Should be set to false in production
                // If true: Returns error description in the http response
                'displayErrorDetails' => boolval(getenv('DISPLAY_ERROR_DETAILS')),
                'logError'            => true,
                'logErrorDetails'     => true,
                'logger' => [
                    'name' => getenv('LOGGER_NAME'),
                    'path' => getenv('LOGGER_PATH'),
                    'level' => getenv('LOGGER_LEVEL')
                ],
                'doctrine' => [
                    // Enables or disables Doctrine metadata caching
                    // for either performance or convenience during development.
                    'dev_mode' => getenv('DATABASE_DEV_MODE'),

                    // Path where Doctrine will cache the processed metadata
                    // when 'dev_mode' is false.
                    'cache_dir' => __DIR__ . '/../var/cache',

                    // Path where Doctrine will retrieve doctrine proxies
                    'proxy_dir' => __DIR__ . '/../doctrine-proxy',

                    // List of paths where Doctrine will search for metadata.
                    // Metadata can be either YML/XML files or PHP classes annotated
                    // with comments or PHP8 attributes.
                    'metadata_dirs' => ['src/Entity'],

                    // The parameters Doctrine needs to connect to your database.
                    // These parameters depend on the driver (for instance the 'pdo_sqlite' driver
                    // needs a 'path' parameter and doesn't use most of the ones shown in this example).
                    // Refer to the Doctrine documentation to see the full list
                    // of valid parameters: https://www.doctrine-project.org/projects/doctrine-dbal/en/current/reference/configuration.html
                    'connection' => [
                        'driver' => getenv('DATABASE_CO_DRIVER'),
                        'path' => getenv('DATABASE_CO_PATH'),
                        'host' => getenv('DATABASE_CO_HOST'),
                        'port' => (int) getenv('DATABASE_CO_PORT'),
                        'dbname' => getenv('DATABASE_CO_DBNAME'),
                        'user' => getenv('DATABASE_CO_USER'),
                        'password' => getenv('DATABASE_CO_PASSWORD'),
                        'dsn_file' => getenv('DATABASE_DSN_FILE'),
                        'charset' => 'utf-8'
                    ]
                ],
                'data_path' => getenv('DATA_PATH'),
                'site_url' => getenv('SITE_URL'),
                'archive_folder' => getenv('ARCHIVE_FOLDER'),
                'archive_retention_period' => getenv('ARCHIVE_RETENTION_PERIOD'),
                'mailer' => [
                    'enabled' => boolval(getenv('MAILER_ENABLED')),
                    'dsn' => getenv('MAILER_DSN')
                ],
                'services_url' => getenv('SERVICES_URL'),
                'base_href' => getenv('BASE_HREF'),
                'sso' => [
                    'auth_url' => getenv('SSO_AUTH_URL'),
                    'realm' => getenv('SSO_REALM'),
                    'client_id' => getenv('SSO_CLIENT_ID')
                ],
                'token' => [
                    'enabled' => boolval(getenv('TOKEN_ENABLED')),
                    'jwks_url' => getenv('TOKEN_JWKS_URL'),
                    'admin_roles' => getenv('TOKEN_ADMIN_ROLES')
                ],
                'rmq' => [
                    'host' => getenv('RMQ_HOST'),
                    'port' => getenv('RMQ_PORT'),
                    'user' => getenv('RMQ_USER'),
                    'password' => getenv('RMQ_PASSWORD')
                ],
                'matomo' => [
                    'enabled' => boolval(getenv('MATOMO_ENABLED')),
                    'site_id' => intval(getenv('MATOMO_SITE_ID')),
                    'tracker_url' => getenv('MATOMO_TRACKER_URL')
                ]
            ]);
        }
    ]);
};
