<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

use Slim\App;
use Slim\Routing\RouteCollectorProxy;
use Psr\Container\ContainerInterface;

return function (App $app, ContainerInterface $container) {
    $app->map([OPTIONS, GET], '/', \App\Actions\Root\RootAction::class);
    $app->map([OPTIONS, GET], '/client-settings', \App\Actions\Root\ClientSettingsAction::class);

    // Metamodel actions (ANIS admin only)
    $app->group('', function (RouteCollectorProxy $group) {
        $group->map([OPTIONS, GET], '/admin-file-explorer[{fpath:.*}]', \App\Actions\Admin\AdminFileExplorerAction::class);
    })->add(new \App\Middleware\RouteGuardMiddleware(
        $container->get(\App\Settings\SettingsInterface::class)->get('token')['enabled'], 
        [GET, POST, PUT, DELETE], 
        explode(',', $container->get(\App\Settings\SettingsInterface::class)->get('token')['admin_roles'])
    ));

    // Metamodel actions
    $app->group('', function (RouteCollectorProxy $group) {
        $group->map([OPTIONS, GET, POST], '/instance', \App\Actions\Instance\InstanceListAction::class);
        $group->map([OPTIONS, POST], '/instance/import', \App\Actions\Instance\InstanceImportAction::class);
        $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}', \App\Actions\Instance\InstanceAction::class);
        $group->map([OPTIONS, GET, POST, PUT], '/instance/{name}/design-config', \App\Actions\Instance\Config\DesignConfigAction::class);
        $group->map([OPTIONS, GET, POST], '/instance/{name}/group', \App\Actions\Instance\Group\InstanceGroupListAction::class);
        $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}/group/{role}', \App\Actions\Instance\Group\InstanceGroupAction::class);
        $group->map([OPTIONS, GET, POST], '/instance/{name}/database', \App\Actions\Instance\Database\DatabaseListAction::class);
        $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}/database/{id}', \App\Actions\Instance\Database\DatabaseAction::class);
        $group->map([OPTIONS, GET], '/instance/{name}/database/{id}/table', \App\Actions\Instance\Database\TableListAction::class);
        $group->map([OPTIONS, GET], '/instance/{name}/database/{id}/table/{tname}/column', \App\Actions\Instance\Database\ColumnListAction::class);
        $group->map([OPTIONS, GET], '/instance/{name}/export', \App\Actions\Instance\InstanceExportAction::class);
        $group->map([OPTIONS, GET], '/instance/{name}/file-explorer[{fpath:.*}]', \App\Actions\Instance\InstanceFileExplorerAction::class);
        $group->map([OPTIONS, GET, POST], '/instance/{name}/dataset-family', \App\Actions\Instance\Family\DatasetFamilyListAction::class);
        $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}/dataset-family/{id}', \App\Actions\Instance\Family\DatasetFamilyAction::class);
        $group->map([OPTIONS, GET, POST], '/instance/{name}/menu-item', \App\Actions\Instance\Menu\MenuItemListAction::class);
        $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}/menu-item/{id}', \App\Actions\Instance\Menu\MenuItemAction::class);
        $group->map([OPTIONS, GET, POST], '/instance/{name}/menu-item/{fid}/item', \App\Actions\Instance\Menu\MenuFamilyItemListAction::class);
        $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}/menu-item/{fid}/item/{id}', \App\Actions\Instance\Menu\MenuFamilyItemAction::class);
        $group->map([OPTIONS, GET, POST], '/instance/{name}/dataset', \App\Actions\Instance\Dataset\DatasetListAction::class);
        $group->map([OPTIONS, POST], '/instance/{name}/dataset/import', \App\Actions\Instance\Dataset\DatasetImportAction::class);
        $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}/dataset/{dname}', \App\Actions\Instance\Dataset\DatasetAction::class);
        $group->map([OPTIONS, GET], '/instance/{name}/dataset/{dname}/verify-token', \App\Actions\Instance\Dataset\VerifyTokenAction::class);
        $group->map([OPTIONS, GET, POST], '/instance/{name}/dataset/{dname}/group', \App\Actions\Instance\Dataset\Group\DatasetGroupListAction::class);
        $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}/dataset/{dname}/group/{role}', \App\Actions\Instance\Dataset\Group\DatasetGroupAction::class);
        $group->map([OPTIONS, GET], '/instance/{name}/dataset/{dname}/export', \App\Actions\Instance\Dataset\DatasetExportAction::class);
        $group->map([OPTIONS, GET], '/instance/{name}/dataset/{dname}/file-explorer[{fpath:.*}]', \App\Actions\Instance\Dataset\DatasetFileExplorerAction::class);
        $group->map([OPTIONS, GET, POST], '/instance/{name}/dataset/{dname}/criteria-family', \App\Actions\Instance\Dataset\Criteria\CriteriaFamilyListAction::class);
        $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}/dataset/{dname}/criteria-family/{id}', \App\Actions\Instance\Dataset\Criteria\CriteriaFamilyAction::class);
        $group->map([OPTIONS, GET, POST], '/instance/{name}/dataset/{dname}/output-family', \App\Actions\Instance\Dataset\Output\OutputFamilyListAction::class);
        $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}/dataset/{dname}/output-family/{id}', \App\Actions\Instance\Dataset\Output\OutputFamilyAction::class);
        $group->map([OPTIONS, GET, POST], '/instance/{name}/dataset/{dname}/output-family/{id}/output-category', \App\Actions\Instance\Dataset\Output\OutputCategoryListAction::class);
        $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}/dataset/{dname}/output-family/{id}/output-category/{ocid}', \App\Actions\Instance\Dataset\Output\OutputCategoryAction::class);
        $group->map([OPTIONS, GET, POST], '/instance/{name}/dataset/{dname}/attribute', \App\Actions\Instance\Dataset\Attribute\AttributeListAction::class);
        $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}/dataset/{dname}/attribute/{id}', \App\Actions\Instance\Dataset\Attribute\AttributeAction::class);
        $group->map([OPTIONS, GET, PUT], '/instance/{name}/dataset/{dname}/attribute/{id}/distinct', \App\Actions\Instance\Dataset\Attribute\AttributeDistinctAction::class);
        $group->map([OPTIONS, GET, POST], '/instance/{name}/dataset/{dname}/image', \App\Actions\Instance\Dataset\File\ImageListAction::class);
        $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}/dataset/{dname}/image/{id}', \App\Actions\Instance\Dataset\File\ImageAction::class);
        $group->map([OPTIONS, GET, POST], '/instance/{name}/dataset/{dname}/file', \App\Actions\Instance\Dataset\File\FileListAction::class);
        $group->map([OPTIONS, GET, PUT, DELETE], '/instance/{name}/dataset/{dname}/file/{id}', \App\Actions\Instance\Dataset\File\FileAction::class);
        $group->map([OPTIONS, GET, POST, PUT], '/instance/{name}/dataset/{dname}/cone-search-config', \App\Actions\Instance\Dataset\Config\ConeSearchConfigAction::class);
        $group->map([OPTIONS, GET, POST, PUT], '/instance/{name}/dataset/{dname}/detail-config', \App\Actions\Instance\Dataset\Config\DetailConfigAction::class);
        $group->map([OPTIONS, GET, POST, PUT], '/instance/{name}/dataset/{dname}/alias-config', \App\Actions\Instance\Dataset\Config\AliasConfigAction::class);
    })->add(new \App\Middleware\RouteGuardMiddleware(
        $container->get(\App\Settings\SettingsInterface::class)->get('token')['enabled'], 
        [POST, PUT, DELETE], 
        explode(',', $container->get(\App\Settings\SettingsInterface::class)->get('token')['admin_roles'])
    ));

    // Search actions
    $app->map([OPTIONS, GET], '/search/{name}/{dname}', \App\Actions\Search\SearchAction::class);
    $app->map([OPTIONS, GET], '/search-alias/{name}/{dname}/{alias}', \App\Actions\Search\SearchAliasAction::class);

    // Archive actions
    $app->map([OPTIONS, GET], '/archive/start-task-create-archive/{name}/{dname}', \App\Actions\Archive\StartTaskCreateArchiveAction::class);
    $app->map([OPTIONS, GET], '/archive/is-archive-available/{id}', \App\Actions\Archive\IsArchiveAvailableAction::class);
    $app->map([OPTIONS, GET], '/archive/download-archive/{id}', \App\Actions\Archive\DownloadArchiveAction::class);
    $app->map([OPTIONS, GET], '/archive/mark-archive-as-in-progress/{id}', \App\Actions\Archive\MarkArchiveAsInProgressAction::class);
    $app->map([OPTIONS, GET], '/archive/mark-archive-as-finished/{id}', \App\Actions\Archive\MarkArchiveAsFinishedAction::class);
    $app->map([OPTIONS, GET], '/archive/mark-archive-as-deleted/{id}', \App\Actions\Archive\MarkArchiveAsDeletedAction::class);
    $app->map([OPTIONS, GET], '/archive/archive-list-by-user', \App\Actions\Archive\ArchiveListByUserAction::class);
};
