<?php

/*
 * This file is part of ANIS Server.
 *
 * (c) Laboratoire d'Astrophysique de Marseille / CNRS
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

use Psr\Container\ContainerInterface;
use Slim\App;

return function (App $app, ContainerInterface $container) {
    $settings = $container->get(\App\Settings\SettingsInterface::class);
    $tokenSettings = $settings->get('token');
    $app->add(new \App\Middleware\AuthorizationMiddleware(
        $tokenSettings['enabled'],
        $tokenSettings['jwks_url']
    ));
};
