#!/bin/bash
set -e

XDEBUG_MODE=coverage ./vendor/bin/phpunit --bootstrap ./tests/bootstrap.php --whitelist src --colors --coverage-html ./phpunit-coverage ./tests